import 'package:flutter/material.dart';

class WashCountProvider extends ChangeNotifier {
  int? _washTshirts;

  void setTshirts(int? washTshirts) {
    _washTshirts = washTshirts;
    notifyListeners();
  }

  int? get washTshirts => _washTshirts;
}
