class App24UserAppAPI {
  //static var deleteAddress;

  App24UserAppAPI._();

//staging
//   static const String baseUrl = "https://stagingapi.app24.co.in/api";
//   static const String socketUrl = 'https://stagingapi.app24.co.in:8993/';
//   static const String configurationUrl = 'https://stagingapi.app24.co.in/base';
//   static const String imageUrl = 'https://stagingapi.app24.co.in/storage/1/home_delivery_bill/';
//dev
 // static const String baseUrl = "https://api.app24.co.in/api";
  static const String baseUrl = "https://api.laundrybasketuae.com/api";
  //static const String socketUrl = 'https://api.app24.co.in/api/';
  static const String socketUrl = 'https://api.laundrybasketuae.com:8995/';
  static const String configurationUrl = 'https://api.app24.co.in/base';
  static const String imageUrl =
      'https://devapi.app24.co.in/storage/1/home_delivery_bill/';

// production
//    static const String baseUrl = 'https://api.app24.co.in/api';
//    static const String socketUrl = 'https://api.app24.co.in:8990/';
//    static const String configurationUrl = 'https://api.app24.co.in/base';
//    static const String imageUrl = 'https://api.app24.co.in/storage/1/home_delivery_bill/';

  static const String loginUrl = '/v1/user/authenticate-user'; //signing
  static const String logoutUrl = '/v1/user/logout';
  static const String sendOtp = '/v1/user/send-otp';
  static const String homeScreenMenuUrl =
      '/v1/user/menus?city_id='; //to fetch offers promo codes
  static const String servicesUrl =
      '/v1/user/service_sub_category/51'; //to fetch services
  static const String historyUrl = '/v2/user/recent_histories?type=';
  static const String walletTransactionUrl =
      '/v1/user/wallet?order_direction=desc&order_by=id';
  static const String notificationUrl =
      '/v1/user/notification?offset=0&limit=100';
  static const String orderEssentialsUrl = '/v1/admin/storetypelist';

  static const String shopListUrl = '/v1/user/store/list/';
  static const String itemListUrl = '/v1/user/store/details/';
  static const String homeLocationUrl = '/v1/admin/store/countries';
  static const String profileUrl = '/v1/user/profile?version=4.9.F';
  static const String saveProfileUrl = '/v1/user/profile?version=4.9.F';
  static const String getAddressUrl = '/v1/user/address';
  static const String addAddressUrl = '/v1/user/address/add';
  static const String editAddressUrl = '/v1/user/address/updates';

  static const String addCartUrl = '/v2/user/store/addcart?';
  //static const String addCartUrl = "/v2/user/laundry/addcart?";
  static const String addLaundryCartUrl = 'v2/user/laundry/addcart?';

  static const String cartListUrl = '/v1/user/store/cartlist';
  static const String laundryCartListUrl = '/v2/user/laundry/cartlist';

  static const String selectPackagesUrl = '/v1/user/packages';
  static const String courierServiceSaveUrl = '/v2/user/homeDeiverySave';
  static const String deliveryOrderInsertUrl =
      '/v2/user/homeDeliveryOrderInsert';
  static const String buyItemsOrderPlace = '/v1/user/store/checkout';
  static const String buyItemsOrderPlaceV2 = '/v2/user/laundry/checkout';

  static const String restaurantUrl = "/v1/user/store/list/6";
  static const String restaurantCuisinesUrl = "/v1/admin/cuisinelist/6";
  static const String autoCompleteLocationUrl =
      '/v1/user/autocomplete?keyword=';
  static const String autoCompleteSaveLocationUrl =
      '/v1/user/autocomplete/save';
  static const String rideVehiclesList = "/v1/user/transport/services?type=1";

  ///user/transport/send/request?
  static const String rideRequest = "/v1/user/transport/send/request?";
  static const String rideCheckRequest = "/v1/user/transport/check/request?";
  static const String selectBid = "/v1/user/transport/selectbid/request";

  // static const String rideVehiclesList = "/user/transport/services";
  static const String orderConfirmationUrl = "/user/homeDeiverySave";
  static const String orderConfirmation2Url = "/user/homeDeliveryCartUpdate";
  static const String walletAddMoney = "/v2/user/add/money";
  static const String vehicleInfoUrl = '/v2/user/homedelivery/send/charges';
  static const String homeDeliverySendOrderDetailUrl =
      '/v2/user/home-delivery-send-order-detail?'; //can pass id
  static const String homeDeliveryBuyOrderDetailUrl =
      '/v2/user/homeDelOrderDetail?'; //can pass id
  static const String cancelOrder = "/v1/user/home-delivery-cancel-order";
  static const String orderCartUpdateUrl = '/v1/user/homeDeliveryCartUpdate';
  static const String checkWalletBalanceUrl = '/v1/user/homeDeiveryUseWallet';
  static const String payWithWalletUrl = '/v1/user/homeDeiveryWalletPayment';
  static const String promoCodes = "/v1/user/promocode/order";
  static const String LandingPagePromoCodes = "/v2/user/promocodes";
  static const String rating = "/v1/user/home-delivery-provider-rating?";
  static const String cartRemove = "/v2/user/store/removecart";
  static const String laundrycartRemove = "/v2/user/laundry/removecart";
  static const String globalSearch = "/v2/user/global_search_data";
  static const String deleteAddress = "/v1/user/address/";
  static const String addOrRemoveFavouriteShop = "/v1/user/store/favourite";
  static const String favouriteShopList = "/v2/user/store/list";
  static const String cancelRide = "/v1/user/transport/cancel/request";
  static const String rideSendRequest = "/v1/user/transport/send/request?";
  static const String rideCancelReasons = "/v1/user/reasons?type=TRANSPORT";
  static const String clearCart = "/v1/user/store/totalremovecart";
  static const String orderDetails = "/v1/user/history/store_orders/";

  //Laundry Api users
  static const String defaultservice = "/v2/user/commondatas";
  static const String storeList = "/v2/user/laundry/shoplist/16";
  static const String serviceitems = "/v2/user/itemsearch/";
  static const String laundryshop = "/v2/user/laundry/homepage/16";
  static const String newAddToCart = "/v2/user/laundry/addcartnew";
  static const String newCartListUrl = '/v2/user/laundry/cartlist';

  static const String countries = '/v1/user/countries';
}
