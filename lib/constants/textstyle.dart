import 'package:app24_user_app/constants/color_coman.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AppTextStyle {
  TextStyle heading = GoogleFonts.monda(
    color: AppColor.white,
    // fontWeight: FontWeight.bold,
    fontSize: 22,
  );
  TextStyle tittlecard = GoogleFonts.recursive(
    color: AppColor.white,
  );
  TextStyle appbathead =
      (GoogleFonts.monda(color: AppColor.white, fontSize: 24));
  TextStyle servicescreenhead = GoogleFonts.monda(
      color: AppColor.servicetext, fontWeight: FontWeight.w600, fontSize: 20);
  TextStyle servicescreentext =
      GoogleFonts.aBeeZee(color: AppColor.servicetext, fontSize: 12);

  TextStyle symbols = GoogleFonts.aBeeZee(
      color: AppColor.servicetext, fontSize: 24, fontWeight: FontWeight.bold);
  TextStyle currencyamount = GoogleFonts.aBeeZee(
      color: AppColor.servicetext, fontSize: 16, fontWeight: FontWeight.bold);
  TextStyle currencytype =
      GoogleFonts.aBeeZee(color: AppColor.servicetext, fontSize: 10);
  TextStyle orderstatushead = GoogleFonts.aBeeZee(
      color: AppColor.servicetext, fontSize: 24, fontWeight: FontWeight.bold);
  TextStyle orderstatustext =
      GoogleFonts.aBeeZee(color: AppColor.servicetext, fontSize: 14);
  TextStyle buttontext =
      GoogleFonts.aBeeZee(color: AppColor.white, fontSize: 14);
}
