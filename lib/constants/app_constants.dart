class AppConstants {
  // locale
  static const String userID = "userID";
  static const String loginStatus = "loginStatus";
  static const String bearerToken = "bearerToken";
  static const String languageCode = "languageCode";
  static const localeEnglish = 'en';
  static const localeTamil = 'ta';
  static const localeHindi = 'hi';
  static const localeKannada = 'kn';
  static const localeTelugu = 'te';
  static const localeMalayalam = 'ml';
}
