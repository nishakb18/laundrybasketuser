class GlobalConstants {
  GlobalConstants._();

  static final String labelPickupLocation = "Pick up Location";
  static final String labelDropLocation = "Drop Location";
  static final String labelGlobalLocation = "Your Location";
  static final String labelFromCart = "Cart";

  static const homeTileFood = "assets/images/food1.png";
  static const COMMON_ROOM_NAME = "joinCommonRoom";
  static const STATUS = "socketStatus";
  static const NEW_REQ = "newRequest";
  static const RIDE_REQ = "rideRequest";
  static const SERVICE_REQ = "serveRequest";
  static const ORDER_REQ = "orderRequest";
  static const TRANSPORT_ROOM_NAME = "joinPrivateRoom";
  static const SERVICE_ROOM_NAME = "joinPrivateRoom";
  static const ORDER_ROOM_NAME = "joinPrivateRoom";
  static const JOIN_ROOM_NAME = "joinPrivateChatRoom";
  static const CHATROOM = "send_message";
  static const ON_MESSAGE_RECEIVE = "new_message";
  static const UPDATE_LOCATION = "updateLocation";
  static const CHECK_ORDER_REQUEST = "checkOrderRequest";

  static const orderStatusOrdered = "ORDERED";
  static const orderStatusCancelled = "CANCELLED";
  static const orderStatusProviderRejected = "PROVIDEREJECTED";
  static const orderStatusSearching = "SEARCHING";
  static const orderStatusProcessing = "PROCESSING";
  static const orderStatusStarted = "STARTED";
  static const orderStatusReached = "REACHED";
  static const orderStatusPickedUP = "PICKEDUP";
  static const orderStatusArrived = "ARRIVED";
  static const orderStatusDelivered = "DELIVERED";
  static const orderStatusPurchaseComplete = "PURCHASECOMPLETE";
  static const orderStatusOrderCompleted = "ORDERCOMPLETED";
}
