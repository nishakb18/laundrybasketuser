import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class AppColor {
  static Color carvebarcolor = HexColor("#02276F");
  static Color locationcontainercolor = HexColor('#0A1D56');
  static Color highlightcontainer = Color(0xFFADDBB2);
  static Color servicetext = HexColor('#02276F');
  static Color servicetext1 = HexColor('#01FF1F');
  static Color white = Colors.white;
  static Color transparent = Colors.transparent;
  static Color dividerColor = Color(0xFF3A0084);
  static Color green = Colors.green;
  static Color iconLightColor = Color(0xFFEFEFEF);
  static Color smokewhite=Color.fromARGB(255, 219, 213, 213);

  static var colorsList = [
    Colors.red,
    Colors.blue,
    Colors.cyan,
    Colors.green,
    Colors.yellow,
    Colors.grey,
    Colors.pinkAccent,
    Colors.amber,
    Colors.tealAccent,
    Colors.purple,
    Colors.brown,
    Colors.deepPurpleAccent,
    Colors.teal,
    Colors.black,
    Colors.teal[100]
  ];
}
