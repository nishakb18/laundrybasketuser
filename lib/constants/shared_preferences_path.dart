import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesPath {
  SharedPreferencesPath._();
  static const accessToken = 'token';
  static const orderType = 'orderType';
  static const userID = 'userID';
  static const cartStoreId = 'cartStoreId';
  static const firebaseTokenKey = "firebase_token";
  static const playStoreVersionKey = "play_store_version";
  static const isUserRegisteredKey = "user_registered";
  static const storeTypeKeywordUpdatedKey = "store_type_keyword_updated";
  static const serviceSubCategoryKeywordUpdatedKey =
      "service_sub_category_keyword_updated";
  static const rideVehicleKeywordUpdatedKey = "ride_vehicle_keyword_updated";
  static const isUserRegistered = 'user_registered';
  static const favouriteRestaurants = 'favourite_restaurants';
  static const lastOrderType = 'lastOrderType';
  static const lastOrderId = 'lastOrderId';
  static const location = 'Loading..';
  static const totalAmountToRazor = '';

  static saveSharedPreferenceList(String key, List<String> value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setStringList(key, value);
  }

  static getSharedPreferenceList(String key) async {
    final prefs = await SharedPreferences.getInstance();
    final value = prefs.getStringList(key);
    return value;
  }
}
