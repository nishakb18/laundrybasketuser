class Apptext {
  static String welcome = "Welcome To,";
  static String appname = "Laundry Basket !";
  static String pick = "Pick your choice..";
  static String our = "Our Highlights";
  static String seeall = "See all";
  static String offers = "% off";
  static String shop = "Shop ";
  static String services = "SERVICES";
  static String cart = "CART";
  static String myWallet = "My Wallet";
  static String editProfile = "Edit Profile";
  static String myAccount = "My Account";
  static String orderDetails = "Order Details";
  static String manageAddress = "Manage Address";
  static String support = "Support";
  static String privacyPolicy = "Privacy Policy";

  static String shoplist = "SHOP LIST";
  static String history = "History";
  static String notifications = "Notifications";

  //service Screen text
  static String wash = "Wash";
  static String washiron = "Wash & Iron";
  static String iron = "Iron";
  static String dryclean = "Dryclean";
  static String itemone = "T-shirt";
  static String itemtwo = "Polo";
  static String itemthree = "Jacket";
  static String itemfour = "Shorts";

  //symbols
  static String multy = "X";

  //shoplist
  static String Shopname1 = "SIMPLECLEAN";
  static String address = "ADDRESS";
  static String distance = "pickup ";

  //checkouScreen text
  static String checkouappbar = "CHECKOUT";
  static String collection = "Collection";
  static String doorstep = "doorstep";
  static String person = "Person";
  static String recepction = " recepction";
  static String collectionaddresshead = "Collection Address";
  static String collectionaddress = "5678 -Street al -United Emirates";
  static String delivery = "Delivery";
  static String deliverydoorstep = " doorstep";

  static String deliveryperson = "Person";
  static String deliveryrecepction = "recepction";
  static String deliveryaddresshead = "Delivery Address";
  static String deliveryaddress = "5678 -Street al -United Emirates";

  static String Paymenthead = "Payment Type";
  static String wallethead = "Pay by Wallet";
  static String cashhead = "Cash on Delivery";
  static String usd = "USD";
  static String ruble = "RUBLE";
  static String euro = "EURO";
  //scheduled Screen
  static String scheduledhead = "SCHEDULED";
  static String confirm = "CONFIRM";
  static String order = "Order Placed";

  //orerlist
  static String orderlisthed = "ORDER LIST";
  static String orderno = "Order No :";
  static String total = "Total";
  static String requst = "REQUEST STATUS";
  //orderstatus
  static String orderstatus = "ORDER STATUS";
  static String orderstatu1 = "ORDER PLACED";
  static String orderstatus1text =
      "Your order is placed sucessfully. Our boy will soon collect the clothes ";
  static String orderstatu2 = "ON THE WAY";
  static String orderstatus2text =
      "Your order is placed sucessfully. Our agent will soon collect the clothes ";
  static String orderstatu3 = "IN PROCESS";
  static String orderstatu3text1 =
      "Please kindly wait. We are still washing your cloths ";
  static String orderstatus3text1 =
      "Please kindly wait. We are still washing your cloths";
  static String estimated = "Estimated time : 1 day";
  static String orderstatu4 = "READY FOR DELIVERY";
  static String orderstatu4text =
      "Our agent will deliver your laundry on 24th April, 2022 @ 4:00PM";
  static String orderstatu4button = "CALL AGENT";
  static String orderstatu5 = "DELIVERED";
  static String orderstatu5text =
      "“Tank you For Choosing Laundry Basket We hope your clothes look and feel great. Your satisfaction is our priority. Have a wonderful day!";
  static String orderstatu5button = "RATE THE SERVICE";

  //shopdetailscreen
  static String swash = "WASH";
  static String swashiron = "WASH&IRON";
  static String siron = "IRON";
  static String sdryclean = "DRYCLEAN";
}
