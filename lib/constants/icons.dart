import 'package:flutter/material.dart';

class AppIcons {
  static const String applogo = "";
  static const IconData backArrow = Icons.arrow_back;
  static const IconData additem= Icons.add;
  static const IconData removeitem= Icons.remove;

}
