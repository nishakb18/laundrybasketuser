import 'package:app24_user_app/config/routes.dart';
import 'package:app24_user_app/constants/route_path.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:package_info_plus/package_info_plus.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      options: const FirebaseOptions(
          apiKey: "AIzaSyD-CUP4RQ40EnABEoa_tkIJZpgsXw52WIc",
          appId: "1:647566901160:android:6b42c7708080980a68279d",
          messagingSenderId: "647566901160",
          projectId: "master-deck-261207"));

  // Get the package info
  PackageInfo packageInfo = await PackageInfo.fromPlatform();


  // Print the version information
  print('Version: ${packageInfo.version}');
  print('Build number: ${packageInfo.buildNumber}');

  runApp(const ProviderScope(
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Laundry Basket',
      debugShowCheckedModeBanner: false,
      navigatorKey: navigatorKey,
      theme: ThemeData(
        primaryColor: const Color(0xff2bd38d),
        primaryColorLight: const Color(0xff90d1fc),
        primaryColorDark: const Color(0xff36506B),
        fontFamily: 'Monda',
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      onGenerateRoute: Routes.generateRoute,
       initialRoute: splashScreen,

     // initialRoute: laundrymain,
    );
  }
}
