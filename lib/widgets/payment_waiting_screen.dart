import 'dart:async';

import 'package:app24_user_app/helpers.dart';
import 'package:app24_user_app/widgets/bottomsheets/order_success_bottomSheet.dart';
import 'package:app24_user_app/widgets/track_order.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:loading_animations/loading_animations.dart';

class PaymentWaiting extends StatefulWidget {
  const PaymentWaiting({Key? key, required this.orderID, required this.orderType}) : super(key: key);
  final int orderID;
  final String orderType;



  @override
  _PaymentWaitingState createState() => _PaymentWaitingState();
}

class _PaymentWaitingState extends State<PaymentWaiting> {
  Timer? _timer;
  int _stop = 30; // time to close the window /// in seconds

  @override
  void initState()  {
    startTimer();
    navigateToTrack();
    super.initState();
  }

  @override
  void dispose() {
    if (_timer != null) {
      _timer!.cancel();
    }
    super.dispose();
  }

  void startTimer() {
    if (_timer != null) {
      _timer!.cancel();
      _timer = null;
    } else {
      _timer = new Timer.periodic(const Duration(seconds: 1), (Timer timer) {
        setState(() {
          if (_stop < 1) {
            _timer!.cancel();
          } else {
            _stop = _stop - 1;
          }
        });
      });
    }
  }

  navigateToTrack()async{
    await Future.delayed(const Duration(seconds: 5), (){
      Helpers().getCommonBottomSheet(context: context, content: OrderSuccess(orderType: widget.orderType,orderID: widget.orderID),/*orderType: "buy",orderID: responseData.id,*/);
    });
      // Navigator.pushReplacement(
      //   context,
      //   MaterialPageRoute(
      //       builder: (context) =>
      //           TrackOrder(
      //             id: widget.orderID.toString(), orderType: widget.orderType,
      //           )),
      // );});
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: null,
      child: SafeArea(
        child: Scaffold(
          body: Container(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  LoadingBouncingGrid.circle(
                    size: 40,
                    backgroundColor: Colors.grey,
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Column(
                      children: [
                        Text(
                          "Please wait, Your payment is processing.",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontWeight: FontWeight.w600),
                        ),
                        Text(
                          "Please don't press back or close the app while payment is processing.",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontWeight: FontWeight.w600),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
