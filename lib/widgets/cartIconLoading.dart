import 'dart:async';

import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class CartIconLoading extends StatefulWidget {
  @override
  _CartIconLoadingState createState() => _CartIconLoadingState();
}

class _CartIconLoadingState extends State<CartIconLoading> {
  Timer? _timer;
  int _start = 10; // time to popup still working window /// in seconds

  @override
  void initState() {
    startTimer();
    super.initState();
  }

  @override
  void dispose() {
    if (_timer != null) {
      _timer!.cancel();
    }
    super.dispose();
  }

  void startTimer() {
    if (_timer != null) {
      _timer!.cancel();
      _timer = null;
    } else {
      _timer = new Timer.periodic(const Duration(seconds: 1), (Timer timer) {
        setState(() {
          if (_start < 1) {
            _timer!.cancel();
          } else {
            _start = _start - 1;
          }
        });
      });
    }
  }

  buildStillLoadingWidget() {
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(15)),
      width: MediaQuery.of(context).size.width,
      child: ListTile(
        title: Text("Please wait..."),
        subtitle: Text(
            "The network seems to be too busy. We suggest you to wait for some more time."),
        leading: CircularProgressIndicator.adaptive(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 10,
        width: 30,
        child: Shimmer.fromColors(
            baseColor: Colors.grey[300]!,
            highlightColor: Colors.grey[100]!,
            child:             Container(
              // width: MediaQuery.of(context).size.width / 6,
              // height: MediaQuery.of(context).size.height / 59,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xffefefef),
                      blurRadius: 20,
                      spreadRadius: 3,
                    ),
                  ]),
            )
          // ListView(
          //     shrinkWrap: true,
          //     children: buildRestaurantItemShimmerItem(context))
        ));
  }
}
