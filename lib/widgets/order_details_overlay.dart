import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/constants/shared_preferences_path.dart';
import 'package:app24_user_app/providers/order_details_provider.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/widgets/track_order.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OrderDetailsOverlay extends StatelessWidget {


/*  String status = '';
  String statusDes = '';*/
  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (context, watch, child) {
      OrderDetailsNotifier detailsNotifier =
          watch(orderDetailsNotifierProvider.notifier);
      final state = watch(orderDetailsNotifierProvider);
      if (state.isLoading ||
          state.isError ||
          !detailsNotifier.onGoingOrderAvailable)
        return Container();
      else {
        return Container(
            decoration: BoxDecoration(color: Color(0xffF5FFF6),
            //     boxShadow: [
            //   // BoxShadow(
            //   //     color: Colors.grey[300]!,
            //   //     spreadRadius: 2.0,
            //   //     blurRadius: 2.0,
            //   //     offset: Offset(1, 0))
            // ]
            ),
            child: ListTile(
              title: Text(
                detailsNotifier.orderStatus,
                style: TextStyle(
                    color: App24Colors.greenOrderEssentialThemeColor,
                    fontWeight: FontWeight.bold),
              ),
              subtitle: Text(detailsNotifier.orderStatusDescription,
                  style: TextStyle(
                    color: App24Colors.greenOrderEssentialThemeColor,
                    fontWeight: FontWeight.w600
                  )),
              trailing: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    backgroundColor: App24Colors.greenOrderEssentialThemeColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),padding: EdgeInsets.symmetric(horizontal: 25,vertical: 10)),
                onPressed: () async {

                  var prefs =
                      await SharedPreferences
                      .getInstance();
                  print("trackid: " + prefs.getInt(SharedPreferencesPath.lastOrderId).toString());
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => TrackOrder(orderType: prefs.getString(SharedPreferencesPath.lastOrderType).toString(),id: prefs.getInt(SharedPreferencesPath.lastOrderId).toString(),)),
                  );
                },
                child: Text("Track",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),),
              ),
              onTap: () {},
            ));
      }
    });
  }
}
