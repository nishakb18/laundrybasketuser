import 'dart:async';
import 'dart:math' show cos, sqrt, asin;
import 'dart:ui';

import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/global_constants.dart';
import 'package:app24_user_app/models/location_model.dart';
import 'package:app24_user_app/modules/ride/ride_vehicle_model.dart';
import 'package:app24_user_app/providers/ride_page_variables_provider.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_animarker/flutter_map_marker_animation.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

final rideVariablesNotifierProvider =
    StateNotifierProvider((ref) => RideVariablesNotifier());

// const kStartPosition = LatLng(10.024783, 76.308127);
// const kSantoDomingo = CameraPosition(target: kStartPosition, zoom: 15);
const kMarkerId = MarkerId('MarkerId1');
const kDuration = Duration(seconds: 2);

class SelectLocationPage extends StatefulWidget {
  final LocationModel? initializedDropLocation;
  final int? providerId;
  final double? dropLat;
  final double? dropLng;
  final double? pickUpLng;
  final double? pickUpLat;
  final String? providerName;

  const SelectLocationPage(
      {Key? key,
      this.initializedDropLocation,
      this.providerId,
      this.dropLat,
      this.dropLng,
      this.pickUpLng,
      this.pickUpLat,
      this.providerName})
      : super(key: key);

  @override
  _SelectLocationPageState createState() => _SelectLocationPageState();
}

class _SelectLocationPageState extends State<SelectLocationPage>
    with TickerProviderStateMixin {
  CameraPosition _initialLocation =
      CameraPosition(target: LatLng(20.5937, 78.9629), zoom: 4.0);
  late GoogleMapController mapController;

  late LocationModel pickUpLocation;
  late LocationModel dropLocation;

  final markers1 = <MarkerId, Marker>{};
  final controller = Completer<GoogleMapController>();
  var stream1;

  static var kLocations = [
    // kStartPosition,
    // LatLng(10.023791, 76.307546),
    // LatLng(10.022955, 76.307001),
    // LatLng(10.022458, 76.306765),
    // LatLng(10.011663, 76.303335)
  ];

  abc() {
    return print(
        "checking latlongs entering list" + kLocations.length.toString());
  }

  double? pickupLat;
  double? pickupLng;
  double? dropLat;
  double? dropLng;
  int i = 0;

  final pickUpAddressController = TextEditingController();
  final dropAddressController = TextEditingController();
  final database = FirebaseDatabase.instance.ref();

  //late RideVariablesNotifier rideVariablesNotifier;

  Set<Marker> markers = {};
  Marker? pickUpMarker;
  Marker? dropMarker;

  PolylinePoints? polylinePoints;
  Map<PolylineId, Polyline>? polyLines = {};
  List<LatLng> polylineCoordinates = [];
  String? providerLat;

  bool _showGoogleMaps = false;
  int selectedVehicleIndex = -1;
  VehicleServices? selectedVehicle;

  bool isPickUpLocationFetching = true;

  String distance = '';

  //String time = '15 mins'; we can get estimated time from polyline package from override function

  double totalDistance = 0.0;

  BitmapDescriptor? pickUpIcon;
  BitmapDescriptor? dropIcon;
  Timer? timer;

  CarouselController vehicleCarouselController = CarouselController();

  @override
  void initState() {
    Future.delayed(const Duration(milliseconds: 500), () {
      setState(() {
        _showGoogleMaps = true;
        initializeDropLocation();
        initializePickUpLocation();
        // selectLocation(GlobalConstants.labelPickupLocation);
        // selectLocation(GlobalConstants.labelDropLocation);
      });
      _activateListeners();
      selectLocation(GlobalConstants.labelDropLocation);
      selectLocation(GlobalConstants.labelPickupLocation);
    });

    timer = Timer.periodic(Duration(seconds: 10),
        (Timer t) => selectLocation(GlobalConstants.labelPickupLocation));
    timer = Timer.periodic(Duration(seconds: 10), (Timer t) {
      print("timer working");
      print(kLocations.toString());

      // stream1.forEach((value) => newLocationUpdate(value));
    });

    //     // kLoc().forEach((value) => newLocationUpdate(value));

//   stream.forEach((value) => newLocationUpdate(value));

    super.initState();
  }




  void newLocationUpdate(LatLng latLng) async{
    pickUpIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(
          devicePixelRatio: 2.5,

        ),
        App24UserAppImages.pickUpIcon);
    var marker = RippleMarker(

      icon: pickUpIcon,
      markerId: kMarkerId,
      position: latLng,
      ripple: false,
      flat: false
    );
    setState(() => markers1[kMarkerId] = marker);
  }

  void setItem(double itemValue) {
    this.pickupLat = itemValue;
    this.itemChanged();
  }

  void itemChanged() {
    print("value changed");
  }

  void _activateListeners() {
    // database.once().then((DataSnapshot snapshot) => print(snapshot.value));
    database.child('Provider/${widget.providerId}').onValue.listen((event) {
      final Object? description = event.snapshot.value;
      setState(() {
        providerLat = description.toString();
      });
    });
  }




  selectLocation(String title) async {
    print("providerId" + widget.providerId.toString());

    DatabaseReference ref = FirebaseDatabase.instance.ref("Provider/${widget.providerId}/l/0");
    DatabaseReference refLng =
        FirebaseDatabase.instance.ref("Provider/${widget.providerId}/l/1");

    DatabaseEvent event = await ref.once();
    DatabaseEvent eventLng = await refLng.once();

    setState(() {
      // if (title == GlobalConstants.labelPickupLocation) {
        // DatabaseReference child = ref.child("0");

        Stream<DatabaseEvent> stream = ref.onValue;
        Stream<DatabaseEvent> streamLng = refLng.onValue;

// Subscribe to the stream!
        stream.listen((DatabaseEvent event) {
          print('Event Type: ${event.type}'); // DatabaseEventType.value;
          print('Snapshot: ${event.snapshot.value}'); // DataSnapshot
        });
        streamLng.listen((DatabaseEvent eventLng) {
          print('Event Type: ${eventLng.type}'); // DatabaseEventType.value;
          print('Snapshot: ${eventLng.snapshot.value}'); // DataSnapshot
        });


        setState(() {
          pickupLat = double.parse(event.snapshot.value.toString());
          pickupLng = double.parse(eventLng.snapshot.value.toString());

          if(kLocations.contains(LatLng(pickupLat!, pickupLng!))){}else {
            kLocations.add(LatLng(pickupLat!, pickupLng!));
            if (kLocations.length > 1) {
              kLocations.removeRange(0, 1);
              print(kLocations);

            }
            print("printing klocation");
            print(kLocations);
            stream1 = Stream.periodic(kDuration, (count) => kLocations[count])
                .take(kLocations.length);
            stream1.forEach((value) => newLocationUpdate(value));
            print(kLocations.length);

            updateMapWithLocations();
            setPickUpMarker();
          }
        });


      //
      // } else {
      //   updateMapWithLocations();
      //   setDropMarker();
      // }
    });


    // }
    // });
  }

  initializeDropLocation() {
    dropLat = /*10.0463102*/ widget.dropLat;
    dropLng = /*76.3173948*/ widget.dropLng;
    // dropLocation = widget.initializedDropLocation!;
    // dropAddressController.text = dropLocation.description!;
    setDropMarker();
  }

  initializePickUpLocation() {
    print("entered initializePickUpLocation");
    getCurrentLocation().then((value) {
      // print(ente)
      //showLog(valyue);
      if (value != null && value.description!.isNotEmpty) {
        LocationModel locationModel = value;
        var lat = widget.pickUpLat;
        var lng = widget.pickUpLng;
        setState(() {
          print("lat inside setstate" + lat.toString());
          pickUpLocation = locationModel;
          pickupLat = lat;
          pickupLng = lng;
          // pickUpAddressController.text = pickUpLocation.description!;
          context.read(homeNotifierProvider.notifier).currentLocation =
              locationModel;
          isPickUpLocationFetching = false;
          setPickUpMarker();
          // loadData1(context: context);
          updateMapWithLocations();
        });
      } else {
        setState(() {
          isPickUpLocationFetching = false;
        });
      }
    });
  }

  updateMapWithLocations() async {
    double? lat = pickupLat;
    double? lng = pickupLng;
    await updateCameraLocation(
        LatLng(lat!, lng!), LatLng(dropLat!, dropLng!), mapController);
    createPolyLines();
  }

  Future<void> setPickUpMarker() async {
    // pickUpIcon = await BitmapDescriptor.fromAssetImage(
    //     ImageConfiguration(
    //       devicePixelRatio: 2.5,
    //     ),
    //     App24UserAppImages.pickUpIcon);

    setState(() {
      print("pickup lat agan" + pickupLat.toString());
      // pickUpMarker = Marker(
      //     markerId: MarkerId('pickup_marker'),
      //     visible: true,
      //     position: LatLng(
      //       pickupLat!,
      //       pickupLng!,
      //     ),
      //     infoWindow: InfoWindow(
      //       title: "Delivery Partner",
      //       snippet: pickUpAddressController.text,
      //     ),
      //     icon: pickUpIcon!);
      if (markers.contains("pickup_marker")) {
        markers.remove("pickup_marker");
      }
      markers.add(pickUpMarker!);
    });
  }

  Future<void> setDropMarker() async {
    dropIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 2.5), App24UserAppImages.dropIcon);

    setState(() {
      dropMarker = Marker(
          markerId: MarkerId('drop_marker'),
          position: LatLng(
            dropLat!,
            dropLng!,
          ),
          infoWindow: InfoWindow(
            title: 'Drop',
            // snippet: dropAddressController.text,
          ),
          icon: dropIcon!);
      if (markers.contains("drop_marker")) {
        markers.remove("drop_marker");
      }
      markers.add(dropMarker!);
    });
  }

  Future<void> updateCameraLocation(
    LatLng source,
    LatLng destination,
    GoogleMapController mapController,
  ) async {
    LatLngBounds bounds;

    if (source.latitude > destination.latitude &&
        source.longitude > destination.longitude) {
      bounds = LatLngBounds(southwest: destination, northeast: source);
    } else if (source.longitude > destination.longitude) {
      bounds = LatLngBounds(
          southwest: LatLng(source.latitude, destination.longitude),
          northeast: LatLng(destination.latitude, source.longitude));
    } else if (source.latitude > destination.latitude) {
      bounds = LatLngBounds(
          southwest: LatLng(destination.latitude, source.longitude),
          northeast: LatLng(source.latitude, destination.longitude));
    } else {
      bounds = LatLngBounds(southwest: source, northeast: destination);
    }

    CameraUpdate cameraUpdate = CameraUpdate.newLatLngBounds(bounds, 150);

    return checkCameraLocation(cameraUpdate, mapController);
  }

  Future<void> checkCameraLocation(
      CameraUpdate cameraUpdate, GoogleMapController mapController) async {
    mapController.animateCamera(cameraUpdate);
    LatLngBounds l1 = await mapController.getVisibleRegion();
    LatLngBounds l2 = await mapController.getVisibleRegion();

    if (l1.southwest.latitude == -90 || l2.southwest.latitude == -90) {
      return checkCameraLocation(cameraUpdate, mapController);
    }
  }

  void createPolyLines() async {
    // Initializing PolylinePoints
    polylinePoints = PolylinePoints();

    // Generating the list of coordinates to be used for
    // drawing the polyLines
    late PolylineResult result;
    late String mapKey;

    if (context.read(homeNotifierProvider.notifier).appConfiguration == null) {
      await context.read(homeNotifierProvider.notifier).getAppConfigurations();
    }

    mapKey = context.read(homeNotifierProvider.notifier).mapKey!;

    showLog("key is : $mapKey");

    result = await polylinePoints!.getRouteBetweenCoordinates(
      mapKey, // Google Maps API Key
      PointLatLng(widget.pickUpLat!, widget.pickUpLng!),
      PointLatLng(dropLat!, dropLng!),
    );

    // Adding the coordinates to the list
    if (result.points.isNotEmpty) {
      polylineCoordinates.clear();
      result.points.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
      getDistance();
    }

    // Defining an ID
    PolylineId id = PolylineId('poly');

    // Initializing Polyline
    Polyline polyline = Polyline(
      polylineId: id,
      color: Colors.black87,
      points: polylineCoordinates,
      width: 3,
    );

    // Adding the polyline to the map
    setState(() {
      polyLines!.clear();
      polyLines![id] = polyline;
    });
  }

  double _coordinateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return 12742 * asin(sqrt(a));
  }

  void getDistance() {
    totalDistance = 0.0;
    for (int i = 0; i < polylineCoordinates.length - 1; i++) {
      totalDistance += _coordinateDistance(

        polylineCoordinates[i].latitude,
        polylineCoordinates[i].longitude,
        polylineCoordinates[i + 1].latitude,
        polylineCoordinates[i + 1].longitude,
      );
    }
    showLog(totalDistance.toStringAsFixed(2) + "calculated distance");
    setState(() {
      distance = totalDistance.toStringAsFixed(2) + " Kms";
    });
  }

  clearing() {
    Navigator.pop(context);

    return kLocations.clear();
  }
  @override
  void dispose() {
    timer!.cancel();
    
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return clearing();
      },
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          body: Consumer(builder: (context, watch, child) {
            //rideVariablesNotifier = watch(rideVariablesNotifierProvider.notifier);
            return SafeArea(
              child: Column(
                children: [
                  Expanded(
                    child: Stack(
                      children: <Widget>[
                        _showGoogleMaps
                            ? Container(
                                child: Animarker(
                                  useRotation: false,
                                duration: Duration(seconds: 45),
                                curve: Curves.ease,
                                mapId: controller.future
                                    .then<int>((value) => value.mapId),
                                //Grab Google Map Id
                                markers: markers1.values.toSet(),
                                child: GoogleMap(
                                  markers: Set<Marker>.from(markers),
                                  initialCameraPosition: _initialLocation,
                                  myLocationEnabled: true,
                                  myLocationButtonEnabled: false,
                                  mapType: MapType.normal,
                                  zoomGesturesEnabled: true,
                                  zoomControlsEnabled: false,
                                  polylines:
                                      Set<Polyline>.of(polyLines!.values),
                                  onMapCreated: (
                                    GoogleMapController gController,
                                  ) {
                                    mapController = gController;
                                    controller.complete(gController);
                                    //_getCurrentLocation();
                                  },
                                ),
                              ))
                            : Center(
                                child: CupertinoActivityIndicator(
                                  radius: 20,
                                ),
                              ),
                        Container(
                          margin: EdgeInsets.only(top: 20, left: 20, right: 20),
                          child: FloatingActionButton(
                            onPressed: () {
                              Navigator.pop(context);
                              kLocations.clear();
                            },
                            child: Icon(Icons.arrow_back),
                            mini: true,
                            backgroundColor:
                                Theme.of(context).colorScheme.secondary,
                          ),
                        ),
                        Positioned(
                            bottom: 10,
                            right: 10,
                            child: distance == ""
                                ? Container()
                                : Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(7),
                                        color: Theme.of(context)
                                            .colorScheme
                                            .secondary,
                                        boxShadow: [
                                          BoxShadow(
                                              color:
                                                  Colors.grey.withOpacity(0.4),
                                              blurRadius: 1,
                                              spreadRadius: 1,
                                              offset: Offset(1, 1))
                                        ]),
                                    padding: EdgeInsets.symmetric(
                                        vertical: 10, horizontal: 15),
                                    child: Text(
                                      "${distance} away",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500),
                                    ),
                                  )),
                      ],
                    ),
                  ),
                ],
              ),
            );
          }) // This trailing comma makes auto-formatting nicer for build methods.
          ),
    );
  }
}
