import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/constants/shared_preferences_path.dart';
import 'package:app24_user_app/modules/delivery/cart/cart.dart';
import 'package:app24_user_app/modules/delivery/cart/cart_count_notifier.dart';
import 'package:app24_user_app/providers/order_details_provider.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:app24_user_app/widgets/track_order.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ViewCartOverlay extends StatefulWidget {
  const ViewCartOverlay({super.key});


  @override
  State<ViewCartOverlay> createState() => _ViewCartOverlayState();
}

class _ViewCartOverlayState extends State<ViewCartOverlay> {
  cartIcon({required BuildContext context, fromPage}) {
    // var cartCount ;


    loadData({bool init = true, required BuildContext context}){

    }

    return
      Consumer(builder: (context, watch, child) {
        final state = watch(cartCountNotifierProvider);

        if (state.isLoading) {
          return Container(height: 0,);
        } else if (state.isError) {
          return LoadingError(
            onPressed: (res) {
              loadData(init: true, context: res);
            },
            message: state.errorMessage.toString(),
          );
        } else {print("state.response of overlay" + state.response.toString());

          if(state.response == "null")
            return Container();

          //  cartCount = state.response;
          //shopName = state.response;
          if(state.response == 0)
            return Container(
                padding: EdgeInsets.only(left: 15),
                decoration: BoxDecoration(color: Color(0xffF5FFF6),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.grey[300]!,
                          spreadRadius: 2.0,
                          blurRadius: 2.0,
                          offset: Offset(1, 0))
                    ]
                ),
                child: ListTile(
                  title: Text("Current location is too far!",style: TextStyle(fontWeight: FontWeight.w600,color: App24Colors.greenOrderEssentialThemeColor),),
                  subtitle:  Text("Your cart contains items from a shop, too far from your current location.",
                    style: TextStyle(color: Colors.black,fontWeight: FontWeight.w500,fontSize: MediaQuery.of(context).size.width/35),),
                  trailing: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        backgroundColor: App24Colors.greenOrderEssentialThemeColor,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(25)),
                        padding: EdgeInsets.symmetric(
                            horizontal: 25, vertical: 10)),
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Cart()));
                    },
                    child: Text("View Cart", style: TextStyle(
                        fontSize: 15, fontWeight: FontWeight.bold),),
                  ),
                  onTap: () {},
                ));
          else
          {
            print("else entering");
            List cartInfo = state.response;
            print("printing cartinfo" + cartInfo.toString());
            if(cartInfo[1] == 0) {
              print("cartcount");
              return Container();
            }else {

              print("printing else condition");
              return
                Container(
                    decoration: BoxDecoration(color: Color(0xffF5FFF6),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.grey[300]!,
                              spreadRadius: 2.0,
                              blurRadius: 2.0,
                              offset: Offset(1, 0))
                        ]
                    ),
                    child: ListTile(
                      title: Text(cartInfo[0].toString(),style: TextStyle(fontWeight: FontWeight.w600,color: App24Colors.greenOrderEssentialThemeColor),),
                      subtitle:  Text("You have ${cartInfo[1]} item(s) saved in your cart.",
                        style: TextStyle(color: Colors.black,fontWeight: FontWeight.w500,fontSize: MediaQuery.of(context).size.width/35),),
                      trailing: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor: App24Colors.greenOrderEssentialThemeColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(25)),
                            padding: EdgeInsets.symmetric(
                                horizontal: 25, vertical: 15)),
                        onPressed: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) => Cart()));
                        },
                        child: Text("View Cart", style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.bold),),
                      ),
                      onTap: () {},
                    ));
            }
          }
        }});
  }



  @override
  Widget build(BuildContext context) {
    return cartIcon(context: context);
  }
}