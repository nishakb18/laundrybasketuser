import 'package:app24_user_app/app24_user_icons.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/constants/global_constants.dart';
import 'package:app24_user_app/models/location_model.dart';
import 'package:app24_user_app/modules/delivery/cart/cart.dart';
import 'package:app24_user_app/modules/delivery/cart/cart_model.dart';
import 'package:app24_user_app/modules/restaurant/restaurant_cart_page.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/widgets/cartIconLoading.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class CustomAppBar extends StatefulWidget implements PreferredSizeWidget {
  CustomAppBar({Key? key,
    required this.title,
    this.actions,
    this.backgroundColor = Colors.white,
    this.onBackPressed, this.fromRestaurant, this.hideCartIcon})
      : preferredSize = Size.fromHeight(kToolbarHeight),
        super(key: key);

  @override
  final Size preferredSize; // default is 56.0

  final String title;
  final Object? actions;
  final Color backgroundColor;
  final VoidCallback? onBackPressed;
  final String? fromRestaurant;
  final String? hideCartIcon;

  @override
  _CustomAppBarState createState() => _CustomAppBarState();
}

class _CustomAppBarState extends State<CustomAppBar> {

  loadData({bool init = true, required BuildContext context}){

  }

  selectLocation(String title) {
    showSelectLocationSheet(context: context, title: title).then((value) {
      if (value != null) {
        LocationModel model = value;
        setState(() {
          context.read(homeNotifierProvider.notifier).currentLocation = model;
          context.read(cartNotifierProvider.notifier).getCartList(
              fromLat: context
                  .read(homeNotifierProvider.notifier)
                  .currentLocation
                  .latitude!
                  .toDouble(),
              fromLng: context
                  .read(homeNotifierProvider.notifier)
                  .currentLocation
                  .longitude!
                  .toDouble(),
              context: context);
          // loadData(context: context);
        });
      }
    });
  }


  // cartIcon({required BuildContext context, fromPage}) {
  //   var cartCount ;
  //
  //
  //
  //   return
  //     Consumer(builder: (context, watch, child) {
  //       final state = watch(cartCountNotifierProvider);
  //
  //       if (state.isLoading) {
  //         return CartIconLoading();
  //       } else if (state.isError) {
  //         return LoadingError(
  //           onPressed: (res) {
  //             loadData(init: true, context: res);
  //           },
  //           message: state.errorMessage.toString(),
  //         );
  //       } else {
  //
  //         cartCount = state.response;
  //
  //         return
  //           Stack(children: [
  //             InkWell(
  //                 borderRadius: BorderRadius.circular(15),
  //                 onTap: () {
  //                  // if(context.read(cartCountNotifierProvider.notifier).locationError==false)
  //                   //{
  //                     cartCount <= 0 ? showMessage(
  //                         context, "Cart is empty, please add any items.", true,
  //                         false) :
  //                     // fromPage == "fromRestaurant" ? Navigator.push(context,
  //                     //     MaterialPageRoute(
  //                     //         builder: (context) => RestaurantCartPage())) :
  //                     Navigator.push(context,
  //                         MaterialPageRoute(
  //                             builder: (context) => Cart() /*TrackOrder()*/));
  //                 /*  }else{
  //                     Center(
  //                       child: Column(
  //                         mainAxisAlignment: MainAxisAlignment.center,
  //                         crossAxisAlignment: CrossAxisAlignment.stretch,
  //                         children: [
  //                           Center(
  //                             child: Column(
  //                               children: [
  //                                 const Text(
  //                                   "Drop Location too far",
  //                                   style: TextStyle(fontWeight: FontWeight.w800),
  //                                 ),
  //                                 const SizedBox(
  //                                   height: 10,
  //                                 ),
  //                                 const Text(
  //                                   "Please select a nearby drop point",
  //                                   style: TextStyle(fontWeight: FontWeight.w600),
  //                                 ),
  //                               ],
  //                             ),
  //                           ),
  //                           const SizedBox(
  //                             height: 10,
  //                           ),
  //                           new MaterialButton(
  //                             padding: EdgeInsets.symmetric(vertical: 15),
  //                             color: App24Colors.redRestaurantThemeColor,
  //                             shape: RoundedRectangleBorder(
  //                                 borderRadius: BorderRadius.circular(15)),
  //                             child: const Text(
  //                               "Change location",
  //                               style: TextStyle(color: Colors.white),
  //                             ),
  //                             onPressed: () {
  //                               // Navigator.pop(context);
  //                               selectLocation(GlobalConstants.labelDropLocation);
  //
  //                               // showMessage(
  //                               //     context, "Please change your location", true, false);
  //                             }
  //                             // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomePage()));}
  //                             *//*  Navigator.push(context,
  //                             MaterialPageRoute(builder: (context) => LogInPage()))*//*
  //                             ,
  //                           ),
  //                           const SizedBox(
  //                             height: 10,
  //                           ),
  //                           new MaterialButton(
  //                             padding: EdgeInsets.symmetric(vertical: 15),
  //                             color: App24Colors.redRestaurantThemeColor,
  //                             shape: RoundedRectangleBorder(
  //                                 borderRadius: BorderRadius.circular(15)),
  //                             child: const Text(
  //                               "Clear cart",
  //                               style: TextStyle(color: Colors.white),
  //                             ),
  //                             onPressed: () {
  //                               // Navigator.pop(context);
  //                               context
  //                                   .read(cartNotifierProvider.notifier)
  //                                   .getClearCart(context: context);
  //                             }
  //                             *//*  Navigator.push(context,
  //                             MaterialPageRoute(builder: (context) => LogInPage()))*//*
  //                             ,
  //                           )
  //                         ],
  //                       ),
  //                     );
  //                     // showDistanceExceededPopUp(context:context);
  //                   }*/
  //
  //                 },
  //                 child: Padding(
  //                     padding:
  //                     const EdgeInsets.only(
  //                         right: 15, left: 8, top: 15, bottom: 10),
  //                     child: Icon(
  //                       CupertinoIcons.cart,
  //                       // App24User.group_636_3x,
  //                       color: App24Colors.darkTextColor,
  //                       size: 22,
  //                     ))),
  //             if(cartCount != 0||cartCount != null )
  //               Positioned(
  //                   top: 10,
  //                   left: 24,
  //                   child: Container(
  //                     width: 15,
  //                     height: 15,
  //                     decoration: BoxDecoration(
  //                       color: cartCount == 0 || cartCount==null ?Colors.transparent:App24Colors.greenOrderEssentialThemeColor,
  //                       borderRadius: BorderRadius.circular(50),
  //                     ),
  //                     child: Center(
  //                         child: Text(
  //
  //                           cartCount == 0 || cartCount==null ? "" :
  //                           cartCount.toString(),
  //                           style: TextStyle(
  //                               color: Colors.white,
  //                               fontWeight: FontWeight.bold,
  //                               fontSize: 10),
  //                         )),
  //                   ))
  //           ]);
  //       }
  //     });
  // }


  @override
  void initState() {
    SchedulerBinding.instance!.addPostFrameCallback((_) {
      if (context
          .read(cartCountNotifierProvider.notifier)
          .state
          .response == null)
        context.read(cartCountNotifierProvider.notifier).getCartCount(context: context);
    });
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return AppBar(
      toolbarHeight: 56,
      elevation: 1.0,
      backgroundColor: Colors.white,
      title: Text(
        widget.title,
        style: TextStyle(
            color: App24Colors.darkTextColor,
            fontSize: MediaQuery
                .of(context)
                .size
                .width / 25,
            fontWeight: FontWeight.bold),
      ),
      leading: Material(
        color: Colors.white,
        child: InkWell(
          onTap: widget.onBackPressed != null
              ? widget.onBackPressed
              : () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back_ios,
            color: App24Colors.darkTextColor,
            size: MediaQuery
                .of(context)
                .size
                .width / 25,
          ),
        ),
      ),

      // actions: [
      //   if(widget.hideCartIcon != "true")
      //     Material(
      //       color: Colors.transparent,
      //       child: cartIcon(context:context,fromPage: widget.fromRestaurant),
      //     ),
      //   const SizedBox(
      //     width: 20,
      //   )
      // ],
    );
  }
}

// cartIcon({required BuildContext context, fromPage}) {
//   var cartCount ;
//
//   loadData({bool init = true, required BuildContext context}){
//
//   }
//
//   return
//     Consumer(builder: (context, watch, child) {
//       final state = watch(cartCountNotifierProvider);
//
//       if (state.isLoading) {
//         return CartIconLoading();
//       } else if (state.isError) {
//         return LoadingError(
//           onPressed: (res) {
//             loadData(init: true, context: res);
//           },
//           message: state.errorMessage.toString(),
//         );
//       } else {
//         cartCount = state.response;
//
//         return
//           Stack(children: [
//             InkWell(
//                 borderRadius: BorderRadius.circular(15),
//                 onTap: () {
//                  // if(context.read(cartCountNotifierProvider.notifier).locationError==false) {
//                     cartCount <= 0 ? showMessage(
//                         context, "Cart is empty, please add any items.", true,
//                         false) :
//                     // fromPage == "fromRestaurant" ? Navigator.push(context,
//                     //     MaterialPageRoute(
//                     //         builder: (context) => RestaurantCartPage())) :
//                     Navigator.push(context,
//                         MaterialPageRoute(
//                             builder: (context) => Cart() /*TrackOrder()*/));
//                   /*}else{
//                     Center(
//                       child: Column(
//                         mainAxisAlignment: MainAxisAlignment.center,
//                         crossAxisAlignment: CrossAxisAlignment.stretch,
//                         children: [
//                           Center(
//                             child: Column(
//                               children: [
//                                 const Text(
//                                   "Drop Location too far",
//                                   style: TextStyle(fontWeight: FontWeight.w800),
//                                 ),
//                                 const SizedBox(
//                                   height: 10,
//                                 ),
//                                 const Text(
//                                   "Please select a nearby drop point",
//                                   style: TextStyle(fontWeight: FontWeight.w600),
//                                 ),
//                               ],
//                             ),
//                           ),
//                           const SizedBox(
//                             height: 10,
//                           ),
//                           new MaterialButton(
//                             padding: EdgeInsets.symmetric(vertical: 15),
//                             color: App24Colors.redRestaurantThemeColor,
//                             shape: RoundedRectangleBorder(
//                                 borderRadius: BorderRadius.circular(15)),
//                             child: const Text(
//                               "Change location",
//                               style: TextStyle(color: Colors.white),
//                             ),
//                             onPressed: () {
//                               // Navigator.pop(context);
//                               // selectLocation(GlobalConstants.labelDropLocation);
//
//                               // showMessage(
//                               //     context, "Please change your location", true, false);
//                             }
//                             // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomePage()));}
//                             *//*  Navigator.push(context,
//                               MaterialPageRoute(builder: (context) => LogInPage()))*//*
//                             ,
//                           ),
//                           const SizedBox(
//                             height: 10,
//                           ),
//                           new MaterialButton(
//                             padding: EdgeInsets.symmetric(vertical: 15),
//                             color: App24Colors.redRestaurantThemeColor,
//                             shape: RoundedRectangleBorder(
//                                 borderRadius: BorderRadius.circular(15)),
//                             child: const Text(
//                               "Clear cart",
//                               style: TextStyle(color: Colors.white),
//                             ),
//                             onPressed: () {
//                               // Navigator.pop(context);
//                               context
//                                   .read(cartNotifierProvider.notifier)
//                                   .getClearCart(context: context);
//                             }
//                             *//*  Navigator.push(context,
//                               MaterialPageRoute(builder: (context) => LogInPage()))*//*
//                             ,
//                           )
//                         ],
//                       ),
//                     );
//                     // showDistanceExceededPopUp(context:context);
//                   }*/
//
//                 },
//                 child: Padding(
//                     padding:
//                     const EdgeInsets.only(
//                         right: 15, left: 8, top: 15, bottom: 10),
//                     child: Icon(
//                       CupertinoIcons.cart,
//                       // App24User.group_636_3x,
//                       color: App24Colors.darkTextColor,
//                       size: 22,
//                     ))),
//             if(cartCount != 0||cartCount != null )
//               Positioned(
//                   top: 10,
//                   left: 24,
//                   child: Container(
//                     width: 15,
//                     height: 15,
//                     decoration: BoxDecoration(
//                       color: cartCount == 0 || cartCount==null ?Colors.transparent:App24Colors.greenOrderEssentialThemeColor,
//                       borderRadius: BorderRadius.circular(50),
//                     ),
//                     child: Center(
//                         child: Text(
//
//                           cartCount == 0 || cartCount==null ? "" :
//                           cartCount.toString(),
//                           style: TextStyle(
//                               color: Colors.white,
//                               fontWeight: FontWeight.bold,
//                               fontSize: 10),
//                         )),
//                   ))
//           ]);
//       }
//     });
// }
