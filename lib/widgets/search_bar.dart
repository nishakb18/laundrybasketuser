import 'package:app24_user_app/modules/tabs/home/global_search/home_search.dart';
import 'package:flutter/material.dart';

class SearchBarDes extends StatefulWidget implements PreferredSizeWidget {
  SearchBarDes({Key? key, required this.title,required this.bgColor,this.iconColor,this.textColor})
      : preferredSize = Size.fromHeight(kToolbarHeight),
        super(key: key);

  @override
  final Size preferredSize; // default is 56.0
  final String title;
  final Color bgColor;
  final Color? iconColor;
  final Color? textColor;


  _SearchBarDesState createState() => _SearchBarDesState();
}

class _SearchBarDesState extends State<SearchBarDes> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20.0),
          color: widget.bgColor,
          // border: Border.all(color: Colors.grey[200]!)
      ),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => HomeSearchBar(),
                  ));
            },
            borderRadius: BorderRadius.circular(14.0),
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 17, horizontal: 15),
              child: Row(
                children: [
                  Icon(
                    Icons.search,
                    color: widget.iconColor,
                    size: MediaQuery.of(context).size.width / 18,
                  ),
                  const SizedBox(
                    width: 4.0,
                  ),
                  Expanded(
                      child: Text(
                        widget.title,
                        style: TextStyle(
                      color: widget.textColor,
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                    ),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ))
                ],
              ),
            )),
      ),
    );
  }
}
