import 'package:flutter/material.dart';

class PaginationBuilder extends StatefulWidget {
  const PaginationBuilder(
      {Key? key,
      this.title,
      this.builder,
      this.onNextPageRequested,
      this.limit = 20,
      this.listLength})
      : super(key: key);
  final String? title;
  final Function(BuildContext, ScrollController)? builder;
  final Function(int)? onNextPageRequested;
  final int? listLength;
  final int limit;

  @override
  _PaginationBuilderState createState() => _PaginationBuilderState();
}

class _PaginationBuilderState extends State<PaginationBuilder> {
  final _scrollController = ScrollController();
  final _scrollThreshold = 200.0;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_onScroll);
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (maxScroll - currentScroll <= _scrollThreshold) {

      final requestMoreData = widget.listLength! % widget.limit == 0;
      final pageToRequest = (widget.listLength! ~/ widget.limit) + 1;

      if (requestMoreData) {
        widget.onNextPageRequested?.call(pageToRequest);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return widget.builder!(context, _scrollController);
  }
}
