import 'package:app24_user_app/Laundry-app/views/ServicesScreens/services_home_screen.dart';
import 'package:app24_user_app/Laundry-app/views/widgets/custum_appbar.dart';
import 'package:app24_user_app/app24_user_icons.dart';
import 'package:app24_user_app/constants/api_path.dart';
import 'package:app24_user_app/constants/app_text.dart';
import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/constants/textstyle.dart';
import 'package:app24_user_app/helpers.dart';
import 'package:app24_user_app/models/home_delivery_send_order_detail.dart';
import 'package:app24_user_app/models/location_model.dart';
import 'package:app24_user_app/modules/home.dart';
import 'package:app24_user_app/modules/restaurant/restaurant_cart_page.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/widgets/bottomsheets/cancel_order/cancel_order_bottomsheet.dart';
import 'package:app24_user_app/widgets/bottomsheets/order_success_bottomSheet.dart';
import 'package:app24_user_app/widgets/bottomsheets/rating/rate_delivery.dart';
import 'package:app24_user_app/widgets/bottomsheets/retry_cancel_bottomSheet.dart';
import 'package:app24_user_app/widgets/common_appbar.dart';
import 'package:app24_user_app/widgets/common_main_button.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:app24_user_app/widgets/select_location.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'bottomsheets/contact_delivery_boy_bottomsheet.dart';

class TrackOrder extends StatefulWidget {
  final String? id;
  final String orderType;
  final bool? isLaundryApp;

  const TrackOrder({super.key, this.id, required this.orderType, this.isLaundryApp = false});

  @override
  _TrackOrderState createState() => _TrackOrderState();
}

class _TrackOrderState extends State<TrackOrder> {
  HomeDeliveryOrderDetailResponseData? detailResponseData;
  final database = FirebaseDatabase.instance.ref();

  @override
  void initState() {
    SchedulerBinding.instance.addPostFrameCallback((_) {
      // _openLoadingDialog(context);
      loadData(context: context);
      // context.read(cartNotifierProvider.notifier).getCartList(
      //     context: context,
      //     lat: context
      //         .read(homeNotifierProvider.notifier)
      //         .currentLocation
      //         .latitude!
      //         .toDouble(),
      //     lng: context
      //         .read(homeNotifierProvider.notifier)
      //         .currentLocation
      //         .longitude!
      //         .toDouble());
      // context
      //     .read(cartCountNotifierProvider.notifier)
      //     .getCartCount(context: context);
    });
    context.read(cartNotifierProvider.notifier);
    super.initState();
  }

  loadData({BuildContext? context}) {
    return context!.read(orderDetailsNotifierProvider.notifier).getOrderDetails(
      isInitialLoad: true,
        id: widget.id, orderType: widget.orderType);
  }

  // void _openLoadingDialog(BuildContext context) {
  //   showDialog(
  //     barrierDismissible: false,
  //     context: context,
  //     builder: (BuildContext context) {
  //       return AlertDialog(
  //         content: CupertinoActivityIndicator(),
  //       );
  //     },
  //   );
  // }

  Widget buildStatus() {
    if (detailResponseData!.status == 'SEARCHING') {
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [Icon(Icons.timelapse), Text("Searching for provider")],
      );
    } else if (detailResponseData!.status == "CANCELLED") {
      return Container();
    } else {
      return buildProviderDetails();
    }
  }

  //  rateDelivery() {
  //
  //    Future.delayed(Duration.zero, () async {
  //      if (detailResponseData!.status == 'ORDERCOMPLETED') {
  //          return Helpers().getCommonBottomSheet(
  //                context: context,
  //                content: RateDeliveryBoy(
  //                  shopName: detailResponseData!.storeName,
  //                  deliveryBoyName:
  //                 detailResponseData!.providerName,
  //                  orderId: detailResponseData!.id!.toInt(),
  //                ),
  //                title: "Your opinion matters to us");
  //          }else{
  //            return Container();
  //          }
  //    });
  //
  // }

  Widget buildProviderDetails() {
    return MaterialButton(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      onPressed: () {
        Helpers().getCommonBottomSheet(
            context: context,
            content: ContactDeliveryBottomSheet(
              deliveryBoyName: detailResponseData!.providerName,
              deliveryBoyNumber: detailResponseData!.providerNumber,
            ),
            title: "Contact");
      },
      child: Row(
        children: [
          Image.asset(
            App24UserAppImages.riderAgentTrackPage,
            width: 70,
          ),
          const SizedBox(
            width: 20,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  "Contact Delivery Agent",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                  overflow: TextOverflow.clip,
                ),
                if (detailResponseData!.providerName!.isNotEmpty)
                  Text(
                    "${detailResponseData!.providerName} (${detailResponseData!.providerNumber})",
                    overflow: TextOverflow.clip,
                  ),
              ],
            ),
          ),
          const SizedBox(
            width: 20,
          ),
          Icon(
            App24User.right_circle,
            color: Color(0xff1ABE7A),
            size: 18,
          )
        ],
      ),
    );
  }

  navigateToMap(
      {LocationModel? location,
      bool isCurrentLocation = false,
      HomeDeliveryOrderDetailResponseData? detailResponseData}) {
    // if (!widget.initialCall!) {
    //   Navigator.pop(context, isCurrentLocation ? 'current_location' : location);
    // } else {
    print(detailResponseData!.providerId);
    print(detailResponseData.id);
    print(detailResponseData.pickUpLng);
    print(detailResponseData.pickUpLat);
    print(detailResponseData.deliveryLng);
    print(detailResponseData.deliveryLat);
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => SelectLocationPage(
                providerName: detailResponseData.providerName,
                providerId: detailResponseData.providerId,
                dropLat: detailResponseData.deliveryLat,
                dropLng: detailResponseData.deliveryLng,
                pickUpLat: detailResponseData.pickUpLat,
                pickUpLng: detailResponseData.pickUpLng,
                // isSavedAddress: false,
                //     locationAutoCompleteModel: location,
                //     title: "Track Order",
                //     isCurrentLocation: isCurrentLocation,
              )),
    );
    // }
  }

  List<Widget> buildOrderStatus(i) {
    List<Widget> list = [];
    for (var i = 0;
        i < detailResponseData!.orderDetails!.aStatus!.length;
        i++) {
      bool lineVisible = false;
      if (i != detailResponseData!.orderDetails!.aStatus!.length - 1) {
        if (detailResponseData!.orderDetails!.aStatus![i].status ==
                "Completed" &&
            detailResponseData!.orderDetails!.aStatus![i + 1].status ==
                "Completed") {
          lineVisible = true;
        } else {
          lineVisible = false;
        }
      }
      list.add(statusWidget(
          statusText: detailResponseData!.orderDetails!.aStatus![i].statusText!,
          isCompleted: detailResponseData!.orderDetails!.aStatus![i].status ==
                  "Completed"
              ? true
              : false,
          isLineVisible: lineVisible));
    }
    /*detailResponseData.orderDetails!.aStatus!.forEach((element) {
      list.add(statusWidget(
          statusText: element.statusText!,
          isCompleted: element.status == "Completed" ? true : false));
    });*/
    return list;
  }

  orderDetail({String? title, String? value, Color? success}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          title!,
          style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
        ),
        Text(
          value!,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w600,
            color: success == null ? App24Colors.lightBlue : success,
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () {
          return Navigator.of(context)
              .pushAndRemoveUntil(
                  MaterialPageRoute(builder: (context) => HomePage()),
                  (Route<dynamic> route) => false)
              .then((value) => value as bool);
        },
        child: Scaffold(
          appBar: (widget.isLaundryApp == true)?
          CusturmAppbar(
            child: ListTile(
              leading: backarrow(context: context,
                onTap: () {
                  Navigator.pop(context);
                },),
              title: Center(
                child: Text(Apptext.orderDetails, style: AppTextStyle().appbathead),
              ),
            ),
          )
              :
          CustomAppBar(
            hideCartIcon: "true",
            title: "Order Details",
            onBackPressed: () {
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(builder: (context) => HomePage()),
                  (Route<dynamic> route) => false);
            },
          ),
          body: RefreshIndicator(
            onRefresh: () {
              return loadData(context: context) ?? false as Future<void>;
            },
            child: Consumer(builder: (context, watch, child) {
              var state = watch(orderDetailsNotifierProvider);
              if (state.isLoading) {
                return Center(
                    child: CupertinoActivityIndicator(
                  radius: 15,
                ));
              } else if (state.isError) {
                return LoadingError(
                  onPressed: (res) {
                    loadData(context: res);
                  },
                  message: state.errorMessage,
                );
              }
              else {
                //String? orderDetails = detailResponseData.homeDelCatDetails!.single.catValue.toString();
                //  final split = orderDetails.split("\n\n");
                // homeDelCatDetails = state.response.responseData[0];
                // packageDetail= state.response.responseData;

                detailResponseData = state.response.responseData[0];
                //   if(detailResponseData!.length > 0 )
                int i = 0;
                return SingleChildScrollView(
                  child: Container(
                    color: Colors.white,
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Image.asset(App24UserAppImages.trackOrderBG),
                          const SizedBox(
                            height: 20,
                          ),
                          // for(var i = 0;i>1;)

                          Text(
                            detailResponseData!.packageDetail != []
                                ? "Your Order"
                                : detailResponseData!
                                        .packageDetail?[0].itemName ??
                                    '',
                            // packageDetail[i].itemName!,
                            // "Your order from Hai Fresh",
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                fontSize: 19, fontWeight: FontWeight.bold),
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          // if (detailResponseData!.deliveryCharge != 0)
                          if (detailResponseData!.weight == 0 &&
                              detailResponseData!.deliveryCharge != 0)
                            orderDetail(
                              title: "Item Total",
                              value:
                                  ("${detailResponseData?.currencySymbol ?? "AED"}" + /*(double.parse(detailResponseData
                                .deliveryCharge
                                .toString()
                                .replaceAll(",", "")) +
                                double.parse(detailResponseData
                                    .taxAmount
                                    .toString()
                                    .replaceAll(",", ""))) +*/
                                      (double.parse(detailResponseData!
                                              .itemtotal
                                              .toString()
                                              .replaceAll(",", "")))
                                          .toStringAsFixed(2)),
                            ),

                          /*   const SizedBox(
                            height: 20,
                          ),*/
                          Visibility(
                              visible: detailResponseData!.weight != 0 &&
                                  detailResponseData!.deliveryCharge != 0,
                              child: orderDetail(
                                  title: "Total Package Weight",
                                  value: detailResponseData!.weight.toString() +
                                      " Kg")),
                          const SizedBox(
                            height: 5,
                          ),
                          if (detailResponseData!.weight == 0 &&
                              detailResponseData!.deliveryCharge != 0)
                            Visibility(
                                visible: detailResponseData!.weight == 0 &&
                                    detailResponseData!.deliveryCharge != 0,
                                child: orderDetail(
                                    title: "Discount",
                                    value: "${detailResponseData?.currencySymbol ?? "AED"}" +
                                        double.parse(detailResponseData!
                                                .discount
                                                .toString())
                                            .toStringAsFixed(2))),
                          // if(detailResponseData!.deliveryMode != "Four Wheeler")
                          const SizedBox(
                            height: 5,
                          ),
                          if (detailResponseData!.deliveryCharge == 0)
                            Center(
                                child: Text(
                                    "(Bill will be updated after purchase)")),
                          // if(detailResponseData!.deliveryMode != "Four Wheeler")

                          const SizedBox(
                            height: 5,
                          ),
                          // if(detailResponseData!.deliveryMode != "Four Wheeler")
                          if (detailResponseData!.deliveryCharge != 0)
                            orderDetail(
                                title: "Delivery Charge",
                                value: "${detailResponseData?.currencySymbol ?? "AED"}" +
                                    double.parse((detailResponseData!
                                                    .deliveryCharge +
                                                detailResponseData!
                                                    .deliveryDiscount)
                                            .toString())
                                        .toStringAsFixed(2)),
                          if(detailResponseData!.shopGst != null && detailResponseData!.shopGst != 0)
                            const SizedBox(height: 10,),
                          if(detailResponseData!.shopGst != null && detailResponseData!.shopGst != 0)
                          orderDetail(
                              title: "Tax",
                              value: "${detailResponseData?.currencySymbol ?? "AED"}" +
                                  double.parse((detailResponseData!.shopGst)
                                      .toString())
                                      .toStringAsFixed(2)),
                          // if(detailResponseData!.weight != 0)
                          // orderDetail(
                          //     title: "GST",
                          //     value: "AED" +
                          //         double.parse((detailResponseData!.taxAmount)
                          //             .toString())
                          //             .toStringAsFixed(2)),
                          const SizedBox(
                            height: 5,
                          ),


                          if (detailResponseData!.deliveryDiscount != 0 &&
                              detailResponseData!.deliveryDiscount != 0.00 &&
                              detailResponseData!.deliveryCharge != 0)
                            orderDetail(
                                title: "Delivery Discount",
                                value: "${detailResponseData?.currencySymbol ?? "AED"}" +
                                    double.parse(detailResponseData!
                                            .deliveryDiscount
                                            .toString())
                                        .toStringAsFixed(2)),
                          if (detailResponseData!.deliveryDiscount != 0)
                            const SizedBox(
                              height: 5,
                            ),
                          if (detailResponseData!.deliveryMode ==
                              "Four Wheeler")
                            const SizedBox(
                              height: 5,
                            ),
                          if (detailResponseData!.deliveryMode ==
                                  "Four Wheeler" &&
                              detailResponseData!.deliveryCharge != 0)
                            orderDetail(
                                title: "Service Charge",
                                value: "${detailResponseData?.currencySymbol ?? "AED"}" +
                                    double.parse(detailResponseData!.taxAmount
                                            .toString())
                                        .toStringAsFixed(2)),
                          if (detailResponseData!.deliveryMode ==
                                  "Four Wheeler" &&
                              detailResponseData!.deliveryCharge != 0)
                            Text("(including Tax)"),

                          // orderDetail(
                          //     title: "Taxes",
                          //     value: "AED" +
                          //         detailResponseData.taxAmount.toString()),

                          const SizedBox(
                            height: 10,
                          ),
                          // orderDetail(
                          //     title: "Wallet Amount used",
                          //     value: "AED" +
                          //         detailResponseData!.walletAmount
                          // //             .toString()),
                          // const SizedBox(
                          //   height: 5,
                          // ),
                          // orderDetail(title: "Convenience Fee", value: "AED0"),
                          // const SizedBox(
                          //   height: 5,
                          // ),
                          if (detailResponseData!.deliveryCharge != 0)
                            orderDetail(
                                title: "Sub Total",
                                value: "${detailResponseData?.currencySymbol ?? "AED"}" +
                                    detailResponseData!.totalPayable
                                        .toString()),
                          const SizedBox(
                            height: 10,
                          ),
                          orderDetail(
                            success: detailResponseData!.toPayAmount == "0.00" ? Colors.green : Colors.red,
                              title: "Payment Status",
                              value: detailResponseData!.toPayAmount == "0.00" ? "Successful" : "Waiting") ,
                          // const SizedBox(
                          //   height: 20,
                          // ),

                          if (detailResponseData!.weight == 0)
                            TextButton(
                                onPressed: () {
                                  // loadData(context: context);
                                  Helpers().getCommonAlertDialogBox(
                                      content: AlertDialog(
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(15)),
                                        title: Center(
                                            child: Text("ID: ${detailResponseData!.bookingId}")),
                                        content: SingleChildScrollView(
                                          child: Column(
                                            children: [
                                              Text(
                                                detailResponseData!
                                                            .homeDelCatDetails!
                                                            .single
                                                            .catValue ==
                                                        null
                                                    ? ""
                                                    : detailResponseData!
                                                        .homeDelCatDetails!
                                                        .single
                                                        .catValue
                                                        .toString(),
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.w600),
                                              ),
                                              if (detailResponseData!
                                                      .homeDelCatDetails!
                                                      .single
                                                      .billImage !=
                                                  null)
                                                CachedNetworkImage(
                                                    imageUrl: App24UserAppAPI
                                                            .imageUrl +
                                                        detailResponseData!
                                                            .homeDelCatDetails!
                                                            .single
                                                            .billImage)
                                              // orderDetail(
                                              //     title: "",
                                              //     value: detailResponseData
                                              //         .homeDelCatDetails!
                                              //         .single
                                              //         .catValue == null ? "" : detailResponseData
                                              //         .homeDelCatDetails!
                                              //         .single
                                              //         .catValue)
                                            ],
                                          ),
                                        ),
                                        actions: [
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                right: 10),
                                            child: MaterialButton(
                                              onPressed: () {
                                                Navigator.pop(context);
                                              },
                                              child: Text(
                                                "Ok",
                                                style: TextStyle(
                                                    color: Colors.white),
                                              ),
                                              shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(5),
                                              ),
                                              color: App24Colors
                                                  .greenOrderEssentialThemeColor,
                                            ),
                                          )
                                        ],
                                      ),
                                      context: context);

                                  // showDialog(
                                  //     context: context,
                                  //     builder: (_) => AlertDialog(
                                  //       shape: RoundedRectangleBorder(
                                  //           borderRadius:
                                  //           BorderRadius.circular(15)),
                                  //       title: Center(
                                  //           child: Text("Order Details")),
                                  //       content: SingleChildScrollView(
                                  //         child: Column(
                                  //           children: [
                                  //             Text(
                                  //               detailResponseData
                                  //                   .homeDelCatDetails!
                                  //                   .single
                                  //                   .catValue ==
                                  //                   null
                                  //                   ? ""
                                  //                   : detailResponseData
                                  //                   .homeDelCatDetails!
                                  //                   .single
                                  //                   .catValue
                                  //                   .toString(),
                                  //               style: TextStyle(
                                  //                   fontWeight:
                                  //                   FontWeight.w600),
                                  //             ),
                                  //             // orderDetail(
                                  //             //     title: "",
                                  //             //     value: detailResponseData
                                  //             //         .homeDelCatDetails!
                                  //             //         .single
                                  //             //         .catValue == null ? "" : detailResponseData
                                  //             //         .homeDelCatDetails!
                                  //             //         .single
                                  //             //         .catValue)
                                  //           ],
                                  //         ),
                                  //       ),
                                  //       actions: [
                                  //         Padding(
                                  //           padding: const EdgeInsets.only(
                                  //               right: 10),
                                  //           child: MaterialButton(
                                  //             onPressed: () {
                                  //               Navigator.pop(context);
                                  //             },
                                  //             child: Text(
                                  //               "Ok",
                                  //               style: TextStyle(
                                  //                   color: Colors.white),
                                  //             ),
                                  //             shape: RoundedRectangleBorder(
                                  //               borderRadius:
                                  //               BorderRadius.circular(5),
                                  //             ),
                                  //             color: App24Colors
                                  //                 .greenOrderEssentialThemeColor,
                                  //           ),
                                  //         )
                                  //       ],
                                  //     ));
                                },
                                child: Text("Order Details",
                                    style: TextStyle(
                                      decoration: TextDecoration.underline,
                                    ))),
                          // ElevatedButton(onPressed: (){}, child: Text("OrderDetails"),style: ElevatedButton.styleFrom(primary: App24Colors.greenOrderEssentialThemeColor),),
                          if (detailResponseData!.weight == 0)
                            const SizedBox(
                              height: 20,
                            ),
                          if (detailResponseData!.weight == 0)
                            Center(
                              child: Text(
                                "Order From",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize:
                                        MediaQuery.of(context).size.width / 25,
                                    decoration: TextDecoration.underline),
                              ),
                            ),
                          const SizedBox(
                            height: 10,
                          ),
                          if (detailResponseData!.weight == 0)
                            Center(
                                child: Text(
                              detailResponseData!.storeName.toString(),
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontWeight: FontWeight.w600,
                              ),
                            )),
                          if (detailResponseData!.weight == 0)
                            Center(
                                child: Text(
                              detailResponseData!.storeLocation.toString(),
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize:
                                      MediaQuery.of(context).size.width / 32),
                            )),
                          const SizedBox(
                            height: 20,
                          ),

                          // Track Order (do not delete)

                          if (detailResponseData!.status != 'SEARCHING' &&
                              detailResponseData!.status != 'SCHEDULED' &&
                              detailResponseData!.status != 'ORDERCOMPLETED' &&
                              detailResponseData!.status != 'PENDING' &&
                              detailResponseData!.status != 'CANCELLED' &&
                              detailResponseData!.status != "WAITINGFORSHOP")
                            Row(
                              children: [
                                Expanded(
                                    child: CommonButton(
                                  onTap: () {
                                    print("providerId :: ${detailResponseData!.providerId}");
                                    print("ID :: ${detailResponseData!.id}");
                                    print("pickUpLng :: ${detailResponseData!.pickUpLng}");
                                    print("pickUpLat :: ${detailResponseData!.pickUpLat}");
                                    print("deliveryLng  :: ${detailResponseData!.deliveryLng}");
                                    print("deliveryLat${detailResponseData!.deliveryLat}");
                                    // print(detailResponseData.packageDetail);
                                    navigateToMap(
                                        detailResponseData: detailResponseData);
                                  },
                                  bgColor:
                                      App24Colors.greenOrderEssentialThemeColor,
                                  text: "Track Order",
                                )),
                              ],
                            ),
                          const SizedBox(
                            height: 20,
                          ),
                          /*for future purpose*/
                          buildStatus(),

                          // rateDelivery(),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              detailResponseData!.status!,
                              style: TextStyle(
                                color: Colors.green,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          if (detailResponseData!.status == "CANCELLED")
                            Center(
                                child: Text("Your order has been cancelled")),
                          if (detailResponseData!.status != "CANCELLED")
                            Column(children: buildOrderStatus(i)),

                          // if(detailResponseData!.status != "PROCESSING")
                          //     showLog(detailResponseData!.status + "asdasda"),

                          if (detailResponseData!.status == "ORDERCOMPLETED")

                            // rateDelivery()
                            Center(
                              child: GestureDetector(
                                onTap: () {
                                  // print("shop name to rate delivery " + detailResponseData!.storeName);
                                  // print(detailResponseData!.providerName);
                                  // Navigator.pushReplacement(
                                  //     context,
                                  //     MaterialPageRoute(
                                  //         builder: (context) => HomePage()));
                                  // Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage()));
                                  // Helpers().getCommonBottomSheet(context: context, content: HomePage(),);
                                  Helpers().getCommonBottomSheet(
                                      context: context,
                                      content: RateDeliveryBoy(

                                        shopName: detailResponseData!.weight == 0 ? detailResponseData!.storeName : "send",
                                        deliveryBoyName:
                                            detailResponseData!.providerName,
                                        orderId: detailResponseData!.id!,
                                      ),
                                      title: "Your opinion matters to us");
                                  state = state.copyWith(
                                      isLoading: true, isError: false);
                                },
                                child: Text(
                                  "Rate your order",
                                  style: TextStyle(
                                      color: App24Colors
                                          .greenOrderEssentialThemeColor,
                                      fontWeight: FontWeight.bold,
                                      decoration: TextDecoration.underline),
                                ),
                              ),
                            ),

                          //         if (detailResponseData.paid == 0 && detailResponseData.status == 'SEARCHING' ||
                          // detailResponseData.status == "PENDING" ||
                          // detailResponseData.status == "SCHEDULED" ||
                          // detailResponseData.status == "ORDERED"
                          // ||detailResponseData.status == "WAITINGFORSHOP" || detailResponseData.status == "CANCELLED")
                          // if (detailResponseData.status == 'SEARCHING' ||
                          //     detailResponseData.status == "PENDING" ||
                          //     detailResponseData.status == "SCHEDULED" ||
                          //     detailResponseData.status == "ORDERED"
                          //     ||detailResponseData.status == "WAITINGFORSHOP")
                          // Center(
                          //   child: GestureDetector(
                          //     onTap: () async {
                          //       // Map body = {
                          //       //   "order_id": detailResponseData.id,
                          //       // };
                          //       // context.read(cancelOrderNotifier.notifier).cancelOrder(
                          //       //     body: body,context: context
                          //       // );
                          //       // setState(() {
                          //       //   context
                          //       //       .read(orderDetailsNotifierProvider.notifier)
                          //       //       .getOrderDetails(id: widget.id == null ? null : widget.id);
                          //       // });
                          //       var cancelled =
                          //           await Helpers().getCommonBottomSheet(
                          //               context: context,
                          //               content: CancelOrderBottomSheet(
                          //                 orderId: detailResponseData!.id,
                          //                 orderDetailId: widget.id,
                          //               ),
                          //               title: "Cancel");
                          //       print("if cancelled" + cancelled.toString());
                          //       if (cancelled != null) {
                          //         loadData(context: context);
                          //         Navigator.pushReplacement(
                          //             context,
                          //             MaterialPageRoute(
                          //                 builder: (context) => HomePage()));
                          //       }
                          //       // getCommonBottomSheet(context, RateDeliveryBoy(deliveryBoyName: detailResponseData.providerName,));
                          //     },
                          //     child: Text(
                          //       "cancel your order.",
                          //       style: TextStyle(
                          //           color: App24Colors
                          //               .greenOrderEssentialThemeColor,
                          //           fontWeight: FontWeight.bold,
                          //           decoration: TextDecoration.underline),
                          //     ),
                          //   ),
                          // )
                        ],
                      ),
                    ),
                  ),
                );
              }
              return Container();
            }),
          ),
        ));
  }

  Container statusWidget(
      {required String statusText,
      bool isCompleted = false,
      bool isLineVisible = false}) {
    return Container(
      // padding: EdgeInsets.symmetric(vertical: 20, horizontal: 4),
      child: IntrinsicHeight(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              children: [
                Container(
                  height: 20,
                  width: 20,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color:
                          (isCompleted) ? Color(0xff1ABE7A) : Colors.grey[200],
                      border: Border.all(
                        color: (isCompleted)
                            ? Colors.transparent
                            : Color(0xff1ABE7A),
                        width: 3,
                      )),
                ),
                if (isLineVisible == true)
                  Container(
                    width: 5,
                    height: 45,
                    color: Color(0xff1ABE7A),
                  ),
                if (isLineVisible == false)
                  const SizedBox(
                    height: 45,
                  )
              ],
            ),
            const SizedBox(
              width: 25,
            ),
            Expanded(
              child: Text(
                statusText,
                style: TextStyle(
                    color: (isCompleted) ? Color(0xff1ABE7A) : Colors.grey[400],
                    fontSize: 16,
                    fontWeight: FontWeight.bold),
              ),
            ),
            // if (detailResponseData.status == "ORDERCOMPLETED")
            //   Helpers().getCommonBottomSheet(
            //                                     context: context,
            //                                     content: RateDeliveryBoy(
            //                                       deliveryBoyName:
            //                                           detailResponseData.providerName,
            //                                       orderId: detailResponseData.id!,
            //                                     ),
            //                                     title: "Your opinion matters to us");
          ],
        ),
      ),
    );
  }
}
