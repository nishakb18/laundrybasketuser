import 'package:app24_user_app/constants/color_coman.dart';
import 'package:favorite_button/favorite_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoadingError extends StatelessWidget {
  final Function(BuildContext) onPressed;
  final String? message;

  LoadingError({required this.onPressed, this.message});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(14.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(message!,
              textAlign: TextAlign.center,),
              SizedBox(height: 10,),
              ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStatePropertyAll(AppColor.carvebarcolor),

                ),
                  onPressed: () {
                    onPressed(context);
                  },
                  child: Text("TRY AGAIN",
                  style: TextStyle(color: AppColor.white),))
            ],
          ),
        ),
      ),
    );
  }
}
