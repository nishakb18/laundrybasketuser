import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/constants/global_constants.dart';
import 'package:app24_user_app/models/location_model.dart';
import 'package:app24_user_app/modules/delivery/shop_list/shop_list_model.dart';
import 'package:app24_user_app/modules/my_account/address/add_update_address.dart';
import 'package:app24_user_app/providers/location_picker_provider.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/utils/services/database/database_service.dart';
import 'package:app24_user_app/utils/services/database/location_database.dart';
import 'package:app24_user_app/widgets/common_main_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:geocoding/geocoding.dart' as geoCoding;
import 'package:google_maps_flutter/google_maps_flutter.dart';

// import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
// import 'package:latlong2/latlong.dart';

class SelectLocationAddressPage extends StatefulWidget {
  const SelectLocationAddressPage(
      {Key? key,
      this.locationAutoCompleteModel,
      required this.title,
      this.isCurrentLocation = false,
      this.fromManageAddress,
      this.mainText,
      this.fromCart,
      this.map,
      this.shopResponseData,
      this.storeTypeIdFromAddList,
      this.addListText,
      this.addListFilePath,
      this.isImageSelectedAddList,
      this.searchUserLocation})
      : super(key: key);
  final LocationModel? locationAutoCompleteModel;
  final String title;
  final bool isCurrentLocation;
  final String? fromManageAddress;
  final String? mainText;
  final String? fromCart;
  final Map<dynamic, dynamic>? map;
  final ShopNamesModelResponseData? shopResponseData;
  final String? storeTypeIdFromAddList;
  final String? addListText;
  final String? addListFilePath;
  final bool? isImageSelectedAddList;
  final bool? searchUserLocation;

  @override
  _SelectLocationAddressPageState createState() =>
      _SelectLocationAddressPageState();
}

class _SelectLocationAddressPageState extends State<SelectLocationAddressPage>
    with AutomaticKeepAliveClientMixin<SelectLocationAddressPage> {
  bool isFromSearch = false;
  CameraPosition _initialLocation =
      CameraPosition(target: LatLng(20.5937, 78.9629), zoom: 4.0);

  // MapController? mapController;
  GoogleMapController? mapController;

  late LocationNotifier _locationPickerProvider;
  final Location location = new Location();
  late bool _serviceEnabled;
  PermissionStatus? _permissionGranted;
  TextEditingController selectedLocations = new TextEditingController();

  bool isCameraMoved = false;
  bool isFirstLoad = true;

  String? _currentAddress;
  String? _exactAddress;
  String? title;
  String? subTitle;

  LocationModel? selectedLocation;
  late LocationModel movedLocation;

  setInitialMapValues() {
    if (widget.isCurrentLocation) {
      _getCurrentLocation();
    } else {
      selectedLocation = widget.locationAutoCompleteModel;
      setLocation(selectedLocation!.latitude, selectedLocation!.longitude);
      if (isCameraMoved) {
        isCameraMoved = false;
      }
      getAddress(
        lat: selectedLocation!.latitude!,
        long: selectedLocation!.longitude!,
      );
      setState(() {
        //_currentAddress=widget.locationAutoCompleteModel!.main_text;

        isFromSearch = false;
      });
    }
  }

  // Method for retrieving the current location
  _getCurrentLocation() async {
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }

    await location.getLocation().then((position) async {
      LocationData _currentLocation = position;

      selectedLocation = new LocationModel(
          latitude: _currentLocation.latitude,
          longitude: _currentLocation.longitude);
      setLocation(selectedLocation!.latitude, selectedLocation!.longitude);

      getAddress(
        lat: selectedLocation!.latitude!,
        long: selectedLocation!.longitude!,
      );

      setState(() {
        isFromSearch = false;
      });
    });
  }

  Future<void> setLocation(lat, long) async {
    setState(() {
      // mapController = LatLng(lat, long) as MapController?;
      // mapController!.move(LatLng(lat, long), 3.0);
      mapController!.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(
            target: LatLng(lat, long),
            zoom: 14.0,
          ),
        ),
      );
      // print("print lat long"+LatLng(lat, long).toString());
    });
  }

  // Method for retrieving the address
  getAddress({
    required double lat,
    required double long,
  }) async {
    try {
      List<geoCoding.Placemark> placeMarks =
          await geoCoding.placemarkFromCoordinates(lat, long);

      //final coordinates = new Coordinates(lat, long);
      //var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
      var first = placeMarks.first;
      setState(() {
        //  showMessage(context, first.thoroughfare .toString(), false, false);

        title = first.name;
        subTitle = first.locality;
        if (isFromSearch == false)
          _currentAddress =
              "${first.name}, ${first.locality}, ${first.postalCode}, ${first.country}";

        if (isCameraMoved) {
          isCameraMoved = false;
        }

        selectedLocation!.description = _currentAddress;
        selectedLocation!.main_text = title;
        selectedLocation!.secondary_text = subTitle;
        selectedLocation!.latitude = lat;
        selectedLocation!.longitude = long;
      });
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  navigateToMap(
      {LocationModel? location,
      bool isCurrentLocation = false /*required bool isSavedAddress*/,
      required bool shouldNavigateToMap}) {
// print("agandev nr");

    // if (!widget.initialCall!) {
    //   Navigator.pop(context, isCurrentLocation ? 'current_location' : location);
    // }
    if (shouldNavigateToMap == false)
      // {
      saveToDbAndServer(location);
    // print("desc "+location!.description.toString());
    // Navigator.pop(context, /*isCurrentLocation ? 'current_location' :*/ location);
    // }else {
    //   Navigator.push(
    //     context,
    //     MaterialPageRoute(
    //         builder: (context) => SelectLocationPage(
    //           isSavedAddress: isSavedAddress,
    //           mainText: location?.main_text ?? "",
    //           locationAutoCompleteModel: location,
    //           title: widget.title!,
    //           isCurrentLocation: isCurrentLocation,
    //         )),
    //   );
    // }
  }

  void saveToDbAndServer(LocationModel? location) async {
    print("in save to db and server");

    LocationDatabase locationDatabase =
        LocationDatabase(DatabaseService.instance);

    await locationDatabase.insertLocation(LocationModel(
        placeID: location?.placeID ?? '',
        latitude: location!.latitude,
        longitude: location.longitude,
        description: location.description,
        main_text: location.main_text,
        secondary_text: location.secondary_text,
        type: 3));

    if (location.pinColor != null && location.pinColor == Colors.red) {
      var body = {
        'mPlaceId': location.placeID,
        'mPrimary': location.main_text,
        'mSecondary': location.secondary_text == ""
            ? location.secondary_text == "unnamed"
            : location.secondary_text,
        'mFullAddress': location.description!,
        'lat': location.latitude,
        'lng': location.longitude
      };
      print("dony body: " + body.toString());
      await context
          .read(locationNotifierProvider.notifier)
          .saveAutoCompleteLocationToServer(body);
    }
  }

  // navigateToMap({LocationModel? location, bool isCurrentLocation = false}) {
  //   // if (!widget.initialCall!) {
  //   //   Navigator.pop(context, isCurrentLocation ? 'current_location' : location);
  //   // } else {
  //     Navigator.push(
  //       context,
  //       MaterialPageRoute(
  //           builder: (context) => SelectLocationAddressPage(
  //             mainText: location?.main_text ?? "",
  //             locationAutoCompleteModel: location,
  //             title: widget.title,
  //             isCurrentLocation: isCurrentLocation,
  //           )),
  //     );
  //   // }
  // }

  Future<void> updateToDatabase(type) async {
    LocationDatabase locationDatabase =
        LocationDatabase(DatabaseService.instance);

    await locationDatabase.insertLocation(LocationModel(
        placeID: selectedLocation?.placeID ?? '',
        latitude: selectedLocation!.latitude,
        longitude: selectedLocation!.longitude,
        description: _currentAddress,
        main_text: selectedLocation!.main_text,
        secondary_text: selectedLocation!.secondary_text,
        type: type));
  }

  updateLatLongByUserSearch(
      LocationModel location, bool isSavedAddress, bool shouldNavigateToMap) {
    print("checking location captured");
    if (location.latitude == null && location.longitude == null) {
      _locationPickerProvider
          .setLatLongFromPlaceId(location.placeID)
          .then((value) {
        if (value != null) {
          location.latitude = value['lat'];
          location.longitude = value['long'];

          navigateToMapByUserSearch(
              location: location,
              isSavedAddress: isSavedAddress,
              shouldNavigateToMap: shouldNavigateToMap);
        } else {
          showMessage(context, "Error occurred. Please try again.", true, true);
        }
      });
    } else {
      navigateToMapByUserSearch(
          location: location,
          isSavedAddress: isSavedAddress,
          shouldNavigateToMap: shouldNavigateToMap);
    }
  }

  navigateToMapByUserSearch(
      {LocationModel? location,
      bool isCurrentLocation = false,
      required bool isSavedAddress,
      required bool shouldNavigateToMap}) {
    print("navigating to map");

    // if (!widget.initialCall!) {
    print("agandve");

    // if (value != null) {
    LocationModel model = location!;
    setState(() {
      context.read(homeNotifierProvider.notifier).currentLocation = model;
      context
          .read(landingPagePromoCodeProvider.notifier)
          .getLandingPromoCodes(context: context);
      // loadData(context: context);
    });
    // }

// Navigator.push(context, MaterialPageRoute(builder: (context) => ShopEssentials()));
//       Navigator.pop(context, isCurrentLocation ? 'current_location' : location);
    // }
    if (shouldNavigateToMap == false) {
      saveToDbAndServer(location);
      LocationModel model = location;
      setState(() {
        context.read(homeNotifierProvider.notifier).currentLocation = model;
        context
            .read(landingPagePromoCodeProvider.notifier)
            .getLandingPromoCodes(context: context);
        // loadData(context: context);
      });
      mapController!.dispose();
      Navigator.pop(context);
      Navigator.pop(context, selectedLocation);
      // print("desc "+location!.description.toString());
      // Navigator.pop(context, /*isCurrentLocation ? 'current_location' :*/ location);
    } else {
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => SelectLocationAddressPage(
                    // searchUserLocation: widget.searchUserLocation,
                    title: 'Enter Location',
                    fromManageAddress: "fromManageAddress",
                    isCurrentLocation: true,
                    fromCart: "true",
                  )));
      // Navigator.push(
      //   context,
      //   MaterialPageRoute(
      //       builder: (context) => SelectLocationPage(
      //             isSavedAddress: isSavedAddress,
      //             mainText: location?.main_text ?? "",
      //             locationAutoCompleteModel: location,
      //             title: widget.title!,
      //             isCurrentLocation: isCurrentLocation,
      //           )),
      // );
    }
  }

  updateLatLong({required LocationModel location, bool? fromSearch}) {
    // print("update latlong location " + location.main_text.toString());
    if (location.latitude == null && location.longitude == null) {
      _locationPickerProvider
          .setLatLongFromPlaceId(location.placeID)
          .then((value) {
        if (value != null) {
          location.latitude = value['lat'];
          location.longitude = value['long'];
          navigateToMap(location: location, shouldNavigateToMap: false);
          print("update latlong location " + location.latitude.toString());
          selectedLocation = location;

          setLocation(selectedLocation!.latitude, selectedLocation!.longitude);
          getAddress(
            lat: selectedLocation!.latitude,
            long: selectedLocation!.longitude,
          );
          // }
          setState(() {
            selectedLocations.text = "";
            // _currentAddress = location.description;
          });
        } else {
          showMessage(context, "Error occurred. Please try again.", true, true);
        }
      });
    } else {
      navigateToMap(location: location, shouldNavigateToMap: false);
    }
  }

  Future<void> confirmLocation() async {
    if (_currentAddress != null) {
      if (widget.title == GlobalConstants.labelPickupLocation) {
        updateToDatabase(0);
      } else {
        updateToDatabase(1);
      }
      if (selectedLocation!.pinColor != null &&
          selectedLocation!.pinColor == Colors.red) {
        var body = {
          'mPlaceId': selectedLocation!.placeID,
          'mPrimary': selectedLocation!.main_text,
          'mSecondary': selectedLocation?.secondary_text == ""
              ? selectedLocation?.secondary_text == "unnamed"
              : selectedLocation?.secondary_text,
          'mFullAddress': selectedLocation!.description,
          'lat': selectedLocation!.latitude,
          'lng': selectedLocation!.longitude
        };
        await context
            .read(locationNotifierProvider.notifier)
            .saveAutoCompleteLocationToServer(body);
      }
      if (widget.fromManageAddress == "fromManageAddress") {
        mapController!.dispose();
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => AddUpdateAddressPage(
                      fromManageAddress: widget.fromManageAddress,
                      searchUserLocation: widget.searchUserLocation,
                      selectedLocation: selectedLocation,
                      fromCart: widget.fromCart,
                      map: widget.map,
                      storeTypeIdFromAddList: widget.storeTypeIdFromAddList,
                      shopResponseData: widget.shopResponseData,
                      addListText: widget.addListText,
                      addListFilePath: widget.addListFilePath,
                      isImageSelectedAddList: widget.isImageSelectedAddList,
                    )));
      } else {
        // print("popping");
        mapController!.dispose();
        Navigator.pop(context);
        Navigator.pop(context, selectedLocation);
      }
    }
  }

  changeLocation() {
    showSelectLocationSheetForAddress(
            context: context, initialCall: false, title: widget.title)
        .then((value) {
      if (value != null) {
        setState(() {
          isCameraMoved = false;
          isFirstLoad = true;
        });
        if (value == 'current_location') {
          _getCurrentLocation();
        } else {
          selectedLocation = value;
          setLocation(selectedLocation!.latitude, selectedLocation!.longitude);
          _currentAddress = selectedLocation!.description;
          //getAddress(lat: selectedLocation!.latitude,long:  selectedLocation!.longitude!);
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
        body: SafeArea(
      child: Column(
        children: <Widget>[
          Expanded(
            child: Stack(
              children: <Widget>[
                //       FlutterMap(
                //         // mapController: mapController,
                //         options: MapOptions(
                // //           onPositionChanged: (CameraPosition newPosition,bool) {
                // //   if (!isFirstLoad) {
                // //     movedLocation = new LocationModel(
                // //       latitude: newPosition.target.latitude,
                // //       longitude: newPosition.target.longitude,
                // //     );
                // //   }
                // //   setState(() {
                // //     isCameraMoved = true;
                // //   });
                // // },
                //           onMapCreated: (MapController controller) {
                //             mapController = controller;
                //             setInitialMapValues();
                //           },
                //
                //           center: LatLng(10.5276, 76.2144),
                //           zoom: 13,
                //           maxZoom: 17 ,
                //         ),
                //         layers: [
                //           TileLayerOptions(
                //               urlTemplate:
                //                   "https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png"),
                //           // MarkerLayerOptions(markers: markersList != null ? markersList! : []),
                //         ],
                //       ),

                GoogleMap(
                  minMaxZoomPreference: MinMaxZoomPreference(15, 21),
                  initialCameraPosition: _initialLocation,
                  myLocationEnabled: false,
                  myLocationButtonEnabled: false,
                  mapType: MapType.normal,
                  zoomGesturesEnabled: true,
                  zoomControlsEnabled: false,
                  onMapCreated: (GoogleMapController controller) {
                    mapController = controller;
                    setInitialMapValues();
                  },
                  onCameraIdle: () {
                    if (isFromSearch == false) {
                      if (isCameraMoved && !isFirstLoad) {
                        getAddress(
                          lat: movedLocation.latitude!,
                          long: movedLocation.longitude!,
                        );
                      }
                      if (isFirstLoad) {
                        setState(() {
                          isFirstLoad = false;
                        });
                      }
                    }
                  },
                  onCameraMove: (CameraPosition newPosition) async {
                    if (!isFirstLoad) {
                      movedLocation = new LocationModel(
                        latitude: newPosition.target.latitude,
                        longitude: newPosition.target.longitude,
                      );
                    }
                    setState(() {
                      isFromSearch = false;
                      isCameraMoved = true;
                    });
                  },
                ),
                Center(
                    child:
                        /*ScaleTransition(
                                  scale: Tween(begin: 0.75, end: 1.0)
                                      .animate(
                                    CurvedAnimation(
                                        parent: _resizableController,
                                        curve: Curves.bounceInOut),
                                  ),
                                  child: Icon(
                                    Icons.location_on,
                                    color: Colors.red,
                                    size: 40,
                                  ))*/
                        Icon(
                  Icons.location_on,
                  color: Colors.red,
                  size: 40,
                )),
                /* AnimatedBuilder(
      animation: _resizableController,
      builder: (context, child) {
          return Container(
            padding: EdgeInsets.all(24),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(
                    color: Colors.blue, width: _resizableController.value * 10),
            ),
            child: Icon(
              Icons.location_on,
              color: Colors.red,
              size: 30,
            ),
          );
      })*/

                Positioned(
                  top: 50,
                  left: 20,
                  child: FloatingActionButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Icon(Icons.arrow_back),
                    mini: true,
                    backgroundColor: Theme.of(context).cardColor,
                  ),
                ),
                Positioned(
                  bottom: 0,
                  right: 0,
                  child: Padding(
                    padding: EdgeInsets.only(bottom: 10, right: 10),
                    child: FloatingActionButton(
                      heroTag: "my_location",
                      onPressed: () {
                        _getCurrentLocation();
                      },
                      child: Icon(Icons.my_location),
                      mini: true,
                      backgroundColor: Theme.of(context).cardColor,
                    ),
                  ),
                )
              ],
            ),
          ),
          Column(
            children: <Widget>[
              Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.symmetric(vertical: 5, horizontal: 20),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(30),
                          topLeft: Radius.circular(30)),
                      color: Colors.white),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      const SizedBox(
                        height: 5,
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: getTitle(widget.title),
                          ),
                          TextButton(
                            onPressed: () {
                              changeLocation();
                            },
                            child: Text(
                              "CHANGE",
                              style: TextStyle(
                                  color: App24Colors.darkTextColor,
                                  fontWeight: FontWeight.w700),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      Consumer(
                        builder: (context, watch, child) {
                          _locationPickerProvider =
                              watch(locationNotifierProvider.notifier);
                          _locationPickerProvider.setInitialValues(
                              context: context);
                          return

                              //Text(_currentAddress!);

                              //       TypeAheadField(
                              //       direction: AxisDirection.up,
                              //       hideOnEmpty: false,
                              //       hideSuggestionsOnKeyboardHide: false,
                              //       keepSuggestionsOnLoading: true,
                              //       getImmediateSuggestions: true,
                              //       keepSuggestionsOnSuggestionSelected: true,
                              //       suggestionsBoxDecoration: SuggestionsBoxDecoration(
                              //         color: Colors.white,
                              //         borderRadius: BorderRadius.circular(8),
                              //       ),
                              //       textFieldConfiguration: TextFieldConfiguration(
                              //         controller: selectedLocations,
                              //         style: TextStyle(fontWeight: FontWeight.w600),
                              //         decoration: InputDecoration(
                              //           border: OutlineInputBorder(
                              //             borderRadius: BorderRadius.circular(15),
                              //             borderSide: BorderSide(
                              //               width: 0,
                              //               style: BorderStyle.none,
                              //             ),
                              //           ),
                              //           // labelText: _currentAddress,
                              //           fillColor: Color(0xff676767).withOpacity(0.09),
                              //           filled: true,
                              //           contentPadding:
                              //               EdgeInsets.symmetric(vertical: 0),
                              //           prefixIcon: Icon(Icons.search),
                              //           hintText: _currentAddress,
                              //         ),
                              //         onEditingComplete: () {
                              //           SystemChannels.textInput
                              //               .invokeMethod('TextInput.hide');
                              //         },
                              //       ),
                              //
                              //       itemBuilder: (context, LocationModel location) {
                              //         print("on itembuilder " +
                              //             location.latitude.toString());
                              //         // print("location model" +location.main_text.toString());
                              //         return ListTile(
                              //           leading: Icon(
                              //             CupertinoIcons.location_solid,
                              //             color: location
                              //                 .pinColor, //green - from local DB , orange - from server , red - from google
                              //           ),
                              //           title: Text(
                              //             location.main_text.toString(),
                              //             style: TextStyle(
                              //                 fontSize: 17,
                              //                 fontWeight: FontWeight.w600),
                              //           ),
                              //           subtitle: Text(
                              //             location.secondary_text.toString(),
                              //             maxLines: 2,
                              //           ),
                              //         );
                              //       },
                              //       onSuggestionSelected:
                              //           (LocationModel location) async {
                              //         FocusScopeNode currentFocus =
                              //             FocusScope.of(context);
                              //
                              //         if (!currentFocus.hasPrimaryFocus) {
                              //           currentFocus.unfocus();
                              //         }
                              //         await updateLatLong(
                              //             location: location, fromSearch: true);
                              //         selectedLocation = location;
                              //         setState(() {
                              //           selectedLocation = location;
                              //         });
                              //
                              //         setLocation(selectedLocation!.latitude,
                              //             selectedLocation!.longitude);
                              //
                              //         getAddress(
                              //             lat: selectedLocation!.latitude,
                              //             long: selectedLocation!.longitude,
                              //             fromSearch: " onSuggestionSelected ");
                              //         setState(() {
                              //           isFromSearch = true;
                              //         });
                              //         // }
                              //         setState(() {
                              //           selectedLocations.text = "";
                              //           print("selected location print" +
                              //               selectedLocation!.main_text.toString());
                              //           //_currentAddress=selectedLocation!.main_text.toString();
                              //           _exactAddress =
                              //               selectedLocation!.main_text.toString();
                              //           // _currentAddress = location.description;
                              //         });
                              //
                              //         //  setInitialMapValue();
                              //       },
                              //       suggestionsCallback: (pattern) async {
                              //         print("pattern :" + pattern);
                              //         if (pattern.length > 2) {
                              //           return await _locationPickerProvider
                              //               .getSuggestions(pattern);
                              //         } else {
                              //           List<LocationModel> list = [];
                              //           return list;
                              //         }
                              //       },
                              //     );
                              //   },
                              // ),

                              Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              color: Theme.of(context).cardColor,
                            ),
                            padding: EdgeInsets.all(13),
                            alignment: Alignment.center,
                            child: Text(
                              _currentAddress ?? "",
                              style: TextStyle(
                                  fontWeight: FontWeight.w600, fontSize: 16),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                          );
                        },
                      ),
                      SizedBox(
                        height: 25,
                      ),
                      CommonButton(
                        onTap: () {
                          if (widget.searchUserLocation == true) {
                            updateLatLongByUserSearch(
                                new LocationModel(
                                    latitude: selectedLocation!.latitude,
                                    longitude: selectedLocation!.longitude,
                                    main_text: selectedLocation!.main_text,
                                    description: (selectedLocation!.main_text
                                                .toString() +
                                            " , " +
                                            // addressLine2!.text.toString() +
                                            // " , " +
                                            selectedLocation!.secondary_text
                                                .toString() /*+
                                                " , " +
                                                landmark!.text.toString()*/
                                        )),
                                true,
                                false);
                            // Navigator.pop(context);
                            // Navigator.pop(context,selectedLocation);
                            // updateLatLongByUserSearch(location: selectedLocation!,);
                          } else {
                            confirmLocation();
                          }
                        },
                        bgColor: App24Colors.darkTextColor,
                        text: "CONFIRM",
                      )
                    ],
                  ))
            ],
          )
        ],
      ),
    ));
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

  @override
  void dispose() {
    mapController!.dispose();
    super.dispose();
  }
}
