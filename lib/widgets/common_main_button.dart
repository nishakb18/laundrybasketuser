
import 'package:app24_user_app/constants/color_path.dart';
import 'package:flutter/material.dart';

class CommonButton extends StatelessWidget {
  const CommonButton({Key? key, this.onTap, this.text, this.bgColor}) : super(key: key);
  final VoidCallback? onTap;
  final String? text;
  final Color? bgColor;

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
        onPressed: onTap,
        color: bgColor,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20)),
        child: Padding(
          padding:
          EdgeInsets.symmetric(vertical: 15),
          child: Text(
            text!,
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold,fontSize: 20),
          ),
        ));
  }
}
