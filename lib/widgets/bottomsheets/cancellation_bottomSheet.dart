

import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/modules/home.dart';
import 'package:app24_user_app/modules/ride/ride_cancel_reason_model.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/widgets/common_main_button.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class CancellationBottomSheet extends StatefulWidget {
  final int? rideId;
  const CancellationBottomSheet({Key? key, this.rideId}) : super(key: key);

  @override
  _CancellationBottomSheetState createState() => _CancellationBottomSheetState();
}

class _CancellationBottomSheetState extends State<CancellationBottomSheet> {

  @override
  void initState() {
    context.read(rideCancelReasons.notifier).getCancelRideReasons();
    super.initState();
  }

  String cancel = "booked wrongly";
String? cancelRideReason;

  List<Widget> getCancelReasons(context, List<CancelReasonResponseData> cancelReason) {
    List<Widget> _cancelReasons = [];
    for (var i = 0; i < cancelReason.length; i++) {

        print(i);
        _cancelReasons.add(Padding(
          padding: const EdgeInsets.only(left: 1, top: 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Material(
                child: ListTile(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  onTap: () {
                    setState(() {
                      cancelRideReason = cancelReason[i].reason;
                      // paymentMode = "cash";
                    });
                  },
                  leading: Icon(
                    cancelRideReason ==
                        cancelReason[i].reason
                        ? Icons.check_circle
                        : Icons.radio_button_unchecked,
                    color: App24Colors.darkTextColor,
                  ),
                  title: Text(
                      cancelReason[i].reason.toString()),

                ),
              ),

              const SizedBox(
                height: 20,
              ),
              // MaterialButton(
              //   onPressed: () {},
              //   padding: EdgeInsets.symmetric( vertical: 15),
              //   shape: RoundedRectangleBorder(
              //       borderRadius: BorderRadius.circular(15)),
              //   color: App24Colors.yellowRideThemeColor,
              //   child: Text(
              //     "Select",
              //     style: TextStyle(color: Colors.white),
              //   ),
              // )
            ],
          ),
        ));
      }

    if (_cancelReasons.isEmpty) {
      _cancelReasons.add(Padding(
          padding: const EdgeInsets.only(left: 1, top: 10),
          child: Container(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              width: MediaQuery.of(context).size.width / 2.3,
              /*decoration: BoxDecoration(
    borderRadius: BorderRadius.circular(15),
    ),*/
              child: Text("No item found!"))));
    }

    return _cancelReasons;
  }




  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        children: [

          Consumer(builder: (context, watch, child) {
            final state = watch(rideCancelReasons);
            if (state.isLoading) {
              return CircularProgressIndicator.adaptive();
            } else if (state.isError) {
              return LoadingError(
                onPressed: (res) {
                  // reloadData(init: true, context: res);
                },
                message: state.errorMessage,
              );
            } else {
              return ListView(
                shrinkWrap: true,
                children: getCancelReasons(context,state.response.responseData),
              );
            }
          }),

          const SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              // crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Expanded(
                  child: CommonButton(
                    onTap: (){
                      Map body = {
                        "id" : widget.rideId,
                        "reason" : cancelRideReason,
                      };
context.read(rideCancelProvider.notifier).getCancelRide(body: body);
Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomePage()));
                    },
                    bgColor: App24Colors.yellowRideThemeColor,
                    text: "Submit",
                  )
                  // MaterialButton(
                  //   onPressed: () {},
                  //   color: App24Colors.darkTextColor,
                  //   shape: RoundedRectangleBorder(
                  //       borderRadius: BorderRadius.circular(10)),
                  //   padding: EdgeInsets.symmetric(vertical: 15),
                  //   child: Text(
                  //     "Submit",
                  //     style: TextStyle(
                  //         color: Colors.white,
                  //         fontWeight: FontWeight.w500),
                  //   ),
                  // ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
