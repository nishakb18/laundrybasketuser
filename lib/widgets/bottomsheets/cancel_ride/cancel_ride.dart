import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/modules/ride/ride.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class CancelRide extends StatefulWidget {
  const CancelRide({Key? key}) : super(key: key);

  @override
  _CancelRideState createState() => _CancelRideState();
}

class _CancelRideState extends State<CancelRide> {

  onCancelPressed(){
Map body = {
  "reason" : "Wrongly booked",
  "id" : 1444,
};
// Navigator.pop(context);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => RidePage()));
    context.read(rideCancelProvider.notifier).getCancelRide(body: body);
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
        child: Column(
          children: [
            const SizedBox(height: 10,),
            Text("Are you sure , you want to cancel your ride?",style: TextStyle(fontWeight: FontWeight.w600),),
            const SizedBox(height: 20,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                    child: MaterialButton(
                      padding: EdgeInsets.symmetric(vertical: 15),
                  onPressed: () {
                        onCancelPressed();

                  },
                  child: Text("Yes",style: TextStyle(color: Colors.white),),
                  color: App24Colors.greenOrderEssentialThemeColor,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                )),
                const SizedBox(width: 10,),
                Expanded(
                    child: MaterialButton(
                      padding: EdgeInsets.symmetric(vertical: 15),
                  onPressed: () {
                        Navigator.pop(context);
                  },
                  child: Text("No",style: TextStyle(color: Colors.white),),
                  color: App24Colors.redRestaurantThemeColor,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                ))
              ],
            )
          ],
        ),
      ),
    );
  }
}
