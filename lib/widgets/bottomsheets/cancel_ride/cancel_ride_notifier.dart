

import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/api/api_response_state2.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';


class CancelRide extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  CancelRide(this._apiRepository) : super(ResponseState2(isLoading: true));



  Future<void> getCancelRide({bool init = true, required Map body}) async {
    try {
      if (init) state = state.copyWith(isLoading: true);

      final cancelRide = await _apiRepository.fetchCancelRide(body: body);

      state = state.copyWith(
          response: cancelRide, isLoading: false, isError: false);
    } catch (e) {
      state = state.copyWith(
          errorMessage: e.toString(), isLoading: false, isError: true);
    }
  }
  Future<void> getCancelRideReasons({bool init = true}) async {
    try {
      if (init) state = state.copyWith(isLoading: true);

      final cancelRide = await _apiRepository.fetchRideCancelReasons();

      state = state.copyWith(
          response: cancelRide, isLoading: false, isError: false);
    } catch (e) {
      state = state.copyWith(
          errorMessage: e.toString(), isLoading: false, isError: true);
    }
  }
  Future<void> getRideSendRequest({bool init = true, required Map body}) async {
    try {
      if (init) state = state.copyWith(isLoading: true);

      final cancelRide = await _apiRepository.fetchRideSendRequest(body: body);

      state = state.copyWith(
          response: cancelRide, isLoading: false, isError: false);
    } catch (e) {
      state = state.copyWith(
          errorMessage: e.toString(), isLoading: false, isError: true);
    }
  }


}
