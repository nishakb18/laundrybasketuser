import 'package:app24_user_app/Laundry-app/views/widgets/normal_address_list_screen.dart';
import 'package:app24_user_app/widgets/bottomsheets/search_location_bottom_sheet.dart';
import 'package:app24_user_app/app24_user_icons.dart';
import 'package:app24_user_app/constants/color_coman.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/constants/global_constants.dart';
import 'package:app24_user_app/models/location_model.dart';
import 'package:app24_user_app/modules/delivery/shop_list/shop_list_model.dart';
import 'package:app24_user_app/modules/my_account/address/manage_address.dart';
import 'package:app24_user_app/modules/my_account/address/manage_address_loading.dart';
import 'package:app24_user_app/modules/my_account/address/manage_address_model.dart';
import 'package:app24_user_app/modules/restaurant/restaurant_cart_page.dart';
import 'package:app24_user_app/modules/shop_list/store_list_screen.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/utils/services/socket/socket_service.dart';
import 'package:app24_user_app/widgets/bottomsheets/addressConfirmation/addressConfirmationLoading.dart';
import 'package:app24_user_app/widgets/bottomsheets/search_location_bottom_sheet.dart';
import 'package:app24_user_app/widgets/common_main_button.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:app24_user_app/widgets/select_location.dart';
import 'package:app24_user_app/widgets/select_location_address.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class AddressConfirmationBottomSheet extends StatefulWidget {
  const AddressConfirmationBottomSheet(
      {super.key,
      this.orderContent,
      this.fromPage,
      this.map,
      this.shopResponseData,
      this.storeTypeIdFromAddList,
      this.addListText,
      this.addListFilePath,
      this.isImageSelectedAddList,
        this.isMainPage = false,
        this.mainPageFun});
  final Map? orderContent;
  final String? fromPage;
  final Map<dynamic, dynamic>? map;
  final ShopNamesModelResponseData? shopResponseData;
  final String? storeTypeIdFromAddList;
  final String? addListText;
  final String? addListFilePath;
  final bool? isImageSelectedAddList;
  final bool? isMainPage;
  final Function()? mainPageFun;

  @override
  _AddressConfirmationBottomSheetState createState() =>
      _AddressConfirmationBottomSheetState();
}

class _AddressConfirmationBottomSheetState
    extends State<AddressConfirmationBottomSheet> {
  String? delivery = "Enter your address";
  LocationModel? confirmedLocation;
  AddressResponseData? addressLocation;
  String? highPriorityAddress;

  // bool selectedAddress = false;
  AddressResponseData? selectedAddress;


  @override
  void initState() {
    SchedulerBinding.instance!.addPostFrameCallback((_) {
      context.read(addressNotifierProvider.notifier).getAddresses();
      // addressLocation =
    });
    super.initState();
  }

  loadData({bool init = true, required BuildContext context}) {
    return context.read(addressNotifierProvider.notifier).getAddresses();
  }

  showSelectLocationSheetForAddress(
      {required BuildContext context, bool initialCall = true, String? title}) {
    return showGeneralDialog(
        context: context,
        barrierDismissible: true,
        barrierLabel:
            MaterialLocalizations.of(context).modalBarrierDismissLabel,
        barrierColor: Colors.black54,
        transitionBuilder: (context, animation, secondaryAnimation, child) {
          var begin = Offset(0.0, 1.0);
          var end = Offset.zero;
          var curve = Curves.ease;

          var tween =
              Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

          return SlideTransition(
            position: animation.drive(tween),
            child: child,
          );
        },
        pageBuilder: (BuildContext context, _, __) =>
            SearchLocationBottomSheetPage(
              initialCall: initialCall,
              title: title,
            ));
  }

  currentLocation() {
    setState(() {
      LocationModel userLocation =
          context.read(homeNotifierProvider.notifier).currentLocation;
    });
  }

  LocationModel location() {
    return context.read(homeNotifierProvider.notifier).currentLocation;
  }

  late var addressList;
  AddressResponseData? selectedAddressLocation;

  selectLocation(String title) {
    showSelectLocationSheet(context: context, title: title).then((value) {
      if (value != null) {
        LocationModel model = value;
        setState(() {
          delivery = model.description;
          confirmedLocation = model;
        });
      }
    });
  }

  int? val = -1;
  bool value = false;

  List<Widget> getAddress(context, List<AddressResponseData> addressModel) {
    List<Widget> _widgetServices = [];

    _widgetServices.add(ListView.builder(
        reverse: true,
        shrinkWrap: true,
        // padding: EdgeInsets.all(10),
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (context, i) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              RadioListTile(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                activeColor: App24Colors.greenOrderEssentialThemeColor,
                value: addressModel[i].addressid!,
                groupValue: val,
                onChanged: (value) {
                  setState(() {
                    val = value as int?;
                    print("abcd" + addressModel[i].longitude.toString());
                    selectedAddressLocation = addressModel[i];
                  });
                },
                title: Text(
                  // "${_addressList[index].house_name}, ${_addressList[index].address}, ${_addressList[index].street}, ${_addressList[index].landmark} ",
                  addressModel[i].title ?? addressModel[i].flatNo!.toString(),
                  //      +  "," +
                  //        addressModel[i].mapAddress.toString()==''? "": addressModel[i].mapAddress.toString() +
                  //        /*  "," +
                  //        addressModel[i].street! +*/
                  // "," +
                  //        (addressModel[i].landmark ?? ""),// + addressModel[i].title.toString(),
                  style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: MediaQuery.of(context).size.width / 25),
                ),
                subtitle: Text(
                    addressModel[i].flatNo!.toString() +
                        "," +
                        addressModel[i].street!.toString(),
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: MediaQuery.of(context).size.width / 28)),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 15),
                child: Visibility(
                    visible: addressModel[i].addressid == val,
                    child: MaterialButton(
                      // padding: EdgeInsets.symmetric(horizontal: 10),
                      color: App24Colors.greenOrderEssentialThemeColor,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      onPressed: () {
                        // print(selectedAddressLocation!.priority);
                        if(widget.isMainPage == true) {
                        widget.mainPageFun!();
                        if (addressModel[i].latitude != null) {
                          AddressResponseData value =
                          addressModel[i];
                          print(value.addressType);
                          if (value.longitude != null) {
                            print("value.latitude! :: ${value.latitude}");
                            setState(() {
                              context.read(homeNotifierProvider.notifier).currentLocation = LocationModel(
                                latitude: value.latitude,
                                longitude: value.longitude,
                                street: value.street,
                                landmark: value.landmark,
                                description: value.flatNo! +
                                    " , " +
                                    value.landmark.toString() +
                                    " , " +
                                    value.street.toString(),
                                  main_text: value.title
                              );
                            });
                             context
                                .read(cartNotifierProvider
                                .notifier)
                                .getCartList(
                              storeID: null,
                                isLaundryBasketCard: true,
                                context: context,
                                fromLat: value.latitude!
                                    .toDouble(),
                                fromLng: value.longitude!
                                    .toDouble());
                            String address = value
                                .flatNo! +
                                " , " +
                                value.street.toString() +
                                " , " +
                                value.city.toString();
                            setState(() {
                              highPriorityAddress =
                                  address;
                              selectedAddress = value;
                            });
                          }
                          print("fadff lat:: ${value.latitude}  long::${value.longitude} ");
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                //routing to the shop list
                                StoreListScreen(
                                  lat: value.latitude!
                                      .toDouble(),
                                  long:  value.longitude!
                                      .toDouble(),
                                  collectionAddress: "${value.flatNo!},${value.street.toString()},${value.city.toString()}",

                                )

                              // CounterWidget()
                              // CartTextingcount()
                            ),
                          );
                        }
                        }
                        else{
                          Navigator.pop(context, selectedAddressLocation);
                        }
                      },
                      child: Icon(
                        Icons.arrow_forward_ios,
                        color: Colors.white,
                      ),
                      // Text(">>",style: TextStyle(color: Colors.white,fontSize: 25),),
                    )),
              ),
              Divider(),
            ],
          );
        },
        itemCount: addressModel.length));
    return _widgetServices;
  }

  // String getAddress(context, AddressResponseData address) {
  //   return address.flatNo! + address.mapAddress!;
  // }

  // onAddressConfirmation() {
  //   // print("eghf " + selectedAddressLocation!.flatNo.toString());
  //   Navigator.pop(context, selectedAddressLocation);
  // }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                         Text(
                          (widget.isMainPage == true)?"Select Pickup Address": "Select Address",
                          style: TextStyle(fontWeight: FontWeight.w600),
                        ),
                        MaterialButton(
                          onPressed: () async {
                            // Helpers().getCommonBottomSheet(context: context, content: SearchLocationBottomSheetPage(initialCall: true,title: GlobalConstants.labelDropLocation,));
                            Map value = await showSelectLocationSheetForAddress(
                                context: context,
                                title: GlobalConstants.labelFromCart);
                            if (value.isNotEmpty) {
                              print("popping working");
                              print(value);
                              if(widget.isMainPage == true) {
                                Future.delayed(Duration(milliseconds: 500), () {
                                  Navigator.pop(context, value);
                                  Navigator.push(context, MaterialPageRoute(
                                    builder: (context) => CheckOutHomeScreen(),));
                                });
                              } else {
                                Navigator.pop(context, value);
                              }
                            }
                            // print("printing checking whether address came to cart");
                            // print(addressFromSearch!["to_adrs"]);
                            setState(() {
                              highPriorityAddress = value["to_adrs"].toString();
                              // print("printing highPriorityAddress : "+highPriorityAddress.toString());
                              // print("printing highPriorityAddress : "+highPriorityAddress.toString());

                              AddressResponseData addressData =
                                  new AddressResponseData(
                                      latitude: value["to_lat"],
                                      longitude: value['to_lng']);
                              selectedAddress = addressData;
                              context
                                  .read(cartNotifierProvider.notifier)
                                  .getCartList(
                                      fromLat: value["to_lat"],
                                      fromLng: value["to_lng"],
                                      context: context);
                            });

                            // Navigator.push(
                            //     context,
                            //     MaterialPageRoute(
                            //         builder: (context) =>
                            //             SelectLocationAddressPage(
                            //               title: GlobalConstants
                            //                   .labelDropLocation,
                            //               isCurrentLocation:
                            //                   true,
                            //               fromManageAddress:
                            //                   "fromManageAddress",
                            //               fromCart:
                            //                   "true",
                            //             )));
                          },
                          // onPressed: () {
                          //   print("1" + widget.addListText.toString());
                          //   print("2" + widget.addListFilePath.toString());
                          //   print("3" + widget.isImageSelectedAddList.toString());
                          //   print("4" + widget.storeTypeIdFromAddList.toString());
                          //   print("5" + widget.map.toString());
                          //   print("6" + widget.shopResponseData.toString());
                          //   Navigator.pop(context);
                          //   print("map in address confirmation page address confirm "+widget.map.toString());
                          //   Navigator.push(
                          //     context,
                          //     MaterialPageRoute(
                          //         builder: (context) => SelectLocationAddressPage(
                          //           title: 'Your address Location',
                          //           fromManageAddress: "fromManageAddress",
                          //           isCurrentLocation: true,
                          //           fromCart: "true",
                          //           map: widget.map,
                          //           addListFilePath: widget.addListFilePath,
                          //           addListText: widget.addListText,
                          //           shopResponseData: widget.shopResponseData,
                          //           storeTypeIdFromAddList: widget.storeTypeIdFromAddList,
                          //           isImageSelectedAddList: widget.isImageSelectedAddList,
                          //         )
                          //       // AddUpdateAddressPage()
                          //     ),
                          //   );
                          //
                          //
                          //
                          //
                          //   Navigator.push(
                          //     context,
                          //     MaterialPageRoute(
                          //         builder: (context) => SelectLocationPage(
                          //           isSavedAddress: false,
                          //           title: 'Enter Location',
                          //           fromManageAddress: "fromManageAddress",
                          //           isCurrentLocation: true,
                          //           fromCart: "true",
                          //         )
                          //       // AddUpdateAddressPage()
                          //     ),
                          //   );
                          // },
                          child: Text(
                            "Add New",
                            style: TextStyle(color: Colors.white),
                          ),
                          color: App24Colors.greenOrderEssentialThemeColor,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                        )
                      ]),
                ),
                const SizedBox(
                  height: 10,
                ),
                Consumer(
                  builder: (context, watch, child) {
                    final state = watch(addressNotifierProvider);
                    if (state.isLoading) {
                      return AddressConfirmationLoading();
                    } else if (state.isError) {
                      return LoadingError(
                        onPressed: (res) {
                          loadData(init: true, context: res);
                        },
                        message: state.errorMessage.toString(),
                      );
                    } else {
                      addressList = state.response;
                      if (addressList!.isEmpty)
                        return Container(
                          child: Center(
                            child: Column(
                              children: [
                                // Icon(Icons.),
                                Text("No addresses added."),
                              ],
                            ),
                          ),
                        );
                      return Column(
                          children: getAddress(context, addressList!));
                    }
                  },
                ),
                if (val == -1)
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 25),
                    child: Text("Please select an address to proceed"),
                  )
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class SavedAddress {
  int? id;
  bool? isSelected = false;
  SavedAddress({this.isSelected, this.id});
}
