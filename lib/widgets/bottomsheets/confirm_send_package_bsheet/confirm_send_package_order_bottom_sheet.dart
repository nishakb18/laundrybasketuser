import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/modules/send_packages/vehicle_info_model.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/widgets/bottomsheets/confirm_send_package_bsheet/confirm_send_package_bsheet_loading.dart';
import 'package:app24_user_app/widgets/common_main_button.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class ConfirmSendPackageOrderBottomSheet extends StatefulWidget {
  final Map sendItemBody;

  const ConfirmSendPackageOrderBottomSheet(
      {Key? key, required this.sendItemBody})
      : super(key: key);

  @override
  _ConfirmSendPackageOrderBottomSheetState createState() =>
      _ConfirmSendPackageOrderBottomSheetState();
}

class _ConfirmSendPackageOrderBottomSheetState
    extends State<ConfirmSendPackageOrderBottomSheet> {
  DeliveryVehicleResponseData? selectedVehicleResponseData;
  bool isReturnRequired = false;
  bool useWallet = false;
  bool lockWallet = false;
  var walletBalance ;
  var total;

  @override
  void initState() {
    SchedulerBinding.instance!.addPostFrameCallback((_) {
      loadData(context: context);
      // walletBalance =  context
      //      .read(walletNotifierProvider.notifier).walletAmount;
      context.read(walletNotifierProvider.notifier).walletAmount.toString();
      context.read(walletNotifierProvider.notifier).getWallet();

      // walletBalance = context
      //     .read(homeNotifierProvider.notifier)
      //     .profile!
      //     .responseData!
      //     .walletBalance!;
      // print("wallet " + walletBalance.toString());
    });
    super.initState();
  }

  loadData({required BuildContext context, bool? init = true}) {
    context.read(selectVehicleNotifier.notifier).getVehicleInfo(
        context: context, body: widget.sendItemBody, init: init!);
  }

  List<Widget> buildVehicles(List<DeliveryVehicleResponseData>? results) {
    List<Widget> list = [];

    results!.forEach((element) {
      list.add(ListTile(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        onTap: () {
          setState(() {
            selectedVehicleResponseData = element;
          });
        },
        title: Text(
          element.deliveryMode ?? '',
          style: TextStyle(fontWeight: FontWeight.w600),
        ),
        trailing: Icon(
          selectedVehicleResponseData == element
              ? Icons.check_circle
              : Icons.radio_button_unchecked,
          color: App24Colors.brownSendPackageThemeColor,
        ),
      ));
    });
    return list;
  }

  onConfirmPressed() {
    if (selectedVehicleResponseData != null) {
      widget.sendItemBody['vehicle'] =
          selectedVehicleResponseData!.deliveryMode;
      widget.sendItemBody['is_return'] = isReturnRequired;
      widget.sendItemBody['wallet'] = useWallet ? '1' : '0';
      widget.sendItemBody['total'] = getTotalAmount();
      print(getTotalAmount());
      Navigator.pop(context, widget.sendItemBody);
    }
  }

  String getTotalAmount() {
    if (isReturnRequired)
      total = double.parse(selectedVehicleResponseData!.estimationCostWithReturn.toString());
    else
      total = double.parse(selectedVehicleResponseData!.estimationCost.toString());

    if (useWallet) {
      total =
          total - context.read(walletNotifierProvider.notifier).walletAmount;
      if (total < 0) total = 0;
    }

    return context.read(walletNotifierProvider.notifier).walletAmount < 0 && isReturnRequired
        ? selectedVehicleResponseData!.estimationCostWithReturn.toString() : context.read(walletNotifierProvider.notifier).walletAmount < 0 ? selectedVehicleResponseData!.estimationCost.toString()
        : total.toString();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: ProviderListener(
        provider: selectVehicleNotifier,
        onChange: (context, dynamic state) {
          if (!state.isLoading && state.response != []) {
            setState(() {
              selectedVehicleResponseData = state.response.responseData[0];
              walletBalance =
                  context.read(walletNotifierProvider.notifier).walletAmount;
              if (context.read(walletNotifierProvider.notifier).walletAmount <
                  0) {
                useWallet = true;
                lockWallet = true;
              }
            });
          }
        },
        child: Column(
          children: [
            Consumer(builder: (context, watch, child) {
              final state = watch(selectVehicleNotifier);

              if (state.isLoading) {
                return SendPackageBSheetLoading();
              } else if (state.isError) {
                if(state.errorMessage.toString().contains("Sorry, our service"))

                  return

                    Container(
                        height: 40,
                        child:
                        Column(
                          children: [
                            SizedBox(height: 10),
                            Text(state.errorMessage),
                          ],
                        ));
                else {
                  return LoadingError(
                    onPressed: (res) {
                      loadData(context: res);
                    },
                    message: state.errorMessage,
                  );
                }
              } else {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const SizedBox(height: 20,),
                    Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(17),
                          border: Border.all(color: Colors.grey.shade200),
                          //     boxShadow: [
                          //   BoxShadow(
                          //       spreadRadius: 3,
                          //       blurRadius: 3,
                          //       color: Colors.grey.shade300)
                          // ],
                          //   color: Colors.white
                        ),
                        child: Wrap(
                            children:
                            buildVehicles(state.response.responseData))),
                    const SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          padding: EdgeInsets.only(left: 15, right: 5),
                          child: Material(
                            color: Colors.transparent,
                            borderRadius: BorderRadius.circular(10),
                            child: InkWell(
                              borderRadius: BorderRadius.circular(10),
                              onTap: () {
                                setState(() {
                                  isReturnRequired = true;
                                });
                              },
                              child: Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                children: [
                                  // CheckboxListTile(
                                  //   title: Text("Require return",style: TextStyle(fontWeight: FontWeight.w500),),
                                  //     value: isReturnRequired,
                                  //     onChanged: (bool? value) {
                                  //   setState(() {
                                  //     isReturnRequired = value!;
                                  //   });
                                  // }),
                                  Text(
                                    "Require return",
                                    style:
                                    TextStyle(fontWeight: FontWeight.w500),
                                  ),
                                  Transform.scale(
                                      scale: 1.1,
                                      child: Checkbox(
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                            BorderRadius.circular(15)),
                                        checkColor: Colors.white,
                                        activeColor: App24Colors
                                            .brownSendPackageThemeColor,
                                        value: isReturnRequired,
                                        onChanged: (bool? value) {
                                          setState(() {
                                            isReturnRequired = value!;
                                          });
                                        },
                                      )),
                                ],
                              ),
                            ),
                          ),
                        ),
                        if (context
                            .read(walletNotifierProvider.notifier)
                            .walletAmount !=
                            0 &&
                            context
                                .read(walletNotifierProvider.notifier)
                                .walletAmount !=
                                "")
                          Expanded(
                            child: Container(
                              padding: EdgeInsets.only(left: 15, right: 5),
                              child: Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                    child: Text(
                                      "Use wallet (AED${context.read(walletNotifierProvider.notifier).walletAmount})",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500),
                                      overflow: TextOverflow.clip,
                                    ),
                                  ),
                                  Transform.scale(
                                      scale: 1.1,
                                      child: Checkbox(
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                            BorderRadius.circular(15)),
                                        checkColor: Colors.white,
                                        activeColor: App24Colors
                                            .brownSendPackageThemeColor,
                                        value: useWallet,
                                        onChanged: lockWallet
                                            ? null
                                            : (bool? value) {
                                          setState(() {
                                            useWallet = value!;
                                          });
                                        },
                                      )),
                                ],
                              ),
                            ),
                          ),
                      ],
                    ),
                    Divider(),
                    const SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Text(
                                "Total Distance :",
                                textAlign: TextAlign.end,
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),

                              const SizedBox(
                                width: 10,
                              ),
                              Text(
                                " ${selectedVehicleResponseData?.distance} Km",
                                textAlign: TextAlign.end,
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),
                              // MaterialButton(onPressed: (){print(walletBalance);},child: Text("abc"),),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Text(
                                "Total Amount :",
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              const SizedBox(
                                width: 5,
                              ),
                              Text(
                                "\u20B9 " + getTotalAmount(),
                                style: Theme.of(context).textTheme.titleSmall,
                              ),

                              // RichText(
                              //   text: TextSpan(
                              //     text: 'Total Amount : ',
                              //     style: Theme.of(context).textTheme.bodyText1,
                              //     children: <TextSpan>[
                              //       TextSpan(
                              //         text: isReturnRequired
                              //             ? selectedVehicleResponseData
                              //                 ?.estimationCostWithReturn
                              //                 .toString()
                              //             : selectedVehicleResponseData?.estimationCost
                              //                 .toString(),
                              //         style: Theme.of(context).textTheme.headline6,
                              //       ),
                              //     ],
                              //   ),
                              //   textAlign: TextAlign.end,
                              // ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    CommonButton(
                      onTap: onConfirmPressed,
                      bgColor: App24Colors.brownSendPackageThemeColor,
                      text: "Confirm",
                    )
                  ],
                );
              }
            }),
          ],
        ),
      ),
    );
  }
}