import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class SendPackageBSheetLoading extends StatefulWidget {
  @override
  _SendPackageBSheetLoadingState createState() => _SendPackageBSheetLoadingState();
}

class _SendPackageBSheetLoadingState extends State<SendPackageBSheetLoading> {
  Timer? _timer;
  int _start = 10; // time to popup still working window /// in seconds

  @override
  void initState() {
    startTimer();
    super.initState();
  }

  @override
  void dispose() {
    if (_timer != null) {
      _timer!.cancel();
    }
    super.dispose();
  }

  void startTimer() {
    if (_timer != null) {
      _timer!.cancel();
      _timer = null;
    } else {
      _timer = new Timer.periodic(const Duration(seconds: 1), (Timer timer) {
        setState(() {
          if (_start < 1) {
            _timer!.cancel();
          } else {
            _start = _start - 1;
          }
        });
      });
    }
  }


  buildStillLoadingWidget() {
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(15)),
      width: MediaQuery.of(context).size.width,
      child: ListTile(
        title: Text("Please wait..."),
        subtitle: Text(
            "The network seems to be too busy. We suggest you to wait for some more time."),
        leading: CircularProgressIndicator.adaptive(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        // width: MediaQuery.of(context).size.width,
        // padding: EdgeInsets.only(top: 10),
        //   height: MediaQuery.of(context).size.height/2.65,
          child: Stack(
            children: [
              Shimmer.fromColors(
                  baseColor: Colors.grey[300]!,
                  highlightColor: Colors.grey[100]!,
                  child: Container(
                    // margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              height: 15,
                              width: MediaQuery.of(context).size.width / 6,
                              // margin: EdgeInsets.only( top: 15),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  color: Colors.white,
                                  boxShadow: [
                                    BoxShadow(
                                      color: Color(0xffefefef),
                                      blurRadius: 20,
                                      spreadRadius: 3,
                                    ),
                                  ]),
                            ),
                            Container(
                              height: 20,
                              width: MediaQuery.of(context).size.width / 20,
                              // margin: EdgeInsets.only( top: 15),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50),
                                  color: Colors.white,
                                  boxShadow: [
                                    BoxShadow(
                                      color: Color(0xffefefef),
                                      blurRadius: 20,
                                      spreadRadius: 3,
                                    ),
                                  ]),
                            )
                          ],
                        ),
                        const SizedBox(height: 20,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              height: 15,
                              width: MediaQuery.of(context).size.width / 4,
                              // margin: EdgeInsets.only( top: 15),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  color: Colors.white,
                                  boxShadow: [
                                    BoxShadow(
                                      color: Color(0xffefefef),
                                      blurRadius: 20,
                                      spreadRadius: 3,
                                    ),
                                  ]),
                            ),
                            Container(
                              height: 20,
                              width: MediaQuery.of(context).size.width / 20,
                              // margin: EdgeInsets.only( top: 15),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50),
                                  color: Colors.white,
                                  boxShadow: [
                                    BoxShadow(
                                      color: Color(0xffefefef),
                                      blurRadius: 20,
                                      spreadRadius: 3,
                                    ),
                                  ]),
                            )
                          ],
                        ),
                        const SizedBox(height: 15,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  height: 10,
                                  width: MediaQuery.of(context).size.width / 4,
                                  // margin: EdgeInsets.only( top: 15),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                          color: Color(0xffefefef),
                                          blurRadius: 20,
                                          spreadRadius: 3,
                                        ),
                                      ]),
                                ),
                                const SizedBox(width: 5,),
                                Container(
                                  height: 20,
                                  width: MediaQuery.of(context).size.width / 20,
                                  // margin: EdgeInsets.only( top: 15),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(50),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                          color: Color(0xffefefef),
                                          blurRadius: 20,
                                          spreadRadius: 3,
                                        ),
                                      ]),
                                )
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  height: 10,
                                  width: MediaQuery.of(context).size.width / 4,
                                  // margin: EdgeInsets.only( top: 15),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                          color: Color(0xffefefef),
                                          blurRadius: 20,
                                          spreadRadius: 3,
                                        ),
                                      ]),
                                ),
                                const SizedBox(width: 5,),
                                Container(
                                  height: 20,
                                  width: MediaQuery.of(context).size.width / 20,
                                  // margin: EdgeInsets.only( top: 15),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(50),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                          color: Color(0xffefefef),
                                          blurRadius: 20,
                                          spreadRadius: 3,
                                        ),
                                      ]),
                                )
                              ],
                            )
                          ],
                        ),
                        const SizedBox(height: 20,),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Container(
                                  height: 10,
                                  width: MediaQuery.of(context).size.width / 5,
                                  // margin: EdgeInsets.only( top: 15),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                          color: Color(0xffefefef),
                                          blurRadius: 20,
                                          spreadRadius: 3,
                                        ),
                                      ]),
                                ),
                                const SizedBox(width: 5,),
                                Container(
                                  height: 15,
                                  width: MediaQuery.of(context).size.width / 25,
                                  // margin: EdgeInsets.only( top: 15),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(50),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                          color: Color(0xffefefef),
                                          blurRadius: 20,
                                          spreadRadius: 3,
                                        ),
                                      ]),
                                )
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Container(
                                  height: 10,
                                  width: MediaQuery.of(context).size.width / 5,
                                  // margin: EdgeInsets.only( top: 15),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                          color: Color(0xffefefef),
                                          blurRadius: 20,
                                          spreadRadius: 3,
                                        ),
                                      ]),
                                ),
                                const SizedBox(width: 5,),
                                Container(
                                  height: 15,
                                  width: MediaQuery.of(context).size.width / 25,
                                  // margin: EdgeInsets.only( top: 15),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(50),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                          color: Color(0xffefefef),
                                          blurRadius: 20,
                                          spreadRadius: 3,
                                        ),
                                      ]),
                                )
                              ],
                            ),
                          ],
                        ),
                        const SizedBox(height: 5,),
                        Container(
                          height: 50,
                          width: MediaQuery.of(context).size.width ,
                          // margin: EdgeInsets.only( top: 15),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: Color(0xffefefef),
                                  blurRadius: 20,
                                  spreadRadius: 3,
                                ),
                              ]),
                        )



                      ],
                    ),
                  )
              ),
              Positioned(
                left: 20,
                right: 20,
                top: 230,
                child: _start == 0 ? buildStillLoadingWidget() : Container(),
              )
            ],
          )),
    );
  }
}
