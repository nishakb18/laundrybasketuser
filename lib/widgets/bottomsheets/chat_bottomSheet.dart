import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:flutter/material.dart';

class ChatBottomSheet extends StatefulWidget {
  ChatBottomSheet({Key? key, this.chatScreenSize}) : super(key: key);
  final String? chatScreenSize;

  @override
  _ChatBottomSheetState createState() => _ChatBottomSheetState();
}

class _ChatBottomSheetState extends State<ChatBottomSheet> {
  List<ChatMessage> messages = [
    ChatMessage(messageContent: "Hey", messageType: "sender"),
    ChatMessage(
        messageContent: "Can you deliver the package on my doorstep?",
        messageType: "sender"),
    ChatMessage(messageContent: "Hey!", messageType: "receiver"),
    ChatMessage(
        messageContent: "Sure, no problem! Anything else I can help you with?",
        messageType: "receiver"),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        // automaticallyImplyLeading: false,
        leading: Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.arrow_back_ios,
              color: App24Colors.darkTextColor,
              size: MediaQuery.of(context).size.width / 25,
            ),
          ),
        ),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Image.asset(
              "assets/images/agent.png",
              width: MediaQuery.of(context).size.width / 6.5,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  "Ram Sharma",
                  style: TextStyle(
                      fontSize: MediaQuery.of(context).size.width / 26,
                      fontWeight: FontWeight.bold,
                      color: App24Colors.darkTextColor),
                  overflow: TextOverflow.ellipsis,
                ),
                Text(
                  "Laundry Basket Delivery agent",
                  style: TextStyle(
                      fontSize: MediaQuery.of(context).size.width / 29,
                      color: App24Colors.darkTextColor),
                  overflow: TextOverflow.ellipsis,
                )
              ],
            ),
          ],
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 10, top: 10, bottom: 10),
            child: Container(
              width: MediaQuery.of(context).size.width / 9,
              decoration:
                  BoxDecoration(borderRadius: BorderRadius.circular(50)),
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(50),
                  child: Material(
                    color: App24Colors.greenOrderEssentialThemeColor,
                    child: (InkWell(
                      onTap: () {},
                      child: Icon(
                        Icons.call,
                        color: Colors.white,
                        size: MediaQuery.of(context).size.width / 20,
                      ),
                    )),
                  )),
            ),
          )
        ],
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            height: widget.chatScreenSize == "fromHomeScreen"
                ? MediaQuery.of(context).size.height
                : MediaQuery.of(context).size.height * 0.77,
            child: Column(
              children: <Widget>[
                Expanded(
                  child: Column(children: [
                    SingleChildScrollView(
                      child: ListView.builder(
                        // scrollDirection: Axis.vertical,
                        itemCount: messages.length,
                        shrinkWrap: true,
                        // padding: EdgeInsets.only( bottom: 10),
                        physics: NeverScrollableScrollPhysics(),
                        itemBuilder: (context, index) {
                          return Container(
                            padding: EdgeInsets.only(
                                left: 14, right: 14, top: 10, bottom: 0),
                            child: Align(
                              alignment:
                                  (messages[index].messageType == "receiver"
                                      ? Alignment.topLeft
                                      : Alignment.topRight),
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius:
                                      (messages[index].messageType == "receiver"
                                          ? BorderRadius.only(
                                              topRight: Radius.circular(30),
                                              topLeft: Radius.circular(30),
                                              bottomRight: Radius.circular(30))
                                          : BorderRadius.only(
                                              topRight: Radius.circular(30),
                                              topLeft: Radius.circular(30),
                                              bottomLeft: Radius.circular(30))),
                                  color:
                                      (messages[index].messageType == "receiver"
                                          ? App24Colors.greenOrderEssentialThemeColor
                                          : Color(0xffE9FFEB)),
                                ),
                                padding: EdgeInsets.all(10),
                                child: Text(
                                  messages[index].messageContent,
                                  style:
                                      (messages[index].messageType == "receiver"
                                          ? TextStyle(
                                              fontSize: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  29,
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold)
                                          : TextStyle(
                                              fontSize: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  29,
                                              color: App24Colors.greenOrderEssentialThemeColor,
                                              fontWeight: FontWeight.bold)),
                                ),
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  ]),
                ),
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(40),
            color: Color(0xffE9FFEB),
          ),
          padding: EdgeInsets.only(left: 10, bottom: 0, top: 0, right: 0),
          height: MediaQuery.of(context).size.height / 15,
          width: double.infinity,
          child: Row(
            children: <Widget>[
              Material(
                color: App24Colors.greenOrderEssentialThemeColor,
                borderRadius: BorderRadius.circular(50),
                child: InkWell(
                  borderRadius: BorderRadius.circular(50),
                  onTap: () {},
                  child: Container(
                    padding: EdgeInsets.all(5),
                    // height: MediaQuery.of(context).size.width / 12,
                    // width: MediaQuery.of(context).size.height / 20,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                    ),
                    child: Icon(
                      Icons.add,
                      color: Colors.white,
                      size: MediaQuery.of(context).size.width / 22,
                    ),
                  ),
                ),
              ),
              const SizedBox(
                width: 15,
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: TextField(
                    decoration: InputDecoration(
                        hintText: "Type message...",
                        hintStyle: TextStyle(
                            color: Colors.black54,
                            fontSize: MediaQuery.of(context).size.width / 29),
                        border: InputBorder.none),
                  ),
                ),
              ),
              const SizedBox(
                width: 15,
              ),
              Material(
                color: App24Colors.greenOrderEssentialThemeColor,
                borderRadius: BorderRadius.circular(50),
                child: InkWell(
                  borderRadius: BorderRadius.circular(50),
                  onTap: () {},
                  child: Container(
                      padding: EdgeInsets.all(7),
                      // height:
                      //     MediaQuery.of(context).size.width / 11,
                      // width:
                      //     MediaQuery.of(context).size.height / 18,
                      decoration: BoxDecoration(shape: BoxShape.circle),
                      child: Image.asset(
                        App24UserAppImages.chatSendIcon,
                        width: MediaQuery.of(context).size.width / 29,
                        // height: MediaQuery.of(context).size.height / 45,
                      )),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ChatMessage {
  String messageContent;
  String messageType;

  ChatMessage({required this.messageContent, required this.messageType});
}
