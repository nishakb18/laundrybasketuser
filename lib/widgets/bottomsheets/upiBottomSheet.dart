import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:flutter/material.dart';

class UpiBottomSheet extends StatefulWidget {
  @override
  _UpiBottomSheetState createState() => _UpiBottomSheetState();
}

class _UpiBottomSheetState extends State<UpiBottomSheet> {
  String paymentOption = "upi";

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: MediaQuery.of(context).size.height * 0.78,
      child: Padding(
        padding: const EdgeInsets.all(30.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(child: Container(width:100,height:5,decoration: BoxDecoration(color: Colors.grey[300],borderRadius: BorderRadius.circular(10)),)),
            const SizedBox(height: 10,),
            Text(
              "Choose Payment Method",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 30,
            ),
            Stack(children: [
              Padding(
                padding: const EdgeInsets.only(top: 15, right: 15),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(
                        width: 2.0,
                        color: Color(0xfff2f2f2),
                        style: BorderStyle.solid),
                  ),
                  child: Material(
                    child: ListTile(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      onTap: () {
                        setState(() {
                          paymentOption = "upi";
                        });
                      },
                      title: Padding(
                        padding: const EdgeInsets.all(40),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              children: [
                                Row(
                                  children: [
                                    Image.asset(
                                      App24UserAppImages.upi,
                                      width: 110,
                                    ),
                                  ],
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Image.asset(
                                      App24UserAppImages.wallet,
                                      width: 30,
                                    ),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    Image.asset(
                                      App24UserAppImages.netBanking,
                                      width: 30,
                                    ),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    Image.asset(
                                      App24UserAppImages.bSheetDebitCard,
                                      width: 30,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            Icon(
                              paymentOption == "upi"
                                  ? Icons.check_circle
                                  : Icons.radio_button_unchecked,
                              color: App24Colors.darkTextColor,
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Positioned(
                left: 255,
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: App24Colors.greenOrderEssentialThemeColor),
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                  child: Text(
                    "Free",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 17),
                  ),
                ),
              ),
            ]),
            const SizedBox(
              height: 40,
            ),
            Stack(children: [
              Padding(
                padding: const EdgeInsets.only(top: 15, right: 15),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(
                        width: 2.0,
                        color: Color(0xfff2f2f2),
                        style: BorderStyle.solid),
                  ),
                  child: Material(
                    child: ListTile(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      onTap: () {
                        setState(() {
                          paymentOption = "razor";
                        });
                      },
                      title: Padding(
                        padding: const EdgeInsets.all(40),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Flexible(
                              child: Column(
                                children: [
                                  Row(
                                    children: [
                                      Image.asset(
                                        App24UserAppImages.razorPay,
                                        width: 170,
                                      ),
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Image.asset(
                                        App24UserAppImages.gPay,
                                        width: 30,
                                      ),
                                      const SizedBox(
                                        width: 10,
                                      ),
                                      Image.asset(
                                        App24UserAppImages.bSheetPaytm,
                                        width: 30,
                                      ),
                                      const SizedBox(
                                        width: 10,
                                      ),
                                      Image.asset(
                                        App24UserAppImages.phonePay,
                                        width: 30,
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                            Icon(
                              paymentOption == "razor"
                                  ? Icons.check_circle
                                  : Icons.radio_button_unchecked,
                              color: App24Colors.darkTextColor,
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Positioned(
                left: 175,
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Color(0xffff5d44)),
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                  child: Text(
                    "Charges apply",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 17),
                  ),
                ),
              )
            ]),
            const SizedBox(
              height: 30,
            ),
            ElevatedButton(
              onPressed: () {},
              child: Text(
                "Proceed",
                style: TextStyle(fontSize: 20),
              ),
            )
          ],
        ),
      ),
    );
  }
}
