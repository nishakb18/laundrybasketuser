import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/modules/ride/ride_vehicle_model.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class VehicleDetailsBottomSheet extends StatelessWidget {
  final VehicleServices vehicleDetail;

  VehicleDetailsBottomSheet({required this.vehicleDetail});

  @override
  Widget build(BuildContext context) {
    return new Stack(
      children: <Widget>[
        Container(
            margin: EdgeInsets.only(top: 50),
            padding: EdgeInsets.only(
              top: 70,
              right: 20,
              left: 20,
            ),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(30),
                    topLeft: Radius.circular(30)),
                color: Colors.white),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [getTitle(vehicleDetail.vehicleName)],
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  getSubTitle("Details", context),
                  const SizedBox(
                    height: 5,
                  ),
                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Theme.of(context).cardColor),
                    padding: EdgeInsets.all(15),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Text(
                              "Base Fare",
                              style: TextStyle(fontWeight: FontWeight.w500),
                            ),
                            const SizedBox(
                              width: 15,
                            ),
                            Expanded(
                              child: Text(
                                "\u20B9${vehicleDetail.priceDetails!.price}",
                                textAlign: TextAlign.end,
                                style: TextStyle(fontWeight: FontWeight.w500),
                              ),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Row(
                          children: <Widget>[
                            Text(
                              "Distance Fare",
                              style: TextStyle(fontWeight: FontWeight.w500),
                            ),
                            const SizedBox(
                              width: 15,
                            ),
                            Expanded(
                              child: Text(
                                "\u20B9${vehicleDetail.priceDetails!.fixed}/km",
                                textAlign: TextAlign.end,
                                style: TextStyle(fontWeight: FontWeight.w500),
                              ),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Row(
                          children: <Widget>[
                            Text(
                              "Time Fare",
                              style: TextStyle(fontWeight: FontWeight.w500),
                            ),
                            const SizedBox(
                              width: 15,
                            ),
                            Expanded(
                              child: Text(
                                "\u20B9${vehicleDetail.priceDetails!.minute}/min",
                                textAlign: TextAlign.end,
                                style: TextStyle(fontWeight: FontWeight.w500),
                              ),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Row(
                          children: <Widget>[
                            Text(
                              "Capacity",
                              style: TextStyle(fontWeight: FontWeight.w500),
                            ),
                            const SizedBox(
                              width: 15,
                            ),
                            Expanded(
                              child: Text(
                                "${vehicleDetail.capacity} nos",
                                textAlign: TextAlign.end,
                                style: TextStyle(fontWeight: FontWeight.w500),
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  MaterialButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      color: App24Colors.darkTextColor,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      child: Padding(
                        padding:
                            EdgeInsets.symmetric(vertical: 15, horizontal: 20),
                        child: Text(
                          "DONE",
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                      )),
                  const SizedBox(
                    height: 10,
                  ),
                ])),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        color: Theme.of(context)
                            .primaryColorLight
                            .withOpacity(0.2),
                        blurRadius: 1,
                        spreadRadius: 1,
                        offset: Offset(2, 2)),
                  ],
                ),
                child: Material(
                  color: Colors.transparent,
                  child: InkWell(
                      borderRadius: BorderRadius.circular(8),
                      onTap: () {},
                      child: Container(
                          width: MediaQuery.of(context).size.width / 2.9,
                          height: 100,
                          padding: EdgeInsets.all(10),
                          child: Hero(
                            tag: vehicleDetail.id!,
                            child: CachedNetworkImage(
                              imageUrl: vehicleDetail.vehicleImage ?? '',
                              fit: BoxFit.contain,
                              color: Colors.black,
                              placeholder: (context, url) => Image.asset(
                                App24UserAppImages.placeHolderImage,
                                fit: BoxFit.contain,
                              ),
                              errorWidget: (context, url, error) => Image.asset(
                                App24UserAppImages.placeHolderImage,
                                fit: BoxFit.cover,
                              ),
                            ),
                          ))),
                )),
          ],
        )
      ],
    );
  }
}
