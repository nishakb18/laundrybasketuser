import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/models/home_delivery_send_order_detail.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/widgets/common_main_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_riverpod/src/provider.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';

class RetryOrCancel extends StatefulWidget {
  const RetryOrCancel({Key? key, required this.detailResponseData,}) : super(key: key);
  final HomeDeliveryOrderDetailResponseData detailResponseData;
  // final Map retryContentBody;

  @override
  _RetryOrCancelState createState() => _RetryOrCancelState();
}

class _RetryOrCancelState extends State<RetryOrCancel> {

  late Razorpay? _razorPay;

  @override
  void initState() {
    SchedulerBinding.instance!.addPostFrameCallback((_) {
      total = double.parse(widget.detailResponseData.totalPayable!);
      // loadData(context: context);
    });
    // _initRazorPay();
    super.initState();
  }


  onConfirmPressed() async {
    // if (total != 0) {
      Map checkOutOptions = {
        'description': 'homedelivery${widget.detailResponseData.id}',
        'amount': total,
        "notes": {
          "store_invoice_id": widget.detailResponseData.bookingId,
          "type": "homedelivery",
          "Store_order_id": widget.detailResponseData.id
        }
      };
      try {
        _razorPay!.open(await getRazorPayOptions(
            context: context, values: checkOutOptions));
      } catch (e) {
        showLog(e.toString());
      }
    // } else {
    //   if (useWallet!) {
    //     payWithWallet();
    //   }
    // }
  }



  late double? total;
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        // padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const SizedBox(height: 20,),
            Center(child: Text("Your payment failed. Retry?")),
            const SizedBox(height: 20,),
            CommonButton(
              onTap: (){
                onConfirmPressed();
              },
              bgColor: App24Colors.darkTextColor,
              text: "Retry",
            )
          ],
        ),
      ),
    );
  }
}
