import 'dart:io';

import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/modules/authentication/login/login.dart';
import 'package:app24_user_app/utils/services/database/address_database.dart';
import 'package:app24_user_app/utils/services/database/database_service.dart';
import 'package:app24_user_app/utils/services/database/global_search_database.dart';
import 'package:app24_user_app/utils/services/database/history_database.dart';
import 'package:app24_user_app/utils/services/database/location_database.dart';
import 'package:app24_user_app/widgets/restart_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart' show join;

class LogoutBottomSheet extends StatefulWidget {
  const LogoutBottomSheet({Key? key}) : super(key: key);

  @override
  _LogoutBottomSheetState createState() => _LogoutBottomSheetState();
}

class _LogoutBottomSheetState extends State<LogoutBottomSheet> {
  final _databaseName = "App24UserAppDatabase.db";
  Database? dbs;
  // late Database database;



  _deleteDatabase() async {

    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    // await dbs!.close();
    return await deleteDatabase(path);
    // print("agandev  del");
  }
  // Future<void> deleteDB(Database db) async {
  //   try{
  //
  //     Directory documentsDirectory = await getApplicationDocumentsDirectory();
  //     String path = join(documentsDirectory.path, _databaseName);
  //
  //     print('deleting db');
  //     db= null;
  //     deleteDatabase(path);
  //   }catch( e){
  //     print(e.toString());
  //   }
  //
  // //   print('db is deleted');
  // }


  onLogoutPressed() async {
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(
            builder: (context) => LogInPage()
        ),
        ModalRoute.withName("/Home")
    );
    LocationDatabase locationDatabase =
    LocationDatabase(DatabaseService.instance);
    AddressDatabase addressDatabase =
    AddressDatabase(DatabaseService.instance);
    GlobalSearchDataBase globalSearchDataBase=
        GlobalSearchDataBase(DatabaseService.instance);
    HistoryDatabase historyDatabase=
    HistoryDatabase(DatabaseService.instance);

    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
    await addressDatabase.deleteUserAddresses();
    await locationDatabase.deleteLocations();
    await historyDatabase.deleteHistory();
    await globalSearchDataBase.deleteGlobalSearchRslt();
    // await dbs!.rawDelete("DROP TABLE locations");
    // _deleteDatabase();
    // Future<void> deleteDB() async {
    //   try{
    //     Directory documentsDirectory = await getApplicationDocumentsDirectory();
    //         String path = join(documentsDirectory.path, _databaseName);
    //     print('deleting db');
    //     dbs=null;
    //     deleteDatabase(path);
    //   }catch( e){
    //     print(e.toString());
    //   }
    //
    //   print('db is deleted');
    // }
    // deleteDB();


    RestartWidget.restartApp(context);

    // Navigator.of(context)
    //     .pushAndRemoveUntil(
    //     MaterialPageRoute(builder: (context) => LogInPage()),
    //         (Route<dynamic> route) => false);



    // Navigator.push(context, MaterialPageRoute(builder: (context) => LogInPage()));
  }
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
        child: Column(
          children: [
            Text("Are you sure you want to logout?",style: TextStyle(fontWeight: FontWeight.w600),),
            const SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: MaterialButton(
                    padding: EdgeInsets.symmetric(vertical: 15),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    color: App24Colors.redRestaurantThemeColor,
                    onPressed: () {
                      onLogoutPressed();
                    },
                    child: Text(
                      "Logout",
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          color: Colors.white,
                          fontSize: 15),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 20,
                ),
                Expanded(
                  child: MaterialButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    padding: EdgeInsets.symmetric(vertical: 15),
                    color: App24Colors.greenOrderEssentialThemeColor,
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text("Cancel",                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        color: Colors.white,
                        fontSize: 15)),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
