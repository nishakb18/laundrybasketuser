import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/widgets/common_main_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class RateOrder extends StatefulWidget {
  const RateOrder({Key? key,}) : super(key: key);


  @override
  _RateOrderState createState() => _RateOrderState();
}

class _RateOrderState extends State<RateOrder> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
        child: Column(
          // crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Center(
                child: Container(
                  width: 100,
                  height: 5,
                  decoration: BoxDecoration(
                      color: Colors.grey[300],
                      borderRadius: BorderRadius.circular(10)),
                )),
            const SizedBox(
              height: 10,
            ),
            // Center(
            //   child: Text(
            //     "Your opinion matters to us!",
            //     style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            //   ),
            // ),

            const SizedBox(
              height: 10,
            ),
            ClipRRect(
              borderRadius: BorderRadius.circular(50),
              child: Image.asset(
                App24UserAppImages.rateOrderImage,
                height:200,
                width: 200,
                fit: BoxFit.cover,
              ),
            ),
            Center(
              child: Text(
                "DROP a review",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
              ),
            ),
            Center(
              child: Text(
                "Your opinion matters to us!",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
              ),
            ),
            const SizedBox(height: 10,),
            Center(
              child: RatingBar.builder(
                itemSize: 45,
                initialRating: 2,
                direction: Axis.horizontal,
                allowHalfRating: true,
                itemCount: 5,
                itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                itemBuilder: (context, _) => Icon(
                  Icons.star,
                  color: Colors.amber,
                ),
                onRatingUpdate: (rating) {
                  print(rating);
                },
                ignoreGestures: true,
              ),
            ),
            const SizedBox(height: 10,),
            // TextField(
            //   maxLines: 2,
            //   decoration: InputDecoration(
            //       border: OutlineInputBorder(
            //         borderRadius: BorderRadius.circular(20),
            //         borderSide: BorderSide(
            //           width: 0,
            //           style: BorderStyle.none,
            //         ),
            //       ),
            //       errorBorder: InputBorder.none,
            //       disabledBorder: InputBorder.none,
            //
            //       filled: true,
            //       fillColor: Colors.grey[300]!,
            //       focusColor:  Colors.grey[300]!,
            //       hintText: "Type you review"
            //   ),
            //
            // ),
            const SizedBox(
              height: 15,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                CommonButton(
                  onTap: () {

                  },
                  bgColor: App24Colors.greenOrderEssentialThemeColor,
                  text: "Rate",
                ),
                const SizedBox(height: 10,),
                TextButton(
                    onPressed: () {},
                    style: TextButton.styleFrom(shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15),
                    ),padding: EdgeInsets.symmetric(vertical: 15)),
                    child: Text(
                      "Maybe later",
                      style: TextStyle(color: App24Colors.darkTextColor),
                    ))
              ],
            )
          ],
        ),
      ),
    );
  }
}
