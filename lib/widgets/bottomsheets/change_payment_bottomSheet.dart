

import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/widgets/common_main_button.dart';
import 'package:flutter/material.dart';

class ChangePaymentBottomSheet extends StatefulWidget {
  const ChangePaymentBottomSheet({Key? key}) : super(key: key);

  @override
  _ChangePaymentBottomSheetState createState() => _ChangePaymentBottomSheetState();
}

class _ChangePaymentBottomSheetState extends State<ChangePaymentBottomSheet> {

  String paymentMode = "cash";
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20,vertical: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Material(
              child: ListTile(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                onTap: () {
                  setState(() {
                    paymentMode = "cash";
                  });
                },
                leading: Icon(
                  paymentMode == "cash"
                      ? Icons.check_circle
                      : Icons.radio_button_unchecked,
                  color: App24Colors.darkTextColor,
                ),
                title: Text("Cash"),
              ),
            ),
            Material(
              child: ListTile(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                onTap: () {
                  setState(() {
                    paymentMode = "card";
                  });
                },
                leading: Icon(
                  paymentMode == "card"
                      ? Icons.check_circle
                      : Icons.radio_button_unchecked,
                  color: App24Colors.darkTextColor,
                ),
                title: Text("Card"),
              ),
            ),
            Material(
              child: ListTile(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                onTap: () {
                  setState(() {
                    paymentMode = "upi";
                  });
                },
                leading: Icon(
                  paymentMode == "upi"
                      ? Icons.check_circle
                      : Icons.radio_button_unchecked,
                  color: App24Colors.darkTextColor,
                ),
                title: Text("UPI"),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            CommonButton(
              onTap: (){},
              bgColor: App24Colors.yellowRideThemeColor,
              text: "Select",
            )
            // MaterialButton(
            //   onPressed: () {},
            //   padding: EdgeInsets.symmetric( vertical: 15),
            //   shape: RoundedRectangleBorder(
            //       borderRadius: BorderRadius.circular(15)),
            //   color: App24Colors.yellowRideThemeColor,
            //   child: Text(
            //     "Select",
            //     style: TextStyle(color: Colors.white),
            //   ),
            // )
          ],
        ),
      ),
    );
  }
}
