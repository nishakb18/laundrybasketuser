import 'dart:async';

import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/constants/shared_preferences_path.dart';
import 'package:app24_user_app/helpers.dart';
import 'package:app24_user_app/models/check_wallet_balance_model.dart';
import 'package:app24_user_app/models/home_delivery_send_order_detail.dart';
import 'package:app24_user_app/modules/home.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/utils/services/socket/socket_service.dart';
import 'package:app24_user_app/widgets/bottomsheets/make_payment_bottomsheet/make_payment_bottom_sheet_loading.dart';
import 'package:app24_user_app/widgets/bottomsheets/order_success_bottomSheet.dart';
import 'package:app24_user_app/widgets/bottomsheets/retry_cancel_bottomSheet.dart';
import 'package:app24_user_app/widgets/common_main_button.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:app24_user_app/widgets/payment_waiting_screen.dart';
import 'package:app24_user_app/widgets/track_order.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_ringtone_player/flutter_ringtone_player.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MakePaymentBottomSheet extends StatefulWidget {
  final HomeDeliveryOrderDetailResponseData detailResponseData;

  const MakePaymentBottomSheet({super.key, required this.detailResponseData});

  @override
  _MakePaymentBottomSheetState createState() => _MakePaymentBottomSheetState();
}

class _MakePaymentBottomSheetState extends State<MakePaymentBottomSheet> {
  bool? useWallet = false;
  bool? lockWallet = false;
  double? walletBalance = 0.0;
  late final SocketService _socketService;
  CheckWalletBalanceModel? orderStatus;

  double? usableWalletAmount = 0.0;

  late double? total;
  late int? orderId;

  late Razorpay? _razorPay;
  late FlutterTts flutterTts = FlutterTts();
  //flutterTts
  Timer? minuteTimer;
  Timer? secondsTimer;
  @override
  void initState() {
    initNotification();
    SchedulerBinding.instance!.addPostFrameCallback((_) {
      total =
          double.parse(widget.detailResponseData.estimationCost!.toString());
      orderId = widget.detailResponseData.id;

      loadData(context: context);
    });
    _initRazorPay();
    super.initState();
  }

  @override
  void dispose() {
    _razorPay!.clear();
    super.dispose();
  }

  _initRazorPay() {
    _razorPay = new Razorpay();

    _razorPay!.on(Razorpay.EVENT_PAYMENT_SUCCESS, handlerPaymentSuccess);
    _razorPay!.on(Razorpay.EVENT_PAYMENT_ERROR, handlerErrorFailure);
    _razorPay!.on(Razorpay.EVENT_EXTERNAL_WALLET, handlerExternalWallet);
    return _razorPay;
  }

  void handlerPaymentSuccess(PaymentSuccessResponse response) async {
    print("handler success payment razor");
    // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomePage()));

    var prefs = await SharedPreferences.getInstance();
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => PaymentWaiting(
                  orderID: prefs.getInt(SharedPreferencesPath.lastOrderId)!,
                  orderType:
                      prefs.getString(SharedPreferencesPath.lastOrderType)!,
                )));

    // final prefs = await SharedPreferences.getInstance();
    // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => PaymentWaiting(orderType: prefs.getString(SharedPreferencesPath.lastOrderType).toString(),orderID: prefs.getInt(SharedPreferencesPath.lastOrderId)!,)));
    print("Payment successful");
    // if (useWallet!) {
    //   payWithWallet();
    // } else {
    //   Navigator.pop(context);
    // }
  }

  void handlerErrorFailure(PaymentFailureResponse response) {
    // context.read(orderDetailsNotifierProvider.notifier).checkOngoingOrders();
    //  _socketService.listenSocket();
    print("Payment failed");
  }

  void handlerExternalWallet(ExternalWalletResponse response) {
    // context.read(orderDetailsNotifierProvider.notifier).checkOngoingOrders();
    // _socketService.listenSocket();
    print("Payment external");
  }

  loadData({required BuildContext context, bool? init = true}) {
    context.read(walletBalanceNotifierProvider.notifier).getWalletBalance(
        amountToBePaid: total.toString(), init: init!, orderId: orderId!);
  }

  onCancelPressed() {
    minuteTimer?.cancel();
    secondsTimer?.cancel();
    // FlutterRingtonePlayer.stop();

    Helpers().getCommonAlertDialogBox(
        content: AlertDialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
          title: Center(child: Text("Cancel")),
          content: SingleChildScrollView(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text("Are you sure you want to cancel your order?"),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: MaterialButton(
                            onPressed: () {
                              Map body = {
                                "order_id": widget.detailResponseData.id,
                                "cancelled_by": "USER",
                                "cancel_reason": "",
                              };
                              Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => HomePage()));
                              // Navigator.pop(context,"reload");

                              context
                                  .read(cancelOrderNotifier.notifier)
                                  .cancelOrder(body: body, context: context);
                            },
                            child: Text("Yes",
                                style: TextStyle(color: Colors.white)),
                            color: App24Colors.redRestaurantThemeColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10))),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: MaterialButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text(
                            "No",
                            style: TextStyle(color: Colors.white),
                          ),
                          color: App24Colors.greenOrderEssentialThemeColor,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                        ),
                      ),
                    ],
                  )
                ]),
          ),
        ),
        context: context);

    // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomePage()));
  }

  onConfirmPressed() async {
    print("whether wallet checked: " + useWallet.toString());
    print("description of order" + widget.detailResponseData.id.toString());
    // Navigator.pop(context);
    minuteTimer?.cancel();
    secondsTimer?.cancel();
    // FlutterRingtonePlayer.stop();
    // Navigator.pop(context);
    if (total != 0) {
      if (useWallet == true) {
        print("entered useWallet only condition");
        await payWithWallets();
        // Map checkOutOptions = {
        //   'description': 'homedelivery${widget.detailResponseData.id}',
        //   'amount': total,
        //   "notes": {
        //     "store_invoice_id": widget.detailResponseData.bookingId,
        //     "type": "homedelivery",
        //     "Store_order_id": widget.detailResponseData.id
        //   }
        // };
        // try {
        //   _razorPay!.open(await getRazorPayOptions(
        //       context: context, values: checkOutOptions));
        // } catch (e) {
        //   showLog(e.toString());
        // }
      } else {
        print("entered else condition");
        /*   print("razor pay"+widget.detailResponseData.id.toString());
      print("razor pay"+total.toString());
      print("razor pay"+widget.detailResponseData.bookingId.toString());
    */
        Map checkOutOptions = {
          'description': 'homedelivery${widget.detailResponseData.id}',
          'amount': total,
          "notes": {
            "store_invoice_id": widget.detailResponseData.bookingId,
            "type": "homedelivery",
            "Store_order_id": widget.detailResponseData.id
          }
        };
        try {
          _razorPay!.open(await getRazorPayOptions(
              context: context, values: checkOutOptions));
        } catch (e) {
          showLog(e.toString());
        }
      }
    } else {
      if (useWallet!) {
        payWithWallet();
      }
    }
  }

  payWithWallet() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print("entered payWithWallet condition ");
    print(usableWalletAmount);
    // Navigator.pop(context);
    Map body = {
      'wallet_amount': usableWalletAmount,
      'id': widget.detailResponseData.id
    };

    context
        .read(payWithWalletNotifierProvider.notifier)
        .payWithWallet(body: body);
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => PaymentWaiting(
                  orderID:
                      prefs.getInt(SharedPreferencesPath.lastOrderId)!.toInt(),
                  orderType: prefs
                      .getString(SharedPreferencesPath.lastOrderType)
                      .toString(),
                )));
    // Helpers().getCommonBottomSheet(context: context, content: OrderSuccess(orderType: prefs.getString(SharedPreferencesPath.lastOrderType),orderID: prefs.getInt(SharedPreferencesPath.lastOrderId),/*orderType: "buy",orderID: responseData.id,*/));
  }

  payWithWallets() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print("entered payWithWallet condition ");
    print(usableWalletAmount);
    // Navigator.pop(context);
    Map body = {
      'wallet_amount': usableWalletAmount,
      'id': widget.detailResponseData.id
    };

    await context
        .read(payWithWalletNotifierProvider.notifier)
        .payWithWallet(body: body);

    Map checkOutOptions = {
      'description': 'homedelivery${widget.detailResponseData.id}',
      'amount': total,
      "notes": {
        "store_invoice_id": widget.detailResponseData.bookingId,
        "type": "homedelivery",
        "Store_order_id": widget.detailResponseData.id
      }
    };
    try {
      _razorPay!.open(
          await getRazorPayOptions(context: context, values: checkOutOptions));
    } catch (e) {
      showLog(e.toString());
    }

    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => PaymentWaiting(
                  orderID: prefs.getInt(SharedPreferencesPath.lastOrderId)!,
                  orderType:
                      prefs.getString(SharedPreferencesPath.lastOrderType)!,
                )));
    // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => TrackOrder(id: prefs.getInt(SharedPreferencesPath.lastOrderId).toString(),orderType: prefs.getString(SharedPreferencesPath.lastOrderType).toString(),)));
    // Helpers().getCommonBottomSheet(context: context, content: OrderSuccess(orderType: prefs.getString(SharedPreferencesPath.lastOrderType),orderID: prefs.getInt(SharedPreferencesPath.lastOrderId),/*orderType: "buy",orderID: responseData.id,*/));
  }

  String getTotalAmount() {
    if (useWallet!) {
      if (walletBalance! > 0) total = total! - walletBalance!;
      if (total! < 0) total = 0;
    } else {
      total = total =
          double.parse(widget.detailResponseData.estimationCost.toString());
    }

    return (walletBalance! < 0
        ? widget.detailResponseData.estimationCost.toString()
        : total.toString());
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return Future.value(false);
      },
      child: SingleChildScrollView(
        child: ProviderListener(
          provider: payWithWalletNotifierProvider,
          onChange: (context, dynamic state) {
            if (state.isLoading!) {
              showProgress(context);
              //showMessage(context, "loading");
            } else if (state.isError!) {
              Navigator.pop(context);
              showMessage(context, state.errorMessage!, true, true);
            } else {
              Navigator.pop(context);
              Navigator.pop(context);
              //checkOut(state.response.responseData);
              //showMessage(context, "Order Created");
            }
          },
          child: ProviderListener(
            provider: walletBalanceNotifierProvider,
            onChange: (context, dynamic state) {
              if (!state.isLoading && state.response != []) {
                setState(() {
                  WalletBalanceResponseData responseData =
                      state.response.responseData;
                  usableWalletAmount =
                      double.parse(responseData.totalWalletBalance.toString());
                  walletBalance = (responseData.totalWalletBalanceLeft! +
                      usableWalletAmount!);
                  print("wallet balance : " + walletBalance.toString());
                  print("use wallet : " + useWallet.toString());
                  if (walletBalance! < 0) {
                    useWallet = true;
                    lockWallet = true;
                  }
                });
              }
            },
            child: Column(
              children: [
                Consumer(builder: (context, watch, child) {
                  final state = watch(walletBalanceNotifierProvider);

                  if (state.isLoading) {
                    return MakePaymentBSheetLoading();
                  } else if (state.isError) {
                    return LoadingError(
                      onPressed: (res) {
                        loadData(context: res);
                      },
                      message: state.errorMessage,
                    );
                  } else {
                    return Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          const Center(
                            child: Text(
                              "Please make payment to continue.",
                              style: TextStyle(fontWeight: FontWeight.w600),
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              if (walletBalance !=
                                  0 /*&& double.parse(getTotalAmount()) < walletBalance!*/)
                                Container(
                                  padding: EdgeInsets.only(left: 15, right: 5),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "Use wallet(AED$walletBalance)",
                                        style: const TextStyle(
                                            fontWeight: FontWeight.w500),
                                      ),
                                      Transform.scale(
                                          scale: 1.1,
                                          child: Checkbox(
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(15)),
                                            checkColor: Colors.white,
                                            activeColor: App24Colors
                                                .brownSendPackageThemeColor,
                                            value: useWallet,
                                            onChanged: lockWallet!
                                                ? null
                                                : (bool? value) {
                                                    setState(() {
                                                      useWallet = value!;
                                                    });
                                                  },
                                          )),
                                    ],
                                  ),
                                ),
                            ],
                          ),
                          const Divider(),
                          const SizedBox(
                            height: 10,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 15),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                // MaterialButton(onPressed: (){
                                //   print(widget.detailResponseData.totalPayable!);
                                // },child: Text("asdas"),),
                                /*Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Text(
                                      "Total Distance :",
                                      textAlign: TextAlign.end,
                                      style:
                                      Theme.of(context).textTheme.bodyText1,
                                    ),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                      " ${selectedVehicleResponseData?.distance}",
                                      textAlign: TextAlign.end,
                                      style:
                                      Theme.of(context).textTheme.bodyText1,
                                    )
                                  ],
                                ),*/
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Text(
                                      "Total Payable :",
                                      style:
                                          Theme.of(context).textTheme.bodyMedium ,
                                    ),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                      // walletBalance! < 0 ? widget.detailResponseData.totalPayable! :
                                      getTotalAmount(),
                                      style:
                                          Theme.of(context).textTheme.titleSmall,
                                    ),

                                    // RichText(
                                    //   text: TextSpan(
                                    //     text: 'Total Amount : ',
                                    //     style: Theme.of(context).textTheme.bodyText1,
                                    //     children: <TextSpan>[
                                    //       TextSpan(
                                    //         text: isReturnRequired
                                    //             ? selectedVehicleResponseData
                                    //                 ?.estimationCostWithReturn
                                    //                 .toString()
                                    //             : selectedVehicleResponseData?.estimationCost
                                    //                 .toString(),
                                    //         style: Theme.of(context).textTheme.headline6,
                                    //       ),
                                    //     ],
                                    //   ),
                                    //   textAlign: TextAlign.end,
                                    // ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          CommonButton(
                            onTap: onConfirmPressed,
                            bgColor: App24Colors.brownSendPackageThemeColor,
                            text: "Confirm",
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          // if(widget.detailResponseData.)

                          Consumer(builder: (context, watch, child) {
                            final state = watch(walletBalanceNotifierProvider);
                            // final state1 = watch(cartNotifierProvider);
                            if (state.isLoading) {
                              return CupertinoActivityIndicator();
                            } else if (state.isError) {
                              return LoadingError(
                                onPressed: (res) {
                                  loadData(init: true, context: res);
                                },
                                message: state.errorMessage.toString(),
                              );
                            } else {
                              // itemListModel =
                              //     state.response.responseData.products;
                              //
                              orderStatus = state.response;
                              // cartTileList = state1.response.responseData.carts;
                              return Visibility(
                                visible: (orderStatus!.responseData!.status ==
                                    "PENDING"),
                                child: CommonButton(
                                  onTap: onCancelPressed,
                                  bgColor: App24Colors.redRestaurantThemeColor,
                                  text: "Cancel Order",
                                ),
                              );
                            }
                          })
                        ],
                      ),
                    );
                  }
                }),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void initNotification() async {
    int counter = 0;
    FlutterRingtonePlayer ringtonePlayer = FlutterRingtonePlayer();

    ringtonePlayer.play(
      android: AndroidSounds.notification,
      ios: IosSounds.glass,
      looping: true, // Android only - API >= 28
      volume: 1.0, // Android only - API >= 28
      asAlarm: false, // Android only - all APIs
    );
    String message = "Please pay the bill amount ";
    await flutterTts.setVolume(1);
    await flutterTts.awaitSpeakCompletion(true);
    await flutterTts.speak(message);
// minuteTimer = Timer.periodic(
//     Duration(minutes: 1), (Timer t1) async{
    secondsTimer = Timer.periodic(Duration(seconds: 1), (Timer t) async {
      counter += 1;
      print("t :" + t.tick.toString());
      await flutterTts.setVolume(1.0);
      await flutterTts.awaitSpeakCompletion(true);
      //await flutterTts.speak("You have received a new order from APP 24");
      await flutterTts.speak(message);
      print("counter :" + counter.toString());
      if (t.tick >= 6) {
        // minuteTimer?.cancel();
        secondsTimer?.cancel();
        // FlutterRingtonePlayer.stop();
      }
    });
//});
  }
}
