import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/constants/global_constants.dart';
import 'package:app24_user_app/models/location_model.dart';
import 'package:app24_user_app/providers/location_picker_provider.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';

import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/utils/services/database/database_service.dart';
import 'package:app24_user_app/utils/services/database/location_database.dart';

import 'package:app24_user_app/widgets/select_location_address.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';

class SearchLocationBottomSheetAddressPage extends StatefulWidget {
  SearchLocationBottomSheetAddressPage({Key? key, this.initialCall, this.title})
      : super(key: key);

  final bool? initialCall;
  final String? title;

  @override
  State<StatefulWidget> createState() {
    return new _SearchLocationBottomSheetAddressPageState();
  }
}

class _SearchLocationBottomSheetAddressPageState
    extends State<SearchLocationBottomSheetAddressPage> {
  final addressController = TextEditingController();

  late LocationNotifier _locationPickerProvider;

  List<LocationModel> recentSearchesList = [];

  @override
  void initState() {
    super.initState();

    getRecentSearches();
  }

  @override
  void dispose() {
    addressController.dispose();
    super.dispose();
  }

  Future<void> getRecentSearches() async {
    LocationDatabase locationDatabase =
        LocationDatabase(DatabaseService.instance);
    int? type;
    if (widget.title == GlobalConstants.labelPickupLocation) {
      type = 0;
    } else if (widget.title == GlobalConstants.labelDropLocation) {
      type = 1;
    }

    await locationDatabase.getRecentSearches(type).then((value) {
      setState(() {
        recentSearchesList = value;
      });
    });
  }

  updateLatLong(LocationModel location) {
    if (location.latitude == null && location.longitude == null) {
      _locationPickerProvider
          .setLatLongFromPlaceId(location.placeID)
          .then((value) {
        if (value != null) {
          location.latitude = value['lat'];
          location.longitude = value['long'];
          navigateToMap(location: location);
        } else {
          showMessage(context, "Error occurred. Please try again.", true, true);
        }
      });
    } else {
      print("location  main_text ::: " + location.main_text.toString());
      print(
          "location secondary_text ::: " + location.secondary_text.toString());
      print("location description ::: " + location.description.toString());
      print("location street ::: " + location.street.toString());
      print("location label ::: " + location.label.toString());
      navigateToMap(location: location);
    }
  }

  navigateToMap({LocationModel? location, bool isCurrentLocation = false}) {
    if (!widget.initialCall!) {
      Navigator.pop(context, isCurrentLocation ? 'current_location' : location);
    } else {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => SelectLocationAddressPage(
                  mainText: location?.main_text ?? "",
                  locationAutoCompleteModel: location,
                  title: widget.title!,
                  isCurrentLocation: isCurrentLocation,
                )),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (context, watch, child) {
      _locationPickerProvider = watch(locationNotifierProvider.notifier);
      _locationPickerProvider.setInitialValues(context: context);
      return Column(mainAxisAlignment: MainAxisAlignment.end, children: [
        Material(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(30), topLeft: Radius.circular(30)),
          child: Container(
              height: MediaQuery.of(context).size.height * 0.8,
              child: Container(
                alignment: Alignment.bottomCenter,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(30),
                        topLeft: Radius.circular(30)),
                    color: Colors.white),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    const SizedBox(
                      height: 6,
                    ),
                    // Center(child: Container(width: 100,height: 5,decoration: BoxDecoration(borderRadius: BorderRadius.circular(15),color: Colors.grey[300]),)),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          // Container(),
                          Text(
                            "Search Location",
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Container(
                              decoration: BoxDecoration(shape: BoxShape.circle),
                              child: Material(
                                color: Colors.transparent,
                                child: InkWell(
                                  customBorder: CircleBorder(),
                                  onTap: () {
                                    setState(() {
                                      Navigator.pop(context);
                                     // showUnAuthorisedPopUp();
                                    });
                                  },
                                  child: Padding(
                                    padding: EdgeInsets.all(10),
                                    child: Text(
                                      "close",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w600,
                                          color: Colors.grey),
                                    ),
                                  ),
                                ),
                              )),
                        ],
                      ),
                    ),
                    Divider(),

                    // Padding(
                    //   padding:
                    //   EdgeInsets.symmetric(horizontal: 20, vertical: 6),
                    //   child: TypeAheadField(
                    //     hideOnEmpty: false,
                    //     hideSuggestionsOnKeyboardHide: false,
                    //     keepSuggestionsOnLoading: true,
                    //     getImmediateSuggestions: true,
                    //     keepSuggestionsOnSuggestionSelected: true,
                    //     suggestionsBoxDecoration: SuggestionsBoxDecoration(
                    //       color: Colors.white,
                    //       borderRadius: BorderRadius.circular(8),
                    //     ),
                    //     textFieldConfiguration: TextFieldConfiguration(
                    //       style: TextStyle(fontWeight: FontWeight.w600),
                    //       decoration: InputDecoration(
                    //         border: OutlineInputBorder(
                    //           borderRadius: BorderRadius.circular(15),
                    //           borderSide: BorderSide(
                    //             width: 0,
                    //             style: BorderStyle.none,
                    //           ),
                    //         ),
                    //         fillColor: Color(0xff676767).withOpacity(0.09),
                    //         filled: true,
                    //         contentPadding: EdgeInsets.symmetric(vertical: 0),
                    //         prefixIcon: Icon(Icons.search),
                    //         hintText: "Search",
                    //       ),
                    //       onEditingComplete: () {
                    //         SystemChannels.textInput
                    //             .invokeMethod('TextInput.hide');
                    //       },
                    //     ),
                    //     itemBuilder: (context, LocationModel location) {
                    //       return ListTile(
                    //         leading: Icon(
                    //           CupertinoIcons.location_solid,
                    //           color: location
                    //               .pinColor, //green - from local DB , orange - from server , red - from google
                    //         ),
                    //         title: Text(
                    //           location.main_text!,
                    //           style: TextStyle(
                    //               fontSize: 17, fontWeight: FontWeight.w600),
                    //         ),
                    //         subtitle: Text(
                    //           location.secondary_text!,
                    //           maxLines: 2,
                    //         ),
                    //       );
                    //     },
                    //     onSuggestionSelected: (LocationModel location) {
                    //       FocusScopeNode currentFocus = FocusScope.of(context);

                    //       if (!currentFocus.hasPrimaryFocus) {
                    //         currentFocus.unfocus();
                    //       }
                    //       updateLatLong(location);

                    //     },
                    //     suggestionsCallback: (pattern) async {
                    //       print("pattern :"+pattern );
                    //       if (pattern.length > 2) {

                    //         return await _locationPickerProvider
                    //             .getSuggestions(pattern);
                    //       }else{
                    //         List<LocationModel> list = [];
                    //         return list;
                    //       }
                    //     },
                    //   ),
                    // ),
                    Flexible(
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                          TextButton(
                              onPressed: () {
                                FocusScopeNode currentFocus =
                                    FocusScope.of(context);

                                if (!currentFocus.hasPrimaryFocus) {
                                  currentFocus.unfocus();
                                }
                                navigateToMap(isCurrentLocation: true);
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Icon(
                                    Icons.my_location,
                                    color: App24Colors.darkTextColor,
                                  ),
                                  const SizedBox(
                                    width: 7,
                                  ),
                                  Text(
                                    "Use current location",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        color: App24Colors.darkTextColor),
                                  )
                                ],
                              )),
                          if (recentSearchesList.isNotEmpty)
                            ListTile(
                              title: Text(
                                "Recent Searches",
                                style: Theme.of(context).textTheme.titleSmall
                              ),
                            ),
                          Flexible(
                            child: ListView.builder(
                              padding: EdgeInsets.all(0),
                              itemCount: recentSearchesList.length,
                              itemBuilder: (context, index) {
                                return ListTile(
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 15, vertical: 0),
                                  onTap: () {
                                    updateLatLong(recentSearchesList[index]);
                                  },
                                  leading: Icon(
                                    Icons.history,
                                    color: Colors.grey,
                                  ),
                                  title: Text(
                                    recentSearchesList[index].main_text ?? "",
                                    style: TextStyle(
                                        fontSize: 17,
                                        fontWeight: FontWeight.bold),
                                    maxLines: 1,
                                  ),
                                  subtitle: Text(
                                    recentSearchesList[index].secondary_text ??
                                        "",
                                    style:
                                        TextStyle(fontWeight: FontWeight.w600),
                                  ),
                                );
                              },
                            ),
                          )
                        ]))
                  ],
                ),
              )),
        )
      ]);
    });
  }
}
