
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/api/api_response_state2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

// import 'cart_model.dart';

class CheckoutBottomSheetNotifier extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  CheckoutBottomSheetNotifier(this._apiRepository) : super(ResponseState2(isLoading: true,response: []));
  List<int>? cartIdList;
  int cartCount=0;
  int cartID = 0;
  String latLong='';
  Future<void> getCartList({bool init = true,double? to_lat,double? to_lng}) async {
    try {
      latLong =
      '?to_lat=${to_lat.toString()}&'
          'to_lng=${to_lng.toString()}';
      if (init) state = state.copyWith(isLoading: true);

      final cartListModel = await _apiRepository.fetchCheckoutBSheetCartList(latLong: latLong);


        //  print("message :: "+cartListModel.message.toString());

        cartIdList = cartListModel.responseData!.cartProductsId;
        cartCount = cartListModel.responseData!.totalCart!;

        state =
            state.copyWith(
                response: cartListModel, isLoading: false, isError: false);

    } catch (e) {

      print("agan dev :: "+e.toString());


/*      state = state.copyWith(
          errorMessage: e.toString(), isLoading: false, isError: true);*/
    }
  }


}
