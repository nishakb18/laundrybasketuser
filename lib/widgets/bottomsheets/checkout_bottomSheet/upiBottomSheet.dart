import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/helpers.dart';
import 'package:app24_user_app/widgets/bottomsheets/order_success_bottomSheet.dart';
import 'package:app24_user_app/widgets/bottomsheets/price_diff_bottomSheet.dart';

// import 'package:app24_user_app/modules/tabs/home_tab.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class UpiBottomSheet extends StatefulWidget {
  @override
  _UpiBottomSheetState createState() => _UpiBottomSheetState();
}

class _UpiBottomSheetState extends State<UpiBottomSheet> {
  String paymentOption = "upi";


  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        // height: MediaQuery.of(context).size.height * 0.84,
        child: Padding(
          padding: const EdgeInsets.all(30.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Choose Payment Method",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              // const SizedBox(
              //   height: 10,
              // ),
              Stack(children: [
                Padding(
                  padding: const EdgeInsets.only(top: 15),
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      border: Border.all(
                          width: 2.0,
                          color: Color(0xfff2f2f2),
                          style: BorderStyle.solid),
                    ),
                    child: Material(
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                      child: ListTile(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        onTap: () {
                          setState(() {
                            paymentOption = "upi";
                          });
                        },
                        title: Padding(
                          padding: const EdgeInsets.all(15),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                children: [
                                  Row(
                                    children: [
                                      Image.asset(
                                        App24UserAppImages.upi,
                                        width:
                                            MediaQuery.of(context).size.width / 5,
                                      ),
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      Image.asset(
                                        App24UserAppImages.wallet,
                                        width: MediaQuery.of(context).size.width /
                                            15,
                                      ),
                                      const SizedBox(
                                        width: 10,
                                      ),
                                      Image.asset(
                                        App24UserAppImages.netBanking,
                                        width: MediaQuery.of(context).size.width /
                                            15,
                                      ),
                                      const SizedBox(
                                        width: 10,
                                      ),
                                      Image.asset(
                                        App24UserAppImages.bSheetDebitCard,
                                        width: MediaQuery.of(context).size.width /
                                            15,
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              Icon(
                                paymentOption == "upi"
                                    ? Icons.check_circle
                                    : Icons.radio_button_unchecked,
                                color: App24Colors.darkTextColor,
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  right: MediaQuery.of(context).size.width / 14,
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: App24Colors.greenOrderEssentialThemeColor),
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                    child: Text(
                      "Free",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 13),
                    ),
                  ),
                ),
              ]),
              const SizedBox(
                height: 20,
              ),
              Stack(children: [
                Padding(
                  padding: const EdgeInsets.only(top: 15),
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(width: 3.0, color: Color(0xfff2f2f2)),
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                    ),
                    child: Material(
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                      child: ListTile(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        onTap: () {
                          setState(() {
                            paymentOption = "razor";
                          });
                        },
                        title: Padding(
                          padding: const EdgeInsets.all(15),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        Image.asset(
                                          App24UserAppImages.razorPay,
                                          width:
                                              MediaQuery.of(context).size.width /
                                                  3.5,
                                        ),
                                      ],
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        Image.asset(
                                          App24UserAppImages.gPay,
                                          width:
                                              MediaQuery.of(context).size.width /
                                                  15,
                                        ),
                                        const SizedBox(
                                          width: 10,
                                        ),
                                        Image.asset(
                                          App24UserAppImages.bSheetPaytm,
                                          width:
                                              MediaQuery.of(context).size.width /
                                                  15,
                                        ),
                                        const SizedBox(
                                          width: 10,
                                        ),
                                        Image.asset(
                                          App24UserAppImages.phonePay,
                                          width:
                                              MediaQuery.of(context).size.width /
                                                  15,
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                              Icon(
                                paymentOption == "razor"
                                    ? Icons.check_circle
                                    : Icons.radio_button_unchecked,
                                color: App24Colors.darkTextColor,
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  right: MediaQuery.of(context).size.width / 14,
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Color(0xffff5d44)),
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                    child: Text(
                      "Charges apply",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 13),
                    ),
                  ),
                )
              ]),
              const SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  Expanded(
                    child: ElevatedButton(
                      onPressed: () {
                        Helpers().getCommonBottomSheet(
                            context: context,
                            content: PRiceDifferenceBottomSheet(),
                            title: "Payment Request");
                        // Navigator.push(
                        //   context,
                        //   MaterialPageRoute(
                        //       builder: (context) => HomeTabPage(
                        //         type: "afterOrder",
                        //       )),
                        // );
                      },
                      style: ElevatedButton.styleFrom(
                          padding: EdgeInsets.symmetric(vertical: 15), backgroundColor: App24Colors.greenOrderEssentialThemeColor,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15))),
                      child: Text("Proceed"),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
