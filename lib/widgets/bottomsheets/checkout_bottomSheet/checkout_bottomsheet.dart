import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/constants/global_constants.dart';
import 'package:app24_user_app/helpers.dart';
import 'package:app24_user_app/models/location_model.dart';
import 'package:app24_user_app/modules/delivery/cart/cart.dart';
import 'package:app24_user_app/modules/delivery/cart/cart_loading.dart';
import 'package:app24_user_app/modules/delivery/cart/cart_model.dart';
import 'package:app24_user_app/modules/my_account/address/manage_address.dart';
import 'package:app24_user_app/modules/my_account/address/manage_address_model.dart';
import 'package:app24_user_app/modules/restaurant/restaurant_cart_page.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/widgets/bottomsheets/checkout_bottomSheet/checkout_loading.dart';
import 'package:app24_user_app/widgets/bottomsheets/promocodes/add_coupon_bottomSheet.dart';
import 'package:app24_user_app/widgets/bottomsheets/addressConfirmation/addressConfirmation.dart';
import 'package:app24_user_app/widgets/bottomsheets/promocodes/promocodes_model.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:dio/dio.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
// import 'file:///D:/downloads/app24latest/patron/lib/widgets/bottomsheets/upiBottomSheet.dart';

// import '../../helpers.dart';

class CheckoutBottomSheet extends StatefulWidget {
  const CheckoutBottomSheet({
    Key? key,
    required this.toAddress,
    this.fromPage,
    required this.cartList,
    this.promoCode,
    this.promoCodeDetails,
    this.latitude,
    this.longitude,
    this.map, this.fromAddList
  }) : super(key: key);
  final String? fromPage;
  final String? promoCode;
  final CartListResponseData cartList;
  final String? promoCodeDetails;
  final double? latitude;
  final double? longitude;
  final String toAddress;
  final Map<dynamic, dynamic>? map;
  final bool? fromAddList;

  @override
  _CheckoutBottomSheetState createState() => _CheckoutBottomSheetState();
}

class _CheckoutBottomSheetState extends State<CheckoutBottomSheet> {

  TextEditingController addressController = new TextEditingController();
  TextEditingController couponController = new TextEditingController();
  var total;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  CartListResponseData? cart;
  late Map cartContentBody={};
  var totalAmount;
   AddressResponseData? addressResultFromCheckout;
  @override
  void initState() {
    SchedulerBinding.instance!.addPostFrameCallback((_) async {
      print("fromaddlist : " + widget.fromAddList.toString());
      if(widget.fromAddList != true)
      context.read(cartNotifierProvider.notifier).getCartList(
          fromLat: double.parse(widget.latitude
              .toString()) /*== null
              ? context
                  .read(homeNotifierProvider.notifier)
                  .currentLocation
                  .latitude
              : widget.latitude*/
          ,
          fromLng: double.parse(widget.longitude.toString()),
          context:
          context/*== null
              ? context
                  .read(homeNotifierProvider.notifier)
                  .currentLocation
                  .longitude
              : widget.longitude*/
      );
      lockWallet = widget.cartList.userWalletBalance < 0 ? true :false;
    });

    super.initState();
  }

  loadData({bool init = true, required BuildContext context}) {}

  onClearCart() async {
    Navigator.pop(context, "clearCart");
    // Helpers().getCommonAlertDialogBox(content: AlertDialog(
    //   title: Text("Cart cleared"),
    //   content: MaterialButton(
    //     onPressed: (){
    //       Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomePage()));
    //     },
    //     child: Text("OK"),
    //   ),
    // ), context: context);

    context.read(cartCountNotifierProvider.notifier).updateCartCount(count: 0);

      context
          .read(cartNotifierProvider.notifier)
          .getClearCart(context: context);


    // context.read(cartNotifierProvider.notifier).getCartList(
    //     context: context,
    //     init: true,
    //     lat: context
    //         .read(homeNotifierProvider.notifier)
    //         .currentLocation
    //         .latitude!
    //         .toDouble(),
    //     lng: context
    //         .read(homeNotifierProvider.notifier)
    //         .currentLocation
    //         .longitude!
    //         .toDouble());
    // int cartCount =
    //     context.read(cartCountNotifierProvider.notifier).state.response;
  }


  selectLocation(String title) {
    showSelectLocationSheet(context: context, title: title).then((value) {
      if (value != null) {
        LocationModel model = value;
        setState(() {
          context.read(homeNotifierProvider.notifier).currentLocation = model;
          context.read(cartNotifierProvider.notifier).getCartList(
              fromLat: context
                  .read(homeNotifierProvider.notifier)
                  .currentLocation
                  .latitude!
                  .toDouble(),
              fromLng: context
                  .read(homeNotifierProvider.notifier)
                  .currentLocation
                  .longitude!
                  .toDouble(),
              context: context);
          // loadData(context: context);
        });
      }
    });
  }



  void addressValue(BuildContext context) async {
    // start the SecondScreen and wait for it to finish with a result
    final address = await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ManageAddressPage(
            callFrom: "checkout",
          ),
        ));

    // after the SecondScreen result comes back update the Text widget with it
    setState(() {
      if (address != null) addressController.text = address;
    });
  }

  // Future<void> addressConfirmation() async {
  //   var result = await Helpers().getCommonBottomSheet(
  //       context: context,
  //       content: AddressConfirmationBottomSheet(),
  //       title: "Address Confirmation");
  //   showDialog(
  //     context: context,
  //     // barrierDismissible: false,
  //     builder: (_) {
  //       // return CouponCode();
  //       return Helpers().getCommonBottomSheet(
  //           context: context,
  //           content: AddressConfirmationBottomSheet(),
  //           title: "Address Confirmation"); /*OrderSuccessAlertBox();*/
  //     },
  //   );
  // }

  // useWallet == true
  // ? (widget.cartList.totalPrice! +
  // widget.cartList.shopGstAmount -
  // widget.cartList.userWalletBalance!)
  //     .toString()
  //     : (widget.cartList.totalPrice! +
  // widget.cartList.shopGstAmount)
  //     .toString(),

  getTotalAmount(CartListResponseData cartList) {
    print("wallet amount " +widget.cartList.userWalletBalance!.toString());
    print("wallet amount " +cartList.totalPrice!.toString());
    // setState(() {
      total = double.parse(cartList.totalPrice!.toString());
    // });
    if (useWallet) {
     // setState(() {
       total = cartList.totalPrice! /*+
          widget.cartList.shopGstAmount*/ -
           widget.cartList.userWalletBalance!;
       print("total amount " +total.toString());

       if (total < 0) total = 0;
     // });
    }else{
      return  double.parse(total.toString()).toStringAsFixed(2);
    }
    //print("barcelona"+double.parse(total.toString()).toStringAsFixed(2));
    return  double.parse(total.toString()).toStringAsFixed(2);
  }

  // payWithWallet() {
  //   Map body = {
  //     'wallet_amount': usableWalletAmount,
  //     'id': widget.detailResponseData.id
  //   };
  //
  //   context
  //       .read(payWithWalletNotifierProvider.notifier)
  //       .payWithWallet(body: body);
  // }

  onConfirmClicked(CartListResponseData cartList) {
    Map checkOutResult ;
    if(addressResultFromCheckout==null)
      {
     checkOutResult = {
      'wallet': useWallet == true ? "1" : "0" ,
      // 'use_wallet': useWallet == true ? "1" : "0" ,
      'total_amount': widget.cartList.userWalletBalance < 0  ? totalAmount :  getTotalAmount(cartList),
      'promo_code': "",
       // 'to_address':addressResultFromCheckout!.landmark.toString()+addressResultFromCheckout!.flatNo.toString()+addressResultFromCheckout!.street.toString() == "" ? widget.toAddress : addressResultFromCheckout!.landmark.toString()+addressResultFromCheckout!.flatNo.toString()+addressResultFromCheckout!.street.toString(),
       // 'to_lat':addressResultFromCheckout!.latitude == null ? widget.latitude : addressResultFromCheckout!.latitude,
       // 'to_lng':addressResultFromCheckout!.longitude == null ? widget.longitude : addressResultFromCheckout!.longitude,


     };}
else{
     checkOutResult = {
      'wallet': useWallet == true ? "1" : "0" ,
      // 'use_wallet': useWallet == true ? "1" : "0" ,
      'total_amount': widget.cartList.userWalletBalance < 0  ? totalAmount :  getTotalAmount(cartList),
      'promo_code': "",

      'to_address':addressResultFromCheckout!.landmark.toString()+addressResultFromCheckout!.flatNo.toString()+addressResultFromCheckout!.street.toString() == "" ? widget.toAddress : addressResultFromCheckout!.landmark.toString()+addressResultFromCheckout!.flatNo.toString()+addressResultFromCheckout!.street.toString(),
      'to_lat':addressResultFromCheckout!.latitude == null ? widget.latitude : addressResultFromCheckout!.latitude,
      'to_lng':addressResultFromCheckout!.longitude == null ? widget.longitude : addressResultFromCheckout!.longitude,


    };}

    // print(widget.cartList.userWalletBalance < 0  ? getTotalAmount(cartList) : totalAmount);
    // print(getTotalAmount());
    // print(totalAmount);
    // print("total" + double.parse(cart!.totalPrice!
    //     .toString()).toString());
    Navigator.pop(context, checkOutResult);
print("pop");
print(checkOutResult);
  }

  void displayBottomSheet(BuildContext context) {}

  bool useWallet = false;
  bool lockWallet = false;

  // onClickSeeAllCoupon() async {
  //   var selectedCoupon = await Helpers().getCommonBottomSheet(
  //       context: context,
  //       content: AddCouponBottomSheet(),
  //       title: "Coupon Codes");
  //   if (selectedCoupon != null) {
  //     PromoCodesResponseData selectedPromoCode = selectedCoupon;
  //     setState(() {
  //       couponController.text = selectedPromoCode.promoCode!;
  //     });
  //   }
  // }


  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
          child:



          Consumer(builder: (context, watch, child) {
            final state = watch(cartNotifierProvider);
            if (state.isLoading) {
              return CheckoutBottomSheetLoading();
            } else if (state.isError) {
              return LoadingError(
                onPressed: (res) {
                  loadData(init: true, context: res);
                },
                message: state.errorMessage.toString(),
              );
            } else {
              if (state.response != 0) {
                cart = state.response.responseData;

// setState(() {
  totalAmount = cart!.totalPrice! + 0;
// });
                return Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // if (widget.cartList.showEstimation == 1)
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Column(
                          children: [
                            Container(
                              //padding: EdgeInsets.only(left: 10,right: 10),
                              child: Text("Item(s) will be delivered to: ",style: TextStyle(fontWeight: FontWeight.bold),),),
                            const SizedBox(
                              height: 10,
                            ),

                            Row(children: [
                              Container(
                                width: MediaQuery.of(context).size.width*.6,
                                padding: EdgeInsets.only(left: 10,right: 10),
                                child: Text(widget.toAddress.toString()),),
                              InkWell(
                                onTap: (){
print("agandev");
// Navigator.pop(context);
                                  callConfirmAddressBottomSheet();
                                  // Navigator.pop(context);
                                },
                                child: Text("Change",style: TextStyle(fontWeight: FontWeight.bold),),),
                            ],),



                            // if(widget.fromAddList != true)
                            if (widget.cartList.showEstimation == 1 && widget.fromAddList != true)
                            Divider(color: Colors.black45,thickness: .25,),
                            if (widget.cartList.showEstimation == 1 && widget.fromAddList != true)
                            Helpers().getDetailsRow(
                                "Item Total",
                                "AED" +
                                    double.parse(
                                        cart!.totalItemPrice.toString())
                                        .toStringAsFixed(2),
                                context),
                            // const SizedBox(
                            //   height: 5,
                            // ),
                            // Helpers().getDetailsRow(
                            //     "GST",
                            //     "AED" +
                            //         double.parse(cart!.shopGstAmount.toString())
                            //             .toStringAsFixed(2),
                            //     context),
                            // const SizedBox(
                            //   height: 5,
                            // ),
                            // Helpers().getDetailsRow(
                            //     "Discount",
                            //     "AED" +
                            //         double.parse(
                            //             cart!.promocodeAmount.toString())
                            //             .toStringAsFixed(2),
                            //     context),
                            const SizedBox(
                              height: 5,
                            ),
                            if (widget.cartList.showEstimation == 1  && widget.fromAddList != true)
                    Helpers().getDetailsRow(
                        "Delivery Charge",
                        "AED" +
                            double.parse(
                                cart!.deliveryCharges.toString())
                                .toStringAsFixed(2),
                        context),
                            const SizedBox(
                              height: 5,
                            ),

                          ],
                        ),
                      ),
                    if (widget.cartList.showEstimation == 1 && widget.fromAddList != true)
                      Form(
                        key: _formKey,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: TextFormField(
                            controller: couponController,
                            validator: (String? arg) {
                              if (arg!.length > 0)
                                return 'Invalid Coupon Code.';
                              else if (arg.length == 0)
                                return "Please enter coupon code";
                            },
                            // maxLines: 1,
                            decoration: InputDecoration(
                              suffixIcon: MaterialButton(
                                onPressed: () {
                                  _formKey.currentState!.validate();
                                  // if (couponController.text.isNotEmpty)

                                    // showMessage(context, "Invalid coupon code", true, true);
                                    // Helpers().getCommonAlertDialogBox(
                                    //     content: AlertDialog(
                                    //       titlePadding: EdgeInsets.all(0),
                                    //       shape: RoundedRectangleBorder(
                                    //           borderRadius:
                                    //           BorderRadius.circular(20)),
                                    //       title: Container(
                                    //         decoration: BoxDecoration(
                                    //           borderRadius: BorderRadius.only(
                                    //               topRight: Radius.circular(20),
                                    //               topLeft: Radius.circular(20)),
                                    //           color: Colors.green,
                                    //         ),
                                    //         height: 140,
                                    //         child: Center(
                                    //           child: Icon(
                                    //             CupertinoIcons
                                    //                 .clear_circled_solid,
                                    //             size: 80,
                                    //           ),
                                    //         ),
                                    //       ),
                                    //       // Center(child: Text("Success",style: TextStyle(fontWeight: FontWeight.w800),)),
                                    //       content: SingleChildScrollView(
                                    //         child: Column(
                                    //           // crossAxisAlignment: CrossAxisAlignment.stretch,
                                    //           children: [
                                    //             Text(
                                    //               "Coupon code is invalid.",
                                    //               textAlign: TextAlign.center,
                                    //               style: TextStyle(
                                    //                   fontWeight: FontWeight.w600),
                                    //             ),
                                    //             const SizedBox(
                                    //               height: 10,
                                    //             ),
                                    //             Center(
                                    //                 child: MaterialButton(
                                    //                   onPressed: () {
                                    //                     Navigator.pop(context);
                                    //                   },
                                    //                   padding: EdgeInsets.symmetric(
                                    //                       horizontal: 80),
                                    //                   shape: RoundedRectangleBorder(
                                    //                       borderRadius:
                                    //                       BorderRadius.circular(
                                    //                           10)),
                                    //                   color: App24Colors
                                    //                       .greenOrderEssentialThemeColor,
                                    //                   child: Text(
                                    //                     "Ok",
                                    //                     style: TextStyle(
                                    //                         color: Colors.white),
                                    //                   ),
                                    //                 ))
                                    //           ],
                                    //         ),
                                    //       ),
                                    //       // actions: [
                                    //       //   Center(
                                    //       //       child: MaterialButton(
                                    //       //     onPressed: () {
                                    //       //       Navigator.pop(context);
                                    //       //     },
                                    //       //     shape: RoundedRectangleBorder(
                                    //       //         borderRadius:
                                    //       //             BorderRadius.circular(10)),
                                    //       //     color: App24Colors
                                    //       //         .greenOrderEssentialThemeColor,
                                    //       //     child: Text(
                                    //       //       "Ok",
                                    //       //       style: TextStyle(color: Colors.white),
                                    //       //     ),
                                    //       //   ))
                                    //       // ],
                                    //     ),
                                    //     context: context);
                                },
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(10),
                                        bottomRight: Radius.circular(10))),
                                child: Text("Apply"),
                              ),
                              contentPadding: EdgeInsets.only(
                                /*bottom: 20,*/
                                  left: 20),
                              hintText: "Enter coupon code here",
                              hintStyle: TextStyle(
                                fontSize: 13,
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: BorderSide(
                                      color: App24Colors
                                          .redRestaurantThemeColor
                                          .withOpacity(0.2))),
                              errorBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: BorderSide(
                                      color: App24Colors
                                          .redRestaurantThemeColor
                                          .withOpacity(0.2))),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: BorderSide(
                                      color: App24Colors
                                          .greenOrderEssentialThemeColor
                                          .withOpacity(0.2))),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: BorderSide(
                                      color: App24Colors
                                          .greenOrderEssentialThemeColor
                                          .withOpacity(0.2))),
                            ),
                          ),
                        ),
                      ),
                    if (widget.cartList.showEstimation == 1 && widget.fromAddList != true)
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 10, 20, 0),
                        child: Row(
                          // crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            RichText(
                                text: TextSpan(
                                    text: "See all coupons",
                                    style: TextStyle(
                                      color: Colors.blue,
                                      fontSize: 11,
                                    ),
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () async {
                                        print(widget.promoCode);
                                        var selectedCoupon = await Helpers()
                                            .getCommonBottomSheet(
                                            context: context,
                                            content: AddCouponBottomSheet(
                                              cartShopID:
                                              widget.cartList.storeId,
                                              promoCode: widget.promoCode,
                                              promoCodeDetails:
                                              widget.promoCodeDetails,
                                            ),
                                            title: "Coupon Codes");
                                        if (selectedCoupon != null) {
                                          PromoCodesResponseData
                                          selectedPromoCode =
                                              selectedCoupon;
                                          setState(() {
                                            couponController.text =
                                            selectedPromoCode.promoCode!;
                                          });
                                        }
                                      })),



                          ],
                        ),
                      ),


                    if (widget.cartList.showEstimation == 1 && widget.fromAddList != true)
                    if(cart!.promocode!.isNotEmpty)

                      Padding(
                        padding: EdgeInsets.only(left: 20,right: 20,top:5),
                        child: Row(

                          children: <Widget>[
                           /* Text(
                              "Promocode applied",
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 14),
                            ),*/

                            Expanded(
                              child: Text(
                                "\""+cart!.promocode.toString().toUpperCase()+"\" offer applied on this order",
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    color: App24Colors.lightBlue,
                                    fontSize: 14),
                              ),
                            ),
                          ],
                        ),
                      ),
                    if (widget.cartList.showEstimation == 1 && widget.fromAddList != true)
                    if(cart!.promocode!.isNotEmpty)
                    Padding(
                      padding: EdgeInsets.only(left: 20,right: 20,top:5),
                      child: Row(

                        children: <Widget>[
                          /* Text(
                              "Promocode applied",
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 14),
                            ),*/

                          Expanded(
                            child: Text(
                             "AED" +cart!.promocodeAmount.toString()+ " discounted on your bill",
                              textAlign: TextAlign.end,
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color: App24Colors.lightBlue,
                                  fontSize: 14),
                            ),
                          ),
                        ],
                      ),
                    ),
                    /*  Container(
                        padding: EdgeInsets.only(left: 20,right: 20),
                        width: MediaQuery.of(context).size.width*.85,
                          child:


                      Helpers().getDetailsRow(
                          "Promocode applied",


                          cart!.promocode,
                          context),),*/

               /*     if(cart!.promocode!.isNotEmpty)
                      const SizedBox(
                        height: 5,
                      ),
                    if(cart!.promocode!.isNotEmpty)
                      Padding(
                        padding: EdgeInsets.only(left: 20,right: 20),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Text(
                                "\""+ "AED" +
                                    double.parse(
                                        cart!.promocodeAmount.toString())
                                        .toStringAsFixed(2)+"\" ",
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    color: App24Colors.lightBlue,
                                    fontSize: 14),
                              ),
                            ),

                          ],
                        ),
                      ),*/
                   /*   Container(
                          padding: EdgeInsets.only(left: 20,right: 20),
                          width: MediaQuery.of(context).size.width*.85,
                          child:
                      Helpers().getDetailsRow(
                          "Promocode amount",

                          "AED" +
                              double.parse(
                                  cart!.promocodeAmount.toString())
                                  .toStringAsFixed(2),
                          context),),*/
                    if (widget.cartList.showEstimation == 1 && widget.fromAddList != true)
                    const SizedBox(
                      height: 20,
                    ),



                    if (widget.cartList.showEstimation == 1 && widget.fromAddList != true)
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Total Amount",
                              style: TextStyle(
                                  fontSize: 24, fontWeight: FontWeight.bold),
                            ),
                            if(widget.cartList.userWalletBalance <= 0 )
                            Text("AED" + totalAmount.toString(),
                              // useWallet == true
                              //     ? "AED" + getTotalAmount()
                              //     : "AED" +
                              //     double.parse(cart!.totalPrice!
                              //         .toString()).toString() /*+
                              //         double.parse(cart!.shopGstAmount
                              //             .toString()))
                              //         .toStringAsFixed(2)*/,
                              style: TextStyle(
                                  fontSize: 24,
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xff55708C)),
                            ),
                            if(widget.cartList.userWalletBalance > 0 )
                              Text(
                                useWallet == true
                                    ? "AED" + getTotalAmount(cart!)
                                    : "AED" +/*totalAmount.toString()*/
                                double.parse(cart!.totalPrice!
                                    .toString()).toString()
                                /*+
                                      double.parse(cart!.shopGstAmount
                                          .toString()))
                                      .toStringAsFixed(2)*/,
                                style: TextStyle(
                                    fontSize: 24,
                                    fontWeight: FontWeight.bold,
                                    color: Color(0xff55708C)),
                              )
                          ],
                        ),
                      ),
                    const SizedBox(
                      height: 10,
                    ),
                    // if (double.parse(
                    //     widget.cartList.userWalletBalance.toString()) >
                    //     (double.parse(widget.cartList.totalPrice!.toString()) +
                    //         double.parse(
                    //             widget.cartList.shopGstAmount.toString())))

                      if (widget.cartList.showEstimation == 1 &&
                          widget.cartList.userWalletBalance != 0 && widget.fromAddList != true)
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          // mainAxisAlignment: MainAxisAlignment.spaceBetween,              // crossAxisAlignment: CrossAxisAlignment.stretch,
                          // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Transform.scale(
                                scale: 1.1,
                                child: Checkbox(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(15)),
                                  checkColor: Colors.white,
                                  activeColor:
                                  App24Colors.greenOrderEssentialThemeColor,
                                  value: lockWallet ? true : useWallet,
                                  onChanged:
                                  /*lockWallet
                                      ? null
                                      :*/ (bool? value) {
                                    setState(() {
                                      // if(widget.cartList.userWalletBalance < 0){
                                      //   useWallet = true;
                                      // }else {
                                        useWallet = value!;
                                      // }
                                      // print(getTotalAmount());
                                    });
                                  },
                                )),

                            // Flexible(
                            //   child: Row(
                            //     children: [
                            //       Text("Use",
                            //           style: TextStyle(
                            //               fontSize: 15,
                            //               fontWeight: FontWeight.bold)),
                            //       const SizedBox(
                            //         width: 5,
                            //       ),
                            //       useWallet == false
                            //           ? Text("(AED0)",
                            //           style: TextStyle(
                            //               fontSize: 18,
                            //               fontWeight: FontWeight.bold,
                            //               color: App24Colors
                            //                   .redRestaurantThemeColor),
                            //       overflow: TextOverflow.clip,)
                            //           : Expanded(
                            //             child: Text(
                            //             " (AED " +
                            //                 (widget.cartList.totalPrice! +
                            //                     widget
                            //                         .cartList.shopGstAmount)
                            //                     .toString() +
                            //                 ")",
                            //             style: TextStyle(
                            //                 fontSize: 18,
                            //                 fontWeight: FontWeight.bold,
                            //                 color: App24Colors
                            //                     .redRestaurantThemeColor),
                            //       overflow: TextOverflow.clip,),
                            //           ),
                            //     ],
                            //   ),
                            // ),
                            // const SizedBox(
                            //   width: 15,
                            // ),

                            // Checkbox(
                            //   checkColor: Colors.greenAccent,
                            //   activeColor: Colors.red,
                            //   value: this.useWallet,
                            //   onChanged: (bool? value) {
                            //     setState(() {
                            //       this.useWallet = value;
                            //     });
                            //   },
                            // ),
                            Row(
                              children: [
                                Text(
                                  widget.cartList.userWalletBalance < 0 ? "Negative Wallet" :
                                  "Wallet Amount",
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Text(
                                  "(AED${widget.cartList.userWalletBalance.toString()})",
                                  // useWallet == false
                                  //     ? "(AED" +
                                  //     widget.cartList.userWalletBalance
                                  //         .toString() +
                                  //     ")"
                                  //     : ("(AED" +
                                  //     (widget.cartList
                                  //         .userWalletBalance! -
                                  //         widget
                                  //             .cartList.totalPrice!)
                                  //         .toString()) +
                                  //     ")",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                      color: App24Colors
                                          .greenOrderEssentialThemeColor),
                                  overflow: TextOverflow.clip,
                                ),
                              ],
                            )
                          ],
                        ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        children: [
                          Expanded(
                            child: ElevatedButton(
                              onPressed: () {
                                onConfirmClicked(cart!,);
                                // Helpers().getCommonBottomSheet(
                                //     context: context,
                                //     content: AddressConfirmationBottomSheet(),
                                //     title: "Address Confirmation");
                                // Helpers()
                                //     .getCommonBottomSheet(context, UpiBottomSheet());
                              },
                              style: ElevatedButton.styleFrom(
                                  padding: EdgeInsets.symmetric(vertical: 15), backgroundColor: widget.fromPage == "restaurant"
                                      ? App24Colors.redRestaurantThemeColor
                                      : App24Colors
                                      .greenOrderEssentialThemeColor,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(24))),
                              child: Text(
                                "Confirm & Continue",
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                );
              } else {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Center(
                      child: Column(
                        children: [
                          const Text(
                            "Drop Location too far",
                            style: TextStyle(fontWeight: FontWeight.w800),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          const Text(
                            "Please select a nearby drop point",
                            style: TextStyle(fontWeight: FontWeight.w600),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    new MaterialButton(
                      padding: EdgeInsets.symmetric(vertical: 15),
                      color: App24Colors.redRestaurantThemeColor,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15)),
                      child: const Text(
                        "Change location",
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: () {
                        // Navigator.pop(context);
                        selectLocation(GlobalConstants.labelDropLocation);

                        // showMessage(
                        //     context, "Please change your location", true, false);
                      }
                      // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomePage()));}
                      /*  Navigator.push(context,
                            MaterialPageRoute(builder: (context) => LogInPage()))*/
                      ,
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    new MaterialButton(
                      padding: EdgeInsets.symmetric(vertical: 15),
                      color: App24Colors.redRestaurantThemeColor,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15)),
                      child: const Text(
                        "Clear Cart",
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: () {
                        onClearCart();
                        // context
                        //     .read(cartNotifierProvider.notifier)
                        //     .getClearCart(context: context);
                      }
                      /*  Navigator.push(context,
                            MaterialPageRoute(builder: (context) => LogInPage()))*/
                      ,
                    )
                  ],
                );

                // return   Helpers().getCommonAlertDialogBox(context:context,barrierDismissible: false,content:           AlertDialog(
                //      shape: RoundedRectangleBorder(
                //          borderRadius: BorderRadius.circular(15)),
                //      title: Container(
                //        decoration: BoxDecoration(borderRadius: BorderRadius.only(
                //            topLeft: Radius.circular(15), topRight: Radius.circular(15)),
                //            color: App24Colors.redRestaurantThemeColor),
                //        height: 100,
                //        child: Icon(CupertinoIcons.clear_circled_solid,size: 50,color: Colors.white,),),
                //      // const Text("Session Expired"),
                //      titlePadding: EdgeInsets.all(0),
                //      content: SingleChildScrollView(
                //        child: Center(
                //          child: Column(
                //            children: [
                //              const Text("Drop Location too far",
                //                style: TextStyle(fontWeight: FontWeight.w800),),
                //              const SizedBox(height: 10,),
                //              const Text("Please select a nearby drop point",
                //                style: TextStyle(fontWeight: FontWeight.w600),),
                //            ],
                //          ),
                //        ),
                //      ),
                //      actions: [
                //        Padding(
                //          padding: const EdgeInsets.symmetric(horizontal: 15),
                //          child: Column(
                //            crossAxisAlignment: CrossAxisAlignment.stretch,
                //            children: [
                //              new MaterialButton(
                //                // padding: EdgeInsets.symmetric(horizontal: 80),
                //                color: App24Colors.redRestaurantThemeColor,
                //                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                //                child: const Text("Change location",style: TextStyle(color: Colors.white),),
                //                onPressed: () {
                //                  showSelectLocationSheet(context: context, title: "title").then((value) {
                //                    if (value != null) {
                //                      LocationModel model = value;
                //
                //                      context.read(homeNotifierProvider.notifier).currentLocation = model;
                //                      // loadData(context: context);
                //                    }
                //                  });
                //                  // selectLocation(String title) {
                //                    showSelectLocationSheet(context: context, title: "title").then((value) {
                //                      if (value != null) {
                //                        LocationModel model = value;
                //                        // setState(() {
                //                          context.read(homeNotifierProvider.notifier).currentLocation = model;
                //                          // context
                //                          //     .read(landingPagePromoCodeProvider.notifier)
                //                          //     .getLandingPromoCodes(context: context);
                //                          // loadData(context: context);
                //                        // });
                //                      }
                //                    });
                //                  // }
                //
                //                  showMessage(
                //                      context, "Please change your location", true, false);
                //                }
                //                // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomePage()));}
                //                /*  Navigator.push(context,
                //             MaterialPageRoute(builder: (context) => LogInPage()))*/,
                //              ),
                //              new MaterialButton(
                //                // padding: EdgeInsets.symmetric(horizontal: 60),
                //                color: App24Colors.redRestaurantThemeColor,
                //                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                //                child: const Text("Clear cart",style: TextStyle(color: Colors.white),),
                //                onPressed: () {
                //                  Navigator.pop(context);
                //                  context.read(cartNotifierProvider.notifier).getClearCart(context: context);
                //                }
                //                /*  Navigator.push(context,
                //             MaterialPageRoute(builder: (context) => LogInPage()))*/,
                //              )
                //
                //            ],
                //          ),
                //        ),
                //      ],
                //    ), );
              }
            }
          }),





        ),
      ),
    );
  }

  callConfirmAddressBottomSheet({Map<dynamic, dynamic>? map}) async {


    var addressResult = await Helpers().getCommonBottomSheet(
        context: context,
        content: AddressConfirmationBottomSheet(map: map,),
        title: "Address confirmation");
setState(() {
  addressResultFromCheckout=addressResult;
 // print("lat after change :"+addressResultFromCheckout!.latitude.toString());

});

    String? toAddress = (addressResultFromCheckout!.landmark.toString()+addressResultFromCheckout!.flatNo.toString() +
        addressResultFromCheckout!.street.toString() +
        addressResultFromCheckout!.city.toString()
    );

    cartContentBody["to_adrs"] = toAddress;
    cartContentBody["to_lat"] = addressResultFromCheckout!.latitude;
    cartContentBody["to_lng"] = addressResultFromCheckout!.longitude;
    context.read(cartNotifierProvider.notifier).getCartList(
        context: context,
        init: true,
        fromLat: addressResultFromCheckout!.latitude!.toDouble(),
        fromLng: addressResultFromCheckout!.longitude!.toDouble());
    CartListResponseData? cartListResponseData = context.read(cartNotifierProvider.notifier).crtlistModel.responseData;


    var checkout = await Helpers().getCommonBottomSheet(
        context: context,

        content: CheckoutBottomSheet(
          toAddress: toAddress,
          cartList: cartListResponseData!,
          latitude: addressResultFromCheckout!.latitude,
          longitude: addressResultFromCheckout!.longitude,
           promoCode: widget.promoCode,
           promoCodeDetails: widget.promoCodeDetails,
           map: widget.map,

        ));



    if(checkout == "clearCart") {
      context.read(cartNotifierProvider.notifier).getCartList(
          context: context,
          init: true,
          fromLat: context
              .read(homeNotifierProvider.notifier)
              .currentLocation
              .latitude!
              .toDouble(),
          fromLng: context
              .read(homeNotifierProvider.notifier)
              .currentLocation
              .longitude!
              .toDouble());
    }


    else

      /* if (checkout != null)*/ {
      var useWallet = checkout;
      print("usewallet"+useWallet["total_amount"]);

      cartContentBody["wallet"] = useWallet["wallet"];
      // if(useWallet["to_address"]!=null) {
      //   cartContentBody["to_adrs"] = useWallet["to_address"];
      //   cartContentBody["to_lat"] = useWallet["to_lat"];
      //   cartContentBody["to_lng"] = useWallet["to_lng"];
      // }
      Map checkOutResult ;
      if(addressResultFromCheckout==null)
      {
        checkOutResult = {
          'wallet': useWallet == true ? "1" : "0" ,
          // 'use_wallet': useWallet == true ? "1" : "0" ,
          'total_amount': widget.cartList.userWalletBalance < 0  ? totalAmount :  getTotalAmount(cartListResponseData),
          'promo_code': "",
        };}
      else{
        checkOutResult = {
          'wallet': useWallet == true ? "1" : "0" ,
          // 'use_wallet': useWallet == true ? "1" : "0" ,
          'total_amount': widget.cartList.userWalletBalance < 0  ? totalAmount :  getTotalAmount(cartListResponseData),
          'promo_code': "",

          'to_address':addressResultFromCheckout!.landmark.toString()+addressResultFromCheckout!.flatNo.toString()+addressResultFromCheckout!.street.toString(),
          'to_lat':addressResultFromCheckout!.latitude,
          'to_lng':addressResultFromCheckout!.longitude,


        };}
      // print(widget.cartList.userWalletBalance < 0  ? getTotalAmount(cartList) : totalAmount);
      // print(getTotalAmount());
      // print(totalAmount);
      // print("total" + double.parse(cart!.totalPrice!
      //     .toString()).toString());
      Navigator.pop(context, checkOutResult);

     print("cartcontent body"+cartContentBody.toString());
      // context
      //     .read(orderBuyItemsNotifier.notifier)
      //     .buyItemsOrderPlaceFromCart(body: cartContentBody);
    // Navigator.pop(context,);
    }




  }

  @override
  void dispose() {
    super.dispose();
  }
}
