import 'package:app24_user_app/modules/home.dart';
import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/api/api_response_state2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class CancelOrderNotifier extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  CancelOrderNotifier(this._apiRepository) : super(ResponseState2.initial());

  Future<void> cancelOrder(
      {bool init = true,required Map body,required BuildContext context}) async {
    try {
      if (init) state = state.copyWith(isLoading: true);
      final cancelOrder = await _apiRepository.fetchCancelOrder(body: body);
      state = state.copyWith(
          response: cancelOrder, isLoading: false, isError: false);
      // if(cancelOrder.statusCode == "200")
      //   Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomePage()));
    } catch (e) {
      print(e.toString());
      state = state.copyWith(
          errorMessage: e.toString(), isError: true, isLoading: false);
    }
  }
}