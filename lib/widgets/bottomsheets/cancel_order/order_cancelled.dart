import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/modules/home.dart';
import 'package:app24_user_app/widgets/common_main_button.dart';
import 'package:flutter/material.dart';

class OrderCancelledBottomSheet extends StatefulWidget {
  const OrderCancelledBottomSheet({Key? key}) : super(key: key);

  @override
  _OrderCancelledBottomSheetState createState() =>
      _OrderCancelledBottomSheetState();
}

class _OrderCancelledBottomSheetState extends State<OrderCancelledBottomSheet> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Center(child: Text("Your order has been cancelled.")),
          const SizedBox(height: 20,),
          CommonButton(
            onTap: (){

              Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage()));
            },
            bgColor: App24Colors.greenOrderEssentialThemeColor,
            text: "Go back to Home",
          )
        ],
      ),
    ));
  }
}
