CancelOrderModel1 cancelOrderModelFromJson(str) {
  return CancelOrderModel1.fromJson(str);
}



class CancelOrderModel1 {
  String? statusCode;
  String? title;
  String? message;
  CancelOrderResponseData? responseData;
  List<dynamic>? error;

  CancelOrderModel1(
      {this.statusCode,
        this.title,
        this.message,
        this.responseData,
        this.error});

  CancelOrderModel1.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    responseData = json['responseData'] != null
        ? new CancelOrderResponseData.fromJson(json['responseData'])
        : null;
    if (json['error'] != null) {
      error = [];
      json['error'].forEach((v) {
        error?.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['title'] = this.title;
    data['message'] = this.message;
    if (this.responseData != null) {
      data['responseData'] = this.responseData?.toJson();
    }
    if (this.error != null) {
      data['error'] = this.error?.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CancelOrderResponseData {
  dynamic id;
  String? adminService;
  String? storeOrderInvoiceId;
  dynamic ongoingAt;
  String? createdAt;
  String? status;
  String? geoFence;
  String? totalPayable;
  dynamic paid;
  dynamic providerId;
  dynamic userId;
  dynamic userAddressId;
  dynamic promocodeId;
  dynamic storeId;
  dynamic storeTypeId;
  dynamic abaId;
  dynamic providerVehicleId;
  dynamic cityId;
  dynamic countryId;
  String? note;
  String? description;
  dynamic routeKey;
  String? deliveryDate;
  String? pickupAddress;
  String? deliveryAddress;
  String? orderType;
  String? orderOtp;
  dynamic orderReadyTime;
  dynamic orderReadyStatus;
  dynamic userRated;
  dynamic providerRated;
  String? cancelledBy;
  String? cancelReason;
  String? currency;
  dynamic scheduleStatus;
  String? assignedAt;
  String? timezone;
  String? requestType;
  String? categoryId;
  double? pickupLat;
  double? pickupLng;
  dynamic pickupContactNum;
  dynamic deliveryLat;
  dynamic deliveryLng;
  dynamic deliveryContactNum;
  String? type;
  dynamic weight;
  String? paymentType;
  String? orderService;
  dynamic totalDistance;
  dynamic deliveryCharge;
  dynamic adminCommission;
  dynamic shopCommission;
  dynamic shopAmount;
  dynamic deliveryVehicleId;
  String? providerRejectId;
  dynamic onlinePaymentCharge;
  dynamic totalDuration;
  String? deliveryMode;
  dynamic waypoints;
  String? pickupPlaceId;
  String? paymentOption;
  String? dlvryBoyChrg;
  String? taxAmount;
  String? pickupNote;
  String? pickupPlacetext;
  dynamic isReturn;
  dynamic firstPayable;
  dynamic differencePayable;
  dynamic discount;
  dynamic shopDiscount;
  dynamic walletAmount;
  dynamic manualAssign;
  dynamic providerLat;
  dynamic providerLng;
  dynamic providerDistance;
  dynamic tatAt;
  String? startedAt;
  String? createdTime;
  String? assignedTime;
  Delivery? delivery;
  Pickup? pickup;

  CancelOrderResponseData(
      {this.id,
        this.adminService,
        this.storeOrderInvoiceId,
        this.ongoingAt,
        this.createdAt,
        this.status,
        this.geoFence,
        this.totalPayable,
        this.paid,
        this.providerId,
        this.userId,
        this.userAddressId,
        this.promocodeId,
        this.storeId,
        this.storeTypeId,
        this.abaId,
        this.providerVehicleId,
        this.cityId,
        this.countryId,
        this.note,
        this.description,
        this.routeKey,
        this.deliveryDate,
        this.pickupAddress,
        this.deliveryAddress,
        this.orderType,
        this.orderOtp,
        this.orderReadyTime,
        this.orderReadyStatus,
        this.userRated,
        this.providerRated,
        this.cancelledBy,
        this.cancelReason,
        this.currency,
        this.scheduleStatus,
        this.assignedAt,
        this.timezone,
        this.requestType,
        this.categoryId,
        this.pickupLat,
        this.pickupLng,
        this.pickupContactNum,
        this.deliveryLat,
        this.deliveryLng,
        this.deliveryContactNum,
        this.type,
        this.weight,
        this.paymentType,
        this.orderService,
        this.totalDistance,
        this.deliveryCharge,
        this.adminCommission,
        this.shopCommission,
        this.shopAmount,
        this.deliveryVehicleId,
        this.providerRejectId,
        this.onlinePaymentCharge,
        this.totalDuration,
        this.deliveryMode,
        this.waypoints,
        this.pickupPlaceId,
        this.paymentOption,
        this.dlvryBoyChrg,
        this.taxAmount,
        this.pickupNote,
        this.pickupPlacetext,
        this.isReturn,
        this.firstPayable,
        this.differencePayable,
        this.discount,
        this.shopDiscount,
        this.walletAmount,
        this.manualAssign,
        this.providerLat,
        this.providerLng,
        this.providerDistance,
        this.tatAt,
        this.startedAt,
        this.createdTime,
        this.assignedTime,
        this.delivery,
        this.pickup});

  CancelOrderResponseData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    adminService = json['admin_service'];
    storeOrderInvoiceId = json['store_order_invoice_id'];
    ongoingAt = json['ongoing_at'];
    createdAt = json['created_at'];
    status = json['status'];
    geoFence = json['geo_fence'];
    totalPayable = json['total_payable'];
    paid = json['paid'];
    providerId = json['provider_id'];
    userId = json['user_id'];
    userAddressId = json['user_address_id'];
    promocodeId = json['promocode_id'];
    storeId = json['store_id'];
    storeTypeId = json['store_type_id'];
    abaId = json['aba_id'];
    providerVehicleId = json['provider_vehicle_id'];
    cityId = json['city_id'];
    countryId = json['country_id'];
    note = json['note'];
    description = json['description'];
    routeKey = json['route_key'];
    deliveryDate = json['delivery_date'];
    pickupAddress = json['pickup_address'];
    deliveryAddress = json['delivery_address'];
    orderType = json['order_type'];
    orderOtp = json['order_otp'];
    orderReadyTime = json['order_ready_time'];
    orderReadyStatus = json['order_ready_status'];
    userRated = json['user_rated'];
    providerRated = json['provider_rated'];
    cancelledBy = json['cancelled_by'];
    cancelReason = json['cancel_reason'];
    currency = json['currency'];
    scheduleStatus = json['schedule_status'];
    assignedAt = json['assigned_at'];
    timezone = json['timezone'];
    requestType = json['request_type'];
    categoryId = json['category_id'];
    pickupLat = json['pickup_lat'];
    pickupLng = json['pickup_lng'];
    pickupContactNum = json['pickup_contact_num'];
    deliveryLat = json['delivery_lat'];
    deliveryLng = json['delivery_lng'];
    deliveryContactNum = json['delivery_contact_num'];
    type = json['type'];
    weight = json['weight'];
    paymentType = json['payment_type'];
    orderService = json['order_service'];
    totalDistance = json['total_distance'];
    deliveryCharge = json['delivery_charge'];
    adminCommission = json['admin_commission'];
    shopCommission = json['shop_commission'];
    shopAmount = json['shop_amount'];
    deliveryVehicleId = json['delivery_vehicle_id'];
    providerRejectId = json['provider_reject_id'];
    onlinePaymentCharge = json['online_payment_charge'];
    totalDuration = json['total_duration'];
    deliveryMode = json['delivery_mode'];
    waypoints = json['waypoints'];
    pickupPlaceId = json['pickup_place_id'];
    paymentOption = json['payment_option'];
    dlvryBoyChrg = json['dlvry_boy_chrg'];
    taxAmount = json['tax_amount'];
    pickupNote = json['pickup_note'];
    pickupPlacetext = json['pickup_placetext'];
    isReturn = json['is_return'];
    firstPayable = json['first_payable'];
    differencePayable = json['difference_payable'];
    discount = json['discount'];
    shopDiscount = json['shop_discount'];
    walletAmount = json['wallet_amount'];
    manualAssign = json['manual_assign'];
    providerLat = json['provider_lat'];
    providerLng = json['provider_lng'];
    providerDistance = json['provider_distance'];
    tatAt = json['tat_at'];
    startedAt = json['started_at'];
    createdTime = json['created_time'];
    assignedTime = json['assigned_time'];
    delivery = json['delivery'] != null
        ? new Delivery.fromJson(json['delivery'])
        : null;
    pickup =
    json['pickup'] != null ? new Pickup.fromJson(json['pickup']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['admin_service'] = this.adminService;
    data['store_order_invoice_id'] = this.storeOrderInvoiceId;
    data['ongoing_at'] = this.ongoingAt;
    data['created_at'] = this.createdAt;
    data['status'] = this.status;
    data['geo_fence'] = this.geoFence;
    data['total_payable'] = this.totalPayable;
    data['paid'] = this.paid;
    data['provider_id'] = this.providerId;
    data['user_id'] = this.userId;
    data['user_address_id'] = this.userAddressId;
    data['promocode_id'] = this.promocodeId;
    data['store_id'] = this.storeId;
    data['store_type_id'] = this.storeTypeId;
    data['aba_id'] = this.abaId;
    data['provider_vehicle_id'] = this.providerVehicleId;
    data['city_id'] = this.cityId;
    data['country_id'] = this.countryId;
    data['note'] = this.note;
    data['description'] = this.description;
    data['route_key'] = this.routeKey;
    data['delivery_date'] = this.deliveryDate;
    data['pickup_address'] = this.pickupAddress;
    data['delivery_address'] = this.deliveryAddress;
    data['order_type'] = this.orderType;
    data['order_otp'] = this.orderOtp;
    data['order_ready_time'] = this.orderReadyTime;
    data['order_ready_status'] = this.orderReadyStatus;
    data['user_rated'] = this.userRated;
    data['provider_rated'] = this.providerRated;
    data['cancelled_by'] = this.cancelledBy;
    data['cancel_reason'] = this.cancelReason;
    data['currency'] = this.currency;
    data['schedule_status'] = this.scheduleStatus;
    data['assigned_at'] = this.assignedAt;
    data['timezone'] = this.timezone;
    data['request_type'] = this.requestType;
    data['category_id'] = this.categoryId;
    data['pickup_lat'] = this.pickupLat;
    data['pickup_lng'] = this.pickupLng;
    data['pickup_contact_num'] = this.pickupContactNum;
    data['delivery_lat'] = this.deliveryLat;
    data['delivery_lng'] = this.deliveryLng;
    data['delivery_contact_num'] = this.deliveryContactNum;
    data['type'] = this.type;
    data['weight'] = this.weight;
    data['payment_type'] = this.paymentType;
    data['order_service'] = this.orderService;
    data['total_distance'] = this.totalDistance;
    data['delivery_charge'] = this.deliveryCharge;
    data['admin_commission'] = this.adminCommission;
    data['shop_commission'] = this.shopCommission;
    data['shop_amount'] = this.shopAmount;
    data['delivery_vehicle_id'] = this.deliveryVehicleId;
    data['provider_reject_id'] = this.providerRejectId;
    data['online_payment_charge'] = this.onlinePaymentCharge;
    data['total_duration'] = this.totalDuration;
    data['delivery_mode'] = this.deliveryMode;
    data['waypoints'] = this.waypoints;
    data['pickup_place_id'] = this.pickupPlaceId;
    data['payment_option'] = this.paymentOption;
    data['dlvry_boy_chrg'] = this.dlvryBoyChrg;
    data['tax_amount'] = this.taxAmount;
    data['pickup_note'] = this.pickupNote;
    data['pickup_placetext'] = this.pickupPlacetext;
    data['is_return'] = this.isReturn;
    data['first_payable'] = this.firstPayable;
    data['difference_payable'] = this.differencePayable;
    data['discount'] = this.discount;
    data['shop_discount'] = this.shopDiscount;
    data['wallet_amount'] = this.walletAmount;
    data['manual_assign'] = this.manualAssign;
    data['provider_lat'] = this.providerLat;
    data['provider_lng'] = this.providerLng;
    data['provider_distance'] = this.providerDistance;
    data['tat_at'] = this.tatAt;
    data['started_at'] = this.startedAt;
    data['created_time'] = this.createdTime;
    data['assigned_time'] = this.assignedTime;
    if (this.delivery != null) {
      data['delivery'] = this.delivery?.toJson();
    }
    if (this.pickup != null) {
      data['pickup'] = this.pickup?.toJson();
    }
    return data;
  }
}

class Delivery {
  String? mapAddress;

  Delivery({this.mapAddress});

  Delivery.fromJson(Map<String, dynamic> json) {
    mapAddress = json['map_address'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['map_address'] = this.mapAddress;
    return data;
  }
}

class Pickup {
  String? storeName;
  String? storeLocation;
  String? storetypeName;
  Storetype? storetype;

  Pickup(
      {this.storeName, this.storeLocation, this.storetypeName, this.storetype});

  Pickup.fromJson(Map<String, dynamic> json) {
    storeName = json['store_name'];
    storeLocation = json['store_location'];
    storetypeName = json['storetype_name'];
    storetype = json['storetype'] != null
        ? new Storetype.fromJson(json['storetype'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['store_name'] = this.storeName;
    data['store_location'] = this.storeLocation;
    data['storetype_name'] = this.storetypeName;
    if (this.storetype != null) {
      data['storetype'] = this.storetype?.toJson();
    }
    return data;
  }
}

class Storetype {
  String? category;

  Storetype({this.category});

  Storetype.fromJson(Map<String, dynamic> json) {
    category = json['category'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['category'] = this.category;
    return data;
  }
}
