import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/helpers.dart';
import 'package:app24_user_app/modules/home.dart';
import 'package:app24_user_app/modules/tabs/home/home_tab.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/widgets/bottomsheets/cancel_order/order_cancelled.dart';
import 'package:app24_user_app/widgets/common_main_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class CancelOrderBottomSheet extends StatefulWidget {
  const CancelOrderBottomSheet({Key? key, this.orderId, this.orderDetailId}) : super(key: key);
  final int? orderId;
  final String? orderDetailId;

  @override
  _CancelOrderBottomSheetState createState() => _CancelOrderBottomSheetState();
}

class _CancelOrderBottomSheetState extends State<CancelOrderBottomSheet> {
  cancelOrder() {
    Navigator.pop(context,"cancelled");
    context
        .read(orderDetailsNotifierProvider.notifier)
        .getOrderDetails();
    // context
    //     .read(orderDetailsNotifierProvider.notifier)
    //     .getOrderDetails(id: widget.orderDetailId == null ? null : widget.orderDetailId);


    // Navigator.push(
    //   context,
    //   MaterialPageRoute(
    //       builder: (context) => HomePage()),
    // );
    // Helpers().getCommonBottomSheet(
    //     context: context, content: OrderCancelledBottomSheet());
    Map body = {
      "order_id": widget.orderId,
    };
    print("abcdefg"+widget.orderId.toString());
    context.read(cancelOrderNotifier.notifier).cancelOrder(
        body: body,context: context
    );
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const SizedBox(
              height: 20,
            ),
            Center(
                child: Text(
                  "Are you sure, you want to cancel your order? ",
                  style: TextStyle(fontWeight: FontWeight.w500,fontSize: MediaQuery.of(context).size.width/25),
                )),
            const SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: CommonButton(
                    onTap: () {
                      // Navigator.pop(context,"orderCancelled");
                      cancelOrder();
                    },
                    bgColor: App24Colors.greenOrderEssentialThemeColor,
                    text: "Yes",
                  ),
                ),
                const SizedBox(width: 15,),
                Expanded(
                  child: CommonButton(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    bgColor: App24Colors.greenOrderEssentialThemeColor,
                    text: "No",
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
