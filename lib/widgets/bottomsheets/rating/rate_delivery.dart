import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/modules/home.dart';
import 'package:app24_user_app/modules/restaurant/restaurant_cart_page.dart';
import 'package:app24_user_app/modules/tabs/home/home_tab.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/widgets/common_main_button.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class RateDeliveryBoy extends StatefulWidget {
  const RateDeliveryBoy({Key? key, this.deliveryBoyName, required this.orderId,this.shopName}) : super(key: key);
  final String? deliveryBoyName;
  final int orderId;
  final String? shopName;

  @override
  _RateDeliveryBoyState createState() => _RateDeliveryBoyState();
}

class _RateDeliveryBoyState extends State<RateDeliveryBoy> {

  late double _orderRating;
  late double _deliveryBoyRating;

  double _initialRating = 0.0;

  @override
  void initState() {
    super.initState();
    _deliveryBoyRating = _initialRating;
    _orderRating = _initialRating;
  }
  // rateDelivery(){
  //   Map body = {
  //     'order_id' : widget.orderId,
  //     "rating" : 3
  //   };
  //   context.read(ratingProvider.notifier).getRating(body :body);
  // }

  loadData({bool init = true, required BuildContext context}){

  }


  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
        child: Column(
          // crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            if(widget.shopName != "send")
            Text("How was your order from ${widget.shopName}?"),
            if(widget.shopName != "send")
            Center(
              child: RatingBar.builder(
                itemSize: 40,
                initialRating: _initialRating,
                direction: Axis.horizontal,
                allowHalfRating: true,
                itemCount: 5,
                itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                itemBuilder: (context, _) => Icon(
                  Icons.star,
                  color: Colors.amber,
                ),
                onRatingUpdate: (rating) {
                  setState(() {
                    _deliveryBoyRating = rating;
                  });
                },
                updateOnDrag: true,
              ),
            ),
            // const SizedBox(height: 10,),
            Divider(),
            Container(
              // decoration: BoxDecoration(
              //     borderRadius: BorderRadius.circular(50),
              //     boxShadow: [
              //       BoxShadow(
              //         color: Colors.grey[300]!,
              //         spreadRadius: 4,
              //         blurRadius: 5,
              //       )
              //     ]),
              // child: ClipRRect(
              //   borderRadius: BorderRadius.circular(50),
                child: Image.asset(
                  App24UserAppImages.dropLogo,
                  height: 70,
                  width: 150,
                  fit: BoxFit.cover,
                ),
              // ),
            ),
            const SizedBox(height: 10,),
            Center(
              child: Text(
                "How was " +widget.deliveryBoyName.toString()+ "'s delivery",
                // style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
              ),
            ),
            Center(
              child: RatingBar.builder(
                itemSize: 40,
                initialRating: _initialRating,
                direction: Axis.horizontal,
                allowHalfRating: true,
                itemCount: 5,
                itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                itemBuilder: (context, _) => Icon(
                  Icons.star,
                  color: Colors.amber,
                ),
                onRatingUpdate: (rating) {
                  setState(() {
                    _orderRating = rating;
                  });
                },
                updateOnDrag: true,
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            TextField(
              textInputAction: TextInputAction.done,
              maxLines: 2,
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                    borderSide: BorderSide(
                      width: 0,
                      style: BorderStyle.none,
                    ),
                  ),
                  errorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                  filled: true,
                  fillColor: Colors.grey[300]!,
                  focusColor: Colors.grey[300]!,
                  hintText: "Type your review"),
            ),
            const SizedBox(
              height: 15,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [

                Consumer(builder: (context, watch, child) {
                  final state = watch(ratingProvider);
                  if (state.isLoading) {
                    return CircularProgressIndicator.adaptive();
                  } else if (state.isError) {
                    {
                      return LoadingError(
                        onPressed: (res) {
                          loadData(init: true, context: res);
                        },
                        message: state.errorMessage,
                      );
                    }
                  } else {
                    return CommonButton(
                      onTap: () {
                        Map body = {
                          'order_id' : widget.orderId,
                          "rating" : _deliveryBoyRating,

                        };
                        context.read(ratingProvider.notifier).getRating(body :body);
                        // getCommonBottomSheet(context, RateOrder());
                        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomePage()));
                      },
                      bgColor: App24Colors.greenOrderEssentialThemeColor,
                      text: "Rate",
                    );
                  }
                })


,
                const SizedBox(
                  height: 10,
                ),
                TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    style: TextButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15),
                        ),
                        padding: EdgeInsets.symmetric(vertical: 15)),
                    child: Text(
                      "Maybe later",
                      style: TextStyle(color: App24Colors.darkTextColor),
                    ))
              ],
            )
          ],
        ),
      ),
    );
  }
}
