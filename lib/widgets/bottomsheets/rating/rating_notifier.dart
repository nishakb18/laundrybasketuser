import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/api/api_response_state2.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class RatingNotifier extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  RatingNotifier(this._apiRepository) : super(ResponseState2.initial());

  Future<void> getRating(
      {bool init = true,required Map body}) async {
    try {
      if (init) state = state.copyWith(isLoading: true);
      final rating = await _apiRepository.fetchRating(body: body);
      state = state.copyWith(
          response: rating, isLoading: false, isError: false);
    } catch (e) {
      print(e.toString());
      state = state.copyWith(
          errorMessage: e.toString(), isError: true, isLoading: false);
    }
  }
}