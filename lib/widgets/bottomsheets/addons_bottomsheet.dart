import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/helpers.dart';
import 'package:app24_user_app/modules/delivery/cart/cart_model.dart';
import 'package:app24_user_app/modules/delivery/item_list/item_list_model.dart'
    as itemModel;
import 'package:app24_user_app/modules/restaurant/restaurant_cart_page.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/widgets/common_main_button.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class AddonsBottomSheet extends StatefulWidget {
  final itemModel.Products? product;
  final BuildContext? context;
  final String? itemID;
  final String? shopName;
  final String? shopID;

  const AddonsBottomSheet({
    Key? key,
    this.product,
    this.context,
    required this.shopName,
    this.itemID,
    this.shopID,
  }) : super(key: key);

  @override
  _AddonsBottomSheetState createState() => _AddonsBottomSheetState();
}

class _AddonsBottomSheetState extends State<AddonsBottomSheet> {
  int? cartID = 0;
  /*List<bool>*/
//  bool _isChecked = false;
  int itemQuantity = 1;

  List<AddOnQuantity> selectedAddOnsList = [];
  List<int> cartList = new List.empty(growable: true);
  List<Carts> cartListData = new List.empty(growable: true);

  @override
  void initState() {
    SchedulerBinding.instance!.addPostFrameCallback((_) {
      //API call after screen build

      // context.read(cartNotifierProvider.notifier).getCartList();
      context.read(cartCountNotifierProvider.notifier).getCartCount();
      cartID = context.read(cartCountNotifierProvider.notifier).cartID;
      print("cartID idzrgarg:  " + cartID.toString());

/*if(context.read(cartNotifierProvider.notifier).cartIdList!=null)
      cartList=context.read(cartNotifierProvider.notifier).cartIdList!;*/
      //print("cartID idz:  "+cartID.toString());
      // context.read(cartCountNotifierProvider.notifier).getCartCount();
      //abc = context.read(itemListNotifierProvider.notifier).replaceCart(storeID: int.parse(widget.itemTypeID!));
      // context.read(cartNotifierProvider.notifier).getCartList();
    });

    super.initState();
    // _isChecked = List<bool>.filled(addOnsList.length, false);
    updateAddons();
  }

  loadData({bool init = true, required BuildContext context}) {}

  updateAddons() {
    widget.product!.itemsaddon!.forEach((element) {
      selectedAddOnsList.add(new AddOnQuantity(qty: 0, id: element.id));
    });
  }

  updateAddonsList({String? type, int? index}) {
    print(index);
    if (type == "remove") {
      if (selectedAddOnsList[index!].qty != 0) {
        setState(() {
          selectedAddOnsList[index].qty = selectedAddOnsList[index].qty! - 1;
          if (selectedAddOnsList[index].qty == 0)
            selectedAddOnsList[index].selected = false;
        });
      }
    } else {
      setState(() {
        selectedAddOnsList[index!].qty = selectedAddOnsList[index].qty! + 1;
      });
    }
  }

  List<Widget> getAddOns(context) {
    List<itemModel.Itemsaddon> addOnsList = widget.product!.itemsaddon!;
    // _isChecked = false /*List<bool>.filled(addOnsList.length, false)*/;
    List<Widget> _widgetAddons = [];
    for (var i = 0; i < addOnsList.length; i++) {
      var total = addOnsList[i].price! * selectedAddOnsList[i].qty!;
      _widgetAddons.add(Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // CheckboxListTile(
          //   title: Text(addOnsList[i].addonName),
          //   value: _isChecked,
          //   onChanged: (newValue) {
          //     setState(() {
          //       _isChecked = newValue;
          //     });
          //   },
          //   controlAffinity: ListTileControlAffinity.leading,  //  <-- leading Checkbox
          // ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Checkbox(
                    checkColor: Colors.greenAccent,
                    activeColor: Colors.red,
                    value: selectedAddOnsList[i].selected ?? false,
                    onChanged: (value) {
                      setState(() {
                        this.selectedAddOnsList[i].selected = value;
                        if (value == true && selectedAddOnsList[i].qty == 0)
                          selectedAddOnsList[i].qty =
                              selectedAddOnsList[i].qty! + 1;
                        if (value == false) selectedAddOnsList[i].qty = 0;
                        // if(selectedAddOnsList[i].qty == 1)
                        //   value = true;
                      });
                    },
                  ),
                  Text(
                    addOnsList[i].addonName ?? "",
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: MediaQuery.of(context).size.width / 27),
                  ),
                ],
              ),
              Text(
                "AED " + total.toString(),
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: MediaQuery.of(context).size.width / 27),
              ),
              Container(
                decoration: BoxDecoration(
                    color: Color(0xffFFECE9),
                    borderRadius: BorderRadius.circular(15)),
                child: Row(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Material(
                      borderRadius: BorderRadius.all(Radius.circular(50)),
                      color: Colors.transparent,
                      // shadowColor: Colors.grey[400],
                      // elevation: 3,
                      child: InkWell(
                        splashColor: Colors.red,
                        borderRadius: BorderRadius.all(Radius.circular(50)),
                        onTap: () {
                          updateAddonsList(type: "remove", index: i);
                          /*  if (addQuantity[i].qty == 1) {
                              addQuantity.removeAt(i);
                            } else
                              addQuantity[i].qty -= 1;*/
                        },
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal:
                                  MediaQuery.of(context).size.width / 45,
                              vertical:
                                  MediaQuery.of(context).padding.vertical / 30),
                          child: Text(
                            "-",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize:
                                    MediaQuery.of(context).size.width / 15,
                                color: Colors.red),
                          ),
                        ),
                      ),
                    ),
                    Center(
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 5),
                        // width: 15,
                        alignment: Alignment.center,
                        child: Text(
                          selectedAddOnsList[i].qty.toString(),
                          style: TextStyle(
                              fontWeight: FontWeight.w700,
                              fontSize: MediaQuery.of(context).size.width / 26),
                        ),
                      ),
                    ),
                    Material(
                      // borderRadius: BorderRadius.all(Radius.circular(50)),

                      color: Colors.transparent,

                      child: InkWell(
                        splashColor: Colors.green,
                        borderRadius: BorderRadius.all(Radius.circular(50)),
                        onTap: () {
                          updateAddonsList(type: "add", index: i);
                          // updateCart(
                          //     cartItem: cartTileList[index], method: "add");
                        },
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal:
                                  MediaQuery.of(context).size.width / 55,
                              vertical:
                                  MediaQuery.of(context).padding.vertical / 30),
                          child: Text(
                            "+",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize:
                                    MediaQuery.of(context).size.width / 15,
                                color:
                                    App24Colors.greenOrderEssentialThemeColor),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          )
        ],
      ));
    }
    return _widgetAddons;
  }

  onAddCartPressed() {
    selectedAddOnsList = selectedAddOnsList
        .where((element) => element.selected ?? false)
        .toList();
    String addonString = '';
    selectedAddOnsList.forEach((element) {
      addonString += "${element.id}_${element.qty},";
    });
    if (addonString != '')
      addonString = addonString.substring(0, addonString.length - 1);

    Map body = {
      "qty": itemQuantity,
      // "repeat" : 3,
      "item_id": widget.itemID.toString().trim(),
      "addons": addonString,
      "to_lat":
          context.read(homeNotifierProvider.notifier).currentLocation.latitude,
      "to_lng":
          context.read(homeNotifierProvider.notifier).currentLocation.longitude,
      // "customize" : 2
    };
    // showMessage(context, "Item added",false,true);
    context.read(cartNotifierProvider.notifier).addCart(
        context: widget.context!,
        itemID: widget.itemID.toString(),
        bodys: body,
        init: true,
        itemAddons: addonString,
        itemQty: itemQuantity);
    print("addons bottom" +
        context.read(cartNotifierProvider.notifier).addResponse.toString());
    if (context.read(cartNotifierProvider.notifier).addResponse.toString() !=
        "far") {
      // context.read(cartCountNotifierProvider.notifier).getCartCount(context: context,init: true);
      if (context.read(cartNotifierProvider.notifier).cartIdList != null) {
        // print("taken from cartNotifierProvider");
        cartList = context.read(cartNotifierProvider.notifier).cartIdList!;
        cartListData = context.read(cartNotifierProvider.notifier).cartListData;
      } else {
        // print("taken from cartCountNotifierProvider");
        cartList =
            context.read(cartCountNotifierProvider.notifier).cartProductsId!;
        cartListData =
            context.read(cartCountNotifierProvider.notifier).cartListData;
      }

      print("cartList Data length" + cartListData.length.toString());
      for (int i = 0; i < cartListData.length; i++) {
        print("cartList " + cartListData[i].product!.itemName.toString());
        print("cartList " + cartListData[i].quantity.toString());
      }
      int cartCount = cartList.length;
      // for (int i = 0; i < cartList.length; i++)
      //   print("cartList " + cartList[i].toString());

      if (cartList.contains(int.parse(widget.itemID.toString()))) {
        print("item is already there in cart" + widget.itemID.toString());

        //showMessage(context, "Item added already", true, false);
        context.read(cartCountNotifierProvider.notifier).updateCartCount(
            count: /*cartCount==null?1:*/ cartCount,
            shopName: widget.shopName,
            storeID: widget.product!.storeId);

        // print("cartcount: "+cartCount.toString());
      } else {
        //  showMessage(context, "Item added!", false, false);
        print("item is not there in cart" + widget.itemID.toString());
        context.read(cartCountNotifierProvider.notifier).updateCartCount(
            count: /*cartCount==null?1: */ cartCount + 1,
            shopName: widget.shopName,
            storeID: widget.product!.storeId);
        // print("cartcount: "+(cartCount+1).toString());
      }

      showMessage(context, "Item added!", false, false);
      // ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Item added!"),duration: Duration(seconds: 1),backgroundColor: Colors.green));

      cartID = context.read(cartCountNotifierProvider.notifier).cartID;

      print("cart id after add" + cartID.toString());
    } else {
      showMessage(context, "Drop Location too far", true, false);
    }
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: [
            // if (widget.product!.itemsaddon!.isNotEmpty)
            //   Text(
            //     "AddOns",
            //     style: TextStyle(
            //         fontWeight: FontWeight.w700,
            //         fontSize: MediaQuery.of(context).size.width / 25,
            //         color: App24Colors.darkTextColor),
            //   ),
            const SizedBox(
              height: 10,
            ),
            if (widget.product!.itemsaddon!.isEmpty)
              Text(
                "Item Quantity",
                style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: MediaQuery.of(context).size.width / 25,
                    color: App24Colors.darkTextColor),
              ),
            if (widget.product!.itemsaddon!.isNotEmpty)
              Text(
                "Choose your add-ons",
                style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
              ),
            const SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 45),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  if (widget.product!.itemsaddon!.isNotEmpty)
                    Text(
                      "Items",
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: MediaQuery.of(context).size.width / 25),
                    ),
                  if (widget.product!.itemsaddon!.isNotEmpty)
                    Text(
                      "Price",
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: MediaQuery.of(context).size.width / 25),
                    ),
                  if (widget.product!.itemsaddon!.isNotEmpty)
                    Text(
                      "Quantity",
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: MediaQuery.of(context).size.width / 25),
                    )
                ],
              ),
            ),
            ListView(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              children: getAddOns(context),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Quantity",
                  style: TextStyle(fontWeight: FontWeight.w600),
                ),
                Container(
                  decoration: BoxDecoration(
                      color: Color(0xffFFECE9),
                      borderRadius: BorderRadius.circular(15)),
                  child: Row(
                    // mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Material(
                        borderRadius: BorderRadius.all(Radius.circular(50)),
                        color: Colors.transparent,
                        // shadowColor: Colors.grey[400],
                        // elevation: 3,
                        child: InkWell(
                          splashColor: Colors.red,
                          borderRadius: BorderRadius.all(Radius.circular(50)),
                          onTap: () {
                            setState(() {
                              if (itemQuantity != 1) itemQuantity -= 1;
                            });

                            // updateCart(
                            //     cartItem: cartTileList[index], method: "remove");
                          },
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal:
                                    MediaQuery.of(context).size.width / 45,
                                vertical:
                                    MediaQuery.of(context).padding.vertical /
                                        30),
                            child: Text(
                              "-",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize:
                                      MediaQuery.of(context).size.width / 15,
                                  color: Colors.red),
                            ),
                          ),
                        ),
                      ),
                      Center(
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 5),
                          // width: 15,
                          alignment: Alignment.center,
                          child: Text(
                            itemQuantity.toString(),
                            // cartTileList[index].quantity.toString(),
                            style: TextStyle(
                                fontWeight: FontWeight.w700,
                                fontSize:
                                    MediaQuery.of(context).size.width / 26),
                          ),
                        ),
                      ),
                      Material(
                        // borderRadius: BorderRadius.all(Radius.circular(50)),

                        color: Colors.transparent,

                        child: InkWell(
                          splashColor: Colors.green,
                          borderRadius: BorderRadius.all(Radius.circular(50)),
                          onTap: () {
                            setState(() {
                              itemQuantity += 1;
                            });
                            // updateCart(
                            //     cartItem: cartTileList[index], method: "add");
                          },
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal:
                                    MediaQuery.of(context).size.width / 55,
                                vertical:
                                    MediaQuery.of(context).padding.vertical /
                                        30),
                            child: Text(
                              "+",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize:
                                      MediaQuery.of(context).size.width / 15,
                                  color: App24Colors
                                      .greenOrderEssentialThemeColor),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            CommonButton(
              onTap: () {
                // showLog("cartID in list pge:: "+cartID.toString());
                if (cartID == 0) {
                  print("added here found");
                  onAddCartPressed();
                }

                /* if(cartID==null){
                  onAddCartPressed();
                }*/
                else if (int.parse(widget.shopID.toString()) != cartID) {
                  print("cartID" + cartID.toString());
                  print("storeID" + widget.shopID.toString());
                  // print("abcdef" + cartID.toString());
                  Helpers().getCommonAlertDialogBox(
                      content: Consumer(builder: (context, watch, child) {
                        final state = watch(itemListNotifierProvider);
                        if (state.isLoading) {
                          return CircularProgressIndicator();
                        } else if (state.isError) {
                          return LoadingError(
                            onPressed: (res) {
                              loadData(init: true, context: res);
                            },
                            message: state.errorMessage.toString(),
                          );
                        } else {
                          // restaurantItemList = state.response.responseData.products;
                          // restaurantItemUnSortedList.addAll(restaurantItemList);
                          return Container(
                            child: AlertDialog(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15)),
                              title: Text("Replace cart items?"),
                              content: Text(
                                "Your cart contains items from another shop. Do you want to discard those and add items from " +
                                    widget.shopName.toString().toLowerCase() +
                                    "?",
                                style: TextStyle(fontWeight: FontWeight.w600),
                              ),
                              actions: [
                                MaterialButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: Text("No"),
                                ),
                                MaterialButton(
                                  onPressed: () {
                                    // abc = context.read(itemListNotifierProvider.notifier).cartID;
                                    Navigator.pop(context);
                                    onAddCartPressed();
                                  },
                                  child: Text("Yes"),
                                )
                              ],
                            ),
                          );
                        }
                      }),
                      context: context);

//                   showDialog(
//                     context: context,
// // barrierDismissible: false,
//                     builder: (_) {
//                       return Consumer(
//                           builder: (context, watch, child) {
//                             final state =
//                             watch(itemListNotifierProvider);
//                             if (state.isLoading) {
//                               return CircularProgressIndicator();
//                             } else if (state.isError) {
//                               return LoadingError(
//                                 onPressed: (res) {
//                                   reloadData(
//                                       init: true, context: res);
//                                 },
//                                 message:
//                                 state.errorMessage.toString(),
//                               );
//                             } else {
//                               // restaurantItemList = state.response.responseData.products;
//                               // restaurantItemUnSortedList.addAll(restaurantItemList);
//                               return Container(
//                                 child: AlertDialog(
//                                   title:
//                                   Text("Replace cart items?"),
//                                   content: Text(
//                                     "Your cart contains items from another shop. Do you want to discard those and add items from " +
//                                         widget.shopName
//                                             .toString()
//                                             .toLowerCase() +
//                                         "?",
//                                     style: TextStyle(
//                                         fontWeight:
//                                         FontWeight.w600),
//                                   ),
//                                   actions: [
//                                     MaterialButton(
//                                       onPressed: () {
//                                         Navigator.pop(context);
//                                       },
//                                       child: Text("No"),
//                                     ),
//                                     MaterialButton(
//                                       onPressed: () {
//                                         // abc = context.read(itemListNotifierProvider.notifier).cartID;
//                                         Navigator.pop(context);
//                                         onAddCartPressed();
//
//                                       },
//                                       child: Text("Yes"),
//                                     )
//                                   ],
//                                 ),
//                               );
//                             }
//                           });
//                     },
//                   );
                } else {
                  onAddCartPressed();
                }
              },

//               onTap:() {
//                 print("asd"+widget.itemID.toString());
//                 // onAddCartPressed(widget.product!);
//                 showDialog(
//                   context: context,
// // barrierDismissible: false,
//                   builder: (_) {
//                     return Consumer(
//                         builder: (context, watch, child) {
//                           final state =
//                           watch(itemListNotifierProvider);
//                           if (state.isLoading) {
//                             return CircularProgressIndicator();
//                           } else if (state.isError) {
//                             return LoadingError(
//                               onPressed: (res) {
//                                 reloadData(
//                                     init: true, context: res);
//                               },
//                               message:
//                               state.errorMessage.toString(),
//                             );
//                           } else {
//                             // restaurantItemList = state.response.responseData.products;
//                             // restaurantItemUnSortedList.addAll(restaurantItemList);
//                             return Container(
//                               child: AlertDialog(
//                                 title:
//                                 Text("Replace cart items?"),
//                                 content: Text(
//                                   "Your cart contains items from other shop. Do you want to discard those and add items from " +
//                                       widget.shopName
//                                           .toString()
//                                           .toLowerCase() +
//                                       "?",style: TextStyle(fontWeight: FontWeight.w600),),
//                                 actions: [
//                                   MaterialButton(
//                                     onPressed: () {
//                                       Navigator.pop(context);
//                                     },
//                                     child: Text("No"),
//                                   ),
//                                   MaterialButton(
//                                     onPressed: () {
//
//                                       Navigator.pop(context);
//                                       onAddCartPressed(
//                                       );
//                                     },
//                                     child: Text("Yes"),
//                                   )
//                                 ],
//                               ),
//                             );
//                           }
//                         });
//                   },
//                 );
//                 },
              bgColor: App24Colors.redRestaurantThemeColor,
              text: "Confirm",
            )
          ],
        ),
      ),
    );
  }
}

class AddOnQuantity {
  int? qty;
  int? id;
  bool? selected = false;

  // bool select = true;
  AddOnQuantity({
    this.qty,
    this.id,
    this.selected,
  });
}
