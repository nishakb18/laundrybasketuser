import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/modules/tabs/home/home_tab.dart';
import 'package:app24_user_app/widgets/common_main_button.dart';
import 'package:app24_user_app/widgets/payment_waiting_screen.dart';
import 'package:app24_user_app/widgets/track_order.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OrderSuccess extends StatefulWidget {
  const OrderSuccess({Key? key, this.orderID, this.orderType}) : super(key: key);
  final int? orderID;
  final String? orderType;

  @override
  _OrderSuccessState createState() => _OrderSuccessState();
}

class _OrderSuccessState extends State<OrderSuccess> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: (){return Future.value(false);},
      child: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              // Center(child: Container(width:100,height:5,decoration: BoxDecoration(color: Colors.grey[300],borderRadius: BorderRadius.circular(10)),)),
              // const SizedBox(height: 10,),
              Center(
                child: Icon(
                  Icons.check_circle,
                  color: App24Colors.greenOrderEssentialThemeColor,
                  size: 100,
                ),
              ),
              Center(
                  child: Text(
                    "Success",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                  )),
              Center(
                  child: Text(
                    "Your order has been placed ",
                    style: TextStyle(
                        fontWeight: FontWeight.w500, color: Color(0xffA9A9A9)),
                  )),
              const SizedBox(height: 10,),
              CommonButton(
                onTap: (){
                  // Navigator.pushReplacement(
                  //   context,
                  //   MaterialPageRoute(
                  //       builder: (context) =>
                  //           PaymentWaiting(orderType: widget.orderType.toString(),orderID: widget.orderID!.toInt(),)),
                  // );



//                     paymentWaiting() async{
//     await Future.delayed(const Duration(seconds: 3), (){
//       Navigator.pushReplacement(
//         context,
//         MaterialPageRoute(
//             builder: (context) =>
// PaymentWaiting(orderType: widget.orderType.toString(),orderID: widget.orderID.toString(),)),
//       );}
//       );
//     // Navigator.pushReplacement(
//     //   context,
//     //   MaterialPageRoute(
//     //       builder: (context) => TrackOrder(
//     //         id: widget.orderID.toString(), orderType: widget.orderType.toString(),
//     //       )),
//     // );
//   }




                  // Navigator.pop(context);
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                        builder: (context) => TrackOrder(
                          isLaundryApp: true,
                          id: widget.orderID.toString(), orderType: widget.orderType.toString(),
                        )),
                  );
                },
                bgColor: App24Colors.greenOrderEssentialThemeColor,
                text: "Ok",
              )
            ],
          ),
        ),
      ),
    );
  }
}
