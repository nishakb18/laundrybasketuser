

import 'package:app24_user_app/constants/color_path.dart';
import 'package:flutter/material.dart';

class PRiceDifferenceBottomSheet extends StatefulWidget {
  const PRiceDifferenceBottomSheet({Key? key}) : super(key: key);

  @override
  _PRiceDifferenceBottomSheetState createState() => _PRiceDifferenceBottomSheetState();
}

class _PRiceDifferenceBottomSheetState extends State<PRiceDifferenceBottomSheet> {

  bool? use_wallet_checkout = false;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20,vertical: 15),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Bill Amount :",style: TextStyle(fontWeight: FontWeight.w500),),
                Text("AED 406",style: TextStyle(fontWeight: FontWeight.w500),),
              ],
            ),
            const SizedBox(height: 10,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Payment Received :",style: TextStyle(fontWeight: FontWeight.w500),),
                Text("AED 360",style: TextStyle(fontWeight: FontWeight.w500),),
              ],
            ),
            const SizedBox(height: 10,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Difference Amount :",style: TextStyle(fontWeight: FontWeight.w500),),
                Text("AED 36",style: TextStyle(fontWeight: FontWeight.w500),),
              ],
            ),
            const SizedBox(height: 10,),
            Row(
              // crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Checkbox(
                  checkColor: Colors.greenAccent,
                  activeColor: Colors.red,
                  value: this.use_wallet_checkout,
                  onChanged: (bool? value) {
                    setState(() {
                      this.use_wallet_checkout = value;
                    });
                  },
                ),
                Text(
                  "Use wallet",
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                ),
                Text(
                  "(AED 221.00)",
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                      color: App24Colors.greenOrderEssentialThemeColor),
                )
              ],
            ),
            Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    MaterialButton(
                      padding: EdgeInsets.symmetric(horizontal: 30),
                      color: Colors.green,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadiusDirectional.circular(10)),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text("Cancel",style: TextStyle(color: Colors.white),),
                    ),
                    MaterialButton(
                      padding: EdgeInsets.symmetric(horizontal: 40),
                      color: Colors.green,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadiusDirectional.circular(10)),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text("Pay",style: TextStyle(color: Colors.white),),
                    )
                  ],
                ))

          ],
        ),
      ),
    );
  }
}
