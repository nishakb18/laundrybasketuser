import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/helpers.dart';
import 'package:app24_user_app/widgets/bottomsheets/addressConfirmation/addressConfirmation.dart';
import 'package:app24_user_app/widgets/bottomsheets/checkout_bottomSheet/checkout_bottomsheet.dart';
import 'package:app24_user_app/widgets/common_main_button.dart';
import 'package:date_format/date_format.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class ScheduleBottomSheet extends StatefulWidget {
  const ScheduleBottomSheet(
      {Key? key,
      this.date,
      this.time,
      this.checkoutType,
      this.orderItemBody,
      this.fromRestaurantCart})
      : super(key: key);
  final String? date;
  final String? time;
  final String? checkoutType;
  final Map? orderItemBody;
  final String? fromRestaurantCart;

  @override
  _ScheduleBottomSheetState createState() => _ScheduleBottomSheetState();
}

class _ScheduleBottomSheetState extends State<ScheduleBottomSheet> {
  final scheduleDateController = TextEditingController();
  final scheduleTimeController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  onDeliverLater() {
    AlertDialog alert = AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      title: Center(
        child: Text("Schedule Delivery",
            style: TextStyle(
              fontSize: MediaQuery.of(context).size.width / 23,
              fontWeight: FontWeight.bold,
            )),
      ),
      content: StatefulBuilder(
          builder: (BuildContext context, StateSetter changeState) {
        return SingleChildScrollView(
          child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  const SizedBox(
                    height: 5,
                  ),
                  const Text(
                    "Schedule Date",
                    style: TextStyle(fontWeight: FontWeight.w600),
                  ),
                  // Helpers().getSubTitle("Schedule Date", context),
                  TextFormField(
                      onTap: () {
                        showDatePicker(
                          context: context,
                          initialDate: DateTime.now(),
                          firstDate: DateTime.now(),
                          lastDate: DateTime.now().add(const Duration(days: 5)),
                        ).then((value) {
                          if (value != null) {
                            changeState(() {
                              scheduleDateController.text =
                                  formatDate(value, [dd, '-', mm, '-', yyyy]);
                            });
                          }
                        });
                      },
                      textInputAction: TextInputAction.next,
                      controller: scheduleDateController,
                      validator: (String? arg) {
                        if (arg!.length == 0)
                          return 'Schedule Date can\'t be empty!';
                        else
                          return null;
                      },
                      readOnly: true,
                      style: const TextStyle(fontWeight: FontWeight.w600),
                      decoration: InputDecoration(
                          contentPadding: const EdgeInsets.symmetric(
                              vertical: 0, horizontal: 20),
                          enabledBorder: OutlineInputBorder(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(6.0)),
                            borderSide:
                                BorderSide(color: Colors.grey[300]!, width: 1),
                          ),
                          focusedBorder: const OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(6.0)),
                            borderSide: BorderSide(
                                color:
                                    App24Colors.greenOrderEssentialThemeColor,
                                width: 1),
                          ),
                          errorBorder: const OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(6.0)),
                            borderSide: BorderSide(color: Colors.red, width: 1),
                          ),
                          focusedErrorBorder: const OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(6.0)),
                            borderSide: BorderSide(color: Colors.red, width: 1),
                          ))),
                  const SizedBox(
                    height: 10,
                  ),
                  const Text(
                    "Schedule Time",
                    style: TextStyle(fontWeight: FontWeight.w600),
                  ),
                  TextFormField(
                      onTap: () {
                        showTimePicker(
                            initialTime: TimeOfDay.now(),
                            context: context,
                            builder: (BuildContext context, Widget? child) {
                              return MediaQuery(
                                data: MediaQuery.of(context)
                                    .copyWith(alwaysUse24HourFormat: false),
                                child: child!,
                              );
                            }).then((value) {
                          if (value != null) {
                            changeState(() {
                              scheduleTimeController.text =
                                  '${value.hour}:${value.minute} ';
                              /*_specifyDateList[i].from =
                                        '${value.hour}:${value.minute}';*/
                            });
                          }
                        });
                      },
                      textInputAction: TextInputAction.next,
                      controller: scheduleTimeController,
                      validator: (String? arg) {
                        if (arg!.length == 0)
                          return 'Schedule Time can\'t be empty!';
                        else
                          return null;
                      },
                      readOnly: true,
                      style: const TextStyle(fontWeight: FontWeight.w600),
                      decoration: InputDecoration(
                          contentPadding: const EdgeInsets.symmetric(
                              vertical: 0, horizontal: 20),
                          enabledBorder: OutlineInputBorder(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(6.0)),
                            borderSide:
                                BorderSide(color: Colors.grey[300]!, width: 1),
                          ),
                          focusedBorder: const OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(6.0)),
                            borderSide: BorderSide(
                                color:
                                    App24Colors.greenOrderEssentialThemeColor,
                                width: 1),
                          ),
                          errorBorder: const OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(6.0)),
                            borderSide: BorderSide(color: Colors.red, width: 1),
                          ),
                          focusedErrorBorder: const OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(6.0)),
                            borderSide: BorderSide(color: Colors.red, width: 1),
                          ))),
                  const SizedBox(
                    height: 35,
                  ),
                  Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                    Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(40),
                            color: App24Colors.greenOrderEssentialThemeColor,
                            boxShadow: [
                              BoxShadow(
                                  color: Theme.of(context)
                                      .primaryColorLight
                                      .withOpacity(0.5),
                                  spreadRadius: 2,
                                  blurRadius: 2,
                                  offset: Offset(0, 1))
                            ]),
                        child: Material(
                          color: Colors.transparent,
                          child: InkWell(
                              borderRadius: BorderRadius.circular(40),
                              onTap: () {
                                if (_formKey.currentState!.validate()) {
                                  Map scheduleItems = {
                                    'date': scheduleDateController.text,
                                    'time': scheduleTimeController.text
                                  };
                                  Navigator.pop(context, scheduleItems);
                                }
                              },
                              child: Container(
                                  alignment: Alignment.center,
                                  padding: const EdgeInsets.all(10),
                                  child: const Row(
                                    children: [
                                      SizedBox(
                                        width: 22,
                                      ),
                                      Text(
                                        "NEXT",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w600,
                                            fontSize: 17),
                                      ),
                                      SizedBox(
                                        width: 17,
                                      ),
                                      Icon(
                                        Icons.arrow_forward_ios,
                                        color: Colors.white,
                                      )
                                    ],
                                  ))),
                        )),
                  ]),
                ],
              )),
        );
      }),
    );
    return showGeneralDialog(
        barrierColor: Colors.black.withOpacity(0.5),
        transitionBuilder: (context, a1, a2, widget) {
          return Transform.scale(
            scale: a1.value,
            child: Opacity(
              opacity: a1.value,
              child: alert,
            ),
          );
        },
        transitionDuration: const Duration(milliseconds: 200),
        // barrierDismissible: true,
        barrierLabel: '',
        context: context,
        pageBuilder: (context, animation1, animation2) {
          return Container();
        }).then((value) {
      if (value != null) {
        Map scheduleMap = {
          'time': scheduleTimeController.text,
          'date': scheduleDateController.text
        };
        Navigator.pop(context, scheduleMap);
      }
    });

    //   showDialog(
    //   context: context,
    //   // barrierDismissible: false,
    //   builder: (BuildContext context) {
    //     return alert;
    //   },
    // ).then((value) {
    //   if (value != null) {
    //     Map scheduleMap = {
    //       'time': scheduleTimeController.text,
    //       'date': scheduleDateController.text
    //     };
    //     Navigator.pop(context, scheduleMap);
    //   }
    // });
  }

  callAddressConfirmationBottomSheet() {
    // var address =
    Helpers().getCommonBottomSheet(
        context: context, content: AddressConfirmationBottomSheet());
    // if (address != null)
    //   Navigator.pop(context,address);
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                CommonButton(
                  onTap: () {
                    onDeliverLater();
                  },
                  bgColor: /*widget.fromPage == "fromSend"
                      ? App24Colors.brownSendPackageThemeColor
                      : widget.fromPage == "restaurant"
                      ? App24Colors.redRestaurantThemeColor
                      :*/
                      App24Colors.greenOrderEssentialThemeColor,
                  text: "Pick Up Later",
                ),
                const SizedBox(
                  height: 25,
                ),
                CommonButton(
                  onTap: () {
                    Navigator.pop(context, 'deliver_now');
                    // Helpers().getCommonBottomSheet(
                    //         context: context,
                    //         content: CheckoutBottomSheet(
                    //           fromPage: widget.checkoutType, cartBody: widget.orderItemBody,
                    //         ),
                    //         title: "Checkout");
                    // showModalBottomSheet<dynamic>(
                    //   shape: RoundedRectangleBorder(
                    //     borderRadius: BorderRadius.only(
                    //         topLeft: Radius.circular(40),
                    //         topRight: Radius.circular(40)),
                    //   ),
                    //   // isScrollControlled: true,
                    //   // context: context,
                    //   // builder: (ctx) => ContactDeliveryBottomSheet(),
                    // );
                  },
                  bgColor: /*widget.fromPage == "fromSend"
                      ? App24Colors.brownSendPackageThemeColor
                      : widget.fromPage == "restaurant"
                      ? App24Colors.redRestaurantThemeColor
                      :*/
                      App24Colors.redRestaurantThemeColor,
                  text: "Pick Up Now",
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
