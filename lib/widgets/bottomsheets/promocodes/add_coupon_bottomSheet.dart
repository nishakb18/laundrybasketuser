import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/widgets/bottomsheets/promocodes/promocodes_model.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class AddCouponBottomSheet extends StatefulWidget {
  const AddCouponBottomSheet(
      {Key? key, this.promoCode, this.promoCodeDetails, this.cartShopID})
      : super(key: key);

  final String? promoCode;
  final String? promoCodeDetails;
  final int? cartShopID;

  @override
  _AddCouponBottomSheetState createState() => _AddCouponBottomSheetState();
}

class _AddCouponBottomSheetState extends State<AddCouponBottomSheet> {
  reloadData({required BuildContext context, required bool init}) {
    return context.read(promoCodesProvider.notifier).getPromoCodes(
          init: init,
        );
  }

  @override
  void initState() {
    SchedulerBinding.instance!.addPostFrameCallback((_) {
      //API call after screen build
      context.read(promoCodesProvider.notifier).getPromoCodes();
    });

    super.initState();
  }

  List<Widget> getCoupons(List<PromoCodesResponseData> promoCodes) {
    List<Widget> coupons = [];
    List<Widget> noCoupons = [] ;
    noCoupons.add(Text("no Coupons"));


    // if(widget.promoCode == null)
    for (var i = 0; i < promoCodes.length; i++) {

      // var active = widget.cartShopID != promoCodes[i].shopDetails!.id;
      // print("asd" + active.toString());

      promoCodes.sort((a, b) => a.promoCode!.compareTo(b.promoCode.toString()));
      if(widget.cartShopID == promoCodes[i].shopDetails!.id)
      coupons.add(Padding(
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: ListTile(
          contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
          isThreeLine: false,
          tileColor: Color(0xffE9FFEB),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
          onTap:  () {
                  Navigator.pop(context, promoCodes[i]);
                },
          title: Text(
            promoCodes[i].promoCode!,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),

          // subtitle: Column(
          //   crossAxisAlignment: CrossAxisAlignment.start,
          //   children: [
          //     Text(
          //       promoCodes[i].promoDescription!,
          //       style: TextStyle(fontWeight: FontWeight.w600),
          //     ),
          //     Text(
          //       "Details",
          //       style: TextStyle(
          //           fontWeight: FontWeight.w600,
          //           decoration: TextDecoration.underline),
          //     ),
          //     Text(
          //         "\u2022 Valid till ${promoCodes[i].expiration.toString().substring(0, 10)}"),
          //     Visibility(
          //         visible: promoCodes[i].minimumPurchase != 0,
          //         child: Text(
          //             "\u2022 On minimum purchase of AED${promoCodes[i].minimumPurchase.toString()}")),
          //   ],
          // ),

          // trailing:
          // Visibility(
          //   visible: widget.cartShopID != promoCodes[i].shopDetails!.id ,
          //   child: Row(
          //       mainAxisSize: MainAxisSize.min,
          //     children: [
          //       Icon(CupertinoIcons.clear_circled_solid),
          //       Text("Not applicable"),
          //     ],
          //   ),
          // ),
        ),
      ));
    }
    // if(widget.promoCode != null)
    //   Padding(
    //     padding: const EdgeInsets.symmetric(vertical: 10),
    //     child: ListTile(
    //       tileColor: Color(0xffE9FFEB),
    //       shape:
    //       RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
    //       onTap: () {
    //         Navigator.pop(context,widget.promoCode);
    //       },
    //       title: Text(widget.promoCode.toString(),style: TextStyle(fontWeight: FontWeight.bold),),
    //       // subtitle: Text(promoCodes[i].promoDescription!,style: TextStyle(fontWeight: FontWeight.w600),),
    //     ),
    //   );
    return coupons/* == null ? noCoupons : coupons*/;

  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
          child: Consumer(builder: (context, watch, child) {
            final state = watch(promoCodesProvider);
            if (state.isLoading) {
              return CircularProgressIndicator.adaptive();
            } else if (state.isError) {
              return LoadingError(
                onPressed: (res) {
                  reloadData(init: true, context: res);
                },
                message: state.errorMessage,
              );
            } else {
              if (widget.promoCode == null) {
                if(getCoupons(state.response.responseData).length == 0) {
                  return Text("No Coupons available");
                }else {
                  // print( getCoupons(state.response.responseData).length);
                  return Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      // mainAxisSize: MainAxisSize.min,
                      children: getCoupons(state.response.responseData));
                }
              } else {
                return Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      child: ListTile(
                        tileColor: Color(0xffE9FFEB),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15)),
                        onTap: () {
                          Navigator.pop(context, widget.promoCode);
                        },
                        title: Text(
                          widget.promoCode.toString(),
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        subtitle: Text(
                          widget.promoCodeDetails.toString(),
                          style: TextStyle(fontWeight: FontWeight.w600),
                        ),
                      ),
                    )
                  ],
                );
              }

            }
          })),
    );
  }
}
