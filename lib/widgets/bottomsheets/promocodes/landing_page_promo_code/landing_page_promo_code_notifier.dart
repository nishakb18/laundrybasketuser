import 'package:app24_user_app/constants/shared_preferences_path.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/api/api_response_state2.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LandingPagePromoCodeNotifier extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  LandingPagePromoCodeNotifier(this._apiRepository)
      : super(ResponseState2(isLoading: true));

  Future<void> getLandingPromoCodes(
      {bool init = true,
      required BuildContext context,
      String? lat,
      String? lng}) async {
    var prefs = await SharedPreferences.getInstance();
    var userRegStatus =
        prefs.getBool('${SharedPreferencesPath.isUserRegisteredKey}') ?? false;
    try {
      if (init) state = state.copyWith(isLoading: true);
      String latLong = '';
      // if (lat != null && lng != null)
      //   {
      //     latLong = '?lat=${lat.toString()}&'
      //           'lng=${lng.toString()}&recommended=';

      if (context
          .read(homeNotifierProvider.notifier)
          .currentLocation
          .latitude !=
          null &&
          context
              .read(homeNotifierProvider.notifier)
              .currentLocation
              .longitude !=
              null) {
        latLong =
        '?lat=${context
            .read(homeNotifierProvider.notifier)
            .currentLocation
            .latitude}&'
            'lng=${context
            .read(homeNotifierProvider.notifier)
            .currentLocation
            .longitude}&recommended=';
      }
      if (userRegStatus){
        final cuisines =
        await _apiRepository.fetchLandingPagePromoCodes(latLong: latLong);
      state =
          state.copyWith(response: cuisines, isLoading: false, isError: false);
    }

    } catch (e) {
      state = state.copyWith(
          errorMessage: e.toString(), isLoading: false, isError: true);
    }
  }
}
