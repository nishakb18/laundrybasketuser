LandingPagePromoCode landingPagePromoCodesModelFromJson(str) {
  return LandingPagePromoCode.fromJson(str);
}


class LandingPagePromoCode {
  String? statusCode;
  String? title;
  String? message;
  List<LandingPagePromoResponseData>? responseData;
  List<dynamic>? responseNewData;
  List<dynamic>? error;

  LandingPagePromoCode(
      {this.statusCode,
        this.title,
        this.message,
        this.responseData,
        this.responseNewData,
        this.error});

  LandingPagePromoCode.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    if (json['responseData'] != null) {
      responseData = [];
      json['responseData'].forEach((v) {
        responseData!.add(new LandingPagePromoResponseData.fromJson(v));
      });
    }
    if (json['responseNewData'] != null) {
      responseNewData =[];
      json['responseNewData'].forEach((v) {
        responseNewData!.add(v);
      });
    }
    if (json['error'] != null) {
      error = [];
      json['error'].forEach((v) {
        error!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['title'] = this.title;
    data['message'] = this.message;
    if (this.responseData != null) {
      data['responseData'] = this.responseData!.map((v) => v.toJson()).toList();
    }
    if (this.responseNewData != null) {
      data['responseNewData'] =
          this.responseNewData!.map((v) => v.toJson()).toList();
    }
    if (this.error != null) {
      data['error'] = this.error!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class LandingPagePromoResponseData {
  int? id;
  String? promoCode;
  dynamic lat;
  dynamic lng;
  int? rad;
  dynamic placeText;
  dynamic placeid;
  String? service;
  String? picture;
  int? percentage;
  int? maxAmount;
  int? minimumPurchase;
  String? promoDescription;
  String? startDate;
  String? expiration;
  int? timeApply;
  int? totalCount;
  int? userCount;
  String? storeMultiTypeId;
  int? allShops;
  String? selectedShops;
  int? shopPercentage;
  String? status;
  dynamic menuTypeId;
  int? shopId;
  String? title;
  Services? services;

  LandingPagePromoResponseData(
      {this.id,
        this.promoCode,
        this.lat,
        this.lng,
        this.rad,
        this.placeText,
        this.placeid,
        this.service,
        this.picture,
        this.percentage,
        this.maxAmount,
        this.minimumPurchase,
        this.promoDescription,
        this.startDate,
        this.expiration,
        this.timeApply,
        this.totalCount,
        this.userCount,
        this.storeMultiTypeId,
        this.allShops,
        this.selectedShops,
        this.shopPercentage,
        this.status,
        this.menuTypeId,
        this.shopId,
        this.title,
        this.services});

  LandingPagePromoResponseData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    promoCode = json['promo_code'];
    lat = json['lat'];
    lng = json['lng'];
    rad = json['rad'];
    placeText = json['place_text'];
    placeid = json['placeid'];
    service = json['service'];
    picture = json['picture'];
    percentage = json['percentage'];
    maxAmount = json['max_amount'];
    minimumPurchase = json['minimum_purchase'];
    promoDescription = json['promo_description'];
    startDate = json['start_date'];
    expiration = json['expiration'];
    timeApply = json['time_apply'];
    totalCount = json['total_count'];
    userCount = json['user_count'];
    storeMultiTypeId = json['store_multi_type_id'];
    allShops = json['all_shops'];
    selectedShops = json['selected_shops'];
    shopPercentage = json['shop_percentage'];
    status = json['status'];
    menuTypeId = json['menu_type_id'];
    shopId = json['shop_id'];
    title = json['title'];
    services = json['services'] != null
        ? new Services.fromJson(json['services'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['promo_code'] = this.promoCode;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['rad'] = this.rad;
    data['place_text'] = this.placeText;
    data['placeid'] = this.placeid;
    data['service'] = this.service;
    data['picture'] = this.picture;
    data['percentage'] = this.percentage;
    data['max_amount'] = this.maxAmount;
    data['minimum_purchase'] = this.minimumPurchase;
    data['promo_description'] = this.promoDescription;
    data['start_date'] = this.startDate;
    data['expiration'] = this.expiration;
    data['time_apply'] = this.timeApply;
    data['total_count'] = this.totalCount;
    data['user_count'] = this.userCount;
    data['store_multi_type_id'] = this.storeMultiTypeId;
    data['all_shops'] = this.allShops;
    data['selected_shops'] = this.selectedShops;
    data['shop_percentage'] = this.shopPercentage;
    data['status'] = this.status;
    data['menu_type_id'] = this.menuTypeId;
    data['shop_id'] = this.shopId;
    data['title'] = this.title;
    if (this.services != null) {
      data['services'] = this.services!.toJson();
    }
    return data;
  }
}

class Services {
  int? id;
  String? adminService;
  String? displayName;
  String? baseUrl;
  int? status;

  Services(
      {this.id,
        this.adminService,
        this.displayName,
        this.baseUrl,
        this.status});

  Services.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    adminService = json['admin_service'];
    displayName = json['display_name'];
    baseUrl = json['base_url'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['admin_service'] = this.adminService;
    data['display_name'] = this.displayName;
    data['base_url'] = this.baseUrl;
    data['status'] = this.status;
    return data;
  }
}
