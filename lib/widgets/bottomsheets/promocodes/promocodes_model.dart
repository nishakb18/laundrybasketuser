PromoCodesModel promoCodesModelFromJson(str) {
  return PromoCodesModel.fromJson(str);
}



class PromoCodesModel {
  String? statusCode;
  String? title;
  String? message;
  List<PromoCodesResponseData>? responseData;
  List<dynamic>? responseNewData;
  List<dynamic>? error;

  PromoCodesModel(
      {this.statusCode,
        this.title,
        this.message,
        this.responseData,
        this.responseNewData,
        this.error});

  PromoCodesModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    if (json['responseData'] != null) {
      responseData = [];
      json['responseData'].forEach((v) {
        responseData!.add(new PromoCodesResponseData.fromJson(v));
      });
    }
    if (json['responseNewData'] != null) {
      responseNewData = [];
      json['responseNewData'].forEach((v) {
        responseNewData!.add(v);
      });
    }
    if (json['error'] != null) {
      error = [];
      json['error'].forEach((v) {
        error!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['title'] = this.title;
    data['message'] = this.message;
    if (this.responseData != null) {
      data['responseData'] = this.responseData!.map((v) => v.toJson()).toList();
    }
    if (this.responseNewData != null) {
      data['responseNewData'] =
          this.responseNewData!.map((v) => v.toJson()).toList();
    }
    if (this.error != null) {
      data['error'] = this.error!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class PromoCodesResponseData {
  int? id;
  String? promoCode;
  String? picture;
  String? storeMultiTypeId;
  String? selectedShops;
  String? selectedItems;
  ShopDetails? shopDetails;

  PromoCodesResponseData(
      {this.id,
        this.promoCode,
        this.picture,
        this.storeMultiTypeId,
        this.selectedShops,
        this.selectedItems,
        this.shopDetails});

  PromoCodesResponseData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    promoCode = json['promo_code'];
    picture = json['picture'];
    storeMultiTypeId = json['store_multi_type_id'];
    selectedShops = json['selected_shops'];
    selectedItems = json['selected_items'];
    shopDetails = json['shop_details'] != null
        ? new ShopDetails.fromJson(json['shop_details'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['promo_code'] = this.promoCode;
    data['picture'] = this.picture;
    data['store_multi_type_id'] = this.storeMultiTypeId;
    data['selected_shops'] = this.selectedShops;
    data['selected_items'] = this.selectedItems;
    if (this.shopDetails != null) {
      data['shop_details'] = this.shopDetails!.toJson();
    }
    return data;
  }
}

class ShopDetails {
  int? id;
  int? storeTypeId;
  String? storeMultiTypeId;
  String? storeName;
  Storetype? storetype;

  ShopDetails(
      {this.id,
        this.storeTypeId,
        this.storeMultiTypeId,
        this.storeName,
        this.storetype});

  ShopDetails.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    storeTypeId = json['store_type_id'];
    storeMultiTypeId = json['store_multi_type_id'];
    storeName = json['store_name'];
    storetype = json['storetype'] != null
        ? new Storetype.fromJson(json['storetype'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['store_type_id'] = this.storeTypeId;
    data['store_multi_type_id'] = this.storeMultiTypeId;
    data['store_name'] = this.storeName;
    if (this.storetype != null) {
      data['storetype'] = this.storetype!.toJson();
    }
    return data;
  }
}

class Storetype {
  int? id;
  String? name;
  String? category;
  String? picture;
  int? status;
  String? itemsStatus;
  String? addonsStatus;
  String? keywords;
  int? visibility;

  Storetype(
      {this.id,
        this.name,
        this.category,
        this.picture,
        this.status,
        this.itemsStatus,
        this.addonsStatus,
        this.keywords,
        this.visibility});

  Storetype.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    category = json['category'];
    picture = json['picture'];
    status = json['status'];
    itemsStatus = json['items_status'];
    addonsStatus = json['addons_status'];
    keywords = json['keywords'];
    visibility = json['visibility'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['category'] = this.category;
    data['picture'] = this.picture;
    data['status'] = this.status;
    data['items_status'] = this.itemsStatus;
    data['addons_status'] = this.addonsStatus;
    data['keywords'] = this.keywords;
    data['visibility'] = this.visibility;
    return data;
  }
}





// class PromoCodesModel {
//   String? statusCode;
//   String? title;
//   String? message;
//   List<PromoCodesResponseData>? responseData;
//   List<dynamic>? responseNewData;
//   List<dynamic>? error;
//
//   PromoCodesModel(
//       {this.statusCode,
//         this.title,
//         this.message,
//         this.responseData,
//         this.responseNewData,
//         this.error});
//
//   PromoCodesModel.fromJson(Map<String, dynamic> json) {
//     statusCode = json['statusCode'];
//     title = json['title'];
//     message = json['message'];
//     if (json['responseData'] != null) {
//       responseData = [];
//       json['responseData'].forEach((v) {
//         responseData!.add(new PromoCodesResponseData.fromJson(v));
//       });
//     }
//     if (json['responseNewData'] != null) {
//       responseNewData = [];
//       json['responseNewData'].forEach((v) {
//         responseNewData!.add(v);
//       });
//     }
//     if (json['error'] != null) {
//       error = [];
//       json['error'].forEach((v) {
//         error!.add(v);
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['statusCode'] = this.statusCode;
//     data['title'] = this.title;
//     data['message'] = this.message;
//     if (this.responseData != null) {
//       data['responseData'] = this.responseData!.map((v) => v.toJson()).toList();
//     }
//     if (this.responseNewData != null) {
//       data['responseNewData'] =
//           this.responseNewData!.map((v) => v.toJson()).toList();
//     }
//     if (this.error != null) {
//       data['error'] = this.error!.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }
//
// class PromoCodesResponseData {
//   dynamic id;
//   String? promoCode;
//   dynamic lat;
//   dynamic lng;
//   dynamic rad;
//   String? placeText;
//   String? placeid;
//   String? service;
//   String? picture;
//   dynamic percentage;
//   dynamic maxAmount;
//   dynamic minimumPurchase;
//   String? promoDescription;
//   String? startDate;
//   String? expiration;
//   dynamic timeApply;
//   dynamic totalCount;
//   dynamic userCount;
//   String? storeMultiTypeId;
//   dynamic allShops;
//   String? selectedShops;
//   dynamic shopPercentage;
//   String? status;
//   ShopDetails? shopDetails;
//
//   PromoCodesResponseData(
//       {this.id,
//         this.promoCode,
//         this.lat,
//         this.lng,
//         this.rad,
//         this.placeText,
//         this.placeid,
//         this.service,
//         this.picture,
//         this.percentage,
//         this.maxAmount,
//         this.minimumPurchase,
//         this.promoDescription,
//         this.startDate,
//         this.expiration,
//         this.timeApply,
//         this.totalCount,
//         this.userCount,
//         this.storeMultiTypeId,
//         this.allShops,
//         this.selectedShops,
//         this.shopPercentage,
//         this.status,
//         this.shopDetails});
//
//   PromoCodesResponseData.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     promoCode = json['promo_code'];
//     lat = json['lat'];
//     lng = json['lng'];
//     rad = json['rad'];
//     placeText = json['place_text'];
//     placeid = json['placeid'];
//     service = json['service'];
//     picture = json['picture'];
//     percentage = json['percentage'];
//     maxAmount = json['max_amount'];
//     minimumPurchase = json['minimum_purchase'];
//     promoDescription = json['promo_description'];
//     startDate = json['start_date'];
//     expiration = json['expiration'];
//     timeApply = json['time_apply'];
//     totalCount = json['total_count'];
//     userCount = json['user_count'];
//     storeMultiTypeId = json['store_multi_type_id'];
//     allShops = json['all_shops'];
//     selectedShops = json['selected_shops'];
//     shopPercentage = json['shop_percentage'];
//     status = json['status'];
//     shopDetails = json['shop_details'] != null
//         ? new ShopDetails.fromJson(json['shop_details'])
//         : null;
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['promo_code'] = this.promoCode;
//     data['lat'] = this.lat;
//     data['lng'] = this.lng;
//     data['rad'] = this.rad;
//     data['place_text'] = this.placeText;
//     data['placeid'] = this.placeid;
//     data['service'] = this.service;
//     data['picture'] = this.picture;
//     data['percentage'] = this.percentage;
//     data['max_amount'] = this.maxAmount;
//     data['minimum_purchase'] = this.minimumPurchase;
//     data['promo_description'] = this.promoDescription;
//     data['start_date'] = this.startDate;
//     data['expiration'] = this.expiration;
//     data['time_apply'] = this.timeApply;
//     data['total_count'] = this.totalCount;
//     data['user_count'] = this.userCount;
//     data['store_multi_type_id'] = this.storeMultiTypeId;
//     data['all_shops'] = this.allShops;
//     data['selected_shops'] = this.selectedShops;
//     data['shop_percentage'] = this.shopPercentage;
//     data['status'] = this.status;
//     if (this.shopDetails != null) {
//       data['shop_details'] = this.shopDetails!.toJson();
//     }
//     return data;
//   }
// }
//
// class ShopDetails {
//   dynamic id;
//   dynamic storeTypeId;
//   String? storeMultiTypeId;
//   String? storeName;
//   String? password;
//   dynamic exemptPayout;
//   String? managerMobile;
//   String? jwtToken;
//   String? email;
//   dynamic abaId;
//   dynamic mkUserId;
//   dynamic commonUserId;
//   String? upiId;
//   String? version;
//   String? storeLocation;
//   dynamic latitude;
//   dynamic longitude;
//   String? storeZipcode;
//   dynamic countryId;
//   dynamic cityId;
//   dynamic zoneId;
//   String? contactPerson;
//   String? contactNumber;
//   dynamic countryCode;
//   String? picture;
//   String? gallery;
//   String? deviceToken;
//   String? deviceId;
//   String? deviceType;
//   String? language;
//   dynamic storePackingCharges;
//   dynamic deliveryRadius;
//   dynamic storeGst;
//   dynamic commission;
//   String? adminCommissionLogic;
//   dynamic isBankdetail;
//   String? offerMinAmount;
//   dynamic offerPercent;
//   String? estimatedDeliveryTime;
//   dynamic rating;
//   dynamic otp;
//   String? description;
//   dynamic status;
//   dynamic freeDelivery;
//   String? currency;
//   String? currencySymbol;
//   dynamic walletBalance;
//   String? isVeg;
//   dynamic shopNegativeBalance;
//   String? createdAt;
//   dynamic commonUserid;
//   dynamic commonAddressid;
//   String? placeid;
//   dynamic priority;
//   String? priorityDesc;
//   String? shopGroup;
//   dynamic acceptOrder;
//   String? slug;
//   dynamic deliveryChargePercentage;
//   Storetype? storetype;
//
//   ShopDetails(
//       {this.id,
//         this.storeTypeId,
//         this.storeMultiTypeId,
//         this.storeName,
//         this.password,
//         this.exemptPayout,
//         this.managerMobile,
//         this.jwtToken,
//         this.email,
//         this.abaId,
//         this.mkUserId,
//         this.commonUserId,
//         this.upiId,
//         this.version,
//         this.storeLocation,
//         this.latitude,
//         this.longitude,
//         this.storeZipcode,
//         this.countryId,
//         this.cityId,
//         this.zoneId,
//         this.contactPerson,
//         this.contactNumber,
//         this.countryCode,
//         this.picture,
//         this.gallery,
//         this.deviceToken,
//         this.deviceId,
//         this.deviceType,
//         this.language,
//         this.storePackingCharges,
//         this.deliveryRadius,
//         this.storeGst,
//         this.commission,
//         this.adminCommissionLogic,
//         this.isBankdetail,
//         this.offerMinAmount,
//         this.offerPercent,
//         this.estimatedDeliveryTime,
//         this.rating,
//         this.otp,
//         this.description,
//         this.status,
//         this.freeDelivery,
//         this.currency,
//         this.currencySymbol,
//         this.walletBalance,
//         this.isVeg,
//         this.shopNegativeBalance,
//         this.createdAt,
//         this.commonUserid,
//         this.commonAddressid,
//         this.placeid,
//         this.priority,
//         this.priorityDesc,
//         this.shopGroup,
//         this.acceptOrder,
//         this.slug,
//         this.deliveryChargePercentage,
//         this.storetype});
//
//   ShopDetails.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     storeTypeId = json['store_type_id'];
//     storeMultiTypeId = json['store_multi_type_id'];
//     storeName = json['store_name'];
//     password = json['password'];
//     exemptPayout = json['exempt_payout'];
//     managerMobile = json['manager_mobile'];
//     jwtToken = json['jwt_token'];
//     email = json['email'];
//     abaId = json['aba_id'];
//     mkUserId = json['mk_user_id'];
//     commonUserId = json['common_user_id'];
//     upiId = json['upi_id'];
//     version = json['version'];
//     storeLocation = json['store_location'];
//     latitude = json['latitude'];
//     longitude = json['longitude'];
//     storeZipcode = json['store_zipcode'];
//     countryId = json['country_id'];
//     cityId = json['city_id'];
//     zoneId = json['zone_id'];
//     contactPerson = json['contact_person'];
//     contactNumber = json['contact_number'];
//     countryCode = json['country_code'];
//     picture = json['picture'];
//     gallery = json['gallery'];
//     deviceToken = json['device_token'];
//     deviceId = json['device_id'];
//     deviceType = json['device_type'];
//     language = json['language'];
//     storePackingCharges = json['store_packing_charges'];
//     deliveryRadius = json['delivery_radius'];
//     storeGst = json['store_gst'];
//     commission = json['commission'];
//     adminCommissionLogic = json['admin_commission_logic'];
//     isBankdetail = json['is_bankdetail'];
//     offerMinAmount = json['offer_min_amount'];
//     offerPercent = json['offer_percent'];
//     estimatedDeliveryTime = json['estimated_delivery_time'];
//     rating = json['rating'];
//     otp = json['otp'];
//     description = json['description'];
//     status = json['status'];
//     freeDelivery = json['free_delivery'];
//     currency = json['currency'];
//     currencySymbol = json['currency_symbol'];
//     walletBalance = json['wallet_balance'];
//     isVeg = json['is_veg'];
//     shopNegativeBalance = json['shop_negative_balance'];
//     createdAt = json['created_at'];
//     commonUserid = json['common_userid'];
//     commonAddressid = json['common_addressid'];
//     placeid = json['placeid'];
//     priority = json['priority'];
//     priorityDesc = json['priority_desc'];
//     shopGroup = json['shop_group'];
//     acceptOrder = json['accept_order'];
//     slug = json['slug'];
//     deliveryChargePercentage = json['delivery_charge_percentage'];
//     storetype = json['storetype'] != null
//         ? new Storetype.fromJson(json['storetype'])
//         : null;
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['store_type_id'] = this.storeTypeId;
//     data['store_multi_type_id'] = this.storeMultiTypeId;
//     data['store_name'] = this.storeName;
//     data['password'] = this.password;
//     data['exempt_payout'] = this.exemptPayout;
//     data['manager_mobile'] = this.managerMobile;
//     data['jwt_token'] = this.jwtToken;
//     data['email'] = this.email;
//     data['aba_id'] = this.abaId;
//     data['mk_user_id'] = this.mkUserId;
//     data['common_user_id'] = this.commonUserId;
//     data['upi_id'] = this.upiId;
//     data['version'] = this.version;
//     data['store_location'] = this.storeLocation;
//     data['latitude'] = this.latitude;
//     data['longitude'] = this.longitude;
//     data['store_zipcode'] = this.storeZipcode;
//     data['country_id'] = this.countryId;
//     data['city_id'] = this.cityId;
//     data['zone_id'] = this.zoneId;
//     data['contact_person'] = this.contactPerson;
//     data['contact_number'] = this.contactNumber;
//     data['country_code'] = this.countryCode;
//     data['picture'] = this.picture;
//     data['gallery'] = this.gallery;
//     data['device_token'] = this.deviceToken;
//     data['device_id'] = this.deviceId;
//     data['device_type'] = this.deviceType;
//     data['language'] = this.language;
//     data['store_packing_charges'] = this.storePackingCharges;
//     data['delivery_radius'] = this.deliveryRadius;
//     data['store_gst'] = this.storeGst;
//     data['commission'] = this.commission;
//     data['admin_commission_logic'] = this.adminCommissionLogic;
//     data['is_bankdetail'] = this.isBankdetail;
//     data['offer_min_amount'] = this.offerMinAmount;
//     data['offer_percent'] = this.offerPercent;
//     data['estimated_delivery_time'] = this.estimatedDeliveryTime;
//     data['rating'] = this.rating;
//     data['otp'] = this.otp;
//     data['description'] = this.description;
//     data['status'] = this.status;
//     data['free_delivery'] = this.freeDelivery;
//     data['currency'] = this.currency;
//     data['currency_symbol'] = this.currencySymbol;
//     data['wallet_balance'] = this.walletBalance;
//     data['is_veg'] = this.isVeg;
//     data['shop_negative_balance'] = this.shopNegativeBalance;
//     data['created_at'] = this.createdAt;
//     data['common_userid'] = this.commonUserid;
//     data['common_addressid'] = this.commonAddressid;
//     data['placeid'] = this.placeid;
//     data['priority'] = this.priority;
//     data['priority_desc'] = this.priorityDesc;
//     data['shop_group'] = this.shopGroup;
//     data['accept_order'] = this.acceptOrder;
//     data['slug'] = this.slug;
//     data['delivery_charge_percentage'] = this.deliveryChargePercentage;
//     if (this.storetype != null) {
//       data['storetype'] = this.storetype!.toJson();
//     }
//     return data;
//   }
// }
//
// class Storetype {
//   dynamic id;
//   String? name;
//   String? category;
//   String? picture;
//   dynamic status;
//   String? itemsStatus;
//   String? addonsStatus;
//   String? keywords;
//
//   Storetype(
//       {this.id,
//         this.name,
//         this.category,
//         this.picture,
//         this.status,
//         this.itemsStatus,
//         this.addonsStatus,
//         this.keywords});
//
//   Storetype.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     name = json['name'];
//     category = json['category'];
//     picture = json['picture'];
//     status = json['status'];
//     itemsStatus = json['items_status'];
//     addonsStatus = json['addons_status'];
//     keywords = json['keywords'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['name'] = this.name;
//     data['category'] = this.category;
//     data['picture'] = this.picture;
//     data['status'] = this.status;
//     data['items_status'] = this.itemsStatus;
//     data['addons_status'] = this.addonsStatus;
//     data['keywords'] = this.keywords;
//     return data;
//   }
// }
