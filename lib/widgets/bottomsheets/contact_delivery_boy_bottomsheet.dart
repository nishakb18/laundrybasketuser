import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/helpers.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/widgets/common_main_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:url_launcher/url_launcher.dart';

import 'chat_bottomSheet.dart';

class ContactDeliveryBottomSheet extends StatefulWidget {
  ContactDeliveryBottomSheet(
      {Key? key, this.deliveryBoyName, this.deliveryBoyNumber})
      : super(key: key);
  final String? deliveryBoyName;
  final String? deliveryBoyNumber;

  @override
  _ContactDeliveryBottomSheetState createState() =>
      _ContactDeliveryBottomSheetState();
}

class _ContactDeliveryBottomSheetState
    extends State<ContactDeliveryBottomSheet> {
  String? phoneNumber = "+91 9895227724";

  // Future<void> _makePhoneCall(String number) async {
  //   final Uri launchUri = Uri(
  //     scheme: 'tel',
  //     path: number,
  //   );
  //   await launch(launchUri.toString());
  //   // if (await canLaunch(url)) {
  //   //   await launch(url);
  //   // } else {
  //   //   throw 'Could not launch $url';
  //   // }
  // }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: [
            Image.asset(
              App24UserAppImages.bSheetRiderAgent,
              width: 200,
            ),
            Text(
              widget.deliveryBoyName.toString(),
              style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
            ),
            Text(
              "Laundry Basket Delivery Agent",
              style: TextStyle(fontWeight: FontWeight.w600),
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                MaterialButton(
                  elevation: 0,
                  onPressed: () {
                    Clipboard.setData(ClipboardData(text: phoneNumber!));
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      content: Text("Phone Number copied to clipboard."),
                    ));
                    // _makePhoneCall('tel:${widget.deliveryBoyNumber}');
                  },
                  color: App24Colors.greenOrderEssentialThemeColor,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25)),
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Icon(
                        Icons.call,
                        size: 30,
                        color: Colors.white,
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      Text(
                        "Call",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  width: 20,
                ),
                // MaterialButton(
                //   elevation: 0,
                //   onPressed: () {
                //     Navigator.push(context, MaterialPageRoute(builder: (context) => ChatBottomSheet()));
                //     // getCommonBottomSheet(context, ChatBottomSheet());
                //   },
                //   color: Color(0xffF5FFF6),
                //   shape: RoundedRectangleBorder(
                //       borderRadius: BorderRadius.circular(25)),
                //   padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                //   child: Row(
                //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //     children: [
                //       Image.asset(
                //         App24UserAppImages.bSheetChat,
                //         width: 30,
                //       ),
                //       const SizedBox(
                //         width: 5,
                //       ),
                //       Text(
                //         "Chat",
                //         style: TextStyle(
                //             color: App24Colors.greenOrderEssentialThemeColor,
                //             fontSize: 20,
                //             fontWeight: FontWeight.bold),
                //       ),
                //     ],
                //   ),
                // )
              ],
            ),
            // Padding(
            //   padding:
            //       EdgeInsets.only(top: 30, left: 30, right: 30, bottom: 10),
            //   child: TextFormField(
            //     // autofocus: true,
            //     maxLines: 6,
            //     textInputAction: TextInputAction.next,
            //     decoration: InputDecoration(
            //       border: OutlineInputBorder(
            //         borderRadius: BorderRadius.circular(20),
            //         borderSide: BorderSide(
            //           width: 0,
            //           style: BorderStyle.none,
            //         ),
            //       ),
            //       contentPadding: EdgeInsets.all(25),
            //       hintText:
            //           'type additional delivery instructions (eg. leave package on the door)',
            //       hintStyle: TextStyle(fontWeight: FontWeight.w600),
            //       fillColor: Color(0xffF5FFF6),
            //       filled: true,
            //     ),
            //   ),
            // ),
            // Padding(
            //   padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
            //   child: Column(
            //     crossAxisAlignment: CrossAxisAlignment.stretch,
            //     children: [
            //       CommonButton(
            //         onTap: () {},
            //         bgColor: App24Colors.greenOrderEssentialThemeColor,
            //         text: "Add Instructions",
            //       )
            //     ],
            //   ),
            // ),
          ],
        ),
      ),
    );
  }
}
