import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/constants/global_constants.dart';
import 'package:app24_user_app/models/location_model.dart';
import 'package:app24_user_app/modules/my_account/address/add_update_address.dart';
import 'package:app24_user_app/modules/my_account/address/manage_address_model.dart';
import 'package:app24_user_app/providers/location_picker_provider.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/utils/services/database/address_database.dart';
import 'package:app24_user_app/utils/services/database/database_service.dart';
import 'package:app24_user_app/utils/services/database/location_database.dart';
import 'package:app24_user_app/widgets/select_location.dart';
import 'package:app24_user_app/widgets/select_location_address.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';

import '../../app24_user_icons.dart';
import '../../helpers.dart';

class SearchLocationBottomSheetPage extends StatefulWidget {
  const SearchLocationBottomSheetPage(
      {Key? key,
      this.initialCall,
      this.title,
      this.searchUserLocation,
      this.fromCart, this.showSearchMapOption = true})
      : super(key: key);

  final bool? initialCall;
  final String? title;
  final bool? searchUserLocation;
  final bool? fromCart;
  final bool? showSearchMapOption;

  @override
  State<StatefulWidget> createState() {
    return new _SearchLocationBottomSheetPageState();
  }
}

class _SearchLocationBottomSheetPageState
    extends State<SearchLocationBottomSheetPage> {
  final addressController = TextEditingController();
  final searchController = TextEditingController();
  final addressPopupTextFieldController = TextEditingController();

  late LocationNotifier _locationPickerProvider;

  List<LocationModel> recentSearchesList = [];
  List<AddressResponseData> addressList = [];
  int addressId = 0;
  AddressResponseData? address;
  bool showAddressTextField = false;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();

    getRecentSearches();
  }

  @override
  void dispose() {
    addressController.dispose();
    super.dispose();
  }

  Future<void> getRecentSearches() async {
    LocationDatabase locationDatabase =
        LocationDatabase(DatabaseService.instance);
    AddressDatabase addressDatabase = AddressDatabase(DatabaseService.instance);

    int? type;
    if (widget.title == GlobalConstants.labelPickupLocation) {
      type = 0;
    } else if (widget.title == GlobalConstants.labelDropLocation) {
      type = 1;
    } else if (widget.title == GlobalConstants.labelGlobalLocation) {
      type = 3;
    }

    await locationDatabase.getRecentSearches(type).then((value) {
      setState(() {
        recentSearchesList = value;
      });
    });
    await addressDatabase.fetchAddresses().then((value) {
      setState(() {
        addressList = value;
      });
    });
  }

  updateLatLong(
      LocationModel location, bool isSavedAddress, bool shouldNavigateToMap,
      {bool? isAlreadySavedAddress}) {
    print("location type: " + location.type.toString());
    if (location.latitude == null && location.longitude == null) {
      _locationPickerProvider
          .setLatLongFromPlaceId(location.placeID)
          .then((value) {
        if (value != null) {
          location.latitude = value['lat'];
          location.longitude = value['long'];

          navigateToMap(
              isAlreadySavedAddress: isAlreadySavedAddress,
              location: location,
              isSavedAddress: isSavedAddress,
              shouldNavigateToMap: shouldNavigateToMap);
        } else {
          showMessage(context, "Error occurred. Please try again.", true, true);
        }
      });
    } else {
      navigateToMap(
          isAlreadySavedAddress: isAlreadySavedAddress,
          location: location,
          isSavedAddress: isSavedAddress,
          shouldNavigateToMap: shouldNavigateToMap);
    }
  }

  addressPopUp(LocationModel location) {
    return Helpers().getCommonAlertDialogBox(
        barrierDismissible: true,
        content: AlertDialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          title: Center(child: Text("Enter address")),
          content: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Form(
                  key: _formKey,
                  child: TextFormField(
                      textCapitalization: TextCapitalization.sentences,
                      validator: (String? arg) {
                        if (arg!.length == 0)
                          return 'Please your extact address';
                      },
                      controller: addressPopupTextFieldController,
                      style: TextStyle(fontWeight: FontWeight.w500),
                      maxLines: 3,
                      // minLines: 2,
                      decoration: getDecoration(context).copyWith(
                          labelText: "Flat No/Building name/House No",
                          fillColor: Color(0xffededed),
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 10, horizontal: 15),
                          labelStyle: TextStyle(
                              fontSize:
                                  MediaQuery.of(context).size.width / 23))),
                ),
                const SizedBox(
                  height: 10,
                ),
                MaterialButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      updateLatLong(location, false, false);

                      location.description =
                          addressPopupTextFieldController.text +
                              " , " +
                              location.description.toString();
                      print("location description :" +
                          location.description.toString());

                      Navigator.pop(
                          context,
                          /*isCurrentLocation ? 'current_location' :*/
                          location);
                    }
                  },
                  child: Text(
                    "Proceed",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: App24Colors.darkTextColor,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                )
              ],
            ),
          ),
        ),
        context: context);
  }

  navigateToMap(
      {LocationModel? location,
      bool isCurrentLocation = false,
      required bool isSavedAddress,
      required bool shouldNavigateToMap,
      bool? isAlreadySavedAddress}) {
    // print("agandve"+  location!.type.toString());
    if (!widget.initialCall!) {
      Navigator.pop(context, isCurrentLocation ? 'current_location' : location);
    }
    if (shouldNavigateToMap == false) {
      if (isAlreadySavedAddress != true) {
        saveToDbAndServer(location);
      }

      print("title navigate to map : " + widget.title.toString());
      //if(widget.title!=GlobalConstants.labelGlobalLocation && widget.title!=GlobalConstants.labelDropLocation && widget.title!=GlobalConstants.labelPickupLocation)
      if (widget.title == GlobalConstants.labelFromCart)
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => AddUpdateAddressPage(
                      selectedLocation: location,
                      fromManageAddress: "fromManageAddress",
                      fromCart: "true",
                    )));
      else
        Navigator.pop(
            context, isCurrentLocation ? 'current_location' : location);
      // print("desc "+location!.description.toString());
      //  Navigator.pop(context, /*isCurrentLocation ? 'current_location' :*/ location);
    } else {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => SelectLocationAddressPage(
                    searchUserLocation: widget.searchUserLocation,
                    title: 'Enter Location',
                    fromManageAddress: "fromManageAddress",
                    isCurrentLocation: true,
                    fromCart: "true",
                  )));
      // Navigator.push(
      //   context,
      //   MaterialPageRoute(
      //       builder: (context) => SelectLocationPage(
      //             isSavedAddress: isSavedAddress,
      //             mainText: location?.main_text ?? "",
      //             locationAutoCompleteModel: location,
      //             title: widget.title!,
      //             isCurrentLocation: isCurrentLocation,
      //           )),
      // );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (context, watch, child) {
      _locationPickerProvider = watch(locationNotifierProvider.notifier);
      _locationPickerProvider.setInitialValues(context: context);
      return Column(mainAxisAlignment: MainAxisAlignment.end,
          children: [
        Material(
          color: Colors.black,
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(30), topLeft: Radius.circular(30)),
          child: Stack(
            children: [
              Container(
                height: MediaQuery.of(context).size.height * 0.8,
                alignment: Alignment.bottomCenter,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(30),
                        topLeft: Radius.circular(30)),
                    color: Colors.white),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    const SizedBox(
                      height: 6,
                    ),
                    // Center(child: Container(width: 100,height: 5,decoration: BoxDecoration(borderRadius: BorderRadius.circular(15),color: Colors.grey[300]),)),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                      child: Center(
                        child: Text(
                          "Search Location",
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    // Divider(),
                    //Text("commented line pending "),
                    // TextField(
                    //   controller: searchController,
                    //   style: TextStyle(fontWeight: FontWeight.w600),
                    //   decoration: InputDecoration(
                    //     border: OutlineInputBorder(
                    //       borderRadius: BorderRadius.circular(15),
                    //       borderSide: BorderSide(
                    //         width: 0,
                    //         style: BorderStyle.none,
                    //       ),
                    //     ),
                    //     fillColor: Color(0xffe5e5e5),
                    //     filled: true,
                    //     contentPadding: EdgeInsets.symmetric(vertical: 0),
                    //     prefixIcon: Icon(Icons.search),
                    //     hintText: "Search",
                    //   ),
                    //   onEditingComplete: () {
                    //     SystemChannels.textInput.invokeMethod('TextInput.hide');
                    //   },
                    // )
                    Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 6),
                      child: TypeAheadField(
                        hideOnEmpty: false,
                        // hideSuggestionsOnKeyboardHide: false,
                        // keepSuggestionsOnLoading: true,
                        // getImmediateSuggestions: true,
                        // keepSuggestionsOnSuggestionSelected: true,
                        // suggestionsBoxDecoration: SuggestionsBoxDecoration(
                        //   color: Colors.white,
                        //   borderRadius: BorderRadius.circular(8),
                        // ),
                        //  textFieldConfiguration: TextFieldConfiguration(
                        //   controller: searchController,
                        //   style: TextStyle(fontWeight: FontWeight.w600),
                        //   decoration: InputDecoration(
                        //     border: OutlineInputBorder(
                        //       borderRadius: BorderRadius.circular(15),
                        //       borderSide: BorderSide(
                        //         width: 0,
                        //         style: BorderStyle.none,
                        //       ),
                        //     ),
                        //     fillColor: Color(0xffe5e5e5),
                        //     filled: true,
                        //     contentPadding: EdgeInsets.symmetric(vertical: 0),
                        //     prefixIcon: Icon(Icons.search),
                        //     hintText: "Search",
                        //   ),
                        //   onEditingComplete: () {
                        //     SystemChannels.textInput
                        //         .invokeMethod('TextInput.hide');
                        //   },
                        // ),
                        builder: (context, controller, focusNode) {
                          return TextField(
                              controller: controller,
                              focusNode: focusNode,
                              autofocus: false,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(),
                                labelText: 'City',
                              )
                          );
                        },
                        itemBuilder: (context, LocationModel location) {
                          return ListTile(
                            leading: Icon(
                              CupertinoIcons.location_solid,
                              color: location
                                  .pinColor, //green - from local DB , orange - from server , red - from google
                            ),
                            title: Text(
                              location.main_text.toString(),
                              style: TextStyle(
                                  fontSize: 17, fontWeight: FontWeight.w600),
                            ),
                            subtitle: Text(
                              location.secondary_text.toString() == "null"
                                  ? ""
                                  : location.secondary_text.toString(),
                              maxLines: 2,
                            ),
                          );
                        },
                        onSelected: (LocationModel location) {
                          /* setState(() {
                            showAddressTextField=true;
                          });*/
                          print("home latitude" + location.latitude.toString());
                          FocusScopeNode currentFocus = FocusScope.of(context);

                          if (!currentFocus.hasPrimaryFocus) {
                            currentFocus.unfocus();
                          }

                          searchController.text = location.main_text!;

                          print("title" + widget.title.toString());

                          if (widget.title ==
                              GlobalConstants.labelGlobalLocation) {
                            location.type = 3;
                            updateLatLong(location, false, false);
                          } else if (widget.title ==
                                  GlobalConstants.labelDropLocation ||
                              widget.title ==
                                  GlobalConstants.labelPickupLocation) {
                            if (widget.title
                                .toString()
                                .contains("Pick up Location")) {
                              print("labelPickupLocation");
                              location.type = 0;
                            } else if (widget.title.toString() ==
                                "Drop Location") {
                              location.type = 1;
                              print("labelDropLocation");
                            }

                            addressPopUp(location);
                          } else
                            updateLatLong(location, false, false);
                          //
                        },
                        suggestionsCallback: (pattern) async {
                          print("pattern :" + pattern);
                          if (pattern.length > 2) {
                            return await _locationPickerProvider
                                .getSuggestions(pattern);
                          } else {
                            List<LocationModel> list = [];
                            return list;
                          }
                        },
                      ),
                    ),
                    /* Visibility(
                      visible: showAddressTextField,
                      child: Padding(
                        padding: EdgeInsets.only(left: 20,right: 20),
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Theme.of(context).accentColor,
                          ),
                          padding: EdgeInsets.all(13),
                          alignment: Alignment.center,
                          child:  TextFormField(
                            maxLines: 3,
                            validator: (String? arg) {
                              if(arg!.isEmpty)
                              {
                                return "Please provide your exact address";
                              }
                            },
                            controller: addressController,
                            // keyboardType: TextInputType.text,
                            //keyboardType: TextInputType.multiline,
                            //  textInputAction: TextInputAction.none,
                            */ /* inputFormatters: <TextInputFormatter?>[
                                  FilteringTextInputFormatter.allow(RegExp("[0-9]"))
                                ],*/ /*
                            decoration: InputDecoration(
                              hintText: "Address (House/Flat no/Building name)",
                            ),
                          ),
                        ),
                      ),
                    ),*/

                   Flexible(
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              if(widget.showSearchMapOption!) TextButton(
                              onPressed: () {
                                FocusScopeNode currentFocus =
                                    FocusScope.of(context);

                                if (!currentFocus.hasPrimaryFocus) {
                                  currentFocus.unfocus();
                                }
                                navigateToMap(
                                    isCurrentLocation: true,
                                    isSavedAddress: false,
                                    shouldNavigateToMap: true);
                              },
                              child: const Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Icon(
                                    Icons.my_location,
                                    color: App24Colors.darkTextColor,
                                  ),
                                  SizedBox(
                                    width: 7,
                                  ),
                                  Text(
                                    "Search in map",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        color: App24Colors.darkTextColor),
                                  )
                                ],
                              )),
                          if (addressList.isNotEmpty &&
                              widget.title != GlobalConstants.labelFromCart)
                            ListTile(
                              title: Text(
                                "Saved Addresses",
                                style: Theme.of(context).textTheme.titleSmall,
                              ),
                            ),
                          if (addressList.isNotEmpty &&
                              widget.title != GlobalConstants.labelFromCart)
                            Flexible(
                              child: ListView.builder(
                                reverse: true,
                                padding: EdgeInsets.all(0),
                                itemCount: addressList.length,
                                itemBuilder: (context, index) {
                                  return Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 15),
                                    child: Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(15),
                                        color: addressId ==
                                                addressList[index].addressid
                                            ? App24Colors.darkTextColor
                                            : Colors.white,
                                      ),
                                      child: ListTile(
                                        onTap: () {
                                          setState(() {
                                            addressId =
                                                addressList[index].addressid!;
                                            address = addressList[index];
                                          });
                                        },
                                        // selectedTileColor: App24Colors.darkTextColor,
                                        // selected: addressList[index].addressid == addressId,
                                        tileColor: App24Colors.darkTextColor,
                                        // addressId == addressList[index].addressid ? App24Colors.darkTextColor : Colors.white,
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(15)),
                                        leading: Icon(
                                          addressList[index].title == "Home"
                                              ? Icons.home_filled
                                              : addressList[index].title ==
                                                      "Work"
                                                  ? Icons.work
                                                  : Icons.flag,
                                          color: addressId ==
                                                  addressList[index].addressid
                                              ? Colors.white
                                              : App24Colors.darkTextColor,
                                        ),
                                        title: Text(
                                          addressList[index].title.toString(),
                                          style: TextStyle(
                                              color: addressId ==
                                                      addressList[index]
                                                          .addressid
                                                  ? Colors.white
                                                  : App24Colors.darkTextColor,
                                              fontWeight: FontWeight.w600),
                                        ),
                                        subtitle: Text(
                                          addressList[index]
                                                          .flatNo!
                                                          .toString() +
                                                      ", " +
                                                      addressList[index]
                                                          .landmark
                                                          .toString() ==
                                                  ''
                                              ? ""
                                              : addressList[index]
                                                      .landmark
                                                      .toString() +
                                                  ", " +
                                                  addressList[index]
                                                      .mapAddress!
                                                      .toString() +
                                                  ", " +
                                                  addressList[index]
                                                      .street!
                                                      .toString(),
                                          style: TextStyle(
                                              color: addressId ==
                                                      addressList[index]
                                                          .addressid
                                                  ? Colors.white
                                                  : App24Colors.darkTextColor,
                                              fontWeight: FontWeight.w400,
                                              fontSize:
                                                  13 /*MediaQuery.of(context).size.width / 25*/),
                                          overflow: TextOverflow.clip,
                                        ),
                                      ),
                                    ),
                                  );
                                  //   Container(
                                  //   width: MediaQuery.of(context).size.width *
                                  //       .85,
                                  //   decoration: BoxDecoration(
                                  //       boxShadow: [
                                  //         BoxShadow(
                                  //           color: Colors.grey[200]!,
                                  //           blurRadius: 10,
                                  //           spreadRadius: 5,
                                  //         )
                                  //       ],
                                  //       borderRadius: BorderRadius.circular(20),
                                  //       color: Colors.white),
                                  //   margin: EdgeInsets.symmetric(
                                  //       vertical: 20, horizontal: 18),
                                  //   child: Material(
                                  //     borderRadius: BorderRadius.circular(15),
                                  //     child: InkWell(
                                  //       borderRadius:
                                  //           BorderRadius.circular(15),
                                  //       onTap: () {
                                  //         updateLatLong(
                                  //             new LocationModel(
                                  //                 latitude: addressList[index]
                                  //                     .latitude,
                                  //                 longitude:
                                  //                     addressList[index]
                                  //                         .longitude),
                                  //             true);
                                  //         /*   if (widget.callFrom != null)
                                  //             Navigator.pop(context,
                                  //                 addressModel[i].flatNo! + addressModel[i].street!);*/
                                  //       },
                                  //       child: Column(
                                  //         crossAxisAlignment:
                                  //             CrossAxisAlignment.start,
                                  //         // crossAxisAlignment:
                                  //         //     CrossAxisAlignment.stretch,
                                  //         children: [
                                  //           const SizedBox(
                                  //             height: 5,
                                  //           ),
                                  //           Row(
                                  //             children: [
                                  //               Icon(addressList[index].title == "Home" ? Icons.home_filled : addressList[index].title == "Work" ? Icons.work : Icons.flag),
                                  //               Padding(
                                  //                 padding: EdgeInsets.only(
                                  //                     left: 20,
                                  //                     right: 17,
                                  //                     // top: 25,
                                  //                     bottom: 5),
                                  //                 child: Column(
                                  //                   crossAxisAlignment: CrossAxisAlignment.start,
                                  //                   children: [
                                  //                     Text(addressList[index].title.toString()),
                                  //                     Text(
                                  //                       addressList[index]
                                  //                                       .flatNo!
                                  //                                       .toString() +
                                  //                                   ", " +
                                  //                                   addressList[index]
                                  //                                       .landmark
                                  //                                       .toString() ==
                                  //                               ''
                                  //                           ? ""
                                  //                           : addressList[index]
                                  //                                   .landmark
                                  //                                   .toString() +
                                  //                               ", " +
                                  //                               addressList[index]
                                  //                                   .mapAddress!
                                  //                                   .toString() +
                                  //                               ", " +
                                  //                               addressList[index]
                                  //                                   .street!
                                  //                                   .toString(),
                                  //                       style: TextStyle(
                                  //                           fontWeight:
                                  //                               FontWeight.w600,
                                  //                           fontSize:
                                  //                               13 /*MediaQuery.of(context).size.width / 25*/),
                                  //                       overflow: TextOverflow.clip,
                                  //                     ),
                                  //                   ],
                                  //                 ),
                                  //               ),
                                  //             ],
                                  //           ),
                                  //         ],
                                  //       ),
                                  //     ),
                                  //   ),
                                  // );
                                },
                              ),
                            ),
                          if (recentSearchesList.isNotEmpty)
                            ListTile(
                              title: Text(
                                "Recent Searches",
                                style: Theme.of(context).textTheme.titleSmall
                              ),
                            ),
                          if (recentSearchesList.isNotEmpty)
                            Flexible(
                              child: ListView.builder(
                                padding: EdgeInsets.all(0),
                                itemCount: recentSearchesList.length,
                                itemBuilder: (context, index) {
                                  return ListTile(
                                    contentPadding: EdgeInsets.symmetric(
                                        horizontal: 15, vertical: 0),
                                    onTap: () {
                                      if (widget.title
                                          .toString()
                                          .contains("Pick up Location")) {
                                        recentSearchesList[index].type = 0;
                                        addressPopUp(recentSearchesList[index]);
                                      } else if (widget.title.toString() ==
                                          "Drop Location") {
                                        recentSearchesList[index].type = 1;
                                        addressPopUp(recentSearchesList[index]);
                                      } else if (widget.title.toString() ==
                                          GlobalConstants.labelGlobalLocation) {
                                        recentSearchesList[index].type = 3;
                                        updateLatLong(recentSearchesList[index],
                                            false, false);
                                      } else {
                                        // recentSearchesList[index].type = 4;
                                        updateLatLong(recentSearchesList[index],
                                            false, false);
                                      }
                                    },
                                    leading: Icon(
                                      Icons.history,
                                      color: Colors.grey,
                                    ),
                                    title: Text(
                                      recentSearchesList[index].main_text ?? "",
                                      style: TextStyle(
                                          fontSize: 17,
                                          fontWeight: FontWeight.bold),
                                      maxLines: 1,
                                    ),
                                    subtitle: Text(
                                      recentSearchesList[index]
                                              .secondary_text ??
                                          "",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w600),
                                    ),
                                  );
                                },
                              ),
                            )
                        ])),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 15, vertical: 10),
                      child: MaterialButton(
                        padding: EdgeInsets.symmetric(vertical: 15),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15)),
                        onPressed: () {
                          int type = 0;
                          if (widget.title
                              .toString()
                              .contains("Pick up Location")) {
                            type = 0;
                          } else if (widget.title.toString() ==
                              "Drop Location") {
                            type = 1;
                          } else if (widget.title.toString() ==
                              GlobalConstants.labelGlobalLocation) {
                            type = 3;
                          }
                          updateLatLong(
                              LocationModel(
                                  type: type,
                                  latitude: address!.latitude,
                                  longitude: address!.longitude,
                                  description: address!.flatNo! +
                                      " , " +
                                      address!.landmark.toString() +
                                      " , " +
                                      address!.street.toString(),
                                  main_text: address!.title),
                              true,
                              false,
                              isAlreadySavedAddress: true);
                        },
                        color: App24Colors.darkTextColor,
                        child: Text(
                          "Select & Proceed",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding:
                  EdgeInsets.symmetric(horizontal: 20, vertical: 14),
                  child: Material(
                    color: Colors.white,
                    elevation: 4,
                    shape: CircleBorder(),
                    child: InkWell(
                      customBorder: CircleBorder(),
                      onTap: () {
                        setState(() {
                          Navigator.pop(context);
                          //showUnAuthorisedPopUp();
                        });
                      },
                      child: Padding(
                        padding: EdgeInsets.all(4),
                        child: Icon(Icons.close,
                        size: 20,)
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        )
      ]);
    });
  }

  void saveToDbAndServer(LocationModel? location) async {
    // print("in save to db and server");
    // print("i type: "+location!.type.toString());

    LocationDatabase locationDatabase =
        LocationDatabase(DatabaseService.instance);

    await locationDatabase.insertLocation(LocationModel(
        placeID: location?.placeID ?? '',
        latitude: location!.latitude,
        longitude: location.longitude,
        description: location.description,
        main_text: location.main_text,
        secondary_text: location.secondary_text,
        type: location.type));

    if (location.pinColor != null && location.pinColor == Colors.red) {
      var body = {
        'mPlaceId': location.placeID,
        'mPrimary': location.main_text,
        'mSecondary': location.secondary_text == ""
            ? location.secondary_text == "unnamed"
            : location.secondary_text,
        'mFullAddress': location.description!,
        'lat': location.latitude,
        'lng': location.longitude
      };

      await context
          .read(locationNotifierProvider.notifier)
          .saveAutoCompleteLocationToServer(body);
    }
  }
}
