import 'dart:io';

import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/widgets/bottomsheets/pick_image.dart';
import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';



class PickImageBottomSheet extends StatefulWidget {
  final bool crop;

  const PickImageBottomSheet({Key? key, this.crop = true}) : super(key: key);

  @override
  _PickImageBottomSheetState createState() => _PickImageBottomSheetState();
}

class _PickImageBottomSheetState extends State<PickImageBottomSheet> {
  final ImagePicker _picker = ImagePicker();
  void getImage({required String source}) async {

    if (source == "Camera") {
// await availableCameras().then((value) =>
//     Navigator.push(context, MaterialPageRoute(builder: (context) =>TakePictureScreen(camera: value.first))));





    // final  pickedFile = await _picker.pickImage(
    //     source: ImageSource.camera,
    //     maxWidth: 512,
    //     maxHeight: 512,
    //     imageQuality: 50,
    //   );
    // if (pickedFile != null) {
    //   Navigator.pop(context, File(pickedFile.path));
    // }
    /*  await ImagePicker.openCamera(
          maxSize: 1024,
          quality: 0.1,
          cropOpt: !widget.crop ? null : CropOption())
          .then((value) {
        if (value != null) {
          Navigator.pop(context, File(value[0].path));
        }
      });*/
    } else {
      final  pickedFile = await _picker.pickImage(
        source: ImageSource.gallery,
        maxWidth: 512,
        maxHeight: 512,
        imageQuality: 50,
      );
      if (pickedFile != null) {
        Navigator.pop(context, File(pickedFile.path));
      }
      /*await ImagesPicker.pick(
        maxSize: 1024,

        quality: 0.1,
        count: 1,
        cropOpt: !widget.crop ? null : CropOption(),
        pickType: PickType.image,
      ).then((value) {
        if (value != null) {
          Navigator.pop(context, File(value[0].path));
        }
      });*/
    }
  }

  buildPickType({required String source}) {
    return Expanded(
      child: TextButton(
        onPressed: () {
          getImage(source: source);
        },
        child: Column(
          children: [
            Icon(
              source == "Camera" ? Icons.camera_alt : Icons.photo_library,
              color: App24Colors.darkTextColor,
              size: 35,
            ),
            const SizedBox(
              height: 10,
            ),
            Text(
              source,
              style: TextStyle(
                  color: App24Colors.darkTextColor,
                  fontWeight: FontWeight.w600,
                  fontSize: 16),
            ),
            const SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.only(
            top: 10, bottom: MediaQuery.of(context).viewInsets.bottom),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: IntrinsicHeight(
                  child: Row(
                    children: [
                      buildPickType(source: "Camera"),
                      VerticalDivider(
                        thickness: 1.1,
                      ),
                      buildPickType(source: "Gallery"),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              )
            ]));
  }
}
