import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/widgets/common_main_button.dart';
import 'package:flutter/material.dart';

class BookingForSomeone extends StatefulWidget {
  const BookingForSomeone({Key? key}) : super(key: key);

  @override
  _BookingForSomeoneState createState() => _BookingForSomeoneState();
}

class _BookingForSomeoneState extends State<BookingForSomeone> {
  TextEditingController _nameController = new TextEditingController();
  TextEditingController _phoneController = new TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  void confirmBookForSomeone() {
    if (_formKey.currentState!.validate()) {
      Map bookingDetails = {
        'name': _nameController.text,
        'phone': _phoneController.text
      };
      Navigator.pop(context, bookingDetails);
    }
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Container(
      // padding: EdgeInsets.symmetric(vertical: 15,horizontal: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisSize: MainAxisSize.min,
        children: [
          const SizedBox(
            height: 30,
          ),
          Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  TextFormField(
                    controller: _nameController,
                    textInputAction: TextInputAction.next,
                    validator: (String? arg) {
                      if (arg!.length == 0)
                        return 'Name can\'t be empty!';
                      else
                        return null;
                    },
                    onFieldSubmitted: (term) {
                      FocusScope.of(context).nextFocus();
                    },
                    style: TextStyle(fontWeight: FontWeight.w500),
                    decoration: InputDecoration(
                      labelText: "Name",
                      labelStyle: TextStyle(
                          color: App24Colors.yellowRideThemeColor,
                          fontSize: MediaQuery.of(context).size.width / 25),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8),
                        borderSide: BorderSide(
                          width: 0,
                          style: BorderStyle.none,
                        ),
                      ),
                      fillColor: Theme.of(context).cardColor,
                      filled: true,
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 0, horizontal: 20),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    validator: (String? arg) {
                      if (arg!.length == 0)
                        return 'Phone Number can\'t be empty!';
                      else
                        return null;
                    },
                    controller: _phoneController,
                    onFieldSubmitted: (value) {
                      confirmBookForSomeone();
                    },
                    style: TextStyle(fontWeight: FontWeight.w500),
                    keyboardType: TextInputType.number,
                    textInputAction: TextInputAction.next,
                    // inputFormatters: <TextInputFormatter>[
                    //   FilteringTextInputFormatter.digitsOnly
                    // ],
                    decoration: InputDecoration(
                      labelText: "Phone Number",
                      labelStyle: TextStyle(
                          color: App24Colors.yellowRideThemeColor,
                          fontSize: MediaQuery.of(context).size.width / 25),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8),
                        borderSide: BorderSide(
                          width: 0,
                          style: BorderStyle.none,
                        ),
                      ),
                      fillColor: Theme.of(context).cardColor,
                      filled: true,
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 0, horizontal: 20),
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  CommonButton(
                    onTap: () {
                      confirmBookForSomeone();
                    },
                    text: "SUBMIT",
                    bgColor: App24Colors.yellowRideThemeColor,
                  )
                ],
              ))
        ],
      ),
    ));
  }
}
