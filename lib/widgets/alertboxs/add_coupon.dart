import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/widgets/common_main_button.dart';
import 'package:flutter/material.dart';

class CouponCode extends StatefulWidget {
  @override
  _CouponCodeState createState() => _CouponCodeState();
}

class _CouponCodeState extends State<CouponCode> {
  TextEditingController _couponController = new TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      title: Center(
        child: Text(
          "Coupon Code",
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      content: Container(
          child: SingleChildScrollView(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: [
              Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      TextFormField(
                        controller: _couponController,
                        textInputAction: TextInputAction.next,
                        validator: (String? arg) {
                          if (arg!.length == 0)
                            return 'Enter Coupon code';
                          else
                            return null;
                        },
                        onFieldSubmitted: (term) {
                          FocusScope.of(context).nextFocus();
                        },
                        style: TextStyle(fontWeight: FontWeight.w600),
                        decoration: InputDecoration(
                          hintText: "Enter coupon code here",
                          hintStyle: TextStyle(fontSize: 12),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8),
                            borderSide: BorderSide(
                              width: 0,
                              style: BorderStyle.none,
                            ),
                          ),
                          fillColor: Theme.of(context).cardColor,
                          filled: true,
                          contentPadding:
                              EdgeInsets.symmetric(vertical: 0, horizontal: 20),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      CommonButton(
                        onTap: () {
                          // showAlertDialog(context);
                        },
                        bgColor: App24Colors.greenOrderEssentialThemeColor,
                        text: "Apply",
                      )
                      // SizedBox(
                      //   height: 10,
                      // ),
                    ],
                  ))
            ]),
      )),
      actions: [],
    );
  }
}
