import 'package:app24_user_app/helpers.dart';
import 'package:app24_user_app/modules/delivery/item_list/item_list_model.dart';
import 'package:app24_user_app/modules/restaurant/restaurant_cart_page.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/widgets/bottomsheets/addons_bottomsheet.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class CartReplaceAlertBox extends StatefulWidget {
  const CartReplaceAlertBox({Key? key, this.currentShopName, this.itemId, this.addCartPressed})
      : super(key: key);
  final String? currentShopName;
  final Products? itemId;
 final Function? addCartPressed;

  @override
  _CartReplaceAlertBoxState createState() => _CartReplaceAlertBoxState();
}

class _CartReplaceAlertBoxState extends State<CartReplaceAlertBox> {

  loadData({bool init = true, required BuildContext context}){

  }
  // String? shopName = widget.currentShopName!;
  List<Products>? restaurantItemList = [];

  @override
  Widget build(BuildContext context) {
    return Consumer(
        builder: (context, watch, child) {
          final state =
          watch(itemListNotifierProvider);
          if (state.isLoading) {
            return CircularProgressIndicator();
          } else if (state.isError) {
            return LoadingError(
              onPressed: (res) {
                loadData(
                    init: true, context: res);
              },
              message:
              state.errorMessage.toString(),
            );
          } else {
            // restaurantItemList = state.response.responseData.products;
            // restaurantItemUnSortedList.addAll(restaurantItemList);
            return Container(
              child:

              AlertDialog(
                title:
                Text("Replace cart items?"),
                content: Text(
                  "Your cart contains items from another shop. Do you want to discard those and add items from " +
                     /* widget.shopName
                          .toString()
                          .toLowerCase()*//* +*/
                      "?",style: TextStyle(fontWeight: FontWeight.w600),),
                actions: [
                  MaterialButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text("No"),
                  ),
                  MaterialButton(
                    onPressed: () {
                      Navigator.pop(context);
                      // onAddCartPressed(
                      //     itemListModel![i]);
                    },
                    child: Text("Yes"),
                  )
                ],
              ),
            );
          }
        });
  }
}
