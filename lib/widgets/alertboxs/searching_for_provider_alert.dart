import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:flutter/material.dart';

class SearchingForProvider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        content: Container(
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Image.asset(App24UserAppImages.providerSearch),
              const SizedBox(
                height: 10,
              ),
              getTitle("Searching for provider"),
              const SizedBox(
                height: 50,
              ),
              MaterialButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  color: Colors.redAccent,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 14, horizontal: 30),
                    child: Text(
                      'CANCEL ORDER',
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                      maxLines: 1,
                    ),
                  )),
            ],
          ),
        ));
  }
}
