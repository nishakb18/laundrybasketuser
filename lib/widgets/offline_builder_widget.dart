import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_offline/flutter_offline.dart';

class OfflineBuilderWidget extends StatelessWidget {
  OfflineBuilderWidget(this.mainChild);

  final Widget mainChild;

  @override
  Widget build(BuildContext context) {
    return OfflineBuilder(connectivityBuilder: (
      BuildContext context,
      ConnectivityResult connectivity,
      Widget child,
    ) {
      final connected = connectivity != ConnectivityResult.none;
      return Stack(
        fit: StackFit.expand,
        children: [
          AnimatedContainer(
            duration: Duration(milliseconds: 700),
            margin: EdgeInsets.only(top: connected ? 0 : 20.0),
            child: mainChild,
          ),
          Positioned(
            left: 0.0,
            right: 0.0,
            child: AnimatedCrossFade(
              firstCurve: Curves.easeInCubic,
              secondCurve: Curves.easeInCirc,
              firstChild: SizedBox(),
              secondChild: AnimatedSwitcher(
                duration: Duration(seconds: 1),
                child: Container(
                  child: Text(
                    !connected ? 'No internet connection' : 'Back online',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                        fontSize: 12),
                  ),
                  color: !connected ? Colors.red : Colors.green,
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.symmetric(vertical: 3),
                  margin: EdgeInsets.only(bottom: 20),
                  alignment: Alignment.center,
                ),
              ),
              crossFadeState: connected
                  ? CrossFadeState.showFirst
                  : CrossFadeState.showSecond,
              duration: Duration(milliseconds: 400),
              reverseDuration: Duration(seconds: 1),
              layoutBuilder: (
                Widget topChild,
                Key topChildKey,
                Widget bottomChild,
                Key bottomChildKey,
              ) {
                return Stack(
                  clipBehavior: Clip.none,
                  alignment: Alignment.center,
                  children: [
                    PositionedDirectional(
                      child: bottomChild,
                      key: bottomChildKey,
                      top: 0,
                    ),
                    PositionedDirectional(
                      child: topChild,
                      key: topChildKey,
                    ),
                  ],
                );
              },
            ),
          ),
        ],
      );
    }, builder: (BuildContext context) {
      return Container();
    });
  }
}
