import 'package:app24_user_app/constants/color_coman.dart';
import 'package:flutter/material.dart';

class CustormtittleCard extends StatelessWidget {
  final dynamic text;
  final double fontSize;
  final BorderRadiusGeometry? borderRadius;
  const CustormtittleCard(
      {super.key, required this.text, required this.fontSize, this.borderRadius});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: AppColor.carvebarcolor,
          borderRadius: borderRadius ?? BorderRadius.circular(10)
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(text,
            style: TextStyle(
                color: AppColor.white,
                fontFamily: 'Regular',
                fontSize: fontSize)),
      ),
    );
  }
}
