import 'package:app24_user_app/Laundry-app/views/ServicesScreens/services_home_screen.dart';
import 'package:app24_user_app/Laundry-app/views/widgets/custum_appbar.dart';
import 'package:app24_user_app/constants/app_text.dart';
import 'package:app24_user_app/constants/color_coman.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/constants/global_constants.dart';
import 'package:app24_user_app/constants/textstyle.dart';
import 'package:app24_user_app/helpers.dart';
import 'package:app24_user_app/models/location_model.dart';
import 'package:app24_user_app/modules/my_account/address/manage_address_model.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/utils/services/database/address_database.dart';
import 'package:app24_user_app/utils/services/database/database_service.dart';
import 'package:app24_user_app/widgets/bottomsheets/addressConfirmation/addressConfirmation.dart';
import 'package:app24_user_app/widgets/bottomsheets/search_location_bottom_sheet.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../utils/services/api/api_providers.dart';

class CheckOutHomeScreen extends StatefulWidget {
  const CheckOutHomeScreen({super.key});

  @override
  State<CheckOutHomeScreen> createState() => _CheckOutHomeScreenState();
}

class _CheckOutHomeScreenState extends State<CheckOutHomeScreen> {
  List<AddressResponseData>? list;
  String? highPriorityAddress;
  AddressResponseData? selectedAddress;

  late LocationModel? pickUpLocation;
  late LocationModel? deliveryLocation;
  String? pickup = "Search pickup location";
  String? delivery = "Search delivery location";

  @override
  void initState() {
    SchedulerBinding.instance.addPostFrameCallback((_) {
      context.read(addressNotifierProvider.notifier).getAddresses();
      // addressLocation =
    });
    getAddressWithHighestPriority();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return  newBody();
  }

  newBody(){
    return Scaffold(
      appBar: PreferredSize(
preferredSize: Size(double.infinity, 80),
        child:  CusturmAppbar(
            child: ListTile(
              leading: backarrow(context: context,
              onTap: () {
                Navigator.pop(context);
              },),
              title: Center(
                child: Text(Apptext.address,
                    style: AppTextStyle().appbathead),
              ),
            )),
      ),
      backgroundColor: AppColor.white,
      body: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 14.0),
        children: [
          SizedBox(height: 10,),
          AddressConfirmationBottomSheet(isMainPage: true,
          mainPageFun: (){},)
        ],
      ),
    );
  }

  Future<void> getAddressWithHighestPriority() async {

    AddressDatabase addressDatabase =
    new AddressDatabase(DatabaseService.instance);
    List<AddressResponseData> abc = await addressDatabase.fetchAddresses();

    setState(() {
      list = abc;
    });

    setState(() {
      if (list!.length > 0) {
        highPriorityAddress = "Select an address";
      } else {
        context.read(cartNotifierProvider.notifier).getCartList(
            context: context,
            fromLat: context
                .read(homeNotifierProvider.notifier)
                .currentLocation
                .latitude!
                .toDouble(),
            fromLng: context
                .read(homeNotifierProvider.notifier)
                .currentLocation
                .longitude!
                .toDouble());

        highPriorityAddress = "No address added!";
      }
    });
  }
}
