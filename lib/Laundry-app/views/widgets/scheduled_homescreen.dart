import 'package:app24_user_app/Laundry-app/views/ServicesScreens/services_home_screen.dart';
import 'package:app24_user_app/Laundry-app/views/widgets/custum_appbar.dart';
import 'package:app24_user_app/constants/app_text.dart';
import 'package:app24_user_app/constants/color_coman.dart';
import 'package:app24_user_app/constants/textstyle.dart';
import 'package:flutter/material.dart';

class ScheduledHomeScreen extends StatefulWidget {
  const ScheduledHomeScreen({Key? key}) : super(key: key);

  @override
  State<ScheduledHomeScreen> createState() => _ScheduledHomeScreenState();
}

class _ScheduledHomeScreenState extends State<ScheduledHomeScreen> {
  late DateTime selectedDate;
  late TimeOfDay selectedTime;

  @override
  void initState() {
    super.initState();
    selectedDate = DateTime.now();
    selectedTime = TimeOfDay.now();
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? pickedDate = await showDatePicker(
      context: context,
      initialDate: selectedDate,
      firstDate: DateTime(2000),
      lastDate: DateTime(2101),
    );

    if (pickedDate != null) {
      setState(() {
        selectedDate = pickedDate;
      });
    }
  }

  Future<void> _selectTime(BuildContext context) async {
    final TimeOfDay? pickedTime = await showTimePicker(
      context: context,
      initialTime: selectedTime,
    );

    if (pickedTime != null) {
      setState(() {
        selectedTime = pickedTime;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Column(
        children: [
          CusturmAppbar(
            child: Padding(
              padding: const EdgeInsets.only(top: 50, bottom: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ListTile(
                    leading: backarrow(context: context,
                    onTap: () {
                      Navigator.pop(context);
                    },),
                    title: Padding(
                      padding: const EdgeInsets.only(left: 50),
                      child: Center(
                        child: Text(
                          Apptext.scheduledhead,
                          style: AppTextStyle().appbathead,
                        ),
                      ),
                    ),
                    trailing: IconButton(
                      onPressed: () {},
                      icon: const Icon(
                        Icons.info_rounded,
                        color: Colors.white,
                        size: 34,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(50),
            child: Container(
              decoration: BoxDecoration(
                color: AppColor.smokewhite,
                borderRadius: BorderRadius.circular(30),
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    TextFormField(
                      decoration: const InputDecoration(
                        labelText: 'Enter Apartment Name',
                      ),
                    ),
                    TextFormField(
                      decoration: const InputDecoration(
                        labelText: 'Address',
                      ),
                    ),
                    TextFormField(
                      decoration: const InputDecoration(
                        labelText: 'Room Number',
                      ),
                    ),
                    TextFormField(
                      decoration: InputDecoration(
                        labelText: 'Date',
                        suffixIcon: IconButton(
                          icon: const Icon(Icons.calendar_today),
                          onPressed: () => _selectDate(context),
                        ),
                      ),
                      readOnly: true,
                      onTap: () => _selectDate(context),
                      controller: TextEditingController(
                        text: selectedDate.toLocal().toString().split(' ')[0],
                      ),
                    ),
                    TextFormField(
                      decoration: InputDecoration(
                        labelText: 'Time',
                        suffixIcon: IconButton(
                          icon: const Icon(Icons.access_time),
                          onPressed: () => _selectTime(context),
                        ),
                      ),
                      readOnly: true,
                      onTap: () => _selectTime(context),
                      controller: TextEditingController(
                        text: selectedTime.format(context),
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    const Text(
                        "This functionality is for regularly used products only. For highlighted or valuable items, opt for normal or quick delivery"),
                    const SizedBox(
                      height: 50,
                    ),
                    ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(
                              AppColor.carvebarcolor),
                        ),
                        onPressed: () {
                          _showConfirmationDialog(context);
                        },
                        child: Text(
                          Apptext.confirm,
                          style: TextStyle(color: AppColor.white),
                        ))
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    ));
  }

  void _showConfirmationDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          // title: Image.asset('assets/icons/sucess.png'),
          content: Text('Your order has been confirmed.'),
          actions: [
            ElevatedButton(
              onPressed: () {
                //  Get.toNamed("/orderlistscreens")
                ;
              },
              child: Text('OK'),
            ),
          ],
        );
      },
    );
  }
}
