import 'package:app24_user_app/Laundry-app/views/OrderStatus/order_status_home_screen.dart';
import 'package:app24_user_app/constants/app_text.dart';
import 'package:app24_user_app/constants/color_coman.dart';
import 'package:app24_user_app/constants/textstyle.dart';
import 'package:flutter/material.dart';

class ActiveOrderCard extends StatelessWidget {
  final dynamic orderid;
  final dynamic orderamount;
  final String currencytype;

  const ActiveOrderCard(
      {Key? key, this.orderid, this.orderamount, required this.currencytype})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    String formattedAmount;
    String? types;

    switch (currencytype) {
      case 'AEA':
        formattedAmount = '$orderamount';
        types = 'AED';
        break;
      case 'US':
        formattedAmount = '$orderamount';
        types = 'USA';

        break;
      case 'INDIAN':
        formattedAmount = '$orderamount';
        types = 'IND';

        break;
      default:
        formattedAmount = orderamount.toString();
    }

    return InkWell(
      child: Card(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Image.asset('assets/icons/shopping-bag.png'),
            Column(
              children: [
                Text("Order No $orderid",
                    style: AppTextStyle().servicescreentext),
                Text(Apptext.requst, style: AppTextStyle().servicescreentext),
              ],
            ),
            Row(
              children: [
                Text("Total", style: AppTextStyle().currencytype),
                const SizedBox(
                  width: 2,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    height: 60,
                    width: 60,
                    decoration: BoxDecoration(
                        color: AppColor.green,
                        borderRadius: BorderRadius.circular(50)),
                    child: Padding(
                      padding: const EdgeInsets.only(top: 15, left: 1),
                      child: Center(
                        child: Column(
                          children: [
                            Text(formattedAmount,
                                style: AppTextStyle().currencyamount),
                            Text(types!, style: AppTextStyle().currencytype),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
      onTap: () => {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => const OrderStatusHomeScreen()))
      },
    );
  }
}
