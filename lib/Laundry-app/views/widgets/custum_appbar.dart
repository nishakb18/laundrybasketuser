import 'package:app24_user_app/constants/color_coman.dart';
import 'package:flutter/material.dart';

class CusturmAppbar extends StatelessWidget implements PreferredSizeWidget{
  final Widget child;
  // final double height;
  const CusturmAppbar({super.key, required this.child});

  final double? preferredHeights = 80.0;

  @override
  Size get preferredSize => Size.fromHeight(preferredHeights!);

  @override
  Widget build(BuildContext context) {
    return PreferredSize(
      preferredSize: Size(double.infinity, preferredHeights!),
      child: Container(
        child: child,
        width: double.infinity,
        decoration: BoxDecoration(
            color: AppColor.carvebarcolor,
            borderRadius:
                const BorderRadius.only(bottomLeft: Radius.circular(30))),
      ),
    );
  }
}
