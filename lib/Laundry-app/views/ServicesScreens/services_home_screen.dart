import 'package:app24_user_app/Laundry-app/views/ServicesScreens/service_list_cards.dart';
import 'package:app24_user_app/Laundry-app/views/widgets/custum_appbar.dart';
import 'package:app24_user_app/Laundry-app/views/widgets/normal_address_list_screen.dart';
import 'package:app24_user_app/constants/app_text.dart';
import 'package:app24_user_app/constants/color_coman.dart';
import 'package:app24_user_app/constants/shared_preferences_path.dart';
import 'package:app24_user_app/constants/textstyle.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/utils/services/api/api_services.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ServicesHomeScreen extends StatefulWidget {
  const ServicesHomeScreen({super.key});

  @override
  State<ServicesHomeScreen> createState() => _ServicesHomeScreenState();
}

class _ServicesHomeScreenState extends State<ServicesHomeScreen> {
  int? cartID = 0;

 /* @override
  void initState() {
    cartID = context.read(cartCountNotifierProvider.notifier).cartID;
    context
        .read(cartCountNotifierProvider.notifier)
        .getCartCount(context: context);
    super.initState();
  }*/
  @override
  Widget build(BuildContext context) {
    print("cartID :: $cartID :: ${context.read(cartCountNotifierProvider.notifier).cartID}");
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size(double.infinity, 80),
        child: CusturmAppbar(
            child: ListTile(
                leading: backarrow(context: context,
                onTap: () {
                  setState(() {
                    context.read(serviceListNotifierProvider.notifier).getItemIdList = [];
                    context.read(serviceListNotifierProvider.notifier).getServiceIdList = [];
                    context.read(serviceListNotifierProvider.notifier).getQuantityList = [];
                  });
                  Navigator.pop(context);
                },),
                title: Center(
                  child: Text(Apptext.services,
                      style: AppTextStyle().appbathead),
                ),
            )),
      ),
        body: Column(
      children: [
        SizedBox(height: 14.0,),
        //default shop service list
        Container(child: Expanded(child: DefaultShopServices(isMainPage: true,))),
        Consumer(builder:
            (context, watch, child) {
          final state = watch(
              serviceListNotifierProvider);
          if (state.isLoading) {
            return SizedBox.shrink();
          }
          else if (state.isError) {
            return SizedBox.shrink();
          }
          else {
            return GestureDetector(
                onTap: () async {
                  var prefs = await SharedPreferences.getInstance();
                  Map<String, dynamic> map = ({});
                  int? cartItemId = prefs.getInt(
                      SharedPreferencesPath.cartStoreId);
                  // if (cartItemId == null || cartItemId == 0) {
                  //   showMessage(context, "Please select service items",
                  //       true, false);
                  // } else {
                    setState(() {
                     map = ({
                        "shop_type_id": 16,
                      });
                      for (var i = 0; i < context.read(serviceListNotifierProvider.notifier).getServiceIdList.length; i++) {
                        map['service_id[$i]'] = context.read(serviceListNotifierProvider.notifier).getServiceIdList[i];
                      }
                      for (var i = 0; i < context.read(serviceListNotifierProvider.notifier).getItemIdList.length; i++) {
                        map['item_id[$i]'] = context.read(serviceListNotifierProvider.notifier).getItemIdList[i];
                      }
                      for (var i = 0; i < context.read(serviceListNotifierProvider.notifier).getQuantityList.length; i++) {
                        map['qty[$i]'] = context.read(serviceListNotifierProvider.notifier).getQuantityList[i];
                      }
                    });
                    Logger.appLogs("map :: ${map.toString()}");
                    context
                        .read(newAddToCartNotifierProvider.notifier)
                        .getNewAddToCart(
                      context: context,
                        successRes: (){
                          Navigator.push(context,
                              MaterialPageRoute(
                                  builder: (context) => CheckOutHomeScreen()));
                        },
                        body:  FormData.fromMap(map)
                    );
                  // }
                  print("cartItemId:: $cartItemId");
                },
                child: Consumer(builder: (context, watch, child) {
                  final state = watch(newAddToCartNotifierProvider);
                  if (state.isLoading) {
                    print("loading");
                    return CircularProgressIndicator();
                  } else {
                    return Container(
                        width: 120,
                        decoration: BoxDecoration(
                          color: AppColor.carvebarcolor,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 14.0, vertical: 8),
                          child: Center(
                            child: Text(
                              "Next",
                              style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                  color: AppColor.white),
                            ),
                          ),
                        ));
                  }
                  }
                ));
          }
          }
        ),
        SizedBox(
          height: 20,
        ),
      ],
    ));
  }
}

Widget backarrow({required BuildContext context, Function()? onTap}) {
  return InkWell(
    child: Container(
        height: 30,
        width: 30,
        decoration: BoxDecoration(
            color: AppColor.white,
            borderRadius: BorderRadiusDirectional.circular(30)),
        child: Container(
          decoration: BoxDecoration(
              color: AppColor.green, borderRadius: BorderRadius.circular(10)),
          child: const Icon(
            Icons.arrow_back,
          ),
        )),
    onTap: onTap
  );
}
