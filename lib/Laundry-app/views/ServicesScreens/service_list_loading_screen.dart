import 'dart:async';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ServiceListLoadingScreen extends StatefulWidget {
  const ServiceListLoadingScreen({super.key, this.isMainPage = false});
  final bool? isMainPage;

  @override
  State<ServiceListLoadingScreen> createState() => _ServiceListLoadingScreenState();
}

class _ServiceListLoadingScreenState extends State<ServiceListLoadingScreen> {
  Timer? _timer;
  int _start = 10; // time to popup still working window /// in seconds

  @override
  void initState() {
    startTimer();
    super.initState();
  }
  @override
  void dispose() {
    if (_timer != null) {
      _timer!.cancel();
    }
    super.dispose();
  }

  void startTimer() {
    if (_timer != null) {
      _timer!.cancel();
      _timer = null;
    } else {
      _timer = new Timer.periodic(const Duration(seconds: 1), (Timer timer) {
        setState(() {
          if (_start < 1) {
            _timer!.cancel();
          } else {
            _start = _start - 1;
          }
        });
      });
    }
  }

  buildServiceShimmerItem(context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 8),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 14.0,vertical: 8),
        //width: MediaQuery.of(context).size.width / 4.5,
        height: 80,
        decoration: BoxDecoration(
          border: Border.all(),
          borderRadius: BorderRadius.all(Radius.circular(12))
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              height: 60,
              width: 60,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xffefefef),
                      blurRadius: 20,
                      spreadRadius: 3,
                    ),
                  ]),
            ),
            Center(
              child: Container(
                height: 30,
                width: 100,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(6),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Color(0xffefefef),
                        blurRadius: 20,
                        spreadRadius: 3,
                      ),
                    ]),
              ),
            ),
            Center(
              child: Container(
                height: 30,
                width: 30,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(6),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Color(0xffefefef),
                        blurRadius: 20,
                        spreadRadius: 3,
                      ),
                    ]),
              ),
            )

          ],
        ),
      ),
    );
  }

  buildStillLoadingWidget() {
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(15)),
      width: MediaQuery.of(context).size.width,
      child: ListTile(
        title: Text("Please wait..."),
        subtitle: Text(
            "The network seems to be too busy. We suggest you to wait for some more time."),
        leading: CircularProgressIndicator.adaptive(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Stack(
          fit: StackFit.expand,
          children: [
            Shimmer.fromColors(
                baseColor: Colors.grey[300]!,
                highlightColor: Colors.grey[100]!,
                child: ListView.builder(
                  padding: EdgeInsets.symmetric(horizontal: 14.0,vertical: (widget.isMainPage == false)?80:0),
                    itemCount: 4,
                    itemBuilder: (context, index) {
                      return buildServiceShimmerItem(context);
                    },
                )
            ),
            Positioned(
              left: 20,
              right: 20,
              top: 50,
              child: _start == 0 ? buildStillLoadingWidget() : Container(),
            )
          ],
        ));
  }
}



class RPSCustomPainter extends CustomPainter {
@override
void paint(Canvas canvas, Size size) {

Paint paint_0_fill = Paint()..style=PaintingStyle.fill;
paint_0_fill.color = Color(0xff02276F).withOpacity(0.0);
canvas.drawRect(Rect.fromLTWH(size.width*0.006722689,size.height*0.4620462,size.width*0.9865546,size.height*0.5115512),paint_0_fill);

Path path_1 = Path();
path_1.moveTo(size.width*0.006722689,size.height*0.1650165);
path_1.cubicTo(size.width*0.006722689,size.height*0.07388053,size.width*0.04434588,0,size.width*0.09075630,0);
path_1.lineTo(size.width*0.9092437,0);
path_1.cubicTo(size.width*0.9556538,0,size.width*0.9932773,size.height*0.07388053,size.width*0.9932773,size.height*0.1650165);
path_1.lineTo(size.width*0.9932773,size.height*0.5298317);
path_1.cubicTo(size.width*0.9932773,size.height*0.6091881,size.width*0.9645126,size.height*0.6773069,size.width*0.9248000,size.height*0.6919967);
path_1.lineTo(size.width*0.6554017,size.height*0.7916502);
path_1.cubicTo(size.width*0.6400588,size.height*0.7973234,size.width*0.6258252,size.height*0.8112805,size.width*0.6142958,size.height*0.8319505);
path_1.lineTo(size.width*0.5924555,size.height*0.8711122);
path_1.cubicTo(size.width*0.5601546,size.height*0.9290231,size.width*0.5106336,size.height*0.9285974,size.width*0.4785916,size.height*0.8701353);
path_1.lineTo(size.width*0.4583849,size.height*0.8332640);
path_1.cubicTo(size.width*0.4466000,size.height*0.8117591,size.width*0.4319210,size.height*0.7973531,size.width*0.4160891,size.height*0.7917492);
path_1.lineTo(size.width*0.07585092,size.height*0.6713300);
path_1.cubicTo(size.width*0.03583462,size.height*0.6571683,size.width*0.006722689,size.height*0.5887756,size.width*0.006722689,size.height*0.5089307);
path_1.lineTo(size.width*0.006722689,size.height*0.1650165);
path_1.close();

Paint paint_1_fill = Paint()..style=PaintingStyle.fill;
paint_1_fill.color = Color(0xffFF0000).withOpacity(1.0);
canvas.drawPath(path_1,paint_1_fill);

}

@override
bool shouldRepaint(covariant CustomPainter oldDelegate) {
return true;
}
}