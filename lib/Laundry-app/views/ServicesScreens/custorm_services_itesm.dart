import 'package:app24_user_app/Laundry-app/views/ServicesScreens/service_list_loading_screen.dart';
import 'package:app24_user_app/constants/color_coman.dart';
import 'package:app24_user_app/constants/shared_preferences_path.dart';
import 'package:app24_user_app/helpers.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:item_count_number_button/item_count_number_button.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../modules/delivery/item_list/item_types_model.dart';
import '../../../widgets/item_counter_widget.dart';

class ServiceItems extends StatefulWidget {
  final String serviceId;
  final int? shopId;
  const ServiceItems({
    super.key,
    required this.serviceId,
    this.shopId,
  });

  @override
  _ServiceItemsState createState() => _ServiceItemsState();
}

class _ServiceItemsState extends State<ServiceItems> {
  int qty = 0;

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) async {
      print("ids :: itemId ::: ${widget.serviceId} shopId:: ${widget.shopId}");
      fetchItems();
    });
  }

  fetchItems() async {
   await context.read(serviceList1NotifierProvider.notifier).getServiceItemsList1(
        itemId: widget.serviceId, shopId: widget.shopId ?? 0);
    //fetchList();
  }

  reloadData({required BuildContext context}) {
    return context
        .read(serviceList1NotifierProvider.notifier)
        .getServiceItemsList1(
            itemId: widget.serviceId, shopId: widget.shopId ?? 0);
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (context, watch, child) {
      final state = watch(serviceList1NotifierProvider);
      if (state.isLoading) {
        return SizedBox(
          height: 300,
            child: ServiceListLoadingScreen(isMainPage: true,));
      } else if (state.isError) {
        {
          return LoadingError(
            onPressed: (res) {
              reloadData(context: res);
            },
            message: state.errorMessage,
          );
        }
      } else {
        //return responseBody(state.response?.responseData?.data ?? []);
       // return responseBody(context.read(serviceList1NotifierProvider.notifier).filteredList);
        return responseBody((context
                .read(serviceList1NotifierProvider.notifier)
                .filteredList!
                .isEmpty)
            ? context.read(serviceList1NotifierProvider.notifier).originalList
            : context.read(serviceList1NotifierProvider.notifier).filteredList);
      }
    });
  }

  Widget responseBody(List<Datum>? model) {
  //  print("model.length :: ${model?.length}");
    return Wrap(
      children: [
        searchWidget(),

        (model == [] || model!.isEmpty)
            ? const Center(
                child: Text("No Items yet!"),
              )
            : ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: model.length ?? 0,
                itemBuilder: (context, index) {
                  initAddList(model: model[index]);
                  return AnimatedContainer(
                    duration: Duration(milliseconds: 120 * index),
                    decoration: BoxDecoration(
                        border:
                            Border(bottom: BorderSide(color: Colors.black12))),
                    child: ListTile(
                        onTap: () {
                          print('Item pressed: ${model[index].itemName}');
                        },
                        title: Text(
                          model[index].itemName ?? '',
                          style: TextStyle(),
                        ),
                        subtitle: (widget.shopId == 0)
                            ? SizedBox.shrink()
                            : Text(
                                '${model[index].userCurrency ?? ''}${model[index].itemPrice.toString()}'),
                        leading: SizedBox(
                          width: 50,
                          child: Image.network(
                            model[index].picture ?? '',
                            errorBuilder: (context, error, stackTrace) {
                              return Image.asset(
                                  "assets/images/emptyImage.png");
                            },
                          ),
                        ),
                        trailing: Wrap(
                          children: [
                            /*QuantityWidget(
                              models: model[index],
                              storeId: widget.shopId,
                              itemId: model[index].id ?? 0,
                              quantity: model[index].cartQuantity,
                              serviceId: int.parse(
                                widget.serviceId,
                              ),
                            ),*/
                            ItemCountWidget(
                              initialValue: model[index].cartQuantity ?? 0,
                              minValue: 0,
                              decimalPlaces: 0,
                              step: 1,
                              textStyle: TextStyle(
                                  fontSize: 20
                              ),
                              buttonSizeHeight: 40,
                              buttonSizeWidth: 40,
                              onChanged: (value) {
                                if((model[index].cartQuantity ?? 0) < value){
                                  print("object was increment");
                                  setState(() {
                                    model[index].cartQuantity = value.toInt();
                                  });
                                  countIncrementFun(quantity: value.toInt(),itemId: model[index].id ?? 0,storeId: widget.shopId ?? 0);
                                }
                                else {
                                  setState(() {
                                    model[index].cartQuantity = value.toInt();
                                  });
                                  addToCart(itemId: model[index].id ?? 0,
                                      quantity: value);
                                }
                                // Handle counter value changes
                                print('Selected value: $value :: ${model[index].cartQuantity ?? 0}');
                              }, maxValue: double.infinity,
                            ),
                          ],
                        )),
                  );
                },
              )
      ],
    );

  }

  countIncrementFun({required int quantity,required int itemId,required int storeId})
    async {
      var prefs = await SharedPreferences.getInstance();
      int? cartStoreId =
      prefs.getInt(SharedPreferencesPath.cartStoreId);
      print("serviceId :: ${widget.serviceId}");
      print("itemId :: ${itemId}");
      if ((cartStoreId != 0 && cartStoreId != null) &&
          cartStoreId != storeId) {
        if (cartStoreId == 810 || storeId == 810) {
          Helpers().getCommonAlertDialogBox(
            context: context,
            barrierDismissible: false,
            content: AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15)),
              title: Text("Replace cart items?"),
              content: Text(
                "You have item(s) in your cart. Do you want to continue with it ?",
                style: TextStyle(fontWeight: FontWeight.w600),
              ),
              actions: [
                MaterialButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text("No"),
                ),
                MaterialButton(
                  onPressed: () {
                    Navigator.pop(context);
                    addToCart(itemId: itemId,quantity: quantity);
                  },
                  child: Text("Yes"),
                )
              ],
            ),
          );
        } else {
          Helpers().getCommonAlertDialogBox(
            context: context,
            barrierDismissible: false,
            content: AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15)),
              title: Text("Replace cart items?"),
              content: Text(
                "Your cart contains items from another shop. Do you want to discard those and add items?",
                style: TextStyle(fontWeight: FontWeight.w600),
              ),
              actions: [
                MaterialButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text("No"),
                ),
                MaterialButton(
                  onPressed: () {
                    Navigator.pop(context);
                    addToCart(itemId: itemId,quantity: quantity);
                  },
                  child: Text("Yes"),
                )
              ],
            ),
          );
        }
      } else {
        addToCart(itemId: itemId,quantity: quantity);
      }
  }

  addToCart({bool? isRemove,quantity,required int itemId}) {
    print(
        "cartID dfsdf:: ${context.read(newAddToCartNotifierProvider.notifier).cartID}");
    /*if (isRemove == true) {
      setState(() {
        quantity--;
      });
    } else {
      setState(() {
        quantity++;
      });
    }*/
    setState(() {
      int existingIndex = context
          .read(serviceListNotifierProvider.notifier)
          .getItemIdList
          .indexOf(itemId);

      if (existingIndex != -1) {
        /// If the newGetItemId already exists, update the corresponding getServiceId and getQuantity at the same index
        context
            .read(serviceListNotifierProvider.notifier)
            .getServiceIdList[existingIndex] = widget.serviceId;
        context
            .read(serviceListNotifierProvider.notifier)
            .getQuantityList[existingIndex] = quantity;

        print('Updated existing entry');
      } else {
        /// If newGetItemId doesn't exist, add it to getItemId list and append newGetServiceId and newGetQuantity to their respective lists
        context
            .read(serviceListNotifierProvider.notifier)
            .getItemIdList
            .add(itemId);
        context
            .read(serviceListNotifierProvider.notifier)
            .getServiceIdList
            .add(widget.serviceId);
        context
            .read(serviceListNotifierProvider.notifier)
            .getQuantityList
            .add(quantity);

        print('Added new entry');
      }


      // }

      print(
          'getServiceId: ${context.read(serviceListNotifierProvider.notifier).getServiceIdList}');
      print(
          'getItemId: ${context.read(serviceListNotifierProvider.notifier).getItemIdList}');
      print(
          'getQuantity: ${context.read(serviceListNotifierProvider.notifier).getQuantityList}');

    });

    print(
        "listes ::getServiceId ${context.read(serviceListNotifierProvider.notifier).getServiceIdList} :: getItemId :: ${context.read(serviceListNotifierProvider.notifier).getItemIdList} :: getQuantity:: ${context.read(serviceListNotifierProvider.notifier).getQuantityList} ");
  }

  Widget searchWidget(){
    return Container(
      height: 50,
      color: Colors.transparent,
      child: TextField(
        autofocus: false,
        onChanged: (val) {
          setState(() {
            context.read(serviceList1NotifierProvider.notifier).filterFun(val);
          });
        },
        decoration: InputDecoration(
          hintText: "Search here...",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
            borderSide: BorderSide(color: AppColor.carvebarcolor),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
            borderSide: BorderSide(color: AppColor.carvebarcolor),
          ),
          disabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
            borderSide:
            BorderSide(color: AppColor.dividerColor.withOpacity(0.16)),
          ),
        ),
      ),
    );
  }

  initAddList({required Datum model}) {
    int existingIndex = context
        .read(serviceListNotifierProvider.notifier)
        .getItemIdList
        .indexOf(model.id);
    if (model.cartQuantity != 0) {
      print(
          "item id :: ${model.id} qty:: ${model.cartQuantity} service_id :: ${model.serviceId}");
      if (existingIndex != -1) {
        /// If the newGetItemId already exists, update the corresponding getServiceId and getQuantity at the same index
        context
            .read(serviceListNotifierProvider.notifier)
            .getServiceIdList[existingIndex] = model.serviceId;
        context
            .read(serviceListNotifierProvider.notifier)
            .getQuantityList[existingIndex] = model.cartQuantity;
        print('Updated existing entry');
      } else {
        /// If newGetItemId doesn't exist, add it to getItemId list and append newGetServiceId and newGetQuantity to their respective lists
        context
            .read(serviceListNotifierProvider.notifier)
            .getItemIdList
            .add(model.id);
        context
            .read(serviceListNotifierProvider.notifier)
            .getServiceIdList
            .add(model.serviceId);
        context
            .read(serviceListNotifierProvider.notifier)
            .getQuantityList
            .add(model.cartQuantity);
        print('Added new entry');
      }
    }
  }
}



class QuantityWidget extends StatefulWidget {
  const QuantityWidget(
      {super.key,
      required this.itemId,
      required this.serviceId,
      this.storeId,
      this.quantity,
      this.models});

  final int itemId;
  final int serviceId;
  final int? storeId;
  final int? quantity;
  final Datum? models;

  @override
  _QuantityWidgetState createState() => _QuantityWidgetState();
}

class _QuantityWidgetState extends State<QuantityWidget> {
  int quantity = 0;

  addToCart({bool? isRemove = false}) {
    print(
        "cartID dfsdf:: ${context.read(newAddToCartNotifierProvider.notifier).cartID}");
    if (isRemove == true) {
      setState(() {
        quantity--;
      });
    } else {
      setState(() {
        quantity++;
      });
    }
    setState(() {
      int existingIndex = context
          .read(serviceListNotifierProvider.notifier)
          .getItemIdList
          .indexOf(widget.itemId);

      if (existingIndex != -1) {
        /// If the newGetItemId already exists, update the corresponding getServiceId and getQuantity at the same index
        context
            .read(serviceListNotifierProvider.notifier)
            .getServiceIdList[existingIndex] = widget.serviceId;
        context
            .read(serviceListNotifierProvider.notifier)
            .getQuantityList[existingIndex] = quantity;

        print('Updated existing entry');
      } else {
        /// If newGetItemId doesn't exist, add it to getItemId list and append newGetServiceId and newGetQuantity to their respective lists
        context
            .read(serviceListNotifierProvider.notifier)
            .getItemIdList
            .add(widget.itemId);
        context
            .read(serviceListNotifierProvider.notifier)
            .getServiceIdList
            .add(widget.serviceId);
        context
            .read(serviceListNotifierProvider.notifier)
            .getQuantityList
            .add(quantity);

        print('Added new entry');
      }


      // }

      print(
          'getServiceId: ${context.read(serviceListNotifierProvider.notifier).getServiceIdList}');
      print(
          'getItemId: ${context.read(serviceListNotifierProvider.notifier).getItemIdList}');
      print(
          'getQuantity: ${context.read(serviceListNotifierProvider.notifier).getQuantityList}');

    });

    print(
        "listes ::getServiceId ${context.read(serviceListNotifierProvider.notifier).getServiceIdList} :: getItemId :: ${context.read(serviceListNotifierProvider.notifier).getItemIdList} :: getQuantity:: ${context.read(serviceListNotifierProvider.notifier).getQuantityList} ");
  }

  @override
  void initState() {
    quantity = widget.quantity ?? 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    //print("model[index].cartQuantity :: ${widget.quantity}");
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        if (quantity > 0)
          Row(
            children: [
              Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: IconButton(
                  onPressed: () {
                    print("serviceId :: ${widget.serviceId}");
                    print("itemId :: ${widget.itemId}");
                    addToCart(isRemove: true);
                  },
                  icon: Icon(Icons.remove),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "$quantity",
                  style: TextStyle(fontSize: 20),
                ),
              ),
            ],
          ),
        Container(
          decoration: BoxDecoration(
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.circular(10),
          ),
          child: IconButton(
            onPressed: () async {
              var prefs = await SharedPreferences.getInstance();
              int? cartStoreId =
                  prefs.getInt(SharedPreferencesPath.cartStoreId);
              print("serviceId :: ${widget.serviceId}");
              print("itemId :: ${widget.itemId}");
              if ((cartStoreId != 0 && cartStoreId != null) &&
                  cartStoreId != widget.storeId) {
                if (cartStoreId == 810 || widget.storeId == 810) {
                  Helpers().getCommonAlertDialogBox(
                    context: context,
                    barrierDismissible: false,
                    content: AlertDialog(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15)),
                      title: Text("Replace cart items?"),
                      content: Text(
                        "You have item(s) in your cart. Do you want to continue with it ?",
                        style: TextStyle(fontWeight: FontWeight.w600),
                      ),
                      actions: [
                        MaterialButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text("No"),
                        ),
                        MaterialButton(
                          onPressed: () {
                            Navigator.pop(context);
                            addToCart();
                          },
                          child: Text("Yes"),
                        )
                      ],
                    ),
                  );
                } else {
                  Helpers().getCommonAlertDialogBox(
                    context: context,
                    barrierDismissible: false,
                    content: AlertDialog(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15)),
                      title: Text("Replace cart items?"),
                      content: Text(
                        "Your cart contains items from another shop. Do you want to discard those and add items?",
                        style: TextStyle(fontWeight: FontWeight.w600),
                      ),
                      actions: [
                        MaterialButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text("No"),
                        ),
                        MaterialButton(
                          onPressed: () {
                            Navigator.pop(context);
                            addToCart();
                          },
                          child: Text("Yes"),
                        )
                      ],
                    ),
                  );
                }
              } else {
                addToCart();
              }
            },
            icon: Icon(Icons.add),
          ),
        ),
      ],
    );
  }
}

class SearchBoxWidget extends StatefulWidget {
  const SearchBoxWidget({super.key});

  @override
  State<SearchBoxWidget> createState() => _SearchBoxWidgetState();
}

class _SearchBoxWidgetState extends State<SearchBoxWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      child: TextField(
        autofocus: false,
        onChanged: (val) {
          setState(() {
            context.read(serviceList1NotifierProvider.notifier).filterFun(val);
          });
        },
        decoration: InputDecoration(
          hintText: "Search here...",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
            borderSide: BorderSide(color: AppColor.carvebarcolor),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
            borderSide: BorderSide(color: AppColor.carvebarcolor),
          ),
          disabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
            borderSide:
                BorderSide(color: AppColor.dividerColor.withOpacity(0.16)),
          ),
        ),
      ),
    );
  }
}
