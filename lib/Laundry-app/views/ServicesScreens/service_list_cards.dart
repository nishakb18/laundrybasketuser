import 'package:app24_user_app/Laundry-app/views/ServicesScreens/custorm_services_itesm.dart';
import 'package:app24_user_app/Laundry-app/views/ServicesScreens/service_list_loading_screen.dart';
import 'package:app24_user_app/constants/color_coman.dart';
import 'package:app24_user_app/modules/delivery/item_list/item_list_category_loading.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import '../../../modules/delivery/item_list/service_item_list_model.dart';
import '../../../modules/delivery/item_list/shop_details_model.dart';
import '../../../utils/services/api/api_providers.dart';

class DefaultShopServices extends StatefulWidget {
  const DefaultShopServices({super.key, this.isMainPage = false, this.shopId, this.itemList});
  final List<StoreCusinie>? itemList;
  final bool? isMainPage;
  final int? shopId;

  @override
  State<DefaultShopServices> createState() => _DefaultShopServicesState();
}

class _DefaultShopServicesState extends State<DefaultShopServices> {
  int selectedIndex = -1;

  @override
  void initState() {
    if(widget.isMainPage == true) {
      SchedulerBinding.instance.addPostFrameCallback((_) async {
        context
            .read(serviceListNotifierProvider.notifier)
            .getServiceItems();
      });
    }
    context.read(serviceListNotifierProvider.notifier).getItemIdList.clear();
    context.read(serviceListNotifierProvider.notifier).getServiceIdList.clear();
    context.read(serviceListNotifierProvider.notifier).getQuantityList.clear();
    super.initState();
   // fetchData();
  }

  TextEditingController serchitem = TextEditingController();

  reloadData(){
    context
        .read(serviceListNotifierProvider.notifier)
        .getServiceItems();
  }

  @override
  Widget build(BuildContext context) {
    return (widget.isMainPage == true)?Scaffold(
      body: _renderBody(),
    ): Container(
      color: Colors.white,
      child: renderDataWidget(model: widget.itemList ?? []),
    );
  }
  Widget _renderBody(){
    return  Consumer(builder:
        (context, watch, child) {
          final state = watch(
              serviceListNotifierProvider);
          if (state.isLoading) {
            return ServiceListLoadingScreen(isMainPage: widget.isMainPage,);
          }
          else if (state.isError) {
            {
              return LoadingError(
                onPressed: (res) {
                  reloadData();
                },
                message:
                state.errorMessage,
              );
            }
          }
          else{
          return renderDataWidget(
              model: state.response.responseData);
          }
      }
    );
  }

  Widget renderDataWidget({List<dynamic>? model}){
    //expandFun(model);
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 14.0),
      child: ListView.builder(
        shrinkWrap: true,
        physics: (widget.isMainPage == true)?AlwaysScrollableScrollPhysics():NeverScrollableScrollPhysics(),
        itemCount: model?.length,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: (){
              setState(() {
                //isExpandedList[index] = !isExpandedList[index];
                if (selectedIndex == index) {
                  selectedIndex = -1;
                } else {
                  selectedIndex = index;
                }
              });
            },
            child: Card(
              child: Column(
                children: [
                  ListTile(
                    leading: Image.network((widget.isMainPage == true)? (model?[index].picture ?? '') :(model?[index].cuisine.picture ?? '')),
                    title: Text((widget.isMainPage == true)? (model?[index].name ?? ''):(model?[index].cuisine.name ?? '')),
                    trailing:  Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(
                            (selectedIndex == index)
                            //isExpandedList[index]
                                ? Icons.arrow_drop_up
                                : Icons.arrow_drop_down,
                          ),
                        if (widget.isMainPage == true && model?[index].cartQuantity != 0 ) Container(
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: AppColor.green
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Center(
                              child: Text(model?[index].cartQuantity.toString() ?? '',
                              style: TextStyle(
                                fontSize: 10
                              ),),
                            ),
                          ),
                        ),
                        if (widget.isMainPage == false && model?[index].cuisine.cartQuantity != 0) Container(
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: AppColor.green
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Center(
                              child: Text(model?[index].cuisine.cartQuantity.toString() ?? '',
                                style: TextStyle(
                                    fontSize: 10
                                ),),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  if (/*isExpandedList[index]*/selectedIndex == index)
                    Padding(
                        padding: const EdgeInsets.all(8.0),
                        child:
            // Column(
            //               children: [
                           /* Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                child: TextFormField(
                                  decoration: InputDecoration(
                                      border: OutlineInputBorder()),
                                ),
                              ),
                            ),*/
                            ServiceItems(
                              serviceId: ((widget.isMainPage == true)? (model?[index].id.toString() ?? ''):(model?[index].cuisine.id.toString() ?? '')),
                              shopId:(widget.isMainPage == true)? 810:model?[index].storeId,
                            ),
                        //   ],
                        // )
                    ),
                ],
              ),
            ),
          );
        },
      ),
    );}
}
