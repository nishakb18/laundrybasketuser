import 'package:app24_user_app/Laundry-app/views/ServicesScreens/services_home_screen.dart';
import 'package:app24_user_app/Laundry-app/views/widgets/custum_appbar.dart';
import 'package:app24_user_app/constants/app_text.dart';
import 'package:app24_user_app/constants/color_coman.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/constants/global_constants.dart';
import 'package:app24_user_app/constants/textstyle.dart';
import 'package:app24_user_app/models/location_model.dart';
import 'package:app24_user_app/modules/delivery/cart/cart.dart';
import 'package:app24_user_app/modules/delivery/laundry/homeshop.dart';
import 'package:app24_user_app/modules/delivery/shop_list/shop_categories.dart';
import 'package:app24_user_app/modules/my_account/profile/profile.dart';
import 'package:app24_user_app/modules/my_account/wallet/wallet_screen.dart';
import 'package:app24_user_app/modules/tabs/home/widgets/offers_loading.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/widgets/bottomsheets/promocodes/landing_page_promo_code/landing_page_promo_codes_model.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:app24_user_app/widgets/offline_builder_widget.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:location/location.dart';
import 'package:upgrader/upgrader.dart';

import 'higlights_items.dart';

class DashboardScreen extends StatefulWidget {
  const DashboardScreen({super.key});

  @override
  State<DashboardScreen> createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  late bool _serviceEnabled;
  final Location location =  Location();
  PermissionStatus? _permissionGranted;
  LocationModel? selectedLocation;

  int _current = 0;

  @override
  void initState() {
    SchedulerBinding.instance.addPostFrameCallback((_) {
      _getCurrentLocation();
      // _getCurrentLocation();
    });
    super.initState();
  }

  _getCurrentLocation() async {
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }

    await location.getLocation().then((position) async {
      LocationData currentLocation = position;
      LocationModel? lm;
      selectedLocation = LocationModel(
          latitude: currentLocation.latitude,
          longitude: currentLocation.longitude);

      lm = await getAddress(
          selectedLocation!.latitude!, selectedLocation!.longitude!);

      setState(() {
        context.read(homeNotifierProvider.notifier).currentLocation = lm!;
        context.read(orderNotifierProvider.notifier).getLaundry(
            init: true,
            lat: selectedLocation!.latitude!,
            long: selectedLocation!.longitude!);
        context.read(servicesNotifierProvider.notifier).getServices();
        context
            .read(landingPagePromoCodeProvider.notifier)
            .getLandingPromoCodes(
                context: context,
                init: true,
                lat: "${selectedLocation!.latitude!}",
                lng: "${selectedLocation!.longitude!}");
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    List<PickChoice> items = [];
    items.add(const PickChoice("NORMAL", image: "assets/images/normal.png"));
    items.add(const PickChoice("QUICK", image: "assets/images/Quick.png"));
    items.add(
        const PickChoice("SCHEDULED", image: "assets/images/schebuled.png"));

    return Scaffold(
        extendBody: true,
        appBar: PreferredSize(
          preferredSize: const Size(double.infinity, 4),
          child: AppBar(
            backgroundColor: AppColor.carvebarcolor,
            leading: const SizedBox.shrink(),
          ),
        ),
        body: UpgradeAlert(
          child: OfflineBuilderWidget(
            SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    color: Colors.transparent,
                    height: 370,
                    child: Stack(
                      children: [
                        SizedBox(
                          height: 300,
                          child: CusturmAppbar(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 14,
                                  ),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            Apptext.welcome,
                                            style: AppTextStyle().heading,
                                          ),
                                          Text(
                                            Apptext.appname,
                                            style: AppTextStyle().heading,
                                          )
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          IconButton(
                                            onPressed: () {
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          const WalletPage(
                                                            isLaundryApp: true,
                                                          )));
                                            },
                                            icon: Icon(
                                              Icons.wallet,
                                              color: AppColor.white,
                                            ),
                                          ),
                                          GestureDetector(
                                            onTap: () {
                                              showLog(
                                                  "lat :: ${context.read(homeNotifierProvider.notifier).currentLocation.latitude}lng:: ${context.read(homeNotifierProvider.notifier).currentLocation.longitude}");
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          const Cart(
                                                            newCartPage: true,
                                                          )));
                                            },
                                            child: Stack(
                                              children: [
                                                Container(
                                                  height: 40,
                                                  width: 40,
                                                  color: Colors.transparent,
                                                  child: Icon(
                                                      Icons.shopping_cart,
                                                      color: AppColor.white),
                                                ),
                                                Positioned(
                                                  top: 0,
                                                  right: 0,
                                                  child: Consumer(builder:
                                                      (context, watch, child) {
                                                    final state = watch(
                                                        orderNotifierProvider);
                                                    if (state.isLoading) {
                                                      return const SizedBox.shrink();
                                                    } else if (state.isError) {
                                                      return const SizedBox.shrink();
                                                    } else {
                                                      if(state.response != null) {
                                                        return (state
                                                                  .response
                                                                  ?.responseData
                                                                  ?.cartCount !=
                                                              0)
                                                          ? Container(
                                                              decoration: BoxDecoration(
                                                                  color: AppColor
                                                                      .green,
                                                                  shape: BoxShape
                                                                      .circle),
                                                              child: Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                        .all(
                                                                            4.0),
                                                                child: Center(
                                                                  child: Text(
                                                                    "${state.response.responseData.cartCount}",
                                                                    style: const TextStyle(
                                                                        fontSize:
                                                                            8),
                                                                  ),
                                                                ),
                                                              ),
                                                            )
                                                          : const SizedBox.shrink();
                                                      } else{
                                                        return const SizedBox.shrink();
                                                      }
                                                    }
                                                  }),
                                                )
                                              ],
                                            ),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 14.0),
                                  child: Container(
                                    color: Colors.transparent,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        //SelectUserCurrentLocation(),
                                        locationWidget(), /*Container(
                                            width: 100,
                                            height: 100,
                                            color: Colors.transparent,
                                            child: Image.asset(
                                              'assets/images/Grouplogo.png',
                                            ))*/
                                      ],
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                              ],
                            ),
                          ),
                        ),
                        Positioned(
                          bottom: 20,
                          left: 20,
                          right: 20,
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                InkWell(
                                  child: pickupitems(items[0]),
                                  onTap: () => {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              const ServicesHomeScreen(),
                                        )),
                                  },
                                ),
                                const SizedBox(width: 10),
                                InkWell(
                                  child: pickupitems(items[1]),
                                  onTap: () => {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              const ServicesHomeScreen(),
                                        )),
                                  },
                                ),
                                const SizedBox(width: 10),
                                InkWell(
                                  child: pickupitems(items[2]),
                                  onTap: () => {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              const ServicesHomeScreen(),
                                        )),
                                  },
                                ),
                              ]),
                        ),
                      ],
                    ),
                  ),
                  const Highlights(),
                  const SizedBox(
                    height: 10,
                  ),
                  const HomeLaundryShop(
                    isManiPage: false,
                  ),
                  //  bottombrand(),
                  const SizedBox(
                    height: 10,
                  ),
                  offerWidget(),
                ],
              ),
            ),
          ),
        ));
  }

  Widget offerWidget() {
    return Consumer(builder: (context, watch, child) {
      final state = watch(landingPagePromoCodeProvider);
      if (state.isLoading) {
        return OffersLoading();
      } else if (state.isError) {
        return LoadingError(
          onPressed: (res) {
            reloadData(init: true, context: res);
          },
          message: state.errorMessage,
        );
      } else {
        if (state.response.responseData.length == 0){
          return const SizedBox.shrink();
          /* return Container(
                      color: Colors.transparent,
                      child: Column(
                          crossAxisAlignment:
                          CrossAxisAlignment.stretch,
                          children: [
                            CarouselSlider(
                              items:
                              Helpers().getCarouselImage(context),
                              options: CarouselOptions(
                                  height: 145,
                                  autoPlay: true,
                                  enlargeCenterPage: false,
                                  onPageChanged: (index, reason) {
                                    setState(() {
                                      _current = index;
                                    });
                                  }),
                            ),
                            Row(
                              mainAxisAlignment:
                              MainAxisAlignment.center,
                              children: <Widget>[
                                ...List.generate(
                                    Helpers()
                                        .foodOfferCarousel
                                        .length,
                                        (index) => Container(
                                      width: 8.0,
                                      height: 8.0,
                                      margin:
                                      EdgeInsets.symmetric(
                                          vertical: 10.0,
                                          horizontal: 2.0),
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: _current == index
                                            ? Theme.of(context)
                                            .primaryColor
                                            : Colors.grey[300],
                                      ),
                                    )),
                              ],
                            )
                          ]),
                    );*/
        }
        else {
          return Container(
            color: AppColor.white,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(14, 0, 0, 0),
                  child: Text(
                    "Offers",
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: MediaQuery
                            .of(context)
                            .size
                            .width / 17,
                        color: App24Colors.darkTextColor),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  color: Colors.transparent,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        CarouselSlider(
                          items: getCarouselImages(
                              context, state.response.responseData),
                          options: CarouselOptions(
                              height: 145,
                              autoPlay: true,
                              enlargeCenterPage: false,
                              onPageChanged: (index, reason) {
                                setState(() {
                                  _current = index;
                                });
                              }),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            ...List.generate(
                                state.response.responseData.length,
                                    (index) =>
                                    Container(
                                      width: 8.0,
                                      height: 8.0,
                                      margin: const EdgeInsets.symmetric(
                                          vertical: 10.0,
                                          horizontal: 2.0),
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: _current == index
                                            ? Theme
                                            .of(context)
                                            .primaryColor
                                            : Colors.grey[300],
                                      ),
                                    )),
                          ],
                        )
                      ]),
                )
              ],
            ),
          );
        }
      }
    });
  }

  reloadData({BuildContext? context, bool? init, String? type}) {
    if (type == "offers") {
      return context!
          .read(landingPagePromoCodeProvider.notifier)
          .getLandingPromoCodes(
              init: init!,
              context: context,
              lat: "${selectedLocation!.latitude!}",
              lng: "${selectedLocation!.longitude!}");
    } else if (type == "services") {
      return context!.read(servicesNotifierProvider.notifier).getServices(
            init: init!,
          );
    } else {
      context!.read(landingPagePromoCodeProvider.notifier).getLandingPromoCodes(
          init: init!,
          context: context,
          lat: "${selectedLocation!.latitude!}",
          lng: "${selectedLocation!.longitude!}");

      return context.read(servicesNotifierProvider.notifier).getServices(
            init: init,
          );
    }
  }

  //Offer banners carousel
  List<Widget> getCarouselImages(
      context, List<LandingPagePromoResponseData> promoCodeList) {
    final List<Widget> imageSliders = promoCodeList
        .map((item) => Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Stack(children: [
                Container(
                  color: Colors.transparent,
                  // padding: EdgeInsets.symmetric(horizontal: 10),
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(30),
                      child: FadeInImage.assetNetwork(
                        placeholder: 'assets/images/placeholder.png',
                        image: item.picture!,
                        fit: BoxFit.fill,
                        width: MediaQuery.of(context).size.width,
                      )),
                ),
                Material(
                  borderRadius: BorderRadius.circular(25),
                  color: Colors.transparent,
                  child: InkWell(
                      borderRadius: BorderRadius.circular(25),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ShopCategoryPage(
                                      isLaundryApp: true,
                                      promoCodeDetails:
                                          item.promoDescription.toString(),
                                      promoCode: item.promoCode,
                                      storeTypeID:
                                          item.storeMultiTypeId.toString(),
                                    )));
                      }),
                )
              ]),
            ))
        .toList();

    return imageSliders;
  }

  Widget locationWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => const ProfilePage(
                        isLaundryApp: true,
                      )),
            );
          },
          child: Container(
              height: 40,
              width: 40,
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.transparent,
              ),
              child: Image.asset('assets/icons/profileicon.png')),
        ),
        const SizedBox(
          height: 10,
        ),
        InkWell(
          child: Container(
            width: 220,
            height: 35,
            decoration: BoxDecoration(
                color: AppColor.locationcontainercolor,
                borderRadius: BorderRadius.circular(10)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Expanded(
                  child: Text(
                    context
                            .read(homeNotifierProvider.notifier)
                            .currentLocation
                            .description ??
                        '',
                    overflow: TextOverflow.ellipsis,
                    // Truncate overflowing text
                    maxLines: 1,
                    // Limit to a single line
                    style: TextStyle(color: AppColor.white),
                  ),
                ),
                Icon(
                  Icons.location_on_outlined,
                  color: AppColor.white,
                )
              ],
            ),
          ),
          onTap: () {
            selectLocation(GlobalConstants.labelGlobalLocation);
          },
        ),
        const SizedBox(
          height: 10,
        ),
        Text(
          Apptext.pick,
          style: const TextStyle(color: Colors.white),
        ),
        const SizedBox(
          height: 50,
        ),
      ],
    );
  }

  selectLocation(String title) {
    showSelectLocationSheet(
            context: context,
            title: title,
            isDismissible: false,
            willPopDismissible: false)
        .then((value) {
      if (value != null) {
        LocationModel model = value;
        setState(() {
          context.read(homeNotifierProvider.notifier).currentLocation = model;
          context.read(orderNotifierProvider.notifier).getLaundry(
              init: true, lat: model.latitude, long: model.longitude);
          context
              .read(landingPagePromoCodeProvider.notifier)
              .getLandingPromoCodes(
                  init: true,
                  context: context,
                  lat: "${model.latitude}",
                  lng: "${model.longitude}");
          // loadData(context: context);
        });
      }
    });
  }

  Widget bottombrand() {
    return Container(
      color: Colors.transparent,
      width: double.infinity,
      child: Image.asset('assets/images/bottombrandgroup.png'),
    );
  }

  Widget pickupitems(PickChoice pickchoice) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: AppColor.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 2,
            blurRadius: 4,
            offset: const Offset(0, 3),
          ),
        ],
      ),
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: AppColor.transparent,
            ),
            width: 100,
            child: Image.asset(pickchoice.image),
          ),
          Text(
            pickchoice.head,
            style: TextStyle(color: AppColor.servicetext, fontSize: 12),
          ),
          const SizedBox(
            height: 10,
          )
        ],
      ),
    );
  }
}



class PickChoice {
  final String head;
  final String image;
  const PickChoice(this.head, {required this.image});
}