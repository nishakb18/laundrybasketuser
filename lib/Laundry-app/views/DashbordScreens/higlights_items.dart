import 'package:app24_user_app/Laundry-app/views/widgets/custorm_tittlecard.dart';
import 'package:app24_user_app/constants/app_text.dart';
import 'package:app24_user_app/constants/color_coman.dart';
import 'package:flutter/material.dart';

class CardItems {
  final String title;
  final String urlImage;

  const CardItems(this.title, {required this.urlImage});
}

class Highlights extends StatelessWidget {
  const Highlights({super.key});

  @override
  Widget build(BuildContext context) {
    List<CardItems> items = [];
    items.add(const CardItems("Soft toy", urlImage: "assets/images/toys.png"));
    items.add(
        const CardItems("Wedding Gown", urlImage: "assets/images/wedding.png"));
    items.add(
        const CardItems("Designer Shoe", urlImage: "assets/images/shoe.png"));
    items.add(const CardItems("Mascot", urlImage: "assets/images/mascot.png"));
    items.add(const CardItems("Luxury Bag", urlImage: "assets/images/bag.png"));
    items.add(const CardItems("Carpet", urlImage: "assets/images/Carpet.png"));

    return Container(
      //height: 400,
      width: double.infinity,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(34)
        ),
          color: AppColor.highlightcontainer),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(height: 20,),
          Padding(
            padding: const EdgeInsets.only(right: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                CustormtittleCard(
                    text: Apptext.our,
                    borderRadius: BorderRadius.horizontal(
                      right: Radius.circular(10)
                    ),
                    fontSize: 16),
               /* CustormtittleCard(
                    text: Apptext.seeall,
                    fontSize: 10),*/
              ],
            ),
          ),
          SizedBox(height: 14,),
          Container(
            height: 290,
            color: Colors.transparent,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child:
              GridView.builder(
                physics: NeverScrollableScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    childAspectRatio: 0.80,
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 10,
                  crossAxisCount: 3),
                  itemCount: items.length,
                  itemBuilder: (context, index) {
                    return buildCardItem(items[index]);
                  },)
            ),
          ),
          SizedBox(height: 14,),
        ],
      ),
    );
  }

  Widget buildCardItem(CardItems cardItem) {
    return Container(
      color: Colors.transparent,
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.white,
            ),
            width: 120,
            height: 110,
            child: Image.asset(cardItem.urlImage),
          ),
          Text(cardItem.title,
          maxLines: 1,
          textAlign: TextAlign.center,
          style: TextStyle(
            overflow: TextOverflow.ellipsis,
            fontSize: 14.0,
            color: AppColor.servicetext
          ),),
        ],
      ),
    );
  }
}
