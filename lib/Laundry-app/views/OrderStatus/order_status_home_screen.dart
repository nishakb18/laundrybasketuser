import 'package:flutter/material.dart';
import 'package:app24_user_app/Laundry-app/views/OrderStatus/trackorder_ststus.dart';
import 'package:app24_user_app/Laundry-app/views/ServicesScreens/services_home_screen.dart';
import 'package:app24_user_app/Laundry-app/views/widgets/custum_appbar.dart';
import 'package:app24_user_app/constants/app_text.dart';
import 'package:app24_user_app/constants/color_coman.dart';
import 'package:app24_user_app/constants/textstyle.dart';
import 'package:app24_user_app/modules/home.dart';
import 'package:app24_user_app/widgets/track_order.dart';
import 'package:flutter/material.dart';

class OrderStatusHomeScreen extends StatefulWidget {
  const OrderStatusHomeScreen({super.key});

  @override
  State<OrderStatusHomeScreen> createState() => _OrderStatusHomeScreenState();
}

class _OrderStatusHomeScreenState extends State<OrderStatusHomeScreen> {
  int selectedButton = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          CusturmAppbar(
            child: Padding(
              padding: const EdgeInsets.only(top: 50, bottom: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ListTile(
                    leading: backarrow(context: context,
                    onTap: () {
                      Navigator.pop(context);
                    },),
                    title: Padding(
                      padding: const EdgeInsets.only(left: 50),
                      child: Center(
                        child: Text(
                          Apptext.orderstatus,
                          style: AppTextStyle().appbathead,
                        ),
                      ),
                    ),
                    trailing: IconButton(
                      onPressed: () {},
                      icon: const Icon(
                        Icons.info_rounded,
                        color: Colors.white,
                        size: 34,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          Row(
            children: [
              for (int i = 1; i <= 6; i++)
                Expanded(
                  child: Column(
                    children: [
                      ElevatedButton(
                        onPressed: () {
                          setState(() {
                            selectedButton = i;
                          });
                        },
                        child: Text("$i"),
                      ),
                      if (i != 6)
                        Divider(), // Add a Divider between buttons except the last one
                    ],
                  ),
                ),
            ],
          ),
          if (selectedButton == 1)
            orderplaced()
          else if (selectedButton == 2)
            ontheway()
          else if (selectedButton == 3)
            process()
          else if (selectedButton == 4)
            readydelivery()
          else if (selectedButton == 5)
            delivered()
          else if (selectedButton == 5)
            // TRACKORDER
            CustomStepper()
        ],
      ),
    );
  }

  Widget orderplaced() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          // Image.asset('assets/icons/orderplaced.png')
          Text(
            Apptext.orderstatu1,
            style: AppTextStyle().orderstatushead,
          ),
          Text(
            Apptext.orderstatus1text,
            style: AppTextStyle().orderstatustext,
          ),
          const SizedBox(
            height: 42,
          ),
          Text(
            Apptext.estimated,
            style: AppTextStyle().orderstatustext,
          )
        ],
      ),
    );
  }

  Widget ontheway() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          // Image.asset('assets/icons/orderplaced.png')
          Text(
            Apptext.orderstatu2,
            style: AppTextStyle().orderstatushead,
          ),
          Text(
            Apptext.orderstatus2text,
            style: AppTextStyle().orderstatustext,
          ),
          const SizedBox(
            height: 42,
          ),
          Text(
            Apptext.estimated,
            style: AppTextStyle().orderstatustext,
          )
        ],
      ),
    );
  }
}

Widget process() {
  return Center(
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        // Image.asset('assets/icons/orderplaced.png')
        Text(
          Apptext.orderstatu3,
          style: AppTextStyle().orderstatushead,
        ),
        Text(
          Apptext.orderstatus3text1,
          style: AppTextStyle().orderstatustext,
        ),
        const SizedBox(
          height: 42,
        ),
        Text(
          Apptext.estimated,
          style: AppTextStyle().orderstatustext,
        )
      ],
    ),
  );
}

Widget readydelivery() {
  return Center(
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        // Image.asset('assets/icons/orderplaced.png')
        Text(
          Apptext.orderstatu4,
          style: AppTextStyle().orderstatushead,
        ),
        Text(
          Apptext.orderstatu4text,
          style: AppTextStyle().orderstatustext,
        ),
        const SizedBox(
          height: 42,
        ),
        Container(
          decoration: BoxDecoration(
              color: AppColor.carvebarcolor,
              borderRadius: BorderRadius.circular(20)),
          child: ElevatedButton(
              onPressed: () {}, child: Text(Apptext.orderstatu4button)),
        )
      ],
    ),
  );
}

Widget delivered() {
  return Center(
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        // Image.asset('assets/icons/orderplaced.png')
        Text(
          Apptext.orderstatu5,
          style: AppTextStyle().orderstatushead,
        ),
        Text(
          Apptext.orderstatu5text,
          style: AppTextStyle().orderstatustext,
        ),
        const SizedBox(
          height: 42,
        ),
        Container(
          decoration: BoxDecoration(
              color: AppColor.carvebarcolor,
              borderRadius: BorderRadius.circular(20)),
          child: ElevatedButton(
              onPressed: () {}, child: Text(Apptext.orderstatu5button)),
        )
      ],
    ),
  );
}
