import 'package:app24_user_app/Laundry-app/views/ServicesScreens/service_list_cards.dart';
import 'package:app24_user_app/constants/app_text.dart';
import 'package:app24_user_app/constants/color_coman.dart';
import 'package:app24_user_app/constants/shared_preferences_path.dart';
import 'package:app24_user_app/constants/textstyle.dart';
import 'package:app24_user_app/modules/delivery/item_list/item_list_notifier.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/utils/services/api/api_services.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../modules/delivery/cart/cart.dart';
import '../../../modules/delivery/item_list/shop_details_model.dart';
import '../../../utils/helpers/common_helpers.dart';

class ShopItems extends StatefulWidget {
  const ShopItems({super.key, this.itemList, this.offerPercent});
  final int? offerPercent;
  final List<StoreCusinie>? itemList;

  @override
  State<ShopItems> createState() => _ShopItemsState();
}

class _ShopItemsState extends State<ShopItems> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: Stack(
        children: [
          SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
              children: [
                Container(
                    child: DefaultShopServices(
                  itemList: widget.itemList,
                )),
                SizedBox(
                  height: 18,
                ),
                /*GestureDetector(
                    onTap: () async {
                      var prefs = await SharedPreferences.getInstance();
                      Map<String, dynamic> map = ({});
                      int? cartItemId = prefs.getInt(
                          SharedPreferencesPath.cartStoreId);
                      // if (cartItemId == null || cartItemId == 0) {
                      //   showMessage(context, "Please select service items",
                      //       true, false);
                      // } else {
                        setState(() {
                          map = ({
                            "shop_type_id": 16,
                          });
                          for (var i = 0; i < context.read(serviceListNotifierProvider.notifier).getServiceIdList.length; i++) {
                            map['service_id[$i]'] = context.read(serviceListNotifierProvider.notifier).getServiceIdList[i];
                          }
                          for (var i = 0; i < context.read(serviceListNotifierProvider.notifier).getItemIdList.length; i++) {
                            map['item_id[$i]'] = context.read(serviceListNotifierProvider.notifier).getItemIdList[i];
                          }
                          for (var i = 0; i < context.read(serviceListNotifierProvider.notifier).getQuantityList.length; i++) {
                            map['qty[$i]'] = context.read(serviceListNotifierProvider.notifier).getQuantityList[i];
                          }
                        });
                        Logger.appLogs("map :: ${map.toString()}");
                        context
                            .read(newAddToCartNotifierProvider.notifier)
                            .getNewAddToCart(
                            context: context,
                            successRes: (){
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Cart(
                                        newCartPage: true,
                                      )));
                            },
                            body:  FormData.fromMap(map)
                        );
                      // }
                      print("cartItemId:: $cartItemId");
                      print("lat :: ${context
                          .read(homeNotifierProvider.notifier)
                          .currentLocation
                          .latitude}lng:: ${context
                          .read(homeNotifierProvider.notifier)
                          .currentLocation
                          .longitude}");

                    },
                    child: Container(
                        width: 120,
                        decoration: BoxDecoration(
                          color: AppColor.carvebarcolor,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 14.0, vertical: 8),
                          child: Center(
                            child: Text(
                              "Next",
                              style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                  color: AppColor.white),
                            ),
                          ),
                        ))),*/
                SizedBox(
                  height: 18,
                ),
                if(widget.offerPercent! > 0)Container(
                  width: double.infinity,
                  height: 50,
                  decoration: BoxDecoration(color: AppColor.carvebarcolor),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "${Apptext.shop}${widget.offerPercent}${Apptext.offers}",
                      style: AppTextStyle().appbathead,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
              child:  Padding(
                padding: const EdgeInsets.symmetric(vertical: 14.0),
                child: GestureDetector(
                    onTap: () async {
                      var prefs = await SharedPreferences.getInstance();
                      Map<String, dynamic> map = ({});
                      int? cartItemId = prefs.getInt(
                          SharedPreferencesPath.cartStoreId);
                      // if (cartItemId == null || cartItemId == 0) {
                      //   showMessage(context, "Please select service items",
                      //       true, false);
                      // } else {
                      setState(() {
                        map = ({
                          "shop_type_id": 16,
                        });
                        for (var i = 0; i < context.read(serviceListNotifierProvider.notifier).getServiceIdList.length; i++) {
                          map['service_id[$i]'] = context.read(serviceListNotifierProvider.notifier).getServiceIdList[i];
                        }
                        for (var i = 0; i < context.read(serviceListNotifierProvider.notifier).getItemIdList.length; i++) {
                          map['item_id[$i]'] = context.read(serviceListNotifierProvider.notifier).getItemIdList[i];
                        }
                        for (var i = 0; i < context.read(serviceListNotifierProvider.notifier).getQuantityList.length; i++) {
                          map['qty[$i]'] = context.read(serviceListNotifierProvider.notifier).getQuantityList[i];
                        }
                      });
                      Logger.appLogs("map :: ${map.toString()}");
                      context
                          .read(newAddToCartNotifierProvider.notifier)
                          .getNewAddToCart(
                          context: context,
                          successRes: (){
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Cart(
                                      newCartPage: true,
                                    )));
                          },
                          body:  FormData.fromMap(map)
                      );
                      // }
                      print("cartItemId:: $cartItemId");
                      print("lat :: ${context
                          .read(homeNotifierProvider.notifier)
                          .currentLocation
                          .latitude}lng:: ${context
                          .read(homeNotifierProvider.notifier)
                          .currentLocation
                          .longitude}");

                    },
                    child: Container(
                      height: 40,
                        width: 120,
                        decoration: BoxDecoration(
                          color: AppColor.carvebarcolor,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 14.0, vertical: 8),
                          child: Center(
                            child: Text(
                              "Next",
                              style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                  color: AppColor.white),
                            ),
                          ),
                        ))),
              ),)
        ],
      ),
    );
  }
}
