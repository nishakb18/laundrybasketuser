
import 'package:app24_user_app/constants/app_text.dart';
import 'package:app24_user_app/constants/color_coman.dart';
import 'package:app24_user_app/constants/textstyle.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import '../../../modules/delivery/item_list/shop_details_model.dart';


class ShopInfo extends StatelessWidget {
  const ShopInfo({super.key, required this.model});
  final ResponseData  model;

  @override
  Widget build(BuildContext context) {
    return  renderBody(context,model);
  }

  Widget renderBody(BuildContext context,ResponseData  model){
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          color: Colors.transparent,
          padding: const EdgeInsets.only(left: 8.0),
          child: Wrap(
            children: <Widget>[
               for (int index = 0;
                  index < (model.servicesName?.length ?? 0);
                  index++)
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: Container(
                    decoration: BoxDecoration(
                        color: AppColor.carvebarcolor,
                        borderRadius: BorderRadius.circular(10)),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 14,vertical: 8),
                      child: Text(
                        model.servicesName?[index] ?? '',
                        style: AppTextStyle().buttontext,
                      ),
                    )),
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        Container(
          color: AppColor.white,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 14.0),
            child: Column(
              children: [
                Center(
                  child: Text(model.storeName ?? '',
                  style: TextStyle(
                    fontSize: 24,
                    color: AppColor.servicetext,
                    fontWeight: FontWeight.w600
                  ),),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(model.description ?? '',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 14,
                      color: AppColor.servicetext,
                      fontWeight: FontWeight.normal
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    locationWidget(context,model.storeLocation ?? ''),
                    ratingWidget("${model.rating}")
                  ],
                )

              ],
            ),
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        if(model.offerPercent! > 0)Container(
          width: double.infinity,
          height: 50,
          decoration: BoxDecoration(color: AppColor.carvebarcolor),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "${Apptext.shop}${model.offerPercent}${Apptext.offers}",
              style: AppTextStyle().appbathead,
            ),
          ),
        )
      ],
    );
  }

  Widget ratingWidget(String rating){
    return Material(
      elevation: 6,
      borderRadius: BorderRadius.circular(100),
      child: Container(
        decoration: BoxDecoration(
          color: AppColor.white,
          borderRadius: BorderRadius.circular(100)
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0,vertical: 4),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(rating,
                style: TextStyle(
                    fontSize: 12,
                    color: AppColor.servicetext,
                    fontWeight: FontWeight.w600
                ),),
              SizedBox(width: 6,),
              RatingBar.builder(
                itemSize: 15,
                initialRating:
                double.parse(rating.toString()),
                direction: Axis.horizontal,
                allowHalfRating: true,
                itemCount: 5,
                itemPadding: EdgeInsets.symmetric(
                    horizontal: 1.0),
                itemBuilder: (context, _) => Icon(
                  Icons.star,
                  color: Colors.amber,
                ),
                onRatingUpdate: (rating) {
                  print(rating);
                },
                ignoreGestures: true,
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget locationWidget(BuildContext context,String address){
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Icon(Icons.location_on_outlined,
        color: AppColor.carvebarcolor,
        size: 18,),
        SizedBox(width: 6,),
        Container(
          color: Colors.transparent,
          width: MediaQuery.of(context).size.width * 0.4,
          child: Text(address,
            maxLines: 2,
            style: TextStyle(
                fontSize: 12,
                overflow: TextOverflow.ellipsis,
                color: AppColor.servicetext,
                fontWeight: FontWeight.bold
            ),),
        )
      ],
    );
  }

}
