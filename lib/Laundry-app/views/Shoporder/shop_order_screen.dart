import 'dart:convert';

import 'package:app24_user_app/Laundry-app/views/ServicesScreens/service_list_loading_screen.dart';
import 'package:app24_user_app/Laundry-app/views/ServicesScreens/services_home_screen.dart';
import 'package:app24_user_app/Laundry-app/views/Shoporder/shop_details.dart';
import 'package:app24_user_app/Laundry-app/views/Shoporder/shop_items.dart';
import 'package:app24_user_app/constants/app_text.dart';
import 'package:app24_user_app/constants/color_coman.dart';
import 'package:app24_user_app/constants/route_path.dart';
import 'package:app24_user_app/constants/shared_preferences_path.dart';
import 'package:app24_user_app/constants/textstyle.dart';
import 'package:app24_user_app/modules/delivery/item_list/item_list_category_loading.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../../../modules/delivery/item_list/shop_details_model.dart';
import '../../../utils/services/api/api_providers.dart';

class ShopOrderScreen extends StatefulWidget {
  const ShopOrderScreen({super.key, this.shopId});

  final int? shopId;

  @override
  State<ShopOrderScreen> createState() => _ShopOrderScreenState();
}

class _ShopOrderScreenState extends State<ShopOrderScreen> {
  @override

  bool isFavourite = false;

  @override
  void initState() {
    SchedulerBinding.instance.addPostFrameCallback((_) async {
      var prefs = await SharedPreferences.getInstance();
      var favouriteShopId = prefs.getStringList(
          SharedPreferencesPath.favouriteRestaurants);
      /*   context
          .read(cartCountNotifierProvider.notifier)
          .getCartCount(context: context);*/
      context.read(shopDetailsNotifierProvider.notifier).getShopDetails(
          storeID: "${widget.shopId}");
      if (favouriteShopId != null)
        if (favouriteShopId.contains(widget.shopId)) {
          isFavourite = true;
        }
    });
    super.initState();
  }

  reloadData({required BuildContext context, bool? init}) {
    return context.read(shopDetailsNotifierProvider.notifier).getShopDetails(
        storeID: widget.shopId.toString());
  }

  Widget build(BuildContext context) {
    return Scaffold(body: Consumer(builder: (context, watch, child) {
      final state = watch(shopDetailsNotifierProvider);
      if (state.isLoading) {
        return ServiceListLoadingScreen();
      } else if (state.isError) {
        {
          return LoadingError(onPressed: (res) {
            reloadData(context: res, init: true);
          }, message: state.errorMessage,);
        }
      } else {
        print("state.response :: ${state.response.toString()}");
        if (state.response != null || state.response != {}){
         return renderBody(state.response.responseData ?? []);
        } else{
          return SizedBox.shrink();
        }
       // return renderBody(state.response.responseData);
      }
    }),);
  }

  Widget renderBody(ResponseData  model) {
    return DefaultTabController(length: 2,
      child: Column(children: [
        Container(color: AppColor.carvebarcolor,
          child: Stack(children: [
            Positioned(child:
            Image.asset('assets/images/Rectangle 151.png')
             /* Container(
                height: 200,
                width: double.infinity,
                color: AppColor.carvebarcolor,
                child: CustomPaint(
                  size: Size(MediaQuery.of(context).size.width, (MediaQuery.of(context).size.width*0.5092436974789916).toDouble()), //You can Replace [WIDTH] with your desired width for Custom Paint and height will be calculated automatically
                  painter: RPSCustomPainter(),
                ),
              )*/
            ),
            Positioned(
              bottom: 10,
              right: MediaQuery.of(context).size.width / 2.8,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(100),
                child: Container(
                  height: 88,
                  width: 88,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(100)
                  ),
                  child: Image.network(model.picture!,
                    fit: BoxFit.cover,
                    errorBuilder:
                    (context, error, stackTrace) {
                  return Image.asset(
                      "assets/images/emptyImage.png");
                },),),
              ),),
            Positioned(top: 30, left: 10,
                child: backarrow(context: context,
                  onTap: (){
                    Navigator.pushReplacementNamed(context,homeScreen);
                  }
                ))
          ]),),
        const TabBar(tabs: [Tab(text: 'INFO'), Tab(text: 'ADD ITEM'),
        ],),
        const SizedBox(height: 20,),
        Expanded(child:
        TabBarView(
          children: [
          ShopInfo(model: model,),
          ShopItems(itemList:model.storeCusinie,
          offerPercent: model.offerPercent,)],),),
      ],),);
  }

}
