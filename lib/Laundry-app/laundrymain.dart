import 'package:app24_user_app/app24_user_icons.dart';
import 'package:app24_user_app/modules/home.dart';
import 'package:app24_user_app/modules/tabs/account/my_account_tab.dart';
import 'package:app24_user_app/modules/tabs/history/history_tab.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:flutter/material.dart';
import 'package:app24_user_app/constants/global_constants.dart';
import 'package:app24_user_app/models/location_model.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class LaundryMain extends StatefulWidget {
  const LaundryMain({super.key});

  @override
  State<LaundryMain> createState() => _LaundryMainState();
}

class _LaundryMainState extends State<LaundryMain> {
  @override
  void initState() {
    SchedulerBinding.instance!.addPostFrameCallback((_) async {
      await _getCurrentLocation();

      if (context
              .read(homeNotifierProvider.notifier)
              .currentLocation
              .main_text ==
          "Loading..") {
        selectLocation(GlobalConstants.labelGlobalLocation);
      }
    });
    super.initState();
  }

  late bool _serviceEnabled;
  final Location location = Location();
  PermissionStatus? _permissionGranted;
  LocationModel? selectedLocation;
  GoogleMapController? mapController;

  selectLocation(String title) {
    showSelectLocationSheet(
            context: context,
            title: title,
            isDismissible: false,
            willPopDismissible: false)
        .then((value) {
      if (value != null) {
        LocationModel model = value;
        setState(() {
          context.read(homeNotifierProvider.notifier).currentLocation = model;
          context
              .read(landingPagePromoCodeProvider.notifier)
              .getLandingPromoCodes(context: context);
        });
      }
    });
  }

  _getCurrentLocation() async {
    print("location from home");
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }
    if (_serviceEnabled) print("_getCurrentLocation()");

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return print("location enabling");
      }
    }

    await location.getLocation().then((position) async {
      LocationData _currentLocation = position;
      LocationModel? lm;
      selectedLocation = new LocationModel(
          latitude: _currentLocation.latitude,
          longitude: _currentLocation.longitude);

      lm = await getAddress(
          selectedLocation!.latitude!, selectedLocation!.longitude!);

      setState(() {
        context.read(homeNotifierProvider.notifier).currentLocation = lm!;
        context
            .read(landingPagePromoCodeProvider.notifier)
            .getLandingPromoCodes(context: context);
      });
    });
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  int _currentIndex = 0;
  final List<Widget> _children = [
    //const DashboardScreen(),
    HistoryTabPage(),
    HomePage(),
    MyAccountTabPage()
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(children: [
      Expanded(
        child: Container(
          height:
              MediaQuery.of(context).size.height - kBottomNavigationBarHeight,
          child: _children[_currentIndex],
        ),
      ),
      BottomNavigationBar(
        currentIndex: _currentIndex,
        onTap: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
        items: [
          BottomNavigationBarItem(
            icon: Icon(
              App24User.home_2,
              size: 22,
              color: Colors.black,
            ),
            label: 'Home',
          ),
          const BottomNavigationBarItem(
            icon: Icon(
              App24User.file_alt,
              color: Colors.black,
              size: 22,
            ),
            label: 'History',
          ),
          const BottomNavigationBarItem(
            icon: Icon(App24User.bell, color: Colors.black),
            label: 'Notifications',
          ),
          BottomNavigationBarItem(
            icon: Icon(App24User.account, color: Colors.black),
            label: 'Profile',
          ),
        ],
      ),
    ]));
  }
}
