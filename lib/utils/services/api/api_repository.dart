import 'dart:math';
import 'package:app24_user_app/constants/api_path.dart';
import 'package:app24_user_app/constants/shared_preferences_path.dart';
import 'package:app24_user_app/models/app_configuration_model.dart';
import 'package:app24_user_app/models/check_wallet_balance_model.dart';
import 'package:app24_user_app/models/history_model.dart';
import 'package:app24_user_app/models/home_delivery_order_insert_model.dart';
import 'package:app24_user_app/models/home_delivery_send_order_detail.dart';
import 'package:app24_user_app/models/location_model.dart';
import 'package:app24_user_app/modules/authentication/login/models/login_model.dart';
import 'package:app24_user_app/modules/authentication/login/models/otp_model.dart';
import 'package:app24_user_app/modules/delivery/cart/cart_model.dart';
import 'package:app24_user_app/modules/delivery/item_list/item_list_model.dart';
import 'package:app24_user_app/modules/delivery/item_list/item_types_model.dart';
import 'package:app24_user_app/modules/delivery/item_list/service_item_list_model.dart';
import 'package:app24_user_app/modules/delivery/item_list/shop_details_model.dart';
import 'package:app24_user_app/modules/delivery/order_essentials/order_essentials_model.dart';
import 'package:app24_user_app/modules/delivery/shop_list/shop_list_model.dart';
import 'package:app24_user_app/modules/delivery/shop_list/shop_list_model_one.dart';
import 'package:app24_user_app/modules/my_account/address/manage_add_address_model.dart';
import 'package:app24_user_app/modules/my_account/address/manage_address_model.dart';
import 'package:app24_user_app/modules/my_account/profile/profile_model.dart';
import 'package:app24_user_app/modules/my_account/wallet/wallet_model.dart';
import 'package:app24_user_app/modules/restaurant/favouriteShopListModel.dart';
import 'package:app24_user_app/modules/restaurant/restaurant_1st_page/Cuisines/cuisine_model.dart';
import 'package:app24_user_app/modules/restaurant/restaurant_shops/restaurants_model.dart';
import 'package:app24_user_app/modules/ride/ride_cancel_reason_model.dart';
import 'package:app24_user_app/modules/ride/ride_request_model.dart';
import 'package:app24_user_app/modules/ride/ride_vehicle_estimated_fare_model.dart';
import 'package:app24_user_app/modules/ride/ride_vehicle_model.dart';
import 'package:app24_user_app/modules/ride/rider_detail_model.dart';
import 'package:app24_user_app/modules/ride/select_bid_model.dart';
import 'package:app24_user_app/modules/send_packages/select_package_model.dart';
import 'package:app24_user_app/modules/send_packages/vehicle_info_model.dart';
import 'package:app24_user_app/modules/shop_list/store_list_model.dart';
import 'package:app24_user_app/modules/tabs/history/order_history_detail_model.dart';
import 'package:app24_user_app/modules/tabs/home/global_search/global_search_model.dart';
import 'package:app24_user_app/modules/tabs/home/models/home_location_model.dart';
import 'package:app24_user_app/modules/tabs/home/models/home_screen_tiles_model.dart';
import 'package:app24_user_app/modules/tabs/home/models/services_model.dart';
import 'package:app24_user_app/modules/tabs/notifications/notification_model.dart';
import 'package:app24_user_app/utils/services/api/api_services.dart';
import 'package:app24_user_app/widgets/bottomsheets/cancel_order/cancel_order_model.dart';
import 'package:app24_user_app/widgets/bottomsheets/promocodes/landing_page_promo_code/landing_page_promo_codes_model.dart';
import 'package:app24_user_app/widgets/bottomsheets/promocodes/promocodes_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ApiRepository {
  // laundryuserApp:
  Future<ShopNamesModel> getlaundryshop({required String latLong}) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.getService(
      url: App24UserAppAPI.laundryshop + latLong,
    );
    print(result);
    return compute(shopNamesModelFromJson, result);
  }

//---------------------------------------------------------------------------------------------------------------------------------------->
//DropuserApi
  Future<HomeScreenTileModel> fetchOffers() async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.getService(
      url: App24UserAppAPI.homeScreenMenuUrl,
    );
    // Simulate network delay
    return Future.delayed(
      Duration(seconds: 5),
      () {
        return compute(homeScreenTileModelFromJson, result);
      },
    );
    //return compute(homeScreenTileModelFromJson, result);
  }

  //shop details.
  Future<ServicesModel> fetchServices() async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.getService(
      url: App24UserAppAPI.servicesUrl,
    );

    return compute(servicesModelFromJson, result);
  }

  Future<WalletModel> fetchWallet() async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.getService(
      url: App24UserAppAPI.walletTransactionUrl,
    );
    return compute(walletModelFromJson, result);
  }

  Future<AppConfigurationModel> fetchAppConfigurations() async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var body = {'salt_key': 'MQ=='};

    var result = await restAPIService.postService(
        url: App24UserAppAPI.configurationUrl, body: body);

    return compute(appConfigurationModelFromJson, result);
  }

  Future<LoginModel> fetchLogin(BuildContext context, Map body) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.postService(
        url: App24UserAppAPI.loginUrl, body: body, useToken: false);

    return compute(loginModelFromJson, result);
  }

  Future<dynamic> fetchLogout(
    BuildContext context,
  ) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.postService(
      url: App24UserAppAPI.logoutUrl,
    );
    return result;
  }

  Future<NotificationModel> fetchNotifications() async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.getService(
      url: App24UserAppAPI.notificationUrl,
    );

    return compute(notificationModelFromJson, result);
  }

  Future<HistoryModel> fetchHistory({required String type}) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.getService(
      url: App24UserAppAPI.historyUrl + type,
    );

    return compute(historyModelFromJson, result);
  }

  Future<OrderEssentials> fetchOrder() async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.getService(
      url: App24UserAppAPI.orderEssentialsUrl,
    );
    return compute(orderModelFromJson, result);
  }

  //laundry
  Future<ShopNamesModelOne> fetchLaundry({double? lat,double? long}) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.getService(
      url: "${App24UserAppAPI.laundryshop}?latitude=$lat&longitude=$long&from=new",
      useToken: true
    );
    //return compute(shopNamesModelOneFromJson, result);
    return ShopNamesModelOne.fromJson(result as Map<String, dynamic>);
  }

  Future<ShopNamesModel> fetchShopList(
      {required String storeTypeID, required String latLong}) async {
    print("qwerty");
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.getService(
      url: App24UserAppAPI.shopListUrl +
          storeTypeID +
          latLong /*+ "item_keyword=biriyani"*/,
    );
    return compute(shopNamesModelFromJson, result);
  }
  //

  Future<ItemListModel> fetchItemList(String storeID) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.getService(
      url: App24UserAppAPI.itemListUrl + storeID,
    );
    print(result);
    return compute(itemListModelFromJson, result);
  }

  Future<ShopDetailsModel> fetchShopDetails(String storeID) async {
    print("shop details api call methiod");
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.getService(
      url: App24UserAppAPI.itemListUrl + storeID,
    );
    print("fetchShopDetails result:: ${result}");
   return ShopDetailsModel.fromJson(result as Map<String, dynamic>);
    //return compute(itemListModelFromJson, result);
  }

  Future<ServiceItemsListModel> fetchServiceItemsList() async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.getService(
      url: App24UserAppAPI.defaultservice,
    );
    print("result:: $result");
    return compute(serviceItemsListModelFromJson, result);
  }

  Future<ItemTypesModel> fetchServiceItemsList1({
    itemId,
    shopId
}) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    print("shopId :: $shopId :: ");
    var url = (shopId != 0)
    ? "${App24UserAppAPI.serviceitems}$itemId?shop_id=$shopId"
        :"${App24UserAppAPI.serviceitems}$itemId";

    var result = await restAPIService.getService(
      url: url,
    );
    print("result:: $result");
    return ItemTypesModel.fromJson(result as Map<String, dynamic>);
  }

  Future<CartListModel> fetchNewAddToCart({FormData? body}) async {
    List<int>? cartIdList =[];
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.postService(
      url: App24UserAppAPI.newAddToCart,
      body: body,
      useToken: true,
    );
    print("result:: $result");
    CartListModel crtlistModel = new CartListModel();
    crtlistModel = CartListModel.fromJson(result as Map<String, dynamic>);
    cartIdList = crtlistModel.responseData!.cartProductsId;
    return CartListModel.fromJson(result as Map<String, dynamic>);
  }

  Future<HomeLocationModel> fetchHomeLocation(BuildContext context) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.getService(
      url: App24UserAppAPI.homeLocationUrl,
    );
    return compute(homeLocationModelFromJson, result);
  }

  Future<ProfileModel> fetchProfile() async {
    var prefs = await SharedPreferences.getInstance();
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.getService(
        url: App24UserAppAPI
            .profileUrl /*+ prefs.getString(SharedPreferencesPath.firebaseTokenKey).toString(),*/);
    return compute(profileModelFromJson, result);
  }

  Future<ProfileModel> fetchSaveProfile(body) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.postService(
        url: App24UserAppAPI.saveProfileUrl, body: body);
    return compute(profileModelFromJson, result);
  }

  Future<ManageAddressModel> fetchManageAddress() async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.getService(
      url: App24UserAppAPI.getAddressUrl,
    );
    return compute(manageAddressModelFromJson, result);
  }

  Future<dynamic> fetchSaveAddress(Map body) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.postService(
        url: App24UserAppAPI.addAddressUrl, body: body);
    return result;
    // return compute(manageAddressModelFromJson, result);
  }

  Future<dynamic> fetchEditAddress(Map body) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.postService(
        url: App24UserAppAPI.editAddressUrl, body: body);
    return result;
    // return compute(manageAddressModelFromJson, result);
  }

  Future<OtpModel> fetchSendOtp({required Map body}) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.postService(
        url: App24UserAppAPI.sendOtp, body: body, useToken: false);
    print(App24UserAppAPI.sendOtp);
    print(result);
    return OtpModel.fromJson(result as Map<String, dynamic>);
    // return compute(manageAddressModelFromJson, result);
  }

  Future<StoreListModel> fetchStoreList({double? lat ,double? long}) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.getService(
      useToken: true,
        url: "${App24UserAppAPI.storeList}?latitude=$lat&longitude=$long");
    return StoreListModel.fromJson(result as Map<String, dynamic>);
    // return compute(manageAddressModelFromJson, result);
  }

  Future<dynamic> fetchAddCart(
      {required BuildContext context, required String itemID, body}) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var url = App24UserAppAPI.addCartUrl;
    var result = await restAPIService.postService(
        url:  url+ itemID, body: body);
    return result;
    // return compute(manageAddressModelFromJson, result);
  }

  //laundryaddcart2024
  Future<dynamic> fetchlaundryAddCart(
      BuildContext context, String itemID, body) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.postService(
        url: App24UserAppAPI.addLaundryCartUrl + itemID, body: body);
    return result;
    // return compute(manageAddressModelFromJson, result);
  }

  Future<CartListModel> fetchCartList() async {
    RestAPIService restAPIService = RestAPIService(Dio());

    var result =
        await restAPIService.getService(url: App24UserAppAPI.laundryCartListUrl);

    return compute(cartModelFromJson, result);
  }

  //2024 laundryfeatchcartlist
  Future<CartListModel> fetchlaundryCartList() async {
    RestAPIService restAPIService = RestAPIService(Dio());

    var result = await restAPIService.getService(
        url: App24UserAppAPI.newCartListUrl);

    return compute(cartModelFromJson, result);
  }

/*  Future<dynamic> fetchCartList() async {

   */ /* var cartListResponseData;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? token = prefs.getString(SharedPreferencesPath.accessToken);
    Map<String, String> headers = {
      "Content-Type": "application/x-www-form-urlencoded",
      "Authorization": "Bearer $token"
    };
    var result;
    Helpers().getData(App24UserAppAPI.cartListUrl,  headers).then((value) {
     // result=value;
      var data = json.decode(value);
  if (json.decode(value)['statusCode'] == "200") {

    var responseData=json.decode(value)['responseData'] as List;

    if(responseData.isNotEmpty) {
      print("is not empty");
      cartListResponseData = json.decode(value)['responseData'];
    }
    else {


      cartListResponseData= json.decode(value)['message'].toString();
      print("is empty"+cartListResponseData);
    }


      }
    });
*/ /*

    return compute(cartModelFromJson, null);
  }*/

  Future<CartListModel> fetchCheckoutBSheetCartList({String? latLong}) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    // print("latitude from cart notifier"+latitude.toString());
    // print("latitude from cart notifier"+longitude.toString());
    print("lionel messi" + App24UserAppAPI.laundryCartListUrl + latLong.toString());
    var result = await restAPIService.getService(
        url: App24UserAppAPI.laundryCartListUrl + latLong.toString());
    //print("result::: "+result.toString());
    // return result;
    return compute(cartModelFromJson, result);
  }

//laundry cartapis 2024
  Future<CartListModel> fetchlaundryCheckoutBSheetCartList(
      {String? latLong}) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    // print("latitude from cart notifier"+latitude.toString());
    // print("latitude from cart notifier"+longitude.toString());
    print("lionel messi" + App24UserAppAPI.laundryCartListUrl + latLong.toString());
    var result = await restAPIService.getService(
        url: App24UserAppAPI.newCartListUrl + latLong.toString());
    //print("result::: "+result.toString());
    // return result;
    return compute(cartModelFromJson, result);
  }

//
  Future<SelectPackageModel> fetchSelectPackages() async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.getService(
      url: App24UserAppAPI.selectPackagesUrl,
    );
    return compute(selectPackageModelFromJson, result);
  }

  Future<dynamic> fetchCourierServiceSave(Map? body) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.postService(
        url: App24UserAppAPI.courierServiceSaveUrl, body: body);
    return result;
    // return compute(manageAddressModelFromJson, result);
  }

  Future<dynamic> fetchOrderCartUpdate(FormData body) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    Logger.appLogs("body fetchOrderCartUpdate:: $body");
    var result = await restAPIService.postService(
        url: App24UserAppAPI.orderCartUpdateUrl, body: body, isMultiPart: true);
    return result;
    // return compute(manageAddressModelFromJson, result);
  }
  Future<dynamic> fetchBuyOrderCartUpdate(Map body) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.postService(
        url: App24UserAppAPI.orderCartUpdateUrl, body: body, isMultiPart: true);
    return result;
    // return compute(manageAddressModelFromJson, result);
  }

  Future<HomeDeliveryOrderInsertModel> fetchDeliveryOrderInsert() async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.postService(
      url: App24UserAppAPI.deliveryOrderInsertUrl,
    );
    //return result;
    return compute(homeDeliveryOrderInsertModelFromJson, result);
  }

  Future<HomeDeliveryOrderInsertModel> fetchBuyItemsOrderPlace(
      {var body}) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.postService(
        url: App24UserAppAPI.buyItemsOrderPlace, body: body);
    //return result;
    return compute(homeDeliveryOrderInsertModelFromJson, result);
  }

  Future<HomeDeliveryOrderDetailModel> fetchDeliveryOrderDetail(
      {String? id, String? orderType}) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    String url = '';
    print("thriprayar" + orderType.toString());
    if (orderType!.toLowerCase() == 'send')
      url = App24UserAppAPI.homeDeliverySendOrderDetailUrl;
    else if (orderType.toLowerCase() == 'buy')
      url = App24UserAppAPI.homeDeliveryBuyOrderDetailUrl;
    if (id != null) {
      url = url + "id=$id";
    }
    var result = await restAPIService.getService(
      url: url,
    );
    //return result;
    return compute(homeDeliveryOrderDetailModelFromJson, result);
  }

  Future<List<LocationModel>> fetchLocation(String url, String from) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    List<LocationModel> list;
    var result = await restAPIService.getService(
      url: url,
    );

    //from("server")-> data from server
    //from("google")-> data from google
    if (from == "server")
      list = LocationModel.parseLocationListFromDB(result);
    else
      list = LocationModel.parseLocationList(result);
    return list;
  }

  Future<Map> fetchLatitudeLongitude(String url) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    Map placeDetails = new Map();

    var result = await restAPIService.getService(
      url: url,
    );

    var lat = result['result']['geometry']['location']['lat'];
    var long = result['result']['geometry']['location']['lng'];

    placeDetails['lat'] = lat;
    placeDetails['long'] = long;

    return placeDetails;
  }

  Future<dynamic> fetchAutoCompleteSaveLocationToServer(Map body) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.postService(
        url: App24UserAppAPI.autoCompleteSaveLocationUrl, body: body);
    return result;
    // return compute(manageAddressModelFromJson, result);
  }

  Future<AllRestaurantsModel> fetchRestaurants(
      {required String latLong}) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.getService(
      url: App24UserAppAPI.restaurantUrl + latLong,
    );
    print("All hotal Datd -----------------------------");
    log(result);
    return compute(restaurantsModelFromJson, result);
  }

  Future<RestaurantCuisineModel> fetchCuisines() async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.getService(
      url: App24UserAppAPI.restaurantCuisinesUrl,
    );
    return compute(cuisinesModelFromJson, result);
  }

  Future<RideVehiclesModel> fetchRideVehicles(
      {required double lat, required double long}) async {
    RestAPIService restAPIService = RestAPIService(Dio());

    String latLong = '&latitude=$lat&longitude=$long';
    var result = await restAPIService.getService(
      url: App24UserAppAPI.rideVehiclesList + latLong,
    );
    print("result dony" + result.toString());
    return compute(rideVehicleModelFromJson, result);
  }

/*  Future<EstimatedFareModel> fetchEstimateFare(
      {required double sLat, required double sLong,required double dLat, required double dLong,required int serviceTypeId,required String payMode,required int cardId}) async {
    RestAPIService restAPIService = RestAPIService(Dio());

    String rideDetails = 's_latitude=$sLat&s_longitude=$sLong&service_type=$serviceTypeId&d_latitude=$dLat&d_longitude=$dLong&payment_mode=$payMode&card_id=$cardId';
    var result = await restAPIService.getService(
      url: App24UserAppAPI.rideEstimatedFare + rideDetails,
    );
    return compute(rideEstimatedFareModelFromJson, result);
  }*/
  Future<RideRequestModel> fetchRideRequest({Map? body}) async {
    RestAPIService restAPIService = RestAPIService(Dio());

    var result = await restAPIService.postService(
        url: App24UserAppAPI.rideRequest, body: body);
    print("body of ride" + body.toString());

    return compute(rideRequestModelFromJson, result);
  }

  Future<SelectBidModel> fetchSelectBid({Map? body}) async {
    RestAPIService restAPIService = RestAPIService(Dio());

    var result = await restAPIService.postService(
        url: App24UserAppAPI.selectBid, body: body);
    print("body of ride" + body.toString());

    return compute(selectBidModelFromJson, result);
  }

  Future<RiderDetailsModel> fetchTaxiRiders() async {
    RestAPIService restAPIService = RestAPIService(Dio());

    var result = await restAPIService.getService(
      url: App24UserAppAPI.rideCheckRequest,
    );
    print("result dony" + result.toString());
    return compute(riderDetailsModelFromJson, result);
  }

  Future<VehicleInfoModel> fetchSendPackageVehicleInfo(Map? body) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.postService(
        url: App24UserAppAPI.vehicleInfoUrl, body: body);

    return compute(vehicleInfoModelFromJson, result);
  }

  Future<dynamic> fetchOrderConfirmation(BuildContext? context, body) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.postService(
        url: App24UserAppAPI.orderConfirmationUrl, body: body);
    return result;
    // return compute(manageAddressModelFromJson, result);
  }

  Future<CancelOrderModel1> fetchCancelOrder({required Map body}) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var cancelOrder = await restAPIService.patchService(
        url: App24UserAppAPI.cancelOrder, body: body);
    return compute(cancelOrderModelFromJson, cancelOrder);
  }

  Future<dynamic> fetchWalletAddMoney({required Map body}) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.postService(
        url: App24UserAppAPI.walletAddMoney, body: body);
    return result;
    // return compute(manageAddressModelFromJson, result);
  }

  Future<CheckWalletBalanceModel> fetchWalletBalance(
      {required Map body}) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var fetchWallet = await restAPIService.postService(
        url: App24UserAppAPI.checkWalletBalanceUrl, body: body);
    return compute(checkWalletBalanceModelFromJson, fetchWallet);
  }

  Future<dynamic> fetchPayWithWallet({required Map body}) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var paymentResult = await restAPIService.postService(
        url: App24UserAppAPI.payWithWalletUrl, body: body);
    return paymentResult;
  }

  Future<PromoCodesModel> fetchPromoCodes() async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.getService(
      url: App24UserAppAPI.promoCodes,
    );
    return compute(promoCodesModelFromJson, result);
  }

  Future<LandingPagePromoCode> fetchLandingPagePromoCodes(
      {required String latLong}) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.getService(
      url: App24UserAppAPI.LandingPagePromoCodes + latLong,
    );
    return compute(landingPagePromoCodesModelFromJson, result);
  }

  Future<PromoCodesModel> fetchRating({required Map body}) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.patchService(
        url: App24UserAppAPI.rating, body: body);
    return compute(promoCodesModelFromJson, result);
  }

  Future<dynamic> fetchRemoveCart({required Map body}) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var cartRemove = await restAPIService.postService(
        url: App24UserAppAPI.cartRemove, body: body);
    return cartRemove;
  }

//2024 laundry
  Future<dynamic> fetchlaundryRemoveCart({required Map body}) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var cartRemove = await restAPIService.postService(
        url: App24UserAppAPI.laundrycartRemove, body: body);
    return cartRemove;
  }

  Future<GlobalSearchModel> fetchGlobalSearch() async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var globalSearch =
        await restAPIService.getService(url: App24UserAppAPI.globalSearch);
    return compute(globalSearchModelFromJson, globalSearch);
  }

  Future<dynamic> fetchDeleteAddress({required String addressID}) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var deleteAddress = await restAPIService.deleteService(
        url: App24UserAppAPI.deleteAddress + addressID);
    return deleteAddress;
  }

  Future<dynamic> fetchFavouriteShop({required Map body}) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var favouriteShop = await restAPIService.postService(
        url: App24UserAppAPI.addOrRemoveFavouriteShop, body: body);
    return favouriteShop;
  }

  Future<FavouriteShopList> fetchFavouriteShopList() async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var favouriteShop =
        await restAPIService.getService(url: App24UserAppAPI.favouriteShopList);
    return compute(favouriteShopListModelFromJson, favouriteShop);
  }

  Future<dynamic> fetchClearCart() async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var clearCart =
        await restAPIService.getService(url: App24UserAppAPI.clearCart);
    return clearCart;
  }

  Future<OrderDetailsModel> fetchOrderDetails({required String orderId}) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var orderDetails = await restAPIService.getService(
        url: App24UserAppAPI.orderDetails + orderId);
    return compute(orderDetailsModelFromJson, orderDetails);
  }

  Future<RideCancelReasonsModel> fetchRideCancelReasons() async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var rideCancelReasons =
        await restAPIService.getService(url: App24UserAppAPI.rideCancelReasons);
    return compute(rideCancelReasonsModelFromJson, rideCancelReasons);
  }

  Future<dynamic> fetchCancelRide({required Map body}) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var cancelRide = await restAPIService.postService(
        url: App24UserAppAPI.cancelRide, body: body);
    return cancelRide;
  }

  Future<dynamic> fetchRideSendRequest({required Map body}) async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var cancelRide = await restAPIService.postService(
        url: App24UserAppAPI.rideSendRequest, body: body);
    return cancelRide;
  }
}
