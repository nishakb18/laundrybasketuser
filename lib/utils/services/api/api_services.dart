import 'dart:developer';

import 'package:app24_user_app/constants/api_path.dart';
import 'package:app24_user_app/constants/shared_preferences_path.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/utils/services/api/api_exceptions.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RestAPIService {
  final Dio _dio;

  RestAPIService(this._dio);

  getHeaders(bool useToken) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? token = prefs.getString(SharedPreferencesPath.accessToken);
    _dio.options.headers['content-Type'] = 'application/json';
    if (useToken) _dio.options.headers["authorization"] = "Bearer $token";
    showLog("TOKEN ====>>>> $token");
    PrettyDioLogger(
      requestHeader: true,
      requestBody: true,
      responseBody: true,
      responseHeader: true,
      error: true,
      request: true,
      logPrint: (object) {
        Logger.appLogs("error on dio logger:: $object");
      },
    );
  }

  Future<dynamic> postService(
      {required String url,
      body,
      useToken = true,
      bool isMultiPart = false}) async {
    String fetchUrl;
    var response;
    try {
      await getHeaders(useToken);
      if (url.contains("http"))
        fetchUrl = url;
      else
        fetchUrl = App24UserAppAPI.baseUrl + url;

      Options options = Options();
      if (isMultiPart) {
        options = Options(contentType: 'multipart/form-data');
      }
       response = await _dio.post(fetchUrl, data: body, options: options);
      showLog("==============================================================");
      showLog("URL ====>>>>>> POST : " + fetchUrl);
      showLog("BODY ====>>>>>>" + body.toString() );
      showLog("RESPONSE ====>>>>>>" + response.data.toString());
      showLog("==============================================================");
      return response.data;
    } on DioException catch (dioError) {
      showLog("==============================================================");
      showLog(" Error URL ====>>>>>> POST : ${App24UserAppAPI.baseUrl + url}");
      showLog("Error is : ${dioError.message}");
      showLog("==============================================================");
      if (RestAPIException.fromDioError(dioError).message == "Unauthorized") {
        showUnAuthorisedPopUp();
      }
      throw RestAPIException.fromDioError(dioError);
    }
  }

  getService({required String url, useToken = true}) async {
     //showLog("get service called   $url");
    String fetchUrl;
    var prefs = await SharedPreferences.getInstance();
    var userRegStatus =
        prefs.getBool('${SharedPreferencesPath.isUserRegisteredKey}') ?? false;
    try {
      await getHeaders(useToken);
      showLog("HEADERS ====>>>>>>" + _dio.options.headers.toString());
      if (url.contains("http")) {
        fetchUrl = url;
      } else {
        fetchUrl = App24UserAppAPI.baseUrl + url;
      }
      if(userRegStatus) {
        showLog("==============================================================");
        showLog("URL ====>>>>>> GET : $fetchUrl" );
        showLog("==============================================================");
        var response = await _dio.get(fetchUrl);
        showLog(
            "==============================================================");
        showLog("RESPONSE ====>>>>>> ${response.data.toString()}");
        showLog(
            "==============================================================");

        return response.data;
      }
    } on DioException catch (dioError) {
      //showLog("dony dioError.message");
      //showLog(dioError.message.toString());
      showLog("==============================================================");
      showLog("Error : URL ====>>>>>> GET : ${App24UserAppAPI.baseUrl}$url");
      showLog("Error : ${dioError.message.toString()}");
      showLog("==============================================================");
      if (RestAPIException.fromDioError(dioError).message == "Unauthorized") {
        showLog("Error : ${dioError.requestOptions}");
        showUnAuthorisedPopUp();
      }

      throw RestAPIException.fromDioError(dioError);
    }
  }

  deleteService({required String url, useToken = true}) async {
    try {
      showLog("baseUrl and url: " + App24UserAppAPI.baseUrl + url);
      await getHeaders(useToken);
      var response = await _dio.delete(
        App24UserAppAPI.baseUrl + url,
      );

      return response;
    } on DioError catch (dioError) {
      throw RestAPIException.fromDioError(dioError);
    }
  }

  putService({required String url, useToken = true}) async {
    try {
      getHeaders(useToken);
      var response = await _dio.put(App24UserAppAPI.baseUrl + url);

      return response;
    } on DioError catch (dioError) {
      throw RestAPIException.fromDioError(dioError);
    }
  }

  Future<dynamic> patchService(
      {required String url, body, useToken = true}) async {
    String fetchUrl;
    try {
      await getHeaders(useToken);
      if (url.contains("http"))
        fetchUrl = url;
      else
        fetchUrl = App24UserAppAPI.baseUrl + url;

      var response = await _dio.patch(fetchUrl, data: body);
      showLog("URL ====>>>>>> PATCH : " + fetchUrl);
      showLog("BODY ====>>>>>>" + body.toString());
      showLog("RESPONSE ====>>>>>>" + response.data.toString());
      return response.data;
    } on DioError catch (dioError) {
      if (RestAPIException.fromDioError(dioError).message == "Unauthorized") {
        showUnAuthorisedPopUp();
      }
      throw RestAPIException.fromDioError(dioError);
    }
  }
}

class Logger {
  Logger._();

  static logToFirebase(err, s) {
    // FirebaseCrashlytics.instance.recordError(err, s);
  }

  static appLogs(msg) {
    if (kDebugMode) {
      log(msg.toString());
    }
  }
}