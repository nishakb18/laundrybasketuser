import 'package:app24_user_app/modules/delivery/cart/cart_count_notifier.dart';
import 'package:app24_user_app/modules/delivery/cart/cart_notifier.dart';
import 'package:app24_user_app/modules/delivery/item_list/item_list_notifier.dart';
import 'package:app24_user_app/modules/delivery/order_essentials/order_essentials_notifier.dart';
import 'package:app24_user_app/modules/delivery/shop_list/laundryshop_provider.dart';
import 'package:app24_user_app/modules/delivery/shop_list/shop_list_notifier.dart';
import 'package:app24_user_app/modules/my_account/address/manage_address_notifier.dart';
import 'package:app24_user_app/modules/my_account/profile/profile_notifier.dart';
import 'package:app24_user_app/modules/my_account/wallet/wallet_add_money/wallet_add_money_notifier.dart';
import 'package:app24_user_app/modules/my_account/wallet/wallet_notifier.dart';
import 'package:app24_user_app/modules/restaurant/favourite_notifier.dart';
import 'package:app24_user_app/modules/restaurant/restaurant_1st_page/Cuisines/cuisine_notifier.dart';
import 'package:app24_user_app/modules/restaurant/restaurant_shops/restaurants_notifier.dart';
import 'package:app24_user_app/modules/ride/ride_select_bid_notifier.dart';
import 'package:app24_user_app/modules/ride/ride_vehicles_notifier.dart';
import 'package:app24_user_app/modules/ride/ride_vehicles_request_notifier.dart';
import 'package:app24_user_app/modules/send_packages/send_package_notifier.dart';
import 'package:app24_user_app/modules/shop_list/store_list_notifier.dart';
import 'package:app24_user_app/modules/sign_in/login_notifier.dart';
import 'package:app24_user_app/modules/tabs/history/history_notifier.dart';
import 'package:app24_user_app/modules/tabs/history/order_history_notifier.dart';
import 'package:app24_user_app/modules/tabs/home/global_search/global_search_notifier.dart';
import 'package:app24_user_app/modules/tabs/home/home_notifier.dart';
import 'package:app24_user_app/modules/tabs/notifications/notification_notifier.dart';
import 'package:app24_user_app/providers/location_picker_provider.dart';
import 'package:app24_user_app/providers/order_details_provider.dart';
import 'package:app24_user_app/providers/wallet_balance_provider.dart';
import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/widgets/bottomsheets/cancel_order/cancel_order_notifier.dart';
import 'package:app24_user_app/widgets/bottomsheets/cancel_ride/cancel_ride_notifier.dart';
import 'package:app24_user_app/widgets/bottomsheets/checkout_bottomSheet/checkout_notifier.dart';
import 'package:app24_user_app/widgets/bottomsheets/promocodes/landing_page_promo_code/landing_page_promo_code_notifier.dart';
import 'package:app24_user_app/widgets/bottomsheets/promocodes/promocodes_notifier.dart';
import 'package:app24_user_app/widgets/bottomsheets/rating/rating_notifier.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final homeApiRepositoryProvider = Provider<ApiRepository>(
  (ref) => ApiRepository(),
);

final homeNotifierProvider = StateNotifierProvider(
  (ref) => HomeNotifier(ref.watch(homeApiRepositoryProvider)),
);

final servicesNotifierProvider = StateNotifierProvider(
  (ref) => HomeNotifier(ref.watch(homeApiRepositoryProvider)),
);

final walletNotifierProvider = StateNotifierProvider(
  (ref) => WalletNotifier(ref.watch(homeApiRepositoryProvider)),
);

final notificationNotifierProvider = StateNotifierProvider(
  (ref) => NotificationNotifier(ref.watch(homeApiRepositoryProvider)),
);

final historyNotifierProvider = StateNotifierProvider(
  (ref) => HistoryNotifier(ref.watch(homeApiRepositoryProvider)),
);

final currentHistoryNotifierProvider = StateNotifierProvider(
  (ref) => HistoryNotifier(ref.watch(homeApiRepositoryProvider)),
);

final upcomingHistoryNotifierProvider = StateNotifierProvider(
  (ref) => HistoryNotifier(ref.watch(homeApiRepositoryProvider)),
);

final locationNotifierProvider =
    ChangeNotifierProvider<LocationNotifier>((ref) => new LocationNotifier());

final orderDetailsNotifierProvider = StateNotifierProvider(
  (ref) => OrderDetailsNotifier(ref.watch(homeApiRepositoryProvider)),
);

final selectPackageNotifier = StateNotifierProvider(
  (ref) => SendPackageNotifier(ref.watch(homeApiRepositoryProvider)),
);

final selectVehicleNotifier = StateNotifierProvider(
  (ref) => SendPackageNotifier(ref.watch(homeApiRepositoryProvider)),
);

final orderPackageNotifier = StateNotifierProvider(
  (ref) => SendPackageNotifier(ref.watch(homeApiRepositoryProvider)),
);

final orderBuyItemsNotifier = StateNotifierProvider(
  (ref) => OrderEssentialNotifier(ref.watch(homeApiRepositoryProvider)),
);

final orderNotifierProvider = StateNotifierProvider(
  (ref) => OrderEssentialNotifier(ref.watch(homeApiRepositoryProvider)),
);
//laundry
final laundryNotifierProvider = StateNotifierProvider(
  (ref) => OrderEssentialNotifier(ref.watch(homeApiRepositoryProvider)),
);

final shopListNotifierProvider = StateNotifierProvider(
  (ref) => ShopListNotifier(ref.watch(homeApiRepositoryProvider)),
);
//laundryshopdetails
final laundryShopProvider = StateNotifierProvider(
    (ref) => LaundryShopListNotifier(ref.watch(homeApiRepositoryProvider)));

final itemListNotifierProvider = StateNotifierProvider(
  (ref) => ItemListNotifier(ref.watch(homeApiRepositoryProvider)),
);

final shopDetailsNotifierProvider = StateNotifierProvider(
        (ref) => ShopDetailsNotifier(ref.watch(homeApiRepositoryProvider)),
);

final serviceListNotifierProvider = StateNotifierProvider(
  (ref) => ServiceItemsListNotifier(ref.watch(homeApiRepositoryProvider)),
);

final serviceList1NotifierProvider = StateNotifierProvider(
        (ref) => ServiceItemsList1Notifier(ref.watch(homeApiRepositoryProvider)),
);

final newAddToCartNotifierProvider = StateNotifierProvider(
        (ref) => NewAddToCartNotifier(ref.watch(homeApiRepositoryProvider)),
);

final profileNotifierProvider = StateNotifierProvider(
  (ref) => ProfileNotifier(ref.watch(homeApiRepositoryProvider)),
);

final addressNotifierProvider = StateNotifierProvider(
  (ref) => AddressNotifier(ref.watch(homeApiRepositoryProvider)),
);

final addUpdateAddressNotifierProvider = StateNotifierProvider(
  (ref) => AddressNotifier(ref.watch(homeApiRepositoryProvider)),
);

final cartNotifierProvider = StateNotifierProvider(
  (ref) => CartListNotifier(ref.watch(homeApiRepositoryProvider)),
);
final checkoutBSheetProvider = StateNotifierProvider(
  (ref) => CheckoutBottomSheetNotifier(ref.watch(homeApiRepositoryProvider)),
);

final cartCountNotifierProvider = StateNotifierProvider(
  (ref) => CartCountNotifier(ref.watch(homeApiRepositoryProvider)),
);

final restaurantNotifier = StateNotifierProvider(
  (ref) => RestaurantsNotifier(ref.watch(homeApiRepositoryProvider)),
);

final favouriteShopNotifier = StateNotifierProvider(
  (ref) => FavouriteNotifier(ref.watch(homeApiRepositoryProvider)),
);

final favouriteShopListNotifier = StateNotifierProvider(
  (ref) => FavouriteNotifier(ref.watch(homeApiRepositoryProvider)),
);

final cuisineNotifier = StateNotifierProvider(
  (ref) => CuisineNotifier(ref.watch(homeApiRepositoryProvider)),
);

final rideVehicleListNotifier = StateNotifierProvider(
  (ref) => RideVehiclesNotifier(ref.watch(homeApiRepositoryProvider)),
);
final rideVehiclesRequestNotifier = StateNotifierProvider(
  (ref) => RideVehiclesRequestNotifier(ref.watch(homeApiRepositoryProvider)),
);
final rideSelectBidNotifier = StateNotifierProvider(
  (ref) => RideSelectBidNotifier(ref.watch(homeApiRepositoryProvider)),
);

final cancelOrderNotifier = StateNotifierProvider(
  (ref) => CancelOrderNotifier(ref.watch(homeApiRepositoryProvider)),
);

final walletBalanceNotifierProvider = StateNotifierProvider(
  (ref) => WalletBalanceNotifier(ref.watch(homeApiRepositoryProvider)),
);

final payWithWalletNotifierProvider = StateNotifierProvider(
  (ref) => WalletBalanceNotifier(ref.watch(homeApiRepositoryProvider)),
);

final promoCodesProvider = StateNotifierProvider(
  (ref) => PromoCodesNotifier(ref.watch(homeApiRepositoryProvider)),
);

final landingPagePromoCodeProvider = StateNotifierProvider(
  (ref) => LandingPagePromoCodeNotifier(ref.watch(homeApiRepositoryProvider)),
);

final ratingProvider = StateNotifierProvider(
  (ref) => RatingNotifier(ref.watch(homeApiRepositoryProvider)),
);

final walletAddMoneyProvider = StateNotifierProvider(
  (ref) => WalletAddMoneyNotifier(ref.watch(homeApiRepositoryProvider)),
);
final globalSearchProvider = StateNotifierProvider(
  (ref) => GlobalSearchNotifier(ref.watch(homeApiRepositoryProvider)),
);
final sendOtpProvider = StateNotifierProvider(
  (ref) => LoginProvider(ref.watch(homeApiRepositoryProvider)),
);
final storeListProvider = StateNotifierProvider(
        (ref) => StoreListProvider(ref.watch(homeApiRepositoryProvider)),
);
final rideSendRequestProvider = StateNotifierProvider(
  (ref) => CancelRide(ref.watch(homeApiRepositoryProvider)),
);
final rideCancelProvider = StateNotifierProvider(
  (ref) => CancelRide(ref.watch(homeApiRepositoryProvider)),
);

final orderHistoryDetails = StateNotifierProvider(
  (ref) => OrderHistoryNotifier(ref.watch(homeApiRepositoryProvider)),
);
final rideCancelReasons = StateNotifierProvider(
  (ref) => CancelRide(ref.watch(homeApiRepositoryProvider)),
);
