abstract class ResponseState {
  const ResponseState();
}

class ResponseInitial extends ResponseState {
  const ResponseInitial();
}

class ResponseLoading extends ResponseState {
  const ResponseLoading();
}

class ResponseLoaded extends ResponseState {
  final dynamic response;

  const ResponseLoaded(this.response);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is ResponseLoaded && o.response == response;
  }

  @override
  int get hashCode => response.hashCode;

  ResponseLoaded copyWith({dynamic response}) {
    return ResponseLoaded(response ?? this.response);
  }
}

class ResponseError extends ResponseState {
  final String message;

  const ResponseError(this.message);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is ResponseError && o.message == message;
  }

  @override
  int get hashCode => message.hashCode;
}
