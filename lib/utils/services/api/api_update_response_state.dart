//import 'package:app24_user_app/modules/my_account/UpdateResponse/UpdateResponse_transaction_model.dart';

abstract class UpdateResponseState {
  const UpdateResponseState();
}

class UpdateResponseInitial extends UpdateResponseState {
  const UpdateResponseInitial();
}

class UpdateResponseLoading extends UpdateResponseState {
  const UpdateResponseLoading();
}

class UpdateResponseLoaded extends UpdateResponseState {
  final dynamic UpdateResponse;

  const UpdateResponseLoaded(this.UpdateResponse);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is UpdateResponseLoaded && o.UpdateResponse == UpdateResponse;
  }

  @override
  int get hashCode => UpdateResponse.hashCode;
}

class UpdateResponseError extends UpdateResponseState {
  final String message;

  const UpdateResponseError(this.message);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is UpdateResponseError && o.message == message;
  }

  @override
  int get hashCode => message.hashCode;
}
