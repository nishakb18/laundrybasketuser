import 'package:dio/dio.dart';

class RestAPIException implements Exception {
  RestAPIException.fromDioError(DioError dioError) {
    switch (dioError.type) {
      case DioExceptionType.cancel:
        message = "Request to API server was cancelled";
        break;
      case DioExceptionType.connectionTimeout:
        message = "Connection timeout with API server";
        break;
      case DioExceptionType.connectionError:
        message = "Connection to API server failed due to internet connection";
        break;
      case DioExceptionType.receiveTimeout:
        message = "Receive timeout in connection with API server";
        break;
      case DioExceptionType.values:
        message = _handleError(
            dioError.response!.statusCode, dioError.response!.data);
        break;
      case DioExceptionType.sendTimeout:
        message = "Send timeout in connection with API server";
        break;
      default:
       // message = "Something went wrong";
        message = _handleError(
            dioError.response!.statusCode, dioError.response!.data);
        break;
    }
  }

  String? message;

  String? _handleError(int? statusCode, data) {
    switch (statusCode) {
      case 400:
        return data['message'];//'Bad request';
      case 401:
        return 'Unauthorized';
      case 422:
        return data['message'];
      case 404:
        return 'The requested resource was not found';
      case 500:
        return 'Internal server error';
      default:
        return 'Oops something went wrong';
    }
  }

  @override
  String toString() => message!;
}
