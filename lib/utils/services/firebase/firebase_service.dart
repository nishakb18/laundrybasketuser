import 'dart:async';
import 'dart:io';

import 'package:app24_user_app/constants/shared_preferences_path.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ringtone_player/flutter_ringtone_player.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:shared_preferences/shared_preferences.dart';

late FlutterTts flutterTts = FlutterTts();
//flutterTts
Timer? minuteTimer;
Timer? secondsTimer;
Future<Map<dynamic, dynamic>?> myBackgroundMessageHandler(message) async {
  print("myBackgroundMessageHandler ");

  String messageToPlay = message.notification!.body.toString();

  if (messageToPlay
      .contains("Purchase completed. Please pay the bill amount")) {
    print("messageToPlay" + messageToPlay);

    await flutterTts.setVolume(1);
    await flutterTts.awaitSpeakCompletion(true);
    await flutterTts.speak(messageToPlay);

    //   minuteTimer = Timer.periodic(Duration(seconds: 10), (Timer t1) async {
    FlutterRingtonePlayer ringtonePlayer = FlutterRingtonePlayer();

    ringtonePlayer.play(
      android: AndroidSounds.notification,
      ios: IosSounds.glass,
      looping: true, // Android only - API >= 28
      volume: 1.0, // Android only - API >= 28
      asAlarm: false, // Android only - all APIs
    );
    await flutterTts.setVolume(1.0);
    await flutterTts.awaitSpeakCompletion(true);
    //await flutterTts.speak("You have received a new order from APP 24");
    await flutterTts.speak(messageToPlay);

    secondsTimer = Timer.periodic(Duration(seconds: 2), (Timer t) async {
      print("t :" + t.tick.toString());

      if (t.tick >= 6) {
        // minuteTimer?.cancel();
        await flutterTts.setVolume(1.0);
        await flutterTts.awaitSpeakCompletion(true);
        //await flutterTts.speak("You have received a new order from APP 24");
        await flutterTts.speak(messageToPlay);
        secondsTimer?.cancel();
        ringtonePlayer.stop();
      }
    });
    //  });
  }

  return null;
}

class FirebaseNotifications {
  FirebaseMessaging? _firebaseMessaging;
  BuildContext context;

  FirebaseNotifications(this.context);

  void setUpFirebase() {
    _firebaseMessaging = FirebaseMessaging.instance;
    firebaseCloudMessagingListeners();
  }

  Future<void> firebaseCloudMessagingListeners() async {
    if (Platform.isIOS) ;
    String? fireBaseToken;
    try {
      await _firebaseMessaging?.getToken().then((token) {
        fireBaseToken = token;
        showLog("firebase token :" + fireBaseToken.toString());
      });

      _firebaseMessaging?.onTokenRefresh.listen((token) {
        showLog("firebase token refresh:" + fireBaseToken.toString());
        fireBaseToken = token;
      });

      FirebaseMessaging.onMessage.listen((RemoteMessage event) {
        print("message recieved");

        print(event.notification!.body.toString());
      });

      FirebaseMessaging.onMessageOpenedApp.listen((message) {
        FlutterRingtonePlayer ringtonePlayer = FlutterRingtonePlayer();

        print("on message opened app");
        minuteTimer!.cancel();
        secondsTimer!.cancel();

        print(message.notification!.body.toString());
      });

      FirebaseMessaging.onBackgroundMessage(myBackgroundMessageHandler);

      var prefs = await SharedPreferences.getInstance();
      // PackageInfo packageInfo = await PackageInfo.fromPlatform();

      // String version = packageInfo.version; // using 2.1 for testing purpose
      String version = "4.9.F";
      prefs.setString(SharedPreferencesPath.playStoreVersionKey, version);

      if (fireBaseToken !=
          prefs.getString(SharedPreferencesPath.firebaseTokenKey)) {
        prefs.setString(SharedPreferencesPath.firebaseTokenKey, fireBaseToken ?? '');
      }
      //showLog('firebase token is ' + prefs.getString(SharedPreferencesPath.firebaseTokenKey) ?? '');
    } on Exception catch (e) {
      showLog('firebase token refresh error $e');
    }

/*    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        showLog('on message $message');

        if (message.containsKey('data')) {
          final dynamic data = message['data'];
          showLog('on message data $message');
        }
      },
      onBackgroundMessage: myBackgroundMessageHandler,
      onResume: (Map<String, dynamic> message) async {
        if (message.containsKey('data')) {
          final dynamic data = message['data'];
          showLog('on resume data $message');
        }
      },
      onLaunch: (Map<String, dynamic> message) async {
        if (message.containsKey('data')) {
          final dynamic data = message['data'];
          showLog('on launch data $message');
        }
      },
    );*/
  }

  // void iOSPermission() {
  //   _firebaseMessaging.requestNotificationPermissions(
  //       IosNotificationSettings(sound: true, badge: true, alert: true));
  //   _firebaseMessaging.onIosSettingsRegistered
  //       .listen((IosNotificationSettings settings) {
  //     showLog("Settings registered: $settings");
  //   });
  // }
}
