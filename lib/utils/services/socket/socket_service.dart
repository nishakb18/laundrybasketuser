
import 'package:app24_user_app/config/routes.dart';
import 'package:app24_user_app/constants/api_path.dart';
import 'package:app24_user_app/constants/shared_preferences_path.dart';
import 'package:app24_user_app/modules/ride/confirm_ride_booking.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:socket_io_client/socket_io_client.dart';

class SocketService{

  // make this a singleton class
  SocketService._privateConstructor();

  static final SocketService instance = SocketService._privateConstructor();

  // only have a single app-wide reference to the socket
  static Socket? _socket;

  Future<Socket?> get socket async {
    if (_socket != null && _socket!.connected) return _socket;

    _socket = await _initSocket();
    return _socket;
  }

  _initSocket() async {
    var prefs = await SharedPreferences.getInstance();
    var _accessToken = prefs.getString(SharedPreferencesPath.accessToken);

    Socket connectSocket = io(App24UserAppAPI.socketUrl,
        OptionBuilder()
            .setTransports(['websocket']) // for Flutter or Dart VM
            .disableAutoConnect()  // disable auto-connection
            .setExtraHeaders({'Authorization': 'JWT $_accessToken'}) // optional
            .build()
    );
    connectSocket.connect();
    return connectSocket;
  }

  listenSocket() async {
    print("Lemon");
    var prefs = await SharedPreferences.getInstance();
    showLog("orderRequest : orderRequest starting.......");
    Socket? socket = await this.socket;

    socket!.on('orderRequest', (data){

      //socket listening
      showLog("orderRequest : orderRequest : $data");
      navigatorKey.currentContext!
          .read(orderDetailsNotifierProvider.notifier)
          .getOrderDetails(orderType: prefs.getString(SharedPreferencesPath.orderType));

      // navigatorKey.currentContext!.read(rideVehiclesRequestNotifier.notifier).getRiderDetails();
    });
    socket.on('rideRequest', (data){

      //socket listening
      showLog("rideRequest : rideRequest : $data");
      // navigatorKey.currentContext!
      //     .read(orderDetailsNotifierProvider.notifier)
      //     .getOrderDetails(orderType: prefs.getString(SharedPreferencesPath.orderType));

      navigatorKey.currentContext!.read(rideVehiclesRequestNotifier.notifier).getRiderDetails();
      ConfirmRideBookingPage();
    });
  }

  /*//late IO.Socket socket;
  late Socket socket;

  connectAndListen() async {
    try {
      var prefs = await SharedPreferences.getInstance();
      var _accessToken = await prefs.getString(SharedPreferencesPath.accessToken);

        socket = io(App24UserAppAPI.socketUrl,
          OptionBuilder()
              .setTransports(['websocket']) // for Flutter or Dart VM
              .disableAutoConnect()  // disable auto-connection
              .setExtraHeaders({'Authorization': 'JWT $_accessToken'}) // optional
              .build()
      );
      socket.connect();

*//*      socket = IO.io(App24UserAppAPI.socketUrl, <String, dynamic>{
        'transports': ['websocket'],
        'extraHeaders': {'Authorization': 'JWT $_accessToken'} // optional
      });

      socket.onConnect((data) {
        showLog("Socket Connected : ${socket.connected}");
        socket.emit('Authorization', 'JWT $_accessToken');

        //check status
        socket.on(SocketRoomNames.STATUS, (data){
          showLog("Socket Status : $data");
        });
      });*//*



      socket.on('connecting', (data) => print("connecting"));
      socket.on('reconnect', (data) => print("reconnect"));
      socket.on('reconnect_attempt', (data) => print("reconnect_attempt"));
      socket.on('reconnect_failed', (data) => print("reconnect_failed"));
      socket.on('reconnect_error', (data) => print("reconnect_error"));
      socket.on('reconnecting', (data) => print("reconnecting"));
      socket.on('event', (data) => print(data));
      socket.on('ping', (data) => print("pinging"));
      socket.on('error', (error) {
        print('error');
        print(error);
      });
      socket.on('receive_message', (data) => print(data));
      socket.on('disconnect', (_) => print('disconnect'));
      socket.on('fromServer', (_) => print(_));

     // socket.open();

    }catch(e){
      print("error3 " + e.toString());
    }
  }

  checkConnectionStatus(){
    showLog("Socket Connected : ${socket.connected}");
    if(!socket.connected)
      socket.connect();
  }*/

}