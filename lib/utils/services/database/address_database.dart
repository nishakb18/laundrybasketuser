import 'dart:async';

import 'package:app24_user_app/models/location_model.dart';
import 'package:app24_user_app/modules/my_account/address/manage_address_model.dart';
import 'package:app24_user_app/utils/services/database/database_service.dart';
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'package:uuid/uuid.dart';

class AddressDatabase {
  final DatabaseService _service;

  AddressDatabase(this._service);

  Future<dynamic> insertLocation(AddressResponseData location) async {
    print("location   addressid " +location.addressid.toString());
    final Database? db = await _service.database;
    List<Map> results = await db!.query(_service.addressTable,
        columns: [
          _service.addressPriority,

        ],
        where:
            "${_service.addressTableId} = ? ",
        whereArgs: [location.addressid]);

    if (results.length > 0) {
      var res =
          LocationModel.fromLocalDBJson(results.first as Map<String, dynamic>);
     // location.priority = res.priority! + 1;
     // location.location_id = res.location_id;

      return await db.update(_service.addressTable, location.toMap(),
          where:
          "${_service.addressTableId} = ? ",
          whereArgs: [location.addressid]);
    } else {
      //location.location_id = Uuid().v4();
     // location.priority = 1;
      return await db.insert(
        _service.addressTable,
        location.toMap(),
      );
    }
  }


  Future<int> updatePriority(AddressResponseData location) async {
    int i=0;

    final Database? db = await _service.database;

    List<Map> results = await db!.query(_service.addressTable,
        columns: [
          _service.addressPriority,

        ],
        where:
        "${_service.addressTableId} = ? ",
        whereArgs: [location.addressid]);

    if (results.length > 0) {
      var res =
      AddressResponseData.fromJsonDB(results.first as Map<String, dynamic>);

  if(res.priority!=null) {
    location.priority = res.priority! + 1;
  }
  else {
    location.priority =  1;
  }
     // location.location_id = res.location_id;

      i= await db.update(_service.addressTable, location.toMap(),
          where:
          "${_service.addressTableId} = ? ",
          whereArgs: [location.addressid]);

  }
  return i;

  }
  Future<int> updateAddress(AddressResponseData location) async {
    int i=0;
   print("ID : "+location.addressid.toString());
    final Database? db = await _service.database;

    List<Map> results = await db!.query(_service.addressTable,
        columns: [
          _service.addressPriority,

        ],
        where:
        "${_service.addressTableId} = ? ",
        whereArgs: [location.addressid]);
    print("results length"+results.length.toString());
    if (results.length > 0) {
      print("results length"+results.length.toString());
     /* var res =
      AddressResponseData.fromJsonDB(results.first as Map<String, dynamic>);

      if(res.priority!=null) {
        location.priority = res.priority! + 1;
      }
      else {
        location.priority =  1;
      }*/
      // location.location_id = res.location_id;

      i= await db.update(_service.addressTable, location.toMap(),
          where:
          "${_service.addressTableId} = ? ",
          whereArgs: [location.addressid]);

    }
    return i;

  }

  Future<List<AddressResponseData>>  fetchAddresses() async {

    final Database? db = await _service.database;
    List<AddressResponseData>  userAddresses=[];

    final List<Map<String, dynamic>> maps = await db!.rawQuery(
        'SELECT * FROM ${_service.addressTable}  ORDER BY ${_service.addressPriority} DESC');
    maps.forEach((v) {
      userAddresses.add(new AddressResponseData.fromJsonDB(v));
    });

    return userAddresses;



  }

  Future<dynamic> addUserAddress(AddressResponseData addressData) async {
    try{
      final Database? db = await _service.database;
//print("adres saved first: "+addressData.toJson().toString());
     return await db!.insert(
        _service.addressTable,
        addressData.toJson(),
      );


    } on DatabaseException catch (e){
      print("data base exception  : "+e.toString());

      //showToast(message: 'Customer with same delivery location and phone number exist!');

    }

  }
  Future<dynamic> deleteUserAddress({required String addressID}) async {
    try{
      final Database? db = await _service.database;
  print("deleted addressID"+addressID);

      int result = await db!.delete(
          _service.addressTable, //table name
          where: "${_service.addressTableId}= ?",
          whereArgs: [addressID] // use whereArgs to avoid SQL injection
      );
      print("rows deleted: "+result.toString());

    } on DatabaseException catch (e){

      print("data base exception  : "+e.toString());

      //showToast(message: 'Customer with same delivery location and phone number exist!');

    }

  }
  Future<dynamic> deleteUserAddresses() async {
    try{
      final Database? db = await _service.database;

      db!.delete (_service.addressTable);
    } on DatabaseException catch (e){

      print("data base exception  : "+e.toString());

      //showToast(message: 'Customer with same delivery location and phone number exist!');

    }

  }

 Future<bool> fetchAddressTypesInDB({required String? addressTypeSelected}) async {
   final Database? db = await _service.database;
 print("addressTypeSelected in db"+addressTypeSelected.toString());
   List<Map> results = await db!.query(_service.addressTable,
       columns: [
         _service.addressTitle,

       ],
       where: "${_service.addressTitle}= ?",
       whereArgs: [addressTypeSelected]
   );


   if (results.length > 0) {
     return true;
   }
   else return false;
 }
/*
  Future<List<LocationModel>> getLocations(String query) async {
    final Database? db = await _service.database;

    List<String> sentence = query.split(" ");
    String whereCondition = '';
    String conditionsWithOR = '';

    for (var i = 0; i < sentence.length; i++) {
      if (i == 0) {
        whereCondition +=
            " WHERE ${_service.locationColumnDescription} LIKE '${sentence[i]}%'";
      } else {
        if (i != 1) conditionsWithOR += " OR ";

        conditionsWithOR +=
            " ${_service.locationColumnDescription} LIKE '%${sentence[i]}%'";
      }
    }
    if (conditionsWithOR != '') whereCondition += " AND ( $conditionsWithOR )";

    final List<Map<String, dynamic>> maps = await db!
        .rawQuery("SELECT * FROM ${_service.locationTable} $whereCondition");

    return List.generate(maps.length, (i) {
      return LocationModel(
          description: maps[i]['description'],
          placeID: maps[i]['place_id'],
          main_text: maps[i]['title'],
          secondary_text: maps[i]['sub_title'],
          latitude: maps[i]['latitude'],
          longitude: maps[i]['longitude'],
          pinColor: Colors.green);
    });
  }*/
}
