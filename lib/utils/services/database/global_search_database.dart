import 'dart:async';
import 'package:app24_user_app/modules/tabs/home/global_search/global_search_model.dart';
import 'package:app24_user_app/utils/services/database/database_service.dart';
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';

class GlobalSearchDataBase {
  final DatabaseService _service;

  GlobalSearchDataBase(this._service);

  Future<dynamic> addGlobalSearchRslt(SearchCategory globalSearch) async {
    final Database? db = await _service.database;
    return await db!.insert(
      _service.globalSearchTable,
      globalSearch.toJson(),
    );
  }
  Future<dynamic> deleteGlobalSearchRslt() async {
    try{
      final Database? db = await _service.database;

      db!.delete (_service.globalSearchTable);
    } on DatabaseException catch (e){

      print("data base exception  : "+e.toString());

      //showToast(message: 'Customer with same delivery location and phone number exist!');

    }

  }

  Future<List<SearchCategory>> fetchLocalGlobalSearch({required String keyword}) async {
    final Database? db = await _service.database;
    List<SearchCategory> globalSearchList = [];

    // int offset = limit * (page - 1);
    final List<Map<String, dynamic>> maps = await db!.rawQuery(
        "SELECT ${_service.globalSearchColumnId},${_service.globalSearchColumnAdminService},"
            "${_service.globalSearchColumnName},${_service.globalSearchColumnPicture} "
            "FROM ${_service.globalSearchTable} WHERE ${_service.globalSearchColumnKeyWords} "
            "LIKE '%$keyword%'"
      // "SELECT * FROM ${_service.globalSearchTable}"
        );

    maps.forEach((v) {
      globalSearchList.add(new SearchCategory.fromJson(v));
    });

    return globalSearchList;
  }
}
