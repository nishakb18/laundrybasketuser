import 'dart:io' show Directory;

import 'package:path/path.dart' show join;
import 'package:path_provider/path_provider.dart'
    show getApplicationDocumentsDirectory;
import 'package:sqflite/sqflite.dart';

class DatabaseService {
  static final _databaseName = "App24UserAppDatabase.db";
  static final _databaseVersion = 1;

  final addressTable = 'address';

  final addressTableId = 'address_table_id';
  final addressUserId='user_id';
  final  addressCompanyId='company_id';
  final addressType='address_type';
  final addressLandmark='landmark';
  final addressFlatNo='flat_no';
  final addressStreet='street';
  final addressCity='city';
  final addressState='state';
  final addressCounty='county';
  final addressTitle='title';
  final addressZipcode='zipcode';
  final addressLatitude='latitude';
  final addressLongitude='longitude';
  final addressMapAddress='map_address';
  final addressPriority = 'priority';
  //location table
  final locationTable = 'locations';

  final locationColumnLocationId = 'location_id';
  final locationColumnPlaceId = 'place_id';
  final locationColumnTitle = 'title';
  final locationColumnSubTitle = 'sub_title';
  final locationColumnDescription = 'description';
  final locationColumnLatitude = 'latitude';
  final locationColumnLongitude = 'longitude';
  final locationColumnType = 'type'; // 0 - pick up, 1 - drop, 3 - saved address
  final locationColumnHouseName = 'house_name';
  final locationColumnStreet = 'street';
  final locationColumnLandmark = 'landmark';
  final locationColumnLabel = 'label';
  final locationColumnPriority = 'priority';

  //history table
  final historyTable = 'history';

  final historyColumnId = 'id';
  final historyColumnAdminService = 'admin_service';
  final historyColumnDescription = 'desc';
  final historyColumnAmount = 'amount';
  final historyColumnDateTime = 'date_time';
  final historyColumnStatus = 'status';
  final historyColumnBookingType = 'booking_type';


  //global search table

  final globalSearchTable = 'global_search';
  final globalSearchTables = 'global_searches';

  final globalSearchColumnKeyWords = "keywords";
  final globalSearchColumnId = "id";
  final globalSearchColumnPicture = "picture";
  final globalSearchColumnName ="name";
  final globalSearchColumnAdminService = "admin_service";

  // make this a singleton class
  DatabaseService._privateConstructor();

  static final DatabaseService instance = DatabaseService._privateConstructor();

  // only have a single app-wide reference to the database
  static Database? _database;

  Future<Database?> get database async {

    print(_database);
    if (_database != null) return _database;
    // lazily instantiate the db the first time it is accessed
    _database = await _initDatabase();
    return _database;
  }

  // this opens the database (and creates it if it doesn't exist)
  _initDatabase() async {
    print("database check opening");
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate);
  }


  // SQL code to create the database table
  Future _onCreate(Database db, int version) async {
    print("database created");
    // _initDatabase();
    await db.execute('''
          CREATE TABLE  IF NOT EXISTS $addressTable (
           id  INTEGER PRIMARY KEY AUTOINCREMENT,
            $addressUserId INTEGER,
            $addressCompanyId INTEGER,
            $addressType TEXT,
            $addressLandmark TEXT,
            $addressLatitude REAL,
            $addressLongitude REAL,
            $addressFlatNo TEXT,
             $addressStreet TEXT,
              $addressCity TEXT,
               $addressState TEXT,
                $addressCounty TEXT,
                $addressTitle TEXT,
                $addressZipcode TEXT,
                $addressMapAddress TEXT,
                 $addressTableId INTEGER,
                $addressPriority INTEGER
          )
          ''');
    await db.execute('''
          CREATE TABLE  IF NOT EXISTS $locationTable (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            $locationColumnLocationId TEXT NOT NULL,
            $locationColumnPlaceId TEXT,
            $locationColumnTitle TEXT,
            $locationColumnSubTitle TEXT,
            $locationColumnDescription TEXT,
            $locationColumnLatitude REAL,
            $locationColumnLongitude REAL,
            $locationColumnType INTEGER,
             $locationColumnHouseName TEXT,
              $locationColumnStreet TEXT,
               $locationColumnLandmark TEXT,
                $locationColumnLabel TEXT,
                $locationColumnPriority INTEGER
          )
          ''');

    await db.execute("""
     CREATE TABLE  IF NOT EXISTS $historyTable(
              auto_id INTEGER PRIMARY KEY AUTOINCREMENT,
              $historyColumnId INTEGER,
              $historyColumnAdminService TEXT,
              $historyColumnDescription TEXT,
              $historyColumnAmount TEXT,
              $historyColumnDateTime TEXT,
              $historyColumnStatus TEXT,
               $historyColumnBookingType TEXT
              )
     """);

    await db.execute("""
    CREATE TABLE  IF NOT EXISTS $globalSearchTable(
              auto_id INTEGER PRIMARY KEY AUTOINCREMENT,
              $globalSearchColumnAdminService TEXT,
              $globalSearchColumnId INTEGER,
              $globalSearchColumnKeyWords TEXT,
              $globalSearchColumnName TEXT,
              $globalSearchColumnPicture TEXT,        
              UNIQUE($globalSearchColumnId,$globalSearchColumnAdminService)
    )
    """);

    await db.execute("""
    CREATE INDEX keyWordIndex ON $globalSearchTable ($globalSearchColumnKeyWords)
     """);
  }
}
