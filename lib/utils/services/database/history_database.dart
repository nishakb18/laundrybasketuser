import 'dart:async';

import 'package:app24_user_app/constants/global_constants.dart';
import 'package:app24_user_app/models/history_model.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/services/api/api_services.dart';
import 'package:app24_user_app/utils/services/database/database_service.dart';
import 'package:sqflite/sqflite.dart';

class HistoryDatabase {
  final DatabaseService _service;

  HistoryDatabase(this._service);

  int limit = 10;

  Future<int> addHistory(HistoryData historyData) async {
    print("booking type: "+historyData.toString());
    final Database? db = await _service.database;

    return await db!.insert(
      _service.historyTable,
      historyData.toJson(),
    );
  }

  Future<dynamic> updateHistory(
      {required String status, required int id}) async {
    try {
      final Database? db = await _service.database;
      Map<String, dynamic> row = {
        _service.historyColumnStatus: status,
      };
      var res = await db!.update(_service.historyTable, row,
          where: '${_service.historyColumnId} = ?', whereArgs: [id]);
      print("result is : $res");
      return res;
    } catch (e) {
      print("error is : ${e.toString()}");

    }
  }
  Future<dynamic> deleteHistory() async {
    try{
      final Database? db = await _service.database;

      db!.delete (_service.historyTable);
    } on DatabaseException catch (e){

      print("data base exception  : "+e.toString());

      //showToast(message: 'Customer with same delivery location and phone number exist!');

    }

  }

  Future<List<HistoryData>> fetchLocalHistory({required int page}) async {
    final Database? db = await _service.database;
    List<HistoryData> historyList = [];

    int offset = limit * (page - 1);
   showLog("offset "+offset.toString()+" limit "+limit.toString());
    final List<Map<String, dynamic>> maps = await db!.rawQuery(
        "SELECT * FROM ${_service.historyTable} WHERE ${_service.historyColumnStatus} in "
        "( '${GlobalConstants.orderStatusOrderCompleted}', '${GlobalConstants.orderStatusCancelled}' ) "
        "ORDER BY ${_service.historyColumnDateTime} DESC LIMIT $limit OFFSET $offset");

    print("Query : "+"SELECT * FROM ${_service.historyTable} WHERE ${_service.historyColumnStatus} in "
        "( '${GlobalConstants.orderStatusOrderCompleted}', '${GlobalConstants.orderStatusCancelled}' ) "
        "ORDER BY ${_service.historyColumnDateTime} DESC LIMIT $limit OFFSET $offset");
    Logger.appLogs("fetchLocalHistory response :: $maps");
    maps.forEach((v) {
      historyList.add(new HistoryData.fromJson(v));
    });

    return historyList;
  }

  Future<List<HistoryData>> fetchNonCompleted() async {
    final Database? db = await _service.database;
    List<HistoryData> historyList = [];
    final List<Map<String, dynamic>> maps = await db!.rawQuery(
        "SELECT * FROM ${_service.historyTable} WHERE ${_service.historyColumnStatus} not in "
        "( '${GlobalConstants.orderStatusOrderCompleted}', '${GlobalConstants.orderStatusCancelled}' ) "
        "ORDER BY ${_service.historyColumnDateTime} DESC");
    print(maps);
    maps.forEach((v) {
      historyList.add(new HistoryData.fromJson(v));
      print("history orders::"+historyList.first.status.toString());
    });
    return historyList;
  }
}
