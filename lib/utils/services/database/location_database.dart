import 'dart:async';

import 'package:app24_user_app/models/location_model.dart';
import 'package:app24_user_app/modules/my_account/address/manage_address_model.dart';
import 'package:app24_user_app/utils/services/database/database_service.dart';
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'package:uuid/uuid.dart';

class LocationDatabase {
  final DatabaseService _service;

  LocationDatabase(this._service);

  Future<dynamic> insertLocation(LocationModel location) async {
    final Database? db = await _service.database;
    List<Map> results = await db!.query(_service.locationTable,
        columns: [
          _service.locationColumnPriority,
          _service.locationColumnLocationId
        ],
        where:
            "${_service.locationColumnDescription} = ? AND ${_service.locationColumnType} = ?",
        whereArgs: [location.description, location.type]);

    if (results.length > 0) {
      var res =
          LocationModel.fromLocalDBJson(results.first as Map<String, dynamic>);
      location.priority = res.priority! + 1;
      location.location_id = res.location_id;

      return await db.update(_service.locationTable, location.toMap(),
          where:
          "${_service.locationColumnDescription} = ? AND ${_service.locationColumnType} = ?",
          whereArgs: [location.description, location.type]);
    } else {

      location.location_id = Uuid().v4();
      location.priority = 1;
      return await db.insert(
        _service.locationTable,
        location.toMap(),
      );
    }
  }

  Future<List<LocationModel>> getRecentSearches(type) async {
    // print("type bottom sheet title: "+type);
    final Database? db = await _service.database;

    final List<Map<String, dynamic>> maps = await db!.rawQuery(
        'SELECT * FROM ${_service.locationTable} WHERE ${_service.locationColumnType} = $type ORDER BY ${_service.locationColumnPriority} DESC');

    return List.generate(maps.length, (i) {
      return LocationModel(
          description: maps[i]['description'],
          placeID: maps[i]['place_id'],
          main_text: maps[i]['title'],
          secondary_text: maps[i]['sub_title'],
          latitude: maps[i]['latitude'],
          longitude: maps[i]['longitude']);
    });
  }
  Future<dynamic> deleteLocations() async {
    try{
      final Database? db = await _service.database;

      db!.delete (_service.locationTable);
    } on DatabaseException catch (e){

      print("data base exception  : "+e.toString());

      //showToast(message: 'Customer with same delivery location and phone number exist!');

    }

  }

  Future<List<LocationModel>> getLocations(String query) async {
    final Database? db = await _service.database;
 print("query in getlocations "+query);
    List<String> sentence = query.split(" ");
    String whereCondition = '';
    String conditionsWithOR = '';

    for (var i = 0; i < sentence.length; i++) {
      if (i == 0) {
        whereCondition +=
            " WHERE ${_service.locationColumnTitle} LIKE '${sentence[i]}%'";
      } else {
        if (i != 1) conditionsWithOR += " OR ";

        conditionsWithOR +=
            " ${_service.locationColumnTitle} LIKE '%${sentence[i]}%'";
      }
    }
    if (conditionsWithOR != '') whereCondition += " AND ( $conditionsWithOR )";

    final List<Map<String, dynamic>> maps = await db!
        .rawQuery("SELECT DISTINCT *  FROM ${_service.locationTable} $whereCondition GROUP BY ${_service.locationColumnTitle} ");
//${_service.locationColumnTitle} , ${_service.locationColumnSubTitle}
    return List.generate(maps.length, (i) {
      print(i);
      return LocationModel(
          description: maps[i]['description'],
          placeID: maps[i]['place_id'],
          main_text: maps[i]['title'],
          secondary_text: maps[i]['sub_title'],
          latitude: maps[i]['latitude'],
          longitude: maps[i]['longitude'],
          pinColor: Colors.green);
    });
  }
}
