class RestAPIHelper {
  // RestAPIHelper._();

  getResults(jsonResponse) {
    // var jsonResponse = json.decode(responseBody);
    Map<String, dynamic> result = {
      'statusCode': jsonResponse["statusCode"] == null
          ? null
          : jsonResponse["statusCode"],
      'title': jsonResponse["title"] == null ? null : jsonResponse["title"],
      'message':
          jsonResponse["message"] == null ? null : jsonResponse["message"],
      'responseData': jsonResponse["responseData"] == null
          ? null
          : jsonResponse["responseData"],
      'error': jsonResponse["error"] == null ? null : jsonResponse["error"],
    };
    return result;
  }
}
