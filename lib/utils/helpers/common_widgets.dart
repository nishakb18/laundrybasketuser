import 'package:app24_user_app/config/routes.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/helpers.dart';
import 'package:app24_user_app/models/location_model.dart';
import 'package:app24_user_app/modules/authentication/login/login.dart';
import 'package:app24_user_app/modules/home.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/widgets/bottomsheets/search_location_bottom_sheet.dart';
import 'package:app24_user_app/widgets/bottomsheets/search_location_bsheet_address.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/src/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'common_helpers.dart';

Widget getTitle(title) {
  return Text(
    title,
    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
  );
}

Widget getSubTitle(subtitle, context) {
  return Row(
    children: <Widget>[
      Padding(
          padding: EdgeInsets.symmetric(vertical: 5),
          child: Text(
            subtitle,
            style: TextStyle(
              color: App24Colors.darkTextColor,
              fontWeight: FontWeight.w600,
            ),
          ))
    ],
  );
}

InputDecoration getDecoration(context) {
  return InputDecoration(
    border: OutlineInputBorder(
      borderRadius: BorderRadius.circular(8),
      borderSide: BorderSide(
        width: 0,
        style: BorderStyle.none,
      ),
    ),
    contentPadding: EdgeInsets.symmetric( vertical:1,horizontal: 10),
    fillColor: Theme.of(context).cardColor,
    filled: true,
  );
}

void getDropIcon(key, context, distance) {
  RepaintBoundary(
      key: key,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: Theme.of(context).cardColor),
              child: Row(
                children: [
                  Container(
                    decoration: BoxDecoration(
                        color: App24Colors.greenOrderEssentialThemeColor,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(8),
                            bottomLeft: Radius.circular(8))),
                    padding: EdgeInsets.all(5),
                    child: Icon(
                      Icons.near_me,
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "$distance away",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                ],
              )),
          SizedBox(
            height: 5,
          ),
          Container(
            decoration: BoxDecoration(
                color: Colors.red[500],
                shape: BoxShape.circle,
                border: Border.all(color: Colors.red[200]!, width: 9.0)),
            padding: EdgeInsets.all(8),
          ),
        ],
      ));
}

Widget getDetailsRow(label, var value, BuildContext context) {
  return Row(
    children: <Widget>[
      Text(
        label,
        style: TextStyle(
            fontWeight: FontWeight.w600,
            fontSize: MediaQuery.of(context).size.width / 28),
      ),
      SizedBox(
        width: 15,
      ),
      Expanded(
        child: Text(
          value,
          textAlign: TextAlign.end,
          style: TextStyle(
              fontWeight: FontWeight.w600,
              color: App24Colors.lightBlue,
              fontSize: MediaQuery.of(context).size.width / 28),
        ),
      )
    ],
  );
}

getCommonBottomSheet(BuildContext context, Widget content) {
  showModalBottomSheet<dynamic>(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30), topRight: Radius.circular(30)),
      ),
      isScrollControlled: true,
      context: context,
      builder: (ctx) {
        return content;
      });
}

showDistanceExceededPopUp({required BuildContext context}) {
  Helpers().getCommonAlertDialogBox(
    context: navigatorKey.currentContext!,
    barrierDismissible: false,
    content: AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      title: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(15), topRight: Radius.circular(15)),
            color: App24Colors.redRestaurantThemeColor),
        height: 100,
        child: Icon(
          CupertinoIcons.clear_circled_solid,
          size: 50,
          color: Colors.white,
        ),
      ),
      // const Text("Session Expired"),
      titlePadding: EdgeInsets.all(0),
      content: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              const Text(
                "Drop Location too far",
                style: TextStyle(fontWeight: FontWeight.w800),
              ),
              const SizedBox(
                height: 10,
              ),
              const Text(
                "Please select a nearby drop point",
                style: TextStyle(fontWeight: FontWeight.w600),
              ),
            ],
          ),
        ),
      ),
      actions: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              new MaterialButton(
                // padding: EdgeInsets.symmetric(horizontal: 80),
                color: App24Colors.redRestaurantThemeColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                child: const Text(
                  "Change location",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  showSelectLocationSheet(context: context, title: "title")
                      .then((value) {
                    if (value != null) {
                      LocationModel model = value;

                      context
                          .read(homeNotifierProvider.notifier)
                          .currentLocation = model;
                      // loadData(context: context);
                    }
                  });

                  showMessage(
                      context, "Please change your location", true, false);
                }
                // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomePage()));}
                /*  Navigator.push(context,
                              MaterialPageRoute(builder: (context) => LogInPage()))*/
                ,
              ),
              new MaterialButton(
                // padding: EdgeInsets.symmetric(horizontal: 60),
                color: App24Colors.redRestaurantThemeColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                child: const Text(
                  "Clear cart",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  Navigator.pop(context);
                  context
                      .read(cartNotifierProvider.notifier)
                      .getClearCart(context: context);
                }
                /*  Navigator.push(context,
                              MaterialPageRoute(builder: (context) => LogInPage()))*/
                ,
              )
            ],
          ),
        ),
      ],
    ),
  );

  // showDialog(
  //     context: navigatorKey.currentContext!,
  //     barrierDismissible: false,
  //     builder: (context) =>
  //         AlertDialog(
  //           shape: RoundedRectangleBorder(
  //               borderRadius: BorderRadius.circular(15)),
  //           title: Container(
  //             decoration: BoxDecoration(borderRadius: BorderRadius.only(
  //                 topLeft: Radius.circular(15), topRight: Radius.circular(15)),
  //                 color: App24Colors.redRestaurantThemeColor),
  //             height: 100,
  //             child: Icon(CupertinoIcons.clear_circled_solid,size: 50,color: Colors.white,),),
  //           // const Text("Session Expired"),
  //           titlePadding: EdgeInsets.all(0),
  //           content: SingleChildScrollView(
  //             child: Center(
  //               child: Column(
  //                 children: [
  //                   const Text("Drop Location too far",
  //                     style: TextStyle(fontWeight: FontWeight.w800),),
  //                   const SizedBox(height: 10,),
  //                   const Text("Please select a nearby drop point",
  //                     style: TextStyle(fontWeight: FontWeight.w600),),
  //                 ],
  //               ),
  //             ),
  //           ),
  //           actions: [
  //             Center(
  //               child: new MaterialButton(
  //                 padding: EdgeInsets.symmetric(horizontal: 80),
  //                 color: App24Colors.redRestaurantThemeColor,
  //                 shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
  //                 child: const Text("Ok",style: TextStyle(color: Colors.white),),
  //                 onPressed: () =>Navigator.pop(context)
  //                   /*  Navigator.push(context,
  //                         MaterialPageRoute(builder: (context) => LogInPage()))*/,
  //               ),
  //             ),
  //           ],
  //         )).then((value) async {
  // SharedPreferences prefs = await SharedPreferences.getInstance();
  // prefs.clear();
}

showUnAuthorisedPopUp() {
  showGeneralDialog(
      barrierColor: Colors.black.withOpacity(0.5),
      transitionBuilder: (context, a1, a2, widget) {
        return Transform.scale(
          scale: a1.value,
          child: Opacity(
            opacity: a1.value,
            child: AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15)),
              title: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(15),
                        topRight: Radius.circular(15)),
                    color: App24Colors.redRestaurantThemeColor),
                height: 100,
                child: Icon(
                  CupertinoIcons.clear_circled_solid,
                  size: 50,
                  color: Colors.white,
                ),
              ),
              // const Text("Session Expired"),
              titlePadding: EdgeInsets.all(0),
              content: SingleChildScrollView(
                child: Center(
                  child: Column(
                    children: [
                      const Text(
                        "Session Expired",
                        style: TextStyle(fontWeight: FontWeight.w800),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const Text(
                        "Please log in again",
                        style: TextStyle(fontWeight: FontWeight.w600),
                      ),
                    ],
                  ),
                ),
              ),
              actions: [
                Center(
                  child: new MaterialButton(
                    padding: EdgeInsets.symmetric(horizontal: 80),
                    color: App24Colors.redRestaurantThemeColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    child: const Text(
                      "Ok",
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () => Navigator.push(context,
                        MaterialPageRoute(builder: (context) => LogInPage())),
                  ),
                ),
              ],
            ),
          ),
        );
      },
      transitionDuration: Duration(milliseconds: 200),
      barrierDismissible: false,
      barrierLabel: '',
      context: navigatorKey.currentContext!,
      pageBuilder: (context, animation1, animation2) {
        return Container();
      }).then((value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
  });
}

showSelectLocationSheetForAddress(
    {required BuildContext context, bool initialCall = true, String? title}) {
  return showGeneralDialog(
      context: context,
      barrierDismissible: true,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: Colors.black54,
      transitionBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset(0.0, 1.0);
        var end = Offset.zero;
        var curve = Curves.ease;

        var tween =
            Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
      pageBuilder: (BuildContext context, _, __) =>
          SearchLocationBottomSheetAddressPage(
            initialCall: initialCall,
            title: title,
          ));
}

showSelectLocationSheet(
    {required BuildContext context,
    bool initialCall = true,
    String? title,
    bool? isDismissible,
    bool? enableDrag,
    bool? willPopDismissible,
    bool? searchUserLocation}) {
  return showGeneralDialog(
      context: context,
      barrierDismissible: isDismissible == false ? false : true,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: Colors.black54,
      transitionBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset(0.0, 1.0);
        var end = Offset.zero;
        var curve = Curves.ease;

        var tween =
            Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
      pageBuilder: (BuildContext context, _, __) => WillPopScope(
            onWillPop: () {
              return Future.value(willPopDismissible == false ? false : true);
            },
            child: SearchLocationBottomSheetPage(
              searchUserLocation: searchUserLocation,
              initialCall: initialCall,
              title: title,
              showSearchMapOption: false,
            ),
          ));
}

Future<void> showProgress(BuildContext context) async {
  showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Center(
          child: CupertinoActivityIndicator(radius: 20),
        );
      });
}
