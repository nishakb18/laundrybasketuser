import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/models/location_model.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:geocoding/geocoding.dart' as geoCoding;
import 'package:location/location.dart';

showLog(String msg) {
  debugPrint(msg);
}

showMessage(BuildContext context, String msg,bool isError,bool showIcon) {
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      duration: Duration(milliseconds: 900),
      //width:MediaQuery.of(context).size.width*.60,

     // padding: EdgeInsets.only(left: 30,right: 30),
      behavior: SnackBarBehavior.fixed,
backgroundColor: isError==true?App24Colors.redRestaurantThemeColor:App24Colors.greenOrderEssentialThemeColor,
      content:

      Row(
        children: [
        if(showIcon == true)
        Icon(isError==true?Icons.cancel:Icons.check_circle_outline,color: Colors.white,),
        SizedBox(width: 10,),
        Container(
          width:MediaQuery.of(context).size.width*.75,
          child:
        Text(msg,style: TextStyle(fontSize: MediaQuery.of(context).size.width/25),),),
      ],)

    ),
  );
}

Future<Map<String, dynamic>> getRazorPayOptions(
    {required BuildContext context, required Map values}) async {

  late String key;
  late String firstName;
  late String mobile;

  if (context.read(homeNotifierProvider.notifier).appConfiguration == null) {
    await context.read(homeNotifierProvider.notifier).getAppConfigurations();
  }

  key = context.read(homeNotifierProvider.notifier).razorPayKey!;




  firstName =context
      .read(homeNotifierProvider.notifier)
      .profile!
      .responseData!
      .firstName!;
  mobile = context
      .read(homeNotifierProvider.notifier)
      .profile!
      .responseData!
      .mobile!;



  var options = {
    "key": key,
    "amount": values['amount'] * 100, // Convert Paisa to Rupees
    "name": firstName,
    "description": values['description'],
    "notes": values['notes'],
    "currency": "INR",
    "prefill": {"contact": mobile, "email": "app24india@gmail.co.in"},
  };
  //print("options "+options.toString());

  return options;
}

// Method for retrieving the current location
Future<dynamic> getCurrentLocation() async {
  final Location location = new Location();
  late bool _serviceEnabled;
  PermissionStatus? _permissionGranted;

  _serviceEnabled = await location.serviceEnabled();
  if (!_serviceEnabled) {
    _serviceEnabled = await location.requestService();
    if (!_serviceEnabled) {
      return;
    }
  }

  _permissionGranted = await location.hasPermission();
  if (_permissionGranted == PermissionStatus.denied) {
    _permissionGranted = await location.requestPermission();
    if (_permissionGranted != PermissionStatus.granted) {
      return;
    }
  }

  LocationData _currentLocation = await location.getLocation();
  var res =
      await getAddress(_currentLocation.latitude!, _currentLocation.longitude!);
  return res;
}

// Method for retrieving the address
Future<LocationModel?> getAddress(double lat, double long) async {
  LocationModel? locationModel;
  try {
    List<geoCoding.Placemark> placeMarks =
        await geoCoding.placemarkFromCoordinates(lat, long);
    String? title;
    String? subTitle;
    String? address;

    var first = placeMarks.first;
    title = first.name;
    subTitle = first.locality;
    address =
        "${first.name}, ${first.locality}, ${first.postalCode}, ${first.country}";

    locationModel = LocationModel(
        description: address,
        main_text: title,
        secondary_text: subTitle,
        latitude: lat,
        longitude: long);
    print(locationModel);
    return locationModel;
  } catch (e) {
    showLog("locationModel error :: ${e.toString()}");
    return locationModel;
  }
}
