import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/api/api_response_state2.dart';
import 'package:app24_user_app/utils/services/database/database_service.dart';
import 'package:app24_user_app/utils/services/database/history_database.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class LocationNotifier extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  LocationNotifier(this._apiRepository) : super(ResponseState2.initial());

  HistoryDatabase historyDatabase = HistoryDatabase(DatabaseService.instance);

  int pageNo = 1;

  Future<void> getPastHistory(
      {bool init = true, int page = 1}) async {
    if (page == 1 || page > pageNo) {
      try {
        pageNo = page;
        if (init) state = state.copyWith(isLoading: true);

        final pastHistory = await historyDatabase.fetchLocalHistory(page: page);

        if (pastHistory.isEmpty) {
          getPastHistoryFromServer();
        } else {
/*        if(page == 1)
          state = ResponseLoaded(pastHistory);
        else {
          state = ResponseLoaded(pastHistory).copyWith();
        }*/
          state = state.copyWith(
              response: [...state.response, ...pastHistory],
              isLoading: false,
              isError: false);
        }
      } catch (e) {
        print(e.toString());
        state = state.copyWith(
            errorMessage: e.toString(), isLoading: false, isError: true);
        // state = ResponseError(e.toString());
      }
    }
  }

  Future<void> getPastHistoryFromServer() async {
    try {
      final history = await _apiRepository.fetchHistory(type: "past");
      if (history.responseData != null) {
        history.responseData!.order!.forEach((element) {
          historyDatabase.addHistory(element);
        });
        history.responseData!.ride!.forEach((element) {
          historyDatabase.addHistory(element);
        });
        history.responseData!.service!.forEach((element) {
          historyDatabase.addHistory(element);
        });
        getPastHistory(init: false);
      } else {
        state = state.copyWith(response: []);
      }
    } catch (e) {
      // state = ResponseError(e.toString());
      state = state.copyWith(
          errorMessage: e.toString(), isLoading: false, isError: true);
    }
  }
}
