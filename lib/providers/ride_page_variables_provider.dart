import 'package:app24_user_app/utils/services/api/api_response_state2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class RideVariablesNotifier extends StateNotifier<ResponseState2> {
  RideVariablesNotifier() : super(ResponseState2(isLoading: true));

  final pickUpAddressController = TextEditingController();
  final dropAddressController = TextEditingController();
  bool isPickUpLocationFetching = true;
  Set<Marker> markers = {};
  Map<PolylineId, Polyline>? polyLines = {};
  int selectedVehicleIndex = -1;
  String distance = '';
}
