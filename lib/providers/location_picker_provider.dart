import 'dart:async';

import 'package:app24_user_app/constants/api_path.dart';
import 'package:app24_user_app/models/location_model.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/database/database_service.dart';
import 'package:app24_user_app/utils/services/database/location_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:uuid/uuid.dart';

class LocationNotifier with ChangeNotifier {
  LocationNotifier();

  LocationModel? currentLocation;

  BuildContext? context;
  String? sessionToken;
  String? mapKey;

  Future<List<LocationModel>> getSuggestions(String query) async {
    LocationDatabase locationDatabase =
        LocationDatabase(DatabaseService.instance);
    print("query: " + query);

    final listFromLocalDB = await locationDatabase.getLocations(query);
    print("local db length: " + listFromLocalDB.length.toString());
    if (listFromLocalDB.isEmpty) {
      final listFromServer = await searchLocationFromDB(query);
      if (listFromServer.isEmpty) {
        final listFromGoogle = await searchLocationFromGoogle(query);
        return listFromGoogle;
      } else {
        return listFromServer;
      }
    } else {
      return listFromLocalDB;
    }
  }

  Future<List<LocationModel>> searchLocationFromDB(String query) async {
    ApiRepository _apiRepository = ApiRepository();
    String url = App24UserAppAPI.autoCompleteLocationUrl + query;
    if (currentLocation != null &&
        currentLocation!.latitude != null &&
        currentLocation!.longitude != null) {
      url = url +
          "&lat=${currentLocation!.latitude}&lng=${currentLocation!.longitude}";
    }
    final list = await _apiRepository.fetchLocation(url, 'server');
    return list;
  }

  Future<List<LocationModel>> searchLocationFromGoogle(String query) async {
    ApiRepository _apiRepository = ApiRepository();
    String url;
    /*  url =
        "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=$query&inputtype=textquery&"
            "fields=formatted_address,name,geometry&key=$mapKey";*/
    url =
        "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=$query"
        "&sessiontoken=$sessionToken"
        "&key=$mapKey";
    final list = await _apiRepository.fetchLocation(url, 'google');
    return list;
  }

  Future<Map> setLatLongFromPlaceId(String? placeID) async {
    ApiRepository _apiRepository = ApiRepository();

    final url =
        'https://maps.googleapis.com/maps/api/place/details/json?place_id=$placeID&fields=geometry&key=$mapKey&sessiontoken=$sessionToken';

    final placeDetails = await _apiRepository.fetchLatitudeLongitude(url);
    setSessionToken();
    return placeDetails;
  }

  Future<dynamic> saveAutoCompleteLocationToServer(Map body) async {
    ApiRepository _apiRepository = ApiRepository();

    final result =
        await _apiRepository.fetchAutoCompleteSaveLocationToServer(body);
    return result;
  }

  void setInitialValues({BuildContext? context}) {
    context = context;
    if (mapKey == null)
      mapKey = context!.read(homeNotifierProvider.notifier).mapKey;

    if (sessionToken == null) setSessionToken();
  }

  void setSessionToken() {
    sessionToken = Uuid().v4();
  }
}
