import 'package:app24_user_app/constants/api_path.dart';
import 'package:app24_user_app/models/delivery_menu_model.dart';
import 'package:app24_user_app/utils/services/api/api_services.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class DeliveryMenuProvider extends ChangeNotifier {
  DeliveryMenuProvider();

  double constraintHeight = 0.0;

  getOffers() async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result =
        await restAPIService.getService(url: App24UserAppAPI.homeScreenMenuUrl);
    final results = List<Map<String, dynamic>>.from(result['responseData']);
    List<DeliveryMenu> movies = results
        .map((movieData) => DeliveryMenu.fromJson(movieData))
        .toList(growable: false);
    return movies;
  }

  getConstraintHeightValue() {
    return constraintHeight;
  }

  setConstraintHeightValue(val) {
    constraintHeight = val;
    notifyListeners();
  }
}
