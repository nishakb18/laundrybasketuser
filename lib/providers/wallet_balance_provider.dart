import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/api/api_response_state2.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class WalletBalanceNotifier extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  WalletBalanceNotifier(this._apiRepository)
      : super(ResponseState2(isLoading: true));

  Future<void> getWalletBalance(
      {bool init = true, required String amountToBePaid,required int orderId}) async {
    try {
      if (init) state = state.copyWith(isLoading: true);
      Map body = {
        'amount': amountToBePaid,
        "id" : orderId
      };
      final walletDetails = await _apiRepository.fetchWalletBalance(body: body);

      state = state.copyWith(
          response: walletDetails, isLoading: false, isError: false);
    } catch (e) {
      state = state.copyWith(
          errorMessage: e.toString(), isLoading: false, isError: true);
    }
  }

  Future<void> payWithWallet({bool init = true, required Map body}) async {
    try {
      if (init) state = state.copyWith(isLoading: false);

      final walletDetails = await _apiRepository.fetchPayWithWallet(body: body);

      state = state.copyWith(
          response: walletDetails, isLoading: false, isError: false);
    } catch (e) {
      state = state.copyWith(
          errorMessage: e.toString(), isLoading: false, isError: true);
    }
  }
}
