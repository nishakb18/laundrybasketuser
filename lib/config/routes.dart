import 'package:app24_user_app/Laundry-app/laundry.dart';
import 'package:app24_user_app/Laundry-app/laundrymain.dart';
import 'package:app24_user_app/Laundry-app/views/DashbordScreens/dashbord_screen.dart';
import 'package:app24_user_app/constants/route_path.dart';
// import 'package:app24_user_app/delete.dart';
import 'package:app24_user_app/models/location_model.dart';
import 'package:app24_user_app/modules/authentication/login/login.dart';
import 'package:app24_user_app/modules/delivery/add_list.dart';
import 'package:app24_user_app/modules/delivery/cart/cart.dart';
import 'package:app24_user_app/modules/delivery/order_essentials/order_essentials_page.dart';
import 'package:app24_user_app/modules/delivery/shop_list/shop_categories.dart';
import 'package:app24_user_app/modules/home.dart';
import 'package:app24_user_app/modules/login/signUp_page.dart';
import 'package:app24_user_app/modules/my_account/address/add_update_address.dart';
import 'package:app24_user_app/modules/my_account/address/manage_address.dart';
import 'package:app24_user_app/modules/my_account/payments.dart';
import 'package:app24_user_app/modules/my_account/profile/change_password.dart';
import 'package:app24_user_app/modules/my_account/profile/profile.dart';
import 'package:app24_user_app/modules/my_account/support.dart';
import 'package:app24_user_app/modules/my_account/wallet/wallet_screen.dart';
import 'package:app24_user_app/modules/ride/confirm_ride_booking.dart';
import 'package:app24_user_app/modules/ride/ride.dart';
import 'package:app24_user_app/modules/ride/ride_completed.dart';
import 'package:app24_user_app/modules/ride/ride_details.dart';
import 'package:app24_user_app/modules/ride/ride_payment.dart';
import 'package:app24_user_app/modules/ride/track_ride.dart';
import 'package:app24_user_app/modules/select_zone.dart';
import 'package:app24_user_app/modules/services/confirm_service_booking.dart';
import 'package:app24_user_app/modules/services/services.dart';
import 'package:app24_user_app/modules/sign_in/otp_verification.dart';
import 'package:app24_user_app/modules/sign_in/sign_in.dart';
import 'package:app24_user_app/modules/splash.dart';
import 'package:app24_user_app/modules/tabs/history/history_tab.dart';
import 'package:app24_user_app/modules/tabs/account/my_account_tab.dart';
import 'package:app24_user_app/modules/tabs/notifications/notifications_tab.dart';
import 'package:flutter/material.dart';

import '../Laundry-app/views/Shoporder/shop_order_screen.dart';

late final navigatorKey = GlobalKey<NavigatorState>();

class Routes {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case homeScreen:
        return MaterialPageRoute(
          builder: (_) => HomePage(),
        );
      case loginScreen:
        return MaterialPageRoute(
          builder: (_) => LogInPage(),
        );
      case accountScreen:
        return MaterialPageRoute(
          builder: (_) => MyAccountTabPage(),
        );
      case notificationScreen:
        return MaterialPageRoute(
          builder: (_) => NotificationsTabPage(),
        );
      case historyScreen:
        return MaterialPageRoute(
          builder: (_) => HistoryTabPage(),
        );
      case rideScreen:
        return MaterialPageRoute(
          builder: (_) => RidePage(
            initializedDropLocation: LocationModel(),
          ),
        );
      case splashScreen:
        return MaterialPageRoute(
          builder: (_) => SplashScreen(),
        );
      case laundrybasketsplashScreen:
        return MaterialPageRoute(
          builder: (_) => LaundryBasketSplashScreen(),
        );
      case cartScreen:
        return MaterialPageRoute(
          builder: (_) => const Cart(),
        );
        case shopDetails:
        return MaterialPageRoute(
          builder: (_) => const ShopOrderScreen(),
        );
      case addListScreen:
        return MaterialPageRoute(
          builder: (_) => AddListPage(
            storeTypeID: '',
          ),
        );
      case orderEssentialsScreen:
        return MaterialPageRoute(
          builder: (_) => ShopEssentials(),
        );
      case shopCategoriesScreen:
        return MaterialPageRoute(
          builder: (_) => const ShopCategoryPage(),
        );
      case addAddressScreen:
        return MaterialPageRoute(
          builder: (_) => const AddUpdateAddressPage(),
        );
      case manageAddressScreen:
        return MaterialPageRoute(
          builder: (_) => const ManageAddressPage(),
        );
      case changePasswordScreen:
        return MaterialPageRoute(
          builder: (_) => ChangePasswordPage(),
        );
      case profileScreen:
        return MaterialPageRoute(
          builder: (_) => ProfilePage(),
        );
      case paymentScreen:
        return MaterialPageRoute(
          builder: (_) => PaymentsPage(),
        );
      //Referral Page
      // case referralsScreen:
      //   return MaterialPageRoute(
      //     builder: (_) => ReferralPage(),
      //   );
      case supportScreen:
        return MaterialPageRoute(
          builder: (_) => SupportPage(),
        );
      case walletScreen:
        return MaterialPageRoute(
          builder: (_) => const WalletPage(),
        );
      // case chooseDriverPage:
      //   return MaterialPageRoute(
      //     builder: (_) => ChooseDriverPage(),
      //   );
      case confirmRideScreen:
        return MaterialPageRoute(
          builder: (_) => const ConfirmRideBookingPage(),
        );
      case rideCompletedScreen:
        return MaterialPageRoute(
          builder: (_) => RideCompletedPage(),
        );
      case rideDetailsScreen:
        return MaterialPageRoute(
          builder: (_) => RideDetailsPage(),
        );
      case ridePaymentScreen:
        return MaterialPageRoute(
          builder: (_) => RidePaymentPage(),
        );
      case trackRideScreen:
        return MaterialPageRoute(
          builder: (_) => const TrackRidePage(),
        );
      case servicesScreen:
        return MaterialPageRoute(
          builder: (_) => ServicesPage(),
        );
      case confirmServicesScreen:
        return MaterialPageRoute(
          builder: (_) => ConfirmBookingPage(),
        );
      case otpVerificationScreen:
        return MaterialPageRoute(
          builder: (_) => OTPVerificationPage(),
        );
      case signInScreen:
        return MaterialPageRoute(
          builder: (_) => SignInPage(),
        );
      case registeruser:
        return MaterialPageRoute(
          builder: (_) => SignUpPage(),
        );

      case laundryhomescreen:
        return MaterialPageRoute(
          builder: (_) => const DashboardScreen(),
        );
      case laundrymain:
        return MaterialPageRoute(
          builder: (_) => const LaundryMain(),
        );

      // case selectLocationScreen:
      //   return MaterialPageRoute(
      //     builder: (_) => SelectLocationPage(
      //       isSavedAddress: false,
      //       title: '',
      //     ),
      //   );
      case selectZoneScreen:
        return MaterialPageRoute(
          builder: (_) => SelectZonePage(),
        );
      // case trackOrderPage:
      //   return MaterialPageRoute(
      //     builder: (_) => TrackOrder(orderType: '',),
      //   );

      // case delete:
      //   return MaterialPageRoute(
      //     builder: (_) => Abhi(),
      //   );
      //Laundry Basket Routes

      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(body: LaundryMain()),
        );
    }
  }
}
