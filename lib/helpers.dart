import 'dart:convert';

import 'package:app24_user_app/constants/api_path.dart';
import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Helpers {
  final String labelPickupLocation = "Pick up Location";
  final String labelDropLocation = "Drop Location";

  //final GlobalKey globalAppKey = GlobalKey();
  final GlobalKey<NavigatorState> globalAppKey = GlobalKey();

  Widget getTitle(title, context) {
    return Text(
      title,
      style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: MediaQuery.of(context).size.width / 20),
    );
  }

  Future<dynamic> getPostData(url, body, headers) async {
    var result;
    try {
      final response = await http.post(
        Uri.parse(App24UserAppAPI.baseUrl + url),
        headers: headers,
        body: json.encode(body),
      );

      result = response.body;
    } catch (e) {
      result = "error $e";
      print("error post data ::: $e");
    }
    return result;
  }

  Future<dynamic> getPostDataForOTP(url, body, headers) async {
    var result;
    try {
      print("url :  " + App24UserAppAPI.baseUrl + url);
      print("json.encode(body)  " + json.encode(body).toString());

      final response = await http.post(
        Uri.parse(App24UserAppAPI.baseUrl + url),
        headers: headers,
        body: body,
      );
      print("response " + response.body);

      result = response.body;
    } catch (e) {
      print("error " + e.toString());
      result = "error";
    }
    return result;
  }

  Future<String?> getData(url, headers) async {
    showLog("==========================================================");
    showLog('getData ==> Url: ${App24UserAppAPI.baseUrl + url},\n header: $headers');
    showLog("==========================================================");

    var result;
    try {
      //showLog('Url: ${baseUrl + url}, header: $header');getData
      await http
          .get(Uri.parse(App24UserAppAPI.baseUrl + url), headers: headers)
          .then((response) {
            showLog("==========================================================");
        showLog("getData ==> Url: $url");
            showLog("==========================================================");
            showLog("==========================================================");
        showLog("getData ==> Response :  ${response.body}");
            showLog("==========================================================");
        result = response.body;
      });
    } catch (e) {
      print("error HTTP getData:: ${e.toString()}");
      result = "error";
    }
    return result;
  }

  Widget getSubTitle(subTitle, context) {
    return Row(
      children: <Widget>[
        Padding(
            padding: EdgeInsets.symmetric(vertical: 5),
            child: Text(
              subTitle,
              style: TextStyle(
                  color: App24Colors.darkTextColor,
                  fontWeight: FontWeight.w700,
                  fontSize: MediaQuery.of(context).size.width / 22),
            ))
      ],
    );
  }

  InputDecoration getDecoration(context) {
    return InputDecoration(
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(8),
        borderSide: BorderSide(
          width: 0,
          style: BorderStyle.none,
        ),
      ),
      contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
      fillColor: Theme.of(context).colorScheme.secondary,
      filled: true,
    );
  }

  void getDropIcon(key, context, distance) {
    RepaintBoundary(
        key: key,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(),
                  color: Theme.of(context).colorScheme.secondary,
                ),
                child: Row(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(8),
                              bottomLeft: Radius.circular(8))),
                      padding: EdgeInsets.all(5),
                      child: Icon(
                        Icons.near_me,
                        color: Colors.white,
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Text(
                      "$distance away",
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                  ],
                )),
            const SizedBox(
              height: 5,
            ),
            Container(
              decoration: BoxDecoration(
                  color: Colors.red[500],
                  shape: BoxShape.circle,
                  border: Border.all(color: Colors.red[200]!, width: 9.0)),
              padding: EdgeInsets.all(8),
            ),
          ],
        ));
  }

  Widget getDetailsRow(label, value, context) {
    return Row(
      children: <Widget>[
        Text(
          label,
          style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: MediaQuery.of(context).size.width / 25),
        ),
        const SizedBox(
          width: 15,
        ),
        Expanded(
          child: Text(
            value,
            textAlign: TextAlign.end,
            style: TextStyle(
                fontWeight: FontWeight.w600,
                color: App24Colors.lightBlue,
                fontSize: MediaQuery.of(context).size.width / 25),
          ),
        )
      ],
    );
  }

  Widget getDetailsRowPayment(label, value, context) {
    return Row(
      children: <Widget>[
        Text(
          label,
          style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: MediaQuery.of(context).size.width / 27),
        ),
        const SizedBox(
          width: 15,
        ),
        Expanded(
          child: Text(
            value,
            textAlign: TextAlign.end,
            style: TextStyle(
                fontWeight: FontWeight.w600,
                color: App24Colors.yellowRideThemeColor,
                fontSize: MediaQuery.of(context).size.width / 27),
          ),
        )
      ],
    );
  }

  getCommonBottomSheet(
      {required BuildContext context,
      required Widget content,
      String title = '',
      bool? isDismissible = false,
      bool? enableDrag = false}) {
    return showModalBottomSheet(
        isDismissible: isDismissible == false ? false : true,
        enableDrag: enableDrag == false ? false : true,
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30), topRight: Radius.circular(30)),
        ),
        isScrollControlled: true,
        context: context,
        // backgroundColor: Colors.transparent,
        builder: (ctx) {
          return ConstrainedBox(
              constraints: BoxConstraints(maxHeight: 500),
              child: SingleChildScrollView(
                  child: Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                      child: Column(mainAxisSize: MainAxisSize.min, children: [
                        Center(
                            child: Container(
                          width: 100,
                          height: 5,
                          decoration: BoxDecoration(
                              color: Colors.grey[300],
                              borderRadius: BorderRadius.circular(10)),
                        )),
                        const SizedBox(
                          height: 10,
                        ),
                        Center(
                          child: Text(
                            title,
                            style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        content
                      ]))));
        });
  }

  List<String> foodOfferCarousel = [
    App24UserAppImages.LandingCoupon1,
    App24UserAppImages.LandingCoupon2,
    App24UserAppImages.LandingCoupon4,
    App24UserAppImages.LandingCoupon3,

    // App24UserAppImages.LandingCoupon2,
  ];

  List<Widget> getCarouselImage(BuildContext context) {
    final List<Widget> imageSliders = foodOfferCarousel
        .map((item) => Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                // border: Border.all(
                //   color: Colors.grey[200],
                // )
              ),
              margin: EdgeInsets.all(15.0),
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(20.0)),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.grey.shade300,
                          blurRadius: 5,
                          spreadRadius: 3,
                          offset: Offset(2, 2))
                    ]),
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(20.0)),
                  child: Image.asset(
                    item,
                    fit: BoxFit.fill,
                    // width: MediaQuery.of(context).size.width,
                  ),
                ),
              ),
            ))
        .toList();

    return imageSliders;
  }

  getCommonAlertDialogBox(
      {required content, required BuildContext context, barrierDismissible}) {
    showGeneralDialog(
        barrierColor: Colors.black.withOpacity(0.5),
        transitionBuilder: (context, a1, a2, widget) {
          return Transform.scale(
            scale: a1.value,
            child: Opacity(
              opacity: a1.value,
              child: content,
            ),
          );
        },
        transitionDuration: Duration(milliseconds: 200),
        barrierDismissible:
            barrierDismissible == false ? barrierDismissible : true,
        barrierLabel: '',
        context: context,
        pageBuilder: (context, animation1, animation2) {
          return Container();
        });
  }
}
