HistoryModel historyModelFromJson(str) {
  return HistoryModel.fromJson(str);
}

class HistoryModel {
  String? statusCode;
  String? title;
  String? message;
  HistoryResponseData? responseData;
  List<dynamic>? error;

  HistoryModel(
      {this.statusCode,
      this.title,
      this.message,
      this.responseData,
      this.error});

  HistoryModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    responseData = json['responseData'] != null
        ? new HistoryResponseData.fromJson(json['responseData'])
        : null;
    if (json['error'] != null) {
      error = [];
      json['error'].forEach((v) {
        error!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['title'] = this.title;
    data['message'] = this.message;
    if (this.responseData != null) {
      data['responseData'] = this.responseData!.toJson();
    }
    if (this.error != null) {
      data['error'] = this.error!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class HistoryResponseData {
  List<HistoryData>? order;
  List<HistoryData>? service;
  List<HistoryData>? ride;

  HistoryResponseData({this.order, this.service, this.ride});

  HistoryResponseData.fromJson(Map<String, dynamic> json) {
    if (json['order'] != null) {
      order = [];
      json['order'].forEach((v) {
        order!.add(new HistoryData.fromJson(v));
      });
    }
    if (json['service'] != null) {
      service = [];
      json['service'].forEach((v) {
        service!.add(new HistoryData.fromJson(v));
      });
    }
    if (json['ride'] != null) {
      ride = [];
      json['ride'].forEach((v) {
        ride!.add(new HistoryData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.order != null) {
      data['order'] = this.order!.map((v) => v.toJson()).toList();
    }
    if (this.service != null) {
      data['service'] = this.service!.map((v) => v.toJson()).toList();
    }
    if (this.ride != null) {
      data['ride'] = this.ride!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class HistoryData {
  int? id;
  String? adminService;
  String? desc;
  String? amount;
  String? dateTime;
  String? status;
  int? auto_id;
  String? booking_type;

  HistoryData(
      {this.id,
      this.adminService,
      this.desc,
      this.amount,
      this.dateTime,
      this.status,
      this.auto_id,
      this.booking_type});

  HistoryData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    adminService = json['admin_service'];
    desc = json['desc'];
    amount = json['amount'];
    dateTime = json['date_time'];
    status = json['status'];
    auto_id = json['auto_id'];
    booking_type = json['booking_type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['admin_service'] = this.adminService;
    data['desc'] = this.desc;
    data['amount'] = this.amount;
    data['date_time'] = this.dateTime;
    data['status'] = this.status;
    data['auto_id'] = this.auto_id;
    data['booking_type'] = this.booking_type;
    return data;
  }
}
