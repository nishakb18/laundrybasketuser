HomeDeliveryOrderInsertModel homeDeliveryOrderInsertModelFromJson(str) {
  return HomeDeliveryOrderInsertModel.fromJson(str);
}



class HomeDeliveryOrderInsertModel {
  String? statusCode;
  String? title;
  String? message;
  HomeDeliveryOrderInsertResponseData? responseData;
  List<dynamic>? responseNewData;
  List<dynamic>? error;

  HomeDeliveryOrderInsertModel(
      {this.statusCode,
        this.title,
        this.message,
        this.responseData,
        this.responseNewData,
        this.error});

  HomeDeliveryOrderInsertModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    responseData = json['responseData'] != null
        ? new HomeDeliveryOrderInsertResponseData.fromJson(json['responseData'])
        : null;
    if (json['responseNewData'] != null) {
      responseNewData = [];
      json['responseNewData'].forEach((v) {
        responseNewData!.add(v);
      });
    }
    if (json['error'] != null) {
      error = [];
      json['error'].forEach((v) {
        error!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['title'] = this.title;
    data['message'] = this.message;
    if (this.responseData != null) {
      data['responseData'] = this.responseData!.toJson();
    }
    if (this.responseNewData != null) {
      data['responseNewData'] =
          this.responseNewData!.map((v) => v.toJson()).toList();
    }
    if (this.error != null) {
      data['error'] = this.error!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class HomeDeliveryOrderInsertResponseData {
  dynamic id;
  String? storeOrderInvoiceId;
  String? adminService;
  String? status;
  dynamic deliveryContactNum;
  String? type;
  String? storeName;
  String? createdAt;
  String? totalPayable;
  dynamic walletAmount;
  dynamic estimationCost;
  dynamic negativeWallet;

  HomeDeliveryOrderInsertResponseData(
      {this.id,
        this.storeOrderInvoiceId,
        this.adminService,
        this.status,
        this.deliveryContactNum,
        this.type,
        this.storeName,
        this.createdAt,
        this.totalPayable,
        this.walletAmount,
        this.estimationCost,
        this.negativeWallet});

  HomeDeliveryOrderInsertResponseData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    storeOrderInvoiceId = json['store_order_invoice_id'];
    adminService = json['admin_service'];
    status = json['status'];
    deliveryContactNum = json['delivery_contact_num'];
    type = json['type'];
    storeName = json['store_name'];
    createdAt = json['created_at'];
    totalPayable = json['total_payable'];
    walletAmount = json['wallet_amount'];
    estimationCost = json['estimation_cost'];
    negativeWallet = json['negative_wallet'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['store_order_invoice_id'] = this.storeOrderInvoiceId;
    data['admin_service'] = this.adminService;
    data['status'] = this.status;
    data['delivery_contact_num'] = this.deliveryContactNum;
    data['type'] = this.type;
    data['store_name'] = this.storeName;
    data['created_at'] = this.createdAt;
    data['total_payable'] = this.totalPayable;
    data['wallet_amount'] = this.walletAmount;
    data['estimation_cost'] = this.estimationCost;
    data['negative_wallet'] = this.negativeWallet;
    return data;
  }
}








//   class HomeDeliveryOrderInsertModel {
//   String? statusCode;
//   String? title;
//   String? message;
//   HomeDeliveryOrderInsertResponseData? responseData;
//   List<dynamic>? error;
//
//   HomeDeliveryOrderInsertModel(
//       {this.statusCode,
//       this.title,
//       this.message,
//       this.responseData,
//       this.error});
//
//   HomeDeliveryOrderInsertModel.fromJson(Map<String?, dynamic> json) {
//     statusCode = json['statusCode'];
//     title = json['title'];
//     message = json['message'];
//     responseData = json['responseData'] != null
//         ? new HomeDeliveryOrderInsertResponseData.fromJson(json['responseData'])
//         : null;
//     if (json['error'] != null) {
//       error = [];
//       json['error'].forEach((v) {
//         error!.add(v);
//       });
//     }
//   }
//
//   Map<String?, dynamic> toJson() {
//     final Map<String?, dynamic> data = new Map<String?, dynamic>();
//     data['statusCode'] = this.statusCode;
//     data['title'] = this.title;
//     data['message'] = this.message;
//     if (this.responseData != null) {
//       data['responseData'] = this.responseData!.toJson();
//     }
//     if (this.error != null) {
//       data['error'] = this.error!.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }
//
// class HomeDeliveryOrderInsertResponseData {
//   int? providerFound;
//   int? id;
//   String? storeOrderInvoiceId;
//   String? adminService;
//   String? status;
//   String? deliveryContactNum;
//   String? type;
//   String? createdAt;
//   String? totalPayable;
//   int? walletAmount;
//
//   HomeDeliveryOrderInsertResponseData(
//       {this.providerFound,
//       this.id,
//       this.storeOrderInvoiceId,
//       this.adminService,
//       this.status,
//       this.deliveryContactNum,
//       this.type,
//       this.createdAt,
//       this.totalPayable,
//       this.walletAmount});
//
//   HomeDeliveryOrderInsertResponseData.fromJson(Map<String?, dynamic> json) {
//     providerFound = json['provider_found'];
//     id = json['id'];
//     storeOrderInvoiceId = json['store_order_invoice_id'];
//     adminService = json['admin_service'];
//     status = json['status'];
//     deliveryContactNum = json['delivery_contact_num'].toString();
//     type = json['type'];
//     createdAt = json['created_at'];
//     totalPayable = json['total_payable'];
//     walletAmount = json['wallet_amount'];
//   }
//
//   Map<String?, dynamic> toJson() {
//     final Map<String?, dynamic> data = new Map<String?, dynamic>();
//     data['provider_found'] = this.providerFound;
//     data['id'] = this.id;
//     data['store_order_invoice_id'] = this.storeOrderInvoiceId;
//     data['admin_service'] = this.adminService;
//     data['status'] = this.status;
//     data['delivery_contact_num'] = this.deliveryContactNum;
//     data['type'] = this.type;
//     data['created_at'] = this.createdAt;
//     data['total_payable'] = this.totalPayable;
//     data['wallet_amount'] = this.walletAmount;
//     return data;
//   }
// }
