HomeDeliveryOrderDetailModel homeDeliveryOrderDetailModelFromJson(str) {
  return HomeDeliveryOrderDetailModel.fromJson(str);
}



class HomeDeliveryOrderDetailModel {
  dynamic statusCode;
  dynamic title;
  dynamic message;
  List<HomeDeliveryOrderDetailResponseData>? responseData;
  List<dynamic>? responseNewData;
  List<dynamic>? error;

  HomeDeliveryOrderDetailModel(
      {this.statusCode,
        this.title,
        this.message,
        this.responseData,
        this.responseNewData,
        this.error});

  HomeDeliveryOrderDetailModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    if (json['responseData'] != null) {
      responseData = [];
      json['responseData'].forEach((v) {
        responseData!.add(new HomeDeliveryOrderDetailResponseData.fromJson(v));
      });
    }
    if (json['responseNewData'] != null) {
      responseNewData = [];
      json['responseNewData'].forEach((v) {
        responseNewData!.add(v);
      });
    }
    if (json['error'] != null) {
      error = [];
      json['error'].forEach((v) {
        error!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['title'] = this.title;
    data['message'] = this.message;
    if (this.responseData != null) {
      data['responseData'] = this.responseData!.map((v) => v.toJson()).toList();
    }
    if (this.responseNewData != null) {
      data['responseNewData'] =
          this.responseNewData!.map((v) => v.toJson()).toList();
    }
    if (this.error != null) {
      data['error'] = this.error!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class HomeDeliveryOrderDetailResponseData {
  int? id;
  dynamic bookingId;
  int? weight;
  dynamic deliveryMode;
  dynamic deliveryCharge;
  dynamic deliveryDiscount;
  dynamic taxAmount;
  dynamic currencySymbol;
  dynamic totalPayable;
  dynamic storeName;
  dynamic serviceCharge;
  dynamic storeLocation;
  dynamic estimationCost;
  dynamic negativeWallet;
  dynamic discount;
  dynamic itemtotal;
  // dynamic walletAmount;
  int? paid;
  dynamic status;
  dynamic providerName;
  dynamic providerId;
  dynamic providerNumber;
  double? pickUpLat;
  double? pickUpLng;
  double? deliveryLat;
  double? deliveryLng;
  dynamic shopGst;
  List<dynamic>? packageDetail;
  OrderDetails? orderDetails;
  List<HomeDelCatDetails>? homeDelCatDetails;
  dynamic toPayAmount;


  HomeDeliveryOrderDetailResponseData(
      {this.id,
        this.bookingId,
        this.weight,
        this.deliveryMode,
        this.deliveryCharge,
        this.deliveryDiscount,
        this.taxAmount,
        this.currencySymbol,
        this.totalPayable,
        this.storeName,
        this.serviceCharge,
        this.storeLocation,
        this.estimationCost,
        this.negativeWallet,
        this.discount,
        this.itemtotal,
        // this.walletAmount,
        this.paid,
        this.status,
        this.providerName,
        this.providerId,
        this.providerNumber,
        this.pickUpLat,
        this.pickUpLng,
        this.deliveryLat,
        this.deliveryLng,
        this.shopGst,
        this.packageDetail,
        this.orderDetails,
        this.homeDelCatDetails,
        this.toPayAmount});

  HomeDeliveryOrderDetailResponseData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    bookingId = json['booking_id'];
    weight = json['weight'];
    deliveryMode = json['delivery_mode'];
    deliveryCharge = json['delivery_charge'];
    deliveryDiscount = json['delivery_discount'];
    taxAmount = json['tax_amount'];
    currencySymbol = json['currency_symbol'];
    totalPayable = json['total_payable'];
    storeName = json['store_name'];
    serviceCharge = json['service_charge'];
    storeLocation = json['store_location'];
    estimationCost = json['estimation_cost'];
    negativeWallet = json['negative_wallet'];
    discount = json['discount'];
    itemtotal = json['itemtotal'];
    // walletAmount = json['wallet_amount'];
    paid = json['paid'];
    status = json['status'];
    providerName = json['providerName'];
    providerId = json['provider_id'];
    providerNumber = json['providerNumber'];
    pickUpLat = json['pickup_lat'];
    pickUpLng = json['pickup_lng'];
    deliveryLat = json['delivery_lat'];
    deliveryLng = json['delivery_lng'];
    shopGst = json['shop_gst_amount'];
    if (json['packageDetail'] != null) {
      packageDetail = [];
      json['packageDetail'].forEach((v) {
        packageDetail!.add(v);
      });
    }
    orderDetails = json['orderDetails'] != null
        ? new OrderDetails.fromJson(json['orderDetails'])
        : null;
    if (json['home_del_cat_details'] != null) {
      homeDelCatDetails = [];
      json['home_del_cat_details'].forEach((v) {
        homeDelCatDetails!.add(new HomeDelCatDetails.fromJson(v));
      });
    }
    toPayAmount = json['toPayAmount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['booking_id'] = this.bookingId;
    data['weight'] = this.weight;
    data['delivery_mode'] = this.deliveryMode;
    data['delivery_charge'] = this.deliveryCharge;
    data['delivery_discount'] = this.deliveryDiscount;
    data['tax_amount'] = this.taxAmount;
    data['currency_symbol'] = this.currencySymbol;
    data['total_payable'] = this.totalPayable;
    data['store_name'] = this.storeName;
    data['service_charge'] = this.serviceCharge;
    data['store_location'] = this.storeLocation;
    data['estimation_cost'] = this.estimationCost;
    data['negative_wallet'] = this.negativeWallet;
    data['discount'] = this.discount;
    data['itemtotal'] = this.itemtotal;
    // data['wallet_amount'] = this.itemtotal;
    data['paid'] = this.paid;
    data['status'] = this.status;
    data['provider_id'] = this.providerId;
    data['providerNumber'] = this.providerNumber;
    data['pickUpLat'] = this.pickUpLat;
    data['pickUpLng'] = this.pickUpLng;
    data['deliveryLat'] = this.deliveryLat;
    data['deliveryLng'] = this.deliveryLng;
    data['shop_gst_amount'] = this.shopGst;
    if (this.packageDetail != null) {
      data['packageDetail'] =
          this.packageDetail!.map((v) => v.toJson()).toList();
    }
    if (this.orderDetails != null) {
      data['orderDetails'] = this.orderDetails!.toJson();
    }
    if (this.homeDelCatDetails != null) {
      data['home_del_cat_details'] =
          this.homeDelCatDetails!.map((v) => v.toJson()).toList();
    }
    data['toPayAmount'] = this.toPayAmount;
    return data;
  }
}

class OrderDetails {
  List<AStatus>? aStatus;

  OrderDetails({this.aStatus});

  OrderDetails.fromJson(Map<String, dynamic> json) {
    if (json['aStatus'] != null) {
      aStatus = [];
      json['aStatus'].forEach((v) {
        aStatus!.add(new AStatus.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.aStatus != null) {
      data['aStatus'] = this.aStatus!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class AStatus {
  dynamic statusText;
  dynamic status;

  AStatus({this.statusText, this.status});

  AStatus.fromJson(Map<String, dynamic> json) {
    statusText = json['status_text'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status_text'] = this.statusText;
    data['status'] = this.status;
    return data;
  }
}

class HomeDelCatDetails {
  int? itemCount;
  dynamic type;
  dynamic catValue;
  dynamic billImage;

  HomeDelCatDetails({this.itemCount, this.type, this.catValue});

  HomeDelCatDetails.fromJson(Map<String, dynamic> json) {
    itemCount = json['item_count'];
    type = json['type'];
    billImage = json['bill_image'];
    catValue = json['cat_value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['item_count'] = this.itemCount;
    data['type'] = this.type;
    data['bill_image'] = this.billImage;
    data['cat_value'] = this.catValue;
    return data;
  }
}








