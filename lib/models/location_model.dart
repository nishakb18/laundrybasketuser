
import 'package:flutter/material.dart';

LocationModel locationModelFromDBJson(str) {
  return LocationModel.fromDBJson(str);
}

LocationModel locationModelFromJson(str) {
  return LocationModel.fromJson(str);
}

class LocationModel {
  String? description;
  String? main_text;
  String? secondary_text;
  dynamic latitude;
  dynamic longitude;
  String? placeID;
  Color? pinColor;
  int? type;
  String? house_name;
  String? street;
  String? landmark;
  String? label;
  int? priority;
  String? location_id;


  LocationModel(
      {this.description,
      this.main_text,
      this.secondary_text,
      this.longitude,
      this.latitude,
      this.placeID,
      this.pinColor,
      this.type,
      this.house_name,
      this.label,
      this.landmark,
      this.location_id,
      this.priority,
      this.street,
     });

  factory LocationModel.fromJson(Map<String, dynamic> map) {
    print("map values google: "+map.toString());
    return LocationModel(
        main_text: map['structured_formatting']['main_text'],
        secondary_text: map['structured_formatting']['secondary_text'],
        description: map['description'],
        placeID: map['place_id'],
        pinColor: Colors.red);
  }

  factory LocationModel.fromDBJson(Map<String, dynamic> map) {
    print("map values server: "+map.toString());
    return LocationModel(
        main_text: map['mPrimary'],
        secondary_text: map['mSecondary'],
        description: map['mFullAddress'],
        latitude: double.parse(map['lat']),
        longitude: double.parse(map['lng']),
        placeID: map['mPlaceId'],
        pinColor: Colors.orange);
  }

  factory LocationModel.fromLocalDBJson(Map<String, dynamic> map) {
    print("map values db: "+map.toString());
    return LocationModel(
        main_text: map['title'],
        secondary_text: map['sub_title'],
        description: map['description'],
        latitude: map['latitude'],
        longitude: map['longitude'],
        placeID: map['place_id'],
        location_id: map['location_id'],
        type: map['type'],
        pinColor: Colors.green,
        priority: map['priority']);
  }

  Map<String, dynamic> toMap() {
    return {
      'description': description,
      'location_id': location_id,
      'place_id': placeID,
      'title': main_text,
      'sub_title': secondary_text,
      'latitude': latitude,
      'longitude': longitude,
      'type': type,
      'label': label,
      'landmark': landmark,
      'street': street,
      'house_name': house_name,
      'priority': priority,

    };
  }

  static List<LocationModel> parseLocationList(Map<String, dynamic> map) {
    Iterable list = map['predictions'];
    debugPrint(list.toString());
    return list.map((res) => LocationModel.fromJson(res)).toList();
  }

  static List<LocationModel> parseLocationListFromDB(Map<String, dynamic> map) {
    Iterable list = map['responseData'];
    debugPrint(list.toString());
    return list.map((res) => LocationModel.fromDBJson(res)).toList();
  }
}
