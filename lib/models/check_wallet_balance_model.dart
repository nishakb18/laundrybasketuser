CheckWalletBalanceModel checkWalletBalanceModelFromJson(str) {
  return CheckWalletBalanceModel.fromJson(str);
}

class CheckWalletBalanceModel {
  String? statusCode;
  String? title;
  String? message;
  WalletBalanceResponseData? responseData;
  List<dynamic>? responseNewData;
  List<dynamic>? error;

  CheckWalletBalanceModel(
      {this.statusCode,
      this.title,
      this.message,
      this.responseData,
      this.responseNewData,
      this.error});

  CheckWalletBalanceModel.fromJson(Map<String?, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    responseData = json['responseData'] != null
        ? new WalletBalanceResponseData.fromJson(json['responseData'])
        : null;
    if (json['responseNewData'] != null) {
      responseNewData = [];
      json['responseNewData'].forEach((v) {
        responseNewData!.add(v);
      });
    }
    if (json['error'] != null) {
      error = [];
      json['error'].forEach((v) {
        error!.add(v);
      });
    }
  }

  Map<String?, dynamic> toJson() {
    final Map<String?, dynamic> data = new Map<String?, dynamic>();
    data['statusCode'] = this.statusCode;
    data['title'] = this.title;
    data['message'] = this.message;
    if (this.responseData != null) {
      data['responseData'] = this.responseData!.toJson();
    }
    if (this.responseNewData != null) {
      data['responseNewData'] =
          this.responseNewData!.map((v) => v.toJson()).toList();
    }
    if (this.error != null) {
      data['error'] = this.error!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class WalletBalanceResponseData {
  String? amount;
  String? status;
  dynamic payable;
  dynamic totalWalletBalance;
  dynamic totalWalletBalanceLeft;

  WalletBalanceResponseData(
      {this.amount,
        this.status,
      this.payable,
      this.totalWalletBalance,
      this.totalWalletBalanceLeft});

  WalletBalanceResponseData.fromJson(Map<String?, dynamic> json) {
    amount = json['amount'];
    status = json['status'];
    payable = json['payable'];
    totalWalletBalance = json['total_wallet_balance'];
    totalWalletBalanceLeft = json['total_wallet_balance_left'];
  }

  Map<String?, dynamic> toJson() {
    final Map<String?, dynamic> data = new Map<String?, dynamic>();
    data['amount'] = this.amount;
    data['status'] = this.status;
    data['payable'] = this.payable;
    data['total_wallet_balance'] = this.totalWalletBalance;
    data['total_wallet_balance_left'] = this.totalWalletBalanceLeft;
    return data;
  }
}
