class DeliveryMenu {
  int? id;
  String? bgColor;
  String? icon;
  int? isFeatured;
  int? comingSoon;
  String? featuredImage;
  String? title;
  int? shopType;
  int? shopTypeId;
  String? adminService;
  int? menuTypeId;
  int? sortOrder;
  int? status;

  DeliveryMenu(
      {this.id,
      this.bgColor,
      this.icon,
      this.isFeatured,
      this.comingSoon,
      this.featuredImage,
      this.title,
      this.shopType,
      this.shopTypeId,
      this.adminService,
      this.menuTypeId,
      this.sortOrder,
      this.status});

  DeliveryMenu.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    bgColor = json['bg_color'];
    icon = json['icon'];
    isFeatured = json['is_featured'];
    comingSoon = json['coming_soon'];
    featuredImage = json['featured_image'];
    title = json['title'];
    shopType = json['shop_type'];
    shopTypeId = json['shop_type_id'];
    adminService = json['admin_service'];
    menuTypeId = json['menu_type_id'];
    sortOrder = json['sort_order'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['bg_color'] = this.bgColor;
    data['icon'] = this.icon;
    data['is_featured'] = this.isFeatured;
    data['coming_soon'] = this.comingSoon;
    data['featured_image'] = this.featuredImage;
    data['title'] = this.title;
    data['shop_type'] = this.shopType;
    data['shop_type_id'] = this.shopTypeId;
    data['admin_service'] = this.adminService;
    data['menu_type_id'] = this.menuTypeId;
    data['sort_order'] = this.sortOrder;
    data['status'] = this.status;
    return data;
  }
}
