class DBLocationModel {
  String? description;
  String? location_id;
  String? place_id;
  String? title;
  String? sub_title;
  double? latitude;
  double? longitude;
  int? type;
  String? house_name;
  String? street;
  String? landmark;
  String? label;
  int? priority;

  DBLocationModel(
      {this.description,
      this.location_id,
      this.place_id,
      this.title,
      this.sub_title,
      this.longitude,
      this.latitude,
      this.type,
      this.label,
      this.landmark,
      this.street,
      this.house_name,
      this.priority});

  factory DBLocationModel.fromJson(Map<String, dynamic> map) {
    return DBLocationModel(
        description: map['description'],
        location_id: map['location_id'],
        place_id: map['place_id'],
        title: map['title'],
        sub_title: map['sub_title'],
        latitude: map['latitude'],
        longitude: map['longitude'],
        type: map['type'],
        label: map['label'],
        landmark: map['landmark'],
        street: map['street'],
        house_name: map['house_name'],
        priority: map['priority']);
  }

  Map<String, dynamic> toMap() {
    return {
      'description': description,
      'location_id': location_id,
      'place_id': place_id,
      'title': title,
      'sub_title': sub_title,
      'latitude': latitude,
      'longitude': longitude,
      'type': type,
      'label': label,
      'landmark': landmark,
      'street': street,
      'house_name': house_name,
      'priority': priority
    };
  }
}
