AppConfigurationModel appConfigurationModelFromJson(str) {
  return AppConfigurationModel.fromJson(str);
}

class AppConfigurationModel {
  String? statusCode;
  String? title;
  String? message;
  ResponseData? responseData;
  List<dynamic>? responseNewData;
  List<dynamic>? error;

  AppConfigurationModel(
      {this.statusCode,
        this.title,
        this.message,
        this.responseData,
        this.responseNewData,
        this.error});

  AppConfigurationModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    responseData = json['responseData'] != null
        ? new ResponseData.fromJson(json['responseData'])
        : null;
    if (json['responseNewData'] != null) {
      responseNewData = [];
      json['responseNewData'].forEach((v) {
        responseNewData!.add(v);
      });
    }
    if (json['error'] != null) {
      error = [];
      json['error'].forEach((v) {
        error!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['title'] = this.title;
    data['message'] = this.message;
    if (this.responseData != null) {
      data['responseData'] = this.responseData!.toJson();
    }
    if (this.responseNewData != null) {
      data['responseNewData'] =
          this.responseNewData!.map((v) => v.toJson()).toList();
    }
    if (this.error != null) {
      data['error'] = this.error!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ResponseData {
  String? baseUrl;
  List<Services>? services;
  Appsetting? appsetting;

  ResponseData({this.baseUrl, this.services, this.appsetting});

  ResponseData.fromJson(Map<String, dynamic> json) {
    baseUrl = json['base_url'];
    if (json['services'] != null) {
      services = [];
      json['services'].forEach((v) {
        services!.add(new Services.fromJson(v));
      });
    }
    appsetting = json['appsetting'] != null
        ? new Appsetting.fromJson(json['appsetting'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['base_url'] = this.baseUrl;
    if (this.services != null) {
      data['services'] = this.services!.map((v) => v.toJson()).toList();
    }
    if (this.appsetting != null) {
      data['appsetting'] = this.appsetting!.toJson();
    }
    return data;
  }
}

class Services {
  int? id;
  String? adminService;
  String? displayName;
  String? baseUrl;
  int? status;

  Services(
      {this.id,
        this.adminService,
        this.displayName,
        this.baseUrl,
        this.status});

  Services.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    adminService = json['admin_service'];
    displayName = json['display_name'];
    baseUrl = json['base_url'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['admin_service'] = this.adminService;
    data['display_name'] = this.displayName;
    data['base_url'] = this.baseUrl;
    data['status'] = this.status;
    return data;
  }
}

class Appsetting {
  int? demoMode;
  String? providerNegativeBalance;
  String? androidKey;
  String? iosKey;
  int? referral;
  int? socialLogin;
  int? sendSms;
  int? sendEmail;
  int? otpVerify;
  int? rideOtp;
  int? orderOtp;
  int? serviceOtp;
  List<Payments>? payments;
  Cmspage? cmspage;
  Supportdetails? supportdetails;
  List<Languages>? languages;
  String? stortypeLast;
  String? servsubcatLast;
  String? ridedelvehLast;

  Appsetting(
      {this.demoMode,
        this.providerNegativeBalance,
        this.androidKey,
        this.iosKey,
        this.referral,
        this.socialLogin,
        this.sendSms,
        this.sendEmail,
        this.otpVerify,
        this.rideOtp,
        this.orderOtp,
        this.serviceOtp,
        this.payments,
        this.cmspage,
        this.supportdetails,
        this.languages,
        this.stortypeLast,
        this.servsubcatLast,
        this.ridedelvehLast});

  Appsetting.fromJson(Map<String, dynamic> json) {
    demoMode = json['demo_mode'];
    providerNegativeBalance = json['provider_negative_balance'];
    androidKey = json['android_key'];
    iosKey = json['ios_key'];
    referral = json['referral'];
    socialLogin = json['social_login'];
    sendSms = json['send_sms'];
    sendEmail = json['send_email'];
    otpVerify = json['otp_verify'];
    rideOtp = json['ride_otp'];
    orderOtp = json['order_otp'];
    serviceOtp = json['service_otp'];
    if (json['payments'] != null) {
      payments = [];
      json['payments'].forEach((v) {
        payments!.add(new Payments.fromJson(v));
      });
    }
    cmspage =
    json['cmspage'] != null ? new Cmspage.fromJson(json['cmspage']) : null;
    supportdetails = json['supportdetails'] != null
        ? new Supportdetails.fromJson(json['supportdetails'])
        : null;
    if (json['languages'] != null) {
      languages = [];
      json['languages'].forEach((v) {
        languages!.add(new Languages.fromJson(v));
      });
    }
    stortypeLast = json['stortype_last'];
    servsubcatLast = json['servsubcat_last'];
    ridedelvehLast = json['ridedelveh_last'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['demo_mode'] = this.demoMode;
    data['provider_negative_balance'] = this.providerNegativeBalance;
    data['android_key'] = this.androidKey;
    data['ios_key'] = this.iosKey;
    data['referral'] = this.referral;
    data['social_login'] = this.socialLogin;
    data['send_sms'] = this.sendSms;
    data['send_email'] = this.sendEmail;
    data['otp_verify'] = this.otpVerify;
    data['ride_otp'] = this.rideOtp;
    data['order_otp'] = this.orderOtp;
    data['service_otp'] = this.serviceOtp;
    if (this.payments != null) {
      data['payments'] = this.payments!.map((v) => v.toJson()).toList();
    }
    if (this.cmspage != null) {
      data['cmspage'] = this.cmspage!.toJson();
    }
    if (this.supportdetails != null) {
      data['supportdetails'] = this.supportdetails!.toJson();
    }
    if (this.languages != null) {
      data['languages'] = this.languages!.map((v) => v.toJson()).toList();
    }
    data['stortype_last'] = this.stortypeLast;
    data['servsubcat_last'] = this.servsubcatLast;
    data['ridedelveh_last'] = this.ridedelvehLast;
    return data;
  }
}

class Payments {
  String? name;
  String? status;
  List<Credentials>? credentials;

  Payments({this.name, this.status, this.credentials});

  Payments.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    status = json['status'];
    if (json['credentials'] != null) {
      credentials = [];
      json['credentials'].forEach((v) {
        credentials!.add(new Credentials.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['status'] = this.status;
    if (this.credentials != null) {
      data['credentials'] = this.credentials!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Credentials {
  String? name;
  String? value;

  Credentials({this.name, this.value});

  Credentials.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    return data;
  }
}

class Cmspage {
  String? privacypolicy;
  String? help;
  String? terms;
  String? cancel;

  Cmspage({this.privacypolicy, this.help, this.terms, this.cancel});

  Cmspage.fromJson(Map<String, dynamic> json) {
    privacypolicy = json['privacypolicy'];
    help = json['help'];
    terms = json['terms'];
    cancel = json['cancel'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['privacypolicy'] = this.privacypolicy;
    data['help'] = this.help;
    data['terms'] = this.terms;
    data['cancel'] = this.cancel;
    return data;
  }
}

class Supportdetails {
  List<ContactNumber>? contactNumber;
  String? contactEmail;

  Supportdetails({this.contactNumber, this.contactEmail});

  Supportdetails.fromJson(Map<String, dynamic> json) {
    if (json['contact_number'] != null) {
      contactNumber = [];
      json['contact_number'].forEach((v) {
        contactNumber!.add(new ContactNumber.fromJson(v));
      });
    }
    contactEmail = json['contact_email'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.contactNumber != null) {
      data['contact_number'] =
          this.contactNumber!.map((v) => v.toJson()).toList();
    }
    data['contact_email'] = this.contactEmail;
    return data;
  }
}

class ContactNumber {
  String? number;

  ContactNumber({this.number});

  ContactNumber.fromJson(Map<String, dynamic> json) {
    number = json['number'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['number'] = this.number;
    return data;
  }
}

class Languages {
  String? name;
  String? key;

  Languages({this.name, this.key});

  Languages.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    key = json['key'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['key'] = this.key;
    return data;
  }
}







// class AppConfigurationModel {
//   String? statusCode;
//   String? title;
//   String? message;
//   ResponseData? responseData;
//   List<dynamic>? error;
//
//   AppConfigurationModel(
//       {this.statusCode,
//       this.title,
//       this.message,
//       this.responseData,
//       this.error});
//
//   AppConfigurationModel.fromJson(Map<String, dynamic> json) {
//     statusCode = json['statusCode'];
//     title = json['title'];
//     message = json['message'];
//     responseData = json['responseData'] != null
//         ? new ResponseData.fromJson(json['responseData'])
//         : null;
//     if (json['error'] != null) {
//       error = [];
//       json['error'].forEach((v) {
//         error!.add(v);
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['statusCode'] = this.statusCode;
//     data['title'] = this.title;
//     data['message'] = this.message;
//     if (this.responseData != null) {
//       data['responseData'] = this.responseData!.toJson();
//     }
//     if (this.error != null) {
//       data['error'] = this.error!.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }
//
// class ResponseData {
//   String? baseUrl;
//   List<Services>? services;
//   Appsetting? appsetting;
//
//   ResponseData({this.baseUrl, this.services, this.appsetting});
//
//   ResponseData.fromJson(Map<String, dynamic> json) {
//     baseUrl = json['base_url'];
//     if (json['services'] != null) {
//       services = [];
//       json['services'].forEach((v) {
//         services!.add(new Services.fromJson(v));
//       });
//     }
//     appsetting = json['appsetting'] != null
//         ? new Appsetting.fromJson(json['appsetting'])
//         : null;
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['base_url'] = this.baseUrl;
//     if (this.services != null) {
//       data['services'] = this.services!.map((v) => v.toJson()).toList();
//     }
//     if (this.appsetting != null) {
//       data['appsetting'] = this.appsetting!.toJson();
//     }
//     return data;
//   }
// }
//
// class Services {
//   int? id;
//   String? adminService;
//   String? displayName;
//   String? baseUrl;
//   int? status;
//
//   Services(
//       {this.id,
//       this.adminService,
//       this.displayName,
//       this.baseUrl,
//       this.status});
//
//   Services.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     adminService = json['admin_service'];
//     displayName = json['display_name'];
//     baseUrl = json['base_url'];
//     status = json['status'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['admin_service'] = this.adminService;
//     data['display_name'] = this.displayName;
//     data['base_url'] = this.baseUrl;
//     data['status'] = this.status;
//     return data;
//   }
// }
//
// class Appsetting {
//   int? demoMode;
//   String? providerNegativeBalance;
//   String? androidKey;
//   String? iosKey;
//   int? referral;
//   int? socialLogin;
//   int? sendSms;
//   int? sendEmail;
//   int? otpVerify;
//   int? rideOtp;
//   int? orderOtp;
//   int? serviceOtp;
//   List<Payments>? payments;
//   Cmspage? cmspage;
//   Supportdetails? supportdetails;
//   List<Languages>? languages;
//
//   Appsetting(
//       {this.demoMode,
//       this.providerNegativeBalance,
//       this.androidKey,
//       this.iosKey,
//       this.referral,
//       this.socialLogin,
//       this.sendSms,
//       this.sendEmail,
//       this.otpVerify,
//       this.rideOtp,
//       this.orderOtp,
//       this.serviceOtp,
//       this.payments,
//       this.cmspage,
//       this.supportdetails,
//       this.languages});
//
//   Appsetting.fromJson(Map<String, dynamic> json) {
//     demoMode = json['demo_mode'];
//     providerNegativeBalance = json['provider_negative_balance'];
//     androidKey = json['android_key'];
//     iosKey = json['ios_key'];
//     referral = json['referral'];
//     socialLogin = json['social_login'];
//     sendSms = json['send_sms'];
//     sendEmail = json['send_email'];
//     otpVerify = json['otp_verify'];
//     rideOtp = json['ride_otp'];
//     orderOtp = json['order_otp'];
//     serviceOtp = json['service_otp'];
//     if (json['payments'] != null) {
//       payments = [];
//       json['payments'].forEach((v) {
//         payments!.add(new Payments.fromJson(v));
//       });
//     }
//     cmspage =
//         json['cmspage'] != null ? new Cmspage.fromJson(json['cmspage']) : null;
//     supportdetails = json['supportdetails'] != null
//         ? new Supportdetails.fromJson(json['supportdetails'])
//         : null;
//     if (json['languages'] != null) {
//       languages = [];
//       json['languages'].forEach((v) {
//         languages!.add(new Languages.fromJson(v));
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['demo_mode'] = this.demoMode;
//     data['provider_negative_balance'] = this.providerNegativeBalance;
//     data['android_key'] = this.androidKey;
//     data['ios_key'] = this.iosKey;
//     data['referral'] = this.referral;
//     data['social_login'] = this.socialLogin;
//     data['send_sms'] = this.sendSms;
//     data['send_email'] = this.sendEmail;
//     data['otp_verify'] = this.otpVerify;
//     data['ride_otp'] = this.rideOtp;
//     data['order_otp'] = this.orderOtp;
//     data['service_otp'] = this.serviceOtp;
//     if (this.payments != null) {
//       data['payments'] = this.payments!.map((v) => v.toJson()).toList();
//     }
//     if (this.cmspage != null) {
//       data['cmspage'] = this.cmspage!.toJson();
//     }
//     if (this.supportdetails != null) {
//       data['supportdetails'] = this.supportdetails!.toJson();
//     }
//     if (this.languages != null) {
//       data['languages'] = this.languages!.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }
//
// class Payments {
//   String? name;
//   String? status;
//   List<Credentials>? credentials;
//
//   Payments({this.name, this.status, this.credentials});
//
//   Payments.fromJson(Map<String, dynamic> json) {
//     name = json['name'];
//     status = json['status'];
//     if (json['credentials'] != null) {
//       credentials = [];
//       json['credentials'].forEach((v) {
//         credentials!.add(new Credentials.fromJson(v));
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['name'] = this.name;
//     data['status'] = this.status;
//     if (this.credentials != null) {
//       data['credentials'] = this.credentials!.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }
//
// class Credentials {
//   String? name;
//   String? value;
//
//   Credentials({this.name, this.value});
//
//   Credentials.fromJson(Map<String, dynamic> json) {
//     name = json['name'];
//     value = json['value'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['name'] = this.name;
//     data['value'] = this.value;
//     return data;
//   }
// }
//
// class Cmspage {
//   String? privacypolicy;
//   String? help;
//   String? terms;
//   String? cancel;
//
//   Cmspage({this.privacypolicy, this.help, this.terms, this.cancel});
//
//   Cmspage.fromJson(Map<String, dynamic> json) {
//     privacypolicy = json['privacypolicy'];
//     help = json['help'];
//     terms = json['terms'];
//     cancel = json['cancel'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['privacypolicy'] = this.privacypolicy;
//     data['help'] = this.help;
//     data['terms'] = this.terms;
//     data['cancel'] = this.cancel;
//     return data;
//   }
// }
//
// class Supportdetails {
//   List<ContactNumber>? contactNumber;
//   String? contactEmail;
//
//   Supportdetails({this.contactNumber, this.contactEmail});
//
//   Supportdetails.fromJson(Map<String, dynamic> json) {
//     if (json['contact_number'] != null) {
//       contactNumber = [];
//       json['contact_number'].forEach((v) {
//         contactNumber!.add(new ContactNumber.fromJson(v));
//       });
//     }
//     contactEmail = json['contact_email'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     if (this.contactNumber != null) {
//       data['contact_number'] =
//           this.contactNumber!.map((v) => v.toJson()).toList();
//     }
//     data['contact_email'] = this.contactEmail;
//     return data;
//   }
// }
//
// class ContactNumber {
//   String? number;
//
//   ContactNumber({this.number});
//
//   ContactNumber.fromJson(Map<String, dynamic> json) {
//     number = json['number'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['number'] = this.number;
//     return data;
//   }
// }
//
// class Languages {
//   String? name;
//   String? key;
//
//   Languages({this.name, this.key});
//
//   Languages.fromJson(Map<String, dynamic> json) {
//     name = json['name'];
//     key = json['key'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['name'] = this.name;
//     data['key'] = this.key;
//     return data;
//   }
// }
