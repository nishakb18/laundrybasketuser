import 'dart:async';

import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class OrderEssentialSeeAllLoading extends StatefulWidget {
  @override
  _OrderEssentialSeeAllLoadingState createState() =>
      _OrderEssentialSeeAllLoadingState();
}

class _OrderEssentialSeeAllLoadingState
    extends State<OrderEssentialSeeAllLoading> {
  Timer? _timer;
  int _start = 10; // time to popup still working window /// in seconds

  @override
  void initState() {
    startTimer();
    super.initState();
  }

  @override
  void dispose() {
    if (_timer != null) {
      _timer!.cancel();
    }
    super.dispose();
  }

  void startTimer() {
    if (_timer != null) {
      _timer!.cancel();
      _timer = null;
    } else {
      _timer = new Timer.periodic(const Duration(seconds: 1), (Timer timer) {
        setState(() {
          if (_start < 1) {
            _timer!.cancel();
          } else {
            _start = _start - 1;
          }
        });
      });
    }
  }

  buildCuisineSeeAllShimmerItem(context) {
    List<Widget> list = [];

    for (var i = 0; i < 12; i++)
      list.add(Container(
        width: MediaQuery.of(context).size.width / 7,
        margin: EdgeInsets.symmetric(vertical: 15, horizontal: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: MediaQuery.of(context).size.width / 6,
              // MediaQuery.of(context).size.width / 2 - 20,
              height: MediaQuery.of(context).size.height / 13,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xffefefef),
                      blurRadius: 20,
                      spreadRadius: 3,
                    ),
                  ]),
            ),
            Container(
              height: 8,
              margin: EdgeInsets.only(top: 10),
              width: MediaQuery.of(context).size.width / 6,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xffefefef),
                      blurRadius: 20,
                      spreadRadius: 3,
                    ),
                  ]),
            ),
          ],
        ),
      ));

    return list;
  }

  buildStillLoadingWidget() {
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
        // boxShadow: [BoxShadow(
        //   color: Colors.grey.shade200,
        //   blurRadius: 5,
        //   spreadRadius: 5
        // )]
      ),
      width: MediaQuery.of(context).size.width,
      child: ListTile(
        title: Text("Please wait..."),
        subtitle: Text(
            "The network seems to be too busy. We suggest you to wait for some more time."),
        leading: CircularProgressIndicator.adaptive(),
      ),
    );
    return Row(
      children: [
        SizedBox(
          width: 16,
          child: CircularProgressIndicator.adaptive(),
          height: 16,
        ),
        const SizedBox(
          width: 10,
        ),
        Expanded(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              "Please wait...",
              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 12),
            ),
            Text(
              "The network seems to be too busy. We suggest you to wait for some more time.",
              style: TextStyle(fontWeight: FontWeight.w400, fontSize: 10),
            ),
          ],
        ))
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        // height: MediaQuery.of(context).size.width/1,
        child: Stack(
      fit: StackFit.expand,
      children: [
        Shimmer.fromColors(
            baseColor: Colors.grey[300]!,
            highlightColor: Colors.grey[100]!,
            child: Wrap(
                alignment: WrapAlignment.start,
                runAlignment: WrapAlignment.start,
                spacing: 0,
                runSpacing: 10,
                // padding:
                // const EdgeInsets.only(top: 10,left: 10,right: 10),
                // scrollDirection: Axis.vertical,
                children: buildCuisineSeeAllShimmerItem(context))),
        Positioned(
          left: 20,
          right: 20,
          top: 100,
          child: _start == 0 ? buildStillLoadingWidget() : Container(),
        )
      ],
    ));
  }
}
