import 'dart:convert';
import 'package:app24_user_app/constants/api_path.dart';
import 'package:app24_user_app/constants/shared_preferences_path.dart';
import 'package:app24_user_app/models/history_model.dart';
import 'package:app24_user_app/models/home_delivery_order_insert_model.dart';
import 'package:app24_user_app/modules/delivery/shop_list/shop_list_model_one.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/api/api_response_state2.dart';
import 'package:app24_user_app/utils/services/database/database_service.dart';
import 'package:app24_user_app/utils/services/database/history_database.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../helpers.dart';

class OrderEssentialNotifier extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  OrderEssentialNotifier(this._apiRepository) : super(ResponseState2(isLoading: true));

  HistoryDatabase historyDatabase = HistoryDatabase(DatabaseService.instance);

  Future<void> getOrder({
    bool init = true,
  }) async {
    try {
      if (init) state = state.copyWith(isLoading: true);
      final order = await _apiRepository.fetchOrder();
      state = state.copyWith(response: order, isLoading: false, isError: false);
    } catch (e) {
      state = state.copyWith(
          errorMessage: e.toString(), isLoading: false, isError: true);
    }
  }

  Future<void> getLaundry({
    bool init = true,
    double? lat,
    double? long,
  }) async {
    try {
      if (init) state = state.copyWith(isLoading: true);
      ShopNamesModelOne order = await _apiRepository.fetchLaundry(
        lat: lat,
        long: long
      );
      state = state.copyWith(response: order, isLoading: false, isError: false);
    } catch (e) {
      state = state.copyWith(
          errorMessage: e.toString(), isLoading: false, isError: true);
    }
  }

  Future<void> buyItemSave(
      {bool init = true,
      required FormData body,
      required BuildContext context,
      bool? fromUpdateAddress}) async {
    try {
      if (init) state = state.copyWith(isLoading: true);

      if (context.read(homeNotifierProvider.notifier).profile == null) {
        await context.read(homeNotifierProvider.notifier).getProfile();
      }

      Map cartInsertBody = {
        "to_lat": fromUpdateAddress == true
            ? body.fields[0].value
            : body.fields[6].value,
        "to_lng": fromUpdateAddress == true
            ? body.fields[1].value
            : body.fields[7].value,
        "to_adrs": fromUpdateAddress == true
            ? body.fields[2].value
            : body.fields[8].value,
        "to_contact_num": context
            .read(homeNotifierProvider.notifier)
            .profile!
            .responseData!
            .mobile,
        "category_id": fromUpdateAddress == true
            ? body.fields[8].value
            : body.fields[5].value,
        "type": "buy",
        "vehicle": "",
      };
      print("lkj" + body.fields[0].value);
      print(body.fields[1].value);
      print(body.fields[2].value);
      print(body.fields[8].value);

      final cartInsertResponse =
          await _apiRepository.fetchCourierServiceSave(cartInsertBody);
      if (cartInsertResponse != null) {
        body.fields
          ..add(MapEntry(
            'cart_id',
            cartInsertResponse['responseData']['iCartId'].toString(),
          ));
      }
      final cartUpdateResponse =
          await _apiRepository.fetchOrderCartUpdate(body);
      final orderCreateResponse =
          await _apiRepository.fetchDeliveryOrderInsert();

      historyDatabase.addHistory(HistoryData(
          id: orderCreateResponse.responseData!.id,
          status: orderCreateResponse.responseData!.status,
          booking_type: 'Buy',
          desc: 'From: ${orderCreateResponse.responseData!.deliveryContactNum}',
          dateTime: orderCreateResponse.responseData!.createdAt,
          amount: orderCreateResponse.responseData!.totalPayable,
          adminService: orderCreateResponse.responseData!.adminService));

      state = state.copyWith(
          response: orderCreateResponse, isLoading: false, isError: false);
    } catch (e) {
      showLog(e.toString());
      state = state.copyWith(
          isError: true, isLoading: false, errorMessage: e.toString());
    }
  }

  Future<void> buyItemsOrderPlaceFromCart(
      {bool init = true, required Map body,required BuildContext context}) async {
    var prefs = await SharedPreferences.getInstance();
    try {
      if (init) state = state.copyWith(isLoading: true);
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String? token = prefs.getString(SharedPreferencesPath.accessToken);
      Map<String, String> headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer $token"
      };
      var orderCreateResponse;
      //await _apiRepository.fetchBuyItemsOrderPlace(body: body);
      // HomeDeliveryOrderInsertModel n;
      // String boddy = json.encode(body);
      showLog("==========================================================");
      showLog("Post Method ===> ${App24UserAppAPI.baseUrl}${App24UserAppAPI.buyItemsOrderPlaceV2} ");
      showLog("Post Method body ===> ${body.toString()} ");
      showLog("==========================================================");
      Helpers()
          .getPostData(App24UserAppAPI.buyItemsOrderPlaceV2, body, headers)
          .then((value) {
        showLog("==========================================================");
        showLog("Post Method ===> ${App24UserAppAPI.baseUrl}${App24UserAppAPI.buyItemsOrderPlaceV2} ");
        showLog("Post Method response status ===> ${json.decode(value)['statusCode']} ");
        showLog("Post Method response ===> ${json.decode(value)} ");
        showLog("==========================================================");
        if (json.decode(value)['statusCode'].toString() == "200") {
          context.read(serviceListNotifierProvider.notifier).getServiceIdList = [];
          context.read(serviceListNotifierProvider.notifier).getItemIdList = [];
          context.read(serviceListNotifierProvider.notifier).getQuantityList = [];
          orderCreateResponse =
              HomeDeliveryOrderInsertModel.fromJson(json.decode(value));
          //  print("orderCreateResponse: "+orderCreateResponse.toString());
          prefs.setInt(SharedPreferencesPath.cartStoreId, 0);
          
          historyDatabase.addHistory(HistoryData(
              id: orderCreateResponse.responseData!.id,
              status: orderCreateResponse.responseData!.status,
              booking_type: 'Buy',
              desc:
                  'From: ${orderCreateResponse.responseData!.deliveryContactNum}',
              dateTime: orderCreateResponse.responseData!.createdAt,
              amount: orderCreateResponse.responseData!.totalPayable,
              adminService: orderCreateResponse.responseData!.adminService));

          state = state.copyWith(
              response: orderCreateResponse, isLoading: false, isError: false);
        }

        /*state = state.copyWith(
            response: orderCreateResponse, isLoading: false, isError: false);*/
      });
    } catch (e) {
      showLog(e.toString());
      state = state.copyWith(
          isError: true, isLoading: false, errorMessage: e.toString());
    }
  }
}
