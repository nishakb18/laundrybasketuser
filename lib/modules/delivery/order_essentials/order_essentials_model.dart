OrderEssentials orderModelFromJson(str) {
  return OrderEssentials.fromJson(str);
}

class OrderEssentials {
  String? statusCode;
  String? title;
  String? message;
  List<OrderEssentialsModelResponseData>? responseData;
  List<dynamic>? responseNewData;
  List<dynamic>? error;

  OrderEssentials(
      {this.statusCode,
      this.title,
      this.message,
      this.responseData,
      this.responseNewData,
      this.error});

  OrderEssentials.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    if (json['responseData'] != null) {
      responseData = [];
      json['responseData'].forEach((v) {
        responseData!.add(new OrderEssentialsModelResponseData.fromJson(v));
      });
    }
    if (json['responseNewData'] != null) {
      responseNewData = [];
      json['responseNewData'].forEach((v) {
        responseNewData!.add(v);
      });
    }
    if (json['error'] != null) {
      error = [];
      json['error'].forEach((v) {
        error!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['title'] = this.title;
    data['message'] = this.message;
    if (this.responseData != null) {
      data['responseData'] = this.responseData!.map((v) => v.toJson()).toList();
    }
    if (this.responseNewData != null) {
      data['responseNewData'] =
          this.responseNewData!.map((v) => v.toJson()).toList();
    }
    if (this.error != null) {
      data['error'] = this.error!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class OrderEssentialsModelResponseData {
  int? id;
  String? name;
  String? category;
  String? picture;
  int? status;
  String? itemsStatus;
  String? addonsStatus;
  String? keywords;
  int? visibility;

  OrderEssentialsModelResponseData(
      {this.id,
      this.name,
      this.category,
      this.picture,
      this.status,
      this.itemsStatus,
      this.addonsStatus,
      this.keywords,
      this.visibility});

  OrderEssentialsModelResponseData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    category = json['category'];
    picture = json['picture'];
    status = json['status'];
    itemsStatus = json['items_status'];
    addonsStatus = json['addons_status'];
    keywords = json['keywords'];
    visibility = json['visibility'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['category'] = this.category;
    data['picture'] = this.picture;
    data['status'] = this.status;
    data['items_status'] = this.itemsStatus;
    data['addons_status'] = this.addonsStatus;
    data['keywords'] = this.keywords;
    data['visibility'] = this.visibility;
    return data;
  }
}
