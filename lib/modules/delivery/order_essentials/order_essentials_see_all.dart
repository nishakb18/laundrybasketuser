import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/modules/delivery/order_essentials/order_essentials_model.dart';
import 'package:app24_user_app/modules/delivery/order_essentials/order_essentials_see_all_loading.dart';
import 'package:app24_user_app/modules/delivery/shop_list/shop_categories.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/widgets/common_appbar.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:app24_user_app/widgets/offline_builder_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class OrderEssentialsSeeAll extends StatefulWidget {
  const OrderEssentialsSeeAll({Key? key, this.itemImage}) : super(key: key);
  final String? itemImage;

  @override
  _OrderEssentialsSeeAllState createState() => _OrderEssentialsSeeAllState();
}

class _OrderEssentialsSeeAllState extends State<OrderEssentialsSeeAll> {
  reloadData({required BuildContext context, bool? init}) {
    return context
        .read(homeNotifierProvider)
        .getOffers(init: init, context: context);
  }

  final List<ItemLists> itemImages = [
    ItemLists(image: App24UserAppImages.orderGrocery),
    ItemLists(image: App24UserAppImages.orderMedicine),
    ItemLists(image: App24UserAppImages.orderVegetable),
    ItemLists(image: App24UserAppImages.orderMeat),
    ItemLists(image: App24UserAppImages.orderFood),
    ItemLists(image: App24UserAppImages.orderNature),
    ItemLists(image: App24UserAppImages.orderOffice),
    ItemLists(image: App24UserAppImages.orderBakery),
    ItemLists(image: App24UserAppImages.orderNature),
    ItemLists(image: App24UserAppImages.orderHomeMade),
    ItemLists(image: App24UserAppImages.orderDairy),
    ItemLists(image: App24UserAppImages.orderFlowers)
  ];

  List<Widget> listItem(context, List<ItemLists> itemImage,
      List<OrderEssentialsModelResponseData>? orderEssentialModel) {
    List<Widget> items = [];

    for (var i = 0; i < 8; i++) {
      items.add(
        Column(
          children: [
            Container(
                width: MediaQuery.of(context).size.width / 5,
                // height: MediaQuery.of(context).size.height / 12,
                // decoration: BoxDecoration(
                //   borderRadius: BorderRadius.circular(15),
                //   color: Theme.of(context).accentColor,
                // ),
                child: Column(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                              color: Colors.grey[100]!,
                              spreadRadius: 3,
                              blurRadius: 3,
                              offset: Offset(2, 4))
                        ],
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.white,
                      ),
                      // padding: EdgeInsets.all(1),
                      child: Material(
                        color: Colors.transparent,
                        child: InkWell(
                          borderRadius: BorderRadius.circular(15),
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ShopCategoryPage(
                                          storeTypeID: orderEssentialModel![i]
                                              .id
                                              .toString(),
                                        )));
                          },
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              // const SizedBox(
                              //   height: 15,
                              // ),
                              Padding(
                                padding: EdgeInsets.all(15),
                                child: Hero(
                                  tag: orderEssentialModel![i].name!,
                                  child: Image.asset(
                                    itemImage[i].image.toString(),
                                    fit: BoxFit.contain,
                                    width: 40,
                                    height: 40,
                                    errorBuilder: (BuildContext context,
                                            Object exception,
                                            StackTrace? stackTrace) =>
                                        Image.asset(
                                      App24UserAppImages.placeHolderImage,
                                      width: 40,
                                      height: 40,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Text(
                      orderEssentialModel[i].name!,
                      style: TextStyle(
                          color: App24Colors.darkTextColor,
                          fontWeight: FontWeight.w600,
                          fontSize: MediaQuery.of(context).size.width / 29),
                      overflow: TextOverflow.clip,
                      textAlign: TextAlign.center,
                      maxLines: 2,
                    )
                  ],
                )),
            const SizedBox(
              height: 5,
            ),
          ],
        ),
      );
    }
    return items;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(title: "See All"),
      body: OfflineBuilderWidget(
        SafeArea(
          child: RefreshIndicator(
            onRefresh: () {
              return reloadData(init: false, context: context) ??
                  false as Future<void>;
            },
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: Consumer(builder: (context, watch, child) {
                final state = watch(orderNotifierProvider);
                if (state.isLoading) {
                  return OrderEssentialSeeAllLoading();
                } else if (state.isError) {
                  return LoadingError(
                    onPressed: (res) {
                      reloadData(init: true, context: res);
                    },
                    message: state.errorMessage.toString(),
                  );
                } else {
                  return Wrap(
                    alignment: WrapAlignment.center,
                    runAlignment: WrapAlignment.center,
                    spacing: 10,
                    runSpacing: 20,
                    children: listItem(
                        context, itemImages, state.response.responseData),
                  );
                }
              }),
            ),
          ),
        ),
      ),
    );
  }
}

class ItemLists {
  final String? image;
  // Color? color;

  ItemLists({this.image});

// final String? title;
// bool? isSelected;
}
