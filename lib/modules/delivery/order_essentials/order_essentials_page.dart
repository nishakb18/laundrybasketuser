import 'package:app24_user_app/app24_user_icons.dart';
import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/constants/global_constants.dart';
import 'package:app24_user_app/models/location_model.dart';
import 'package:app24_user_app/modules/delivery/cart/cart.dart';
import 'package:app24_user_app/modules/delivery/order_essentials/order_essentials_loading.dart';
import 'package:app24_user_app/modules/delivery/order_essentials/order_essentials_model.dart';
import 'package:app24_user_app/modules/delivery/order_essentials/order_essentials_see_all.dart';
import 'package:app24_user_app/modules/delivery/shop_list/shop_categories.dart';
import 'package:app24_user_app/modules/tabs/home/models/home_screen_tiles_model.dart';
import 'package:app24_user_app/providers/delivery_menu_provider.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/widgets/common_appbar.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:app24_user_app/widgets/offline_builder_widget.dart';
import 'package:app24_user_app/widgets/view_cart_overlay.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final deliveryMenuProvider =
    ChangeNotifierProvider<DeliveryMenuProvider>((ref) {
  return DeliveryMenuProvider();
});

class ShopEssentials extends StatefulWidget {
  @override
  _ShopEssentialsState createState() => _ShopEssentialsState();
}

class _ShopEssentialsState extends State<ShopEssentials> {
  // String selected_tab = "buy";
  double sliverAppbarHeight = 0.0;
  double collapsedHeight = 0.0;

  reloadData({required BuildContext context, bool? init}) {
    context
        .read(cartCountNotifierProvider.notifier)
        .getCartCount(context: context);
    context.read(orderNotifierProvider.notifier).getOrder();
  }

  int _current = 0;
  List<String> _homeCarouselList = [
    App24UserAppImages.offerApp24,
    App24UserAppImages.offerCardApp24,
    App24UserAppImages.offerBanner,
    App24UserAppImages.offerBannerPizza,
  ];

  // final List<ItemLists> itemImages = [
  //   ItemLists(image: App24UserAppImages.orderGrocery),
  //   ItemLists(image: App24UserAppImages.orderMedicine),
  //   ItemLists(image: App24UserAppImages.orderVegetable),
  //   ItemLists(image: App24UserAppImages.orderMeat),
  //   ItemLists(image: App24UserAppImages.orderNature),
  //   ItemLists(image: App24UserAppImages.orderNature),
  //   ItemLists(image: App24UserAppImages.orderBakery),
  //   ItemLists(image: App24UserAppImages.orderHomeMade),
  //   ItemLists(image: App24UserAppImages.orderDairy),
  //   ItemLists(image: App24UserAppImages.orderFlowers),
  //   ItemLists(image: App24UserAppImages.orderFlowers),
  //   // ItemLists(
  //   //     image: App24UserAppImages.orderNature
  //   // )
  //   // ItemLists(
  //   //   image: App24UserAppImages
  //   // )
  // ];

  @override
  void initState() {
    SchedulerBinding.instance!.addPostFrameCallback((_) {
      //API call after screen build
      // context.read(homeNotifierProvider).getOffers(context: context);
      context.read(orderNotifierProvider.notifier).getOrder();
      /*  if (context
          .read(cartCountNotifierProvider.notifier)
          .state
          .response == null)*/
      context
          .read(cartCountNotifierProvider.notifier)
          .getCartCount(context: context);
    });
    super.initState();
  }

  Route _createRoute() {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) => Cart(),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        const begin = Offset(1.0, 0.0);
        const end = Offset.zero;
        const curve = Curves.ease;

        var tween =
            Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }

  //Shop by categories
  List<Widget> listItem(
      context,
      /* List<ItemLists> itemImage,*/
      String categoryType,
      List<OrderEssentialsModelResponseData>? orderEssentialModel) {
    List<Widget> items = [];

    for (var i = 0; i < orderEssentialModel!.length; i++) {
      final laundryshop = orderEssentialModel[i].id;

      // orderEssentialModel.sort((a, b) => a.category!.compareTo(b.category!));
      if (orderEssentialModel[i].name != "Food" &&
          orderEssentialModel[i].name != "Delivery") if (categoryType ==
              "Medical"
          ? orderEssentialModel[i].category == "Medical"
          : categoryType == "Gifts and Flowers"
              ? orderEssentialModel[i].category == "Gifts and Flowers"
              : categoryType == "Daily Provisions"
                  ? orderEssentialModel[i].category == "Daily Provisions"
                  : categoryType == "Confectionary"
                      ? orderEssentialModel[i].category == "OTHERS"
                      : orderEssentialModel[i].category == "laundry"
                          ? orderEssentialModel[i].category == "Confectionary"
                          : orderEssentialModel[i].category == "Desi Items")
        items.add(
          Column(
            children: [
              Container(
                  width: MediaQuery.of(context).size.width / 5,
                  child: Column(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey[100]!,
                                spreadRadius: 3,
                                blurRadius: 5,
                                offset: Offset(2, 4))
                          ],
                          borderRadius: BorderRadius.circular(15),
                          color: Colors.white,
                        ),
                        // padding: EdgeInsets.all(1),
                        child: Material(
                          color: Colors.transparent,
                          child: InkWell(
                            borderRadius: BorderRadius.circular(15),
                            onTap: () {
                              Navigator.push(
                                  context,
                                  PageRouteBuilder(
                                    pageBuilder: (context, animation,
                                            secondaryAnimation) =>
                                        ShopCategoryPage(
                                      storeTypeID:
                                          orderEssentialModel[i].id.toString(),
                                    ),
                                    transitionsBuilder: (context, animation,
                                        secondaryAnimation, child) {
                                      const begin = Offset(1.0, 0.0);
                                      const end = Offset.zero;
                                      const curve = Curves.ease;
                                      var tween = Tween(begin: begin, end: end)
                                          .chain(CurveTween(curve: curve));

                                      return SlideTransition(
                                        position: animation.drive(tween),
                                        child: child,
                                      );
                                    },
                                  )

                                  // MaterialPageRoute(
                                  //     builder: (context) => )
                                  );
                            },
                            child: Hero(
                              tag: orderEssentialModel[i].name!,
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 15, vertical: 10),
                                child: CachedNetworkImage(
                                  imageUrl:
                                      orderEssentialModel[i].picture.toString(),
                                  fit: BoxFit.contain,
                                  width: 40,
                                  height: 40,
                                  placeholder: (context, url) => Image.asset(
                                    App24UserAppImages.placeHolderImage,
                                    width: 40,
                                    height: 40,
                                  ),
                                  errorWidget: (context, url, error) =>
                                      Image.asset(
                                    App24UserAppImages.placeHolderImage,
                                    width: 40,
                                    height: 40,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 5),
                        child: Text(
                          orderEssentialModel[i].name!,
                          style: TextStyle(
                              color: App24Colors.darkTextColor,
                              fontWeight: FontWeight.w600,
                              fontSize: MediaQuery.of(context).size.width / 32),
                          overflow: TextOverflow.clip,
                          textAlign: TextAlign.center,
                          maxLines: 2,
                        ),
                      )
                    ],
                  )),
              const SizedBox(
                height: 5,
              ),
            ],
          ),
        );
    }
    return items;
  }

  List<Widget> getCarouselImages(context, List<Promocodes> promoCodeList) {
    final List<Widget> imageSliders = promoCodeList
        .map((item) => Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Container(
                // padding: EdgeInsets.symmetric(horizontal: 10),
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(30),
                    child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: new FadeInImage.assetNetwork(
                          placeholder: 'assets/images/placeholder.png',
                          image: item.picture!,
                          fit: BoxFit.cover,
                        ))),
              ),
            ))
        .toList();

    return imageSliders;
  }

  selectLocation(String title, bool searchUserLocation) {
    showSelectLocationSheet(
            context: context,
            title: title,
            searchUserLocation: searchUserLocation)
        .then((value) {
      if (value != null) {
        LocationModel model = value;
        setState(() {
          context.read(homeNotifierProvider.notifier).currentLocation = model;
          // loadData(context: context);
        });
      }
    });
  }

  Widget buildSearchBar() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(
            sliverAppbarHeight > collapsedHeight ? 14 : 18),
        color: Color(0xffF1FFF7),
        // border: Border.all(color: Colors.grey[200]!)
      ),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
            onTap: () {
              selectLocation(GlobalConstants.labelGlobalLocation, true);
              // print(context
              //     .read(homeNotifierProvider.notifier)
              //     .currentLocation
              //     .latitude);
            },
            borderRadius: BorderRadius.circular(14.0),
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: 10,
                  vertical: sliverAppbarHeight > collapsedHeight ? 10 : 18),
              child: Row(
                children: [
                  Icon(
                    App24User.path_2568_3x,
                    color: App24Colors.greenOrderEssentialThemeColor,
                    size: sliverAppbarHeight > collapsedHeight ? 15 : 19,
                  ),
                  const SizedBox(
                    width: 7.0,
                  ),
                  Expanded(
                      child: Text(
                    context
                            .read(homeNotifierProvider.notifier)
                            .currentLocation
                            .main_text ??
                        'Look up your address',
                    style: TextStyle(
                        color: App24Colors.greenOrderEssentialThemeColor,
                        fontWeight: FontWeight.bold,
                        fontSize:
                            sliverAppbarHeight > collapsedHeight ? 11 : 15),
                    // maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ))
                ],
              ),
            )),
      ),
    );
  }

  Widget buildChangeLocationButton() {
    return Container(
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Color(0xffE3FFEE),
          border: Border.all(color: Colors.grey[200]!)),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: () {},
          borderRadius: BorderRadius.circular(15),
          child: Padding(
            padding: EdgeInsets.symmetric(
                vertical: sliverAppbarHeight > 80 ? 8 : 14,
                horizontal: sliverAppbarHeight > 80 ? 9 : 16),
            child: Icon(
              Icons.my_location,
              color: App24Colors.greenOrderEssentialThemeColor,
              size: sliverAppbarHeight > 80 ? 15 : 24,
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    collapsedHeight = MediaQuery.of(context).size.height * 0.130;
    return Scaffold(
        backgroundColor: Colors.white,
        body: OfflineBuilderWidget(
          SafeArea(
            child: RefreshIndicator(
              onRefresh: () {
                return reloadData(init: false, context: context) ??
                    false as Future<void>;
              },
              child: Container(
                  child: Column(
                children: [
                  Expanded(
                    child: CustomScrollView(
                      slivers: <Widget>[
                        SliverAppBar(
                            backgroundColor: Colors.white,
                            automaticallyImplyLeading: true,
                            pinned: true,
                            toolbarHeight: 56.0,
                            elevation: 0.0,
                            iconTheme:
                                IconThemeData(color: App24Colors.darkTextColor),
                            // actions: [
                            //   Padding(
                            //       padding: const EdgeInsets.only(right: 10),
                            //       child:
                            //           cartIcon(context: context, fromPage: "")
                            //       // IconButton(
                            //       //     icon: Icon(App24User.group_636_3x,size: 17,),
                            //       //     onPressed: () {
                            //       //       Navigator.push(
                            //       //           context,
                            //       //          _createRoute());
                            //       //     }),
                            //       )
                            // ],
                            title: const Text(
                              "Shop Essentials",
                              style:
                                  TextStyle(color: App24Colors.darkTextColor),
                            ),
                            collapsedHeight: collapsedHeight,
                            expandedHeight:
                                MediaQuery.of(context).size.height * 0.470,
                            flexibleSpace: LayoutBuilder(builder:
                                (BuildContext context,
                                    BoxConstraints constraints) {
                              // print('constraints=' + constraints.toString());
                              sliverAppbarHeight = constraints.biggest.height;
                              return FlexibleSpaceBar(
                                titlePadding: EdgeInsetsDirectional.zero,
                                title: Container(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 13, vertical: 10),
                                  child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        buildSearchBar(),
                                      ]),
                                ),
                                background: Container(
                                  //padding: EdgeInsets.only(top: 60),
                                  color: Colors.white,
                                  child: Container(
                                      color: Colors.white,
                                      margin: EdgeInsets.only(bottom: 35),
                                      child: Stack(
                                        fit: StackFit.expand,
                                        children: [
                                          Positioned(
                                            bottom: 0,
                                            child: ClipRRect(
                                              borderRadius:
                                                  const BorderRadius.only(
                                                bottomLeft: Radius.circular(30),
                                                bottomRight:
                                                    Radius.circular(30),
                                              ),
                                              child: Image.asset(
                                                App24UserAppImages
                                                    .orderEssentialBG,
                                                fit: BoxFit.cover,
                                                width: MediaQuery.of(context)
                                                    .size
                                                    .width,
                                              ),
                                            ),
                                          ),
                                          // Positioned(
                                          //     left: 20,
                                          //     bottom: 40,
                                          //     child: Text(
                                          //       "Get essentials from nearby stores",
                                          //       style: TextStyle(
                                          //           fontSize:
                                          //               MediaQuery.of(context)
                                          //                       .size
                                          //                       .width /
                                          //                   22,
                                          //           color: Colors.white,
                                          //           fontWeight:
                                          //               FontWeight.w700),
                                          //     ))
                                        ],
                                      )),
                                ),
                              );
                            })),
                        SliverList(
                            delegate: SliverChildListDelegate([
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Shop by Category",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize:
                                          MediaQuery.of(context).size.width /
                                              20,
                                      color: App24Colors.darkTextColor),
                                ),
                                // Material(
                                //   color: Colors.transparent,
                                //   child: InkWell(
                                //     onTap: () {
                                //       Navigator.push(
                                //           context,
                                //
                                //           MaterialPageRoute(
                                //               builder: (context) =>
                                //                   OrderEssentialsSeeAll(itemImage: itemImages.toString(),)));
                                //     },
                                //     child: Row(
                                //       children: [
                                //         Text("See All",style: TextStyle(fontWeight: FontWeight.w600),),
                                //         const SizedBox(
                                //           width: 10,
                                //         ),
                                //         Image.asset(
                                //           App24UserAppImages.roundRArrow,
                                //           width:
                                //               MediaQuery.of(context).size.width /
                                //                   20,
                                //         )
                                //       ],
                                //     ),
                                //   ),
                                // )
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Consumer(builder: (context, watch, child) {
                            final state = watch(orderNotifierProvider);
                            print(state);
                            if (state.isLoading) {
                              return OrderEssentialLoading();
                            } else if (state.isError) {
                              return LoadingError(
                                onPressed: (res) {
                                  reloadData(init: true, context: res);
                                },
                                message: state.errorMessage.toString(),
                              );
                            } else {
                              return Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(left: 15),
                                    child: Wrap(
                                        alignment: WrapAlignment.start,
                                        runAlignment: WrapAlignment.center,
                                        spacing: 10,
                                        runSpacing: 20,
                                        children: listItem(
                                                context,
                                                /*itemImages,*/
                                                "Daily Provisions",
                                                state.response.responseData) +
                                            listItem(
                                                context,
                                                /*itemImages,*/
                                                "Desi Items",
                                                state.response.responseData) +
                                            listItem(
                                                context,
                                                /*itemImages,*/
                                                "Confectionary",
                                                state.response.responseData) +
                                            listItem(
                                                context,
                                                /*itemImages,*/
                                                "Medical",
                                                state.response.responseData) +
                                            listItem(
                                                context,
                                                /*itemImages,*/
                                                "Gifts and Flowers",
                                                state.response.responseData)
                                        //     +
                                        // listItem(
                                        //     context,
                                        //     /*itemImages,*/
                                        //     "laundryshop",
                                        //     state.response.responseData),
                                        ),
                                  ),
                                  // const SizedBox(height: 10,),
                                  // Padding(
                                  //   padding: const EdgeInsets.symmetric(horizontal: 15),
                                  //   child: Divider(),
                                  // ),
                                  // const SizedBox(height: 10,),
                                  // Padding(
                                  //   padding: const EdgeInsets.only(left: 15),
                                  //   child: Wrap(
                                  //     alignment: WrapAlignment.start,
                                  //     runAlignment: WrapAlignment.center,
                                  //     spacing: 10,
                                  //     runSpacing: 20,
                                  //     children: listItem(
                                  //         context,
                                  //         /*itemImages,*/
                                  //         "Desi Items",
                                  //         state.response.responseData),
                                  //   ),
                                  // ),
                                  // const SizedBox(height: 10,),
                                  // Padding(
                                  //   padding: const EdgeInsets.symmetric(horizontal: 15),
                                  //   child: Divider(),
                                  // ),
                                  // const SizedBox(height: 10,),
                                  // Padding(
                                  //   padding: const EdgeInsets.only(left: 15),
                                  //   child: Wrap(
                                  //     alignment: WrapAlignment.start,
                                  //     runAlignment: WrapAlignment.center,
                                  //     spacing: 10,
                                  //     runSpacing: 20,
                                  //     children: listItem(
                                  //         context,
                                  //         /*itemImages,*/
                                  //         "Confectionary",
                                  //         state.response.responseData),
                                  //   ),
                                  // ),
                                  // const SizedBox(height: 10,),
                                  // Padding(
                                  //   padding: const EdgeInsets.symmetric(horizontal: 15),
                                  //   child: Divider(),
                                  // ),
                                  // const SizedBox(height: 10,),
                                  //
                                  // Padding(
                                  //   padding: const EdgeInsets.only(left: 15),
                                  //   child: Wrap(
                                  //     alignment: WrapAlignment.start,
                                  //     runAlignment: WrapAlignment.center,
                                  //     spacing: 10,
                                  //     runSpacing: 20,
                                  //     children: listItem(
                                  //         context,
                                  //         /*itemImages,*/
                                  //         "Medical",
                                  //         state.response.responseData),
                                  //   ),
                                  // ),
                                  //
                                  //
                                  // const SizedBox(height: 10,),
                                  // Padding(
                                  //   padding: const EdgeInsets.symmetric(horizontal: 15),
                                  //   child: Divider(),
                                  // ),
                                  // const SizedBox(height: 10,),
                                  // Padding(
                                  //   padding: const EdgeInsets.only(left: 15),
                                  //   child: Wrap(
                                  //     alignment: WrapAlignment.start,
                                  //     runAlignment: WrapAlignment.center,
                                  //     spacing: 10,
                                  //     runSpacing: 20,
                                  //     children: listItem(
                                  //         context,
                                  //         /*itemImages,*/
                                  //         "Gifts and Flowers",
                                  //         state.response.responseData),
                                  //   ),
                                  // ),
                                ],
                              );
                            }
                          }),

                          // Consumer(builder: (context, watch, child) {
                          //   final state = watch(homeNotifierProvider);
                          //   if (state.isLoading) {
                          //     return OrderEssentialLoading();
                          //   } else if (state.isError) {
                          //     return LoadingError(
                          //       onPressed: (res) {
                          //         reloadData(init: true, context: res);
                          //       },
                          //       message: state.errorMessage.toString(),
                          //     );
                          //   } else {
                          //     return                           Column(
                          //         crossAxisAlignment: CrossAxisAlignment.stretch,
                          //         children: [
                          //           CarouselSlider(
                          //             items: getCarouselImages(context,
                          //                 context.read(homeNotifierProvider).getOffers.response.responseData.promocodes),
                          //             options: CarouselOptions(
                          //               height:
                          //               MediaQuery.of(context).size.height / 3.9,
                          //               autoPlay: false,
                          //               enlargeCenterPage: true,
                          //             ),
                          //           ),
                          //           Row(
                          //             mainAxisAlignment: MainAxisAlignment.center,
                          //             children: <Widget>[
                          //               ...List.generate(
                          //                   state.response.responseData.promocodes
                          //                       .length,
                          //                       (index) => Container(
                          //                     width: 8.0,
                          //                     height: 8.0,
                          //                     margin: EdgeInsets.symmetric(
                          //                         vertical: 10.0,
                          //                         horizontal: 2.0),
                          //                     decoration: BoxDecoration(
                          //                       shape: BoxShape.circle,
                          //                       color: _current == index
                          //                           ? Theme.of(context)
                          //                           .primaryColor
                          //                           : Colors.grey[300],
                          //                     ),
                          //                   )),
                          //             ],
                          //           )
                          //         ]);
                          //   }
                          // }),
                          const SizedBox(
                            height: 50,
                          )
                        ])),
                      ],
                    ),
                  ),
                  ViewCartOverlay()
                ],
              )),
            ),
          ),
        ));
  }

  Widget oderdata(int a) {
    return Container(child: Text(a.toString()));
  }
}



// class ItemLists {
//   final String? image;
//
//   // Color? color;
//
//   ItemLists({this.image});
//
// // final String? title;
// // bool? isSelected;
// }
