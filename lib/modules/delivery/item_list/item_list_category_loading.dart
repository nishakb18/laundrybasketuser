import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ItemListCategoryLoading extends StatefulWidget {
  @override
  _ItemListCategoryLoadingState createState() =>
      _ItemListCategoryLoadingState();
}

class _ItemListCategoryLoadingState extends State<ItemListCategoryLoading> {
  Timer? _timer;
  int _start = 10; // time to popup still working window /// in seconds

  @override
  void initState() {
    startTimer();
    super.initState();
  }

  @override
  void dispose() {
    if (_timer != null) {
      _timer!.cancel();
    }
    super.dispose();
  }

  void startTimer() {
    if (_timer != null) {
      _timer!.cancel();
      _timer = null;
    } else {
      _timer = new Timer.periodic(const Duration(seconds: 1), (Timer timer) {
        setState(() {
          if (_start < 1) {
            _timer!.cancel();
          } else {
            _start = _start - 1;
          }
        });
      });
    }
  }

  buildRestaurantShimmerItem(context) {
    List<Widget> list = [];

    for (var i = 0; i < 10; i++)
      list.add(Container(
        width: MediaQuery.of(context).size.width / 4.5,
        margin: EdgeInsets.symmetric(vertical: 10, horizontal: 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 20,
              // margin: EdgeInsets.only(top: 15),
              width: 70,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xffefefef),
                      blurRadius: 20,
                      spreadRadius: 3,
                    ),
                  ]),
            ),
          ],
        ),
      ));

    return list;
  }

  buildStillLoadingWidget() {
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(15)),
      width: MediaQuery.of(context).size.width,
      child: ListTile(
        title: Text("Please wait..."),
        subtitle: Text(
            "The network seems to be too busy. We suggest you to wait for some more time."),
        leading: CircularProgressIndicator.adaptive(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height,
        child: Stack(
          fit: StackFit.expand,
          children: [
            Shimmer.fromColors(
                baseColor: Colors.grey[300]!,
                highlightColor: Colors.grey[100]!,
                child: ListView(
                  // padding:
                  // const EdgeInsets.symmetric(horizontal: 10),
                  scrollDirection: Axis.horizontal,
                  children: buildRestaurantShimmerItem(context)
                )

                // ListView(
                //   children:[ ],
                //   scrollDirection: Axis.horizontal,
                // )
                // Wrap(
                //     alignment: WrapAlignment.center,
                //     runAlignment: WrapAlignment.center,
                //     spacing: 0,
                //     runSpacing: 0,
                //     children: buildRestaurantShimmerItem(context))
                ),
            // Positioned(
            //   left: 20,
            //   right: 20,
            //   top: 50,
            //   child:  _start == 0 ? buildStillLoadingWidget() : Container(),
            // )
          ],
        ));
  }
}
