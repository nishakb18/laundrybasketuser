ItemListModel itemListModelFromJson(str) {
  return ItemListModel.fromJson(str);
}

class ItemListModel {
  String? statusCode;
  String? title;
  String? message;
  ItemListResponseData? responseData;
  List<dynamic>? responseNewData;
  List<dynamic>? error;

  ItemListModel(
      {this.statusCode,
        this.title,
        this.message,
        this.responseData,
        this.responseNewData,
        this.error});

  ItemListModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    responseData = json['responseData'] != null
        ? new ItemListResponseData.fromJson(json['responseData'])
        : null;
    if (json['responseNewData'] != null) {
      responseNewData = [];
      json['responseNewData'].forEach((v) {
        responseNewData!.add(v);
      });
    }
    if (json['error'] != null) {
      error = [];
      json['error'].forEach((v) {
        error!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['title'] = this.title;
    data['message'] = this.message;
    if (this.responseData != null) {
      data['responseData'] = this.responseData!.toJson();
    }
    if (this.responseNewData != null) {
      data['responseNewData'] =
          this.responseNewData!.map((v) => v.toJson()).toList();
    }
    if (this.error != null) {
      data['error'] = this.error!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ItemListResponseData {
  dynamic id;
  dynamic storeTypeId;
  String? storeMultiTypeId;
  String? storeName;
  String? currencySymbol;
  String? storeLocation;
  double? latitude;
  double? longitude;
  String? gallery;
  String? picture;
  String? offerMinAmount;
  String? estimatedDeliveryTime;
  dynamic freeDelivery;
  String? isVeg;
  dynamic rating;
  dynamic offerPercent;
  dynamic walletBalance;
  dynamic hidePrice;
  dynamic totalstorecart;
  String? shopstatus;
  dynamic usercart;
  dynamic totalcartprice;
  List<AllCategories>? allCategories;
  List<AllCategories>? categories;
  Storetype? storetype;
  List<ProductsBanners>? productsBanners;
  List<Products>? products;

  ItemListResponseData(
      {this.id,
        this.storeTypeId,
        this.storeMultiTypeId,
        this.storeName,
        this.currencySymbol,
        this.storeLocation,
        this.latitude,
        this.longitude,
        this.gallery,
        this.picture,
        this.offerMinAmount,
        this.estimatedDeliveryTime,
        this.freeDelivery,
        this.isVeg,
        this.rating,
        this.offerPercent,
        this.walletBalance,
        this.hidePrice,
        this.totalstorecart,
        this.shopstatus,
        this.usercart,
        this.totalcartprice,
        this.allCategories,
        this.categories,
        this.storetype,
        this.productsBanners,
        this.products});

  ItemListResponseData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    storeTypeId = json['store_type_id'];
    storeMultiTypeId = json['store_multi_type_id'];
    storeName = json['store_name'];
    currencySymbol = json['currency_symbol'];
    storeLocation = json['store_location'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    gallery = json['gallery'];
    picture = json['picture'];
    offerMinAmount = json['offer_min_amount'];
    estimatedDeliveryTime = json['estimated_delivery_time'];
    freeDelivery = json['free_delivery'];
    isVeg = json['is_veg'];
    rating = json['rating'];
    offerPercent = json['offer_percent'];
    walletBalance = json['wallet_balance'];
    hidePrice = json['hide_price'];
    totalstorecart = json['totalstorecart'];
    shopstatus = json['shopstatus'];
    usercart = json['usercart'];
    totalcartprice = json['totalcartprice'];
    if (json['all_categories'] != null) {
      allCategories = [];
      json['all_categories'].forEach((v) {
        allCategories!.add(new AllCategories.fromJson(v));
      });
    }
    if (json['categories'] != null) {
      categories = [];
      json['categories'].forEach((v) {
        categories!.add(new AllCategories.fromJson(v));
      });
    }
    storetype = json['storetype'] != null
        ? new Storetype.fromJson(json['storetype'])
        : null;
    if (json['products_banners'] != null) {
      productsBanners = [];
      json['products_banners'].forEach((v) {
        productsBanners!.add(new ProductsBanners.fromJson(v));
      });
    }
    if (json['products'] != null) {
      products = [];
      json['products'].forEach((v) {
        products!.add(new Products.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['store_type_id'] = this.storeTypeId;
    data['store_multi_type_id'] = this.storeMultiTypeId;
    data['store_name'] = this.storeName;
    data['currency_symbol'] = this.currencySymbol;
    data['store_location'] = this.storeLocation;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['gallery'] = this.gallery;
    data['picture'] = this.picture;
    data['offer_min_amount'] = this.offerMinAmount;
    data['estimated_delivery_time'] = this.estimatedDeliveryTime;
    data['free_delivery'] = this.freeDelivery;
    data['is_veg'] = this.isVeg;
    data['rating'] = this.rating;
    data['offer_percent'] = this.offerPercent;
    data['wallet_balance'] = this.walletBalance;
    data['hide_price'] = this.hidePrice;
    data['totalstorecart'] = this.totalstorecart;
    data['shopstatus'] = this.shopstatus;
    data['usercart'] = this.usercart;
    data['totalcartprice'] = this.totalcartprice;
    if (this.allCategories != null) {
      data['all_categories'] =
          this.allCategories!.map((v) => v.toJson()).toList();
    }
    if (this.categories != null) {
      data['categories'] = this.categories!.map((v) => v.toJson()).toList();
    }
    if (this.storetype != null) {
      data['storetype'] = this.storetype!.toJson();
    }
    if (this.productsBanners != null) {
      data['products_banners'] =
          this.productsBanners!.map((v) => v.toJson()).toList();
    }
    if (this.products != null) {
      data['products'] = this.products!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class AllCategories {
  dynamic id;
  dynamic storeId;
  String? storeCategoryName;
  String? storeCategoryDescription;
  String? picture;
  dynamic storeCategoryStatus;

  AllCategories(
      {this.id,
        this.storeId,
        this.storeCategoryName,
        this.storeCategoryDescription,
        this.picture,
        this.storeCategoryStatus});

  AllCategories.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    storeId = json['store_id'];
    storeCategoryName = json['store_category_name'];
    storeCategoryDescription = json['store_category_description'];
    picture = json['picture'];
    storeCategoryStatus = json['store_category_status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['store_id'] = this.storeId;
    data['store_category_name'] = this.storeCategoryName;
    data['store_category_description'] = this.storeCategoryDescription;
    data['picture'] = this.picture;
    data['store_category_status'] = this.storeCategoryStatus;
    return data;
  }
}

class Storetype {
  dynamic id;
  String? name;
  String? category;
  String? picture;
  dynamic status;
  String? itemsStatus;
  String? addonsStatus;
  String? keywords;
  dynamic visibility;

  Storetype(
      {this.id,
        this.name,
        this.category,
        this.picture,
        this.status,
        this.itemsStatus,
        this.addonsStatus,
        this.keywords,
        this.visibility});

  Storetype.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    category = json['category'];
    picture = json['picture'];
    status = json['status'];
    itemsStatus = json['items_status'];
    addonsStatus = json['addons_status'];
    keywords = json['keywords'];
    visibility = json['visibility'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['category'] = this.category;
    data['picture'] = this.picture;
    data['status'] = this.status;
    data['items_status'] = this.itemsStatus;
    data['addons_status'] = this.addonsStatus;
    data['keywords'] = this.keywords;
    data['visibility'] = this.visibility;
    return data;
  }
}

class ProductsBanners {
  dynamic id;
  dynamic storeId;
  String? itemName;
  String? itemDescription;
  String? picture;
  dynamic storeCategoryId;
  String? isVeg;
  dynamic itemPrice;
  dynamic itemDiscount;
  String? itemDiscountType;
  dynamic isAddon;
  dynamic isStock;
  dynamic qty;
  dynamic status;
  dynamic isBanner;
  dynamic reviewCount;
  dynamic avgRating;
  dynamic offer;
  dynamic productOffer;
  dynamic outofstock;
  dynamic hidePrice;
  List<Itemsaddon>? itemsaddon;
  List<dynamic>? itemcart;

  ProductsBanners(
      {this.id,
        this.storeId,
        this.itemName,
        this.itemDescription,
        this.picture,
        this.storeCategoryId,
        this.isVeg,
        this.itemPrice,
        this.itemDiscount,
        this.itemDiscountType,
        this.isAddon,
        this.isStock,
        this.qty,
        this.status,
        this.isBanner,
        this.reviewCount,
        this.avgRating,
        this.offer,
        this.productOffer,
        this.outofstock,
        this.hidePrice,
        this.itemsaddon,
        this.itemcart});

  ProductsBanners.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    storeId = json['store_id'];
    itemName = json['item_name'];
    itemDescription = json['item_description'];
    picture = json['picture'];
    storeCategoryId = json['store_category_id'];
    isVeg = json['is_veg'];
    itemPrice = json['item_price'];
    itemDiscount = json['item_discount'];
    itemDiscountType = json['item_discount_type'];
    isAddon = json['is_addon'];
    isStock = json['is_stock'];
    qty = json['qty'];
    status = json['status'];
    isBanner = json['is_banner'];
    reviewCount = json['review_count'];
    avgRating = json['avg_rating'];
    offer = json['offer'];
    productOffer = json['product_offer'];
    outofstock = json['outofstock'];
    hidePrice = json['hide_price'];
    if (json['itemsaddon'] != null) {
      itemsaddon = [];
      json['itemsaddon'].forEach((v) {
        itemsaddon!.add(new Itemsaddon.fromJson(v));
      });
    }
    if (json['itemcart'] != null) {
      itemcart = [];
      json['itemcart'].forEach((v) {
        itemcart!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['store_id'] = this.storeId;
    data['item_name'] = this.itemName;
    data['item_description'] = this.itemDescription;
    data['picture'] = this.picture;
    data['store_category_id'] = this.storeCategoryId;
    data['is_veg'] = this.isVeg;
    data['item_price'] = this.itemPrice;
    data['item_discount'] = this.itemDiscount;
    data['item_discount_type'] = this.itemDiscountType;
    data['is_addon'] = this.isAddon;
    data['is_stock'] = this.isStock;
    data['qty'] = this.qty;
    data['status'] = this.status;
    data['is_banner'] = this.isBanner;
    data['review_count'] = this.reviewCount;
    data['avg_rating'] = this.avgRating;
    data['offer'] = this.offer;
    data['product_offer'] = this.productOffer;
    data['outofstock'] = this.outofstock;
    data['hide_price'] = this.hidePrice;
    if (this.itemsaddon != null) {
      data['itemsaddon'] = this.itemsaddon!.map((v) => v.toJson()).toList();
    }
    if (this.itemcart != null) {
      data['itemcart'] = this.itemcart!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Itemsaddon {
  dynamic id;
  dynamic storeId;
  dynamic storeItemId;
  dynamic storeAddonId;
  dynamic price;
  String? addonName;

  Itemsaddon(
      {this.id,
        this.storeId,
        this.storeItemId,
        this.storeAddonId,
        this.price,
        this.addonName});

  Itemsaddon.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    storeId = json['store_id'];
    storeItemId = json['store_item_id'];
    storeAddonId = json['store_addon_id'];
    price = json['price'];
    addonName = json['addon_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['store_id'] = this.storeId;
    data['store_item_id'] = this.storeItemId;
    data['store_addon_id'] = this.storeAddonId;
    data['price'] = this.price;
    data['addon_name'] = this.addonName;
    return data;
  }
}

class Products {
  dynamic id;
  dynamic storeId;
  String? itemName;
  String? itemDescription;
  String? picture;
  dynamic storeCategoryId;
  String? isVeg;
  dynamic itemPrice;
  dynamic itemDiscount;
  String? itemDiscountType;
  dynamic isAddon;
  dynamic isStock;
  dynamic qty;
  dynamic status;
  dynamic isBanner;
  dynamic reviewCount;
  dynamic avgRating;
  dynamic offer;
  dynamic productOffer;
  dynamic outofstock;
  dynamic hidePrice;
  List<Itemsaddon>? itemsaddon;
  List<dynamic>? itemcart;

  Products(
      {this.id,
        this.storeId,
        this.itemName,
        this.itemDescription,
        this.picture,
        this.storeCategoryId,
        this.isVeg,
        this.itemPrice,
        this.itemDiscount,
        this.itemDiscountType,
        this.isAddon,
        this.isStock,
        this.qty,
        this.status,
        this.isBanner,
        this.reviewCount,
        this.avgRating,
        this.offer,
        this.productOffer,
        this.outofstock,
        this.hidePrice,
        this.itemsaddon,
        this.itemcart});

  Products.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    storeId = json['store_id'];
    itemName = json['item_name'];
    itemDescription = json['item_description'];
    picture = json['picture'];
    storeCategoryId = json['store_category_id'];
    isVeg = json['is_veg'];
    itemPrice = json['item_price'];
    itemDiscount = json['item_discount'];
    itemDiscountType = json['item_discount_type'];
    isAddon = json['is_addon'];
    isStock = json['is_stock'];
    qty = json['qty'];
    status = json['status'];
    isBanner = json['is_banner'];
    reviewCount = json['review_count'];
    avgRating = json['avg_rating'];
    offer = json['offer'];
    productOffer = json['product_offer'];
    outofstock = json['outofstock'];
    hidePrice = json['hide_price'];
    if (json['itemsaddon'] != null) {
      itemsaddon = [];
      json['itemsaddon'].forEach((v) {
        itemsaddon!.add(new Itemsaddon.fromJson(v));
      });
    }
    if (json['itemcart'] != null) {
      itemcart = [];
      json['itemcart'].forEach((v) {
        itemcart!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['store_id'] = this.storeId;
    data['item_name'] = this.itemName;
    data['item_description'] = this.itemDescription;
    data['picture'] = this.picture;
    data['store_category_id'] = this.storeCategoryId;
    data['is_veg'] = this.isVeg;
    data['item_price'] = this.itemPrice;
    data['item_discount'] = this.itemDiscount;
    data['item_discount_type'] = this.itemDiscountType;
    data['is_addon'] = this.isAddon;
    data['is_stock'] = this.isStock;
    data['qty'] = this.qty;
    data['status'] = this.status;
    data['is_banner'] = this.isBanner;
    data['review_count'] = this.reviewCount;
    data['avg_rating'] = this.avgRating;
    data['offer'] = this.offer;
    data['product_offer'] = this.productOffer;
    data['outofstock'] = this.outofstock;
    data['hide_price'] = this.hidePrice;
    if (this.itemsaddon != null) {
      data['itemsaddon'] = this.itemsaddon!.map((v) => v.toJson()).toList();
    }
    if (this.itemcart != null) {
      data['itemcart'] = this.itemcart!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}












// class ItemListModel {
//   String? statusCode;
//   String? title;
//   String? message;
//   ItemListResponseData? responseData;
//   List<dynamic>? responseNewData;
//   List<dynamic>? error;
//
//   ItemListModel(
//       {this.statusCode,
//         this.title,
//         this.message,
//         this.responseData,
//         this.responseNewData,
//         this.error});
//
//   ItemListModel.fromJson(Map<String, dynamic> json) {
//     statusCode = json['statusCode'];
//     title = json['title'];
//     message = json['message'];
//     responseData = json['responseData'] != null
//         ? new ItemListResponseData.fromJson(json['responseData'])
//         : null;
//     if (json['responseNewData'] != null) {
//       responseNewData = [];
//       json['responseNewData'].forEach((v) {
//         responseNewData!.add(v);
//       });
//     }
//     if (json['error'] != null) {
//       error = [];
//       json['error'].forEach((v) {
//         error!.add(v);
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['statusCode'] = this.statusCode;
//     data['title'] = this.title;
//     data['message'] = this.message;
//     if (this.responseData != null) {
//       data['responseData'] = this.responseData!.toJson();
//     }
//     if (this.responseNewData != null) {
//       data['responseNewData'] =
//           this.responseNewData!.map((v) => v.toJson()).toList();
//     }
//     if (this.error != null) {
//       data['error'] = this.error!.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }
//
// class ItemListResponseData {
//   dynamic  id;
//   dynamic  storeTypeId;
//   String? storeMultiTypeId;
//   String? storeName;
//   String? currencySymbol;
//   String? storeLocation;
//   double? latitude;
//   double? longitude;
//   String? gallery;
//   String? picture;
//   String? offerMinAmount;
//   String? estimatedDeliveryTime;
//   dynamic  freeDelivery;
//   String? isVeg;
//   dynamic rating;
//   dynamic  offerPercent;
//   dynamic walletBalance;
//   dynamic  hidePrice;
//   dynamic  totalstorecart;
//   String? shopstatus;
//   dynamic  usercart;
//   dynamic  totalcartprice;
//   List<AllCategories>? allCategories;
//   List<AllCategories>? categories;
//   Storetype? storetype;
//   List<dynamic>? productsBanners;
//   List<Products>? products;
//
//   ItemListResponseData(
//       {this.id,
//         this.storeTypeId,
//         this.storeMultiTypeId,
//         this.storeName,
//         this.currencySymbol,
//         this.storeLocation,
//         this.latitude,
//         this.longitude,
//         this.gallery,
//         this.picture,
//         this.offerMinAmount,
//         this.estimatedDeliveryTime,
//         this.freeDelivery,
//         this.isVeg,
//         this.rating,
//         this.offerPercent,
//         this.walletBalance,
//         this.hidePrice,
//         this.totalstorecart,
//         this.shopstatus,
//         this.usercart,
//         this.totalcartprice,
//         this.allCategories,
//         this.categories,
//         this.storetype,
//         this.productsBanners,
//         this.products});
//
//   ItemListResponseData.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     storeTypeId = json['store_type_id'];
//     storeMultiTypeId = json['store_multi_type_id'];
//     storeName = json['store_name'];
//     currencySymbol = json['currency_symbol'];
//     storeLocation = json['store_location'];
//     latitude = json['latitude'];
//     longitude = json['longitude'];
//     gallery = json['gallery'];
//     picture = json['picture'];
//     offerMinAmount = json['offer_min_amount'];
//     estimatedDeliveryTime = json['estimated_delivery_time'];
//     freeDelivery = json['free_delivery'];
//     isVeg = json['is_veg'];
//     rating = json['rating'];
//     offerPercent = json['offer_percent'];
//     walletBalance = json['wallet_balance'];
//     hidePrice = json['hide_price'];
//     totalstorecart = json['totalstorecart'];
//     shopstatus = json['shopstatus'];
//     usercart = json['usercart'];
//     totalcartprice = json['totalcartprice'];
//     if (json['all_categories'] != null) {
//       allCategories = [];
//       json['all_categories'].forEach((v) {
//         allCategories!.add(new AllCategories.fromJson(v));
//       });
//     }
//     if (json['categories'] != null) {
//       categories = [];
//       json['categories'].forEach((v) {
//         categories!.add(new AllCategories.fromJson(v));
//       });
//     }
//     storetype = json['storetype'] != null
//         ? new Storetype.fromJson(json['storetype'])
//         : null;
//     if (json['products_banners'] != null) {
//       productsBanners = [];
//       json['products_banners'].forEach((v) {
//         productsBanners!.add(v);
//       });
//     }
//     if (json['products'] != null) {
//       products =[];
//       json['products'].forEach((v) {
//         products!.add(new Products.fromJson(v));
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['store_type_id'] = this.storeTypeId;
//     data['store_multi_type_id'] = this.storeMultiTypeId;
//     data['store_name'] = this.storeName;
//     data['currency_symbol'] = this.currencySymbol;
//     data['store_location'] = this.storeLocation;
//     data['latitude'] = this.latitude;
//     data['longitude'] = this.longitude;
//     data['gallery'] = this.gallery;
//     data['picture'] = this.picture;
//     data['offer_min_amount'] = this.offerMinAmount;
//     data['estimated_delivery_time'] = this.estimatedDeliveryTime;
//     data['free_delivery'] = this.freeDelivery;
//     data['is_veg'] = this.isVeg;
//     data['rating'] = this.rating;
//     data['offer_percent'] = this.offerPercent;
//     data['wallet_balance'] = this.walletBalance;
//     data['hide_price'] = this.hidePrice;
//     data['totalstorecart'] = this.totalstorecart;
//     data['shopstatus'] = this.shopstatus;
//     data['usercart'] = this.usercart;
//     data['totalcartprice'] = this.totalcartprice;
//     if (this.allCategories != null) {
//       data['all_categories'] =
//           this.allCategories!.map((v) => v.toJson()).toList();
//     }
//     if (this.categories != null) {
//       data['categories'] = this.categories!.map((v) => v.toJson()).toList();
//     }
//     if (this.storetype != null) {
//       data['storetype'] = this.storetype!.toJson();
//     }
//     if (this.productsBanners != null) {
//       data['products_banners'] =
//           this.productsBanners!.map((v) => v.toJson()).toList();
//     }
//     if (this.products != null) {
//       data['products'] = this.products!.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }
//
// class AllCategories {
//   dynamic  id;
//   dynamic  storeId;
//   String? storeCategoryName;
//   String? storeCategoryDescription;
//   String? picture;
//   dynamic  storeCategoryStatus;
//
//   AllCategories(
//       {this.id,
//         this.storeId,
//         this.storeCategoryName,
//         this.storeCategoryDescription,
//         this.picture,
//         this.storeCategoryStatus});
//
//   AllCategories.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     storeId = json['store_id'];
//     storeCategoryName = json['store_category_name'];
//     storeCategoryDescription = json['store_category_description'];
//     picture = json['picture'];
//     storeCategoryStatus = json['store_category_status'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['store_id'] = this.storeId;
//     data['store_category_name'] = this.storeCategoryName;
//     data['store_category_description'] = this.storeCategoryDescription;
//     data['picture'] = this.picture;
//     data['store_category_status'] = this.storeCategoryStatus;
//     return data;
//   }
// }
//
// class Storetype {
//   dynamic  id;
//   String? name;
//   String? category;
//   String? picture;
//   dynamic  status;
//   String? itemsStatus;
//   String? addonsStatus;
//   String? keywords;
//   dynamic  visibility;
//
//   Storetype(
//       {this.id,
//         this.name,
//         this.category,
//         this.picture,
//         this.status,
//         this.itemsStatus,
//         this.addonsStatus,
//         this.keywords,
//         this.visibility});
//
//   Storetype.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     name = json['name'];
//     category = json['category'];
//     picture = json['picture'];
//     status = json['status'];
//     itemsStatus = json['items_status'];
//     addonsStatus = json['addons_status'];
//     keywords = json['keywords'];
//     visibility = json['visibility'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['name'] = this.name;
//     data['category'] = this.category;
//     data['picture'] = this.picture;
//     data['status'] = this.status;
//     data['items_status'] = this.itemsStatus;
//     data['addons_status'] = this.addonsStatus;
//     data['keywords'] = this.keywords;
//     data['visibility'] = this.visibility;
//     return data;
//   }
// }
//
// class Products {
//   dynamic  id;
//   dynamic  storeId;
//   String? itemName;
//   String? itemDescription;
//   String? picture;
//   dynamic  storeCategoryId;
//   String? isVeg;
//   dynamic  itemPrice;
//   dynamic  itemDiscount;
//   String? itemDiscountType;
//   dynamic  isAddon;
//   dynamic  isStock;
//   dynamic  qty;
//   dynamic  status;
//   dynamic  isBanner;
//   dynamic  reviewCount;
//   dynamic avgRating;
//   dynamic  offer;
//   dynamic  productOffer;
//   dynamic  outofstock;
//   dynamic  hidePrice;
//   List<Itemsaddon>? itemsaddon;
//   List<dynamic>? itemcart;
//
//   Products(
//       {this.id,
//         this.storeId,
//         this.itemName,
//         this.itemDescription,
//         this.picture,
//         this.storeCategoryId,
//         this.isVeg,
//         this.itemPrice,
//         this.itemDiscount,
//         this.itemDiscountType,
//         this.isAddon,
//         this.isStock,
//         this.qty,
//         this.status,
//         this.isBanner,
//         this.reviewCount,
//         this.avgRating,
//         this.offer,
//         this.productOffer,
//         this.outofstock,
//         this.hidePrice,
//         this.itemsaddon,
//         this.itemcart});
//
//   Products.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     storeId = json['store_id'];
//     itemName = json['item_name'];
//     itemDescription = json['item_description'];
//     picture = json['picture'];
//     storeCategoryId = json['store_category_id'];
//     isVeg = json['is_veg'];
//     itemPrice = json['item_price'];
//     itemDiscount = json['item_discount'];
//     itemDiscountType = json['item_discount_type'];
//     isAddon = json['is_addon'];
//     isStock = json['is_stock'];
//     qty = json['qty'];
//     status = json['status'];
//     isBanner = json['is_banner'];
//     reviewCount = json['review_count'];
//     avgRating = json['avg_rating'];
//     offer = json['offer'];
//     productOffer = json['product_offer'];
//     outofstock = json['outofstock'];
//     hidePrice = json['hide_price'];
//     if (json['itemsaddon'] != null) {
//       itemsaddon = [];
//       json['itemsaddon'].forEach((v) {
//         itemsaddon!.add(new Itemsaddon.fromJson(v));
//       });
//     }
//     if (json['itemcart'] != null) {
//       itemcart = [];
//       json['itemcart'].forEach((v) {
//         itemcart!.add(v);
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['store_id'] = this.storeId;
//     data['item_name'] = this.itemName;
//     data['item_description'] = this.itemDescription;
//     data['picture'] = this.picture;
//     data['store_category_id'] = this.storeCategoryId;
//     data['is_veg'] = this.isVeg;
//     data['item_price'] = this.itemPrice;
//     data['item_discount'] = this.itemDiscount;
//     data['item_discount_type'] = this.itemDiscountType;
//     data['is_addon'] = this.isAddon;
//     data['is_stock'] = this.isStock;
//     data['qty'] = this.qty;
//     data['status'] = this.status;
//     data['is_banner'] = this.isBanner;
//     data['review_count'] = this.reviewCount;
//     data['avg_rating'] = this.avgRating;
//     data['offer'] = this.offer;
//     data['product_offer'] = this.productOffer;
//     data['outofstock'] = this.outofstock;
//     data['hide_price'] = this.hidePrice;
//     if (this.itemsaddon != null) {
//       data['itemsaddon'] = this.itemsaddon!.map((v) => v.toJson()).toList();
//     }
//     if (this.itemcart != null) {
//       data['itemcart'] = this.itemcart!.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }
//
// class Itemsaddon {
//   dynamic  id;
//   dynamic  storeId;
//   dynamic  storeItemId;
//   dynamic  storeAddonId;
//   dynamic  price;
//   String? addonName;
//
//   Itemsaddon(
//       {this.id,
//         this.storeId,
//         this.storeItemId,
//         this.storeAddonId,
//         this.price,
//         this.addonName});
//
//   Itemsaddon.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     storeId = json['store_id'];
//     storeItemId = json['store_item_id'];
//     storeAddonId = json['store_addon_id'];
//     price = json['price'];
//     addonName = json['addon_name'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['store_id'] = this.storeId;
//     data['store_item_id'] = this.storeItemId;
//     data['store_addon_id'] = this.storeAddonId;
//     data['price'] = this.price;
//     data['addon_name'] = this.addonName;
//     return data;
//   }
// }


