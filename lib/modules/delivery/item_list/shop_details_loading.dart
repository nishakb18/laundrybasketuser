import 'dart:async';

import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ShopDetailsLoading extends StatefulWidget {
  @override
  _ShopDetailsLoadingState createState() => _ShopDetailsLoadingState();
}

class _ShopDetailsLoadingState extends State<ShopDetailsLoading> {
  Timer? _timer;
  int _start = 10; // time to popup still working window /// in seconds

  @override
  void initState() {
    startTimer();
    super.initState();
  }

  @override
  void dispose() {
    if (_timer != null) {
      _timer!.cancel();
    }
    super.dispose();
  }

  void startTimer() {
    if (_timer != null) {
      _timer!.cancel();
      _timer = null;
    } else {
      _timer = new Timer.periodic(const Duration(seconds: 1), (Timer timer) {
        setState(() {
          if (_start < 1) {
            _timer!.cancel();
          } else {
            _start = _start - 1;
          }
        });
      });
    }
  }

  // buildShopDetailShimmerItem(context) {
  //   List<Widget> list = [];
  //
  //   for (var i = 0; i < 1; i++)
  //     list.add(Container(
  //       // width: MediaQuery.of(context).size.width / 2.5,
  //       margin: EdgeInsets.symmetric(vertical: 10,),
  //       child: Column(
  //         crossAxisAlignment: CrossAxisAlignment.start,
  //         children: [
  //           Container(
  //             width: MediaQuery.of(context).size.width / 1,
  //             height: 14,
  //             decoration: BoxDecoration(
  //                 borderRadius: BorderRadius.circular(15),
  //                 color: Colors.white,
  //                 boxShadow: [
  //                   BoxShadow(
  //                     color: Color(0xffefefef),
  //                     blurRadius: 20,
  //                     spreadRadius: 3,
  //                   ),
  //                 ]),
  //           ),
  //           Container(
  //             width: MediaQuery.of(context).size.width/2.5,
  //             height: 25,
  //             margin: EdgeInsets.only(right: 30,top: 15),
  //             decoration: BoxDecoration(
  //                 borderRadius: BorderRadius.circular(5),
  //                 color: Colors.white,
  //                 boxShadow: [
  //                   BoxShadow(
  //                     color: Color(0xffefefef),
  //                     blurRadius: 20,
  //                     spreadRadius: 3,
  //                   ),
  //                 ]),
  //           ),
  //           const SizedBox(height: 25,),
  //           Row(
  //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //             children: [
  //               Container(
  //                 height: 15,
  //                 width: MediaQuery.of(context).size.width/5,
  //                 // margin: EdgeInsets.only(right: 80,top: 5),
  //                 decoration: BoxDecoration(
  //                     borderRadius: BorderRadius.circular(5),
  //                     color: Colors.white,
  //                     boxShadow: [
  //                       BoxShadow(
  //                         color: Color(0xffefefef),
  //                         blurRadius: 20,
  //                         spreadRadius: 3,
  //                       ),
  //                     ]),
  //               ),
  //               Container(
  //                 height: 15,
  //                 width: MediaQuery.of(context).size.width/8,
  //                 // margin: EdgeInsets.only(right: 80,top: 5),
  //                 decoration: BoxDecoration(
  //                     borderRadius: BorderRadius.circular(5),
  //                     color: Colors.white,
  //                     boxShadow: [
  //                       BoxShadow(
  //                         color: Color(0xffefefef),
  //                         blurRadius: 20,
  //                         spreadRadius: 3,
  //                       ),
  //                     ]),
  //               ),
  //               Container(
  //                 height: 15,
  //                 width: MediaQuery.of(context).size.width/7,
  //                 // margin: EdgeInsets.only(top: 5),
  //                 decoration: BoxDecoration(
  //                     borderRadius: BorderRadius.circular(5),
  //                     color: Colors.white,
  //                     boxShadow: [
  //                       BoxShadow(
  //                         color: Color(0xffefefef),
  //                         blurRadius: 20,
  //                         spreadRadius: 3,
  //                       ),
  //                     ]),
  //               )
  //             ],
  //           ),
  //         ],
  //       ),
  //     ));
  //
  //   return list;
  // }

  buildStillLoadingWidget() {
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(15)
      ),

      width: MediaQuery.of(context).size.width,
      child: ListTile(
        title: Text("Please wait..."),
        subtitle: Text(
            "The network seems to be too busy. We suggest you to wait for some more time."),
        leading: CircularProgressIndicator.adaptive(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height/6,
        child: Stack(
          fit: StackFit.expand,
          children: [
            Shimmer.fromColors(
                baseColor: Colors.grey[300]!,
                highlightColor: Colors.grey[100]!,
                child: Container(
                  // width: MediaQuery.of(context).size.width / 2.5,
                  margin: EdgeInsets.symmetric(vertical: 10,),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width / 1,
                        height: 14,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Color(0xffefefef),
                                blurRadius: 20,
                                spreadRadius: 3,
                              ),
                            ]),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width/2.5,
                        height: 25,
                        margin: EdgeInsets.only(right: 30,top: 15),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Color(0xffefefef),
                                blurRadius: 20,
                                spreadRadius: 3,
                              ),
                            ]),
                      ),
                      const SizedBox(height: 25,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            height: 15,
                            width: MediaQuery.of(context).size.width/5,
                            // margin: EdgeInsets.only(right: 80,top: 5),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Color(0xffefefef),
                                    blurRadius: 20,
                                    spreadRadius: 3,
                                  ),
                                ]),
                          ),
                          Container(
                            height: 15,
                            width: MediaQuery.of(context).size.width/8,
                            // margin: EdgeInsets.only(right: 80,top: 5),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Color(0xffefefef),
                                    blurRadius: 20,
                                    spreadRadius: 3,
                                  ),
                                ]),
                          ),
                          Container(
                            height: 15,
                            width: MediaQuery.of(context).size.width/7,
                            // margin: EdgeInsets.only(top: 5),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Color(0xffefefef),
                                    blurRadius: 20,
                                    spreadRadius: 3,
                                  ),
                                ]),
                          )
                        ],
                      ),
                    ],
                  ),
                ) ),
            // Positioned(
            //   left: 20,
            //   right: 20,
            //   top: 50,
            //   child:  _start == 0 ? buildStillLoadingWidget() : Container(),
            // )
          ],
        )
    );
  }
}
