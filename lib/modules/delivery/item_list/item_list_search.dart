

import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/helpers.dart';
import 'package:app24_user_app/modules/delivery/item_list/item_list_loading.dart';
import 'package:app24_user_app/modules/delivery/item_list/item_list_model.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/widgets/bottomsheets/addons_bottomsheet.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class ItemListSearchPage extends StatefulWidget {
  final String? storeTypeID;
  final String? shopName;
  final String? shopID;
  final String? itemTypeID;
  const ItemListSearchPage({Key? key, this.storeTypeID, this.shopName, this.itemTypeID, this.shopID}) : super(key: key);

  @override
  _ItemListSearchPageState createState() => _ItemListSearchPageState();
}

class _ItemListSearchPageState extends State<ItemListSearchPage> {
  String? searchText = '';
  List<Products>? itemListModel = new List.empty(growable: true);
  int? categoryID;
  // List<Carts>? cartTileList = new List.empty(growable: true);


  reloadData({required BuildContext context, bool? init}) {
    return context
        .read(itemListNotifierProvider.notifier)
        .getItemList(storeID: widget.shopID.toString());
    // context.read(itemListNotifierProvider.notifier).cartID;
  }



  List<Widget> getItemTiles(
      context,
      /*ItemListResponseData listResponseData,*/
      /*CartListResponseData carts*/
      ) {
    // List<Products> itemListModel = listResponseData.products!;
    List<Widget> _widgetItem = new List.empty(growable: true);
    // List<Carts> cartTileList = [];
    for (var i = 0; i < itemListModel!.length; i++) {
      showLog(itemListModel![i].storeCategoryId.toString());

      if (itemListModel![i]
          .itemName!
          .toLowerCase()
          .contains(searchText!.toLowerCase()))
// itemListModel![i].storeCategoryId == 0 ?
        if (categoryID == 0 || categoryID == null) {
          _widgetItem.add(Container(
            // padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
            width: MediaQuery.of(context).size.width / 2.4,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
            ),
            child: Material(
              color: Colors.transparent,
              child: Container(
                padding: EdgeInsets.all(3),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        offset: const Offset(0, 3.0),
                        color: Color(0xffefefef),
                        blurRadius: 10.0,
                        spreadRadius: 3.0,
                      )
                    ]),
                // padding: EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding:
                      const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(20.0),
                        child: CachedNetworkImage(
                          imageUrl: itemListModel![i].picture ?? "",
                          width: MediaQuery.of(context).size.width / 2.5,
                          height: MediaQuery.of(context).size.height / 8,
                          fit: BoxFit.cover,
                          placeholder: (context, url) => Image.asset(
                            App24UserAppImages.placeHolderImage,
                            width: MediaQuery.of(context).size.width / 2.5,
                            height: MediaQuery.of(context).size.height / 8,
                            fit: BoxFit.cover,
                          ),
                          errorWidget: (context, url, error) => Image.asset(
                            App24UserAppImages.placeHolderImage,
                            width: MediaQuery.of(context).size.width / 2.5,
                            height: MediaQuery.of(context).size.height / 8,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                    // const SizedBox(
                    //   height: 10,
                    // ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          child: Text(
                            itemListModel![i].itemName!,
                            style: TextStyle(
                              fontSize: MediaQuery.of(context).size.width / 24,
                              fontWeight: FontWeight.bold,
                            ),
                            // textAlign: TextAlign.center,
                            // maxLines: 2,
                            overflow: TextOverflow.clip,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          child: Text(
                            itemListModel![i].itemDescription!,
                            style: TextStyle(
                              fontSize: 11,
                              fontWeight: FontWeight.bold,
                              color: Color(0xff83889A),
                            ),
                          ),
                        ),
                        Row(
                          // crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            if (widget.storeTypeID != "1" &&
                                widget.storeTypeID != "4")
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10),
                                      child: Text(
                                        "AED " +
                                            itemListModel![i]
                                                .productOffer
                                                .toString(),
                                        style: TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold,
                                            color: App24Colors
                                                .greenOrderEssentialThemeColor),
                                        overflow: TextOverflow.clip,
                                      ),
                                    ),
                                    if (itemListModel![i].itemDiscount > 0)
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 10),
                                        child: Text(
                                          "AED " +
                                              itemListModel![i]
                                                  .itemPrice
                                                  .toString(),
                                          style: TextStyle(
                                              decoration:
                                              TextDecoration.lineThrough,
                                              fontSize: 12,
                                              fontWeight: FontWeight.bold,
                                              color: App24Colors.lightTextColor),
                                          overflow: TextOverflow.clip,
                                          //textAlign: TextAlign.right,
                                        ),
                                      ),
                                  ],
                                ),
                              ),
                            Padding(
                                padding: const EdgeInsets.only(right: 10),
                                child: CupertinoButton(
                                    borderRadius: BorderRadius.circular(20),
                                    padding: EdgeInsets.symmetric(horizontal: 15),
                                    child: Text(
                                      "add",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w600,
                                          fontSize:
                                          MediaQuery.of(context).size.width /
                                              27),
                                    ),
                                    color:
                                    App24Colors.greenOrderEssentialThemeColor,
                                    // shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),

                                    onPressed: () {
                                      Helpers().getCommonBottomSheet(
                                          context: context,
                                          content: AddonsBottomSheet(
                                            itemID:
                                            itemListModel![i].id.toString(),
                                            product: itemListModel![i],
                                            shopName: widget.shopName,
                                            shopID: widget.itemTypeID,
                                            context: context,
                                          ),
                                          title: "Extras");
                                    })),
                          ],
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ),
            ),
          ));
        } else {
          if (itemListModel![i].storeCategoryId == categoryID)
            _widgetItem.add(Container(
              // padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
              width: MediaQuery.of(context).size.width / 2.4,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
              ),
              child: Material(
                color: Colors.transparent,
                child: Container(
                  padding: EdgeInsets.all(3),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          offset: const Offset(0, 3.0),
                          color: Color(0xffefefef),
                          blurRadius: 10.0,
                          spreadRadius: 3.0,
                        )
                      ]),
                  // padding: EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 5, vertical: 5),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(20.0),
                          child: CachedNetworkImage(
                            imageUrl: itemListModel![i].picture ?? "",
                            width: MediaQuery.of(context).size.width / 2.5,
                            height: MediaQuery.of(context).size.height / 8,
                            fit: BoxFit.cover,
                            placeholder: (context, url) => Image.asset(
                              App24UserAppImages.placeHolderImage,
                              width: MediaQuery.of(context).size.width / 2.5,
                              height: MediaQuery.of(context).size.height / 8,
                              fit: BoxFit.cover,
                            ),
                            errorWidget: (context, url, error) => Image.asset(
                              App24UserAppImages.placeHolderImage,
                              width: MediaQuery.of(context).size.width / 2.5,
                              height: MediaQuery.of(context).size.height / 8,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
                      // const SizedBox(
                      //   height: 10,
                      // ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Text(
                              itemListModel![i].itemName!,
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                              ),
                              // textAlign: TextAlign.center,
                              maxLines: 2,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Text(
                              itemListModel![i].itemDescription!,
                              style: TextStyle(
                                fontSize: 11,
                                fontWeight: FontWeight.bold,
                                color: Color(0xff83889A),
                              ),
                            ),
                          ),
                          Row(
                            // crossAxisAlignment: CrossAxisAlignment.end,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              if (widget.storeTypeID != "1" &&
                                  widget.storeTypeID != "4")
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10),
                                    child: Text(
                                      "AED " +
                                          itemListModel![i].itemPrice.toString(),
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold,
                                          color: App24Colors
                                              .greenOrderEssentialThemeColor),
                                    ),
                                  ),
                                ),
                              Padding(
                                  padding: const EdgeInsets.only(right: 10),
                                  child: CupertinoButton(
                                      borderRadius: BorderRadius.circular(20),
                                      padding:
                                      EdgeInsets.symmetric(horizontal: 15),
                                      child: Text(
                                        "add",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w600,
                                            fontSize: MediaQuery.of(context)
                                                .size
                                                .width /
                                                27),
                                      ),
                                      color: App24Colors
                                          .greenOrderEssentialThemeColor,
                                      // shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),

                                      onPressed: () {
                                        Helpers().getCommonBottomSheet(
                                            context: context,
                                            content: AddonsBottomSheet(
                                              itemID:
                                              itemListModel![i].id.toString(),
                                              product: itemListModel![i],
                                              shopName: widget.shopName,
                                              shopID: widget.itemTypeID,
                                              context: context,
                                            ),
                                            title: "Extras");
                                      })),
                            ],
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
              ),
            ));
        }
    }
    if (_widgetItem.isEmpty) {
      _widgetItem.add(Padding(
          padding: const EdgeInsets.only(left: 1, top: 10),
          child: Container(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              width: MediaQuery.of(context).size.width / 2.3,
              /*decoration: BoxDecoration(
    borderRadius: BorderRadius.circular(15),
    ),*/
              child: Text("No item found!"))));
    }
    return _widgetItem;
  }


  Widget buildSearchBar() {
    return Container(
      height: 60,
      child: TextField(

        onChanged: (String? s) {
          setState(() {
            searchText = s;
          });
        },
        style: TextStyle(fontWeight: FontWeight.w300, fontSize: 16),
        // keyboardType: TextInputType.number,
        decoration: InputDecoration(
          // contentPadding: EdgeInsets.symmetric(
          //              horizontal: 5, vertical: top > 80 ? 9 : 10),
          prefixIcon: Icon(
            Icons.search,
            color: App24Colors.greenOrderEssentialThemeColor,
            size: 21,
          ),
          hintText: "Search for items",
          hintStyle: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.bold,
              color: App24Colors.greenOrderEssentialThemeColor),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(13)
                ,
            borderSide: BorderSide(
              width: 0,
              style: BorderStyle.none,
            ),
          ),
          fillColor: Color(0xffE3FFEE),
          filled: true,
          contentPadding: EdgeInsets.symmetric(
              vertical: 0, horizontal: 10),
        ),
      ),
    );
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
// toolbarHeight: 70,
        backgroundColor: Colors.white,
        automaticallyImplyLeading: false,
        // primary: false,
        title: buildSearchBar(),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
            child: Column(
              children: [
                // Padding(
                //   padding: const EdgeInsets.symmetric(vertical: 20),
                //   child: buildSearchBar(),
                // ),
                Consumer(builder: (context, watch, child) {
                  final state = watch(itemListNotifierProvider);
                  // final state1 = watch(cartNotifierProvider);
                  if (state.isLoading) {
                    return ItemListLoading();
                  } else if (state.isError) {
                    return LoadingError(
                      onPressed: (res) {
                        reloadData(init: true, context: res);
                      },
                      message: state.errorMessage.toString(),
                    );
                  } else {
                    itemListModel =
                        state.response.responseData.products;

                    // cartTileList = state1.response.responseData.carts;
                    return Wrap(
                      runSpacing: 10,
                      spacing: 10,
                      alignment: WrapAlignment.center,
                      runAlignment: WrapAlignment.center,
                      children: getItemTiles(
                        context, /*state1.response.responseData.carts*/
                      ),
                    );
                  }
                })
              ],
            ),
          ),
        ),
      ),
    );
  }
}
