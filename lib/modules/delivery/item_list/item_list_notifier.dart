import 'package:app24_user_app/constants/api_path.dart';
import 'package:app24_user_app/constants/shared_preferences_path.dart';
import 'package:app24_user_app/modules/delivery/cart/cart_model.dart';
import 'package:app24_user_app/modules/delivery/item_list/item_types_model.dart';
import 'package:app24_user_app/modules/delivery/item_list/service_item_list_model.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/api/api_response_state2.dart';
import 'package:app24_user_app/utils/services/api/api_services.dart';
import 'package:app24_user_app/widgets/alertboxs/cart_replace_alert_box.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ItemListNotifier extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  ItemListNotifier(this._apiRepository) : super(ResponseState2.initial());
int? cartID;

  Future<void> getItemList(
      {bool init = true,required String storeID}) async {
    try {
      if (init) state = state.copyWith(isLoading: true);
      final order = await _apiRepository.fetchItemList(storeID);
      state = state.copyWith(response: order, isLoading: false, isError: false);
    } catch (e) {
      print(e.toString());
      state = state.copyWith(
          errorMessage: e.toString(), isError: true, isLoading: false);
    }
  }

/*  Future<void> replaceCart1(
      {int? storeID,context}) async {
    final cartList = await _apiRepository.fetchCartList();
    cartID = cartList.responseData!.carts![0].storeId!;
    if (storeID != cartID) {
      // showDialog(context: context, builder: (_) {return CartReplaceAlertBox();});
      // cartID = storeID;
      print("store ID  from notifier" +storeID.toString());
      print("cartID from notifier" +cartID.toString());
// CartReplaceAlertBox();
    }
  }*/


}

class ShopDetailsNotifier extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  ShopDetailsNotifier(this._apiRepository) : super(ResponseState2(isLoading: true));
  int? cartID;

  Future<void> getShopDetails(
      {bool init = true,required String storeID}) async {
    print("getShopDetails");
    try {
      if (init) state = state.copyWith(isLoading: true);
      final order = await _apiRepository.fetchShopDetails(storeID);
      state = state.copyWith(response: order, isLoading: false, isError: false);
    } catch (e) {
      print(e.toString());
      state = state.copyWith(
          errorMessage: e.toString(), isError: true, isLoading: false);
    }
  }
}

class ServiceItemsListNotifier extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  ServiceItemsListNotifier(this._apiRepository) : super(ResponseState2(isLoading: true));
  List getServiceIdList = [];
  List getItemIdList = [];
  List getQuantityList = [];

  addItems({required int itemId,required int quantity,required int serviceId}){
    int existingIndex = getItemIdList.indexOf(itemId);
    if(quantity == 0){
      // If the quantity is 0, remove the corresponding elements from all lists
      if (existingIndex != -1) {
        getServiceIdList.removeAt(existingIndex);
        getItemIdList.removeAt(existingIndex);
        getQuantityList.removeAt(existingIndex);
        print('Removed entry');
      } else {
        print('Item not found to remove');
      }
    } else{
      if (existingIndex != -1) {
        // If the newGetItemId already exists, update the corresponding getServiceId and getQuantity at the same index
        getServiceIdList[existingIndex] = serviceId;
        getQuantityList[existingIndex] = quantity;
        print('Updated existing entry');
      }
      else {
        // If newGetItemId doesn't exist, add it to getItemId list and append newGetServiceId and newGetQuantity to their respective lists
        getItemIdList.add(itemId);
        getServiceIdList.add(serviceId);
        getQuantityList.add(quantity);
        print('Added new entry');
      }
    }

  }

  Future<void> getServiceItems(
      {bool init = true}) async {
    try {
      if (init) state = state.copyWith(isLoading: true);
      var service = await _apiRepository.fetchServiceItemsList();
      state = state.copyWith(response: service, isLoading: false, isError: false);
    } catch (e) {
      print(e.toString());
      state = state.copyWith(
          errorMessage: e.toString(), isError: true, isLoading: false);
    }
  }

}

class ServiceItemsList1Notifier extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  ServiceItemsList1Notifier(this._apiRepository) : super(ResponseState2(isLoading: true));
  List<Datum>? originalList = [];
  List<Datum>? filteredList = [];


  List<Datum>? listId11 = [];

  void filterFun(String? query) {
    if(query == null || query == '' || query.isEmpty){
      filteredList = originalList;
    }
     else if (query.isNotEmpty) {
        filteredList = originalList!
            .where((item) =>
            item.itemName!.toLowerCase().contains(query.toLowerCase()))
            .toList();
      }
      else {
        filteredList = [];
      }
     // print("filteredList :: ${filteredList?[0].cartQuantity}");
      state = state.copyWith(response: filteredList, isLoading: false, isError: false);
  }

  Future<void> getServiceItemsList1(
      {bool init = true,
      itemId,
      shopId}) async {
    try {
      if (init) state = state.copyWith(isLoading: true);
      originalList?.clear();
      filteredList?.clear();
      var service = await _apiRepository.fetchServiceItemsList1(
        itemId: itemId,
        shopId: shopId,
      );
      originalList = service.responseData?.data;
      filteredList = originalList;
      // print("orginalList ::::: # ${originalList.toString()}");
      state = state.copyWith(response: service, isLoading: false, isError: false);
    } catch (e) {
      print(e.toString());
      state = state.copyWith(
          errorMessage: e.toString(), isError: true, isLoading: false);
    }
  }

}

class NewAddToCartNotifier extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  NewAddToCartNotifier(this._apiRepository) : super(ResponseState2.initial());
  List<int>? cartIdList;
  int? cartID;

  Future<void> getNewAddToCart(
      {bool init = true,
        FormData? body,
        Function()? successRes,
      required BuildContext context}) async {
    var prefs = await SharedPreferences.getInstance();
    try {
      if (init) state = state.copyWith(isLoading: true);
      CartListModel service = await _apiRepository.fetchNewAddToCart(body: body);
      if(service.statusCode == "200"){
        successRes!();
      }
      print("statusCode service:: ${service.statusCode}\n DiID getQuantityList ${context.read(serviceListNotifierProvider.notifier).getQuantityList} \n cartIdList :: $cartIdList ");
      cartID = service.responseData?.carts?[0].storeId;
        prefs.setInt(SharedPreferencesPath.cartStoreId, cartID ?? 0);
      print("cartID wetafafe :: cartID $cartID:: ${service.responseData?.carts?[0].storeId}");
      state = state.copyWith(response: service, isLoading: false, isError: false);
    } catch (e) {
      print(e.toString());
      state = state.copyWith(
          errorMessage: e.toString(), isError: true, isLoading: false);
    }
  }

}