import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/constants/shared_preferences_path.dart';
import 'package:app24_user_app/helpers.dart';
import 'package:app24_user_app/modules/delivery/cart/cart.dart';
import 'package:app24_user_app/modules/delivery/cart/cart_model.dart';
import 'package:app24_user_app/modules/delivery/item_list/item_list_category_loading.dart';
import 'package:app24_user_app/modules/delivery/item_list/item_list_loading.dart';
import 'package:app24_user_app/modules/delivery/item_list/item_list_model.dart';
import 'package:app24_user_app/modules/delivery/item_list/item_list_search.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/widgets/bottomsheets/addons_bottomsheet.dart';
import 'package:app24_user_app/widgets/common_appbar.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:app24_user_app/widgets/offline_builder_widget.dart';
import 'package:app24_user_app/widgets/view_cart_overlay.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:favorite_button/favorite_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ItemList extends StatefulWidget {
  final String? itemTypeID;
  final String? storeTypeID;
  final String? shopName;
  final String? deliveryTime;
  final String? storeLocation;
  final String? promoCode;
  final String? promoCodeDetails;
  final String? distance;
  final int? shopID;
  final String? shopRating;

  const ItemList(
      {Key? key,
      this.itemTypeID,
      this.storeTypeID,
      this.shopName,
      this.deliveryTime,
      this.storeLocation,
      this.promoCode,
      this.promoCodeDetails,
      this.distance,
      this.shopID,
      this.shopRating})
      : super(key: key);

  @override
  _ItemListState createState() => _ItemListState();
}

class _ItemListState extends State<ItemList> {
  reloadData({required BuildContext context, bool? init}) {
    return context
        .read(itemListNotifierProvider.notifier)
        .getItemList(storeID: widget.shopID.toString());
    // context.read(itemListNotifierProvider.notifier).cartID;
  }

  String itemCategory = "all";
  var top = 0.0;
  var rating = 3.5;
  bool isFavourite = false;
  String? searchText = '';

  // List<Products>? itemList = [];
  List<Products>? itemListModel = new List.empty(growable: true);
  List<Carts>? cartTileList = new List.empty(growable: true);

  @override
  void initState() {
    SchedulerBinding.instance!.addPostFrameCallback((_) async {
      //API call after screen build
      cartID = context.read(cartCountNotifierProvider.notifier).cartID;
      // context.read(cartNotifierProvider.notifier).getCartList();
      var prefs = await SharedPreferences.getInstance();
      var favouriteShopId =
          prefs.getStringList(SharedPreferencesPath.favouriteRestaurants);

      context
          .read(itemListNotifierProvider.notifier)
          .getItemList(storeID: widget.itemTypeID!);

      context
          .read(cartCountNotifierProvider.notifier)
          .getCartCount(context: context);

      // if(favouriteShopId != null)
      // if(favouriteShopId.contains(widget.itemTypeID))
      //   isFavourite = true;
      //abc = context.read(itemListNotifierProvider.notifier).replaceCart(storeID: int.parse(widget.itemTypeID!));
      // context.read(cartNotifierProvider.notifier).getCartList();
      categoryID = categoryID;
      if (favouriteShopId != null) if (favouriteShopId
          .contains(widget.itemTypeID)) {
        isFavourite = true;
      } else {
        isFavourite = false;
      }
    });
    super.initState();
  }

  List<Carts>? cart;

  String? getShopName(context, ItemListResponseData shopName) {
    return shopName.storeName;
  }

  int? cartID = 0;

  onFavouriteClicked(favourite) {
    Map body = {
      "store_id": widget.itemTypeID,
      "isfavourite": favourite == true ? 1 : 0
    };
    print(widget.itemTypeID);
    context.read(favouriteShopNotifier.notifier).getFavouriteShop(body: body);
  }

/*

  onAddCartPressed(Products product) {
    Map body = {
      "qty": 1,
      // "repeat" : product.,
      "item_id": product.id.toString(),
      // "customize" :0,
    };
    showMessage(context, "Item added",false,true);
    // if(cartTileList!.length == 0)
    //   cartReplace();

    context
        .read(cartNotifierProvider.notifier)
        .addCart(context: context, itemID: product.id.toString(), body: body,init: true,itemQty: 1.toString(),itemAddons: '');

    int cartCount =
        context.read(cartCountNotifierProvider.notifier).cartCount;

    debugPrint("product store id : ${product.storeId}");
    context
        .read(cartCountNotifierProvider.notifier)
        .updateCartCount(count: cartCount + 1, storeID: product.storeId);
    cartID= context.read(cartCountNotifierProvider.notifier).cartID;

    // showCartBanner();
  }*/

  //get estimate time
  String? getEstimateTime(context, ItemListResponseData estimateTime) {
    return estimateTime.estimatedDeliveryTime;
  }

  //get store location
  String? getStoreLocation(context, ItemListResponseData storeLocation) {
    return storeLocation.storeLocation;
  }

  onCategory(int? category) {
    showLog(category.toString());
    // getItemTiles(context);
  }

  int? categoryID;
  int? productBannerLength;
  List<ProductsBanners>? productBanner;

  List<Widget> getProductBanner(context, ItemListResponseData productBanner) {
    List<ProductsBanners> category = productBanner.productsBanners!;
    List<Widget> _items = new List<Widget>.empty(growable: true);
    // setState(() {
    //   productBannerLength = category.length;
    // });
    for (var i = 0; i < category.length; i++) {
      _items.add(Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 7),
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  spreadRadius: 3,
                  blurRadius: 5,
                  color: Colors.grey.shade200,
                )
              ]),
          child: Material(
            borderRadius: BorderRadius.circular(15),
            color: Colors.transparent,
            child: InkWell(
              borderRadius: BorderRadius.circular(15),
              onTap: () {
                Helpers().getCommonBottomSheet(
                    context: context,
                    content: AddonsBottomSheet(
                      itemID: category[i].id.toString(),
                      product: itemListModel![i],
                      shopName: widget.shopName,
                      shopID: widget.itemTypeID,
                      context: context,
                    ),
                    title: "Extras");
              },
              child: Stack(children: [
                Container(
                  decoration:
                      BoxDecoration(borderRadius: BorderRadius.circular(15)),
                  padding: EdgeInsets.all(10),
                  child: Row(
                    children: [
                      ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: CachedNetworkImage(
                            imageUrl: category[i].picture.toString(),
                            width: MediaQuery.of(context).size.width / 6.5,
                            fit: BoxFit.cover,
                          )),
                      const SizedBox(
                        width: 15,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            category[i].itemName.toString(),
                            style: TextStyle(fontWeight: FontWeight.w800),
                          ),
                          Text(
                            category[i].itemDescription.toString(),
                            style: TextStyle(
                                fontSize:
                                    MediaQuery.of(context).size.width / 35),
                          ),
                          Text(
                            "AED " + category[i].productOffer.toString(),
                            style: TextStyle(fontWeight: FontWeight.w600),
                          )
                        ],
                      )
                    ],
                  ),
                ),
                Positioned(
                  // left: 20,
                  right: 0,
                  child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      decoration: BoxDecoration(
                          color: App24Colors.greenOrderEssentialThemeColor,
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(10),
                              bottomLeft: Radius.circular(10))
                          // shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(topRight: Radius.circular(10)))
                          ),
                      child: Text(
                        "COMBO",
                        style: TextStyle(
                            fontWeight: FontWeight.w600, fontSize: 12),
                      )),
                )
              ]),
            ),
          ),
        ),
      ));
    }
    return _items;
  }

  List<Widget> getCategories(context, ItemListResponseData forCategories) {
    List<AllCategories> category = forCategories.allCategories!;
    List<Widget> _items = new List<Widget>.empty(growable: true);
    for (var i = 0; i < category.length; i++) {
      _items.add(Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 2),
        child: Container(
            padding: EdgeInsets.symmetric(horizontal: 25),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25),
                color: itemCategory == category[i].storeCategoryName!
                    ? App24Colors.greenOrderEssentialThemeColor
                    : Color(0xffF6F6F6)),
            child: Material(
                color: Colors.transparent,
                child: InkWell(
                  borderRadius: BorderRadius.circular(25),
                  onTap: () {
                    setState(() {
                      categoryID = category[i].id;
                      showLog("categoryID" + categoryID.toString());
                      onCategory(category[i].id);
                      itemCategory = category[i].storeCategoryName!;
                    });
                  },
                  child: Center(
                    child: Text(
                      category[i].storeCategoryName!,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          color: itemCategory == category[i].storeCategoryName!
                              ? Colors.white
                              : App24Colors.darkTextColor),
                    ),
                  ),
                ))),
      ));
    }
    return _items;
  }

  //Tiles of Items
  List<Widget> getItemTiles(
    context,
    /*ItemListResponseData listResponseData,*/
    /*CartListResponseData carts*/
  ) {
    // List<Products> itemListModel = listResponseData.products!;
    List<Widget> _widgetItem = new List.empty(growable: true);
    // List<Carts> cartTileList = [];
    for (var i = 0; i < itemListModel!.length; i++) {
      showLog(itemListModel![i].storeCategoryId.toString());

      if (itemListModel![i]
          .itemName!
          .toLowerCase()
          .contains(searchText!.toLowerCase()))
// itemListModel![i].storeCategoryId == 0 ?
      if (categoryID == 0 || categoryID == null) {
        _widgetItem.add(Container(
          // padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
          width: MediaQuery.of(context).size.width / 2.4,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
          ),
          child: Material(
            color: Colors.transparent,
            child: Container(
              padding: EdgeInsets.all(3),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      offset: const Offset(0, 3.0),
                      color: Color(0xffefefef),
                      blurRadius: 10.0,
                      spreadRadius: 3.0,
                    )
                  ]),
              // padding: EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(20.0),
                      child: CachedNetworkImage(
                        imageUrl: itemListModel![i].picture ?? "",
                        width: MediaQuery.of(context).size.width / 2.5,
                        height: MediaQuery.of(context).size.height / 8,
                        fit: BoxFit.cover,
                        placeholder: (context, url) => Image.asset(
                          App24UserAppImages.placeHolderImage,
                          width: MediaQuery.of(context).size.width / 2.5,
                          height: MediaQuery.of(context).size.height / 8,
                          fit: BoxFit.cover,
                        ),
                        errorWidget: (context, url, error) => Image.asset(
                          App24UserAppImages.placeHolderImage,
                          width: MediaQuery.of(context).size.width / 2.5,
                          height: MediaQuery.of(context).size.height / 8,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  // const SizedBox(
                  //   height: 10,
                  // ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: Text(
                          itemListModel![i].itemName!,
                          style: TextStyle(
                            fontSize: MediaQuery.of(context).size.width / 24,
                            fontWeight: FontWeight.bold,
                          ),
                          // textAlign: TextAlign.center,
                          // maxLines: 2,
                          overflow: TextOverflow.clip,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: Text(
                          itemListModel![i].itemDescription!,
                          style: TextStyle(
                            fontSize: 11,
                            fontWeight: FontWeight.bold,
                            color: Color(0xff83889A),
                          ),
                        ),
                      ),
                      Row(
                        // crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          if (widget.storeTypeID != "1" &&
                              widget.storeTypeID != "4")
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10),
                                    child: Text(
                                      "AED " +
                                          itemListModel![i]
                                              .productOffer
                                              .toString(),
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold,
                                          color: App24Colors
                                              .greenOrderEssentialThemeColor),
                                      overflow: TextOverflow.clip,
                                    ),
                                  ),
                                  if (itemListModel![i].itemDiscount > 0)
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10),
                                      child: Text(
                                        "AED " +
                                            itemListModel![i]
                                                .itemPrice
                                                .toString(),
                                        style: TextStyle(
                                            decoration:
                                                TextDecoration.lineThrough,
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold,
                                            color: App24Colors.lightTextColor),
                                        overflow: TextOverflow.clip,
                                        //textAlign: TextAlign.right,
                                      ),
                                    ),
                                ],
                              ),
                            ),
                          Padding(
                              padding: const EdgeInsets.only(right: 10),
                              child: CupertinoButton(
                                  borderRadius: BorderRadius.circular(20),
                                  padding: EdgeInsets.symmetric(horizontal: 15),
                                  child: Text(
                                    "add",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600,
                                        fontSize:
                                            MediaQuery.of(context).size.width /
                                                27),
                                  ),
                                  color:
                                      App24Colors.greenOrderEssentialThemeColor,
                                  // shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),

                                  onPressed: () {
                                    Helpers().getCommonBottomSheet(
                                        context: context,
                                        content: AddonsBottomSheet(
                                          itemID:
                                              itemListModel![i].id.toString(),
                                          product: itemListModel![i],
                                          shopName: widget.shopName,
                                          shopID: widget.itemTypeID,
                                          context: context,
                                        ),
                                        title: "Extras");
                                  })),
                        ],
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
          ),
        ));
      } else {
        if (itemListModel![i].storeCategoryId == categoryID)
          _widgetItem.add(Container(
            // padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
            width: MediaQuery.of(context).size.width / 2.4,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
            ),
            child: Material(
              color: Colors.transparent,
              child: Container(
                padding: EdgeInsets.all(3),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        offset: const Offset(0, 3.0),
                        color: Color(0xffefefef),
                        blurRadius: 10.0,
                        spreadRadius: 3.0,
                      )
                    ]),
                // padding: EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 5, vertical: 5),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(20.0),
                        child: CachedNetworkImage(
                          imageUrl: itemListModel![i].picture ?? "",
                          width: MediaQuery.of(context).size.width / 2.5,
                          height: MediaQuery.of(context).size.height / 8,
                          fit: BoxFit.cover,
                          placeholder: (context, url) => Image.asset(
                            App24UserAppImages.placeHolderImage,
                            width: MediaQuery.of(context).size.width / 2.5,
                            height: MediaQuery.of(context).size.height / 8,
                            fit: BoxFit.cover,
                          ),
                          errorWidget: (context, url, error) => Image.asset(
                            App24UserAppImages.placeHolderImage,
                            width: MediaQuery.of(context).size.width / 2.5,
                            height: MediaQuery.of(context).size.height / 8,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                    // const SizedBox(
                    //   height: 10,
                    // ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          child: Text(
                            itemListModel![i].itemName!,
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                            // textAlign: TextAlign.center,
                            maxLines: 2,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          child: Text(
                            itemListModel![i].itemDescription!,
                            style: TextStyle(
                              fontSize: 11,
                              fontWeight: FontWeight.bold,
                              color: Color(0xff83889A),
                            ),
                          ),
                        ),
                        Row(
                          // crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            if (widget.storeTypeID != "1" &&
                                widget.storeTypeID != "4")
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 10),
                                  child: Text(
                                    "AED " +
                                        itemListModel![i].itemPrice.toString(),
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                        color: App24Colors
                                            .greenOrderEssentialThemeColor),
                                  ),
                                ),
                              ),
                            Padding(
                                padding: const EdgeInsets.only(right: 10),
                                child: CupertinoButton(
                                    borderRadius: BorderRadius.circular(20),
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 15),
                                    child: Text(
                                      "add",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w600,
                                          fontSize: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              27),
                                    ),
                                    color: App24Colors
                                        .greenOrderEssentialThemeColor,
                                    // shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),

                                    onPressed: () {
                                      Helpers().getCommonBottomSheet(
                                          context: context,
                                          content: AddonsBottomSheet(
                                            itemID:
                                                itemListModel![i].id.toString(),
                                            product: itemListModel![i],
                                            shopName: widget.shopName,
                                            shopID: widget.itemTypeID,
                                            context: context,
                                          ),
                                          title: "Extras");
                                    })),
                          ],
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ),
            ),
          ));
      }
    }
    if (_widgetItem.isEmpty) {
      _widgetItem.add(Padding(
          padding: const EdgeInsets.only(left: 1, top: 10),
          child: Container(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              width: MediaQuery.of(context).size.width / 2.3,
              /*decoration: BoxDecoration(
    borderRadius: BorderRadius.circular(15),
    ),*/
              child: Text("No item found!"))));
    }
    return _widgetItem;
  }

  Widget buildSearchBar() {
    return Container(
      height: top > 80 ? 40 : 50,
      child: TextField(
        readOnly: true,
        onTap:() {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => ItemListSearchPage()));
        },
        // onChanged: (String? s) {
        //   setState(() {
        //     searchText = s;
        //   });
        // },
        style: TextStyle(fontWeight: FontWeight.w300, fontSize: 12),
        // keyboardType: TextInputType.number,
        decoration: InputDecoration(
          // contentPadding: EdgeInsets.symmetric(
          //              horizontal: 5, vertical: top > 80 ? 9 : 10),
          prefixIcon: Icon(
            Icons.search,
            color: App24Colors.greenOrderEssentialThemeColor,
            size: top > 80 ? 18 : 24,
          ),
          hintText: "Search for items",
          hintStyle: TextStyle(
              fontSize: top > 80 ? 12 : 16,
              fontWeight: FontWeight.bold,
              color: App24Colors.greenOrderEssentialThemeColor),
          border: OutlineInputBorder(
            borderRadius: top > 80
                ? BorderRadius.circular(13)
                : BorderRadius.circular(15),
            borderSide: BorderSide(
              width: 0,
              style: BorderStyle.none,
            ),
          ),
          fillColor: Color(0xffE3FFEE),
          filled: true,
          contentPadding: EdgeInsets.symmetric(
              vertical: top > 80 ? 0 : 0, horizontal: top > 80 ? 5 : 10),
        ),
      ),
    );
  }

  // showCartBanner(){
  //   ScaffoldMessenger.of(context).showMaterialBanner(
  //
  //       MaterialBanner(
  //         forceActionsBelow: true,
  //         content: const Text('This is Flutter 2.5 Material Banner'),
  //         leading: const Icon(Icons.info),
  //         backgroundColor: Colors.yellow,
  //         actions: [
  //           TextButton(onPressed: (){}, child: const Text('Some Action')),
  //           TextButton(onPressed: (){
  //             ScaffoldMessenger.of(context).hideCurrentMaterialBanner();
  //           }, child: const Text('Dismiss')),
  //         ],
  //       )
  //   );
  // }

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: OfflineBuilderWidget(
        SafeArea(
          child: RefreshIndicator(
            onRefresh: () {
              return reloadData(init: false, context: context) ??
                  false as Future<void>;
            },
            child: Container(
              child: Column(
                children: [
                  CustomAppBar(
                    title: widget.shopName.toString(),
                    hideCartIcon: "true",
                    // getShopName(context, state.response.responseData) ??
                    //     "",
                    // actions: InkWell(
                    //     borderRadius: BorderRadius.circular(15),
                    //     onTap: () {
                    //       Navigator.push(
                    //           context,
                    //           MaterialPageRoute(
                    //               builder: (context) => Cart(
                    //                     storeTypeID: widget.storeTypeID,
                    //                   ) /*TrackOrder()*/));
                    //     },
                    //     child: Padding(
                    //         padding: const EdgeInsets.only(right: 8, left: 8),
                    //         child: cartIcon(context:context,fromPage:""))),
                  ),
                  Expanded(
                      child: CustomScrollView(
                    slivers: [
                      SliverAppBar(
                          automaticallyImplyLeading: false,
                          backgroundColor: Colors.white,
                          pinned: true,
                          toolbarHeight: 0,
                          collapsedHeight: 60,
                          expandedHeight: productBannerLength == 0 ? 260 : 270,
                          flexibleSpace: LayoutBuilder(builder:
                              (BuildContext context,
                                  BoxConstraints constraints) {
                            // print('constraints=' + constraints.toString());
                            top = constraints.biggest.height;
                            return FlexibleSpaceBar(
                              titlePadding: EdgeInsetsDirectional.zero,
                              title: Padding(
                                padding: EdgeInsets.symmetric(horizontal: 17),
                                child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Row(
                                        children: [
                                          Expanded(child:
                                          buildSearchBar()),
                                          // const SizedBox(
                                          //   width: 5,
                                          // ),
                                          // buildChangeLocationButton()
                                        ],
                                      ),
                                      top > 80
                                          ? Container()
                                          : SizedBox(
                                              height: 10,
                                            )
                                    ]),
                              ),
                              background: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 20),
                                    child: Text(
                                      widget.storeLocation.toString(),
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          fontSize: 13,
                                          fontWeight: FontWeight.bold,
                                          color: Color(0xff000000)
                                              .withOpacity(0.34)),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 20),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: Text(
                                            widget.shopName ?? "",
                                            style: TextStyle(
                                                color:
                                                    App24Colors.darkTextColor,
                                                fontSize: 30,
                                                fontWeight: FontWeight.bold),
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                        const SizedBox(
                                          width: 20,
                                        ),
                                        FavoriteButton(
                                          iconSize: 30,
                                          isFavorite: isFavourite,
                                          valueChanged: (_isFavorite) async {
                                            onFavouriteClicked(_isFavorite);

                                            var prefs = await SharedPreferences
                                                .getInstance();
                                            List<String>
                                                restaurantFavouriteList =
                                                prefs.getStringList(
                                                        SharedPreferencesPath
                                                            .favouriteRestaurants) ??
                                                    [];

                                            if (restaurantFavouriteList
                                                .contains(widget.itemTypeID)) {
                                              if (_isFavorite == false) {
                                                showMessage(
                                                    context,
                                                    "Removed from favourites",
                                                    false,
                                                    true);

                                                restaurantFavouriteList.remove(
                                                    widget.itemTypeID
                                                        .toString());
                                                prefs.setStringList(
                                                    SharedPreferencesPath
                                                        .favouriteRestaurants,
                                                    restaurantFavouriteList);

                                                print(prefs.getStringList(
                                                    SharedPreferencesPath
                                                        .favouriteRestaurants));
                                              }
                                            } else {
                                              if (_isFavorite == true) {
                                                showMessage(
                                                    context,
                                                    "Added to favourites",
                                                    false,
                                                    true);
                                                restaurantFavouriteList.add(
                                                    widget.itemTypeID
                                                        .toString());
                                                prefs.setStringList(
                                                    SharedPreferencesPath
                                                        .favouriteRestaurants,
                                                    restaurantFavouriteList);

                                                prefs
                                                    .getStringList(
                                                        SharedPreferencesPath
                                                            .favouriteRestaurants)!
                                                    .toList();
                                                print(prefs.getStringList(
                                                    SharedPreferencesPath
                                                        .favouriteRestaurants));
                                              }
                                            }
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 15,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 20),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "50+ ratings",
                                              style: TextStyle(
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.w600,
                                                  color: Color(0xffa9a9a9)),
                                            ),
                                            RatingBar.builder(
                                              itemSize: 15,
                                              initialRating:
                                                  rating /* double.parse(widget.shopRating.toString())*/,
                                              direction: Axis.horizontal,
                                              allowHalfRating: true,
                                              itemCount: 5,
                                              itemPadding: EdgeInsets.symmetric(
                                                  horizontal: 1.0),
                                              itemBuilder: (context, _) => Icon(
                                                Icons.star,
                                                color: Colors.amber,
                                              ),
                                              onRatingUpdate: (rating) {
                                                print(rating);
                                              },
                                              ignoreGestures: true,
                                            )
                                          ],
                                        ),
                                        Column(
                                          children: [
                                            Text("Delivery Time",
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    fontWeight: FontWeight.w600,
                                                    color: Color(0xffa9a9a9))),
//                                             if(int.parse(widget.deliveryTime.toString()) == 0)
//                                             Text( "30 min",
// /*                                                    getEstimateTime(
//                                                                 context,
//                                                                 state.response
//                                                             //         .responseData)!*/
//                                                 // " min",
//                                                 style: TextStyle(
//                                                     fontSize: 14,
//                                                     fontWeight: FontWeight.w600,
//                                                     color: App24Colors
//                                                         .darkTextColor)),
//                                             if(int.parse(widget.deliveryTime.toString()) != 0)
                                            Text(widget.deliveryTime ?? "",
/*                                                    getEstimateTime(
                                                                context,
                                                                state.response
                                                            //         .responseData)!*/
                                                // " min",
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    fontWeight: FontWeight.w600,
                                                    color: App24Colors
                                                        .darkTextColor))
                                          ],
                                        ),
                                        Column(
                                          children: [
                                            Text("Distance",
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    fontWeight: FontWeight.w600,
                                                    color: Color(0xffa9a9a9))),
                                            Text(widget.distance! + " kms",
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    fontWeight: FontWeight.w600,
                                                    color: App24Colors
                                                        .darkTextColor))
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),

                                  Consumer(builder: (context, watch, child) {
                                    final state =
                                        watch(itemListNotifierProvider);
                                    // final state1 = watch(cartNotifierProvider);
                                    if (state.isLoading) {
                                      return CircularProgressIndicator();
                                    } else if (state.isError) {
                                      return LoadingError(
                                        onPressed: (res) {
                                          reloadData(init: true, context: res);
                                        },
                                        message: state.errorMessage.toString(),
                                      );
                                    } else {
                                      productBanner = state.response
                                          .responseData.productsBanners;
                                      print("product banner" +
                                          productBanner.toString());
                                      // itemListModel =
                                      //     state.response.responseData.products;
                                      //
                                      if (productBanner != []) {
                                        // cartTileList = state1.response.responseData.carts;
                                        return Container(
                                          height: 90,
                                          child: ListView(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 10),
                                            scrollDirection: Axis.horizontal,
                                            children: getProductBanner(context,
                                                state.response.responseData),
                                          ),
                                        );
                                      } else {
                                        return Container();
                                      }
                                    }
                                  })

                                  // Container(
                                  //   padding: EdgeInsets.symmetric(
                                  //       vertical: 15, horizontal: 15),
                                  //   // height: 20,
                                  //   width:
                                  //       MediaQuery.of(context).size.width / 1,
                                  //   decoration: BoxDecoration(
                                  //     borderRadius: BorderRadius.circular(15),
                                  //     color: Color(0xffE3F7FF),
                                  //   ),
                                  //   child: Row(
                                  //     mainAxisAlignment:
                                  //         MainAxisAlignment.spaceBetween,
                                  //     children: [
                                  //       Image.asset(
                                  //         App24UserAppImages.shopOfferPaytm,
                                  //         width: 50,
                                  //       ),
                                  //       Column(
                                  //         crossAxisAlignment:
                                  //             CrossAxisAlignment.start,
                                  //         children: [
                                  //           Text(
                                  //             "15% off on using Paytm UPI",
                                  //             style: TextStyle(
                                  //                 color: Color(0xff233266),
                                  //                 fontWeight: FontWeight.bold,
                                  //                 fontSize:
                                  //                     MediaQuery.of(context)
                                  //                             .size
                                  //                             .width /
                                  //                         27),
                                  //           ),
                                  //           Text(
                                  //             "On an order of AED100 or more",
                                  //             style: TextStyle(
                                  //                 color: Color(0xff6D83CC),
                                  //                 fontWeight: FontWeight.bold,
                                  //                 fontSize:
                                  //                     MediaQuery.of(context)
                                  //                             .size
                                  //                             .width /
                                  //                         34),
                                  //           )
                                  //         ],
                                  //       ),
                                  //       Icon(
                                  //         Icons.chevron_right_rounded,
                                  //         color: Colors.black,
                                  //       )
                                  //     ],
                                  //   ),
                                  // ),
                                ],
                              ),
                            );
                          })),
                      SliverList(
                          delegate: SliverChildListDelegate([
                        const SizedBox(
                          height: 20,
                        ),
                        Container(
                          // padding: EdgeInsets.symmetric(horizontal: 10),
                          height: 40,
                          // width: 200,
                          decoration: BoxDecoration(
                              // color: Theme.of(context).accentColor,
                              borderRadius: BorderRadius.circular(18)),
                          // margin: EdgeInsets.symmetric(horizontal: 20),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child:
                                    Consumer(builder: (context, watch, child) {
                                  final state = watch(itemListNotifierProvider);
                                  if (state.isLoading) {
                                    return ItemListCategoryLoading();
                                  } else if (state.isError) {
                                    {
                                      return LoadingError(
                                        onPressed: (res) {
                                          reloadData(init: true, context: res);
                                        },
                                        message: state.errorMessage,
                                      );
                                    }
                                  } else {
                                    return ListView(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10),
                                      scrollDirection: Axis.horizontal,
                                      children: getCategories(
                                          context, state.response.responseData),
                                    );
                                  }
                                }),
                              )
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                        Consumer(builder: (context, watch, child) {
                          final state = watch(itemListNotifierProvider);
                          // final state1 = watch(cartNotifierProvider);
                          if (state.isLoading) {
                            return ItemListLoading();
                          } else if (state.isError) {
                            return LoadingError(
                              onPressed: (res) {
                                reloadData(init: true, context: res);
                              },
                              message: state.errorMessage.toString(),
                            );
                          } else {
                            itemListModel =
                                state.response.responseData.products;

                            // cartTileList = state1.response.responseData.carts;
                            return Wrap(
                              runSpacing: 10,
                              spacing: 10,
                              alignment: WrapAlignment.center,
                              runAlignment: WrapAlignment.center,
                              children: getItemTiles(
                                context, /*state1.response.responseData.carts*/
                              ),
                            );
                          }
                        }),
                        const SizedBox(
                          height: 20,
                        )
                      ])),
                    ],
                  )),
                  ViewCartOverlay()
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
