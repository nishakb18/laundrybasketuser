ServiceItemsListModel serviceItemsListModelFromJson(str) {
  return ServiceItemsListModel.fromJson(str);
}

class ServiceItemsListModel {
  String? statusCode;
  String? title;
  String? message;
  List<ResponseDatum>? responseData;
  List<dynamic>? responseNewData;
  List<dynamic>? error;

  ServiceItemsListModel({
    this.statusCode,
    this.title,
    this.message,
    this.responseData,
    this.responseNewData,
    this.error,
  });

  factory ServiceItemsListModel.fromJson(Map<String, dynamic> json) => ServiceItemsListModel(
    statusCode: json["statusCode"],
    title: json["title"],
    message: json["message"],
    responseData: json["responseData"] == null ? [] : List<ResponseDatum>.from(json["responseData"]!.map((x) => ResponseDatum.fromJson(x))),
    responseNewData: json["responseNewData"] == null ? [] : List<dynamic>.from(json["responseNewData"]!.map((x) => x)),
    error: json["error"] == null ? [] : List<dynamic>.from(json["error"]!.map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "statusCode": statusCode,
    "title": title,
    "message": message,
    "responseData": responseData == null ? [] : List<dynamic>.from(responseData!.map((x) => x.toJson())),
    "responseNewData": responseNewData == null ? [] : List<dynamic>.from(responseNewData!.map((x) => x)),
    "error": error == null ? [] : List<dynamic>.from(error!.map((x) => x)),
  };
}

class ResponseDatum {
  int? id;
  String? name;
  String? picture;
  int? cartQuantity;

  ResponseDatum({
    this.id,
    this.name,
    this.picture,
    this. cartQuantity
  });

  factory ResponseDatum.fromJson(Map<String, dynamic> json) => ResponseDatum(
    id: json["id"],
    name: json["name"],
    picture: json["picture"],
    cartQuantity: json["cart_quantity"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "picture": picture,
    "cart_quantity": cartQuantity,
  };
}
