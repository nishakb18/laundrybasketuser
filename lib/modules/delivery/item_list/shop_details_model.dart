// To parse this JSON data, do
//
//     final shopDetailsModel = shopDetailsModelFromJson(jsonString);

import 'dart:convert';

ShopDetailsModel shopDetailsModelFromJson(String str) => ShopDetailsModel.fromJson(json.decode(str));

String shopDetailsModelToJson(ShopDetailsModel data) => json.encode(data.toJson());

class ShopDetailsModel {
  String? statusCode;
  String? title;
  String? message;
  ResponseData? responseData;
  List<dynamic>? responseNewData;
  List<dynamic>? error;

  ShopDetailsModel({
    this.statusCode,
    this.title,
    this.message,
    this.responseData,
    this.responseNewData,
    this.error,
  });

  factory ShopDetailsModel.fromJson(Map<String, dynamic> json) => ShopDetailsModel(
    statusCode: json["statusCode"],
    title: json["title"],
    message: json["message"],
    responseData: json["responseData"] == null ? null : ResponseData.fromJson(json["responseData"]),
    responseNewData: json["responseNewData"] == null ? [] : List<dynamic>.from(json["responseNewData"]!.map((x) => x)),
    error: json["error"] == null ? [] : List<dynamic>.from(json["error"]!.map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "statusCode": statusCode,
    "title": title,
    "message": message,
    "responseData": responseData?.toJson(),
    "responseNewData": responseNewData == null ? [] : List<dynamic>.from(responseNewData!.map((x) => x)),
    "error": error == null ? [] : List<dynamic>.from(error!.map((x) => x)),
  };
}

class ResponseData {
  int? id;
  int? storeTypeId;
  String? storeMultiTypeId;
  String? storeName;
  String? currencySymbol;
  String? storeLocation;
  double? latitude;
  double? longitude;
  String? gallery;
  String? picture;
  String? offerMinAmount;
  String? estimatedDeliveryTime;
  int? freeDelivery;
  String? isVeg;
  int? rating;
  int? offerPercent;
  double? walletBalance;
  String? description;
  List<Category>? categories;
  int? hidePrice;
  int? totalstorecart;
  String? shopstatus;
  int? usercart;
  int? totalcartprice;
  List<Category>? allCategories;
  List<String>? servicesName;
  Storetype? storetype;
  List<dynamic>? productsBanners;
  List<Product>? products;
  List<StoreCusinie>? storeCusinie;

  ResponseData({
    this.id,
    this.storeTypeId,
    this.storeMultiTypeId,
    this.storeName,
    this.currencySymbol,
    this.storeLocation,
    this.latitude,
    this.longitude,
    this.gallery,
    this.picture,
    this.offerMinAmount,
    this.estimatedDeliveryTime,
    this.freeDelivery,
    this.isVeg,
    this.rating,
    this.offerPercent,
    this.walletBalance,
    this.description,
    this.categories,
    this.hidePrice,
    this.totalstorecart,
    this.shopstatus,
    this.usercart,
    this.totalcartprice,
    this.allCategories,
    this.servicesName,
    this.storetype,
    this.productsBanners,
    this.products,
    this.storeCusinie,
  });

  factory ResponseData.fromJson(Map<String, dynamic> json) => ResponseData(
    id: json["id"],
    storeTypeId: json["store_type_id"],
    storeMultiTypeId: json["store_multi_type_id"],
    storeName: json["store_name"],
    currencySymbol: json["currency_symbol"],
    storeLocation: json["store_location"],
    latitude: json["latitude"]?.toDouble(),
    longitude: json["longitude"]?.toDouble(),
    gallery: json["gallery"],
    picture: json["picture"],
    offerMinAmount: json["offer_min_amount"],
    estimatedDeliveryTime: json["estimated_delivery_time"],
    freeDelivery: json["free_delivery"],
    isVeg: json["is_veg"],
    rating: json["rating"],
    offerPercent: json["offer_percent"],
    walletBalance: json["wallet_balance"]?.toDouble(),
    description: json["description"],
    categories: json["categories"] == null ? [] : List<Category>.from(json["categories"]!.map((x) => Category.fromJson(x))),
    hidePrice: json["hide_price"],
    totalstorecart: json["totalstorecart"],
    shopstatus: json["shopstatus"],
    usercart: json["usercart"],
    totalcartprice: json["totalcartprice"],
    allCategories: json["all_categories"] == null ? [] : List<Category>.from(json["all_categories"]!.map((x) => Category.fromJson(x))),
    servicesName: json["services_name"] == null ? [] : List<String>.from(json["services_name"]!.map((x) => x)),
    storetype: json["storetype"] == null ? null : Storetype.fromJson(json["storetype"]),
    productsBanners: json["products_banners"] == null ? [] : List<dynamic>.from(json["products_banners"]!.map((x) => x)),
    products: json["products"] == null ? [] : List<Product>.from(json["products"]!.map((x) => Product.fromJson(x))),
    storeCusinie: json["store_cusinie"] == null ? [] : List<StoreCusinie>.from(json["store_cusinie"]!.map((x) => StoreCusinie.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "store_type_id": storeTypeId,
    "store_multi_type_id": storeMultiTypeId,
    "store_name": storeName,
    "currency_symbol": currencySymbol,
    "store_location": storeLocation,
    "latitude": latitude,
    "longitude": longitude,
    "gallery": gallery,
    "picture": picture,
    "offer_min_amount": offerMinAmount,
    "estimated_delivery_time": estimatedDeliveryTime,
    "free_delivery": freeDelivery,
    "is_veg": isVeg,
    "rating": rating,
    "offer_percent": offerPercent,
    "wallet_balance": walletBalance,
    "description": description,
    "categories": categories == null ? [] : List<dynamic>.from(categories!.map((x) => x.toJson())),
    "hide_price": hidePrice,
    "totalstorecart": totalstorecart,
    "shopstatus": shopstatus,
    "usercart": usercart,
    "totalcartprice": totalcartprice,
    "all_categories": allCategories == null ? [] : List<dynamic>.from(allCategories!.map((x) => x.toJson())),
    "services_name": servicesName == null ? [] : List<dynamic>.from(servicesName!.map((x) => x)),
    "storetype": storetype?.toJson(),
    "products_banners": productsBanners == null ? [] : List<dynamic>.from(productsBanners!.map((x) => x)),
    "products": products == null ? [] : List<dynamic>.from(products!.map((x) => x.toJson())),
    "store_cusinie": storeCusinie == null ? [] : List<dynamic>.from(storeCusinie!.map((x) => x.toJson())),
  };
}

class Category {
  int? id;
  int? storetypeId;
  int? agencyId;
  int? storeId;
  String? storeCategoryName;
  String? storeCategoryDescription;
  dynamic picture;
  int? storeCategoryStatus;

  Category({
    this.id,
    this.storetypeId,
    this.agencyId,
    this.storeId,
    this.storeCategoryName,
    this.storeCategoryDescription,
    this.picture,
    this.storeCategoryStatus,
  });

  factory Category.fromJson(Map<String, dynamic> json) => Category(
    id: json["id"],
    storetypeId: json["storetype_id"],
    agencyId: json["agency_id"],
    storeId: json["store_id"],
    storeCategoryName: json["store_category_name"],
    storeCategoryDescription: json["store_category_description"],
    picture: json["picture"],
    storeCategoryStatus: json["store_category_status"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "storetype_id": storetypeId,
    "agency_id": agencyId,
    "store_id": storeId,
    "store_category_name": storeCategoryName,
    "store_category_description": storeCategoryDescription,
    "picture": picture,
    "store_category_status": storeCategoryStatus,
  };
}

class Product {
  int? id;
  String? itemName;
  String? slug;
  int? storeId;
  String? highlight;
  int? serviceId;
  int? laundryItemId;
  int? storetypeId;
  dynamic cstoretypeId;
  dynamic agencyId;
  dynamic cagencyId;
  int? storeCategoryId;
  String? storeCategoryName;
  String? itemDescription;
  String? picture;
  dynamic isVeg;
  int? itemPrice;
  int? itemDiscount;
  String? itemDiscountType;
  int? isAddon;
  int? isStock;
  int? qty;
  int? status;
  int? isBanner;
  int? reviewCount;
  int? avgRating;
  int? offer;
  int? productOffer;
  int? outofstock;
  int? hidePrice;
  String? servicename;
  List<dynamic>? itemsaddon;
  List<dynamic>? itemcart;

  Product({
    this.id,
    this.itemName,
    this.slug,
    this.storeId,
    this.highlight,
    this.serviceId,
    this.laundryItemId,
    this.storetypeId,
    this.cstoretypeId,
    this.agencyId,
    this.cagencyId,
    this.storeCategoryId,
    this.storeCategoryName,
    this.itemDescription,
    this.picture,
    this.isVeg,
    this.itemPrice,
    this.itemDiscount,
    this.itemDiscountType,
    this.isAddon,
    this.isStock,
    this.qty,
    this.status,
    this.isBanner,
    this.reviewCount,
    this.avgRating,
    this.offer,
    this.productOffer,
    this.outofstock,
    this.hidePrice,
    this.servicename,
    this.itemsaddon,
    this.itemcart,
  });

  factory Product.fromJson(Map<String, dynamic> json) => Product(
    id: json["id"],
    itemName: json["item_name"],
    slug: json["slug"],
    storeId: json["store_id"],
    highlight: json["highlight"],
    serviceId: json["service_id"],
    laundryItemId: json["laundry_item_id"],
    storetypeId: json["storetype_id"],
    cstoretypeId: json["cstoretype_id"],
    agencyId: json["agency_id"],
    cagencyId: json["cagency_id"],
    storeCategoryId: json["store_category_id"],
    storeCategoryName: json["store_category_name"],
    itemDescription: json["item_description"],
    picture: json["picture"],
    isVeg: json["is_veg"],
    itemPrice: json["item_price"],
    itemDiscount: json["item_discount"],
    itemDiscountType: json["item_discount_type"],
    isAddon: json["is_addon"],
    isStock: json["is_stock"],
    qty: json["qty"],
    status: json["status"],
    isBanner: json["is_banner"],
    reviewCount: json["review_count"],
    avgRating: json["avg_rating"],
    offer: json["offer"],
    productOffer: json["product_offer"],
    outofstock: json["outofstock"],
    hidePrice: json["hide_price"],
    servicename: json["servicename"],
    itemsaddon: json["itemsaddon"] == null ? [] : List<dynamic>.from(json["itemsaddon"]!.map((x) => x)),
    itemcart: json["itemcart"] == null ? [] : List<dynamic>.from(json["itemcart"]!.map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "item_name": itemName,
    "slug": slug,
    "store_id": storeId,
    "highlight": highlight,
    "service_id": serviceId,
    "laundry_item_id": laundryItemId,
    "storetype_id": storetypeId,
    "cstoretype_id": cstoretypeId,
    "agency_id": agencyId,
    "cagency_id": cagencyId,
    "store_category_id": storeCategoryId,
    "store_category_name": storeCategoryName,
    "item_description": itemDescription,
    "picture": picture,
    "is_veg": isVeg,
    "item_price": itemPrice,
    "item_discount": itemDiscount,
    "item_discount_type": itemDiscountType,
    "is_addon": isAddon,
    "is_stock": isStock,
    "qty": qty,
    "status": status,
    "is_banner": isBanner,
    "review_count": reviewCount,
    "avg_rating": avgRating,
    "offer": offer,
    "product_offer": productOffer,
    "outofstock": outofstock,
    "hide_price": hidePrice,
    "servicename": servicename,
    "itemsaddon": itemsaddon == null ? [] : List<dynamic>.from(itemsaddon!.map((x) => x)),
    "itemcart": itemcart == null ? [] : List<dynamic>.from(itemcart!.map((x) => x)),
  };
}

class StoreCusinie {
  int? id;
  int? storeTypeId;
  int? storeId;
  int? cuisinesId;
  Cuisine? cuisine;

  StoreCusinie({
    this.id,
    this.storeTypeId,
    this.storeId,
    this.cuisinesId,
    this.cuisine,
  });

  factory StoreCusinie.fromJson(Map<String, dynamic> json) => StoreCusinie(
    id: json["id"],
    storeTypeId: json["store_type_id"],
    storeId: json["store_id"],
    cuisinesId: json["cuisines_id"],
    cuisine: json["cuisine"] == null ? null : Cuisine.fromJson(json["cuisine"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "store_type_id": storeTypeId,
    "store_id": storeId,
    "cuisines_id": cuisinesId,
    "cuisine": cuisine?.toJson(),
  };
}

class Cuisine {
  int? id;
  int? storeTypeId;
  String? name;
  int? status;
  String? picture;
  int? cartQuantity;

  Cuisine({
    this.id,
    this.storeTypeId,
    this.name,
    this.status,
    this.picture,
    this.cartQuantity,
  });

  factory Cuisine.fromJson(Map<String, dynamic> json) => Cuisine(
    id: json["id"],
    storeTypeId: json["store_type_id"],
    name: json["name"],
    status: json["status"],
    picture: json["picture"],
    cartQuantity: json["cart_quantity"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "store_type_id": storeTypeId,
    "name": name,
    "status": status,
    "picture": picture,
    "cart_quantity": cartQuantity,
  };
}

class Storetype {
  int? id;
  String? name;
  String? category;
  dynamic picture;
  int? status;
  String? itemsStatus;
  String? addonsStatus;
  String? keywords;
  int? visibility;

  Storetype({
    this.id,
    this.name,
    this.category,
    this.picture,
    this.status,
    this.itemsStatus,
    this.addonsStatus,
    this.keywords,
    this.visibility,
  });

  factory Storetype.fromJson(Map<String, dynamic> json) => Storetype(
    id: json["id"],
    name: json["name"],
    category: json["category"],
    picture: json["picture"],
    status: json["status"],
    itemsStatus: json["items_status"],
    addonsStatus: json["addons_status"],
    keywords: json["keywords"],
    visibility: json["visibility"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "category": category,
    "picture": picture,
    "status": status,
    "items_status": itemsStatus,
    "addons_status": addonsStatus,
    "keywords": keywords,
    "visibility": visibility,
  };
}
