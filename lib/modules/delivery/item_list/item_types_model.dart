import 'dart:convert';

ItemTypesModel itemTypesModelFromJson(String str) => ItemTypesModel.fromJson(json.decode(str));

String itemTypesModelToJson(ItemTypesModel data) => json.encode(data.toJson());

class ItemTypesModel {
  String? statusCode;
  String? title;
  String? message;
  ResponseData? responseData;
  List<dynamic>? responseNewData;
  List<dynamic>? error;

  ItemTypesModel({
    this.statusCode,
    this.title,
    this.message,
    this.responseData,
    this.responseNewData,
    this.error,
  });

  factory ItemTypesModel.fromJson(Map<String, dynamic> json) => ItemTypesModel(
    statusCode: json["statusCode"],
    title: json["title"],
    message: json["message"],
    responseData: json["responseData"] == null ? null : ResponseData.fromJson(json["responseData"]),
    responseNewData: json["responseNewData"] == null ? [] : List<dynamic>.from(json["responseNewData"]!.map((x) => x)),
    error: json["error"] == null ? [] : List<dynamic>.from(json["error"]!.map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "statusCode": statusCode,
    "title": title,
    "message": message,
    "responseData": responseData?.toJson(),
    "responseNewData": responseNewData == null ? [] : List<dynamic>.from(responseNewData!.map((x) => x)),
    "error": error == null ? [] : List<dynamic>.from(error!.map((x) => x)),
  };
}

class ResponseData {
  int? currentPage;
  List<Datum>? data;
  String? firstPageUrl;
  int? from;
  int? lastPage;
  String? lastPageUrl;
  dynamic nextPageUrl;
  String? path;
  int? perPage;
  dynamic prevPageUrl;
  int? to;
  int? total;

  ResponseData({
    this.currentPage,
    this.data,
    this.firstPageUrl,
    this.from,
    this.lastPage,
    this.lastPageUrl,
    this.nextPageUrl,
    this.path,
    this.perPage,
    this.prevPageUrl,
    this.to,
    this.total,
  });

  factory ResponseData.fromJson(Map<String, dynamic> json) => ResponseData(
    currentPage: json["current_page"],
    data: json["data"] == null ? [] : List<Datum>.from(json["data"]!.map((x) => Datum.fromJson(x))),
    firstPageUrl: json["first_page_url"],
    from: json["from"],
    lastPage: json["last_page"],
    lastPageUrl: json["last_page_url"],
    nextPageUrl: json["next_page_url"],
    path: json["path"],
    perPage: json["per_page"],
    prevPageUrl: json["prev_page_url"],
    to: json["to"],
    total: json["total"],
  );

  Map<String, dynamic> toJson() => {
    "current_page": currentPage,
    "data": data == null ? [] : List<dynamic>.from(data!.map((x) => x.toJson())),
    "first_page_url": firstPageUrl,
    "from": from,
    "last_page": lastPage,
    "last_page_url": lastPageUrl,
    "next_page_url": nextPageUrl,
    "path": path,
    "per_page": perPage,
    "prev_page_url": prevPageUrl,
    "to": to,
    "total": total,
  };
}

class Datum {
  int? id;
  String? itemName;
  int? itemPrice;
  String? picture;
  String? userCurrency;
  int? cartQuantity;
  dynamic servicename;
  int? serviceId;

  Datum({
    this.id,
    this.itemName,
    this.itemPrice,
    this.picture,
    this.userCurrency,
    this.cartQuantity,
    this.servicename,
    this.serviceId,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"],
    itemName: json["item_name"],
    itemPrice: json["item_price"],
    picture: json["picture"],
    userCurrency: json["user_currency"],
    cartQuantity: json["cart_quantity"],
    servicename: json["servicename"],
    serviceId: json["service_id"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "item_name": itemName,
    "item_price": itemPrice,
    "picture": picture,
    "user_currency": userCurrency,
    "cart_quantity": cartQuantity,
    "servicename": servicename,
    "service_id": serviceId,
  };
}
