import 'package:app24_user_app/models/location_model.dart';
import 'package:app24_user_app/modules/delivery/order_essentials/order_essentials_loading.dart';
import 'package:app24_user_app/modules/delivery/order_essentials/order_essentials_model.dart';
import 'package:app24_user_app/modules/delivery/shop_list/shop_list_model.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:favorite_button/favorite_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../Laundry-app/views/Shoporder/shop_order_screen.dart';
import '../../../constants/color_coman.dart';

class HomeLaundryShop extends StatefulWidget {
  const HomeLaundryShop({super.key, this.isManiPage = false});
  final bool? isManiPage;

  @override
  State<HomeLaundryShop> createState() => _HomeLaundryShopState();
}

class _HomeLaundryShopState extends State<HomeLaundryShop> {
  LocationModel? selectedLocation;
  @override
  void initState() {
    super.initState();
   /* SchedulerBinding.instance.addPostFrameCallback((_) {
      context.read(orderNotifierProvider.notifier).getLaundry(
        lat: selectedLocation?.latitude ?? 0,
        long: selectedLocation?.longitude ?? 0
      );
    });*/

  }

  reloadData({required BuildContext context, bool? init}) {
    WidgetsBinding.instance.addPostFrameCallback((_){
      context.read(orderNotifierProvider.notifier).getLaundry(
          lat: selectedLocation?.latitude ?? 0,
          long: selectedLocation?.longitude ?? 0
      );
    });
  }

  Widget build(BuildContext context) {
    return SafeArea(
      child:
    (widget.isManiPage == true)?
    Scaffold(
      backgroundColor: AppColor.white,
      body: _renderBody(),
    ):_renderBody());
  }
  Widget _renderBody(){
    return Container(
      color: Colors.white,
      child: Consumer(builder: (context, watch, child) {
        final state = watch(orderNotifierProvider);
        print("Homelaundrybasket");
        print(state);
        if (state.isLoading) {
          print("loading");
          return OrderEssentialLoading();
        } else if (state.isError) {
          print("Error rgarga");
          return LoadingError(
            onPressed: (res) {
              reloadData(init: true, context: res);
            },
            message: state.errorMessage.toString(),
          );
        } else{
          if(state.response?.responseData?.storeList?.data?.isEmpty){
            return LoadingError(
              onPressed: (res) {
                reloadData(init: true, context: res);
              },
              message: state.response?.message ?? '',
            );
          }
        else {
          debugPrint("ui update 11:: ${state.response.toString()}");
          return (state.response?.responseData?.storeList?.data == null)?SizedBox.shrink() :Container(
            height: 200 *double.parse("${state.response?.responseData?.storeList?.data?.length ?? 1}"),
            color: Colors.transparent,
            child: ListView.builder(
              physics:(widget.isManiPage == true)? AlwaysScrollableScrollPhysics(): NeverScrollableScrollPhysics(),
              itemCount: state.response.responseData.storeList.data.length,
              padding: EdgeInsets.symmetric(horizontal: 8),
              itemBuilder: (context, index) {
                return storeCardWidget(
                  onTap: (){
                    Navigator.push(context,
                        MaterialPageRoute(builder: ((context) => ShopOrderScreen(
                          shopId: state.response.responseData.storeList.data[index].id ?? 0,
                        ))));
                  },
                    storeName: state.response.responseData.storeList.data[index].storeName ?? "",
                    storeId: state.response.responseData.storeList.data[index].id ?? 0,
                    storeLocation: state.response.responseData.storeList.data[index].storeLocation ?? '',
                    storeImg: state.response.responseData.storeList.data[index].picture ?? "",
                    distance: state.response.responseData.storeList.data[index].distance,
                    offer: state.response.responseData.storeList.data[index].offerPercent,
                    isFavorite: (state.response.responseData.storeList.data[index].isfavourite == 0)? false: true,
                    rating: state.response.responseData.storeList.data[index].rating);
              },),
          );
          /*ListView(
            children: listItem(context, state.response.responseData),
          );*/
        }}
      }),
    );
  }
  onFavouriteClicked(favourite,storeId) {
    Map body = {
      "store_id": storeId,
      "isfavourite": favourite == true ? 1 : 0
    };
    print("storeId :: $storeId");
    context.read(favouriteShopNotifier.notifier).getFavouriteShop(body: body);
  }

  Widget storeCardWidget({
    String? storeName,
    int? storeId,
    String? storeLocation,
    double? distance,
    int? rating,
    int? offer,
    bool? isFavorite,
    Function()? onTap,
    String? storeImg,}){
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: GestureDetector(
          onTap: onTap,
          child: Card(
            color: Colors.white,
            surfaceTintColor: Colors.transparent,
            elevation: 4,
            child: Column(mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  height: 120,width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      color: AppColor.white,
                    borderRadius: BorderRadius.vertical(
                      top: Radius.circular(10)
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      color: Colors.transparent,
                      child: Stack(
                        children: [
                          Center(
                            child: (storeImg != null && storeImg != '')?
                                Image.network(storeImg):
                            Image.asset("assets/images/emptyImage.png",
                            fit: BoxFit.fill,),
                          ),
                          if(offer != 0)
                          Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              height: 20,width: 80,
                              decoration: BoxDecoration(
                                  color: AppColor.carvebarcolor,
                                  borderRadius: BorderRadius.circular(100)
                              ),
                              child: Center(child: Text("$offer% Off",
                                style: TextStyle(
                                    color: AppColor.white
                                ),)),
                            ),
                          ),
                          Align(
                            alignment: Alignment.topRight,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(100),
                              child: Card(
                                elevation: 6,
                                shape: CircleBorder(),
                                child: Container(
                                  height: 32,width: 32,
                                  decoration: BoxDecoration(
                                      color: AppColor.white,
                                      borderRadius: BorderRadius.circular(100)
                                  ),
                                  child: Center(child: FavoriteButton(
                                    valueChanged: (_isFavorite) async {
                                      onFavouriteClicked(
                                          _isFavorite,storeId);
                                    },
                                    iconSize: 35,
                                    isFavorite: isFavorite,
                                  )),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Divider(
                  color: AppColor.dividerColor.withOpacity(0.16),
                  height: 1.0,
                ),

                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: AppColor.white,
                      borderRadius: BorderRadius.vertical(
                          bottom: Radius.circular(10)
                      ),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(storeName ?? "",
                            style: TextStyle(
                              color: AppColor.servicetext,
                              fontSize: 14
                            ),),
                            RatingBar.builder(
                              itemSize: 15,
                              initialRating:
                              double.parse(rating.toString()),
                              direction: Axis.horizontal,
                              allowHalfRating: true,
                              itemCount: 5,
                              itemPadding: EdgeInsets.symmetric(
                                  horizontal: 1.0),
                              itemBuilder: (context, _) => Icon(
                                Icons.star,
                                color: Colors.amber,
                              ),
                              onRatingUpdate: (rating) {
                                print(rating);
                              },
                              ignoreGestures: true,
                            )
                          ],
                        ),
                        SizedBox(height: 10,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            iconTextWidget(
                                text: "${distance ?? ''} Km",
                            icon: Icons.my_location),
                            iconTextWidget(
                                width: MediaQuery.of(context).size.width * 0.6,
                                text: storeLocation,
                                icon: Icons.location_on_outlined)
                          ],
                        ),
                        SizedBox(height: 8,),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget iconTextWidget({IconData? icon,String? text,double? width}){
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Icon(icon,size: 20,
        color: AppColor.iconLightColor),
        Container(
          width: width,
          color: Colors.transparent,
          child: Text(text ?? '',
          maxLines: 1,
          style: TextStyle(
            overflow: TextOverflow.ellipsis,
            color: AppColor.servicetext,
            fontSize: 11,
          ),),
        )
      ],
    );
  }

}
