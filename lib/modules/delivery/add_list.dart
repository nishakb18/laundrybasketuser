import 'dart:io' as IO;
import 'dart:io';

import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/constants/shared_preferences_path.dart';
import 'package:app24_user_app/helpers.dart';
import 'package:app24_user_app/models/home_delivery_order_insert_model.dart';
import 'package:app24_user_app/models/location_model.dart';
import 'package:app24_user_app/modules/delivery/shop_list/shop_list_model.dart';
import 'package:app24_user_app/modules/my_account/address/manage_address_model.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/utils/services/socket/socket_service.dart';
import 'package:app24_user_app/widgets/alertboxs/searching_for_provider_alert.dart';
import 'package:app24_user_app/widgets/bottomsheets/addressConfirmation/addressConfirmation.dart';
import 'package:app24_user_app/widgets/bottomsheets/order_success_bottomSheet.dart';
import 'package:app24_user_app/widgets/bottomsheets/pick_image.dart';
import 'package:app24_user_app/widgets/bottomsheets/pick_image_bottom_sheet.dart';
import 'package:app24_user_app/widgets/bottomsheets/schedule_bottomSheet.dart';
import 'package:app24_user_app/widgets/common_appbar.dart';
import 'package:app24_user_app/widgets/common_main_button.dart';
import 'package:app24_user_app/widgets/offline_builder_widget.dart';
import 'package:app24_user_app/widgets/payment_waiting_screen.dart';
import 'package:app24_user_app/widgets/track_order.dart';
import 'package:camera/camera.dart';
import 'package:dio/dio.dart';

// import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:socket_io_client/socket_io_client.dart';
// import 'package:url_launcher/url_launcher.dart';

class AddListPage extends StatefulWidget {
  AddListPage(
      {Key? key, this.selectedShop, required this.storeTypeID, this.medicine})
      : super(key: key);

  final ShopNamesModelResponseData? selectedShop;
  final String storeTypeID;
  final bool? medicine;

  @override
  _AddListPageState createState() => _AddListPageState();
}

class _AddListPageState extends State<AddListPage> {
  bool isImageSelected = false;
  late IO.File? selectedImage;
  String? deliveryAddress = "";
  double? latitude;
  double? longitude;
  final ImagePicker _picker = ImagePicker();
  var result;

  String website = "app24.co.in/pages/help";

  late FormData cartContentBody = FormData();
  TextEditingController itemController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  late final SocketService _socketService;

  @override
  void initState() {
    _socketService = SocketService.instance;
    super.initState();
  }

  void gotoUploadListPage() {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (_) {
          return SearchingForProvider();
        });
  }

  // Future<void> _openInBrowser(String url) async {
  //   if (await canLaunch(url)) {
  //     await launch(
  //       url,
  //       forceSafariVC: false,
  //       forceWebView: false,
  //     );
  //   } else {
  //     throw 'Could not launch $url';
  //   }
  // }

  // void getImage({required String source}) async {
  //
  //
  //
  //
  //   if (source == "Camera") {
  //
  //
  //     final XFile? image = await _picker.pickImage(
  //       source: ImageSource.camera,
  //       maxWidth: 512,
  //       maxHeight: 512,
  //       imageQuality: 50,
  //     );
  //     if (image != null) {
  //       Navigator.pop(context, File(image.path));
  //     }
  //     /*  await ImagePicker.openCamera(
  //         maxSize: 1024,
  //         quality: 0.1,
  //         cropOpt: !widget.crop ? null : CropOption())
  //         .then((value) {
  //       if (value != null) {
  //         Navigator.pop(context, File(value[0].path));
  //       }
  //     });*/
  //   } else {
  //     final pickedFile = await _picker.pickImage(
  //       source: ImageSource.gallery,
  //       maxWidth: 512,
  //       maxHeight: 512,
  //       imageQuality: 50,
  //     );
  //     if (pickedFile != null) {
  //       Navigator.pop(context, File(pickedFile.path));
  //     }
  //     /*await ImagesPicker.pick(
  //       maxSize: 1024,
  //
  //       quality: 0.1,
  //       count: 1,
  //       cropOpt: !widget.crop ? null : CropOption(),
  //       pickType: PickType.image,
  //     ).then((value) {
  //       if (value != null) {
  //         Navigator.pop(context, File(value[0].path));
  //       }
  //     });*/
  //   }
  // }

  pickImage() async {
    var res = await Helpers().getCommonBottomSheet(
      context: context,
      content: PickImageBottomSheet(
        crop: false,
      ),
      title: "Pick image from",
    );

    if (res != null) {
      showLog(res.toString());
      setState(() {
        selectedImage = res;
        isImageSelected = true;
      });
    }
  }

  Widget buildPreviewImage() {
    return Stack(children: [
      Container(
        margin: EdgeInsets.only(top: 8, right: 10, left: 10),
        height: 350,
        decoration: BoxDecoration(
          color: Theme.of(context).cardColor,
          borderRadius: BorderRadius.circular(20.0),
          border: Border.all(color: Colors.grey.shade300, width: 1),
          image: DecorationImage(
              image: FileImage(selectedImage!), fit: BoxFit.cover),
        ),
      ),
      Positioned(
        right: 0,
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            customBorder: CircleBorder(),
            onTap: () {
              setState(() {
                selectedImage = null;
                isImageSelected = false;
              });
            },
            child: Container(
              decoration:
                  BoxDecoration(shape: BoxShape.circle, color: Colors.red),
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: Icon(
                  Icons.close,
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ),
      )
    ]);
  }

  insertOrder() async {
    // LocationModel userLocation =
    //     context.read(homeNotifierProvider.notifier).currentLocation;
    late ShopNamesModelResponseData recommendedShop;

    if (widget.selectedShop == null) {
      recommendedShop = context
          .read(shopListNotifierProvider.notifier)
          .state
          .response
          .responseData[0];
    } else {
      recommendedShop = widget.selectedShop!;
    }

    cartContentBody = FormData.fromMap({
      // "to_lat": latitude,
      // "to_lng": longitude,
      // "to_adrs": deliveryAddress,
      "shop_name": recommendedShop.storeName,
      "shop_id": recommendedShop.id,
      "shop_lat": recommendedShop.latitude,
      "shop_lng": recommendedShop.longitude,
      "shop_adrs": recommendedShop.storeLocation,
      "category_id": widget.storeTypeID,

      // "shop_contact_num" :
      // "schedule_date"
      // "schedule_time"
      // "type":TEXT
      // "cat_value"
      // "cart_id"
    });
    // if (!isImageSelected) {
    //   cartContentBody.fields
    //     ..add(MapEntry(
    //       'type',
    //       'text',
    //     ))
    //     ..add(MapEntry(
    //       'cat_value',
    //       itemController.text,
    //     ));
    // } else {
    //   cartContentBody.fields
    //     ..add(MapEntry(
    //       'type',
    //       'image',
    //     ));
    //   cartContentBody.files.add(MapEntry(
    //     'cat_value',
    //     await MultipartFile.fromFile(
    //       selectedImage!.path,
    //       filename: selectedImage!.path.split('/').last,
    //     ),
    //   ));
    //   showLog("file path : ${selectedImage!.path}");
    //   showLog("file name : ${selectedImage!.path.split('/').last}");
    // }
    print("delivery address" + deliveryAddress.toString());
    buildScheduleBottomSheet(recommendedShop);
    // context
    //     .read(orderBuyItemsNotifier.notifier)
    //     .buyItemSave(body: cartContentBody, context: context);
  }

  Future<void> buildScheduleBottomSheet(
      ShopNamesModelResponseData shopResponseData) async {
    // var result = await Helpers().getCommonBottomSheet(
    //     context: context,
    //     content: CheckoutBottomSheet(
    //       cartList: cartListResponseData!,
    //       promoCode: widget.promoCode,
    //       promoCodeDetails: widget.promoCodeDetails,
    //     ),
    //     title: "Checkout");
    // if (result != null) {
    //   Map checkOutResult = result;
    //   // / cartContentBody['promocode_id'] = checkOutResult['promo_code'];
    //   cartContentBody['wallet'] = checkOutResult['use_wallet'] ? 1 : 0;

    var value = await Helpers().getCommonBottomSheet(
        context: context,
        content: ScheduleBottomSheet(
          checkoutType: "fromSend",
          // orderItemBody: cartContentBody,
        ),
        title: "Schedule Delivery");
    // if (value != null) {
    if (value == 'deliver_now') {
      callConfirmAddressBottomSheet(shopResponseData);
    } else {
      Map map = value;

      // cartContentBody['delivery_date'] = map['date'];

      callConfirmAddressBottomSheet(shopResponseData);
    }
    // }
    // }
    // Helpers().getCommonBottomSheet(context: context, content: ScheduleBottomSheet());
  }

  callConfirmAddressBottomSheet(
      ShopNamesModelResponseData shopResponseData) async {
    var addressResult = await Helpers().getCommonBottomSheet(
        context: context,
        content: AddressConfirmationBottomSheet(
          isImageSelectedAddList: isImageSelected,
          shopResponseData: shopResponseData,
          storeTypeIdFromAddList: widget.storeTypeID,
          addListText: itemController.text,
          addListFilePath: isImageSelected == true
              ? selectedImage!.path.split('/').last
              : "",
        ),
        title: "Delivery Address");
    if (addressResult != null) {
      AddressResponseData confirmedLocation = addressResult;

      String? toAddress = (confirmedLocation.flatNo.toString() +
          confirmedLocation.street.toString() +
          confirmedLocation.city.toString() +
          confirmedLocation.landmark.toString());

//       setState(() {
//         latitude = confirmedLocation.latitude;
//         longitude = confirmedLocation.longitude;
//         deliveryAddress = toAddress;
      // });

      cartContentBody.fields
          .add(MapEntry('to_lat', confirmedLocation.latitude.toString()));
      cartContentBody.fields
          .add(MapEntry('to_lng', confirmedLocation.longitude.toString()));
      cartContentBody.fields.add(MapEntry('to_address', toAddress));

      if (!isImageSelected) {
        cartContentBody.fields
          ..add(MapEntry(
            'type',
            'text',
          ))
          ..add(MapEntry(
            'cat_value',
            itemController.text,
          ));
      } else {
        cartContentBody.fields
          ..add(MapEntry(
            'type',
            'image',
          ));
        cartContentBody.files.add(MapEntry(
          'cat_value',
          await MultipartFile.fromFile(
            selectedImage!.path,
            filename: selectedImage!.path.split('/').last,
          ),
        ));
        showLog("file path : ${selectedImage!.path}");
        showLog("file name : ${selectedImage!.path.split('/').last}");
      }

      print("0 " + cartContentBody.fields[0].toString());
      print("1 " + cartContentBody.fields[1].toString());
      print("2 " + cartContentBody.fields[2].toString());
      print("3 " + cartContentBody.fields[3].toString());
      print("4 " + cartContentBody.fields[4].toString());
      print("5 " + cartContentBody.fields[5].toString());
      print("6 " + cartContentBody.fields[6].toString());
      print("7 " + cartContentBody.fields[7].toString());
      print("8 " + cartContentBody.fields[8].toString());
      print("9 " + cartContentBody.fields[9].toString());
      // print("10 "+cartContentBody.fields[10].toString());
      // print("11 "+cartContentBody.fields[11].toString());

      // cartContentBody["to_adrs"] = toAddress;
      // cartContentBody["to_lat"] = confirmedLocation.latitude;
      // cartContentBody["to_lng"] = confirmedLocation.longitude;
// print("agandev"+cartContentBody.fields[2].value);
      // print();
      context
          .read(orderBuyItemsNotifier.notifier)
          .buyItemSave(body: cartContentBody, context: context);
    }
  }

  checkOut(HomeDeliveryOrderInsertResponseData responseData) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString(SharedPreferencesPath.lastOrderType, "buy");
    print("abcdef" +
        prefs.getString(SharedPreferencesPath.lastOrderType).toString());
    prefs.setInt(SharedPreferencesPath.lastOrderId, responseData.id);
    emitSocket(orderID: responseData.id ?? 1);
  }

  emitSocket({required int orderID}) async {
    showLog("Socket emitting");
    try {
      Socket? socket = await _socketService.socket;
      socket!.emit('joinPrivateRoom', {'room_1_R${orderID}_ORDER'});
      showLog("Socket emitted");
      //_socketService.listenSocket();
      context.read(orderDetailsNotifierProvider.notifier).orderType = 'Buy';

      // Navigator.pushReplacement(
      //   context,
      //   MaterialPageRoute(
      //       builder: (context) => PaymentWaiting(
      //         orderID: orderID,orderType: "buy",
      //       )),
      // );
      Helpers().getCommonBottomSheet(
          enableDrag: false,
          isDismissible: false,
          context: context,
          content: OrderSuccess(
            orderType: "buy",
            orderID: orderID,
          ));
      // Navigator.push(
      //   context,
      //   MaterialPageRoute(
      //       builder: (context) => TrackOrder(
      //         id: orderID.toString(),orderType: "buy",
      //       )),
      // );
    } catch (e) {
      showLog("error in socket is : ${e.toString()}");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomAppBar(
          title: "Buy Items",
          hideCartIcon: "true",
          // actions: cartIcon(context),
        ),
        backgroundColor: Colors.white,
        body: OfflineBuilderWidget(
          SafeArea(
            child: ProviderListener(
              provider: orderBuyItemsNotifier,
              onChange: (context, dynamic state) {
                if (state.isLoading!) {
                  showProgress(context);
                } else if (state.isError!) {
                  Navigator.pop(context);
                  showMessage(context, state.errorMessage!, true, true);
                } else {
                  Navigator.pop(context);
                  checkOut(state.response.responseData);
                }
              },
              child: Container(
                margin: EdgeInsets.only(top: 10),
                padding: EdgeInsets.symmetric(horizontal: 25),
                decoration: BoxDecoration(color: Colors.white),
                child: SingleChildScrollView(
                  child: Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        if (widget.selectedShop != null)
                          Center(
                            child: Text(
                              widget.selectedShop?.storeName ?? '',
                              style: TextStyle(
                                  fontWeight: FontWeight.w600, fontSize: 17),
                            ),
                          ),
                        Center(
                          child: Text(
                            widget.selectedShop?.storeLocation ?? "",
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                fontWeight: FontWeight.w600, fontSize: 13),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Text(
                          widget.medicine == true
                              ? "Upload Your Prescription Here"
                              : "Type your shopping list here...",
                          style: TextStyle(
                              fontSize: 19, fontWeight: FontWeight.bold),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Material(
                              color: Colors.transparent,
                              child: InkWell(
                                borderRadius: BorderRadius.circular(5),
                                onTap: () async {
                                  var camera = await availableCameras()
                                      .then((value) => value.first);
                                  var result = await Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              TakePictureScreen(
                                                camera: camera,
                                              )));
                                  // result = Navigator.push(context, MaterialPageRoute(builder: (context) =>TakePictureScreen(camera: value.first))));
                                  // pickImage();
                                  // print(result);
                                  if (result != null) {
                                    showLog(result.toString());
                                    setState(() {
                                      selectedImage = result;
                                      isImageSelected = true;
                                    });
                                  }
                                },
                                child: Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 5),
                                  child: Row(
                                    children: [
                                      Container(
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(15),
                                            color: Color(0xffEFFFF5)),
                                        padding: EdgeInsets.all(10),
                                        alignment: Alignment.center,
                                        child: Icon(
                                          Icons.camera_alt,
                                          color: App24Colors
                                              .greenOrderEssentialThemeColor,
                                          size: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              25,
                                        ),
                                      ),
                                      const SizedBox(
                                        width: 10,
                                      ),
                                      Text(
                                        "Take a photo",
                                        style: TextStyle(
                                            color: App24Colors.darkTextColor,
                                            fontWeight: FontWeight.w600,
                                            fontSize: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                30),
                                        overflow: TextOverflow.clip,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Material(
                                color: Colors.transparent,
                                child: InkWell(
                                    borderRadius: BorderRadius.circular(5),
                                    onTap: () {
                                      pickImage();
                                    },
                                    child: Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 10, vertical: 5),
                                        child: Row(children: [
                                          Container(
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(15),
                                                color: Color(0xffEFFFF5)),
                                            padding: EdgeInsets.all(10),
                                            alignment: Alignment.center,
                                            child: Icon(
                                              Icons.attach_file,
                                              color: App24Colors
                                                  .greenOrderEssentialThemeColor,
                                              size: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  25,
                                            ),
                                          ),
                                          const SizedBox(
                                            width: 10,
                                          ),
                                          Text(
                                            "Upload List",
                                            style: TextStyle(
                                                color:
                                                    App24Colors.darkTextColor,
                                                fontWeight: FontWeight.w600,
                                                fontSize: MediaQuery.of(context)
                                                        .size
                                                        .width /
                                                    30),
                                          ),
                                        ]))))
                          ],
                        ),
                        if (isImageSelected)
                          Column(
                            children: [
                              const SizedBox(
                                height: 30,
                              ),
                              buildPreviewImage(),
                            ],
                          ),
                        if (!isImageSelected)
                          Column(
                            children: [
                              Container(
                                  alignment: Alignment.center,
                                  padding: EdgeInsets.symmetric(vertical: 10),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Container(
                                        color:
                                            Color(0xffAFB9C4).withOpacity(0.22),
                                        height: 1,
                                        width: 130,
                                      ),
                                      const Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 10),
                                        child: Text(
                                          "or",
                                          style: TextStyle(
                                              fontSize: 15,
                                              fontWeight: FontWeight.bold,
                                              color: Color(0xffAFB9C4)),
                                        ),
                                      ),
                                      Container(
                                        color:
                                            Color(0xffAFB9C4).withOpacity(0.22),
                                        height: 1,
                                        width: 130,
                                      )
                                    ],
                                  )),
                              const SizedBox(
                                height: 10,
                              ),
                              TextFormField(
                                textCapitalization:
                                    TextCapitalization.sentences,
                                // keyboardType: TextInputType.multiline,
                                // textInputAction: TextInputAction.done,
                                controller: itemController,
                                maxLines: 14,
                                decoration: InputDecoration(
                                    contentPadding: EdgeInsets.all(30),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(20),
                                      borderSide: BorderSide(
                                        width: 0,
                                        style: BorderStyle.none,
                                      ),
                                    ),
                                    hintText: 'Type Your List....',
                                    hintStyle: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        color: App24Colors
                                            .greenOrderEssentialThemeColor),
                                    fillColor: Color(0xffEFFFF5),
                                    filled: true),
                                validator: (String? arg) {
                                  if (isImageSelected) {
                                    return null;
                                  } else if (arg!.length == 0) {
                                    return 'List can\'t be empty!';
                                  } else {
                                    return null;
                                  }
                                },
                              ),
                            ],
                          ),
                        const SizedBox(
                          height: 50,
                        ),
                        // InkWell(
                        //   onTap: () {
                        //     // _openInBrowser("https://$website")
                        //   },
                        //   child: Center(
                        //       child: Text(
                        //     "Terms and conditions",
                        //     style: TextStyle(
                        //         fontWeight: FontWeight.w800,
                        //         fontSize: 16,
                        //         color:
                        //             App24Colors.greenOrderEssentialThemeColor),
                        //   )),
                        // ),
                        // const SizedBox(
                        //   // height: 20,
                        // ),
                        CommonButton(
                          onTap: () {
                            if (_formKey.currentState!.validate()) {
                              insertOrder();
                            }
                          },
                          bgColor: App24Colors.greenOrderEssentialThemeColor,
                          text: "Order Now",
                        ),
                        SizedBox(
                          height: 20,
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ) // This trailing comma makes auto-formatting nicer for build methods.
        );
  }
}
