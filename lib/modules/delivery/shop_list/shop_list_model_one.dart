import 'dart:convert';

ShopNamesModelOne shopNamesModelOneFromJson(String str) => ShopNamesModelOne.fromJson(json.decode(str));

String shopNamesModelOneToJson(ShopNamesModelOne data) => json.encode(data.toJson());

class ShopNamesModelOne {
  String? statusCode;
  String? title;
  String? message;
  ResponseData? responseData;
  List<dynamic>? responseNewData;
  List<dynamic>? error;

  ShopNamesModelOne({
    this.statusCode,
    this.title,
    this.message,
    this.responseData,
    this.responseNewData,
    this.error,
  });

  factory ShopNamesModelOne.fromJson(Map<String, dynamic> json) => ShopNamesModelOne(
    statusCode: json["statusCode"],
    title: json["title"],
    message: json["message"],
    responseData: json["responseData"] == null ? null : ResponseData.fromJson(json["responseData"]),
    responseNewData: json["responseNewData"] == null ? [] : List<dynamic>.from(json["responseNewData"]!.map((x) => x)),
    error: json["error"] == null ? [] : List<dynamic>.from(json["error"]!.map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "statusCode": statusCode,
    "title": title,
    "message": message,
    "responseData": responseData?.toJson(),
    "responseNewData": responseNewData == null ? [] : List<dynamic>.from(responseNewData!.map((x) => x)),
    "error": error == null ? [] : List<dynamic>.from(error!.map((x) => x)),
  };
}

class ResponseData {
  StoreList? storeList;
  int? cartCount;

  ResponseData({
    this.storeList,
    this.cartCount,
  });

  factory ResponseData.fromJson(Map<String, dynamic> json) => ResponseData(
    storeList: json["store_list"] == null ? null : StoreList.fromJson(json["store_list"]),
    cartCount: json["cart_count"],
  );

  Map<String, dynamic> toJson() => {
    "store_list": storeList?.toJson(),
    "cart_count": cartCount,
  };
}

class StoreList {
  int? currentPage;
  List<Datum>? data;
  String? firstPageUrl;
  int? from;
  int? lastPage;
  String? lastPageUrl;
  dynamic nextPageUrl;
  String? path;
  int? perPage;
  dynamic prevPageUrl;
  int? to;
  int? total;

  StoreList({
    this.currentPage,
    this.data,
    this.firstPageUrl,
    this.from,
    this.lastPage,
    this.lastPageUrl,
    this.nextPageUrl,
    this.path,
    this.perPage,
    this.prevPageUrl,
    this.to,
    this.total,
  });

  factory StoreList.fromJson(Map<String, dynamic> json) => StoreList(
    currentPage: json["current_page"],
    data: json["data"] == null ? [] : List<Datum>.from(json["data"]!.map((x) => Datum.fromJson(x))),
    firstPageUrl: json["first_page_url"],
    from: json["from"],
    lastPage: json["last_page"],
    lastPageUrl: json["last_page_url"],
    nextPageUrl: json["next_page_url"],
    path: json["path"],
    perPage: json["per_page"],
    prevPageUrl: json["prev_page_url"],
    to: json["to"],
    total: json["total"],
  );

  Map<String, dynamic> toJson() => {
    "current_page": currentPage,
    "data": data == null ? [] : List<dynamic>.from(data!.map((x) => x.toJson())),
    "first_page_url": firstPageUrl,
    "from": from,
    "last_page": lastPage,
    "last_page_url": lastPageUrl,
    "next_page_url": nextPageUrl,
    "path": path,
    "per_page": perPage,
    "prev_page_url": prevPageUrl,
    "to": to,
    "total": total,
  };
}

class Datum {
  int? id;
  int? storeTypeId;
  String? storeName;
  String? storeLocation;
  double? latitude;
  double? longitude;
  dynamic picture;
  String? offerMinAmount;
  String? estimatedDeliveryTime;
  int? freeDelivery;
  String? isVeg;
  int? rating;
  int? offerPercent;
  double? distance;
  String? cusineList;
  String? shopstatus;
  DateTime? shopStartTime;
  DateTime? shopEndTime;
  int? isfavourite;
  String? storetypename;
  List<Category>? categories;
  Storetype? storetype;
  List<StoreCusinie>? storeCusinie;

  Datum({
    this.id,
    this.storeTypeId,
    this.storeName,
    this.storeLocation,
    this.latitude,
    this.longitude,
    this.picture,
    this.offerMinAmount,
    this.estimatedDeliveryTime,
    this.freeDelivery,
    this.isVeg,
    this.rating,
    this.offerPercent,
    this.distance,
    this.cusineList,
    this.shopstatus,
    this.shopStartTime,
    this.shopEndTime,
    this.isfavourite,
    this.storetypename,
    this.categories,
    this.storetype,
    this.storeCusinie,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"],
    storeTypeId: json["store_type_id"],
    storeName: json["store_name"],
    storeLocation: json["store_location"],
    latitude: json["latitude"]?.toDouble(),
    longitude: json["longitude"]?.toDouble(),
    picture: json["picture"],
    offerMinAmount: json["offer_min_amount"],
    estimatedDeliveryTime: json["estimated_delivery_time"],
    freeDelivery: json["free_delivery"],
    isVeg: json["is_veg"],
    rating: json["rating"],
    offerPercent: json["offer_percent"],
    distance: json["distance"]?.toDouble(),
    cusineList: json["cusine_list"],
    shopstatus: json["shopstatus"],
    shopStartTime: json["shop_start_time"] == null ? null : DateTime.parse(json["shop_start_time"]),
    shopEndTime: json["shop_end_time"] == null ? null : DateTime.parse(json["shop_end_time"]),
    isfavourite: json["isfavourite"],
    storetypename: json["storetypename"],
    categories: json["categories"] == null ? [] : List<Category>.from(json["categories"]!.map((x) => Category.fromJson(x))),
    storetype: json["storetype"] == null ? null : Storetype.fromJson(json["storetype"]),
    storeCusinie: json["store_cusinie"] == null ? [] : List<StoreCusinie>.from(json["store_cusinie"]!.map((x) => StoreCusinie.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "store_type_id": storeTypeId,
    "store_name": storeName,
    "store_location": storeLocation,
    "latitude": latitude,
    "longitude": longitude,
    "picture": picture,
    "offer_min_amount": offerMinAmount,
    "estimated_delivery_time": estimatedDeliveryTime,
    "free_delivery": freeDelivery,
    "is_veg": isVeg,
    "rating": rating,
    "offer_percent": offerPercent,
    "distance": distance,
    "cusine_list": cusineList,
    "shopstatus": shopstatus,
    "shop_start_time": shopStartTime?.toIso8601String(),
    "shop_end_time": shopEndTime?.toIso8601String(),
    "isfavourite": isfavourite,
    "storetypename": storetypename,
    "categories": categories == null ? [] : List<dynamic>.from(categories!.map((x) => x.toJson())),
    "storetype": storetype?.toJson(),
    "store_cusinie": storeCusinie == null ? [] : List<dynamic>.from(storeCusinie!.map((x) => x.toJson())),
  };
}

class Category {
  int? id;
  int? storetypeId;
  int? agencyId;
  int? storeId;
  String? storeCategoryName;
  String? storeCategoryDescription;
  dynamic picture;
  int? storeCategoryStatus;

  Category({
    this.id,
    this.storetypeId,
    this.agencyId,
    this.storeId,
    this.storeCategoryName,
    this.storeCategoryDescription,
    this.picture,
    this.storeCategoryStatus,
  });

  factory Category.fromJson(Map<String, dynamic> json) => Category(
    id: json["id"],
    storetypeId: json["storetype_id"],
    agencyId: json["agency_id"],
    storeId: json["store_id"],
    storeCategoryName: json["store_category_name"],
    storeCategoryDescription: json["store_category_description"],
    picture: json["picture"],
    storeCategoryStatus: json["store_category_status"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "storetype_id": storetypeId,
    "agency_id": agencyId,
    "store_id": storeId,
    "store_category_name": storeCategoryName,
    "store_category_description": storeCategoryDescription,
    "picture": picture,
    "store_category_status": storeCategoryStatus,
  };
}

class StoreCusinie {
  int? id;
  int? storeTypeId;
  int? storeId;
  int? cuisinesId;
  Cuisine? cuisine;

  StoreCusinie({
    this.id,
    this.storeTypeId,
    this.storeId,
    this.cuisinesId,
    this.cuisine,
  });

  factory StoreCusinie.fromJson(Map<String, dynamic> json) => StoreCusinie(
    id: json["id"],
    storeTypeId: json["store_type_id"],
    storeId: json["store_id"],
    cuisinesId: json["cuisines_id"],
    cuisine: json["cuisine"] == null ? null : Cuisine.fromJson(json["cuisine"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "store_type_id": storeTypeId,
    "store_id": storeId,
    "cuisines_id": cuisinesId,
    "cuisine": cuisine?.toJson(),
  };
}

class Cuisine {
  int? id;
  int? storeTypeId;
  String? name;
  int? status;
  String? picture;

  Cuisine({
    this.id,
    this.storeTypeId,
    this.name,
    this.status,
    this.picture,
  });

  factory Cuisine.fromJson(Map<String, dynamic> json) => Cuisine(
    id: json["id"],
    storeTypeId: json["store_type_id"],
    name: json["name"],
    status: json["status"],
    picture: json["picture"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "store_type_id": storeTypeId,
    "name": name,
    "status": status,
    "picture": picture,
  };
}

class Storetype {
  int? id;
  String? name;
  String? category;
  dynamic picture;
  int? status;
  String? itemsStatus;
  String? addonsStatus;
  String? keywords;
  int? visibility;

  Storetype({
    this.id,
    this.name,
    this.category,
    this.picture,
    this.status,
    this.itemsStatus,
    this.addonsStatus,
    this.keywords,
    this.visibility,
  });

  factory Storetype.fromJson(Map<String, dynamic> json) => Storetype(
    id: json["id"],
    name: json["name"],
    category: json["category"],
    picture: json["picture"],
    status: json["status"],
    itemsStatus: json["items_status"],
    addonsStatus: json["addons_status"],
    keywords: json["keywords"],
    visibility: json["visibility"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "category": category,
    "picture": picture,
    "status": status,
    "items_status": itemsStatus,
    "addons_status": addonsStatus,
    "keywords": keywords,
    "visibility": visibility,
  };
}
