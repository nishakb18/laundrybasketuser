import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/api/api_response_state2.dart';

class LaundryShopListNotifier extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  LaundryShopListNotifier(this._apiRepository)
      : super(ResponseState2(isLoading: true));

  Future<void> getShopNames() async {
    try {
      state = state.copyWith(isLoading: true);
      final shopList =
          await _apiRepository.getlaundryshop(latLong: 'your_lat_long_here');
      state =
          state.copyWith(response: shopList, isLoading: false, isError: false);
    } catch (e) {
      print(e.toString());
      state = state.copyWith(
          errorMessage: e.toString(), isLoading: false, isError: true);
    }
  }
}
