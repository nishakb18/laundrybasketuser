import 'package:app24_user_app/Laundry-app/views/ServicesScreens/services_home_screen.dart';
import 'package:app24_user_app/Laundry-app/views/Shoporder/shop_order_screen.dart';
import 'package:app24_user_app/Laundry-app/views/widgets/custum_appbar.dart';
import 'package:app24_user_app/app24_user_icons.dart';
import 'package:app24_user_app/constants/app_text.dart';
import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/constants/global_constants.dart';
import 'package:app24_user_app/constants/textstyle.dart';
import 'package:app24_user_app/helpers.dart';
import 'package:app24_user_app/models/location_model.dart';
import 'package:app24_user_app/modules/delivery/item_list/item_list.dart';
import 'package:app24_user_app/modules/delivery/shop_list/shop_list_loading.dart';
import 'package:app24_user_app/modules/delivery/shop_list/shop_list_model.dart';
import 'package:app24_user_app/modules/restaurant/restaurant_cart_page.dart';
import 'package:app24_user_app/modules/tabs/home/widgets/offers_loading.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/widgets/bottomsheets/promocodes/landing_page_promo_code/landing_page_promo_codes_model.dart';
import 'package:app24_user_app/widgets/common_appbar.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:app24_user_app/widgets/offline_builder_widget.dart';
import 'package:app24_user_app/widgets/view_cart_overlay.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
// import 'package:flutter/services.dart';
// import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../add_list.dart';

class ShopCategoryPage extends StatefulWidget {
  final String? storeTypeID;
  final String? promoCode;
  final String? promoCodeDetails;
  final bool? isLaundryApp;

  const ShopCategoryPage({
    Key? key,
    this.storeTypeID,
    this.promoCode,
    this.promoCodeDetails, this.isLaundryApp = false,
  }) : super(key: key);

  @override
  _ShopCategoryPageState createState() => _ShopCategoryPageState();
}

class _ShopCategoryPageState extends State<ShopCategoryPage> {
  // List<ShopTile> _shopTileList = [];
  var top = 0.0;
  bool isNearbyShopsEmpty = true;
  String? searchText = '';
  int _current = 0;

  @override
  void initState() {
    // setLocation();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      //API call after screen build
      // context.read(homeNotifierProvider).getOffers(context: context);
      context
          .read(cartCountNotifierProvider.notifier)
          .getCartCount(context: context);
      loadData(context: context);
    });

    super.initState();
  }

  loadData({bool init = true, required BuildContext context}) {
    return context.read(shopListNotifierProvider.notifier).getShopNames(
        init: true, storeTypeID: widget.storeTypeID!, context: context);
  }

  selectLocation(String title, bool searchUserLocation) {
    showSelectLocationSheet(
            context: context,
            title: title,
            searchUserLocation: searchUserLocation)
        .then((value) {
      if (value != null) {
        LocationModel model = value;
        setState(() {
          context.read(homeNotifierProvider.notifier).currentLocation = model;
          loadData(context: context);
        });
      }
    });
  }

  //List of Shop Tiles
  List<Widget> getShopTiles(
      context, List<ShopNamesModelResponseData> shopListModel) {
    List<Widget> _widgetShop = [];
    shopListModel.sort((a, b) => b.shopstatus!.compareTo(a.shopstatus!));
    for (var i = 0; i < shopListModel.length; i++) {
      if (shopListModel[i]
          .storeName!
          .toLowerCase()
          .contains(searchText!.toLowerCase())) {
        _widgetShop.add(Padding(
          padding: const EdgeInsets.only(left: 1, top: 10),
          child: Container(
            // padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
            width: MediaQuery.of(context).size.width / 2.3,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
            ),
            child: Stack(children: [
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        offset: const Offset(0, 3.0),
                        color: Color(0xfff0f0f0),
                        blurRadius: 10.0,
                        spreadRadius: 3.0,
                      )
                    ]),
                child: Material(
                  color: Colors.transparent,
                  child: InkWell(
                    borderRadius: BorderRadius.circular(25),
                    onTap: shopListModel[i].shopstatus == "CLOSED"
                        ? null
                        : () {
                      if (widget.isLaundryApp == true) {
                        Navigator.push(context,
                            MaterialPageRoute(builder: ((context) =>
                                ShopOrderScreen(
                                  shopId: shopListModel[i].id ?? 0,
                                ))));
                      }
                      else{
                        print("shopID from shopPage" +
                            shopListModel[i].id.toString());

                      if (widget.storeTypeID != "1" &&
                          widget.storeTypeID != "3") {
                        Navigator.push(
                            context,
                            PageRouteBuilder(
                              pageBuilder: (context, animation,
                                  secondaryAnimation) =>
                                  ItemList(
                                    promoCodeDetails: widget.promoCodeDetails,
                                    promoCode: widget.promoCode,
                                    shopName: shopListModel[i].storeName,
                                    shopID: shopListModel[i].id,
                                    //delivery time should be changed in the future in api side
                                    deliveryTime: shopListModel[i]
                                        .estimatedDeliveryTime,
                                    storeLocation:
                                    shopListModel[i].storeLocation,
                                    storeTypeID: widget.storeTypeID,
                                    itemTypeID:
                                    shopListModel[i].id.toString(),
                                    distance:
                                    shopListModel[i].distance.toString(),
                                  ),
                              transitionsBuilder: (context, animation,
                                  secondaryAnimation, child) {
                                const begin = Offset(1.0, 0.0);
                                const end = Offset.zero;
                                const curve = Curves.ease;

                                var tween = Tween(begin: begin, end: end)
                                    .chain(CurveTween(curve: curve));

                                return SlideTransition(
                                  position: animation.drive(tween),
                                  child: child,
                                );
                              },
                            )

                          // MaterialPageRoute(
                          //     builder: (context) => ItemList(
                          //           shopName: shopListModel[i].storeName,
                          //           deliveryTime:
                          //               shopListModel[i].estimatedDeliveryTime,
                          //           storeLocation:
                          //               shopListModel[i].storeLocation,
                          //           storeTypeID: widget.storeTypeID,
                          //           itemTypeID: shopListModel[i].id.toString(),
                          //         ))
                        );
                      } else {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    AddListPage(
                                      medicine: widget.storeTypeID == "3"
                                          ? true
                                          : false,
                                      selectedShop: shopListModel[i],
                                      storeTypeID: widget.storeTypeID!,
                                    )));
                      }
                    }
                          },
                    child: Padding(
                      padding: const EdgeInsets.all(10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(25.0),
                            child: CachedNetworkImage(
                                colorBlendMode: BlendMode.color,
                                color: shopListModel[i].shopstatus == "CLOSED"
                                    ? Colors.grey
                                    : null,
                                imageUrl: shopListModel[i].picture ?? "",
                                width: MediaQuery.of(context).size.width / 2,
                                height: MediaQuery.of(context).size.height / 7,
                                fit: BoxFit.cover,
                                placeholder: (context, url) => Image.asset(
                                      App24UserAppImages.placeHolderImage,
                                      width: MediaQuery.of(context).size.width /
                                              2 -
                                          20,
                                      height:
                                          MediaQuery.of(context).size.height /
                                              7,
                                      fit: BoxFit.cover,
                                    ),
                                errorWidget: (context, url, error) =>
                                    Image.asset(
                                      App24UserAppImages.placeHolderImage,
                                      width: MediaQuery.of(context).size.width /
                                              2 -
                                          20,
                                      height:
                                          MediaQuery.of(context).size.height /
                                              7,
                                      fit: BoxFit.cover,
                                    )),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Text("data"),
                          Text(
                            shopListModel[i].storeName!,
                            style: TextStyle(
                                color: shopListModel[i].shopstatus == "CLOSED"
                                    ? Colors.grey.withOpacity(0.5)
                                    : App24Colors.shopAndItemNameColor,
                                fontWeight: FontWeight.bold,
                                fontSize: 16),
                            // textAlign: TextAlign.center,
                            overflow: TextOverflow.ellipsis,
                            // maxLines: 2,
                          ),
                          Row(
                            children: [
                              Text(
                                shopListModel[i].distance!.toString() + "km",
                                style: TextStyle(
                                    color:
                                        shopListModel[i].shopstatus == "CLOSED"
                                            ? Colors.grey.withOpacity(0.3)
                                            : App24Colors.shopAndItemNameColor,
                                    fontSize: 9,
                                    fontWeight: FontWeight.w600),
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                child: Text(
                                  shopListModel[i].shopEndTime ?? "",
                                  style: TextStyle(
                                      color: shopListModel[i].shopstatus ==
                                              "CLOSED"
                                          ? Colors.grey.withOpacity(0.3)
                                          : Color(0xff55697D),
                                      fontSize: 9,
                                      fontWeight: FontWeight.w600),
                                  overflow: TextOverflow.ellipsis,
                                ),
                              )
                            ],
                          ),
                          Text(
                            shopListModel[i].storeLocation!,
                            style: TextStyle(
                                color: shopListModel[i].shopstatus == "CLOSED"
                                    ? Colors.grey.withOpacity(0.3)
                                    : Color(0xff55697D),
                                fontSize: 9,
                                fontWeight: FontWeight.w600),
                            overflow: TextOverflow.ellipsis,
                          ),
                          RatingBar.builder(
                            initialRating: double.parse(
                                shopListModel[i].rating.toString()),
                            direction: Axis.horizontal,
                            allowHalfRating: true,
                            itemCount: 5,
                            itemSize: 14,
                            itemPadding: EdgeInsets.symmetric(horizontal: 1.0),
                            itemBuilder: (context, _) => Icon(
                              Icons.star,
                              color: shopListModel[i].shopstatus == "CLOSED"
                                  ? Colors.grey.withOpacity(0.3)
                                  : Colors.amber,
                            ),
                            onRatingUpdate: (rating) {
                              print(rating);
                            },
                            ignoreGestures: true,
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ]),
          ),
        ));
      }
    }
    if (_widgetShop.isEmpty) {
      _widgetShop.add(Padding(
          padding: const EdgeInsets.only(left: 1, top: 10),
          child: Container(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              width: MediaQuery.of(context).size.width / 2.3,
              /*decoration: BoxDecoration(
    borderRadius: BorderRadius.circular(15),
    ),*/
              child: Text("No shop found"))));
    }
    return _widgetShop;
  }

  //Search Bar
  Widget buildSearchBar() {
    return Container(
      height: top > 80 ? 40 : 50,
      child: TextField(
        onChanged: (String? s) {
          setState(() {
            searchText = s;
          });
        },
        style: TextStyle(fontWeight: FontWeight.w300, fontSize: 14),
        // keyboardType: TextInputType.number,
        decoration: InputDecoration(
          // contentPadding: EdgeInsets.symmetric(
          //              horizontal: 5, vertical: top > 80 ? 9 : 10),
          prefixIcon: Icon(
            Icons.search,
            color: App24Colors.greenOrderEssentialThemeColor,
            size: top > 80 ? 20 : 24,
          ),
          hintText: "Search for a store...",
          hintStyle: TextStyle(
              fontSize: top > 80 ? 12 : 16,
              fontWeight: FontWeight.w600,
              color: App24Colors.greenOrderEssentialThemeColor),
          border: OutlineInputBorder(
            borderRadius: top > 80
                ? BorderRadius.circular(15)
                : BorderRadius.circular(15),
            borderSide: BorderSide(
              width: 0,
              style: BorderStyle.none,
            ),
          ),
          fillColor: Color(0xffE3FFEE),
          filled: true,
          contentPadding: EdgeInsets.symmetric(
              vertical: top > 80 ? 0 : 0, horizontal: top > 80 ? 5 : 10),
        ),
      ),
    );
  }

  List<Widget> getCarouselImages(
      context, List<LandingPagePromoResponseData> promoCodeList) {
    final List<Widget> imageSliders = promoCodeList
        .map((item) => Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Stack(children: [
                Container(
                  // padding: EdgeInsets.symmetric(horizontal: 10),
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(30),
                      child: new FadeInImage.assetNetwork(
                        placeholder: 'assets/images/placeholder.png',
                        image: item.picture!,
                        fit: BoxFit.fill,
                        width: MediaQuery.of(context).size.width,
                      )),
                ),
                Material(
                  borderRadius: BorderRadius.circular(25),
                  color: Colors.transparent,
                  child: InkWell(
                      borderRadius: BorderRadius.circular(25), onTap: null),
                )
              ]),
            ))
        .toList();

    return imageSliders;
  }

  //Location Button
  // Widget buildChangeLocationButton() {
  //   return Container(
  //     decoration: BoxDecoration(
  //         borderRadius: BorderRadius.circular(15),
  //         color: Color(0xffE3FFEE),
  //         border: Border.all(color: Colors.grey[200])),
  //     child: Material(
  //       color: Colors.transparent,
  //       child: InkWell(
  //         onTap: () {},
  //         borderRadius: BorderRadius.circular(15),
  //         child: Padding(
  //           padding: EdgeInsets.symmetric(
  //               vertical: top > 80 ? 9 : 14, horizontal: top > 80 ? 12 : 16),
  //           child: Icon(
  //             Icons.my_location,
  //             color: App24Colors.greenOrderEssentialThemeColor,
  //             size: top > 80 ? 20 : 24,
  //           ),
  //         ),
  //       ),
  //     ),
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: (widget.isLaundryApp == true)
          ? PreferredSize(
        preferredSize: Size(double.infinity, 80),
        child: CusturmAppbar(
            child: ListTile(
              leading: backarrow(
                context: context,
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              title: Center(
                child: Text("All Essentials", style: AppTextStyle().appbathead),
              ),
            )),
      )
          :CustomAppBar(
        title: "All Essentials",
        hideCartIcon: "true",
        // actions: cartIcon(context:context,fromPage:""),
      ),
      body: OfflineBuilderWidget(
        RefreshIndicator(
          onRefresh: () {
            return loadData(init: false, context: context);
          },
          child: Container(
            child: Column(
              children: [
                Expanded(
                  child: CustomScrollView(
                    slivers: <Widget>[
                      SliverAppBar(
                          automaticallyImplyLeading: false,
                          backgroundColor: Colors.white,
                          pinned: true,
                          toolbarHeight: 0,
                          collapsedHeight: 60,
                          expandedHeight: 330,
                          flexibleSpace: LayoutBuilder(builder:
                              (BuildContext context,
                                  BoxConstraints constraints) {
                            // print('constraints=' + constraints.toString());
                            top = constraints.biggest.height;
                            return FlexibleSpaceBar(
                              titlePadding: EdgeInsetsDirectional.zero,
                              title: Padding(
                                padding: EdgeInsets.symmetric(horizontal: 17),
                                child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Row(
                                        children: [
                                          Expanded(child: buildSearchBar()),
                                          // const SizedBox(
                                          //   width: 5,
                                          // ),
                                          // buildChangeLocationButton()
                                        ],
                                      ),
                                      top > 80
                                          ? Container()
                                          : SizedBox(
                                              height: 10,
                                            )
                                    ]),
                              ),
                              background: Container(
                                  // decoration: BoxDecoration(
                                  //   borderRadius: BorderRadius.circular(25),
                                  //   // boxShadow: [
                                  //   //   BoxShadow(
                                  //   //     color: Colors.grey,
                                  //   //     spreadRadius: 10,
                                  //   //     blurRadius: 20,
                                  //   //   )
                                  //   // ]
                                  // ),
                                  child: Column(
                                children: [
                                  Container(
                                    margin: EdgeInsets.fromLTRB(25, 0, 25, 0),
                                    child: TextButton(
                                      onPressed: () {
                                        selectLocation(
                                            GlobalConstants
                                                .labelGlobalLocation,
                                            true);
                                      },
                                      child: Row(
                                        children: [
                                          Icon(
                                            App24User.path_2568_3x,
                                            size: 35,
                                            color: App24Colors
                                                .redRestaurantThemeColor,
                                          ),
                                          Expanded(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  context
                                                          .read(
                                                              homeNotifierProvider
                                                                  .notifier)
                                                          .currentLocation
                                                          .main_text ??
                                                      '',
                                                  style: TextStyle(
                                                    fontSize:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width /
                                                            20,
                                                    fontWeight:
                                                        FontWeight.bold,
                                                    color: Color(0xffFF8282),
                                                  ),
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                                // Text(
                                                //   "Change Location",
                                                //   style: TextStyle(
                                                //       fontSize: 12,
                                                //       fontWeight:
                                                //           FontWeight.w600,
                                                //       color:
                                                //           Color(0xff373737)
                                                //               .withOpacity(
                                                //                   0.3)),
                                                // )
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  if (widget.storeTypeID == "1" ||
                                      widget.storeTypeID == "3")
                                    Container(
                                      margin:
                                          EdgeInsets.fromLTRB(25, 0, 25, 40),
                                      child: Material(
                                        color: Colors.transparent,
                                        child: InkWell(
                                          borderRadius:
                                              BorderRadius.circular(30),
                                          onTap: () {
                                            if (isNearbyShopsEmpty) {
                                              showMessage(
                                                  context,
                                                  "No nearby shops available",
                                                  true,
                                                  false);
                                            } else {
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        AddListPage(
                                                      storeTypeID:
                                                          widget.storeTypeID!,
                                                    ),
                                                  ));
                                            }
                                          },
                                          child: Container(
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(25),
                                                boxShadow: [
                                                  BoxShadow(
                                                      color: Colors.grey
                                                          .withOpacity(0.4),
                                                      spreadRadius: 3,
                                                      blurRadius: 10,
                                                      offset: Offset(0, 7))
                                                ]),
                                            child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(25),
                                              child: Image.asset(
                                                App24UserAppImages
                                                    .offerHomeTab,
                                                fit: BoxFit.fill,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  if (widget.storeTypeID != "1" &&
                                      widget.storeTypeID != "3")
                                    Consumer(
                                        builder: (context, watch, child) {
                                      final state =
                                          watch(landingPagePromoCodeProvider);
                                      if (state.isLoading) {
                                        return OffersLoading();
                                      } else if (state.isError) {
                                        return LoadingError(
                                          onPressed: (res) {
                                            loadData(
                                                init: true, context: res);
                                          },
                                          message: state.errorMessage,
                                        );
                                      } else {
                                        if (state.response.responseData
                                                .length !=
                                            0) {
                                          return Container(
                                            child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment
                                                        .stretch,
                                                children: [
                                                  CarouselSlider(
                                                    items: getCarouselImages(
                                                        context,
                                                        state.response
                                                            .responseData),
                                                    options: CarouselOptions(
                                                        height: 145,
                                                        autoPlay: true,
                                                        enlargeCenterPage:
                                                            false,
                                                        onPageChanged:
                                                            (index, reason) {
                                                          setState(() {
                                                            _current = index;
                                                          });
                                                        }),
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: <Widget>[
                                                      ...List.generate(
                                                          state
                                                              .response
                                                              .responseData
                                                              .length,
                                                          (index) =>
                                                              Container(
                                                                width: 8.0,
                                                                height: 8.0,
                                                                margin: EdgeInsets.symmetric(
                                                                    vertical:
                                                                        10.0,
                                                                    horizontal:
                                                                        2.0),
                                                                decoration:
                                                                    BoxDecoration(
                                                                  shape: BoxShape
                                                                      .circle,
                                                                  color: _current ==
                                                                          index
                                                                      ? Theme.of(context)
                                                                          .primaryColor
                                                                      : Colors
                                                                          .grey[300],
                                                                ),
                                                              )),
                                                    ],
                                                  )
                                                ]),
                                          );
                                        } else {
                                          return Container(
                                            child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment
                                                        .stretch,
                                                children: [
                                                  CarouselSlider(
                                                    items: Helpers()
                                                        .getCarouselImage(
                                                            context),
                                                    options: CarouselOptions(
                                                        height: 145,
                                                        autoPlay: true,
                                                        enlargeCenterPage:
                                                            false,
                                                        onPageChanged:
                                                            (index, reason) {
                                                          setState(() {
                                                            _current = index;
                                                          });
                                                        }),
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: <Widget>[
                                                      ...List.generate(
                                                          Helpers()
                                                              .foodOfferCarousel
                                                              .length,
                                                          (index) =>
                                                              Container(
                                                                width: 8.0,
                                                                height: 8.0,
                                                                margin: EdgeInsets.symmetric(
                                                                    vertical:
                                                                        10.0,
                                                                    horizontal:
                                                                        2.0),
                                                                decoration:
                                                                    BoxDecoration(
                                                                  shape: BoxShape
                                                                      .circle,
                                                                  color: _current ==
                                                                          index
                                                                      ? Theme.of(context)
                                                                          .primaryColor
                                                                      : Colors
                                                                          .grey[300],
                                                                ),
                                                              )),
                                                    ],
                                                  )
                                                ]),
                                          );
                                        }
                                      }
                                    })
                                ],
                              )),
                            );
                          })),
                      SliverList(
                          delegate: SliverChildListDelegate([
                        Consumer(builder: (context, watch, child) {
                          final state = watch(shopListNotifierProvider);
                          if (state.isLoading) {
                            return ShopListLoading();
                          } else if (state.isError) {
                            return LoadingError(
                              onPressed: (res) {
                                loadData(init: true, context: res);
                              },
                              message: state.errorMessage,
                            );
                          } else {
                            if (state.response.responseData.length == 0) {
                              isNearbyShopsEmpty = true;
                              return Container(
                                margin: EdgeInsets.only(top: 80),
                                alignment: Alignment.center,
                                child: Text(
                                  state.response.message,
                                  style:
                                      TextStyle(fontWeight: FontWeight.w600),
                                ),
                              );
                            } else {
                              isNearbyShopsEmpty = false;
                              return Wrap(
                                alignment: WrapAlignment.center,
                                runAlignment: WrapAlignment.center,
                                spacing: 20,
                                // runSpacing:20,
                                children: getShopTiles(
                                    context, state.response.responseData),
                              );
                            }
                          }
                        }),
                        const SizedBox(
                          height: 20,
                        )
                      ])),
                    ],
                  ),
                ),
               if(widget.isLaundryApp!)  ViewCartOverlay()
              ],
            ),
          ),
        ),
      ),
    );
  }
}

// class ShopTile {
//   String shop_image;
//   String shop_name;
//   String shop_distance;
//   String shop_rating;
//   String shop_location;
//   String shop_ratecount;
//
//   ShopTile(
//       {this.shop_distance,
//       this.shop_image,
//       this.shop_location,
//       this.shop_name,
//       this.shop_ratecount,
//       this.shop_rating});
// }
