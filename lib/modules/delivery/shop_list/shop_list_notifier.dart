import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/api/api_response_state2.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class ShopListNotifier extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  ShopListNotifier(this._apiRepository)
      : super(ResponseState2(isLoading: true));

  Future<void> getShopNames(
      {bool init = true,
      required String storeTypeID,
      required BuildContext context}) async {
    try {
      if (init) state = state.copyWith(isLoading: true);
      String latLong = '';
      if (context
                  .read(homeNotifierProvider.notifier)
                  .currentLocation
                  .latitude !=
              null &&
          context
                  .read(homeNotifierProvider.notifier)
                  .currentLocation
                  .longitude !=
              null) {
        latLong =
            '?latitude=${context.read(homeNotifierProvider.notifier).currentLocation.latitude}&'
            'longitude=${context.read(homeNotifierProvider.notifier).currentLocation.longitude}&recommended=';
      }
      final shopList = await _apiRepository.fetchShopList(
          storeTypeID: storeTypeID, latLong: latLong);
      state =
          state.copyWith(response: shopList, isLoading: false, isError: false);
    } catch (e) {
      print(e.toString());
      state = state.copyWith(
          errorMessage: e.toString(), isLoading: false, isError: true);
    }
  }
}
