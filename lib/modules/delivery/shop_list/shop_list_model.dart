ShopNamesModel shopNamesModelFromJson(str) {
  return ShopNamesModel.fromJson(str);
}


class ShopNamesModel {
  String? statusCode;
  String? title;
  String? message;
  List<ShopNamesModelResponseData>? responseData;
  List<dynamic>? responseNewData;
  List<dynamic>? error;

  ShopNamesModel(
      {this.statusCode,
      this.title,
      this.message,
      this.responseData,
      this.responseNewData,
      this.error});

  ShopNamesModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    if (json['responseData'] != null) {
      responseData = [];
      json['responseData'].forEach((v) {
        responseData!.add(new ShopNamesModelResponseData.fromJson(v));
      });
    }
    if (json['responseNewData'] != null) {
      responseNewData = [];
      json['responseNewData'].forEach((v) {
        responseNewData!.add(v);
      });
    }
    if (json['error'] != null) {
      error = [];
      json['error'].forEach((v) {
        error!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['title'] = this.title;
    data['message'] = this.message;
    if (this.responseData != null) {
      data['responseData'] = this.responseData!.map((v) => v.toJson()).toList();
    }
    if (this.responseNewData != null) {
      data['responseNewData'] =
          this.responseNewData!.map((v) => v.toJson()).toList();
    }
    if (this.error != null) {
      data['error'] = this.error!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ShopNamesModelResponseData {
  dynamic id;
  dynamic storeTypeId;
  String? storeName;
  String? storeLocation;
  double? latitude;
  double? longitude;
  String? picture;
  String? offerMinAmount;
  String? estimatedDeliveryTime;
  dynamic freeDelivery;
  String? isVeg;
  dynamic rating;
  dynamic offerPercent;
  dynamic distance;
  String? cusineList;
  String? shopstatus;
  String? shopStartTime;
  String? shopEndTime;
  dynamic isfavourite;
  String? storetypename;
  List<Categories>? categories;
  Storetype? storetype;
  List<dynamic>? storeCusinie;

  ShopNamesModelResponseData(
      {this.id,
      this.storeTypeId,
      this.storeName,
      this.storeLocation,
      this.latitude,
      this.longitude,
      this.picture,
      this.offerMinAmount,
      this.estimatedDeliveryTime,
      this.freeDelivery,
      this.isVeg,
      this.rating,
      this.offerPercent,
      this.distance,
      this.cusineList,
      this.shopstatus,
      this.shopStartTime,
      this.shopEndTime,
      this.isfavourite,
      this.storetypename,
      this.categories,
      this.storetype,
      this.storeCusinie});

  ShopNamesModelResponseData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    storeTypeId = json['store_type_id'];
    storeName = json['store_name'];
    storeLocation = json['store_location'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    picture = json['picture'];
    offerMinAmount = json['offer_min_amount'];
    estimatedDeliveryTime = json['estimated_delivery_time'];
    freeDelivery = json['free_delivery'];
    isVeg = json['is_veg'];
    rating = json['rating'];
    offerPercent = json['offer_percent'];
    distance = json['distance'];
    cusineList = json['cusine_list'];
    shopstatus = json['shopstatus'];
    shopStartTime = json['shop_start_time'];
    shopEndTime = json['shop_end_time'];
    isfavourite = json['isfavourite'];
    storetypename = json['storetypename'];
    if (json['categories'] != null) {
      categories = [];
      json['categories'].forEach((v) {
        categories!.add(new Categories.fromJson(v));
      });
    }
    storetype = json['storetype'] != null
        ? new Storetype.fromJson(json['storetype'])
        : null;
    if (json['store_cusinie'] != null) {
      storeCusinie = [];
      json['store_cusinie'].forEach((v) {
        storeCusinie!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['store_type_id'] = this.storeTypeId;
    data['store_name'] = this.storeName;
    data['store_location'] = this.storeLocation;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['picture'] = this.picture;
    data['offer_min_amount'] = this.offerMinAmount;
    data['estimated_delivery_time'] = this.estimatedDeliveryTime;
    data['free_delivery'] = this.freeDelivery;
    data['is_veg'] = this.isVeg;
    data['rating'] = this.rating;
    data['offer_percent'] = this.offerPercent;
    data['distance'] = this.distance;
    data['cusine_list'] = this.cusineList;
    data['shopstatus'] = this.shopstatus;
    data['shop_start_time'] = this.shopStartTime;
    data['shop_end_time'] = this.shopEndTime;
    data['isfavourite'] = this.isfavourite;
    data['storetypename'] = this.storetypename;
    if (this.categories != null) {
      data['categories'] = this.categories!.map((v) => v.toJson()).toList();
    }
    if (this.storetype != null) {
      data['storetype'] = this.storetype!.toJson();
    }
    if (this.storeCusinie != null) {
      data['store_cusinie'] =
          this.storeCusinie!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Categories {
  dynamic id;
  dynamic storeId;
  String? storeCategoryName;
  String? storeCategoryDescription;
  String? picture;
  dynamic storeCategoryStatus;

  Categories(
      {this.id,
      this.storeId,
      this.storeCategoryName,
      this.storeCategoryDescription,
      this.picture,
      this.storeCategoryStatus});

  Categories.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    storeId = json['store_id'];
    storeCategoryName = json['store_category_name'];
    storeCategoryDescription = json['store_category_description'];
    picture = json['picture'];
    storeCategoryStatus = json['store_category_status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['store_id'] = this.storeId;
    data['store_category_name'] = this.storeCategoryName;
    data['store_category_description'] = this.storeCategoryDescription;
    data['picture'] = this.picture;
    data['store_category_status'] = this.storeCategoryStatus;
    return data;
  }
}

class Storetype {
  dynamic id;
  String? name;
  String? category;
  String? picture;
  dynamic status;
  String? itemsStatus;
  String? addonsStatus;
  String? keywords;
  int? visibility;

  Storetype(
      {this.id,
      this.name,
      this.category,
      this.picture,
      this.status,
      this.itemsStatus,
      this.addonsStatus,
      this.keywords,
      this.visibility});

  Storetype.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    category = json['category'];
    picture = json['picture'];
    status = json['status'];
    itemsStatus = json['items_status'];
    addonsStatus = json['addons_status'];
    keywords = json['keywords'];
    visibility = json['visibility'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['category'] = this.category;
    data['picture'] = this.picture;
    data['status'] = this.status;
    data['items_status'] = this.itemsStatus;
    data['addons_status'] = this.addonsStatus;
    data['keywords'] = this.keywords;
    data['visibility'] = this.visibility;
    return data;
  }
}


