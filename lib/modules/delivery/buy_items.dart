import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/widgets/common_appbar.dart';
import 'package:app24_user_app/widgets/offline_builder_widget.dart';
import 'package:flutter/material.dart';

class BuyItems extends StatefulWidget {
  // const BuyItems({Key? key}) : super(key: key);

  @override
  _BuyItemsState createState() => _BuyItemsState();
}

class _BuyItemsState extends State<BuyItems> {
  TextEditingController address = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        hideCartIcon: "true",
        title: "BuyItems",
        // actions: cartIcon(context:context,fromPage: ""),
      ),
      body: OfflineBuilderWidget(SafeArea(
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 20, horizontal: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Delivery Address"),
              TextField(
                  controller: address,
                  maxLines: 3,
                  decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(30),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20),
                        borderSide: BorderSide(
                          width: 0,
                          style: BorderStyle.none,
                        ),
                      ),
                      hintText: 'Address',
                      hintStyle: TextStyle(
                          fontWeight: FontWeight.w600,
                          color: App24Colors.greenOrderEssentialThemeColor),
                      fillColor: Theme.of(context).cardColor,
                      filled: true)),
              const SizedBox(
                height: 15,
              ),
              Text("ContactNumber"),
              TextField(
                  // controller: address,
                  // maxLines: 3,
                  decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(15),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15),
                        borderSide: BorderSide(
                          width: 0,
                          style: BorderStyle.none,
                        ),
                      ),
                      hintText: 'Contact number',
                      hintStyle: TextStyle(
                          fontWeight: FontWeight.w600,
                          color: App24Colors.greenOrderEssentialThemeColor),
                      fillColor: Theme.of(context).cardColor,
                      filled: true))
            ],
          ),
        ),
      )),
    );
  }
}
