import 'package:app24_user_app/Laundry-app/views/ServicesScreens/services_home_screen.dart';
import 'package:app24_user_app/Laundry-app/views/Shoporder/shop_order_screen.dart';
import 'package:app24_user_app/Laundry-app/views/widgets/custum_appbar.dart';
import 'package:app24_user_app/constants/app_text.dart';
import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_coman.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/constants/global_constants.dart';
import 'package:app24_user_app/constants/shared_preferences_path.dart';
import 'package:app24_user_app/constants/textstyle.dart';
import 'package:app24_user_app/helpers.dart';
import 'package:app24_user_app/models/home_delivery_order_insert_model.dart';
import 'package:app24_user_app/models/home_delivery_send_order_detail.dart';
import 'package:app24_user_app/models/location_model.dart';
import 'package:app24_user_app/modules/delivery/cart/cart_loading.dart';
import 'package:app24_user_app/modules/delivery/cart/cart_model.dart';
import 'package:app24_user_app/modules/delivery/shop_list/shop_list_model.dart';
import 'package:app24_user_app/modules/my_account/address/manage_address_model.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/utils/services/api/api_services.dart';
import 'package:app24_user_app/utils/services/database/address_database.dart';
import 'package:app24_user_app/utils/services/database/database_service.dart';
import 'package:app24_user_app/utils/services/socket/socket_service.dart';
import 'package:app24_user_app/widgets/bottomsheets/addressConfirmation/addressConfirmation.dart';
import 'package:app24_user_app/widgets/bottomsheets/order_success_bottomSheet.dart';
import 'package:app24_user_app/widgets/bottomsheets/schedule_bottomSheet.dart';
import 'package:app24_user_app/widgets/bottomsheets/search_location_bottom_sheet.dart';
import 'package:app24_user_app/widgets/common_appbar.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:app24_user_app/widgets/offline_builder_widget.dart';
import 'package:app24_user_app/widgets/payment_waiting_screen.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:socket_io_client/socket_io_client.dart';

class Cart extends StatefulWidget {
  final bool? newCartPage;
  final String? itemID;
  final ShopNamesModelResponseData? selectedShop;
  final String? storeTypeID;
  final int? isMainStoreID;
  final String? promoCode;
  final String? promoCodeDetails;
  final List<int>? cartProductsId;
  final double? lat;
  final double? long;
  final String? fromAddress;

  const Cart(
      {super.key,
      this.itemID,
      this.selectedShop,
      this.storeTypeID,
      this.promoCode,
      this.promoCodeDetails,
      this.newCartPage = false,
      this.cartProductsId,
      this.isMainStoreID,
      this.lat,
      this.long,
      this.fromAddress});

  @override
  State<Cart> createState() => _CartState();
}

class _CartState extends State<Cart> {
  ///CART PROCESS
  ///
  /// checkout (on click)->  checkOut()->buildCheckoutBottomSheet();
  /// confirm & continue (on click)-> schedule delivery->callConfirmAddressBottomSheet();
  /// buyItemsOrderPlaceFromCart
  /// provider listener -> if order inserted
  /// checkPaymentAndEmitSocket()-> 2 scenarios
  /// 1. payment need to done first
  /// call razorpay->after success emitSocket()
  /// 2. emitSocket()
  ///
  /// emitSocket
  /// 1.socket emits with order ID
  /// 2.order detail order type set to buy
  /// 3.navigate to order detail screen (track page)

  bool useWalletCheckout = false;
  List<Carts>? cartTileList = [];
  CartListModel? cartListModel;
  late Map cartContentBody;
  bool status = false;
  double? amountToCheckout = 0.0;
  var totalAmountToRazor;
  bool handlerSuccess = false;
  HomeDeliveryOrderDetailResponseData? abc;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController couponController = TextEditingController();
  bool useWallet = false;
  bool useCOD = false;
  bool lockWallet = false;
  List<AddressResponseData>? list;
  Map? addressFromSearch;

  bool? isSameAddress = false;

  late int amountToCheckOut;
  late final SocketService _socketService;
  var total;

  var totalAmount;
  int? totalCart;
  String? shopName;
  CartListResponseData? cartListResponseData;
  String? walletActive;
  AddressResponseData? selectedAddress;

  late Razorpay? _razorPay;
  String? highPriorityAddress;
  String? toAddress;
  double? toLat;
  double? toLng;
  bool isInitShowError = true;

  final ScrollController _scrollController = ScrollController();

  reloadData({required BuildContext context, bool? init}) {
    return context.read(cartNotifierProvider.notifier).getCartList(
        storeID: widget.isMainStoreID ?? 0,
        isLaundryBasketCard: widget.newCartPage,
        context: context,
        init: init!,
        fromLat: widget.lat ??
            context
                .read(homeNotifierProvider.notifier)
                .currentLocation
                .latitude!
                .toDouble(),
        fromLng: widget.long ??
            context
                .read(homeNotifierProvider.notifier)
                .currentLocation
                .longitude!
                .toDouble());
  }

  buttonDisabled() {
    // if (cartListResponseData!.carts!.isEmpty)
    setState(() {
      status = false;
    });
  }

  @override
  void initState() {
    print("list cart :: ${widget.cartProductsId}");
    print(
        "list lat lng :: ${list?[0].latitude ?? widget.lat}  ${selectedAddress?.latitude} ${list?[0].longitude ?? widget.long} ${selectedAddress?.longitude} ");
    getAddressWithHighestPriority();
    // print(list![0].latitude);
    highPriorityAddress = widget.fromAddress;
    _socketService = SocketService.instance;
    SchedulerBinding.instance.addPostFrameCallback((_) {
      //API call after screen build
      // context.read(addToCartNotifierProvider).addCart(context: context,itemID: widget.itemID);
      print("init statte :: method 1");
      if (widget.lat != null && widget.long != null) {}
      context.read(cartNotifierProvider.notifier).getCartList(
          storeID: widget.isMainStoreID ?? 0,
          isLaundryBasketCard: widget.newCartPage,
          context: context,
          fromLat: widget.lat ??
              context
                  .read(homeNotifierProvider.notifier)
                  .currentLocation
                  .latitude!
                  .toDouble(),
          fromLng: widget.long ??
              context
                  .read(homeNotifierProvider.notifier)
                  .currentLocation
                  .longitude!
                  .toDouble());
    });

    // lockWallet = cartListResponseData!.userWalletBalance < 0 ? true : false;
    super.initState();
    _initRazorPay();
  }

  @override
  void dispose() {
    _razorPay!.clear();
    super.dispose();
  }

  selectLocation(String title) {
    showSelectLocationSheet(context: context, title: title).then((value) {
      if (value != null) {
        LocationModel model = value;
        setState(() {
          context.read(homeNotifierProvider.notifier).currentLocation = model;
          context.read(cartNotifierProvider.notifier).getCartList(
              isLaundryBasketCard: widget.newCartPage,
              fromLat: widget.lat ??
                  context
                      .read(homeNotifierProvider.notifier)
                      .currentLocation
                      .latitude!
                      .toDouble(),
              fromLng: widget.long ??
                  context
                      .read(homeNotifierProvider.notifier)
                      .currentLocation
                      .longitude!
                      .toDouble(),
              context: context);
          // loadData(context: context);
        });
      }
    });
  }

  _initRazorPay() {
    _razorPay = new Razorpay();

    _razorPay!.on(Razorpay.EVENT_PAYMENT_SUCCESS, handlerPaymentSuccess);
    _razorPay!.on(Razorpay.EVENT_PAYMENT_ERROR, handlerErrorFailure);
    _razorPay!.on(Razorpay.EVENT_EXTERNAL_WALLET, handlerExternalWallet);
    return _razorPay;
  }

//   paymentWaiting() async{
//     await Future.delayed(const Duration(seconds: 3), (){
//       Navigator.pushReplacement(
//         context,
//         MaterialPageRoute(
//             builder: (context) =>
// PaymentWaiting()),
//       );}
//       );
//     Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => TrackOrder(orderType: 'buy',id: context
//         .read(orderBuyItemsNotifier.notifier)
//         .state
//         .response
//         .responseData!
//         .id.toString(),)));
//   }

  void handlerPaymentSuccess(
      PaymentSuccessResponseresponse /*HomeDeliveryOrderDetailResponseData responseData*/) async {
    var prefs = await SharedPreferences.getInstance();
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => PaymentWaiting(
                  orderID: prefs.getInt(SharedPreferencesPath.lastOrderId)!,
                  orderType: "buy",
                )));
    // Helpers().getCommonBottomSheet(context: context, content: OrderSuccess(orderType: 'buy',orderID: prefs.getInt(SharedPreferencesPath.lastOrderId),/*orderType: "buy",orderID: responseData.id,*/));
    // print("printing Payment successful");

//     // paymentWaiting();
// Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => TrackOrder(orderType: context
//     .read(orderBuyItemsNotifier.notifier)
//     .state
//     .response
//     .responseData!
//     .id,id: "buy",)));

    // Navigator.push(
    //   context,
    //   MaterialPageRoute(
    //       builder: (context) => PaymentWaiting(
    //         orderID:  context
    //             .read(orderBuyItemsNotifier.notifier)
    //             .state
    //             .response
    //             .responseData!
    //             .id,orderType: "buy",
    //       )),
    // );
    // Helpers().getCommonBottomSheet(context: context, content: OrderSuccess());
    // emitSocket(
    //     orderID: context
    //         .read(orderBuyItemsNotifier.notifier)
    //         .state
    //         .response
    //         .responseData!
    //         .id);
  }

  void handlerErrorFailure(
    PaymentFailureResponse response,
    /*HomeDeliveryOrderDetailResponseData responseData*/
  ) {
    // print("printing Payment failed" + response.message.toString());

    context.read(orderDetailsNotifierProvider.notifier).checkOngoingOrders();
    _socketService.listenSocket();

    /* Helpers().getCommonBottomSheet(context: context, content: Container(
      padding: EdgeInsets.all(15),
      child: Column(
        children: [
          Center(
            child: Text("Your Payment was failed."),
          ),
          MaterialButton(onPressed: (){

          },
            child: Text("Retry"),
          )
        ],
      ),
    ));
*/
    // Navigator.pushReplacement(
    //     context, MaterialPageRoute(builder: (context) => HomePage()));
    // Helpers().getCommonBottomSheet(
    //   enableDrag: false,
    //     isDismissible: false,
    //     context: context,
    //     content: MakePaymentBottomSheet(
    //       detailResponseData: responseData,
    //     ),
    //     title: "Retry");
    print("printing Payment failed");
  }

  showSelectLocationSheetForAddress(
      {required BuildContext context, bool initialCall = true, String? title}) {
    return showGeneralDialog(
        context: context,
        barrierDismissible: true,
        barrierLabel:
            MaterialLocalizations.of(context).modalBarrierDismissLabel,
        barrierColor: Colors.black54,
        transitionBuilder: (context, animation, secondaryAnimation, child) {
          var begin = const Offset(0.0, 1.0);
          var end = Offset.zero;
          var curve = Curves.ease;

          var tween =
              Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

          return SlideTransition(
            position: animation.drive(tween),
            child: child,
          );
        },
        pageBuilder: (BuildContext context, _, __) =>
            SearchLocationBottomSheetPage(
              initialCall: initialCall,
              title: title,
            ));
  }

  void handlerExternalWallet(
    ExternalWalletResponse response,
    /* HomeDeliveryOrderDetailResponseData responseData*/
  ) {
    context.read(orderDetailsNotifierProvider.notifier).checkOngoingOrders();
    _socketService.listenSocket();
    // Helpers().getCommonBottomSheet(
    //   enableDrag: false,
    //     isDismissible: false,
    //     context: context,
    //     content: MakePaymentBottomSheet(
    //       detailResponseData: responseData,
    //     ),
    //     title: "Retry");
    print("printing Payment external");
  }

  getTotalAmount(CartListResponseData cartList) {
    // print("printing wallet amount " + cartList.userWalletBalance.toString());
    // print("printing wallet amount " + cartList.totalPrice!.toString());
    // setState(() {
    total = double.parse(cartList.totalPrice.toString());
    // });
    if (useWallet) {
      // setState(() {
      total = cartList.totalPrice! /*+
          widget.cartList.shopGstAmount*/
          -
          cartList.userWalletBalance;
      // print("printing total amount " + total.toString());

      if (total < 0) total = 0;
      // });
    } else {
      return double.parse(total.toString()).toStringAsFixed(2);
    }
    //print("printing barcelona"+double.parse(total.toString()).toStringAsFixed(2));
    return double.parse(total.toString()).toStringAsFixed(2);
  }

  updateDetails({CartListResponseData? responseData}) async {
    var prefs = await SharedPreferences.getInstance();
    totalAmount = responseData?.payable;
    totalCart = responseData?.totalCart;
    if (responseData != null && responseData.carts!.isEmpty) {
      prefs.setInt(SharedPreferencesPath.cartStoreId, 0);
    }
  }

  emitSocket(
      {required int orderID,
      CartListResponseData? cartListResponseData}) async {
    var prefs = await SharedPreferences.getInstance();
    var totalPayable = double.parse(
        prefs.getString(SharedPreferencesPath.totalAmountToRazor).toString());
    showLog("Socket emitting");
    try {
      Socket? socket = await _socketService.socket;
      socket!.emit('joinPrivateRoom', {'room_1_R${orderID}_ORDER'});
      showLog("Socket emitted");
      //_socketService.listenSocket();

      // context.read(orderDetailsNotifierProvider.notifier).orderType = 'Buy';

      // print("printing used wallet" + use_wallet_checkout.toString());
      // print("printing used wallet" + handlerSuccess.toString());
      // Navigator.popAndPushNamed(context, "/homeScreen");
      // print("show estimation"+cartListResponseData!.showEstimation.toString());
      if (totalPayable == 0.0 || cartListResponseData!.showEstimation == 0) {
        print(cartListResponseData!.showEstimation);
        print("printing wallet used");
        print("useWallet :: $useWallet");
        Helpers().getCommonBottomSheet(
            enableDrag: false,
            context: context,
            isDismissible: false,
            content: OrderSuccess(
              orderID: orderID,
              orderType: "buy",
            ));
      } else {
        print("printing wallet not used else condition");
        // Helpers().getCommonBottomSheet(
        //     context: context,
        //     isDismissible: false,
        //     content: MakePaymentBottomSheet(detailResponseData: abc!,)
        // );
        // Navigator.pushReplacement(
        //       context,
        //       MaterialPageRoute(
        //           builder: (context) =>
        //               TrackOrder(
        //                 id: orderID.toString(), orderType: "buy",
        //               )));
        // print("printing testing if wallet");
      }

      // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => TrackOrder(orderType: 'buy',id: orderID.toString(),)));
      // if(handlerSuccess == false)

      // Navigator.pushReplacement(
      //     context,
      //     MaterialPageRoute(
      //         builder: (context) =>
      //             TrackOrder(
      //               id: orderID.toString(), orderType: "buy",
      //             )));

/*      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => PaymentWaiting(
              orderID: orderID,orderType: "buy",
            )),
      );*/
      // Future.delayed(const Duration(seconds: 5), () {
/*      await Future.delayed(const Duration(seconds: 5), (){
        Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) =>
                TrackOrder(
                  id: orderID.toString(), orderType: "buy",
                )),
      );});*/

      // } );

      // Navigator.push(
      //   context,
      //   MaterialPageRoute(
      //       builder: (context) =>
      //           TrackOrder(
      //             id: orderID.toString(),
      //           )),
      // );
    } catch (e) {
      showLog("error in socket is : ${e.toString()}");
    }
  }

  Future<void> buildCheckoutBottomSheet() async {
    print("totalamounttorazor" + totalAmountToRazor.toString());
    // var result = await Helpers().getCommonBottomSheet(
    //     context: context,
    //     content: CheckoutBottomSheet(
    //       cartList: cartListResponseData!,
    //       promoCode: widget.promoCode,
    //       promoCodeDetails: widget.promoCodeDetails,
    //     ),
    //     title: "Checkout");
    // if (result != null) {
    //   Map checkOutResult = result;
    //   // / cartContentBody['promocode_id'] = checkOutResult['promo_code'];
    //   cartContentBody['wallet'] = checkOutResult['use_wallet'] ? 1 : 0;

    var value = await Helpers().getCommonBottomSheet(
        context: context,
        content: const ScheduleBottomSheet(
          checkoutType: "fromSend",
          // orderItemBody: cartContentBody,
        ),
        title: "Schedule Delivery");
    // if (value != null) {
    print("object value :: $value ");
    if (value == 'deliver_now') {
      print("printing deliver now");
      context
          .read(orderBuyItemsNotifier.notifier)
          .buyItemsOrderPlaceFromCart(body: cartContentBody, context: context);

      print("printing cart content body : " + cartContentBody.toString());
      // callConfirmAddressBottomSheet();
    } else {
      print("printing deliver later");
      Map map = value;
      // cartContentBody['schedule_date'] = map['date'];
      // cartContentBody['schedule_time'] = map['time'];
      cartContentBody['delivery_date'] = map['date'];
      context
          .read(orderBuyItemsNotifier.notifier)
          .buyItemsOrderPlaceFromCart(body: cartContentBody, context: context);
      print("printing cart content body : " + cartContentBody.toString());
      // callConfirmAddressBottomSheet(map: map);
    }
    // }
    // }
    // Helpers().getCommonBottomSheet(context: context, content: ScheduleBottomSheet());
  }

//   callConfirmAddressBottomSheet({Map<dynamic, dynamic>? map}) async {
//     var addressResult = await Helpers().getCommonBottomSheet(
//         context: context,
//         content: AddressConfirmationBottomSheet(map: map),
//         title: "Delivery confirmation");
//     if (addressResult != null) {
//       // print(addressResult);
//       AddressResponseData confirmedLocation = addressResult;
//
//       String? toAddress = (confirmedLocation.flatNo.toString() +
//           ", " +
//           confirmedLocation.street.toString() +
//           ", " +
//           confirmedLocation.city.toString() +
//           ", " +
//           confirmedLocation.landmark.toString());
//
//       cartContentBody["to_adrs"] = toAddress;
//       cartContentBody["to_lat"] = confirmedLocation.latitude;
//       cartContentBody["to_lng"] = confirmedLocation.longitude;
//
//       var checkout = await Helpers().getCommonBottomSheet(
//           enableDrag: true,
//           isDismissible: true,
//           context: context,
//           content: CheckoutBottomSheet(
//             toAddress: toAddress,
//             cartList: cartListResponseData!,
//             latitude: confirmedLocation.latitude,
//             longitude: confirmedLocation.longitude,
//             promoCode: widget.promoCode,
//             promoCodeDetails: widget.promoCodeDetails,
//           ));
//
//       if (checkout == "clearCart") {
//         context.read(cartNotifierProvider.notifier).getCartList(
//             context: context,
//             init: true,
//             lat: context
//                 .read(homeNotifierProvider.notifier)
//                 .currentLocation
//                 .latitude!
//                 .toDouble(),
//             lng: context
//                 .read(homeNotifierProvider.notifier)
//                 .currentLocation
//                 .longitude!
//                 .toDouble());
//       } else {
//         // print(checkout);
//         var useWallet = checkout;
//         // print("printing checknow");
//         // print(useWallet);
//
//         setState(() {
//           walletActive = useWallet["wallet"];
//           // totalAmountToRazor = useWallet["total_amount"];
//           use_wallet_checkout = walletActive == "1" ? true : false;
//           // print("printing checknow wallet");
//           // print(useWallet["wallet"]);
//           // print("printing if wallet used :");
//           // print(use_wallet_checkout);
//           // print("printing success payment razor"+handlerSuccess.toString());
//         });
//         if (useWallet["to_address"] != null) {
//           cartContentBody["to_adrs"] = useWallet["to_address"];
//           cartContentBody["to_lat"] = useWallet["to_lat"];
//           cartContentBody["to_lng"] = useWallet["to_lng"];
//         }
//         cartContentBody["wallet"] = useWallet["wallet"];
//
//         context
//             .read(orderBuyItemsNotifier.notifier)
//             .buyItemsOrderPlaceFromCart(body: cartContentBody);
// /*
//         if (cartContentBody['wallet'] == '1') {
//           //if wallet ? update the user wallet amount
//           context.read(homeNotifierProvider.notifier).getProfile();
//         }
// */
//
//         // await context.read(cartNotifierProvider.notifier).getCartList();
//         // }
//
//       }
//     }
//   }

  // payWithWallet() {
  //   Map body = {
  //     'wallet_amount': usableWalletAmount,
  //     'id': widget.detailResponseData.id
  //   };
  //
  //   context
  //       .read(payWithWalletNotifierProvider.notifier)
  //       .payWithWallet(body: body);
  // }

  checkOut() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString(SharedPreferencesPath.orderType, "buy");
    // prefs.setString(SharedPreferencesPath.totalAmountToRazor, getTotalAmount(cartListResponseData!));
    prefs.setString(SharedPreferencesPath.totalAmountToRazor, '0.0');
    print("totalAmount :: ${getTotalAmount(cartListResponseData!)}");
    print(
        "list lat lng :: ${list?[0].latitude ?? widget.lat}  ${selectedAddress?.latitude} ${list?[0].longitude ?? widget.long} ${selectedAddress?.longitude} ");
    // LocationModel userLocation =
    //     context.read(homeNotifierProvider.notifier).currentLocation;
    // print(useWallet);
    cartContentBody = {
      //"promocode_id": "",
      "wallet": useWallet,
      "payment_mode": (useWallet) ? "WALLET" : "COD",
      //"payment_id" : razorID.value.toString())
      //"card_id": "",
      "user_address_id": '0',
      "deliver_address_id": '0',
      "from_adrs": highPriorityAddress,
      "from_lat": widget.lat ??
          ((selectedAddress == null)
              ? (list?[0].latitude)
              : selectedAddress!.latitude),
      "from_lng": widget.long ??
          ((selectedAddress == null)
              ? (list?[0].longitude)
              : selectedAddress!.longitude),
      "to_adrs": toAddress,
      "to_lat": toLat,
      "to_lng": toLng,
      //"delivery_date" :  Delivery_Date!!)
      "order_type": "DELIVERY"
    };
    print("whether wallet used : " + useWallet.toString());
    Logger.appLogs("whether cartContentBody : " + cartContentBody.toString());
    // context
    //     .read(orderBuyItemsNotifier.notifier)
    //     .buyItemsOrderPlaceFromCart(
    //     body: cartContentBody, context: context);
    buildCheckoutBottomSheet();

    // context
    //     .read(orderBuyItemsNotifier.notifier)
    //     .buyItemsOrderPlaceFromCart(body: cartContentBody, context: context);
  }

  onClearCart() async {
    await context
        .read(cartNotifierProvider.notifier)
        .getClearCart(context: context);
    context.read(cartNotifierProvider.notifier).getCartList(
        isLaundryBasketCard: widget.newCartPage,
        context: context,
        init: true,
        fromLat: widget.lat ??
            context
                .read(homeNotifierProvider.notifier)
                .currentLocation
                .latitude!
                .toDouble(),
        fromLng: widget.long ??
            context
                .read(homeNotifierProvider.notifier)
                .currentLocation
                .longitude!
                .toDouble());
    int cartCount =
        context.read(cartCountNotifierProvider.notifier).state.response;

    context
        .read(cartCountNotifierProvider.notifier)
        .updateCartCount(count: cartCount = 0);
  }

  onTapRemove(index) async {
    Map body = {"cart_id": cartTileList![index].id};

    setState(() {
      if (context.read(cartNotifierProvider.notifier).cartIdList != null) {
        /* int b = context
            .read(cartNotifierProvider.notifier)
            .cartIdList!
            .removeAt(index);*/
       // int? b = widget.cartProductsId?.removeAt(index);

        for (int i = 0;
            i < context.read(cartNotifierProvider.notifier).cartIdList!.length;
            i++) {
          // print("printing products id: " +
          //     context
          //         .read(cartNotifierProvider.notifier)
          //         .cartIdList![i]
          //         .toString());
        }
      }
    });
    await context
        .read(cartNotifierProvider.notifier)
        .removeItemCart(body: body, context: context);
    context.read(cartNotifierProvider.notifier).getCartList(
        isLaundryBasketCard: widget.newCartPage,
        context: context,
        init: false,
        fromLat: widget.lat ??
            context
                .read(homeNotifierProvider.notifier)
                .currentLocation
                .latitude!
                .toDouble(),
        fromLng: widget.long ??
            context
                .read(homeNotifierProvider.notifier)
                .currentLocation
                .longitude!
                .toDouble());

    var cartCount =
        context.read(cartCountNotifierProvider.notifier).state.response;

    print("cartCount :: $cartCount");
    context.read(cartCountNotifierProvider.notifier).updateCartCount(
        count: (cartCount != null) ? cartCount.length - 1 : 0,
        storeID: cartTileList![index].storeId!);
  }

  // List<Product> efg = [];
  buildCartItems(int index) {
    // List<Product> cart = [];
    var total = cartTileList![index].product!.itemPrice! *
        cartTileList![index].quantity!;

    return (cartTileList![index].quantity != 0)
        ? Container(
            height: MediaQuery.of(context).size.height / 6.8,
            margin: const EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),
            padding: const EdgeInsets.all(10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25),
                color: Colors.white,
                border: Border.all(color: Colors.grey[200]!),
                boxShadow: [
                  BoxShadow(
                    offset: const Offset(0.0, 1.0),
                    color: Colors.grey[200]!,
                    blurRadius: 4.0,
                    spreadRadius: 4.0,
                  )
                ]),
            child: Row(
              // crossAxisAlignment: CrossAxisAlignment.stretch,
              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(20),
                  child: CachedNetworkImage(
                    imageUrl: cartTileList![index].product!.picture ??
                        App24UserAppImages.placeHolderImage,
                    width: MediaQuery.of(context).size.width / 3.5,
                    height: MediaQuery.of(context).size.height / 7,
                    fit: BoxFit.fill,
                    placeholder: (context, url) => Image.asset(
                      App24UserAppImages.placeHolderImage,
                      width: MediaQuery.of(context).size.width / 4.5,
                    ),
                    errorWidget: (context, url, error) => Image.asset(
                      App24UserAppImages.placeHolderImage,
                      width: MediaQuery.of(context).size.width / 4.5,
                    ),
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: Tooltip(
                              message: cartTileList![index].product!.itemName!,
                              child: Text(
                                cartTileList![index].product!.itemName!,
                                style: TextStyle(
                                    fontSize:
                                        MediaQuery.of(context).size.width / 27,
                                    fontWeight: FontWeight.bold),
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                          if (cartListResponseData!.showEstimation == 1)
                            Text(
                              "${cartListResponseData?.userCurrency ?? ''} $total",
                              style: const TextStyle(
                                  fontSize: 13,
                                  color:
                                      App24Colors.greenOrderEssentialThemeColor,
                                  fontWeight: FontWeight.bold),
                              overflow: TextOverflow.clip,
                            )
                        ],
                      ),
                      Text(
                        cartTileList![index].product!.servicename ?? '',
                        style: TextStyle(
                            fontSize: MediaQuery.of(context).size.width / 27,
                            fontWeight: FontWeight.bold),
                        overflow: TextOverflow.ellipsis,
                      ),
                      if (cartTileList![index].cartaddon!.isNotEmpty)
                        if (cartListResponseData!.showEstimation == 1)
                          Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                if (cartTileList![index].cartaddon!.isNotEmpty)
                                  Expanded(
                                      child: buildAddonsList(
                                          cartTileList![index].cartaddon!)),
                              ]),
                      const SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            decoration: BoxDecoration(
                                color: const Color(0xffFFECE9),
                                borderRadius: BorderRadius.circular(15)),
                            child: Row(
                              // mainAxisAlignment: MainAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Material(
                                  borderRadius:
                                      const BorderRadius.all(Radius.circular(50)),
                                  color: Colors.transparent,
                                  // shadowColor: Colors.grey[400],
                                  // elevation: 3,
                                  child: InkWell(
                                    splashColor: Colors.red,
                                    borderRadius:
                                        const BorderRadius.all(Radius.circular(50)),
                                    onTap: () {
                                      updateCart(
                                          cartItem: cartTileList![index],
                                          method: "remove",
                                          index: index);
                                    },
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              45,
                                          vertical: MediaQuery.of(context)
                                                  .padding
                                                  .vertical /
                                              30),
                                      child: Text(
                                        "-",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                18,
                                            color: Colors.red),
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 5, horizontal: 10),
                                  child: Center(
                                    child: Container(
                                      // width: 15,
                                      alignment: Alignment.center,
                                      child: Text(
                                        cartTileList![index]
                                            .quantity
                                            .toString(),
                                        style: const TextStyle(
                                            fontWeight: FontWeight.w700,
                                            fontSize: 12),
                                      ),
                                    ),
                                  ),
                                ),
                                Material(
                                  // borderRadius: BorderRadius.all(Radius.circular(50)),

                                  color: Colors.transparent,
                                  child: InkWell(
                                    splashColor: Colors.green,
                                    borderRadius:
                                        const BorderRadius.all(Radius.circular(50)),
                                    onTap: () {
                                      updateCart(
                                          cartItem: cartTileList![index],
                                          method: "add");
                                    },
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              55,
                                          vertical: MediaQuery.of(context)
                                                  .padding
                                                  .vertical /
                                              30),
                                      child: Text(
                                        "+",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                18,
                                            color: App24Colors
                                                .greenOrderEssentialThemeColor),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          // const SizedBox(
                          //   width: 10,
                          // ),
                          Material(
                            color: Colors.white,
                            child: InkWell(
                              borderRadius: BorderRadius.circular(10),
                              onTap: () {
                                // print(cartTileList!.length);
                                onTapRemove(index);
                                // cartTileList!.removeAt(index);
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  children: [
                                    Icon(
                                      Icons.delete,
                                      color: const Color(0xff995D5D),
                                      size: MediaQuery.of(context).size.width /
                                          23,
                                    ),
                                    Text(
                                      "Remove",
                                      style: TextStyle(
                                          fontSize: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              31,
                                          fontWeight: FontWeight.bold,
                                          color: const Color(0xff995D5D)),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          )
        : const SizedBox.shrink();
  }

  int? oldQty;
  int? qty;

  Widget buildAddonsList(List<Cartaddon> addonList) {
    List<Widget> list = [];
    addonList.forEach((element) {
      list.add(Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
              child: Text(
            element.addonName!,
            style: const TextStyle(
                color: Color(0xff9B9B9B),
                fontWeight: FontWeight.w500,
                fontSize: 11),
          )),
          Flexible(
            child: Text(
              element.addonQuantity!
                  .substring(0, element.addonQuantity!.length - 3),
              style: const TextStyle(
                  color: Color(0xffFF533C),
                  fontSize: 10,
                  fontWeight: FontWeight.w500),
            ),
          ),
          Text(
            "${cartListResponseData?.userCurrency ?? ''} " +
                (element.addonPrice! *
                        double.parse(element.addonQuantity.toString()))
                    .toString(),
            style: const TextStyle(
                color: Color(0xffFF533C),
                fontSize: 10,
                fontWeight: FontWeight.w500),
          )
        ],
      ));
    });
    return Column(
      children: list,
    );
  }

  updateCart({required Carts cartItem, String? method, index}) async {
    int? qty = cartItem.quantity;
    if (method == 'remove') {
      // print("printing remove qty");
      // print(qty);
      if (qty != 0) qty = qty! - 1;
      // print(qty);
      //showMessage(context, "item(s) removed", false, true);
      if (qty == 0) onTapRemove(index);
    } else {
      // print("printing add qty");
      print(qty);
      oldQty = qty;
      qty = qty! + 1;
      // print(oldQty);
      print("qty :: $qty");

      // if(qty != qty + 1)
      //   CircularProgressIndicator();
      //showMessage(context, "item added", false, true);
    }
    // print(qty);

    Map body = {
      "qty": method == "remove" ? -0 : 0, // "repeat" : product.,
      "item_id": cartItem.product!.id,
      "to_lat":
      toLat ??
          widget.lat ??
          ((selectedAddress == null)
              ? (list?[0].latitude)
              : selectedAddress!.latitude),
      "to_lng": toLng ??
      widget.long ??
          ((selectedAddress == null)
              ? (list?[0].longitude)
              : selectedAddress!.longitude),
      "customize": 1, // "repeat" :0
    };
    if (qty != 0) {
      check() {
        if (oldQty != qty) {
          // print(oldQty != qty);
          showDialog(
              context: context,
              builder: (_) => Container(
                  alignment: Alignment.topCenter,
                  margin: const EdgeInsets.only(top: 20),
                  child: const CircularProgressIndicator(
                    backgroundColor: Colors.grey,
                    color: Colors.purple,
                  )));
        } else {
          return null;
        }
      }
     // check();
      await context.read(cartNotifierProvider.notifier).addCart(
          itemQty: qty as int,
          itemAddons: '',
          context: context,
          itemID: cartItem.product!.id.toString(),
          bodys: body,
          fromCartAdd: true,
          init: true);
      context.read(cartNotifierProvider.notifier).getCartList(
          storeID: widget.isMainStoreID ?? 0,
          isLaundryBasketCard: widget.newCartPage,
          context: context,
          fromLat: widget.lat ??
              context
                  .read(homeNotifierProvider.notifier)
                  .currentLocation
                  .latitude!
                  .toDouble(),
          fromLng: widget.long ??
              context
                  .read(homeNotifierProvider.notifier)
                  .currentLocation
                  .longitude!
                  .toDouble());
      setState(() {
        oldQty = qty;
        // print("printing old quantity" + oldQty.toString());
      });

      // print(oldQty);
      // print(qty);
    }
    // print(oldQty);
    // print(qty);
    // showMessage(context, "msg", true);

    // await context.read(cartCountNotifierProvider.notifier).updateCartCount(
    //    cartStoreId: cartItem.product.id);

    //  reloadData(context: context, init: false);
  }

  checkPaymentAndEmitSocket(
      {required HomeDeliveryOrderInsertModel responseData}) async {
    print("printing entered checkpaymentandemitsocket");
    var prefs = await SharedPreferences.getInstance();
    prefs.setString(SharedPreferencesPath.lastOrderType, "buy");
    prefs.setInt(
        SharedPreferencesPath.lastOrderId, responseData.responseData!.id);
    print("amount outside : " +
        double.parse(prefs
                .getString(SharedPreferencesPath.totalAmountToRazor)
                .toString())
            .toString());
    print(double.parse(responseData.responseData!.totalPayable.toString()) !=
        0.0);
    print(
        " cartListResponseData!.showEstimation :: ${cartListResponseData!.showEstimation}");
    if (useCOD == true) {
      print("printing if useCOD = $useCOD condition for ordersucces check");
      emitSocket(
          orderID: responseData.responseData!.id!,
          cartListResponseData: cartListResponseData);
    } else if (cartListResponseData!.showEstimation == 1 &&
        double.parse(prefs
                .getString(SharedPreferencesPath.totalAmountToRazor)
                .toString()) >
            0.0) {
      print("description : " + 'homedelivery${responseData.responseData!.id}');
      print(double.parse(responseData.responseData!.totalPayable.toString()) !=
          0.0);
      print("amount inside : " +
          double.parse(prefs
                  .getString(SharedPreferencesPath.totalAmountToRazor)
                  .toString())
              .toString());
      print("store_invoice_id : " +
          responseData.responseData!.storeOrderInvoiceId.toString());
      // print("printing razorpay");
      totalAmountToRazor = double.parse(
          prefs.getString(SharedPreferencesPath.totalAmountToRazor).toString());
      Map checkOutOptions = {
        'description': 'homedelivery${responseData.responseData!.id}',
        'amount': totalAmountToRazor,
        // double.parse(responseData.responseData!.totalPayable.toString()),
        // double.parse(responseData.responseData!.totalPayable!),
        "notes": {
          "store_invoice_id": responseData.responseData!.storeOrderInvoiceId,
          "type": "homedelivery",
          "Store_order_id": responseData.responseData!.id
        }
      };
      try {
        print("entering razor pay");
        _razorPay!.open(await getRazorPayOptions(
            context: context, values: checkOutOptions));
        emitSocket(
            orderID: responseData.responseData!.id!,
            cartListResponseData: cartListResponseData);
        print("printing try condition for ordersucces check");
        // prefs.setString(SharedPreferencesPath.totalAmountToRazor, "0.0");
      } catch (e) {
        showLog("emit socket error" + e.toString());
      }
    } else {
      print("printing else condition for ordersucces check");
      emitSocket(
          orderID: responseData.responseData!.id!,
          cartListResponseData: cartListResponseData);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: (widget.newCartPage == true)
          ? PreferredSize(
              preferredSize: const Size(double.infinity, 80),
              child: CusturmAppbar(
                  child: ListTile(
                leading: backarrow(
                  context: context,
                  onTap: () {
                    print("card ::if pop print:: ${widget.isMainStoreID}");
                    if (widget.isMainStoreID == null ||
                        widget.isMainStoreID == 0) {
                      Navigator.pop(context);
                    } else {
                      print("card ::else pop");
                      Navigator.pushReplacement(context, MaterialPageRoute(
                        builder: (context) {
                          return ShopOrderScreen(
                            shopId: widget.isMainStoreID,
                          );
                        },
                      ));
                    }
                  },
                ),
                title: Center(
                  child: Text(Apptext.cart, style: AppTextStyle().appbathead),
                ),
              )),
            )
          : CustomAppBar(
              hideCartIcon: "true",
              title: "Cart",
            ),
      body: OfflineBuilderWidget(
        SafeArea(
          child: Container(
            child: RefreshIndicator(
              onRefresh: () {
                return reloadData(init: false, context: context) ??
                    false as Future<void>;
              },
              child: ProviderListener(
                provider: orderBuyItemsNotifier,
                onChange: (context, dynamic state) {
                  if (state.isLoading!) {
                    showProgress(context);
                  } else if (state.isError!) {
                    Navigator.pop(context);
                    showMessage(context, state.errorMessage!, true, true);
                  } else {
                    print("printing OrderPLaced and socket emiting");
                    Navigator.pop(context);
                    checkPaymentAndEmitSocket(responseData: state.response);

                    // emitSocket(
                    //     orderID: context
                    //         .read(orderBuyItemsNotifier.notifier)
                    //         .state
                    //         .response
                    //         .responseData!
                    //         .id);
                    context.read(cartNotifierProvider.notifier).getCartList(
                        isLaundryBasketCard: widget.newCartPage,
                        context: context,
                        fromLat: widget.lat ??
                            context
                                .read(homeNotifierProvider.notifier)
                                .currentLocation
                                .latitude!
                                .toDouble(),
                        fromLng: widget.long ??
                            context
                                .read(homeNotifierProvider.notifier)
                                .currentLocation
                                .longitude!
                                .toDouble());
                    // if(checkPaymentAndEmitSocket(responseData: state.response))
                    //   print("printing OrderPLaced and socket emiting");
                  }
                },
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Consumer(builder: (context, watch, child) {
                    final state = watch(cartNotifierProvider);
                    print("state. ${state.isError}");
                    if (state.isLoading) {
                      return CartListLoading();
                    } else if (state.isError && (isInitShowError == true)) {
                      if(widget.newCartPage == true) {
                        return locationErrorPage(
                            msg: state.errorMessage.toString());
                      }else{
                     return LoadingError(
                        onPressed: (res) {
                          reloadData(init: true, context: res);
                        },
                        message: state.errorMessage.toString(),
                      );}
                    } else if (state.response != 0) {
                      cartListResponseData = state.response.responseData;
                      cartTileList = cartListResponseData?.carts;
                      updateDetails(responseData: cartListResponseData);

                      if (cartTileList != null && cartTileList!.isEmpty) {
                        // prefs.setInt(SharedPreferencesPath.cartStoreId, 0);
                        return const Center(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                CupertinoIcons.cart_badge_minus,
                                size: 150,
                                color: Colors.grey,
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Text("Your cart is empty.")
                            ],
                          ),
                        );
                      }

                      return Column(
                        children: [
                          Expanded(
                            child: SingleChildScrollView(
                              controller: _scrollController,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(top: 10),
                                    child: Text(
                                      "There are $totalCart item(s) in your cart",
                                      style: TextStyle(
                                          color: App24Colors.darkTextColor,
                                          fontWeight: FontWeight.bold,
                                          fontSize: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              25),
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  // Row(
                                  //   mainAxisAlignment:
                                  //       MainAxisAlignment.spaceBetween,
                                  //   children: [
                                  //     if (cartListResponseData!
                                  //             .showEstimation ==
                                  //         1)
                                  //       Text("Total Amount",
                                  //           style: TextStyle(
                                  //               fontSize: 20,
                                  //               fontWeight: FontWeight.bold)),
                                  //     if (cartListResponseData!
                                  //             .showEstimation ==
                                  //         1)
                                  //       Text(
                                  //           // carts?.totalPrice.toString() ?? "",
                                  //           "AED$totalAmount /-",
                                  //           style: TextStyle(
                                  //               fontSize: 20,
                                  //               color: Color(0xff55708C),
                                  //               fontWeight: FontWeight.bold)),
                                  //   ],
                                  // ),
                                  if (cartTileList != null)
                                    ListView.builder(
                                      itemBuilder: (context, index) =>
                                          buildCartItems(index),
                                      padding: const EdgeInsets.only(
                                        top: 5,
                                      ),
                                      itemCount: cartTileList?.length,
                                      shrinkWrap: true,
                                      physics: const NeverScrollableScrollPhysics(),
                                    ),
                                  const SizedBox(
                                    height: 10,
                                  ),

                                  // Center(
                                  //   child: Text(
                                  //     "Item(s) will be delivered to:",
                                  //     style: TextStyle(
                                  //         color: App24Colors.darkTextColor,
                                  //         fontWeight: FontWeight.bold,
                                  //         fontSize: MediaQuery.of(context)
                                  //                 .size
                                  //                 .width /
                                  //             25),
                                  //   ),
                                  // ),
                                  const SizedBox(
                                    height: 10,
                                  ),
//                                   Row(
//                                     children: [
//                                       Expanded(
//                                           child: Text(
//                                               highPriorityAddress.toString())),
//                                       TextButton(
//                                         style: ElevatedButton.styleFrom(
//                                             primary: App24Colors
//                                                 .greenOrderEssentialThemeColor,
//                                             shape: RoundedRectangleBorder(
//                                                 borderRadius:
//                                                     BorderRadius.circular(20))),
//                                         onPressed: () {
//
// showMessage(context, "adddd", false, false);
//
//                                         },
//                                         child: Text(
//                                           "Add",
//                                           style: TextStyle(color: Colors.white,fontSize: MediaQuery.of(context).size.width/30,fontWeight: FontWeight.w600),
//                                         ),
//                                         // color:
//                                         //     App24Colors.redRestaurantThemeColor,
//                                         // shape: RoundedRectangleBorder(
//                                         //     borderRadius:
//                                         //         BorderRadius.circular(20)),
//                                       ),
//                                       const SizedBox(
//                                         width: 10,
//                                       ),
//                                       if(!highPriorityAddress.toString().contains("No address added!"))
//                                       ElevatedButton(
//                                         onPressed: () {
//
//                                           showMessage(context, "change", false, false);
//                                         },
//                                         style: ElevatedButton.styleFrom(
//                                             primary: App24Colors
//                                                 .greenOrderEssentialThemeColor,
//                                             shape: RoundedRectangleBorder(
//                                                 borderRadius:
//                                                     BorderRadius.circular(20))),
//                                         child: Text(
//                                           "Change",
//                                           style: TextStyle(color: Colors.white,fontSize: MediaQuery.of(context).size.width/30,fontWeight: FontWeight.w600),
//                                         ),
//                                         // color:
//                                         //     App24Colors.redRestaurantThemeColor,
//                                         // shape: RoundedRectangleBorder(
//                                         //     borderRadius:
//                                         //         BorderRadius.circular(20)),
//                                       )
//                                     ],
//                                   ),
                                  const Divider(),
                                  if (cartListResponseData!.showEstimation == 1)
                                    Helpers().getDetailsRow(
                                        "Item Total",
                                        "${cartListResponseData?.userCurrency ?? ''}" +
                                            double.parse(cartListResponseData!
                                                    .totalItemPrice
                                                    .toString())
                                                .toStringAsFixed(2),
                                        context),
                                  if (cartListResponseData!.showEstimation == 1)
                                    const SizedBox(height: 5),

                                  if (cartListResponseData!.showEstimation == 1)
                                    // if (highPriorityAddress != "Select an address")
                                    Helpers().getDetailsRow(
                                        "Delivery charge",
                                        "${cartListResponseData?.userCurrency ?? ''}" +
                                            double.parse(cartListResponseData!
                                                    .deliveryCharges
                                                    .toString())
                                                .toStringAsFixed(2),
                                        context),
                                  if (highPriorityAddress ==
                                          "Select an address" ||
                                      highPriorityAddress == '' ||
                                      highPriorityAddress == null)
                                    const Text(
                                      "(Delivery charge will be updated after selecting the address)",
                                      style: TextStyle(
                                          fontSize: 10,
                                          color: App24Colors
                                              .redRestaurantThemeColor),
                                    ),
                                  // Text("For exact delivery charge, please select your address"),
                                  if (cartListResponseData!.discount != null)
                                    // if (cartListResponseData!
                                    //     .promocode!.isEmpty)
                                    Helpers().getDetailsRow(
                                        "Discount",
                                        cartListResponseData?.userCurrency ??
                                            '' +
                                                double.parse(
                                                        cartListResponseData
                                                                ?.discount ??
                                                            "0.00".toString())
                                                    .toStringAsFixed(2),
                                        context),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  if (cartListResponseData!.shopGstAmount != 0)
                                    Helpers().getDetailsRow(
                                        "Shop GST",
                                        "${cartListResponseData?.userCurrency ?? ''} ${cartListResponseData!.shopGstAmount}",
                                        context),
                                  //For Future purpose

                                  // if (cartListResponseData!.showEstimation == 1)
                                  //   Form(
                                  //     key: _formKey,
                                  //     child: TextFormField(
                                  //       controller: couponController,
                                  //       validator: (String? arg) {
                                  //         if (arg!.length > 0)
                                  //           return 'Invalid Coupon Code.';
                                  //         else if (arg.length == 0)
                                  //           return "Please enter coupon code";
                                  //       },
                                  //       // maxLines: 1,
                                  //       decoration: InputDecoration(
                                  //         suffixIcon: MaterialButton(
                                  //           onPressed: () {
                                  //             _formKey.currentState!.validate();
                                  //           },
                                  //           shape: RoundedRectangleBorder(
                                  //               borderRadius: BorderRadius.only(
                                  //                   topRight:
                                  //                       Radius.circular(10),
                                  //                   bottomRight:
                                  //                       Radius.circular(10))),
                                  //           child: Text("Apply"),
                                  //         ),
                                  //         contentPadding: EdgeInsets.only(
                                  //             /*bottom: 20,*/
                                  //             left: 20),
                                  //         hintText: "Enter coupon code here",
                                  //         hintStyle: TextStyle(
                                  //           fontSize: 13,
                                  //         ),
                                  //         focusedErrorBorder: OutlineInputBorder(
                                  //             borderRadius:
                                  //                 BorderRadius.circular(10),
                                  //             borderSide: BorderSide(
                                  //                 color: App24Colors
                                  //                     .redRestaurantThemeColor
                                  //                     .withOpacity(0.2))),
                                  //         errorBorder: OutlineInputBorder(
                                  //             borderRadius:
                                  //                 BorderRadius.circular(10),
                                  //             borderSide: BorderSide(
                                  //                 color: App24Colors
                                  //                     .redRestaurantThemeColor
                                  //                     .withOpacity(0.2))),
                                  //         enabledBorder: OutlineInputBorder(
                                  //             borderRadius:
                                  //                 BorderRadius.circular(10),
                                  //             borderSide: BorderSide(
                                  //                 color: App24Colors
                                  //                     .greenOrderEssentialThemeColor
                                  //                     .withOpacity(0.2))),
                                  //         focusedBorder: OutlineInputBorder(
                                  //             borderRadius:
                                  //                 BorderRadius.circular(10),
                                  //             borderSide: BorderSide(
                                  //                 color: App24Colors
                                  //                     .greenOrderEssentialThemeColor
                                  //                     .withOpacity(0.2))),
                                  //       ),
                                  //     ),
                                  //   ),
                                  // if (cartListResponseData!.showEstimation == 1)
                                  //   Padding(
                                  //     padding: const EdgeInsets.fromLTRB(
                                  //         0, 10, 10, 0),
                                  //     child: Row(
                                  //       mainAxisAlignment:
                                  //           MainAxisAlignment.end,
                                  //       children: [
                                  //         RichText(
                                  //             text: TextSpan(
                                  //                 text: "See all coupons",
                                  //                 style: TextStyle(
                                  //                   color: Colors.blue,
                                  //                   fontSize: 11,
                                  //                 ),
                                  //                 recognizer:
                                  //                     TapGestureRecognizer()
                                  //                       ..onTap = () async {
                                  //                         // print(
                                  //                         //     widget.promoCode);
                                  //                         var selectedCoupon =
                                  //                             await Helpers()
                                  //                                 .getCommonBottomSheet(
                                  //                                     context:
                                  //                                         context,
                                  //                                     content:
                                  //                                         AddCouponBottomSheet(
                                  //                                       cartShopID:
                                  //                                           cartListResponseData!.storeId,
                                  //                                       promoCode:
                                  //                                           widget.promoCode,
                                  //                                       promoCodeDetails:
                                  //                                           widget.promoCodeDetails,
                                  //                                     ),
                                  //                                     title:
                                  //                                         "Coupon Codes");
                                  //                         if (selectedCoupon !=
                                  //                             null) {
                                  //                           PromoCodesResponseData
                                  //                               selectedPromoCode =
                                  //                               selectedCoupon;
                                  //                           setState(() {
                                  //                             couponController
                                  //                                     .text =
                                  //                                 selectedPromoCode
                                  //                                     .promoCode!;
                                  //                           });
                                  //                         }
                                  //                       })),
                                  //       ],
                                  //     ),
                                  //   ),
                                  if (cartListResponseData!.showEstimation == 1)
                                    if (cartListResponseData!.discount <
                                        cartListResponseData!.promocodeAmount)
                                      if (cartListResponseData!
                                          .promocode!.isNotEmpty)
                                        Row(
                                          children: <Widget>[
                                            /* Text(
                              "Promocode applied",
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 14),
                            ),*/

                                            Expanded(
                                              child: Text(
                                                "\"" +
                                                    cartListResponseData!
                                                        .promocode
                                                        .toString()
                                                        .toUpperCase() +
                                                    "\" offer applied on this order",
                                                textAlign: TextAlign.end,
                                                style: const TextStyle(
                                                    fontWeight: FontWeight.w600,
                                                    color:
                                                        App24Colors.lightBlue,
                                                    fontSize: 14),
                                              ),
                                            ),
                                          ],
                                        ),
                                  if (cartListResponseData!.showEstimation == 1)
                                    if (cartListResponseData!.discount <
                                        cartListResponseData!.promocodeAmount)
                                      if (cartListResponseData!
                                          .promocode!.isNotEmpty)
                                        Padding(
                                          padding: const EdgeInsets.only(top: 5),
                                          child: Row(
                                            children: <Widget>[
                                              /* Text(
                              "Promocode applied",
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 14),
                            ),*/

                                              Expanded(
                                                child: Text(
                                                  cartListResponseData
                                                          ?.userCurrency ??
                                                      '' +
                                                          cartListResponseData!
                                                              .promocodeAmount
                                                              .toString() +
                                                          " discounted on your bill",
                                                  textAlign: TextAlign.end,
                                                  style: const TextStyle(
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      color:
                                                          App24Colors.lightBlue,
                                                      fontSize: 14),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  if (cartListResponseData!.showEstimation !=
                                          1 &&
                                      cartListResponseData!.carts!.isNotEmpty)
                                    Container(
                                      padding: const EdgeInsets.all(20),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(20),
                                        color: const Color(0xffFFFABD),
                                      ),
                                      child: const Text(
                                        "Prices will be updated once the purchase is completed at the shop. You shall be alerted for the payment when the bill is ready.",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Color(0xffB7791B),
                                            fontWeight: FontWeight.w800),
                                      ),
                                    ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  (widget.newCartPage == true)
                                      ? const Divider()
                                      : const SizedBox.shrink(),
                                  (widget.newCartPage == true)
                                      ? selectAddressWidget()
                                      : const SizedBox.shrink(),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                ],
                              ),
                            ),
                          ),

                          //bottom Detivery to
                          Container(
                            /*height: 100,*/
                            decoration: BoxDecoration(
                                border: Border(
                                    top: BorderSide(color: Colors.grey[300]!))),
                            padding:
                                const EdgeInsets.only(top: 10.0, bottom: 10.0),
                            child: Column(
                              // crossAxisAlignment: CrossAxisAlignment.stretch,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  color: Colors.transparent,
                                  child: Column(
                                    // mainAxisSize: MainAxisSize.min,
                                    // crossAxisAlignment:
                                    // CrossAxisAlignment.start,
                                    children: [
                                      (widget.newCartPage == true)
                                          ? const SizedBox.shrink()
                                          : Center(
                                              child: Text(
                                                "Deliver to:",
                                                style: TextStyle(
                                                    color: App24Colors
                                                        .darkTextColor,
                                                    fontWeight: FontWeight.bold,
                                                    fontSize:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width /
                                                            25),
                                              ),
                                            ),

                                      (widget.newCartPage == true)
                                          ? const SizedBox.shrink()
                                          : Row(
                                              children: [
                                                Expanded(
                                                    child: Text(
                                                        highPriorityAddress
                                                            .toString())),
                                                TextButton(
                                                  // style: ElevatedButton.styleFrom(
                                                  //     primary: App24Colors
                                                  //         .greenOrderEssentialThemeColor,
                                                  //     shape: RoundedRectangleBorder(
                                                  //         borderRadius:
                                                  //         BorderRadius.circular(20))),
                                                  onPressed: () async {
                                                    // Helpers().getCommonBottomSheet(context: context, content: SearchLocationBottomSheetPage(initialCall: true,title: GlobalConstants.labelDropLocation,));
                                                    Map value =
                                                        await showSelectLocationSheetForAddress(
                                                            context: context,
                                                            title: GlobalConstants
                                                                .labelFromCart);
                                                    // print("printing checking whether address came to cart");
                                                    // print(addressFromSearch!["to_adrs"]);
                                                    setState(() {
                                                      highPriorityAddress =
                                                          value["to_adrs"]
                                                              .toString();
                                                      // print("printing highPriorityAddress : "+highPriorityAddress.toString());
                                                      // print("printing highPriorityAddress : "+highPriorityAddress.toString());

                                                      AddressResponseData
                                                          addressData =
                                                          new AddressResponseData(
                                                              latitude: value[
                                                                  "to_lat"],
                                                              longitude: value[
                                                                  'to_lng']);
                                                      selectedAddress =
                                                          addressData;
                                                      context
                                                          .read(
                                                              cartNotifierProvider
                                                                  .notifier)
                                                          .getCartList(
                                                              isLaundryBasketCard:
                                                                  widget
                                                                      .newCartPage,
                                                              fromLat: widget
                                                                      .lat ??
                                                                  value[
                                                                      "to_lat"],
                                                              fromLng: widget
                                                                      .long ??
                                                                  value[
                                                                      "to_lng"],
                                                              context: context);
                                                    });

                                                    // Navigator.push(
                                                    //     context,
                                                    //     MaterialPageRoute(
                                                    //         builder: (context) =>
                                                    //             SelectLocationAddressPage(
                                                    //               title: GlobalConstants
                                                    //                   .labelDropLocation,
                                                    //               isCurrentLocation:
                                                    //                   true,
                                                    //               fromManageAddress:
                                                    //                   "fromManageAddress",
                                                    //               fromCart:
                                                    //                   "true",
                                                    //             )));
                                                  },
                                                  child: Text(
                                                    "Add",
                                                    style: TextStyle(
                                                        color: App24Colors
                                                            .greenOrderEssentialThemeColor,
                                                        fontSize: MediaQuery.of(
                                                                    context)
                                                                .size
                                                                .width /
                                                            30,
                                                        fontWeight:
                                                            FontWeight.w600),
                                                  ),
                                                  // color:
                                                  //     App24Colors.redRestaurantThemeColor,
                                                  // shape: RoundedRectangleBorder(
                                                  //     borderRadius:
                                                  //         BorderRadius.circular(20)),
                                                ),
                                                const SizedBox(
                                                  width: 10,
                                                ),
                                                if (!highPriorityAddress
                                                    .toString()
                                                    .contains(
                                                        "No address added!"))
                                                  TextButton(
                                                    onPressed: () async {
                                                      var values = await Helpers()
                                                          .getCommonBottomSheet(
                                                              context: context,
                                                              content:
                                                                  const AddressConfirmationBottomSheet());
                                                      print(
                                                          "address form confirmation");
                                                      print(values);

                                                      if (values.latitude !=
                                                          null) {
                                                        AddressResponseData
                                                            value = values;
                                                        print(
                                                            value.addressType);
                                                        if (value.longitude !=
                                                            null) {
                                                          context.read(cartNotifierProvider.notifier).getCartList(
                                                              isLaundryBasketCard:
                                                                  widget
                                                                      .newCartPage,
                                                              context: context,
                                                              fromLat: widget
                                                                      .lat ??
                                                                  value
                                                                      .latitude!
                                                                      .toDouble(),
                                                              fromLng: widget
                                                                      .long ??
                                                                  value
                                                                      .longitude!
                                                                      .toDouble());
                                                          String address = value
                                                                  .flatNo! +
                                                              " , " +
                                                              value.street
                                                                  .toString() +
                                                              " , " +
                                                              value.city
                                                                  .toString();
                                                          setState(() {
                                                            highPriorityAddress =
                                                                address;
                                                            selectedAddress =
                                                                value;
                                                          });
                                                        }
                                                      } else if (values[
                                                              "to_adrs"] !=
                                                          null) {
                                                        Map value = values;
                                                        setState(() {
                                                          highPriorityAddress =
                                                              value["to_adrs"];
                                                        });
                                                      }
                                                    },

                                                    // style: ElevatedButton.styleFrom(
                                                    //     primary: App24Colors
                                                    //         .greenOrderEssentialThemeColor,
                                                    //     shape: RoundedRectangleBorder(
                                                    //         borderRadius:
                                                    //         BorderRadius.circular(20))),
                                                    child: Text(
                                                      highPriorityAddress ==
                                                              "Select an address"
                                                          ? "Select"
                                                          : "Change",
                                                      style: TextStyle(
                                                          color: App24Colors
                                                              .greenOrderEssentialThemeColor,
                                                          fontSize: MediaQuery.of(
                                                                      context)
                                                                  .size
                                                                  .width /
                                                              30,
                                                          fontWeight:
                                                              FontWeight.w600),
                                                    ),
                                                    // color:
                                                    //     App24Colors.redRestaurantThemeColor,
                                                    // shape: RoundedRectangleBorder(
                                                    //     borderRadius:
                                                    //         BorderRadius.circular(20)),
                                                  )
                                              ],
                                            ),
                                      // Row(
                                      //   mainAxisAlignment:
                                      //       MainAxisAlignment.spaceBetween,
                                      //   children: [
                                      //     Text(
                                      //       "Total Amount",
                                      //       style: TextStyle(
                                      //           fontSize:
                                      //               MediaQuery.of(context)
                                      //                       .size
                                      //                       .width /
                                      //                   18,
                                      //           fontWeight:
                                      //               FontWeight.bold),
                                      //     ),
                                      //     Text(
                                      //       "AED$totalAmount /-",
                                      //       style: TextStyle(
                                      //           fontSize:
                                      //               MediaQuery.of(context)
                                      //                       .size
                                      //                       .width /
                                      //                   18,
                                      //           fontWeight: FontWeight.bold,
                                      //           color: App24Colors
                                      //               .darkTextColor),
                                      //     ),
                                      //   ],
                                      // ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        // mainAxisAlignment: MainAxisAlignment.spaceBetween,              // crossAxisAlignment: CrossAxisAlignment.stretch,
                                        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Transform.scale(
                                              scale: 1.1,
                                              child: Checkbox(
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15)),
                                                checkColor: Colors.white,
                                                activeColor: App24Colors
                                                    .greenOrderEssentialThemeColor,
                                                value: useCOD,
                                                onChanged: (bool? value) {
                                                  setState(() {
                                                    useCOD = value!;
                                                    if (useCOD == true) {
                                                      useWallet = false;
                                                    }
                                                  });
                                                },
                                              )),
                                          const Text(
                                            "Cash on Delivery (COD)",
                                            style: TextStyle(
                                                fontSize: 15,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          const SizedBox(
                                            width: 5,
                                          )
                                        ],
                                      ),

                                      if (cartListResponseData!
                                                  .showEstimation ==
                                              1 &&
                                          cartListResponseData!
                                                  .userWalletBalance !=
                                              0)
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          // mainAxisAlignment: MainAxisAlignment.spaceBetween,              // crossAxisAlignment: CrossAxisAlignment.stretch,
                                          // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            Transform.scale(
                                                scale: 1.1,
                                                child: Checkbox(
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              15)),
                                                  checkColor: Colors.white,
                                                  activeColor: App24Colors
                                                      .greenOrderEssentialThemeColor,
                                                  value: /*(useCOD == true)
                                                      ? false
                                                      : */
                                                      useWallet,
                                                  /* value: lockWallet
                                                      ? true
                                                      : useWallet,*/
                                                  onChanged:
                                                      /*lockWallet
                                        ? null
                                        :*/
                                                      (bool? value) {
                                                    // context.read(cartNotifierProvider.notifier).getCartList(
                                                    //     context: context,
                                                    //     lat: context
                                                    //         .read(homeNotifierProvider.notifier)
                                                    //         .currentLocation
                                                    //         .latitude!
                                                    //         .toDouble(),
                                                    //     lng: context
                                                    //         .read(homeNotifierProvider.notifier)
                                                    //         .currentLocation
                                                    //         .longitude!
                                                    //         .toDouble());
                                                    setState(() {
                                                      // if(widget.cartList.userWalletBalance < 0){
                                                      //   useWallet = true;
                                                      // }else {
                                                      useWallet = value!;
                                                      if (useWallet == true) {
                                                        useCOD = false;
                                                      }
                                                      // }
                                                      // print(getTotalAmount());
                                                    });
                                                  },
                                                )),

                                            // Flexible(
                                            //   child: Row(
                                            //     children: [
                                            //       Text("Use",
                                            //           style: TextStyle(
                                            //               fontSize: 15,
                                            //               fontWeight: FontWeight.bold)),
                                            //       const SizedBox(
                                            //         width: 5,
                                            //       ),
                                            //       useWallet == false
                                            //           ? Text("(AED0)",
                                            //           style: TextStyle(
                                            //               fontSize: 18,
                                            //               fontWeight: FontWeight.bold,
                                            //               color: App24Colors
                                            //                   .redRestaurantThemeColor),
                                            //       overflow: TextOverflow.clip,)
                                            //           : Expanded(
                                            //             child: Text(
                                            //             " (AED " +
                                            //                 (widget.cartList.totalPrice! +
                                            //                     widget
                                            //                         .cartList.shopGstAmount)
                                            //                     .toString() +
                                            //                 ")",
                                            //             style: TextStyle(
                                            //                 fontSize: 18,
                                            //                 fontWeight: FontWeight.bold,
                                            //                 color: App24Colors
                                            //                     .redRestaurantThemeColor),
                                            //       overflow: TextOverflow.clip,),
                                            //           ),
                                            //     ],
                                            //   ),
                                            // ),
                                            // const SizedBox(
                                            //   width: 15,
                                            // ),

                                            // Checkbox(
                                            //   checkColor: Colors.greenAccent,
                                            //   activeColor: Colors.red,
                                            //   value: this.useWallet,
                                            //   onChanged: (bool? value) {
                                            //     setState(() {
                                            //       this.useWallet = value;
                                            //     });
                                            //   },
                                            // ),
                                            Row(
                                              children: [
                                                Text(
                                                  /*cartListResponseData!
                                                              .userWalletBalance <
                                                          0
                                                      ? "Negative Wallet"
                                                      :*/ "Wallet Amount",
                                                  style: TextStyle(
                                                      fontSize: 15,
                                                      color: cartListResponseData!
                                                          .userWalletBalance <
                                                          0
                                                          ? App24Colors.redRestaurantThemeColor:Colors.black,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                const SizedBox(
                                                  width: 5,
                                                ),
                                                Text(
                                                  "(${cartListResponseData?.userCurrency ?? ''}${cartListResponseData!.userWalletBalance.toString()})",
                                                  // useWallet == false
                                                  //     ? "(AED" +
                                                  //     widget.cartList.userWalletBalance
                                                  //         .toString() +
                                                  //     ")"
                                                  //     : ("(AED" +
                                                  //     (widget.cartList
                                                  //         .userWalletBalance! -
                                                  //         widget
                                                  //             .cartList.totalPrice!)
                                                  //         .toString()) +
                                                  //     ")",
                                                  style: TextStyle(
                                                      fontSize: 18,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color:cartListResponseData!
                                                          .userWalletBalance <
                                                          0
                                                          ? App24Colors.redRestaurantThemeColor: App24Colors
                                                          .greenOrderEssentialThemeColor),
                                                  overflow: TextOverflow.clip,
                                                ),
                                              ],
                                            )
                                          ],
                                        ),
                                    ],
                                  ),
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    if (cartListResponseData!
                                                .userWalletBalance >=
                                            0 &&
                                        cartListResponseData!.showEstimation ==
                                            1)
                                      // if (highPriorityAddress != "Select an address")
                                      Flexible(
                                        child: Text(
                                          useWallet == true
                                              ? "To pay : " +
                                                  "${cartListResponseData?.userCurrency ?? ''}" +
                                                  getTotalAmount(
                                                      cartListResponseData!)
                                              : "To pay : " +
                                                  "${cartListResponseData?.userCurrency ?? ''}" + /*totalAmount.toString()*/
                                                  (double.parse(
                                                          cartListResponseData!
                                                              .totalPrice!
                                                              .toString()) /* - cartListResponseData!.deliveryCharges*/)
                                                      .toString()

                                          /*+
                                        double.parse(cart!.shopGstAmount
                                            .toString()))
                                        .toStringAsFixed(2)*/
                                          ,
                                          style: TextStyle(
                                              fontSize: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  18,
                                              fontWeight: FontWeight.bold,
                                              color: const Color(0xff55708C)),
                                        ),
                                      ),

                                    // Flexible(
                                    //   child: Text(
                                    //     useWallet == true
                                    //         ? "To pay : " +
                                    //             "AED" +
                                    //             getTotalAmount(
                                    //                 cartListResponseData!)
                                    //         : "To pay : " +
                                    //             "AED" + /*totalAmount.toString()*/
                                    //             double.parse(
                                    //                     cartListResponseData!
                                    //                         .totalPrice!
                                    //                         .toString())
                                    //                 .toString()
                                    //     /*+
                                    //   double.parse(cart!.shopGstAmount
                                    //       .toString()))
                                    //   .toStringAsFixed(2)*/
                                    //     ,
                                    //     style: TextStyle(
                                    //         fontSize: MediaQuery.of(context)
                                    //                 .size
                                    //                 .width /
                                    //             18,
                                    //         fontWeight: FontWeight.bold,
                                    //         color: Color(0xff55708C)),
                                    //   ),
                                    // ),

                                    // Text(
                                    //   "To pay : "+"AED$totalAmount /-",
                                    //   style: TextStyle(
                                    //       fontSize:
                                    //       MediaQuery.of(context)
                                    //           .size
                                    //           .width /
                                    //           18,
                                    //       fontWeight: FontWeight.bold,
                                    //       color: App24Colors
                                    //           .darkTextColor),
                                    // ),
                                    if (cartListResponseData!.showEstimation ==
                                        1)
                                      const SizedBox(
                                        width: 15,
                                      ),
                                    Expanded(
                                      child: ElevatedButton(
                                        style: ElevatedButton.styleFrom(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 15, horizontal: 20),
                                            backgroundColor: App24Colors
                                                .greenOrderEssentialThemeColor,
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(25.0),
                                            )),
                                        onPressed: () {
                                          if (highPriorityAddress == null ||
                                              highPriorityAddress!.contains(
                                                  "Select an address") ||
                                              highPriorityAddress == '' || context.read(cartNotifierProvider.notifier).errorMsg.toLowerCase().contains("collection address")) {
                                            showMessage(
                                                context,
                                                "Please select your collection address",
                                                true,
                                                false);
                                            _scrollController.animateTo(
                                              _scrollController.position.maxScrollExtent,
                                              duration: const Duration(milliseconds: 500),
                                              curve: Curves.easeInOut,
                                            );
                                          } else if (highPriorityAddress!
                                              .contains("No address added!")) {
                                            showMessage(
                                                context,
                                                "Please add your collection address",
                                                true,
                                                false);
                                            _scrollController.animateTo(
                                              _scrollController.position.maxScrollExtent,
                                              duration: const Duration(milliseconds: 500),
                                              curve: Curves.easeInOut,
                                            );
                                          } else if (toAddress == null ||
                                              toAddress == '' || context.read(cartNotifierProvider.notifier).errorMsg.toLowerCase().contains("delivery address")) {
                                            showMessage(
                                                context,
                                                "Please add your delivery address",
                                                true,
                                                false);
                                            _scrollController.animateTo(
                                              _scrollController.position.maxScrollExtent,
                                              duration: const Duration(milliseconds: 500),
                                              curve: Curves.easeInOut,
                                            );
                                          } else if (useCOD == false &&
                                              useWallet == false) {
                                            showMessage(
                                                context,
                                                "Please select your payment type",
                                                true,
                                                false);
                                          } else {
                                            if (int.parse(cartListResponseData!
                                                    .cartMinAmount
                                                    .toString()) <
                                                cartListResponseData!
                                                    .totalPrice) {
                                              checkOut();
                                              print((int.parse(
                                                          cartListResponseData!
                                                              .cartMinAmount
                                                              .toString()) <
                                                      cartListResponseData!
                                                          .totalPrice)
                                                  .toString());
                                            } else {
                                              print((int.parse(
                                                          cartListResponseData!
                                                              .cartMinAmount
                                                              .toString()) <
                                                      cartListResponseData!
                                                          .totalPrice)
                                                  .toString());
                                              Helpers().getCommonAlertDialogBox(
                                                  content: AlertDialog(
                                                    shape:
                                                        RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        15)),
                                                    content: Text(
                                                        "A minimum amount of  ${cartListResponseData?.userCurrency ?? ''}${cartListResponseData!.cartMinAmount} is required inorder to place order from this shop"),
                                                    actions: [
                                                      MaterialButton(
                                                        onPressed: () {
                                                          Navigator.pop(
                                                              context);
                                                        },
                                                        child: const Text("Ok"),
                                                      )
                                                    ],
                                                  ),
                                                  context: context);
                                            }
                                          }
                                        },
                                        child: Text(
                                          "Proceed",
                                          style: TextStyle(
                                              fontSize: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  22,
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ],
                      );
                    } else {
                      return locationErrorPage();
                    }
                  }),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
  String errorMsg(message){
    String msg =  message;
    SchedulerBinding.instance.addPostFrameCallback((_) {
    setState(() {
      if(message != null || message != '') {
        msg = message;
      }else {
        msg = '';
      }
    });
    });
    return msg;
  }

Widget locationErrorPage({String? msg}){
    return Center(
      child: Padding(
        padding: const EdgeInsets.only(bottom: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Center(
              child: Column(
                children: [
                   Text(
          (widget.newCartPage == true)? "Selected Location too far":"Drop Location too far",
                    style: const TextStyle(
                        fontWeight: FontWeight.w800),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    (widget.newCartPage == true)? (msg ?? ""):"Please select a nearby drop point",
                    style: const TextStyle(
                        fontWeight: FontWeight.w600),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            new MaterialButton(
              padding: const EdgeInsets.symmetric(vertical: 15),
              color: App24Colors.redRestaurantThemeColor,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15)),
              child: const Text(
                "Change location",
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () async {
                // Navigator.pop(context);

                var address = await Helpers()
                    .getCommonBottomSheet(
                    context: context,
                    content:
                    const AddressConfirmationBottomSheet());
                if (address != null) {
                  AddressResponseData value = address;
                  setState(() {
                    highPriorityAddress = value.flatNo! +
                        " , " +
                        value.street.toString() +
                        " , " +
                        value.city.toString();
                    selectedAddress = value;
                  });
                  context
                      .read(cartNotifierProvider.notifier)
                      .getCartList(
                      isLaundryBasketCard:
                      widget.newCartPage,
                      context: context,
                      fromLat:
                      widget.lat ?? value.latitude!,
                      fromLng: widget.long ??
                          value.longitude!);
                }
                // selectLocation(
                //     GlobalConstants.labelDropLocation);

                // showMessage(
                //     context, "Please change your location", true, false);
              }
              // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomePage()));}
              /*  Navigator.push(context,
                                      MaterialPageRoute(builder: (context) => LogInPage()))*/
              ,
            ),
            const SizedBox(
              height: 10,
            ),
            new MaterialButton(
              padding: const EdgeInsets.symmetric(vertical: 15),
              color: App24Colors.redRestaurantThemeColor,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15)),
              child: const Text(
                "Clear cart",
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                onClearCart();
                // Navigator.pop(context);
                // context
                //     .read(cartNotifierProvider.notifier)
                //     .getClearCart(context: context);
              }
              /*  Navigator.push(context,
                                      MaterialPageRoute(builder: (context) => LogInPage()))*/
              ,
            )
          ],
        ),
      ),
    );
}

  Widget selectAddressWidget() {
    return Container(
      decoration: BoxDecoration(
          color: AppColor.servicetext1.withOpacity(0.16),
          borderRadius: BorderRadius.circular(14.0)),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          addressCommonCard(
            showLoader: context.read(cartNotifierProvider.notifier).isFromAdsLoad,
            onTap: () async {
              var values = await Helpers().getCommonBottomSheet(
                  context: context, content: const AddressConfirmationBottomSheet());
              print("address form confirmation");
              print(values);

              if (values.latitude != null) {
                AddressResponseData value = values;
                print(value.addressType);
                if (value.longitude != null) {
                  setState(() {
                    isInitShowError = false;
                    context.read(cartNotifierProvider.notifier).isFromAdsLoad = true;
                  });
                  context.read(cartNotifierProvider.notifier).getCartList(
                    init: false,
                      isLaundryBasketCard: widget.newCartPage,
                      context: context,
                      fromLat: widget.lat ?? value.latitude!.toDouble(),
                      fromLng: widget.long ?? value.longitude!.toDouble());
                  String address = value.flatNo! +
                      " , " +
                      value.street.toString() +
                      " , " +
                      value.city.toString();
                    setState(() {
                      highPriorityAddress = address;
                      selectedAddress = value;
                      context.read(cartNotifierProvider.notifier).isFromAdsLoad = false;
                    });
                }
              } else if (values["to_adrs"] != null) {
                Map value = values;
                setState(() {
                  highPriorityAddress = value["to_adrs"];
                });
              }
            },
            address: widget.fromAddress ?? highPriorityAddress,
            addressType: "Collect From",
          ),
          if(context.read(cartNotifierProvider.notifier).errorMsg.toLowerCase().contains("collection address"))
            Padding(
              padding: const EdgeInsets.symmetric( horizontal: 14.0),
              child: Text(context.read(cartNotifierProvider.notifier).errorMsg,
                style: const TextStyle(
                    fontSize: 10,
                    color: Colors.redAccent
                ),),
            ),
          const SizedBox(
            height: 10,
          ),
          const Divider(),
          addressCommonCard(
            showLoader: context.read(cartNotifierProvider.notifier).isToAdsLoad,
            onTap: () async {
              if (isSameAddress == false) {
                var values = await Helpers().getCommonBottomSheet(
                    context: context,
                    content: const AddressConfirmationBottomSheet());
                print("address form confirmation");
                print(values);

                if (values.latitude != null) {
                  AddressResponseData value = values;
                  print(value.addressType);
                  setState(() {
                    context.read(cartNotifierProvider.notifier).isToAdsLoad = true;
                  });
                  if (value.longitude != null) {
                    context.read(cartNotifierProvider.notifier).getCartList(
                        init: false,
                        isLaundryBasketCard: widget.newCartPage,
                        context: context,
                        fromLat: widget.lat ?? selectedAddress?.latitude,
                        fromLng: widget.long ?? selectedAddress?.longitude,
                        toLat: toLat ?? value.latitude,
                        toLng: toLng ?? value.longitude);
                    String address = value.flatNo! +
                        " , " +
                        value.street.toString() +
                        " , " +
                        value.city.toString();
                    setState(() {
                        toAddress = address;
                      toLat = value.latitude;
                      toLng = value.longitude;
                      isInitShowError = false;
                        context.read(cartNotifierProvider.notifier).isToAdsLoad = false;
                      // selectedAddress = value;
                    });
                  }
                } else if (values["to_adrs"] != null) {
                  Map value = values;
                  setState(() {
                    toAddress = value["to_adrs"];
                  });
                }
              }
            },
            address: toAddress,
            addressType: "Deliver To",
            //enableColor: (isSameAddress == true)?AppColor.dividerColor.withOpacity(0.16): AppColor.white
          ),
          if(context.read(cartNotifierProvider.notifier).errorMsg.toLowerCase().contains("delivery address"))
          Padding(
            padding: const EdgeInsets.symmetric( horizontal: 14.0),
            child: Text(context.read(cartNotifierProvider.notifier).errorMsg,
            style: const TextStyle(
              fontSize: 10,
              color: Colors.redAccent
            ),),
          ),
          const SizedBox(
            height: 10,
          )
        ],
      ),
    );
  }

  addressCommonCard(
      {String? addressType,
      String? address,
      Function()? onTap,
        bool? showLoader = false,
      Color? enableColor}) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        const SizedBox(
          height: 10,
        ),
        Center(
          child: Text(
            addressType ?? '',
            style: TextStyle(
                color: App24Colors.darkTextColor,
                fontWeight: FontWeight.bold,
                fontSize: MediaQuery.of(context).size.width / 25),
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12.0),
          child: GestureDetector(
            onTap: onTap,
            child: Card(
              surfaceTintColor: AppColor.transparent,
              color: enableColor ?? AppColor.white,
              child: Container(
                height: 70,
                decoration: BoxDecoration(
                    color: AppColor.transparent,
                    borderRadius: BorderRadius.circular(14.0)),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                          child: (addressType == 'Deliver To')
                              ? (address == null ||
                                      address == '' ||
                                      isSameAddress == true)
                                  ? Center(
                                      child: Row(
                                          mainAxisSize: MainAxisSize.min,
                                          children: [
                                            Checkbox(
                                                visualDensity: const VisualDensity(
                                                    horizontal: -4,
                                                    vertical: 0.0),
                                                value: isSameAddress,
                                                activeColor:
                                                    AppColor.carvebarcolor,
                                                onChanged: (val) {
                                                  setState(() {
                                                    isInitShowError = false;
                                                    isSameAddress = val;
                                                    if (isSameAddress!) {
                                                      context.read(cartNotifierProvider.notifier).isToAdsLoad = true;
                                                    }
                                                  });
                                                  if (isSameAddress!) {
                                                    context
                                                        .read(
                                                        cartNotifierProvider
                                                            .notifier)
                                                        .getCartList(
                                                      init: false,
                                                      isLaundryBasketCard:
                                                      widget
                                                          .newCartPage,
                                                      context: context,
                                                      fromLat:
                                                      selectedAddress
                                                          ?.latitude,
                                                      fromLng:
                                                      selectedAddress
                                                          ?.longitude,
                                                    );
                                                    setState(() {
                                                      context.read(cartNotifierProvider.notifier).isToAdsLoad = false;
                                                      toAddress =
                                                          highPriorityAddress ??
                                                              widget
                                                                  .fromAddress;
                                                      toLat = widget.lat ??selectedAddress?.latitude;
                                                      toLng = widget.long ??selectedAddress?.longitude;
                                                    });
                                                  } else {
                                                    toAddress = null;
                                                  }
                                                }),
                                            //SizedBox(width: 10),
                                            const Text(
                                              'Same as Collection Address',
                                            ),
                                          ]),
                                    )
                                  : Text(
                                      address,
                                      maxLines: 3,
                                      overflow: TextOverflow.ellipsis,
                                    )
                              : Text(
                                  address ?? 'Select your collection address',
                                  maxLines: 3,
                                  overflow: TextOverflow.ellipsis,
                                ),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      (showLoader == true)?
                          Container(
                            height: 24,width: 24,
                              color: AppColor.transparent,
                              child: const CircularProgressIndicator.adaptive())
                      :Icon(
                        Icons.edit_location_outlined,
                        color: AppColor.servicetext1,
                        size: 24,
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }


  Future<void> getAddressWithHighestPriority() async {
    // print("printing get addresses cart");

    String addressString = '';
    AddressDatabase addressDatabase =
        new AddressDatabase(DatabaseService.instance);
    //final list = await addressDatabase.fetchAddresses();
    List<AddressResponseData> abc = await addressDatabase.fetchAddresses();

    setState(() {
      list = abc;
    });
    // final address;
    // if (list.isEmpty) {
    //
    //   address = await _apiRepository.fetchManageAddress();
    // }
    // else {
    //   address = list;
    // }
    print("getAddressWithHighestPriority :: method run :: ${list!.length}");
    setState(() {
      if (list!.length > 0) {
        // print("printing address string" + addressString);
        if (widget.newCartPage != true) {
          highPriorityAddress = "Select an address";
        }
      } else {
        print("method 1");
        context.read(cartNotifierProvider.notifier).getCartList(
            context: context,
            fromLat: widget.lat ??
                context
                    .read(homeNotifierProvider.notifier)
                    .currentLocation
                    .latitude!
                    .toDouble(),
            fromLng: widget.long ??
                context
                    .read(homeNotifierProvider.notifier)
                    .currentLocation
                    .longitude!
                    .toDouble());

        highPriorityAddress = "No address added!";
      }
    });
  }
}

// class CartTile {
//   String cartItemImage;
//   String cartItemName;
//   String cartItemPrice;
//   int cartItemCount;
//
//   CartTile(
//       {this.cartItemImage,
//       this.cartItemName,
//       this.cartItemPrice,
//       this.cartItemCount});
// }
