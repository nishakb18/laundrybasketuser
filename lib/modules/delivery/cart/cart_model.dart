CartListModel cartModelFromJson(str) {
  return CartListModel.fromJson(str);
}


class CartListModel {
  String? statusCode;
  String? title;
  String? message;
  CartListResponseData? responseData;
  int? deletedItemCount;
  List<dynamic>? responseNewData;
  List<dynamic>? error;

  CartListModel(
      {this.statusCode,
        this.title,
        this.message,
        this.responseData,
        this.responseNewData,
        this.error});

  CartListModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    responseData = json['responseData'] != null
        ? new CartListResponseData.fromJson(json['responseData'])
        : null;
    if (json['responseNewData'] != null) {
      responseNewData = [];
      json['responseNewData'].forEach((v) {
        responseNewData!.add(v);
      });
    }
    if (json['error'] != null) {
      error = [];
      json['error'].forEach((v) {
        error!.add(v);
      });
    }
  }
  CartListModel.fromJsonClearCart(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    deletedItemCount=json['responseData'];
    // responseData = json['responseData'] != null
    //     ? new CartListResponseData.fromJson(json['responseData'])
    //     : null;
    if (json['responseNewData'] != null) {
      responseNewData = [];
      json['responseNewData'].forEach((v) {
        responseNewData!.add(v);
      });
    }
    if (json['error'] != null) {
      error = [];
      json['error'].forEach((v) {
        error!.add(v);
      });
    }
  }


  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['title'] = this.title;
    data['message'] = this.message;
    if (this.responseData != null) {
      data['responseData'] = this.responseData!.toJson();
    }
    data["responseNewData"]= this.responseNewData == null ? [] : List<dynamic>.from(responseNewData!.map((x) => x));
    if (this.error != null) {
      data['error'] = this.error!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CartListResponseData {
  dynamic deliveryCharges;
  int? deliveryFreeMinimum;
  int? shopDeliveryCharge;
  dynamic taxPercentage;
  int? showEstimation;
  dynamic negativeWallet;
  List<int>? cartProductsId;
  List<Carts>? carts;
  dynamic totalPrice;
  dynamic shopDiscount;
  dynamic discount;
  String? storeType;
  dynamic totalItemPrice;
  int? promocodeId;
  dynamic promocodeAmount;
  String? promocode;
  dynamic net;
  dynamic walletBalance;
  dynamic payable;
  dynamic totalCart;
  dynamic shopGst;
  dynamic shopGstAmount;
  int? shopPackageCharge;
  int? storeId;
  int? storeCommisionPer;
  String? storeCommisionLogic;
  List<dynamic>? storeAdmincommisionSlap;
  String? shopCusines;
  dynamic rating;
  dynamic userWalletBalance;
  String? userCurrency;
  dynamic cartMinAmount;

  CartListResponseData(
      {this.deliveryCharges,
        this.deliveryFreeMinimum,
        this.shopDeliveryCharge,
        this.taxPercentage,
        this.showEstimation,
        this.negativeWallet,
        this.cartProductsId,
        this.carts,
        this.totalPrice,
        this.shopDiscount,
        this.discount,
        this.storeType,
        this.totalItemPrice,
        this.promocodeId,
        this.promocodeAmount,
        this.promocode,
        this.net,
        this.walletBalance,
        this.payable,
        this.totalCart,
        this.shopGst,
        this.shopGstAmount,
        this.shopPackageCharge,
        this.storeId,
        this.storeCommisionPer,
        this.storeCommisionLogic,
        this.storeAdmincommisionSlap,
        this.shopCusines,
        this.rating,
        this.userWalletBalance,
        this.userCurrency,
        this.cartMinAmount
      });

  CartListResponseData.fromJson(Map<String, dynamic> json) {
    deliveryCharges = json['delivery_charges'];
    deliveryFreeMinimum = json['delivery_free_minimum'];
    shopDeliveryCharge = json['shop_delivery_charge'];
    taxPercentage = json['tax_percentage'];
    showEstimation = json['show_estimation'];
    negativeWallet = json['negative_wallet'];

  //  if(json['cart_products_id']!=null)



    if (json['carts'] != null) {
      carts = [];
      json['carts'].forEach((v) {
        carts!.add(new Carts.fromJson(v));
      });
    }
    totalPrice = json['total_price'];
    shopDiscount = json['shop_discount'];
    discount = json['discount'];
    storeType = json['store_type'];
    totalItemPrice = json['total_item_price'];
    promocodeId = json['promocode_id'];
    promocode = json['promo_code'];

    promocodeAmount = json['promocode_amount'];
    net = json['net'];
    walletBalance = json['wallet_balance'];
    payable = json['payable'];
    totalCart = json['total_cart'];
    if(totalCart!>0)
    cartProductsId = json['cart_products_id'].cast<int>();

    shopGst = json['shop_gst'];
    shopGstAmount = json['shop_gst_amount'];
    shopPackageCharge = json['shop_package_charge'];
    storeId = json['store_id'];
    storeCommisionPer = json['store_commision_per'];
    storeCommisionLogic = json['store_commision_logic'];
    if (json['store_admincommision_slap'] != null) {
      storeAdmincommisionSlap =[];
      json['store_admincommision_slap'].forEach((v) {
        storeAdmincommisionSlap!.add(v);
      });
    }
    shopCusines = json['shop_cusines'];
    rating = json['rating'];
    userWalletBalance = json['user_wallet_balance'];
    userCurrency = json['user_currency'];
    cartMinAmount = json['offer_min_amount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['delivery_charges'] = this.deliveryCharges;
    data['delivery_free_minimum'] = this.deliveryFreeMinimum;
    data['shop_delivery_charge'] = this.shopDeliveryCharge;
    data['tax_percentage'] = this.taxPercentage;
    data['show_estimation'] = this.showEstimation;
    data['negative_wallet'] = this.negativeWallet;
    data['cart_products_id'] = this.cartProductsId;
    if (this.carts != null) {
      data['carts'] = this.carts!.map((v) => v.toJson()).toList();
    }
    data['total_price'] = this.totalPrice;
    data['shop_discount'] = this.shopDiscount;
    data['discount'] = this.discount;
    data['store_type'] = this.storeType;
    data['total_item_price'] = this.totalItemPrice;
    data['promocode_id'] = this.promocodeId;
    data['promo_code'] = this.promocode;
    data['promocode_amount'] = this.promocodeAmount;
    data['net'] = this.net;
    data['wallet_balance'] = this.walletBalance;
    data['payable'] = this.payable;
    data['total_cart'] = this.totalCart;
    data['shop_gst'] = this.shopGst;
    data['shop_gst_amount'] = this.shopGstAmount;
    data['shop_package_charge'] = this.shopPackageCharge;
    data['store_id'] = this.storeId;
    data['store_commision_per'] = this.storeCommisionPer;
    data['store_commision_logic'] = this.storeCommisionLogic;
    if (this.storeAdmincommisionSlap != null) {
      data['store_admincommision_slap'] =
          this.storeAdmincommisionSlap!.map((v) => v.toJson()).toList();
    }
    data['shop_cusines'] = this.shopCusines;
    data['rating'] = this.rating;
    data['user_wallet_balance'] = this.userWalletBalance;
    data['user_currency'] = this.userCurrency;
    data['offer_min_amount'] = this.cartMinAmount;
    return data;
  }
}

class Carts {
  int? id;
  int? userId;
  int? storeItemId;
  int? storeId;
  int? serviceId;
  int? storeTypeId;
  int? storeOrderId;
  int? quantity;
  dynamic itemPrice;
  dynamic totalItemPrice;
  int? totAddonPrice;
  String? note;
  String? productData;
  int? isReturn;
  dynamic toLat;
  dynamic toLng;
  Product? product;
  Store? store;
  List<Cartaddon>? cartaddon;

  Carts(
      {this.id,
        this.userId,
        this.storeItemId,
        this.storeId,
        this.serviceId,
        this.storeTypeId,
        this.storeOrderId,
        this.quantity,
        this.itemPrice,
        this.totalItemPrice,
        this.totAddonPrice,
        this.note,
        this.productData,
        this.isReturn,
        this.toLat,
        this.toLng,
        this.product,
        this.store,
        this.cartaddon});

  Carts.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    storeItemId = json['store_item_id'];
    storeId = json['store_id'];
    serviceId = json['service_id'];
    storeTypeId = json['store_type_id'];
    storeOrderId = json['store_order_id'];
    quantity = json['quantity'];
    itemPrice = json['item_price'];
    totalItemPrice = json['total_item_price'];
    totAddonPrice = json['tot_addon_price'];
    note = json['note'];
    productData = json['product_data'];
    isReturn = json['is_return'];
    toLat = json['to_lat'];
    toLng = json['to_lng'];
    product =
    json['product'] != null ? new Product.fromJson(json['product']) : null;
    store = json['store'] != null ? new Store.fromJson(json['store']) : null;
    if (json['cartaddon'] != null) {
      cartaddon = [];
      json['cartaddon'].forEach((v) {
        cartaddon!.add(new Cartaddon.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['store_item_id'] = this.storeItemId;
    data['store_id'] = this.storeId;
    data['service_id'] = this.serviceId;
    data['store_type_id'] = this.storeTypeId;
    data['store_order_id'] = this.storeOrderId;
    data['quantity'] = this.quantity;
    data['item_price'] = this.itemPrice;
    data['total_item_price'] = this.totalItemPrice;
    data['tot_addon_price'] = this.totAddonPrice;
    data['note'] = this.note;
    data['product_data'] = this.productData;
    data['is_return'] = this.isReturn;
    data['to_lat'] = this.toLat;
    data['to_lng'] = this.toLng;
    if (this.product != null) {
      data['product'] = this.product!.toJson();
    }
    if (this.store != null) {
      data['store'] = this.store!.toJson();
    }
    if (this.cartaddon != null) {
      data['cartaddon'] = this.cartaddon!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Product {
  String? itemName;
  String? servicename;
  dynamic itemPrice;
  int? id;
  String? isVeg;
  String? picture;
  int? itemDiscount;
  String? itemDiscountType;
  int? hidePrice;
  List<Itemsaddon>? itemsaddon;

  Product(
      {
        this.itemName,
        this.servicename,
        this.itemPrice,
        this.id,
        this.isVeg,
        this.picture,
        this.itemDiscount,
        this.itemDiscountType,
        this.hidePrice,
        this.itemsaddon});

  Product.fromJson(Map<String, dynamic> json) {
    itemName = json['item_name'];
    servicename = json['servicename'];
    itemPrice = json['item_price'];
    id = json['id'];
    isVeg = json['is_veg'];
    picture = json['picture'];
    itemDiscount = json['item_discount'];
    itemDiscountType = json['item_discount_type'];
    hidePrice = json['hide_price'];
    if (json['itemsaddon'] != null) {
      itemsaddon = [];
      json['itemsaddon'].forEach((v) {
        itemsaddon!.add(new Itemsaddon.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['item_name'] = this.itemName;
    data['servicename'] = this.servicename;
    data['item_price'] = this.itemPrice;
    data['id'] = this.id;
    data['is_veg'] = this.isVeg;
    data['picture'] = this.picture;
    data['item_discount'] = this.itemDiscount;
    data['item_discount_type'] = this.itemDiscountType;
    data['hide_price'] = this.hidePrice;
    if (this.itemsaddon != null) {
      data['itemsaddon'] = this.itemsaddon!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Itemsaddon {
  int? id;
  int? storeId;
  int? storeItemId;
  int? storeAddonId;
  int? price;
  String? addonName;

  Itemsaddon(
      {this.id,
        this.storeId,
        this.storeItemId,
        this.storeAddonId,
        this.price,
        this.addonName});

  Itemsaddon.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    storeId = json['store_id'];
    storeItemId = json['store_item_id'];
    storeAddonId = json['store_addon_id'];
    price = json['price'];
    addonName = json['addon_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['store_id'] = this.storeId;
    data['store_item_id'] = this.storeItemId;
    data['store_addon_id'] = this.storeAddonId;
    data['price'] = this.price;
    data['addon_name'] = this.addonName;
    return data;
  }
}

class Store {
  int? storeTypeId;
  String? storeMultiTypeId;
  String? storeName;
  String? currencySymbol;
  String? picture;
  String? gallery;
  dynamic rating;
  int? storePackingCharges;
  int? storeGst;
  int? commission;
  String? offerMinAmount;
  int? offerPercent;
  int? freeDelivery;
  int? id;
  double? latitude;
  double? longitude;
  int? cityId;
  String? adminCommissionLogic;
  List<dynamic>? admincommisionSlab;
  Storetype? storetype;
  List<dynamic>? storeCusinie;

  Store(
      {this.storeTypeId,
        this.storeMultiTypeId,
        this.storeName,
        this.currencySymbol,
        this.picture,
        this.gallery,
        this.rating,
        this.storePackingCharges,
        this.storeGst,
        this.commission,
        this.offerMinAmount,
        this.offerPercent,
        this.freeDelivery,
        this.id,
        this.latitude,
        this.longitude,
        this.cityId,
        this.adminCommissionLogic,
        this.admincommisionSlab,
        this.storetype,
        this.storeCusinie});

  Store.fromJson(Map<String, dynamic> json) {
    storeTypeId = json['store_type_id'];
    storeMultiTypeId = json['store_multi_type_id'];
    storeName = json['store_name'];
    currencySymbol = json['currency_symbol'];
    picture = json['picture'];
    gallery = json['gallery'];
    rating = json['rating'];
    storePackingCharges = json['store_packing_charges'];
    storeGst = json['store_gst'];
    commission = json['commission'];
    offerMinAmount = json['offer_min_amount'];
    offerPercent = json['offer_percent'];
    freeDelivery = json['free_delivery'];
    id = json['id'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    cityId = json['city_id'];
    adminCommissionLogic = json['admin_commission_logic'];
    if (json['admincommision_slab'] != null) {
      admincommisionSlab = [];
      json['admincommision_slab'].forEach((v) {
        admincommisionSlab!.add(v);
      });
    }
    storetype = json['storetype'] != null
        ? new Storetype.fromJson(json['storetype'])
        : null;
    if (json['store_cusinie'] != null) {
      storeCusinie = [];
      json['store_cusinie'].forEach((v) {
        storeCusinie!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['store_type_id'] = this.storeTypeId;
    data['store_multi_type_id'] = this.storeMultiTypeId;
    data['store_name'] = this.storeName;
    data['currency_symbol'] = this.currencySymbol;
    data['picture'] = this.picture;
    data['gallery'] = this.gallery;
    data['rating'] = this.rating;
    data['store_packing_charges'] = this.storePackingCharges;
    data['store_gst'] = this.storeGst;
    data['commission'] = this.commission;
    data['offer_min_amount'] = this.offerMinAmount;
    data['offer_percent'] = this.offerPercent;
    data['free_delivery'] = this.freeDelivery;
    data['id'] = this.id;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['city_id'] = this.cityId;
    data['admin_commission_logic'] = this.adminCommissionLogic;
    if (this.admincommisionSlab != null) {
      data['admincommision_slab'] =
          this.admincommisionSlab!.map((v) => v.toJson()).toList();
    }
    if (this.storetype != null) {
      data['storetype'] = this.storetype!.toJson();
    }
    if (this.storeCusinie != null) {
      data['store_cusinie'] = this.storeCusinie!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Storetype {
  int? id;
  String? name;
  String? category;
  String? picture;
  int? status;
  String? itemsStatus;
  String? addonsStatus;
  String? keywords;

  Storetype(
      {this.id,
        this.name,
        this.category,
        this.picture,
        this.status,
        this.itemsStatus,
        this.addonsStatus,
        this.keywords});

  Storetype.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    category = json['category'];
    picture = json['picture'];
    status = json['status'];
    itemsStatus = json['items_status'];
    addonsStatus = json['addons_status'];
    keywords = json['keywords'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['category'] = this.category;
    data['picture'] = this.picture;
    data['status'] = this.status;
    data['items_status'] = this.itemsStatus;
    data['addons_status'] = this.addonsStatus;
    data['keywords'] = this.keywords;
    return data;
  }
}

class Cartaddon {
  int? id;
  int? storeCartItemId;
  int? storeItemAddonsId;
  int? storeCartId;
  int? storeAddonId;
  int? addonPrice;
  String? addonQuantity;
  dynamic addonTotal;
  String? addonName;

  Cartaddon(
      {this.id,
        this.storeCartItemId,
        this.storeItemAddonsId,
        this.storeCartId,
        this.storeAddonId,
        this.addonPrice,
        this.addonQuantity,
        this.addonTotal,
        this.addonName});

  Cartaddon.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    storeCartItemId = json['store_cart_item_id'];
    storeItemAddonsId = json['store_item_addons_id'];
    storeCartId = json['store_cart_id'];
    storeAddonId = json['store_addon_id'];
    addonPrice = json['addon_price'];
    addonQuantity = json['addon_quantity'];
    addonTotal = json['addon_total'];
    addonName = json['addon_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['store_cart_item_id'] = this.storeCartItemId;
    data['store_item_addons_id'] = this.storeItemAddonsId;
    data['store_cart_id'] = this.storeCartId;
    data['store_addon_id'] = this.storeAddonId;
    data['addon_price'] = this.addonPrice;
    data['addon_quantity'] = this.addonQuantity;
    data['addon_total'] = this.addonTotal;
    data['addon_name'] = this.addonName;
    return data;
  }
}

