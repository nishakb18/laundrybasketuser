import 'dart:convert';
import 'package:app24_user_app/constants/api_path.dart';
import 'package:app24_user_app/constants/shared_preferences_path.dart';
import 'package:app24_user_app/modules/delivery/cart/cart_model.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/api/api_response_state2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../helpers.dart';

class CartCountNotifier extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  CartCountNotifier(this._apiRepository)
      : super(ResponseState2(isLoading: true));
  List<int>? cartProductsId = [];
  List<Carts> cartListData = new List.empty(growable: true);
  int cartID = 0;
  String? latLong = "";
  String shopName = "";

  //bool? locationError=false;

  // cartCount(){
  //   print("cart count called");
  // }

  int cartCount = 0;
  var cart;

  Future<void> getCartCount({bool init = true, BuildContext? context}) async {
    try {
      double? to_lat =
          context!.read(homeNotifierProvider.notifier).currentLocation.latitude;
      double? to_lng =
          context.read(homeNotifierProvider.notifier).currentLocation.longitude;
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String? token = prefs.getString(SharedPreferencesPath.accessToken);
      latLong = '?to_lat=${to_lat.toString()}&'
          'to_lng=${to_lng.toString()}';
      if (init) state = state.copyWith(isLoading: true);
      CartListModel cartListModel = new CartListModel();
      Map<String, String> headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer $token"
      };
      Helpers()
          .getData(App24UserAppAPI.laundryCartListUrl + latLong!, headers)
          .then((value) {
        if (json.decode(value!)['statusCode'] == "200") {
          print("200 else condition");
          // if(!json.decode(value)['message'].toString().contains("Please check the Drop Point")){

          var data = json.decode(value);
          var rest = data["responseData"];
          if (data["responseData"]['cart_products_id'] != null) {
            print("printing if condition inside");
            cartListModel = CartListModel.fromJson(json.decode(value));

            if (cartListModel.responseData!.totalCart > 0)
              cartProductsId = cartListModel.responseData!.cartProductsId;
            cartListData = cartListModel.responseData!.carts!;

            cartID = int.parse(
                data["responseData"]["carts"][0]["store_id"].toString());

             print("cartID  dd  :" + cartID.toString());
          } else if (data["message"] != null) {
            print("printing else if condition in 200");
            shopName = "";
            List cartInfo = ["", 0];
            state = state.copyWith(
                response: cartInfo, isLoading: false, isError: false);
          } else {
            cartID = 0;
          }
          //print("total cart count: "+rest['total_cart'].toString());
          cartCount = int.parse(rest['total_cart'].toString());
          shopName = (data["responseData"]["carts"][0]["store"]["store_name"])
              .toString();
          var carts = data["responseData"]["carts"];
          print("printing shop name from notifier" + shopName);
//
          List cartInfo = [carts == [] ? 0 : shopName, cartCount, cart];

          // locationError=false;
          state = state.copyWith(
              response: cartInfo, isLoading: false, isError: false);
          //  }
          // else{
          //    cartID=0;
          //   // locationError=true;
          //    state = state.copyWith(
          //
          //        isLoading: false,
          //        isError: false);
          //    // showDistanceExceededPopUp(context:context);
          //
          //  }
        } else if (json.decode(value)['statusCode'] == "400") {
          print("400 else if");
          // cartID = 0;
          state = state.copyWith(response: 0, isLoading: false, isError: false);
        } else {
          print("else condition");
          cartID = 0;
          // locationError=true;
          state = state.copyWith(
              response: "null", isLoading: false, isError: false);
        }
      });
    } catch (e) {
      state = state.copyWith(
          errorMessage: e.toString(), isLoading: false, isError: false);
    }
  }

  Future<void> updateCartCount(
      {required int count, int? storeID, context, String? shopName}) async {
    print("UPDAT cart ID: " +
        cartID.toString() +
        "Store ID" +
        storeID.toString() +
        "count" +
        count.toString());

    if (storeID != null && storeID != cartID) {
      cartID = storeID;

      count = 1;
    } else if (count == 0) {
      cartID = 0;
    }
    List cartInfo = [shopName, count];

    state =
        state.copyWith(response: cartInfo, isLoading: false, isError: false);
  }
}
