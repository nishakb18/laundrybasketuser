import 'dart:convert';

import 'package:app24_user_app/constants/api_path.dart';
import 'package:app24_user_app/constants/shared_preferences_path.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/api/api_response_state2.dart';
import 'package:app24_user_app/utils/services/api/api_services.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../helpers.dart';
import 'cart_model.dart';

class CartListNotifier extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  CartListNotifier(this._apiRepository)
      : super(ResponseState2(isLoading: true, response: []));
  List<int>? cartIdList;
  List<Carts> cartListData = new List.empty(growable: true);
  int cartCount = 0;
  int cartID = 0;
  String latLong = '';
  String toLatLong = '';
  String fromLatLong = '';
  String? addResponse;
  bool isCartCleared = false;
  bool isFromAdsLoad = false;
  bool isToAdsLoad = false;
  String errorMsg = '';
  CartListModel crtlistModel = new CartListModel();

  Future<void> getCartList(
      {
        int? storeID,
        bool init = true,
      double? fromLat,
      double? fromLng,
        double? toLat,
        double? toLng,
        bool? isLaundryBasketCard = false,
      required BuildContext context}) async {
    if(isLaundryBasketCard == true) {
      print(" isLaundryBasketCard was :: ${isLaundryBasketCard} so if part work");
      try {
      print("storeID getCartList :: $storeID");

      fromLatLong = '?from_lat=${fromLat.toString()}&'
          'from_lng=${fromLng.toString()}';

      toLatLong = '&to_lat=${toLat ?? fromLat}&'
          'to_lng=${toLng ?? fromLng }';

      if (init) state = state.copyWith(isLoading: true);
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String? token = prefs.getString(SharedPreferencesPath.accessToken);

      if (init) state = state.copyWith(isLoading: true);

      Map<String, String> headers = {
        "Content-Type": "application/x-www-form-urlencoded",
        "Authorization": "Bearer $token"
      };

      if(storeID == null || storeID == 0){
        print("if condition storeID :: init :: $init");
        RestAPIService restAPIService = RestAPIService(Dio());
        errorMsg = '';
        var result =
        await restAPIService.getService(
          useToken: true,
            url: App24UserAppAPI.laundryCartListUrl+fromLatLong+toLatLong);
        crtlistModel = CartListModel.fromJson(result);
        print("errorMsg :: $errorMsg :: crtlistModel.message :: ${crtlistModel.message}");
        if(crtlistModel.message != '' || crtlistModel.message != null){
          errorMsg = crtlistModel.message ?? '';
        }
        isFromAdsLoad = false;
          state = state.copyWith(
            response: crtlistModel, isLoading: false, isError: false);

        /*Helpers()
            .getData(App24UserAppAPI.laundryCartListUrl+fromLatLong+toLatLong, headers)
            .then((value) {
          var data = json.decode(value!);
          if (json.decode(value)['statusCode'] == "200") {


            if (data["responseData"]["carts"] != null) {
              crtlistModel = CartListModel.fromJson(data);
              state = state.copyWith(
                  response: crtlistModel, isLoading: false, isError: false);
            }

          } else {
            if (json
                .decode(value)['message']
                .toString()
                .contains("Please check the collection address. Distance is more than the allowed limit")) {
              state =
                  state.copyWith(response: 0, isLoading: false, isError: false);
            }
          }
        }
        );*/
      }
      else{
        print("else condition storeID");
        Map<String, dynamic> body = {"latitude" : fromLat,"longitude" : fromLng};
          RestAPIService restAPIService = RestAPIService(Dio());
          var result = await restAPIService.postService(
            url: "${App24UserAppAPI.storeList}/$storeID",
            body: body,
            useToken: true,
          );
          crtlistModel = CartListModel.fromJson(result as Map<String, dynamic>);
          if(crtlistModel.statusCode == '200'){
            if(crtlistModel.responseData?.carts != null){
              state = state.copyWith(
                  response: crtlistModel, isLoading: false, isError: false);
            }
            else{
              if(crtlistModel.message.toString() == "Please check the Drop Point"){
                state =
                    state.copyWith(response: 0, isLoading: false, isError: false);
              }
            }
          }
      }
    } catch (e) {
        isFromAdsLoad = false;
        errorMsg = e.toString();
      state = state.copyWith(
          errorMessage: e.toString(), isLoading: false, isError: true);
    }
    }
    else{
      print(" isLaundryBasketCard was :: ${isLaundryBasketCard} so else part work");
      try {
        latLong = '?to_lat=${fromLat.toString()}&'
            'to_lng=${fromLat.toString()}';

        if (init) state = state.copyWith(isLoading: true);
        //  final cartListModel = await _apiRepository.fetchCartList();
        // List<dynamic>  cartListItems;
        SharedPreferences prefs = await SharedPreferences.getInstance();
        String? token = prefs.getString(SharedPreferencesPath.accessToken);

        if (init) state = state.copyWith(isLoading: true);

        Map<String, String> headers = {
          "Content-Type": "application/x-www-form-urlencoded",
          "Authorization": "Bearer $token"
        };
        Helpers()
            .getData(App24UserAppAPI.cartListUrl + latLong, headers)
            .then((value) {
          if (json.decode(value!)['statusCode'] == "200") {
            // if (!json.decode(value)['message'].toString().contains(
            //     "Please check the Drop Point")) {
            var data = json.decode(value);

            if (data["responseData"]["carts"] != null) {
              // cartListItems  = data["responseData"]['cart_products_id'] as List<dynamic>;
              crtlistModel = CartListModel.fromJson(data);
              state = state.copyWith(
                  response: crtlistModel, isLoading: false, isError: false);
            }

            // }
            // else {
            //   state = state.copyWith(
            //       response: 0,
            //       isLoading: false,
            //       isError: false);
            //   // showDistanceExceededPopUp(context:context);
            // }
          } else {
            if (json
                .decode(value)['message']
                .toString()
                .contains("Please check the Drop Point")) {
              state =
                  state.copyWith(response: 0, isLoading: false, isError: false);
            }
          }
        });
      } catch (e) {
        state = state.copyWith(
            errorMessage: e.toString(), isLoading: false, isError: true);
      }

    }


  }

  Future<void> addCart(
      {bool init = true,
      required BuildContext context,
      required String itemAddons,
      required int itemQty,
      required String itemID,
      bool? fromCartAdd,
      //bool? serviceId,
      Map? bodys}) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String? token = prefs.getString(SharedPreferencesPath.accessToken);
      Map<String, String> headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer $token"
      };

      Map<String, dynamic> body = {
       // "service_id": serviceId,
        "qty": itemQty,
        // "repeat" : 3,
        "item_id": itemID,
        "addons": itemAddons,
        "to_lat": context
            .read(homeNotifierProvider.notifier)
            .currentLocation
            .latitude,
        "to_lng": context
            .read(homeNotifierProvider.notifier)
            .currentLocation
            .longitude,
        "customize": 0
      };

      if (init) state = state.copyWith(isLoading: true);

      await _apiRepository.fetchAddCart(context: context,itemID: itemID,body: body);
      //if success ? show message ("item added to cart")
      // showMessage(context, "item added to cart");
      Helpers()
          .getPostData(
              App24UserAppAPI
                  .addCartUrl /*+"item_id=$itemID&qty=$itemQty&addons=$itemAddons&to_lat=$to_lat&to_lng=$to_lng"*/,
              fromCartAdd == true ? bodys : body,
              headers)
          .then((value) {
        //  print("json.decode(value)['statusCode'].toString() "+json.decode(value)['statusCode'].toString());
        CartListModel crtlistModel =  CartListModel();
        if (json.decode(value)['statusCode'].toString() != "200") {
          //  print("json.decode(value)['message'].toString()"+json.decode(value)['message'].toString());
          if (json
              .decode(value)['message']
              .toString()
              .contains("Please check the Drop Point")) {
            print("add cart notifier");
            addResponse = "far";
            // showMessage(context, "Drop Location too far", true, false);
            //context.read(cartCountNotifierProvider.notifier).locationError=true;
            //     showDistanceExceededPopUp(context:context);
          }
        } else {
          if (json.decode(value)['statusCode'].toString() == "200") {
            crtlistModel = CartListModel.fromJson(json.decode(value));
            if (crtlistModel.responseData!.totalCart > 0)
              cartIdList = crtlistModel.responseData!.cartProductsId;
            print("cartIdList :: $cartIdList");

            cartListData = crtlistModel.responseData!.carts!;

            getCartList(
                fromLat: context
                    .read(homeNotifierProvider.notifier)
                    .currentLocation
                    .latitude!
                    .toDouble(),
                fromLng: context
                    .read(homeNotifierProvider.notifier)
                    .currentLocation
                    .longitude!
                    .toDouble(),
                context: context);
            /* context
                .read(cartCountNotifierProvider.notifier)
                .locationError = false;*/

          }
        }
        state = state.copyWith(isLoading: false);
      });
    } catch (e) {
      showMessage(context, e.toString(), true, true);
      state = state.copyWith(
          errorMessage: e.toString(), isLoading: false, isError: true);
    }
  }

  Future<void> removeItemCart(
      {bool init = true,
      required Map body,
      required BuildContext context}) async {
    try {
      if (init) state = state.copyWith(isLoading: true);

      final removeCart = await _apiRepository.fetchRemoveCart(body: body);

      //  state.copyWith(isLoading: false);
      /* context
                .read(cartCountNotifierProvider.notifier)
                .locationError = false;*/

      //state = state.copyWith(
      //  response: removeCart, isLoading: false, isError: false);
    } catch (e) {
      // state = state.copyWith(
      //   errorMessage: e.toString(), isLoading: false, isError: true);
    }
  }

  Future<void> getClearCart(
      {bool init = true, required BuildContext context}) async {
    //try {
    if (init) state = state.copyWith(isLoading: true);

    final cartList = await _apiRepository.fetchClearCart();
    // CartListModel.fromJson(json.decode(cartList)).responseData;
    //print("cartlist after clear : "+json.decode(cartList)['responseData']);

    print("cartlist after clear : " +
        CartListModel.fromJsonClearCart(cartList).deletedItemCount.toString());
    if (CartListModel.fromJsonClearCart(cartList).deletedItemCount! > 0) {
      isCartCleared = true;
    } else {
      isCartCleared = false;
    }
    state = state.copyWith(response: 1, isLoading: false, isError: false);
    //}
    // catch (e) {
    //   print("errorr...................."+e.toString());
    //   // state = state.copyWith(
    //   //     errorMessage: e.toString(), isLoading: false, isError: true);
    // }
  }

/*  Future<void> getCartCount({
    bool init = true,
  }) async {

    try {
      if (init) state = state.copyWith(isLoading: true);
      final cartList = await _apiRepository.fetchCartList();
      showLog("dony cartList totalCart :" + cartList.responseData!.totalCart.toString());
      if(cartList.responseData!.carts!.length>0) {
        showLog("cart list length in notifier:: :  "+cartList.responseData!.carts!.length.toString());
        cartID = cartList.responseData!.carts![0].storeId!;
        showLog("cartID in notifier:: "+cartID.toString());

      }
*/ /*

      debugPrint("cart store id from cartCount: $cartID");
      debugPrint("totalcart" + cartList.responseData!.totalCart.toString());
      // if(cartList.responseData!.totalCart == 0){
      //
      // }
      print("cart count in notifier"+cartList.responseData!.totalCart.toString());*/ /*
      print("cart count in notifier"+cartList.responseData!.totalCart.toString());
      state = state.copyWith(
          response: cartList.responseData!.totalCart,
          isLoading: false,
          isError: false);


    } catch (e) {
      showLog("agandev in cart notifier : "+e.toString());

      // state.errorMessage
*/ /*
      state = state.copyWith(
          errorMessage: e.toString(), isLoading: false, isError: true);*/ /*
    }
  }*/

}
