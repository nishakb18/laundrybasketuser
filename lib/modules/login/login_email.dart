import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/modules/login/signUp_page.dart';
import 'package:app24_user_app/widgets/offline_builder_widget.dart';
import 'package:flutter/material.dart';

import '../home.dart';

class EmailLoginPage extends StatefulWidget {
  @override
  _EmailLoginPageState createState() => _EmailLoginPageState();
}

class _EmailLoginPageState extends State<EmailLoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(""),
        backgroundColor: Colors.white,
        elevation: 0,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: App24Colors.darkTextColor,
              size: MediaQuery.of(context).size.width / 25,
            ),
            onPressed: () {
              Navigator.pop(context);
            }),
      ),
      backgroundColor: Colors.white,
      body: OfflineBuilderWidget(
         SafeArea(
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Image.asset(
                    App24UserAppImages.dropLogoIcon,
                    width: MediaQuery.of(context).size.width / 7,
                  ),
                  const SizedBox(
                    height: 35,
                  ),
                  Text(
                    "Login",
                    style: TextStyle(
                        fontSize: MediaQuery.of(context).size.width / 10,
                        fontWeight: FontWeight.w700,
                        color: App24Colors.darkTextColor),
                  ),
                  const SizedBox(
                    height: 35,
                  ),
                  TextField(
                    style: TextStyle(
                      fontSize: MediaQuery.of(context).size.width / 24,
                      color: App24Colors.darkTextColor,
                      fontWeight: FontWeight.w600,
                    ),
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      contentPadding: const EdgeInsets.symmetric(
                          horizontal: 15, vertical: 15),
                      fillColor: Color(0xfff5f5f5),
                      filled: true,
                      hintText: "Email Address",
                      hintStyle: TextStyle(
                          fontSize: MediaQuery.of(context).size.width / 24,
                          fontWeight: FontWeight.w600,
                          color: Color(0xff929292)),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(15),
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(15),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  TextField(
                    obscureText: true,
                    decoration: InputDecoration(
                      contentPadding: const EdgeInsets.symmetric(
                          horizontal: 15, vertical: 15),
                      fillColor: Color(0xfff5f5f5),
                      filled: true,
                      hintText: "Password",
                      hintStyle: TextStyle(
                          fontSize: MediaQuery.of(context).size.width / 24,
                          fontWeight: FontWeight.w600,
                          color: Color(0xff929292)),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(15),
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(15),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 35,
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: TextButton(
                            style: TextButton.styleFrom(
                                backgroundColor: App24Colors.darkTextColor,
                                padding: EdgeInsets.symmetric(
                                    vertical:
                                        MediaQuery.of(context).size.width / 23),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(25))),
                            onPressed: () {
                              Navigator.of(context).pushReplacement(
                                  MaterialPageRoute(
                                      builder: (context) => HomePage()));
                            },
                            child: Text(
                              "Login",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize:
                                      MediaQuery.of(context).size.width / 22,
                                  fontWeight: FontWeight.w600),
                            )),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Row(
                    children: [
                      Text(
                        "Don't have an account?",
                        style: TextStyle(
                            decoration: TextDecoration.underline,
                            color: App24Colors.darkTextColor,
                            fontSize: MediaQuery.of(context).size.width / 23,
                            fontWeight: FontWeight.w600),
                      ),
                      TextButton(
                          onPressed: () {
                            Navigator.of(context).pushReplacement(
                                MaterialPageRoute(
                                    builder: (context) => SignUpPage()));
                          },
                          child: Text(
                            "Sign Up",
                            style: TextStyle(
                                fontSize: MediaQuery.of(context).size.width / 23),
                          ))
                    ],
                  )
                  // Text("Don't have an account? SignUp")
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
