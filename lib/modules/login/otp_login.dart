// import 'package:app24_user_app/constants/asset_path.dart';
// import 'package:app24_user_app/constants/color_path.dart';
// import 'package:app24_user_app/modules/login/login_email.dart';
// import 'package:app24_user_app/modules/sign_in/otp_verification.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
//
// class OtpLoginPage extends StatefulWidget {
//   @override
//   _OtpLoginPageState createState() => _OtpLoginPageState();
// }
//
// class _OtpLoginPageState extends State<OtpLoginPage> {
//   String? _countryCodeValue;
//   List _countryCode = ["+91", "+92", "+93", "+94", "+101"];
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Colors.white,
//       body: SafeArea(
//         child: SingleChildScrollView(
//           child: Container(
//             padding: EdgeInsets.symmetric(horizontal: 25, vertical: 20),
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: [
//                 Image.asset(
//                   App24UserAppImages.newLogo,
//                   width: MediaQuery.of(context).size.width / 7,
//                 ),
//                 const SizedBox(
//                   height: 70,
//                 ),
//                 Image.asset(App24UserAppImages.loginTopImage),
//                 const SizedBox(
//                   height: 35,
//                 ),
//                 Text(
//                   "Get 4-digit OTP",
//                   style: TextStyle(
//                       fontSize: MediaQuery.of(context).size.width / 20,
//                       fontWeight: FontWeight.w600),
//                 ),
//                 const SizedBox(
//                   height: 25,
//                 ),
//                 Column(
//                   crossAxisAlignment: CrossAxisAlignment.stretch,
//                   children: [
//                     Row(
//                       children: [
//                         Expanded(
//                           child: Container(
//                             padding: EdgeInsets.only(left: 5),
//                             decoration: BoxDecoration(
//                               borderRadius: BorderRadius.circular(25),
//                               color: Color(0xfff5f5f5),
//                             ),
//                             child: Row(
//                               children: [
//                                 Container(
//                                   padding: EdgeInsets.symmetric(
//                                       vertical: 0, horizontal: 10),
//                                   decoration: BoxDecoration(
//                                     // color: Colors.grey[200],
//                                     borderRadius: BorderRadius.circular(15),
//                                   ),
//                                   child: DropdownButton(
//                                       underline: SizedBox(),
//                                       isExpanded: false,
//                                       // hint: Text(
//                                       //   "Gender",
//                                       //   style: TextStyle(color: App24Colors.darkTextColor,fontSize: MediaQuery.of(context).size.width/25),
//                                       // ),
//                                       elevation: 2,
//                                       dropdownColor: Colors.white,
//                                       iconEnabledColor: App24Colors.darkTextColor,
//                                       style: TextStyle(
//                                           color: App24Colors.darkTextColor,
//                                           fontWeight: FontWeight.bold),
//                                       value: _countryCodeValue,
//                                       onChanged: (dynamic value) {
//                                         setState(() {
//                                           _countryCodeValue = value;
//                                         });
//                                       },
//                                       items: _countryCode.map((value) {
//                                         return DropdownMenuItem(
//                                             value: value, child: Text(value));
//                                       }).toList()),
//                                 ),
//                                 Expanded(
//                                   child: TextField(
//                                     keyboardType: TextInputType.number,
//                                     style: TextStyle(
//                                       fontSize:
//                                           MediaQuery.of(context).size.width /
//                                               24,
//                                       color: App24Colors.darkTextColor,
//                                       fontWeight: FontWeight.w600,
//                                     ),
//                                     decoration: InputDecoration(
//                                       border: InputBorder.none,
//                                       errorBorder: InputBorder.none,
//                                       disabledBorder: InputBorder.none,
//                                       contentPadding:
//                                           const EdgeInsets.symmetric(
//                                               horizontal: 15, vertical: 15),
//                                       fillColor: Color(0xfff5f5f5),
//                                       filled: true,
//                                       hintText: "Phone No.",
//                                       hintStyle: TextStyle(
//                                           fontSize: MediaQuery.of(context)
//                                                   .size
//                                                   .width /
//                                               24,
//                                           fontWeight: FontWeight.w500,
//                                           color: Color(0xff929292)),
//                                       focusedBorder: OutlineInputBorder(
//                                         borderSide:
//                                             BorderSide(color: Colors.white),
//                                         borderRadius: BorderRadius.circular(25),
//                                       ),
//                                       enabledBorder: UnderlineInputBorder(
//                                         borderSide:
//                                             BorderSide(color: Colors.white),
//                                         borderRadius: BorderRadius.circular(25),
//                                       ),
//                                     ),
//                                   ),
//                                 ),
//                               ],
//                             ),
//                           ),
//                         ),
//                         const SizedBox(
//                           width: 10,
//                         ),
//                         Material(
//                           shape: RoundedRectangleBorder(
//                               borderRadius: BorderRadius.circular(25)),
//                           color: App24Colors.darkTextColor,
//                           child: InkWell(
//                             borderRadius: BorderRadius.circular(25),
//                             onTap: () {
//                               Navigator.push(
//                                 context,
//                                 MaterialPageRoute(
//                                     builder: (context) =>
//                                         OTPVerificationPage()),
//                               );
//                             },
//                             child: Padding(
//                               padding: const EdgeInsets.all(7.0),
//                               child: Icon(
//                                 Icons.chevron_right,
//                                 color: Colors.white,
//                               ),
//                             ),
//                           ),
//                         )
//                       ],
//                     ),
//                     const SizedBox(
//                       height: 15,
//                     ),
//                     TextButton.icon(
//                         style: TextButton.styleFrom(
//                             backgroundColor: App24Colors.darkTextColor,
//                             padding: EdgeInsets.symmetric(vertical: 12),
//                             shape: RoundedRectangleBorder(
//                                 borderRadius: BorderRadius.circular(25))),
//                         icon: Icon(
//                           Icons.email,
//                           color: Colors.white,
//                         ),
//                         onPressed: () {
//                           Navigator.push(
//                             context,
//                             MaterialPageRoute(
//                                 builder: (context) => EmailLoginPage()),
//                           );
//                         },
//                         label: Text(
//                           "Continue with Email",
//                           style: TextStyle(
//                               color: Colors.white,
//                               fontWeight: FontWeight.w500,
//                               fontSize: MediaQuery.of(context).size.width / 24),
//                         )),
//                     const SizedBox(
//                       height: 15,
//                     ),
//                     TextButton.icon(
//                         onPressed: () {},
//                         style: TextButton.styleFrom(
//                             shape: RoundedRectangleBorder(
//                                 borderRadius: BorderRadius.circular(25)),
//                             side: BorderSide(
//                                 color: Color(0xffeaeaea),
//                                 width: 1,
//                                 style: BorderStyle.solid)),
//                         icon: Image.asset(
//                           App24UserAppImages.googleLogo,
//                           width: MediaQuery.of(context).size.width / 12,
//                         ),
//                         label: Text(
//                           "Continue with Google",
//                           style: TextStyle(
//                               color: Colors.black,
//                               fontWeight: FontWeight.w500,
//                               fontSize: MediaQuery.of(context).size.width / 24),
//                         )),
//                   ],
//                 ),
//                 const SizedBox(
//                   height: 20,
//                 )
//               ],
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }
