import 'package:app24_user_app/app24_user_icons.dart';
import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/constants/global_constants.dart';
import 'package:app24_user_app/helpers.dart';
import 'package:app24_user_app/models/location_model.dart';
import 'package:app24_user_app/modules/restaurant/food_items_page/food_item_page.dart';
import 'package:app24_user_app/modules/restaurant/restaurant_cart_page.dart';
import 'package:app24_user_app/modules/restaurant/restaurant_shops/restaurant_loading.dart';
import 'package:app24_user_app/modules/restaurant/restaurant_shops/restaurants_model.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/widgets/common_appbar.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:app24_user_app/widgets/offline_builder_widget.dart';
import 'package:app24_user_app/widgets/view_cart_overlay.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

// import 'add_list.dart';

class AllRestaurantPage extends StatefulWidget {
  final String? cuisineName;

  const AllRestaurantPage({Key? key, this.cuisineName}) : super(key: key);

  @override
  _AllRestaurantPageState createState() => _AllRestaurantPageState();
}

class _AllRestaurantPageState extends State<AllRestaurantPage> {
  // List<RecentFoodTile> recentFoodTile = [];
  var top = 0.0;
  int _current = 0;
  var rating = 3.5;
  bool isNearbyShopsEmpty = true;
  final List<String> _homeCarouselList = [
    App24UserAppImages.allRestaurantCoupon,
    App24UserAppImages.allRestaurantCoupon,
    App24UserAppImages.allRestaurantCoupon,
    App24UserAppImages.allRestaurantCoupon,
  ];
  String? searchText = '';

  reloadData({required BuildContext context, bool? init}) {
    return context
        .read(restaurantNotifier.notifier)
        .getRestaurants(context: context);
  }

  @override
  void initState() {
    SchedulerBinding.instance!.addPostFrameCallback((_) {
      //API call after screen build
      // context.read(homeNotifierProvider).getOffers(context: context);
      context
          .read(restaurantNotifier.notifier)
          .getRestaurants(context: context);
      loadData(context: context);
    });
    super.initState();
  }

  List<Widget> getCarouselImages() {
    final List<Widget> imageSliders = _homeCarouselList
        .map((item) => ClipRRect(
              borderRadius: const BorderRadius.all(Radius.circular(25.0)),
              child: Image.asset(
                item,
                fit: BoxFit.cover,
              ),
            ))
        .toList();

    return imageSliders;
  }

  loadData({bool init = true, required BuildContext context}) {
    return context
        .read(restaurantNotifier.notifier)
        .getRestaurants(init: init, context: context);
  }

  selectLocation(String title, bool searchUserLocation) {
    showSelectLocationSheet(
            context: context,
            title: title,
            searchUserLocation: searchUserLocation)
        .then((value) {
      if (value != null) {
        LocationModel model = value;
        setState(() {
          context.read(homeNotifierProvider.notifier).currentLocation = model;
          loadData(context: context);
        });
      }
    });
  }

  //List of food Tiles
  List<Widget> getFoodTiles(
      context, List<RestaurantResponseData> restaurantListModel) {
    List<Widget> _widgetFood = [];
    restaurantListModel.sort((a, b) => b.shopstatus!.compareTo(a.shopstatus!));
    for (var i = 0; i < restaurantListModel.length; i++) {
      List<String> cuisineList = restaurantListModel[i].cusineList!.split(",");
      if (restaurantListModel[i]
          .storeName!
          .toLowerCase()
          .contains(searchText!.toLowerCase())) if (widget.cuisineName ==
              null ||
          (cuisineList
                  .indexWhere((element) => element == widget.cuisineName)) !=
              -1) {
        _widgetFood.add(Padding(
          padding: const EdgeInsets.only(left: 1, top: 0),
          child: Container(
            width: MediaQuery.of(context).size.width / 2.2,
            padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(25),
            ),
            child: Stack(children: [
              Container(
                padding: EdgeInsets.all(4),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25),
                    color: restaurantListModel[i].shopstatus == "CLOSED"
                        ? Colors.grey.withOpacity(0.3)
                        : Colors.white,
                    boxShadow: [
                      BoxShadow(
                        offset: const Offset(0, 3.0),
                        color: Color(0xfff0f0f0),
                        blurRadius: 10.0,
                        spreadRadius: 3.0,
                      )
                    ]),
                child: Material(
                  color: Colors.transparent,
                  child: InkWell(
                    borderRadius: BorderRadius.circular(25),
                    onTap: restaurantListModel[i].shopstatus == "CLOSED"
                        ? null
                        : () {
                            // print("asdasfas" + widget.cuisineName);
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => FoodItemsPage(
                                          shopRating: restaurantListModel[i]
                                              .rating
                                              .toString(),
                                          shopId: restaurantListModel[i]
                                              .id
                                              .toString(),
                                          shopPic:
                                              restaurantListModel[i].picture,
                                          shopLocation: restaurantListModel[i]
                                              .storeLocation,
                                          estimateTime: restaurantListModel[i]
                                              .estimatedDeliveryTime,
                                          restaurantName:
                                              restaurantListModel[i].storeName,
                                          cuisinesTypes:
                                              restaurantListModel[i].cusineList,
                                        )));
                          },
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(25.0),
                            child: CachedNetworkImage(
                              colorBlendMode: BlendMode.color,
                              color:
                                  restaurantListModel[i].shopstatus == "CLOSED"
                                      ? Colors.grey
                                      : null,
                              imageUrl: restaurantListModel[i].picture ?? "",
                              width: MediaQuery.of(context).size.width / 2.5,
                              height: MediaQuery.of(context).size.height / 7,
                              fit: BoxFit.fill,
                              // color: restaurantListModel[i].shopstatus == "CLOSED" ? Colors.grey.withOpacity(0.3) : null,
                              placeholder: (context, url) => Image.asset(
                                App24UserAppImages.placeHolderImage,
                                width: MediaQuery.of(context).size.width / 2.5,
                                height: MediaQuery.of(context).size.height / 7,
                                fit: BoxFit.fill,
                              ),
                              errorWidget: (context, url, error) => Image.asset(
                                App24UserAppImages.placeHolderImage,
                                width: MediaQuery.of(context).size.width / 2.5,
                                height: MediaQuery.of(context).size.height / 7,
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8, right: 8),
                          child: Text(
                            restaurantListModel[i].storeName ?? "",
                            style: TextStyle(
                                color: restaurantListModel[i].shopstatus ==
                                        "CLOSED"
                                    ? Colors.grey.withOpacity(0.6)
                                    : App24Colors.shopAndItemNameColor,
                                fontWeight: FontWeight.bold,
                                fontSize: 16),
                            // textAlign: TextAlign.center,
                            // maxLines: 2,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: Text(
                            restaurantListModel[i].cusineList ?? "",
                            style: TextStyle(
                              color:
                                  restaurantListModel[i].shopstatus == "CLOSED"
                                      ? Colors.grey.withOpacity(0.5)
                                      : Color(0xffFF1616).withOpacity(0.62),
                              fontSize: 10,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 3, right: 8, bottom: 13),
                          child: Row(
                            children: [
                              RatingBar.builder(
                                initialRating: rating,
                                direction: Axis.horizontal,
                                allowHalfRating: true,
                                itemCount: 5,
                                itemSize: 10,
                                itemPadding:
                                    EdgeInsets.symmetric(horizontal: 4.0),
                                itemBuilder: (context, _) => Icon(
                                  Icons.star,
                                  color: restaurantListModel[i].shopstatus ==
                                          "CLOSED"
                                      ? Colors.grey.withOpacity(0.3)
                                      : Colors.amber,
                                ),
                                onRatingUpdate: (rating) {
                                  print(rating);
                                },
                                ignoreGestures: true,
                              ),
                              // SmoothStarRating(
                              //   allowHalfRating: true,
                              //   isReadOnly: true,
                              //   rating: recentFoodTile[i].shop_rating,
                              // ),
                              Container(
                                height: 2,
                                width: 2,
                                decoration: BoxDecoration(
                                    color: Color(0xff9b9b9b),
                                    borderRadius: BorderRadius.circular(50)),
                              ),
                              const SizedBox(
                                width: 5,
                              ),
                              Text(
                                restaurantListModel[i].estimatedDeliveryTime! +
                                    " min",
                                style: TextStyle(
                                    color: Color(0xff9B9B9B),
                                    fontSize: 8,
                                    fontWeight: FontWeight.w600),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              // if(restaurantListModel[i].shopstatus == "CLOSED")
              //   Positioned(
              //     top: 60,
              //    width:  MediaQuery.of(context).size.width / 2.3,
              //    // top: 100,
              //     child: Container(
              //         height: 20,
              //       color: Colors.grey.withOpacity(0.6),
              //         child: Center(child: Text("Closed",style: TextStyle(fontWeight: FontWeight.w600,color: Colors.white),))),
              //   )
              // Positioned(
              //
              //   child: Container(
              //     decoration: BoxDecoration(
              //       borderRadius: BorderRadius.circular(24),
              //       color: Colors.grey.withOpacity(0.4),
              //     ),
              //     height: 200,
              //
              //   ),
              // )
              if (restaurantListModel[i].offerPercent! > 0)
                Positioned(
                  top: 100,
                  left: 10,
                  child: Container(
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                        color: App24Colors.greenOrderEssentialThemeColor,
                        borderRadius: BorderRadius.circular(5),
                        border: Border.all(
                            color: App24Colors.greenOrderEssentialThemeColor,
                            style: BorderStyle.solid,
                            width: 1)),
                    child: Center(
                        child: Text(
                      "${restaurantListModel[i].offerPercent}% OFF",
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.w500),
                    )),
                  ),
                )
            ]),
          ),
        ));
      }
      // if(widget.cuisineID == restaurantListModel[i].cusineList)
    }
    if (_widgetFood.isEmpty) {
      _widgetFood.add(Padding(
          padding: const EdgeInsets.only(left: 1, top: 10),
          child: Container(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              width: MediaQuery.of(context).size.width / 2.3,
              /*decoration: BoxDecoration(
    borderRadius: BorderRadius.circular(15),
    ),*/
              child: Text("No restaurant found"))));
    }
    return _widgetFood;
  }

  //Search Bar
  Widget buildSearchBar() {
    return Container(
      height: top > 80
          ? MediaQuery.of(context).size.width / 9
          : MediaQuery.of(context).size.width / 8,
      child: TextFormField(
        onChanged: (String? s) {
          setState(() {
            searchText = s;
          });
        },
        style: TextStyle(fontWeight: FontWeight.w300, fontSize: 14),
        // keyboardType: TextInputType.number,
        decoration: InputDecoration(
          // contentPadding: EdgeInsets.symmetric(
          //              horizontal: 5, vertical: top > 80 ? 9 : 10),
          prefixIcon: Icon(
            Icons.search,
            color: App24Colors.redRestaurantThemeColor,
            size: top > 80
                ? MediaQuery.of(context).size.width / 25
                : MediaQuery.of(context).size.width / 20,
          ),
          hintText: "search restaurants",
          hintStyle: TextStyle(
              fontSize: top > 80
                  ? MediaQuery.of(context).size.width / 32
                  : MediaQuery.of(context).size.width / 25,
              fontWeight: FontWeight.w600,
              color: App24Colors.redRestaurantThemeColor),
          border: OutlineInputBorder(
            borderRadius: top > 80
                ? BorderRadius.circular(15)
                : BorderRadius.circular(15),
            borderSide: BorderSide(
              width: 0,
              style: BorderStyle.none,
            ),
          ),
          fillColor: Theme.of(context).cardColor,
          filled: true,
          contentPadding: EdgeInsets.symmetric(
              vertical: top > 80 ? 0 : 0, horizontal: top > 80 ? 5 : 10),
        ),
      ),
    );
    //   Container(
    //   decoration: BoxDecoration(
    //       borderRadius: BorderRadius.circular(14.0),
    //       color: Theme.of(context).cardColor,
    //       border: Border.all(color: Colors.grey[200])),
    //   child: Material(
    //     color: Colors.transparent,
    //     child: InkWell(
    //         onTap: () {},
    //         borderRadius: BorderRadius.circular(14.0),
    //         child: Padding(
    //           padding: EdgeInsets.symmetric(
    //               horizontal: 5, vertical: top > 80 ? 9 : 10),
    //           child: Row(
    //             children: [
    //               Icon(
    //                 Icons.search,
    //                 color: App24Colors.greenOrderEssentialThemeColor,
    //                 size: top > 80 ? 18 : 24,
    //               ),
    //               const SizedBox(
    //                 width: 4.0,
    //               ),
    //               Expanded(
    //                   child: Text(
    //                     "Search for a store",
    //                     style: TextStyle(
    //                         color: App24Colors.greenOrderEssentialThemeColor,
    //                         fontWeight: FontWeight.w500,
    //                         fontSize: top > 80 ? 13 : 16),
    //                     maxLines: 1,
    //                     overflow: TextOverflow.ellipsis,
    //                   ))
    //             ],
    //           ),
    //         )),
    //   ),
    // );
  }

  //Location Button
  // Widget buildChangeLocationButton() {
  //   return Container(
  //     decoration: BoxDecoration(
  //         borderRadius: BorderRadius.circular(15),
  //         color: Color(0xffE3FFEE),
  //         border: Border.all(color: Colors.grey[200])),
  //     child: Material(
  //       color: Colors.transparent,
  //       child: InkWell(
  //         onTap: () {},
  //         borderRadius: BorderRadius.circular(15),
  //         child: Padding(
  //           padding: EdgeInsets.symmetric(
  //               vertical: top > 80 ? 9 : 14, horizontal: top > 80 ? 12 : 16),
  //           child: Icon(
  //             Icons.my_location,
  //             color: App24Colors.greenOrderEssentialThemeColor,
  //             size: top > 80 ? 20 : 24,
  //           ),
  //         ),
  //       ),
  //     ),
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: OfflineBuilderWidget(
        RefreshIndicator(
          onRefresh: () {
            return reloadData(init: false, context: context) ??
                false as Future<void>;
          },
          child: Container(
            child: Column(
              children: [
                CustomAppBar(
                  fromRestaurant: "fromRestaurant",
                  title: "All Restaurants",
                  hideCartIcon: "true",
                  // actions: cartIcon(context,"fromRestaurant"),
                ),
                Expanded(
                  child: CustomScrollView(
                    slivers: <Widget>[
                      SliverAppBar(
                          automaticallyImplyLeading: false,
                          backgroundColor: Colors.white,
                          pinned: true,
                          toolbarHeight: 0,
                          collapsedHeight:
                              MediaQuery.of(context).size.height / 20,
                          expandedHeight:
                              MediaQuery.of(context).size.height / 3,
                          flexibleSpace: LayoutBuilder(builder:
                              (BuildContext context,
                                  BoxConstraints constraints) {
                            // print('constraints=' + constraints.toString());
                            top = constraints.biggest.height;
                            return FlexibleSpaceBar(
                              titlePadding: EdgeInsetsDirectional.zero,
                              title: Padding(
                                padding: EdgeInsets.symmetric(horizontal: 17),
                                child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Row(
                                        children: [
                                          Expanded(child: buildSearchBar()),
                                          // const SizedBox(
                                          //   width: 5,
                                          // ),
                                          // buildChangeLocationButton()
                                        ],
                                      ),
                                      top > 80
                                          ? Container()
                                          : const SizedBox(
                                              height: 10,
                                            ),
                                    ]),
                              ),
                              background: Container(
                                  // margin: EdgeInsets.fromLTRB(20, 0, 20, 40),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(25),
                                    // boxShadow: [
                                    //   BoxShadow(
                                    //     color: Colors.grey,
                                    //     spreadRadius: 10,
                                    //     blurRadius: 20,
                                    //   )
                                    // ]
                                  ),
                                  child: Column(
                                    children: [
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 15),
                                        child: TextButton(
                                          onPressed: () {
                                            selectLocation(
                                                GlobalConstants
                                                    .labelDropLocation,
                                                true);
                                          },
                                          child: Row(
                                            children: [
                                              const Icon(
                                                App24User.path_2568_3x,
                                                size: 35,
                                                color: Color(0xffFF8282),
                                              ),
                                              Expanded(
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      context
                                                              .read(
                                                                  homeNotifierProvider
                                                                      .notifier)
                                                              .currentLocation
                                                              .main_text ??
                                                          '',
                                                      style: TextStyle(
                                                        fontSize: MediaQuery.of(
                                                                    context)
                                                                .size
                                                                .width /
                                                            20,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        color:
                                                            Color(0xffFF8282),
                                                      ),
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                    ),
                                                    // Text(
                                                    //   "Change Location",
                                                    //   style: TextStyle(
                                                    //       fontSize: 12,
                                                    //       fontWeight:
                                                    //           FontWeight.w600,
                                                    //       color: Color(0xff373737)
                                                    //           .withOpacity(0.3)),
                                                    // )
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      Container(
                                        child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.stretch,
                                            children: [
                                              CarouselSlider(
                                                items: Helpers()
                                                    .getCarouselImage(context),
                                                options: CarouselOptions(
                                                    height: 145,
                                                    autoPlay: true,
                                                    enlargeCenterPage: false,
                                                    onPageChanged:
                                                        (index, reason) {
                                                      setState(() {
                                                        _current = index;
                                                      });
                                                    }),
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: <Widget>[
                                                  ...List.generate(
                                                      Helpers()
                                                          .foodOfferCarousel
                                                          .length,
                                                      (index) => Container(
                                                            width: 8.0,
                                                            height: 8.0,
                                                            margin:
                                                                const EdgeInsets
                                                                    .symmetric(
                                                                    vertical:
                                                                        10.0,
                                                                    horizontal:
                                                                        2.0),
                                                            decoration:
                                                                BoxDecoration(
                                                              shape: BoxShape
                                                                  .circle,
                                                              color: _current ==
                                                                      index
                                                                  ? Theme.of(
                                                                          context)
                                                                      .primaryColor
                                                                  : Colors.grey[
                                                                      300],
                                                            ),
                                                          )),
                                                ],
                                              )
                                            ]),
                                      )
                                    ],
                                  )),
                            );
                          })),
                      SliverList(
                          delegate: SliverChildListDelegate([
                        Consumer(builder: (context, watch, child) {
                          final state = watch(restaurantNotifier);
                          if (state.isLoading) {
                            return RestaurantLoading();
                          } else if (state.isError) {
                            return LoadingError(
                              onPressed: (res) {
                                reloadData(init: true, context: res);
                              },
                              message: state.errorMessage.toString(),
                            );
                          } else {
                            if (state.response.responseData.length == 0) {
                              isNearbyShopsEmpty = true;
                              return Container(
                                margin: EdgeInsets.only(top: 80),
                                alignment: Alignment.center,
                                child: Text(
                                  state.response.message,
                                  style: TextStyle(fontWeight: FontWeight.w600),
                                ),
                              );
                            } else {
                              isNearbyShopsEmpty = false;
                              return Wrap(
                                alignment: WrapAlignment.center,
                                runAlignment: WrapAlignment.center,
                                spacing: 0,
                                runSpacing: 0,
                                children: getFoodTiles(
                                    context, state.response.responseData),
                              );
                            }
                          }
                        }),
                        const SizedBox(
                          height: 20,
                        )
                      ])),
                    ],
                  ),
                ),
                ViewCartOverlay()
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class RecentFoodTile {
  String? foodImage;
  String? foodShopName;
  String? foodMainItems;
  String? foodEstimateTime;
  String? foodShopRating;

  RecentFoodTile(
      {this.foodEstimateTime,
      this.foodImage,
      this.foodMainItems,
      this.foodShopName,
      this.foodShopRating});
}
