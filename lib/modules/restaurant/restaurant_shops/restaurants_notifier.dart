

import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/api/api_response_state2.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';


class RestaurantsNotifier extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  RestaurantsNotifier(this._apiRepository) : super(ResponseState2(isLoading: true));

  Future<void> getRestaurants({bool init = true,required BuildContext context}) async {
    // try {
      if (init) state = state.copyWith(isLoading: true);
      String latLong = '';
      if (context
          .read(homeNotifierProvider.notifier)
          .currentLocation
          .latitude !=
          null &&
          context
              .read(homeNotifierProvider.notifier)
              .currentLocation
              .longitude !=
              null) {
        latLong =
        '?latitude=${context.read(homeNotifierProvider.notifier).currentLocation.latitude}&'
            'longitude=${context.read(homeNotifierProvider.notifier).currentLocation.longitude}&recommended=';
      }
      final restaurants = await _apiRepository.fetchRestaurants(latLong: latLong);
      state = state.copyWith(
          response: restaurants, isLoading: false, isError: false);
    // } catch (e) {
    //   state = state.copyWith(
    //       errorMessage: e.toString(), isLoading: false, isError: true);
    // }
  }

// Future<void> getCuisines({bool init = true, BuildContext context}) async {
//   try {
//     if (init) state = state.copyWith(isLoading: true);
//     final cuisines = await _apiRepository.fetchCuisines(context);
//     state = state.copyWith(response: cuisines,isLoading: false,isError:false );
//   } catch (e) {
//     state = state.copyWith(errorMessage: e.toString(),isLoading: false,isError: true);
//   }
// }

}
