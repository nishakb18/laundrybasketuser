AllRestaurantsModel restaurantsModelFromJson(str) {
  return AllRestaurantsModel.fromJson(str);
}

class AllRestaurantsModel {
  String? statusCode;
  String? title;
  String? message;
  List<RestaurantResponseData>? responseData;
  List<dynamic>? error;

  AllRestaurantsModel(
      {this.statusCode,
      this.title,
      this.message,
      this.responseData,
      this.error});

  AllRestaurantsModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    if (json['responseData'] != null) {
      responseData = [];
      json['responseData'].forEach((v) {
        responseData!.add(new RestaurantResponseData.fromJson(v));
      });
    }
    if (json['error'] != null) {
      error = [];
      json['error'].forEach((v) {
        error!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['title'] = this.title;
    data['message'] = this.message;
    if (this.responseData != null) {
      data['responseData'] = this.responseData!.map((v) => v.toJson()).toList();
    }
    if (this.error != null) {
      data['error'] = this.error!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class RestaurantResponseData {
  int? id;
  int? storeTypeId;
  String? storeName;
  String? storeLocation;
  double? latitude;
  double? longitude;
  String? picture;
  String? offerMinAmount;
  String? estimatedDeliveryTime;
  int? freeDelivery;
  String? isVeg;
  dynamic rating;
  int? offerPercent;
  String? cusineList;
  dynamic shopstatus;
  String? storetypename;
  List<Categories>? categories;
  Storetype? storetype;
  List<StoreCusinie>? storeCusinie;

  RestaurantResponseData(
      {this.id,
      this.storeTypeId,
      this.storeName,
      this.storeLocation,
      this.latitude,
      this.longitude,
      this.picture,
      this.offerMinAmount,
      this.estimatedDeliveryTime,
      this.freeDelivery,
      this.isVeg,
      this.rating,
      this.offerPercent,
      this.cusineList,
      this.shopstatus,
      this.storetypename,
      this.categories,
      this.storetype,
      this.storeCusinie});

  RestaurantResponseData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    storeTypeId = json['store_type_id'];
    storeName = json['store_name'];
    storeLocation = json['store_location'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    picture = json['picture'];
    offerMinAmount = json['offer_min_amount'];
    estimatedDeliveryTime = json['estimated_delivery_time'];
    freeDelivery = json['free_delivery'];
    isVeg = json['is_veg'];
    rating = json['rating'];
    offerPercent = json['offer_percent'];
    cusineList = json['cusine_list'];
    shopstatus = json['shopstatus'];
    storetypename = json['storetypename'];
    if (json['categories'] != null) {
      categories = [];
      json['categories'].forEach((v) {
        categories!.add(new Categories.fromJson(v));
      });
    }
    storetype = json['storetype'] != null
        ? new Storetype.fromJson(json['storetype'])
        : null;
    if (json['store_cusinie'] != null) {
      storeCusinie = [];
      json['store_cusinie'].forEach((v) {
        storeCusinie!.add(new StoreCusinie.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['store_type_id'] = this.storeTypeId;
    data['store_name'] = this.storeName;
    data['store_location'] = this.storeLocation;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['picture'] = this.picture;
    data['offer_min_amount'] = this.offerMinAmount;
    data['estimated_delivery_time'] = this.estimatedDeliveryTime;
    data['free_delivery'] = this.freeDelivery;
    data['is_veg'] = this.isVeg;
    data['rating'] = this.rating;
    data['offer_percent'] = this.offerPercent;
    data['cusine_list'] = this.cusineList;
    data['shopstatus'] = this.shopstatus;
    data['storetypename'] = this.storetypename;
    if (this.categories != null) {
      data['categories'] = this.categories!.map((v) => v.toJson()).toList();
    }
    if (this.storetype != null) {
      data['storetype'] = this.storetype!.toJson();
    }
    if (this.storeCusinie != null) {
      data['store_cusinie'] = this.storeCusinie!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Categories {
  int? id;
  int? storeId;
  String? storeCategoryName;
  String? storeCategoryDescription;
  String? picture;
  int? storeCategoryStatus;

  Categories(
      {this.id,
      this.storeId,
      this.storeCategoryName,
      this.storeCategoryDescription,
      this.picture,
      this.storeCategoryStatus});

  Categories.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    storeId = json['store_id'];
    storeCategoryName = json['store_category_name'];
    storeCategoryDescription = json['store_category_description'];
    picture = json['picture'];
    storeCategoryStatus = json['store_category_status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['store_id'] = this.storeId;
    data['store_category_name'] = this.storeCategoryName;
    data['store_category_description'] = this.storeCategoryDescription;
    data['picture'] = this.picture;
    data['store_category_status'] = this.storeCategoryStatus;
    return data;
  }
}

class Storetype {
  int? id;
  String? name;
  String? category;
  String? picture;
  int? status;
  String? itemsStatus;
  String? addonsStatus;

  Storetype(
      {this.id,
      this.name,
      this.category,
      this.picture,
      this.status,
      this.itemsStatus,
      this.addonsStatus});

  Storetype.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    category = json['category'];
    picture = json['picture'];
    status = json['status'];
    itemsStatus = json['items_status'];
    addonsStatus = json['addons_status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['category'] = this.category;
    data['picture'] = this.picture;
    data['status'] = this.status;
    data['items_status'] = this.itemsStatus;
    data['addons_status'] = this.addonsStatus;
    return data;
  }
}

class StoreCusinie {
  int? id;
  int? storeTypeId;
  int? storeId;
  int? cuisinesId;
  Cuisine? cuisine;

  StoreCusinie(
      {this.id, this.storeTypeId, this.storeId, this.cuisinesId, this.cuisine});

  StoreCusinie.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    storeTypeId = json['store_type_id'];
    storeId = json['store_id'];
    cuisinesId = json['cuisines_id'];
    cuisine =
        json['cuisine'] != null ? new Cuisine.fromJson(json['cuisine']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['store_type_id'] = this.storeTypeId;
    data['store_id'] = this.storeId;
    data['cuisines_id'] = this.cuisinesId;
    if (this.cuisine != null) {
      data['cuisine'] = this.cuisine!.toJson();
    }
    return data;
  }
}

class Cuisine {
  int? id;
  int? storeTypeId;
  String? name;
  int? status;

  Cuisine({this.id, this.storeTypeId, this.name, this.status});

  Cuisine.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    storeTypeId = json['store_type_id'];
    name = json['name'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['store_type_id'] = this.storeTypeId;
    data['name'] = this.name;
    data['status'] = this.status;
    return data;
  }
}
