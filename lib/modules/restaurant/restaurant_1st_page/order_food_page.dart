import 'package:app24_user_app/app24_user_icons.dart';
import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/constants/global_constants.dart';
import 'package:app24_user_app/constants/shared_preferences_path.dart';
import 'package:app24_user_app/helpers.dart';
import 'package:app24_user_app/models/location_model.dart';
import 'package:app24_user_app/modules/delivery/shop_list/shop_categories.dart';
import 'package:app24_user_app/modules/restaurant/favouriteShopListModel.dart';
import 'package:app24_user_app/modules/restaurant/food_items_page/food_item_page.dart';
import 'package:app24_user_app/modules/restaurant/restaurant_1st_page/Cuisines/cuisine_loading.dart';
import 'package:app24_user_app/modules/restaurant/restaurant_1st_page/Cuisines/cuisine_model.dart';
import 'package:app24_user_app/modules/restaurant/restaurant_1st_page/Cuisines/cuisine_see_all.dart';
import 'package:app24_user_app/modules/restaurant/restaurant_cart_page.dart';
import 'package:app24_user_app/modules/restaurant/restaurant_shops/all_restaurants_page.dart';
import 'package:app24_user_app/modules/restaurant/restaurant_shops/restaurant_loading.dart';
import 'package:app24_user_app/modules/restaurant/restaurant_shops/restaurants_model.dart';
import 'package:app24_user_app/modules/tabs/home/widgets/offers_loading.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/widgets/bottomsheets/promocodes/landing_page_promo_code/landing_page_promo_codes_model.dart';
import 'package:app24_user_app/widgets/common_appbar.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:app24_user_app/widgets/offline_builder_widget.dart';
import 'package:app24_user_app/widgets/view_cart_overlay.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OrderFoodPage extends StatefulWidget {
  const OrderFoodPage({Key? key}) : super(key: key);

  @override
  _OrderFoodPageState createState() => _OrderFoodPageState();
}

class _OrderFoodPageState extends State<OrderFoodPage> {
  List<RecentFoodTile> recentFoodTile = [];

  // List<CuisineTiles> cuisinesTile = [];
  double sliverAppbarHeight = 0.0;
  double collapsedHeight = 0.0;
  int _current = 0;
  bool isNearbyShopsEmpty = true;
  List<String> favouriteShops = [];

  // List<String> foodOfferCarousel = [
  //   App24UserAppImages.foodOffer,
  //   App24UserAppImages.foodOffer,
  //   App24UserAppImages.foodOffer,
  //   App24UserAppImages.foodOffer,
  // ];

  reloadData({required BuildContext context, required bool init}) async {
     context.read(cuisineNotifier.notifier).getCuisines(init: init);
    var prefs =
        await SharedPreferences
        .getInstance();
    favouriteShops = prefs.getStringList(SharedPreferencesPath.favouriteRestaurants) ?? [];
    // print("abcdefgh" + favouriteShops.toString());
    //API call after screen build
    // context.read(homeNotifierProvider).getOffers(context: context);
    context.read(cuisineNotifier.notifier).getCuisines();
    context
        .read(restaurantNotifier.notifier)
        .getRestaurants(context: context);
    context.read(cartCountNotifierProvider.notifier).getCartCount(context: context);
  }

  @override
  void initState() {
    SchedulerBinding.instance!.addPostFrameCallback((_) async {
      var prefs =
      await SharedPreferences
          .getInstance();
      favouriteShops = prefs.getStringList(SharedPreferencesPath.favouriteRestaurants) ?? [];
      // print("abcdefgh" + favouriteShops.toString());
      //API call after screen build
      // context.read(homeNotifierProvider).getOffers(context: context);
      context.read(cuisineNotifier.notifier).getCuisines();
      context
          .read(restaurantNotifier.notifier)
          .getRestaurants(context: context);
      context.read(cartCountNotifierProvider.notifier).getCartCount(context: context);
      loadData(context: context);
    });

    super.initState();
  }

  loadData({bool init = true, required BuildContext context}) {
    return context
        .read(restaurantNotifier.notifier)
        .getRestaurants(init: init, context: context);

  }

  // List<Widget> getCarouselImage() {
  //   final List<Widget> imageSliders = foodOfferCarousel
  //       .map((item) => Container(
  //             decoration: BoxDecoration(
  //               borderRadius: BorderRadius.circular(20),
  //               // border: Border.all(
  //               //   color: Colors.grey[200],
  //               // )
  //             ),
  //             margin: EdgeInsets.all(15.0),
  //             child: ClipRRect(
  //               borderRadius: BorderRadius.all(Radius.circular(20.0)),
  //               child: Image.asset(
  //                 item,
  //                 fit: BoxFit.contain,
  //                 width: MediaQuery.of(context).size.width,
  //               ),
  //             ),
  //           ))
  //       .toList();
  //
  //   return imageSliders;
  // }

  List<Widget> getFavouriteRestaurantTiles(
      context, List<RestaurantResponseData>? restaurantListModel) {
    List<Widget> _widgetFood = [];
print("favourite shop list" + favouriteShops.toString());
    // restaurantListModel!
    //     .sort((a, b) => a.shopstatus!.compareTo(b.shopstatus!));

    for (var i = 0; i < restaurantListModel!.length; i++) {
      print("abcdef" + favouriteShops.contains("43").toString());
      if(favouriteShops.contains(restaurantListModel[i].id.toString()))
        // _widgetFood.add(Text("asdas"));
        // print("favourite" + favouriteShops.contains(restaurantListModel[i].id).toString());
      // if(restaurantListModel[i].id!.toString().contains((favouriteShops.toString())))
      // if(restaurantListModel[i].id == favouriteShops.contains(restaurantListModel[i].id.toString()))
      // if(restaurantListModel[i].id.toString().contains(favouriteShops))


      _widgetFood.add(Padding(
        padding: const EdgeInsets.only(left: 1, top: 10),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 5, vertical: 14),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
          ),
          child: Stack(children: [
            Container(
              width: MediaQuery.of(context).size.width / 2.25,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(25),
                  color: (restaurantListModel[i].shopstatus == "CLOSED" || restaurantListModel[i].shopstatus == "Temporarily Closed")
                      ? Colors.grey.withOpacity(0.3)
                      : Colors.white,
                  boxShadow: [
                    BoxShadow(
                      offset: const Offset(0, 3.0),
                      color: Color(0xfff0f0f0),
                      blurRadius: 10.0,
                      spreadRadius: 3.0,
                    )
                  ]),
              child: Material(
                color: Colors.transparent,
                child: InkWell(
                  borderRadius: BorderRadius.circular(25),
                  onTap: (restaurantListModel[i].shopstatus == "CLOSED" || restaurantListModel[i].shopstatus == "Temporarily Closed")
                      ? null
                      : () {
                    // print("asdasfas" + widget.cuisineName);
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => FoodItemsPage(
                              shopRating:
                              restaurantListModel[i].rating.toString(),
                              shopId: restaurantListModel[i]
                                  .id
                                  .toString(),
                              shopLocation: restaurantListModel[i].storeLocation,
                              estimateTime: restaurantListModel[i].estimatedDeliveryTime,
                              restaurantName:
                              restaurantListModel[i].storeName,
                              shopPic: restaurantListModel[i].picture,
                              cuisinesTypes:
                              restaurantListModel[i].cusineList,
                            )));
                  },
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(25.0),
                          child: CachedNetworkImage(
                            colorBlendMode: BlendMode.color,
                            color: (restaurantListModel[i].shopstatus == "CLOSED" || restaurantListModel[i].shopstatus == "Temporarily Closed")
                                ? Colors.grey
                                : null,
                            imageUrl: restaurantListModel[i].picture ?? '',
                            width: MediaQuery.of(context).size.width / 2.5,
                            height: MediaQuery.of(context).size.height / 7,
                            fit: BoxFit.fill,
                            placeholder: (context, url) => Image.asset(
                              App24UserAppImages.placeHolderImage,
                              width: MediaQuery.of(context).size.width / 2.5,
                              height: MediaQuery.of(context).size.height / 7,
                              fit: BoxFit.fill,
                            ),
                            errorWidget: (context, url, error) => Image.asset(
                              App24UserAppImages.placeHolderImage,
                              width: MediaQuery.of(context).size.width / 2.5,
                              height: MediaQuery.of(context).size.height / 7,
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8, right: 8),
                        child: Text(
                          restaurantListModel[i].storeName ?? "",
                          style: TextStyle(
                              color:
                              (restaurantListModel[i].shopstatus == "CLOSED" || restaurantListModel[i].shopstatus == "Temporarily Closed")
                                  ? Colors.grey.withOpacity(0.6)
                                  : App24Colors.shopAndItemNameColor,
                              fontWeight: FontWeight.bold,
                              fontSize: 16),
                          // textAlign: TextAlign.center,
                          // maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: Text(
                          restaurantListModel[i].shopstatus ?? "",
                          style: TextStyle(
                            color: (restaurantListModel[i].shopstatus == "CLOSED" || restaurantListModel[i].shopstatus == "Temporarily Closed")
                                ? Colors.grey.withOpacity(0.5)
                                : Color(0xffFF1616).withOpacity(0.62),
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Padding(
                        padding:
                        const EdgeInsets.only(left: 3, right: 8, bottom: 8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            RatingBar.builder(
                              initialRating: double.parse(
                                  restaurantListModel[i].rating.toString()),
                              direction: Axis.horizontal,
                              allowHalfRating: true,
                              itemCount: 5,
                              itemSize: 11,
                              itemPadding:
                              EdgeInsets.symmetric(horizontal: 4.0),
                              itemBuilder: (context, _) => Icon(
                                Icons.star,
                                color: restaurantListModel[i].shopstatus ==
                                    "CLOSED"
                                    ? Colors.grey.withOpacity(0.3)
                                    : Colors.amber,
                              ),
                              onRatingUpdate: (rating) {
                                print(rating);
                              },
                              ignoreGestures: true,
                            ),
                            Container(
                              height: 2,
                              width: 2,
                              decoration: BoxDecoration(
                                  color: Color(0xff9b9b9b),
                                  borderRadius: BorderRadius.circular(50)),
                            ),
                            const SizedBox(
                              width: 5,
                            ),
                            Text(
                              restaurantListModel[i].estimatedDeliveryTime! +
                                  " min",
                              style: TextStyle(
                                  color: Color(0xff9B9B9B),
                                  fontSize: 8,
                                  fontWeight: FontWeight.w600),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ]),
        ),
      ));
    }
    return _widgetFood;
  }

  List<Widget> getNearByRestaurantTiles(
      context, List<RestaurantResponseData>? restaurantListModel) {
    List<Widget> _widgetFood = [];
    restaurantListModel!
        .sort((a, b) => b.shopstatus!.compareTo(a.shopstatus!));
    for (var i = 0; i < restaurantListModel.length; i++) {
      _widgetFood.add(Padding(
        padding: const EdgeInsets.only(left: 1, top: 10),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 5, vertical: 14),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
          ),
          child: Stack(children: [
            Container(
              width: MediaQuery.of(context).size.width / 2.25,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(25),
                  color: (restaurantListModel[i].shopstatus == "CLOSED" || restaurantListModel[i].shopstatus == "Temporarily Closed")
                      ? Colors.grey.withOpacity(0.3)
                      : Colors.white,
                  boxShadow: [
                    BoxShadow(
                      offset: const Offset(0, 3.0),
                      color: Color(0xfff0f0f0),
                      blurRadius: 10.0,
                      spreadRadius: 3.0,
                    )
                  ]),
              child: Material(
                color: Colors.transparent,
                child: InkWell(
                  borderRadius: BorderRadius.circular(25),
                  onTap: (restaurantListModel[i].shopstatus == "CLOSED" || restaurantListModel[i].shopstatus == "Temporarily Closed")
                      ? null
                      : () {
                          // print("asdasfas" + widget.cuisineName);
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => FoodItemsPage(
                                    shopRating:
                                        restaurantListModel[i].rating.toString(),
                                        shopId: restaurantListModel[i]
                                            .id
                                            .toString(),
                                        shopPic: restaurantListModel[i].picture,
                                    shopLocation: restaurantListModel[i].storeLocation,
                                    estimateTime: restaurantListModel[i].estimatedDeliveryTime,
                                        restaurantName:
                                            restaurantListModel[i].storeName,
                                        cuisinesTypes:
                                            restaurantListModel[i].cusineList,
                                      )));
                        },
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(25.0),
                          child: CachedNetworkImage(
                            colorBlendMode: BlendMode.color,
                            color: (restaurantListModel[i].shopstatus == "CLOSED" || restaurantListModel[i].shopstatus == "Temporarily Closed")
                                ? Colors.grey
                                : null,
                            imageUrl: restaurantListModel[i].picture ?? '',
                            width: MediaQuery.of(context).size.width / 2.5,
                            height: MediaQuery.of(context).size.height / 7,
                            fit: BoxFit.fill,
                            placeholder: (context, url) => Image.asset(
                              App24UserAppImages.placeHolderImage,
                              width: MediaQuery.of(context).size.width / 2.5,
                              height: MediaQuery.of(context).size.height / 7,
                              fit: BoxFit.fill,
                            ),
                            errorWidget: (context, url, error) => Image.asset(
                              App24UserAppImages.placeHolderImage,
                              width: MediaQuery.of(context).size.width / 2.5,
                              height: MediaQuery.of(context).size.height / 7,
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8, right: 8),
                        child: Text(
                          restaurantListModel[i].storeName ?? "",
                          style: TextStyle(
                              color:
                                  (restaurantListModel[i].shopstatus == "CLOSED" || restaurantListModel[i].shopstatus == "Temporarily Closed")
                                      ? Colors.grey.withOpacity(0.6)
                                      : App24Colors.shopAndItemNameColor,
                              fontWeight: FontWeight.bold,
                              fontSize: 16),
                          // textAlign: TextAlign.center,
                          // maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: Text(
                          restaurantListModel[i].shopstatus ?? "",
                          style: TextStyle(
                            color: (restaurantListModel[i].shopstatus == "CLOSED" || restaurantListModel[i].shopstatus == "Temporarily Closed")
                                ? Colors.grey.withOpacity(0.5)
                                : Color(0xffFF1616).withOpacity(0.62),
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Padding(
                        padding:
                            const EdgeInsets.only(left: 3, right: 8, bottom: 8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            RatingBar.builder(
                              initialRating: double.parse(
                                  restaurantListModel[i].rating.toString()),
                              direction: Axis.horizontal,
                              allowHalfRating: true,
                              itemCount: 5,
                              itemSize: 11,
                              itemPadding:
                                  EdgeInsets.symmetric(horizontal: 4.0),
                              itemBuilder: (context, _) => Icon(
                                Icons.star,
                                color: restaurantListModel[i].shopstatus ==
                                        "CLOSED"
                                    ? Colors.grey.withOpacity(0.3)
                                    : Colors.amber,
                              ),
                              onRatingUpdate: (rating) {
                                print(rating);
                              },
                              ignoreGestures: true,
                            ),
                            Container(
                              height: 2,
                              width: 2,
                              decoration: BoxDecoration(
                                  color: Color(0xff9b9b9b),
                                  borderRadius: BorderRadius.circular(50)),
                            ),
                            const SizedBox(
                              width: 5,
                            ),
                            Text(
                              restaurantListModel[i].estimatedDeliveryTime! +
                                  " min",
                              style: TextStyle(
                                  color: Color(0xff9B9B9B),
                                  fontSize: 8,
                                  fontWeight: FontWeight.w600),
                            )
                          ],
                        ),
                      ),
                      if(restaurantListModel[i].offerPercent! > 0)
                      Center(child: Text("${restaurantListModel[i].offerPercent}% OFF",style: TextStyle(color: App24Colors.greenOrderEssentialThemeColor,fontWeight: FontWeight.w500),))
                    ],
                  ),
                ),
              ),
            ),
          ]),
        ),
      ));
    }
    return _widgetFood;
  }

  selectLocation(String title,bool searchUserLocation) {
    showSelectLocationSheet(context: context, title: title,searchUserLocation:searchUserLocation).then((value) {
      if (value != null) {
        LocationModel model = value;
        setState(() {
          context.read(homeNotifierProvider.notifier).currentLocation = model;
          loadData(context: context);
        });
      }
    });
  }

  Widget buildSearchBar() {
    return Container(
      decoration: BoxDecoration(
        boxShadow: [
          sliverAppbarHeight > collapsedHeight
              ? BoxShadow(
                  color: Colors.grey[200]!,
                  spreadRadius: 1,
                  blurRadius: 3,
                  offset: Offset(0, 2))
              : BoxShadow(color: Colors.transparent)
        ],
        borderRadius: BorderRadius.circular(
            sliverAppbarHeight > collapsedHeight ? 14 : 15),
        color: Colors.white,
        // border: Border.all(color: Colors.grey[200]!)
      ),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
            onTap: () {
              selectLocation(GlobalConstants.labelGlobalLocation,true);
            },
            borderRadius: BorderRadius.circular(14.0),
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: 10,
                  vertical: sliverAppbarHeight > collapsedHeight ? 12 : 15),
              child: Row(
                children: [
                  Icon(
                    App24User.path_2568_3x,
                    color: App24Colors.redRestaurantThemeColor,
                    size: sliverAppbarHeight > collapsedHeight ? 15 : 16,
                  ),
                  const SizedBox(
                    width: 7.0,
                  ),
                  Expanded(
                      child: Text(
                    context
                            .read(homeNotifierProvider.notifier)
                            .currentLocation
                            .main_text ??
                        'Look up your address',
                    style: TextStyle(
                        color: App24Colors.redRestaurantThemeColor,
                        fontWeight: FontWeight.w600,
                        fontSize:
                            sliverAppbarHeight > collapsedHeight ? 11 : 15),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ))
                ],
              ),
            )),
      ),
    );
  }

  List<Widget> cuisineTiles(context, List<CuisineResponseData>? cuisineModel) {
    List<Widget> cuisineItems = [];
    print(cuisineModel!.length);
    for (var i = 0; i < 8; i++) {
      cuisineItems.add(
        Column(
          children: [
            Container(
              width: MediaQuery.of(context).size.width / 5,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
              ),
              // padding: EdgeInsets.all(1),
              child: Material(
                borderRadius: BorderRadius.circular(15),
                color: Colors.transparent,
                child: InkWell(
                  borderRadius: BorderRadius.circular(15),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => AllRestaurantPage(
                                cuisineName: cuisineModel[i].name!,
                              )),
                    );
                  },
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      // const SizedBox(
                      //   height: 15,
                      // ),
                      ClipRRect(
                        borderRadius: BorderRadius.circular(20),
                        child: CachedNetworkImage(
                          imageUrl: cuisineModel[i].picture ?? '',
                          fit: BoxFit.cover,
                          width: MediaQuery.of(context).size.width / 5,
                          // MediaQuery.of(context).size.width / 2 - 20,
                          height: MediaQuery.of(context).size.height / 11,
                          placeholder: (context, url) => Image.asset(
                            App24UserAppImages.placeHolderImage,
                            width: MediaQuery.of(context).size.width / 6,
                            height: MediaQuery.of(context).size.height / 12,
                            fit: BoxFit.cover,
                          ),
                          errorWidget: (context, url, error) => Image.asset(
                            App24UserAppImages.placeHolderImage,
                            width: MediaQuery.of(context).size.width / 6,
                            height: MediaQuery.of(context).size.height / 12,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 3,
                      ),
                      Text(
                        cuisineModel[i].name!,
                        style: TextStyle(
                            color: App24Colors.darkTextColor,
                            fontWeight: FontWeight.w600,
                            fontSize: 12),
                        textAlign: TextAlign.center,
                        maxLines: 2,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 5,
            ),
          ],
        ),
      );
    }
    return cuisineItems;
  }

  List<Widget> getCarouselImages(
      context, List<LandingPagePromoResponseData> promoCodeList) {
    final List<Widget> imageSliders = promoCodeList
        .map((item) => Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Stack(children: [
                Container(
                  // padding: EdgeInsets.symmetric(horizontal: 10),
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(30),
                      child: new FadeInImage.assetNetwork(
                        placeholder: 'assets/images/placeholder.png',
                        image: item.picture!,
                        fit: BoxFit.fitHeight,
                        width: MediaQuery.of(context).size.width,
                      )),
                ),
                Material(
                  borderRadius: BorderRadius.circular(25),
                  color: Colors.transparent,
                  child: InkWell(
                      borderRadius: BorderRadius.circular(25),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ShopCategoryPage(
                                      promoCodeDetails:
                                          item.promoDescription.toString(),
                                      promoCode: item.promoCode,
                                      storeTypeID:
                                          item.storeMultiTypeId.toString(),
                                    )));
                      }),
                )
              ]),
            ))
        .toList();

    return imageSliders;
  }

  @override
  Widget build(BuildContext context) {
    collapsedHeight = MediaQuery.of(context).size.height * 0.125;
    return Scaffold(
      // appBar: CustomAppBar(
      //   title: "Order Food",
      // ),
      body: SafeArea(
        child: OfflineBuilderWidget(
           Container(
            // padding: EdgeInsets.symmetric(vertical: 10),
            child: Column(
              children: [
                Expanded(
                    child: CustomScrollView(slivers: <Widget>[
                  SliverAppBar(
                      backgroundColor: App24Colors.redRestaurantThemeColor,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                              bottomRight: Radius.circular(20),
                              bottomLeft: Radius.circular(20))),
                      automaticallyImplyLeading: true,
                      pinned: true,
                      toolbarHeight: 56.0,
                      elevation: 0.0,
                      iconTheme: IconThemeData(color: App24Colors.darkTextColor),
                      // actions: [
                      //   Padding(
                      //     padding: const EdgeInsets.only(right: 15),
                      //     child: cartIcon(context:context,fromPage: "fromRestaurant"),
                      //     // IconButton(
                      //     //     iconSize: 17,
                      //     //     icon: Icon(
                      //     //       App24User.group_636_3x,
                      //     //     ),
                      //     //     onPressed: () {
                      //     //       cartIcon(context,"fromRestaurant");
                      //     //       // Navigator.push(
                      //     //       //     context,
                      //     //       //     MaterialPageRoute(
                      //     //       //         builder: (context) =>
                      //     //       //             RestaurantCartPage() /*TrackOrder()*/));
                      //     //     }),
                      //   )
                      // ],
                      title: Text(
                        "Shop Essentials",
                        style: TextStyle(
                            color: App24Colors.darkTextColor,
                            fontSize: MediaQuery.of(context).size.width / 25,
                            fontWeight: FontWeight.bold),
                      ),
                      collapsedHeight: collapsedHeight,
                      expandedHeight: MediaQuery.of(context).size.height * 0.460,
                      flexibleSpace: LayoutBuilder(builder:
                          (BuildContext context, BoxConstraints constraints) {
                        // print('constraints=' + constraints.toString());
                        sliverAppbarHeight = constraints.biggest.height;
                        return FlexibleSpaceBar(
                          titlePadding: EdgeInsetsDirectional.zero,
                          title: Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 13,
                                vertical: sliverAppbarHeight > collapsedHeight
                                    ? 5
                                    : 10),
                            child: buildSearchBar(),
                          ),
                          background: Hero(
                            tag: "3",
                            child: Container(
                              //padding: EdgeInsets.only(top: 60),
                              color: Colors.white,
                              child: Container(
                                  color: Colors.white,
                                  margin: EdgeInsets.only(bottom: 35),
                                  child: Stack(
                                    fit: StackFit.expand,
                                    children: [
                                      Positioned(
                                        bottom: 0,
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.only(
                                            bottomLeft: Radius.circular(30),
                                            bottomRight: Radius.circular(30),
                                          ),
                                          child: Image.asset(
                                            App24UserAppImages.foodPageBG,
                                            fit: BoxFit.cover,
                                            width:
                                                MediaQuery.of(context).size.width,
                                          ),
                                        ),
                                      ),
                                      Positioned(
                                          left: 20,
                                          bottom: 50,
                                          right: 20,
                                          child: Text(
                                            "Get delicious food delivered home safe and COVID-free",
                                            overflow: TextOverflow.clip,
                                            style: TextStyle(
                                                fontSize: MediaQuery.of(context).size.width/22,
                                                color: Colors.white,
                                                fontWeight: FontWeight.w600),
                                          ))
                                    ],
                                  )),
                            ),
                          ),
                        );
                      })),
                  SliverList(
                    delegate: SliverChildListDelegate([
                      Container(
                        padding: EdgeInsets.only(top: 15),
                        color: Colors.white,
                        child: Column(
                          children: [

                            Consumer(builder: (context, watch, child) {
                              final state = watch(landingPagePromoCodeProvider);
                              if (state.isLoading) {
                                return OffersLoading();
                              } else if (state.isError) {
                                return LoadingError(
                                  onPressed: (res) {
                                    reloadData(init: true, context: res);
                                  },
                                  message: state.errorMessage,
                                );
                              } else {
                                if(state.response.responseData.length != 0) {
                                  return Container(
                                    child: Column(
                                        crossAxisAlignment:
                                        CrossAxisAlignment.stretch,
                                        children: [
                                          CarouselSlider(
                                            items: getCarouselImages(context,
                                                state.response.responseData),
                                            options: CarouselOptions(
                                                height: 150,
                                                autoPlay: true,
                                                enlargeCenterPage: false,
                                                onPageChanged: (index, reason) {
                                                  setState(() {
                                                    _current = index;
                                                  });
                                                }),
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                            MainAxisAlignment.center,
                                            children: <Widget>[
                                              ...List.generate(
                                                  state
                                                      .response.responseData
                                                      .length,
                                                      (index) =>
                                                      Container(
                                                        width: 8.0,
                                                        height: 8.0,
                                                        margin:
                                                        EdgeInsets.symmetric(
                                                            vertical: 10.0,
                                                            horizontal: 2.0),
                                                        decoration: BoxDecoration(
                                                          shape: BoxShape
                                                              .circle,
                                                          color: _current ==
                                                              index
                                                              ? Theme
                                                              .of(context)
                                                              .primaryColor
                                                              : Colors
                                                              .grey[300],
                                                        ),
                                                      )),
                                            ],
                                          )
                                        ]),
                                  );
                                }else{
                                  return Container(
                                    child: Column(
                                        crossAxisAlignment: CrossAxisAlignment
                                            .stretch,
                                        children: [
                                          CarouselSlider(
                                            items: Helpers().getCarouselImage(context),
                                            options: CarouselOptions(
                                                height: 145,
                                                autoPlay: true,
                                                enlargeCenterPage: false,
                                                onPageChanged: (
                                                    index,
                                                    reason) {
                                                  setState(() {
                                                    _current =
                                                        index;
                                                  });
                                                }),
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment
                                                .center,
                                            children: <Widget>[
                                              ...List.generate(
                                                  Helpers().foodOfferCarousel
                                                      .length,
                                                      (index) =>
                                                      Container(
                                                        width: 8.0,
                                                        height: 8.0,
                                                        margin: EdgeInsets
                                                            .symmetric(
                                                            vertical: 10.0,
                                                            horizontal: 2.0),
                                                        decoration: BoxDecoration(
                                                          shape: BoxShape
                                                              .circle,
                                                          color: _current ==
                                                              index
                                                              ? Theme
                                                              .of(
                                                              context)
                                                              .primaryColor
                                                              : Colors
                                                              .grey[300],
                                                        ),
                                                      )),
                                            ],
                                          )
                                        ]),
                                  );
                                }
                              }
                            })

                            // Container(
                            //   // padding: EdgeInsets.symmetric(horizontal: 30),
                            //   child: Column(
                            //       crossAxisAlignment: CrossAxisAlignment.center,
                            //       children: [
                            //         CarouselSlider(
                            //           items: getCarouselImages(),
                            //           options: CarouselOptions(
                            //               // height: MediaQuery.of(context).size.height/3.2,
                            //               autoPlay: false,
                            //               enlargeCenterPage: false,
                            //               aspectRatio: 2.39,
                            //               viewportFraction: 1.0,
                            //               initialPage: 0,
                            //               onPageChanged: (index, reason) {
                            //                 setState(() {
                            //                   _current = index;
                            //                 });
                            //               }),
                            //         ),
                            //         Row(
                            //           mainAxisAlignment: MainAxisAlignment.center,
                            //           children: <Widget>[
                            //             ...List.generate(
                            //                 foodOfferCarousel.length,
                            //                 (index) => Container(
                            //                       width: 8.0,
                            //                       height: 8.0,
                            //                       margin: EdgeInsets.symmetric(
                            //                           vertical: 10.0,
                            //                           horizontal: 2.0),
                            //                       decoration: BoxDecoration(
                            //                         shape: BoxShape.circle,
                            //                         color: _current == index
                            //                             ? App24Colors
                            //                                 .redRestaurantThemeColor
                            //                             : Colors.grey[300],
                            //                       ),
                            //                     )),
                            //           ],
                            //         )
                            //       ]),
                            // ),
                            ,
                            Padding(
                              padding: EdgeInsets.symmetric(
                                horizontal: 15,
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      Icon(
                                        App24User.path_2568_3x,
                                        color: Color(0xff636363),
                                        size: MediaQuery.of(context).size.width /
                                            20,
                                      ),
                                      const SizedBox(
                                        width: 10,
                                      ),
                                      Text("Nearby Restaurants",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 19,
                                              color: App24Colors.darkTextColor)),
                                    ],
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  AllRestaurantPage()));
                                    },
                                    child: Row(children: [
                                      Text("See All",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontSize: 13,
                                              color: App24Colors.darkTextColor)),
                                      const SizedBox(
                                        width: 10,
                                      ),
                                      Image.asset(
                                        App24UserAppImages.roundRArrow,
                                        color: App24Colors.darkTextColor,
                                        width: MediaQuery.of(context).size.width /
                                            22,
                                      )
                                    ]),
                                  )
                                ],
                              ),
                            ),
                            Container(
                                height: MediaQuery.of(context).size.width / 1.6,
                                child: Consumer(builder: (context, watch, child) {
                                  final state = watch(restaurantNotifier);
                                  if (state.isLoading) {
                                    return RestaurantLoading();
                                  } else if (state.isError) {
                                    print("error");
                                      return LoadingError(
                                        onPressed: (res) {
                                          reloadData(init: true, context: res);
                                        },
                                        message: state.errorMessage,
                                      );

                                  } else {
                                    if (state.response.responseData.length == 0) {
                                      isNearbyShopsEmpty = true;
                                      return Container(
                                        margin: EdgeInsets.only(top: 80),
                                        alignment: Alignment.center,
                                        child: Center(
                                            child: Text(
                                          state.response.message,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600),
                                        )),
                                      );
                                    } else {
                                      isNearbyShopsEmpty = false;

                                      return ListView(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 10),
                                        scrollDirection: Axis.horizontal,
                                        children: getNearByRestaurantTiles(
                                            context, state.response.responseData),
                                      );
                                    }
                                  }
                                })),
                            const SizedBox(
                              height: 10,
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                horizontal: 15,
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  if(favouriteShops.isNotEmpty)
                                  Row(
                                    children: [
                                      Icon(
                                        Icons.favorite,
                                        color: Color(0xff636363),
                                        size: MediaQuery.of(context).size.width /
                                            20,
                                      ),
                                      const SizedBox(
                                        width: 10,
                                      ),

                                      Text("Favourite Restaurants",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 19,
                                              color: App24Colors.darkTextColor)),
                                    ],
                                  ),
                                  // GestureDetector(
                                  //   onTap: () {
                                  //     Navigator.push(
                                  //         context,
                                  //         MaterialPageRoute(
                                  //             builder: (context) =>
                                  //                 AllRestaurantPage()));
                                  //   },
                                  //   child: Row(children: [
                                  //     Text("See All",
                                  //         style: TextStyle(
                                  //             fontWeight: FontWeight.w600,
                                  //             fontSize: 13,
                                  //             color: App24Colors.darkTextColor)),
                                  //     const SizedBox(
                                  //       width: 10,
                                  //     ),
                                  //     Image.asset(
                                  //       App24UserAppImages.roundRArrow,
                                  //       color: App24Colors.darkTextColor,
                                  //       width: MediaQuery.of(context).size.width /
                                  //           22,
                                  //     )
                                  //   ]),
                                  // )
                                ],
                              ),
                            ),
                            if(favouriteShops.isNotEmpty)
                            Container(
                                height: MediaQuery.of(context).size.width / 1.7,
                                child: Consumer(builder: (context, watch, child) {
                                  final state = watch(restaurantNotifier);
                                  if (state.isLoading) {
                                    return RestaurantLoading();
                                  } else if (state.isError) {
                                    {
                                      return LoadingError(
                                        onPressed: (res) {
                                          reloadData(init: true, context: res);
                                        },
                                        message: state.errorMessage,
                                      );
                                    }
                                  } else {
                                    if (state.response.responseData.length == 0) {
                                      isNearbyShopsEmpty = true;
                                      return Container(
                                        margin: EdgeInsets.only(top: 80),
                                        alignment: Alignment.center,
                                        child: Center(
                                            child: Text(
                                          state.response.message,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600),
                                        )),
                                      );
                                    } else {
                                      isNearbyShopsEmpty = false;

                                      return ListView(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 10),
                                        scrollDirection: Axis.horizontal,
                                        children: getFavouriteRestaurantTiles(
                                            context, state.response.responseData),
                                      );
                                    }
                                  }
                                })),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                horizontal: 15,
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      Icon(
                                        Icons.restaurant_menu_rounded,
                                        color: Color(0xff636363),
                                        size: MediaQuery.of(context).size.width /
                                            20,
                                      ),
                                      const SizedBox(
                                        width: 5,
                                      ),
                                      Text("Cuisines",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 19,
                                              color: App24Colors.darkTextColor)),
                                    ],
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  CuisineSeeAll() /*TrackOrder()*/));
                                    },
                                    child: Row(
                                      children: [
                                        Text("See All",
                                            style: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 13,
                                                color:
                                                    App24Colors.darkTextColor)),
                                        const SizedBox(
                                          width: 10,
                                        ),
                                        Image.asset(
                                          App24UserAppImages.roundRArrow,
                                          color: App24Colors.darkTextColor,
                                          width:
                                              MediaQuery.of(context).size.width /
                                                  22,
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Hero(
                              tag: "12",
                              child: Padding(
                                  padding:
                                      const EdgeInsets.symmetric(horizontal: 0),
                                  child:
                                      Consumer(builder: (context, watch, child) {
                                    final state = watch(cuisineNotifier);
                                    if (state.isLoading) {
                                      return CuisineLoading();
                                    } else if (state.isError) {
                                      return LoadingError(
                                        onPressed: (res) {
                                          reloadData(init: true, context: res);
                                        },
                                        message: state.errorMessage,
                                      );
                                    } else {
                                      return Wrap(
                                        alignment: WrapAlignment.center,
                                        runAlignment: WrapAlignment.center,
                                        spacing: 10,
                                        runSpacing: 20,
                                        children: cuisineTiles(
                                            context, state.response.responseData),
                                      );
                                    }
                                  })),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                       /*     Padding(
                              padding: EdgeInsets.symmetric(
                                horizontal: 15,
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      Icon(Icons.access_time_rounded,
                                          color: Color(0xff636363),
                                          size:
                                              MediaQuery.of(context).size.width /
                                                  20),
                                      const SizedBox(
                                        width: 5,
                                      ),
                                      Text("Recent Orders",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 19,
                                              color: App24Colors.darkTextColor)),
                                    ],
                                  ),
                                  GestureDetector(
                                    onTap: () {},
                                    child: Row(
                                      children: [
                                        Text("See All",
                                            style: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 13,
                                                color:
                                                    App24Colors.darkTextColor)),
                                        const SizedBox(
                                          width: 10,
                                        ),
                                        Image.asset(
                                          App24UserAppImages.roundRArrow,
                                          color: App24Colors.darkTextColor,
                                          width:
                                              MediaQuery.of(context).size.width /
                                                  22,
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),*/
                       /*     Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Container(
                                padding: EdgeInsets.all(10),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: App24Colors.redRestaurantThemeColor
                                        .withOpacity(0.2),
                                  ),
                                  borderRadius: BorderRadius.circular(10),
                                  color: Color(0xffFFF4F3),
                                ),
                                child: Column(
                                  // crossAxisAlignment: CrossAxisAlignment.start,
                                  // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "Last Order",
                                              style: TextStyle(
                                                  color: App24Colors
                                                      .redRestaurantThemeColor
                                                      .withOpacity(0.7),
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: MediaQuery.of(context)
                                                          .size
                                                          .width /
                                                      27),
                                            ),
                                            Text(
                                              "Dominos Pizza",
                                              style: TextStyle(
                                                  color: App24Colors
                                                      .redRestaurantThemeColor,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: MediaQuery.of(context)
                                                          .size
                                                          .width /
                                                      24),
                                            ),
                                          ],
                                        ),
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            children: [
                                              Text(
                                                "Order No. 1199DOM23",
                                                style: TextStyle(
                                                    color: App24Colors
                                                        .redRestaurantThemeColor
                                                        .withOpacity(0.7),
                                                    fontWeight: FontWeight.w600,
                                                    fontSize:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width /
                                                            30),
                                              ),
                                              Text(
                                                "Ordered on: 19/12/2021",
                                                style: TextStyle(
                                                    color: App24Colors
                                                        .redRestaurantThemeColor
                                                        .withOpacity(0.7),
                                                    fontWeight: FontWeight.w600,
                                                    fontSize:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width /
                                                            30),
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "3xMed.Fresh Veggie",
                                                style: TextStyle(
                                                    color: App24Colors
                                                        .redRestaurantThemeColor
                                                        .withOpacity(0.7),
                                                    fontWeight: FontWeight.w600,
                                                    fontSize:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width /
                                                            30),
                                                overflow: TextOverflow.ellipsis,
                                              ),
                                              Text(
                                                "3xMed.Fresh Veggie",
                                                style: TextStyle(
                                                    color: App24Colors
                                                        .redRestaurantThemeColor
                                                        .withOpacity(0.7),
                                                    fontWeight: FontWeight.w600,
                                                    fontSize:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width /
                                                            30),
                                                overflow: TextOverflow.ellipsis,
                                              ),
                                              Text(
                                                "3xMed.Fresh Veggie"
                                                "",
                                                style: TextStyle(
                                                    color: App24Colors
                                                        .redRestaurantThemeColor
                                                        .withOpacity(0.7),
                                                    fontWeight: FontWeight.w600,
                                                    fontSize:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width /
                                                            30),
                                                overflow: TextOverflow.ellipsis,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        TextButton(
                                            style: TextButton.styleFrom(
                                                backgroundColor: App24Colors
                                                    .redRestaurantThemeColor,
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            25)),
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 15)),
                                            onPressed: () {},
                                            child: Text(
                                              "Edit Order",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: MediaQuery.of(context)
                                                          .size
                                                          .width /
                                                      28),
                                            )),
                                        const SizedBox(
                                          width: 5,
                                        ),
                                        TextButton(
                                            style: TextButton.styleFrom(
                                                backgroundColor: App24Colors
                                                    .redRestaurantThemeColor,
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            25)),
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 15)),
                                            onPressed: () {},
                                            child: Text(
                                              "Quick Order",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: MediaQuery.of(context)
                                                          .size
                                                          .width /
                                                      28),
                                            )),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ),*/
                          ],
                        ),
                      ),
                    ]),
                  )
                ])),
                ViewCartOverlay()
              ],
            ),
          ),
        ),
      ),
    );
  }
}
