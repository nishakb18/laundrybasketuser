RestaurantCuisineModel cuisinesModelFromJson(str) {
  return RestaurantCuisineModel.fromJson(str);
}

class RestaurantCuisineModel {
  String? statusCode;
  String? title;
  String? message;
  List<CuisineResponseData>? responseData;
  List<dynamic>? error;

  RestaurantCuisineModel(
      {this.statusCode,
      this.title,
      this.message,
      this.responseData,
      this.error});

  RestaurantCuisineModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    if (json['responseData'] != null) {
      responseData = [];
      json['responseData'].forEach((v) {
        responseData!.add(new CuisineResponseData.fromJson(v));
      });
    }
    if (json['error'] != null) {
      error = [];
      json['error'].forEach((v) {
        error!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['title'] = this.title;
    data['message'] = this.message;
    if (this.responseData != null) {
      data['responseData'] = this.responseData!.map((v) => v.toJson()).toList();
    }
    if (this.error != null) {
      data['error'] = this.error!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CuisineResponseData {
  int? id;
  int? storeTypeId;
  String? name;
  int? status;
  String? picture;

  CuisineResponseData(
      {this.id, this.storeTypeId, this.name, this.status, this.picture});

  CuisineResponseData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    storeTypeId = json['store_type_id'];
    name = json['name'];
    status = json['status'];
    picture = json['picture'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['store_type_id'] = this.storeTypeId;
    data['name'] = this.name;
    data['status'] = this.status;
    data['picture'] = this.picture;
    return data;
  }
}
