import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/modules/restaurant/restaurant_1st_page/Cuisines/cuisine_model.dart';
import 'package:app24_user_app/modules/restaurant/restaurant_1st_page/Cuisines/cuisine_see_all_loading.dart';
import 'package:app24_user_app/modules/restaurant/restaurant_shops/all_restaurants_page.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/widgets/common_appbar.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class CuisineSeeAll extends StatefulWidget {
  // const CuisineSeeAll({Key? key}) : super(key: key);

  @override
  _CuisineSeeAllState createState() => _CuisineSeeAllState();
}

class _CuisineSeeAllState extends State<CuisineSeeAll> {
  reloadData({required BuildContext context, required bool init}) {
    return context
        .read(cuisineNotifier.notifier)
        .getCuisines(init: init,);
  }

  List<Widget> cuisineTiles(context, List<CuisineResponseData> cuisineModel) {
    List<Widget> cuisineItems = [];
    print(cuisineModel.length);
    for (var i = 0; i < cuisineModel.length; i++) {
      cuisineItems.add(
        Column(
          children: [
            Container(
              width: MediaQuery.of(context).size.width / 5,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
              ),
              // padding: EdgeInsets.all(1),
              child: Material(
                borderRadius: BorderRadius.circular(15),
                color: Colors.transparent,
                child: InkWell(
                  borderRadius: BorderRadius.circular(15),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => AllRestaurantPage(
                            cuisineName: cuisineModel[i].name,
                          )),
                    );
                  },
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      // const SizedBox(
                      //   height: 15,
                      // ),
                      ClipRRect(
                        borderRadius: BorderRadius.circular(20),
                        child: CachedNetworkImage(
                          imageUrl: cuisineModel[i].picture ?? '',
                          fit: BoxFit.cover,
                          width: MediaQuery.of(context).size.width / 5,
                          // MediaQuery.of(context).size.width / 2 - 20,
                          height: MediaQuery.of(context).size.height / 11,
                          placeholder: (context, url) => Image.asset(
                            App24UserAppImages.placeHolderImage,
                            width: MediaQuery.of(context).size.width / 6,
                            height: MediaQuery.of(context).size.height / 12,
                            fit: BoxFit.cover,
                          ),
                          errorWidget: (context, url, error) => Image.asset(
                            App24UserAppImages.placeHolderImage,
                            width: MediaQuery.of(context).size.width / 6,
                            height: MediaQuery.of(context).size.height / 12,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 3,
                      ),
                      Text(
                        cuisineModel[i].name!,
                        style: TextStyle(
                            color: App24Colors.darkTextColor,
                            fontWeight: FontWeight.w600,
                            fontSize: MediaQuery.of(context).size.width / 29),
                        textAlign: TextAlign.center,
                        maxLines: 2,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 5,
            ),
          ],
        ),
      );
    }
    return cuisineItems;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(

        fromRestaurant: "fromRestaurant",
        title: "All Cuisines",
      ),
      body: SafeArea(
        child: RefreshIndicator(
          onRefresh: () {
            return reloadData(init: false, context: context) ?? false as Future<void>;
          },
          child: Hero(
            tag: "12",
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 25),
              child: Consumer(builder: (context, watch, child) {
                final state = watch(cuisineNotifier);
                if (state.isLoading) {
                  return CuisineSeeAllLoading();
                } else if (state.isError) {
                  return LoadingError(
                    onPressed: (res) {
                      reloadData(init: true, context: res);
                    },
                    message: state.errorMessage,
                  );
                } else {
                  return Wrap(
                    alignment: WrapAlignment.start,
                    runAlignment: WrapAlignment.center,
                    spacing: 10,
                    runSpacing: 20,
                    children:
                        cuisineTiles(context, state.response.responseData),
                  );
                }
              }),
            ),
          ),
        ),
      ),
    );
  }
}
