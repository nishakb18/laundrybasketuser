import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/api/api_response_state2.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class CuisineNotifier extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  CuisineNotifier(this._apiRepository) : super(ResponseState2(isLoading: true));

  Future<void> getCuisines({
    bool init = true,
  }) async {
    try {
      if (init) state = state.copyWith(isLoading: true);
      final cuisines = await _apiRepository.fetchCuisines();
      state =
          state.copyWith(response: cuisines, isLoading: false, isError: false);
    } catch (e) {
      state = state.copyWith(
          errorMessage: e.toString(), isLoading: false, isError: true);
    }
  }
}
