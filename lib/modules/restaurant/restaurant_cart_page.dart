// import 'package:app24_user_app/constants/asset_path.dart';
// import 'package:app24_user_app/constants/color_path.dart';
// import 'package:app24_user_app/constants/global_constants.dart';
// import 'package:app24_user_app/constants/shared_preferences_path.dart';
// import 'package:app24_user_app/helpers.dart';
// import 'package:app24_user_app/models/home_delivery_order_insert_model.dart';
// import 'package:app24_user_app/models/home_delivery_send_order_detail.dart';
// import 'package:app24_user_app/models/location_model.dart';
// import 'package:app24_user_app/modules/delivery/cart/cart_loading.dart';
// import 'package:app24_user_app/modules/delivery/cart/cart_model.dart';
// import 'package:app24_user_app/modules/delivery/shop_list/shop_list_model.dart';
// import 'package:app24_user_app/modules/home.dart';
// import 'package:app24_user_app/modules/my_account/address/manage_address_model.dart';
// import 'package:app24_user_app/modules/restaurant/restaurant_cart_page.dart';
// import 'package:app24_user_app/utils/helpers/common_helpers.dart';
// import 'package:app24_user_app/utils/helpers/common_widgets.dart';
// import 'package:app24_user_app/utils/services/api/api_providers.dart';
// import 'package:app24_user_app/utils/services/api/api_repository.dart';
// import 'package:app24_user_app/utils/services/database/address_database.dart';
// import 'package:app24_user_app/utils/services/database/database_service.dart';
// import 'package:app24_user_app/utils/services/socket/socket_service.dart';
// import 'package:app24_user_app/widgets/bottomsheets/addressConfirmation/addressConfirmation.dart';
// import 'package:app24_user_app/widgets/bottomsheets/checkout_bottomSheet/checkout_bottomsheet.dart';
// import 'package:app24_user_app/widgets/bottomsheets/make_payment_bottomsheet/make_payment_bottom_sheet.dart';
// import 'package:app24_user_app/widgets/bottomsheets/order_success_bottomSheet.dart';
// import 'package:app24_user_app/widgets/bottomsheets/promocodes/add_coupon_bottomSheet.dart';
// import 'package:app24_user_app/widgets/bottomsheets/promocodes/promocodes_model.dart';
// import 'package:app24_user_app/widgets/bottomsheets/retry_cancel_bottomSheet.dart';
// import 'package:app24_user_app/widgets/bottomsheets/schedule_bottomSheet.dart';
// import 'package:app24_user_app/widgets/common_appbar.dart';
// import 'package:app24_user_app/widgets/loading_error.dart';
// import 'package:app24_user_app/widgets/offline_builder_widget.dart';
// import 'package:app24_user_app/widgets/payment_waiting_screen.dart';
// import 'package:app24_user_app/widgets/track_order.dart';
// import 'package:cached_network_image/cached_network_image.dart';
// import 'package:dio/dio.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/gestures.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/scheduler.dart';
//
// // import 'package:flutter_neumorphic/flutter_neumorphic.dart';
// import 'package:flutter_riverpod/flutter_riverpod.dart';
// import 'package:razorpay_flutter/razorpay_flutter.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:socket_io_client/socket_io_client.dart';
//
// class RestaurantCartPage extends StatefulWidget {
//   final String? itemID;
//   final ShopNamesModelResponseData? selectedShop;
//   final String? storeTypeID;
//   final String? promoCode;
//   final String? promoCodeDetails;
//
//   const RestaurantCartPage(
//       {Key? key,
//         this.itemID,
//         this.selectedShop,
//         this.storeTypeID,
//         this.promoCode,
//         this.promoCodeDetails})
//       : super(key: key);
//
//   @override
//   _RestaurantCartPageState createState() => _RestaurantCartPageState();
// }
//
// class _RestaurantCartPageState extends State<RestaurantCartPage> {
//   ///CART PROCESS
//   ///
//   /// checkout (on click)->  checkOut()->buildCheckoutBottomSheet();
//   /// confirm & continue (on click)-> schedule delivery->callConfirmAddressBottomSheet();
//   /// buyItemsOrderPlaceFromCart
//   /// provider listener -> if order inserted
//   /// checkPaymentAndEmitSocket()-> 2 scenarios
//   /// 1. payment need to done first
//   /// call razorpay->after success emitSocket()
//   /// 2. emitSocket()
//   ///
//   /// emitSocket
//   /// 1.socket emits with order ID
//   /// 2.order detail order type set to buy
//   /// 3.navigate to order detail screen (track page)
//
//   bool use_wallet_checkout = false;
//   List<Carts>? cartTileList = [];
//   CartListModel? cartListModel;
//   late Map cartContentBody;
//   bool status = false;
//   double? amountToCheckout = 0.0;
//   var totalAmountToRazor;
//   bool handlerSuccess = false;
//   HomeDeliveryOrderDetailResponseData? abc;
//   final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
//   TextEditingController couponController = new TextEditingController();
//   bool useWallet = false;
//   bool lockWallet = false;
//   String? highPriorityAddress;
//   late int amountToCheckOut;
//   late final SocketService _socketService;
//
//   var totalAmount;
//   int? totalCart;
//   String? shopName;
//   CartListResponseData? cartListResponseData;
//   String? walletActive;
//
//   late Razorpay? _razorPay;
//
//   reloadData({required BuildContext context, bool? init}) {
//     return context.read(cartNotifierProvider.notifier).getCartList(
//         context: context,
//         init: init!,
//         lat: context
//             .read(homeNotifierProvider.notifier)
//             .currentLocation
//             .latitude!
//             .toDouble(),
//         lng: context
//             .read(homeNotifierProvider.notifier)
//             .currentLocation
//             .longitude!
//             .toDouble());
//   }
//
//   buttonDisabled() {
//     // if (cartListResponseData!.carts!.isEmpty)
//     setState(() {
//       status = false;
//     });
//   }
//
//   @override
//   void initState() {
//     _socketService = SocketService.instance;
//     getAddressWithHighestPriority();
//     SchedulerBinding.instance!.addPostFrameCallback((_) {
//       //API call after screen build
//       // context.read(addToCartNotifierProvider).addCart(context: context,itemID: widget.itemID);
//       context.read(cartNotifierProvider.notifier).getCartList(
//           context: context,
//           lat: context
//               .read(homeNotifierProvider.notifier)
//               .currentLocation
//               .latitude!
//               .toDouble(),
//           lng: context
//               .read(homeNotifierProvider.notifier)
//               .currentLocation
//               .longitude!
//               .toDouble());
//     });
//     // lockWallet = cartListResponseData!.userWalletBalance < 0 ? true : false;
//     super.initState();
//     _initRazorPay();
//   }
//
//   @override
//   void dispose() {
//     _razorPay!.clear();
//     super.dispose();
//   }
//
//   Widget buildAddonsList(List<Cartaddon> addonList) {
//     List<Widget> list = [];
//     addonList.forEach((element) {
//       list.add(Row(
//         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//         children: [
//           Expanded(
//               child: Text(
//                 element.addonName!,
//                 style: TextStyle(
//                     color: Color(0xff9B9B9B),
//                     fontWeight: FontWeight.w500,
//                     fontSize: 11),
//               )),
//           Expanded(
//             child: Text(
//               element.addonQuantity!
//                   .substring(0, element.addonQuantity!.length - 3),
//               style: TextStyle(
//                   color: Color(0xffFF533C),
//                   fontSize: 10,
//                   fontWeight: FontWeight.w500),
//             ),
//           ),
//           Expanded(
//             child: Text(
//               "AED " + (element.addonPrice! *  double.parse(element.addonQuantity.toString())).toString(),
//               style: TextStyle(
//                   color: Color(0xffFF533C),
//                   fontSize: 10,
//                   fontWeight: FontWeight.w500),
//             ),
//           )
//         ],
//       ));
//     });
//     return Column(
//       children: list,
//     );
//   }
//
//   selectLocation(String title) {
//     showSelectLocationSheet(context: context, title: title).then((value) {
//       if (value != null) {
//         LocationModel model = value;
//         setState(() {
//           context.read(homeNotifierProvider.notifier).currentLocation = model;
//           context.read(cartNotifierProvider.notifier).getCartList(
//               lat: context
//                   .read(homeNotifierProvider.notifier)
//                   .currentLocation
//                   .latitude!
//                   .toDouble(),
//               lng: context
//                   .read(homeNotifierProvider.notifier)
//                   .currentLocation
//                   .longitude!
//                   .toDouble(),
//               context: context);
//           // loadData(context: context);
//         });
//       }
//     });
//   }
//
//   _initRazorPay() {
//     _razorPay = new Razorpay();
//
//     _razorPay!.on(Razorpay.EVENT_PAYMENT_SUCCESS, handlerPaymentSuccess);
//     _razorPay!.on(Razorpay.EVENT_PAYMENT_ERROR, handlerErrorFailure);
//     _razorPay!.on(Razorpay.EVENT_EXTERNAL_WALLET, handlerExternalWallet);
//     return _razorPay;
//   }
//
// //   paymentWaiting() async{
// //     await Future.delayed(const Duration(seconds: 3), (){
// //       Navigator.pushReplacement(
// //         context,
// //         MaterialPageRoute(
// //             builder: (context) =>
// // PaymentWaiting()),
// //       );}
// //       );
// //     Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => TrackOrder(orderType: 'buy',id: context
// //         .read(orderBuyItemsNotifier.notifier)
// //         .state
// //         .response
// //         .responseData!
// //         .id.toString(),)));
// //   }
//
//   void handlerPaymentSuccess(
//       PaymentSuccessResponse
//       response /*HomeDeliveryOrderDetailResponseData responseData*/) async {
//     var prefs = await SharedPreferences.getInstance();
//     Navigator.pushReplacement(
//         context,
//         MaterialPageRoute(
//             builder: (context) => PaymentWaiting(
//               orderID: prefs.getInt(SharedPreferencesPath.lastOrderId)!,
//               orderType: "buy",
//             )));
//     // Helpers().getCommonBottomSheet(context: context, content: OrderSuccess(orderType: 'buy',orderID: prefs.getInt(SharedPreferencesPath.lastOrderId),/*orderType: "buy",orderID: responseData.id,*/));
//     // print("Payment successful");
//
// //     // paymentWaiting();
// // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => TrackOrder(orderType: context
// //     .read(orderBuyItemsNotifier.notifier)
// //     .state
// //     .response
// //     .responseData!
// //     .id,id: "buy",)));
//
//     // Navigator.push(
//     //   context,
//     //   MaterialPageRoute(
//     //       builder: (context) => PaymentWaiting(
//     //         orderID:  context
//     //             .read(orderBuyItemsNotifier.notifier)
//     //             .state
//     //             .response
//     //             .responseData!
//     //             .id,orderType: "buy",
//     //       )),
//     // );
//     // Helpers().getCommonBottomSheet(context: context, content: OrderSuccess());
//     // emitSocket(
//     //     orderID: context
//     //         .read(orderBuyItemsNotifier.notifier)
//     //         .state
//     //         .response
//     //         .responseData!
//     //         .id);
//   }
//
//   void handlerErrorFailure(
//       PaymentFailureResponse response,
//       /*HomeDeliveryOrderDetailResponseData responseData*/
//       ) {
//     // print("Payment failed" + response.message.toString());
//
//     context.read(orderDetailsNotifierProvider.notifier).checkOngoingOrders();
//     _socketService.listenSocket();
//
//     /* Helpers().getCommonBottomSheet(context: context, content: Container(
//       padding: EdgeInsets.all(15),
//       child: Column(
//         children: [
//           Center(
//             child: Text("Your Payment was failed."),
//           ),
//           MaterialButton(onPressed: (){
//
//           },
//             child: Text("Retry"),
//           )
//         ],
//       ),
//     ));
// */
//     // Navigator.pushReplacement(
//     //     context, MaterialPageRoute(builder: (context) => HomePage()));
//     // Helpers().getCommonBottomSheet(
//     //   enableDrag: false,
//     //     isDismissible: false,
//     //     context: context,
//     //     content: MakePaymentBottomSheet(
//     //       detailResponseData: responseData,
//     //     ),
//     //     title: "Retry");
//     print("Payment failed");
//   }
//
//   void handlerExternalWallet(
//       ExternalWalletResponse response,
//       /* HomeDeliveryOrderDetailResponseData responseData*/
//       ) {
//     context.read(orderDetailsNotifierProvider.notifier).checkOngoingOrders();
//     _socketService.listenSocket();
//     // Helpers().getCommonBottomSheet(
//     //   enableDrag: false,
//     //     isDismissible: false,
//     //     context: context,
//     //     content: MakePaymentBottomSheet(
//     //       detailResponseData: responseData,
//     //     ),
//     //     title: "Retry");
//     print("Payment external");
//   }
//
//   updateDetails({required CartListResponseData responseData}) {
//     totalAmount = responseData.payable;
//     totalCart = responseData.totalCart;
//   }
//
//   emitSocket({required int orderID}) async {
//     showLog("Socket emitting");
//     try {
//       Socket? socket = await _socketService.socket;
//       socket!.emit('joinPrivateRoom', {'room_1_R${orderID}_ORDER'});
//       showLog("Socket emitted");
//       //_socketService.listenSocket();
//       context.read(orderDetailsNotifierProvider.notifier).orderType = 'Buy';
//
//       print("used wallet" + use_wallet_checkout.toString());
//       // print("used wallet" + handlerSuccess.toString());
//       // Navigator.popAndPushNamed(context, "/homeScreen");
//
//       if (use_wallet_checkout == true) {
//         print("wallet used");
//         Helpers().getCommonBottomSheet(
//             context: context,
//             isDismissible: false,
//             content: OrderSuccess(
//               orderID: orderID,
//               orderType: "buy",
//             ));
//       } else {
//         // Helpers().getCommonBottomSheet(
//         //     context: context,
//         //     isDismissible: false,
//         //     content: MakePaymentBottomSheet(detailResponseData: abc!,)
//         // );
//         // Navigator.pushReplacement(
//         //       context,
//         //       MaterialPageRoute(
//         //           builder: (context) =>
//         //               TrackOrder(
//         //                 id: orderID.toString(), orderType: "buy",
//         //               )));
//         // print("testing if wallet");
//       }
//
//       // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => TrackOrder(orderType: 'buy',id: orderID.toString(),)));
//       // if(handlerSuccess == false)
//
//       // Navigator.pushReplacement(
//       //     context,
//       //     MaterialPageRoute(
//       //         builder: (context) =>
//       //             TrackOrder(
//       //               id: orderID.toString(), orderType: "buy",
//       //             )));
//
// /*      Navigator.pushReplacement(
//         context,
//         MaterialPageRoute(
//             builder: (context) => PaymentWaiting(
//               orderID: orderID,orderType: "buy",
//             )),
//       );*/
//       // Future.delayed(const Duration(seconds: 5), () {
// /*      await Future.delayed(const Duration(seconds: 5), (){
//         Navigator.pushReplacement(
//         context,
//         MaterialPageRoute(
//             builder: (context) =>
//                 TrackOrder(
//                   id: orderID.toString(), orderType: "buy",
//                 )),
//       );});*/
//
//       // } );
//
//       // Navigator.push(
//       //   context,
//       //   MaterialPageRoute(
//       //       builder: (context) =>
//       //           TrackOrder(
//       //             id: orderID.toString(),
//       //           )),
//       // );
//     } catch (e) {
//       showLog("error in socket is : ${e.toString()}");
//     }
//   }
//
//   Future<void> buildCheckoutBottomSheet() async {
//     // var result = await Helpers().getCommonBottomSheet(
//     //     context: context,
//     //     content: CheckoutBottomSheet(
//     //       cartList: cartListResponseData!,
//     //       promoCode: widget.promoCode,
//     //       promoCodeDetails: widget.promoCodeDetails,
//     //     ),
//     //     title: "Checkout");
//     // if (result != null) {
//     //   Map checkOutResult = result;
//     //   // / cartContentBody['promocode_id'] = checkOutResult['promo_code'];
//     //   cartContentBody['wallet'] = checkOutResult['use_wallet'] ? 1 : 0;
//
//     var value = await Helpers().getCommonBottomSheet(
//         context: context,
//         content: ScheduleBottomSheet(
//           checkoutType: "fromSend",
//           // orderItemBody: cartContentBody,
//         ),
//         title: "Schedule Delivery");
//     // if (value != null) {
//     if (value == 'deliver_now') {
//       callConfirmAddressBottomSheet();
//     } else {
//       Map map = value;
//       // cartContentBody['schedule_date'] = map['date'];
//       // cartContentBody['schedule_time'] = map['time'];
//       cartContentBody['delivery_date'] = map['date'];
//
//       callConfirmAddressBottomSheet(map: map);
//     }
//     // }
//     // }
//     // Helpers().getCommonBottomSheet(context: context, content: ScheduleBottomSheet());
//   }
//
//   callConfirmAddressBottomSheet({Map<dynamic, dynamic>? map}) async {
//     var addressResult = await Helpers().getCommonBottomSheet(
//         context: context,
//         content: AddressConfirmationBottomSheet(map: map),
//         title: "Delivery confirmation");
//     if (addressResult != null) {
//       print(addressResult);
//       AddressResponseData confirmedLocation = addressResult;
//
//       String? toAddress = (confirmedLocation.flatNo.toString() +
//           ", " +
//           confirmedLocation.street.toString() +
//           ", " +
//           confirmedLocation.city.toString() +
//           ", " +
//           confirmedLocation.landmark.toString());
//
//       cartContentBody["to_adrs"] = toAddress;
//       cartContentBody["to_lat"] = confirmedLocation.latitude;
//       cartContentBody["to_lng"] = confirmedLocation.longitude;
//
//       var checkout = await Helpers().getCommonBottomSheet(
//           enableDrag: true,
//           isDismissible: true,
//           context: context,
//           content: CheckoutBottomSheet(
//             toAddress: toAddress,
//             cartList: cartListResponseData!,
//             latitude: confirmedLocation.latitude,
//             longitude: confirmedLocation.longitude,
//             promoCode: widget.promoCode,
//             promoCodeDetails: widget.promoCodeDetails,
//           ));
//
//       if (checkout == "clearCart") {
//         context.read(cartNotifierProvider.notifier).getCartList(
//             context: context,
//             init: true,
//             lat: context
//                 .read(homeNotifierProvider.notifier)
//                 .currentLocation
//                 .latitude!
//                 .toDouble(),
//             lng: context
//                 .read(homeNotifierProvider.notifier)
//                 .currentLocation
//                 .longitude!
//                 .toDouble());
//       } else {
//         // print(checkout);
//         var useWallet = checkout;
//         print("checknow");
//         print(useWallet);
//
//         setState(() {
//           walletActive = useWallet["wallet"];
//           // totalAmountToRazor = useWallet["total_amount"];
//           use_wallet_checkout = walletActive == "1" ? true : false;
//           print("checknow wallet");
//           print(useWallet["wallet"]);
//           print("if wallet used :");
//           print(use_wallet_checkout);
//           // print("success payment razor"+handlerSuccess.toString());
//         });
//         if (useWallet["to_address"] != null) {
//           cartContentBody["to_adrs"] = useWallet["to_address"];
//           cartContentBody["to_lat"] = useWallet["to_lat"];
//           cartContentBody["to_lng"] = useWallet["to_lng"];
//         }
//         cartContentBody["wallet"] = useWallet["wallet"];
//
//         context
//             .read(orderBuyItemsNotifier.notifier)
//             .buyItemsOrderPlaceFromCart(body: cartContentBody);
// /*
//         if (cartContentBody['wallet'] == '1') {
//           //if wallet ? update the user wallet amount
//           context.read(homeNotifierProvider.notifier).getProfile();
//         }
// */
//
//         // await context.read(cartNotifierProvider.notifier).getCartList();
//         // }
//
//       }
//     }
//   }
//
//   // payWithWallet() {
//   //   Map body = {
//   //     'wallet_amount': usableWalletAmount,
//   //     'id': widget.detailResponseData.id
//   //   };
//   //
//   //   context
//   //       .read(payWithWalletNotifierProvider.notifier)
//   //       .payWithWallet(body: body);
//   // }
//
//   checkOut() async {
//     var prefs = await SharedPreferences.getInstance();
//     prefs.setString(SharedPreferencesPath.orderType, "buy");
//     // LocationModel userLocation =
//     //     context.read(homeNotifierProvider.notifier).currentLocation;
//
//     cartContentBody = {
//       //"promocode_id": "",
//       // "wallet" :  "1",
//       "payment_mode": "RAZORPAY",
//       //"payment_id" : razorID.value.toString())
//       //"card_id": "",
//       "user_address_id": '0',
//       //"to_adrs" :  userLocation.description,
//       //"to_lat" :  HomeLatLng!!.latitude)
//       //"to_lng" :  HomeLatLng!!.longitude)
//       //"delivery_date" :  Delivery_Date!!)
//       "order_type": "DELIVERY"
//     };
//     // context
//     //     .read(orderBuyItemsNotifier.notifier)
//     //     .buyItemsOrderPlaceFromCart(
//     //     body: cartContentBody, context: context);
//     buildCheckoutBottomSheet();
//
//     // context
//     //     .read(orderBuyItemsNotifier.notifier)
//     //     .buyItemsOrderPlaceFromCart(body: cartContentBody, context: context);
//   }
//
//   onClearCart() async {
//     await context
//         .read(cartNotifierProvider.notifier)
//         .getClearCart(context: context);
//     context.read(cartNotifierProvider.notifier).getCartList(
//         context: context,
//         init: true,
//         lat: context
//             .read(homeNotifierProvider.notifier)
//             .currentLocation
//             .latitude!
//             .toDouble(),
//         lng: context
//             .read(homeNotifierProvider.notifier)
//             .currentLocation
//             .longitude!
//             .toDouble());
//     int cartCount =
//         context.read(cartCountNotifierProvider.notifier).state.response;
//
//     context
//         .read(cartCountNotifierProvider.notifier)
//         .updateCartCount(count: cartCount = 0);
//   }
//
//   onTapRemove(index) async {
//     Map body = {"cart_id": cartTileList![index].id};
//
//     setState(() {
//       if (context.read(cartNotifierProvider.notifier).cartIdList != null) {
//         int b = context
//             .read(cartNotifierProvider.notifier)
//             .cartIdList!
//             .removeAt(index);
//         print("deleted ?" + b.toString());
//
//         for (int i = 0;
//         i < context.read(cartNotifierProvider.notifier).cartIdList!.length;
//         i++) {
//           print("products id: " +
//               context
//                   .read(cartNotifierProvider.notifier)
//                   .cartIdList![i]
//                   .toString());
//         }
//       }
//     });
//     await context
//         .read(cartNotifierProvider.notifier)
//         .removeItemCart(body: body, context: context);
//     context.read(cartNotifierProvider.notifier).getCartList(
//         context: context,
//         init: false,
//         lat: context
//             .read(homeNotifierProvider.notifier)
//             .currentLocation
//             .latitude!
//             .toDouble(),
//         lng: context
//             .read(homeNotifierProvider.notifier)
//             .currentLocation
//             .longitude!
//             .toDouble());
//     int cartCount =
//         context.read(cartCountNotifierProvider.notifier).state.response;
//
//     context.read(cartCountNotifierProvider.notifier).updateCartCount(
//         count: cartCount - 1, storeID: cartTileList![index].storeId!);
//   }
//
//   // List<Product> efg = [];
//   buildCartItems(int index) {
//     // List<Product> cart = [];
//     var total = cartTileList![index].product!.itemPrice! *
//         cartTileList![index].quantity!;
//
//     return Container(
//       height: MediaQuery.of(context).size.height / 7,
//       margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),
//       padding: EdgeInsets.all(10),
//       decoration: BoxDecoration(
//           borderRadius: BorderRadius.circular(25),
//           color: Colors.white,
//           border: Border.all(color: Colors.grey[200]!),
//           boxShadow: [
//             BoxShadow(
//               offset: const Offset(0.0, 1.0),
//               color: Colors.grey[200]!,
//               blurRadius: 4.0,
//               spreadRadius: 4.0,
//             )
//           ]),
//       child: Row(
//         // crossAxisAlignment: CrossAxisAlignment.stretch,
//         // mainAxisAlignment: MainAxisAlignment.spaceBetween,
//         children: [
//           ClipRRect(
//             borderRadius: BorderRadius.circular(20),
//             child: CachedNetworkImage(
//               imageUrl: cartTileList![index].product!.picture ??
//                   App24UserAppImages.placeHolderImage,
//               width: MediaQuery.of(context).size.width / 3.5,
//               height: MediaQuery.of(context).size.height / 7,
//               fit: BoxFit.fill,
//               placeholder: (context, url) => Image.asset(
//                 App24UserAppImages.placeHolderImage,
//                 width: MediaQuery.of(context).size.width / 4.5,
//               ),
//               errorWidget: (context, url, error) => Image.asset(
//                 App24UserAppImages.placeHolderImage,
//                 width: MediaQuery.of(context).size.width / 4.5,
//               ),
//             ),
//           ),
//           const SizedBox(
//             width: 10,
//           ),
//           Expanded(
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               mainAxisAlignment: MainAxisAlignment.center,
//               children: [
//                 Row(
//                   children: [
//                     Expanded(
//                       child: Tooltip(
//                         message: cartTileList![index].product!.itemName!+ " asdadsasfafaf",
//
//                         child: Text(
//                           cartTileList![index].product!.itemName! + " asdadsasfafaf",
//                           style: TextStyle(
//                               fontSize: MediaQuery.of(context).size.width / 27,
//                               fontWeight: FontWeight.bold),
//                           overflow: TextOverflow.ellipsis,
//                         ),
//                       ),
//                     ),
//                     if (cartListResponseData!.showEstimation == 1)
//                       Text(
//                         "AED " + total.toString(),
//                         style: TextStyle(
//                             fontSize: 13,
//                             color: App24Colors.greenOrderEssentialThemeColor,
//                             fontWeight: FontWeight.bold),
//                         overflow: TextOverflow.clip,
//                       )
//                   ],
//                 ),
//
//                 if (cartTileList![index].cartaddon!.isNotEmpty)
//                   if (cartListResponseData!.showEstimation == 1)
//                     Row(mainAxisAlignment: MainAxisAlignment.end, children: [
//                       if (cartTileList![index].cartaddon!.isNotEmpty)
//                         Expanded(
//                             child: buildAddonsList(cartTileList![index].cartaddon!)),
//                     ]),
//
//                 const SizedBox(
//                   height: 10,
//                 ),
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: [
//                     Container(
//                       decoration: BoxDecoration(
//                           color: Color(0xffFFECE9),
//                           borderRadius: BorderRadius.circular(15)),
//                       child: Row(
//                         // mainAxisAlignment: MainAxisAlignment.center,
//                         mainAxisAlignment: MainAxisAlignment.spaceAround,
//                         children: [
//                           Material(
//                             borderRadius: BorderRadius.all(Radius.circular(50)),
//                             color: Colors.transparent,
//                             // shadowColor: Colors.grey[400],
//                             // elevation: 3,
//                             child: InkWell(
//                               splashColor: Colors.red,
//                               borderRadius:
//                               BorderRadius.all(Radius.circular(50)),
//                               onTap: () {
//                                 updateCart(
//                                     cartItem: cartTileList![index],
//                                     method: "remove",
//                                     index: index);
//                               },
//                               child: Padding(
//                                 padding: EdgeInsets.symmetric(
//                                     horizontal:
//                                     MediaQuery.of(context).size.width / 45,
//                                     vertical: MediaQuery.of(context)
//                                         .padding
//                                         .vertical /
//                                         30),
//                                 child: Text(
//                                   "-",
//                                   style: TextStyle(
//                                       fontWeight: FontWeight.bold,
//                                       fontSize:
//                                       MediaQuery.of(context).size.width /
//                                           18,
//                                       color: Colors.red),
//                                 ),
//                               ),
//                             ),
//                           ),
//                           Padding(
//                             padding: const EdgeInsets.symmetric(
//                                 vertical: 5, horizontal: 10),
//                             child: Center(
//                               child: Container(
//                                 // width: 15,
//                                 alignment: Alignment.center,
//                                 child: Text(
//                                   cartTileList![index].quantity.toString(),
//                                   style: TextStyle(
//                                       fontWeight: FontWeight.w700,
//                                       fontSize: 12),
//                                 ),
//                               ),
//                             ),
//                           ),
//                           Material(
//                             // borderRadius: BorderRadius.all(Radius.circular(50)),
//
//                             color: Colors.transparent,
//                             child: InkWell(
//                               splashColor: Colors.green,
//                               borderRadius:
//                               BorderRadius.all(Radius.circular(50)),
//                               onTap: () {
//                                 updateCart(
//                                     cartItem: cartTileList![index],
//                                     method: "add");
//                               },
//                               child: Padding(
//                                 padding: EdgeInsets.symmetric(
//                                     horizontal:
//                                     MediaQuery.of(context).size.width / 55,
//                                     vertical: MediaQuery.of(context)
//                                         .padding
//                                         .vertical /
//                                         30),
//                                 child: Text(
//                                   "+",
//                                   style: TextStyle(
//                                       fontWeight: FontWeight.bold,
//                                       fontSize:
//                                       MediaQuery.of(context).size.width /
//                                           18,
//                                       color: App24Colors
//                                           .greenOrderEssentialThemeColor),
//                                 ),
//                               ),
//                             ),
//                           ),
//                         ],
//                       ),
//                     ),
//                     // const SizedBox(
//                     //   width: 10,
//                     // ),
//                     Material(
//                       color: Colors.white,
//                       child: InkWell(
//                         borderRadius: BorderRadius.circular(10),
//                         onTap: () {
//                           // print(cartTileList!.length);
//                           onTapRemove(index);
//                           // cartTileList!.removeAt(index);
//                         },
//                         child: Padding(
//                           padding: const EdgeInsets.all(8.0),
//                           child: Row(
//                             children: [
//                               Icon(
//                                 Icons.delete,
//                                 color: Color(0xff995D5D),
//                                 size: MediaQuery.of(context).size.width / 23,
//                               ),
//                               Text(
//                                 "Remove",
//                                 style: TextStyle(
//                                     fontSize:
//                                     MediaQuery.of(context).size.width / 31,
//                                     fontWeight: FontWeight.bold,
//                                     color: Color(0xff995D5D)),
//                               )
//                             ],
//                           ),
//                         ),
//                       ),
//                     )
//                   ],
//                 )
//               ],
//             ),
//           ),
//         ],
//       ),
//     );
//   }
//
//   int? oldQty;
//   int? qty;
//
//   updateCart({required Carts cartItem, String? method, index}) async {
//     int? qty = cartItem.quantity;
//     if (method == 'remove') {
//       if (qty != 0) qty = qty! - 1;
//       //showMessage(context, "item(s) removed", false, true);
//       if (qty == 0) onTapRemove(index);
//     } else {
//       oldQty = qty;
//       qty = qty! + 1;
//       // print(oldQty);
//
//       // if(qty != qty + 1)
//       //   CircularProgressIndicator();
//       //showMessage(context, "item added", false, true);
//     }
//     // print(qty);
//
//     Map body = {
//       "qty": qty,
//       // "repeat" : product.,
//       "item_id": cartItem.product!.id,
//       "to_lat":
//       context.read(homeNotifierProvider.notifier).currentLocation.latitude,
//       "to_lng":
//       context.read(homeNotifierProvider.notifier).currentLocation.longitude,
//       "customize" : 1,
//       // "repeat" :0
//     };
//     if (qty != 0) {
//       check() {
//         if (oldQty != qty) {
//           print(oldQty != qty);
//           showDialog(
//               context: context,
//               builder: (_) => Container(
//                   alignment: Alignment.topCenter,
//                   margin: EdgeInsets.only(top: 20),
//                   child: CircularProgressIndicator(
//                     backgroundColor: Colors.grey,
//                     color: Colors.purple,
//                   )));
//         } else {
//           return null;
//         }
//       }
//       // check();
//
//       await context.read(cartNotifierProvider.notifier).addCart(
//           itemQty: qty as int,
//           itemAddons: '',
//           context: context,
//           itemID: cartItem.product!.id.toString(),
//           bodys: body,
//           init: true);
//
//       setState(() {
//         oldQty = qty;
//         print("old quantity" + oldQty.toString());
//       });
//
//       // print(oldQty);
//       // print(qty);
//     }
//     // print(oldQty);
//     // print(qty);
//     // showMessage(context, "msg", true);
//
//     // await context.read(cartCountNotifierProvider.notifier).updateCartCount(
//     //    cartStoreId: cartItem.product.id);
//
//     //  reloadData(context: context, init: false);
//   }
//
//   checkPaymentAndEmitSocket(
//       {required HomeDeliveryOrderInsertModel responseData}) async {
//     var prefs = await SharedPreferences.getInstance();
//     prefs.setString(SharedPreferencesPath.lastOrderType, "buy");
//     prefs.setInt(
//         SharedPreferencesPath.lastOrderId, responseData.responseData!.id);
//
//     if (cartListResponseData!.showEstimation == 1 &&
//         responseData.responseData!.totalPayable != '0.00') {
//       // print("razorpay");
//       Map checkOutOptions = {
//         'description': 'homedelivery${responseData.responseData!.id}',
//         'amount':
//         double.parse(responseData.responseData!.totalPayable.toString()),
//         // double.parse(responseData.responseData!.totalPayable!),
//         "notes": {
//           "store_invoice_id": responseData.responseData!.storeOrderInvoiceId,
//           "type": "homedelivery",
//           "Store_order_id": responseData.responseData!.id
//         }
//       };
//       try {
//         _razorPay!.open(await getRazorPayOptions(
//             context: context, values: checkOutOptions));
//         emitSocket(orderID: responseData.responseData!.id!);
//       } catch (e) {
//         showLog("emit socket error" + e.toString());
//       }
//     } else {
//       emitSocket(orderID: responseData.responseData!.id!);
//     }
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: CustomAppBar(
//         hideCartIcon: "true",
//         title: "Cart",
//       ),
//       body: OfflineBuilderWidget(
//         SafeArea(
//           child: Container(
//             child: RefreshIndicator(
//               onRefresh: () {
//                 return reloadData(init: false, context: context) ??
//                     false as Future<void>;
//               },
//               child: ProviderListener(
//                 provider: orderBuyItemsNotifier,
//                 onChange: (context, dynamic state) {
//                   if (state.isLoading!) {
//                     showProgress(context);
//                   } else if (state.isError!) {
//                     Navigator.pop(context);
//                     showMessage(context, state.errorMessage!, true, true);
//                   } else {
//                     print("OrderPLaced and socket emiting");
//                     Navigator.pop(context);
//                     checkPaymentAndEmitSocket(responseData: state.response);
//
//                     // emitSocket(
//                     //     orderID: context
//                     //         .read(orderBuyItemsNotifier.notifier)
//                     //         .state
//                     //         .response
//                     //         .responseData!
//                     //         .id);
//                     context.read(cartNotifierProvider.notifier).getCartList(
//                         context: context,
//                         lat: context
//                             .read(homeNotifierProvider.notifier)
//                             .currentLocation
//                             .latitude!
//                             .toDouble(),
//                         lng: context
//                             .read(homeNotifierProvider.notifier)
//                             .currentLocation
//                             .longitude!
//                             .toDouble());
//                     // if(checkPaymentAndEmitSocket(responseData: state.response))
//                     //   print("OrderPLaced and socket emiting");
//                   }
//                 },
//                 child: Padding(
//                   padding: const EdgeInsets.symmetric(horizontal: 20),
//                   child: Consumer(builder: (context, watch, child) {
//                     final state = watch(cartNotifierProvider);
//                     if (state.isLoading) {
//                       return CartListLoading();
//                     } else if (state.isError) {
//                       return LoadingError(
//                         onPressed: (res) {
//                           reloadData(init: true, context: res);
//                         },
//                         message: state.errorMessage.toString(),
//                       );
//                     } else if (state.response != 0) {
//                       cartListResponseData = state.response.responseData;
//                       cartTileList = cartListResponseData!.carts;
//                       updateDetails(responseData: cartListResponseData!);
//
//                       if(cartTileList!.isEmpty)
//                         return Center(
//                           child: Column(
//                             crossAxisAlignment:
//                             CrossAxisAlignment.center,
//                             mainAxisAlignment:
//                             MainAxisAlignment.center,
//                             children: [
//                               Icon(
//                                 CupertinoIcons.cart_badge_minus,
//                                 size: 150,
//                                 color: Colors.grey,
//                               ),
//                               const SizedBox(
//                                 height: 20,
//                               ),
//                               Text("Your cart is empty.")
//                             ],
//                           ),
//                         );
//
//
//                       return Column(
//                         children: [
//                           Expanded(
//                             child: SingleChildScrollView(
//                               child: Column(
//                                 crossAxisAlignment: CrossAxisAlignment.start,
//                                 children: [
//                                   Padding(
//                                     padding: const EdgeInsets.only(top: 10),
//                                     child: Text(
//                                       "There are $totalCart item(s) in your cart",
//                                       style: TextStyle(
//                                           color: App24Colors.darkTextColor,
//                                           fontWeight: FontWeight.bold,
//                                           fontSize: MediaQuery.of(context)
//                                               .size
//                                               .width /
//                                               25),
//                                     ),
//                                   ),
//                                   const SizedBox(
//                                     height: 5,
//                                   ),
//                                   // Row(
//                                   //   mainAxisAlignment:
//                                   //       MainAxisAlignment.spaceBetween,
//                                   //   children: [
//                                   //     if (cartListResponseData!
//                                   //             .showEstimation ==
//                                   //         1)
//                                   //       Text("Total Amount",
//                                   //           style: TextStyle(
//                                   //               fontSize: 20,
//                                   //               fontWeight: FontWeight.bold)),
//                                   //     if (cartListResponseData!
//                                   //             .showEstimation ==
//                                   //         1)
//                                   //       Text(
//                                   //           // carts?.totalPrice.toString() ?? "",
//                                   //           "AED$totalAmount /-",
//                                   //           style: TextStyle(
//                                   //               fontSize: 20,
//                                   //               color: Color(0xff55708C),
//                                   //               fontWeight: FontWeight.bold)),
//                                   //   ],
//                                   // ),
//                                  ListView.builder(
//                                     itemBuilder: (context, index) =>
//                                         buildCartItems(index),
//                                     padding: EdgeInsets.only(
//                                       top: 5,
//                                     ),
//                                     itemCount: cartTileList!.length,
//                                     shrinkWrap: true,
//                                     physics:
//                                     NeverScrollableScrollPhysics(),
//                                   ),
//                                   const SizedBox(
//                                     height: 10,
//                                   ),
//
//                                   Center(
//                                     child: Text(
//                                       "Item(s) will be delivered to:",
//                                       style: TextStyle(
//                                           color: App24Colors.darkTextColor,
//                                           fontWeight: FontWeight.bold,
//                                           fontSize: MediaQuery.of(context)
//                                               .size
//                                               .width /
//                                               25),
//                                     ),
//                                   ),
//                                   const SizedBox(
//                                     height: 10,
//                                   ),
//                                   Row(
//                                     children: [
//                                       Expanded(
//                                           child: Text(
//                                     highPriorityAddress.toString())),
//                                       ElevatedButton(
//                                         style: ElevatedButton.styleFrom(
//                                             primary: App24Colors
//                                                 .redRestaurantThemeColor,
//                                             shape: RoundedRectangleBorder(
//                                                 borderRadius:
//                                                 BorderRadius.circular(20))),
//                                         onPressed: () {
//
//
//                                         },
//                                         child: Text(
//                                           "Add",
//                                           style: TextStyle(color: Colors.white,fontSize: MediaQuery.of(context).size.width/30,fontWeight: FontWeight.w600),
//                                         ),
//                                         // color:
//                                         //     App24Colors.redRestaurantThemeColor,
//                                         // shape: RoundedRectangleBorder(
//                                         //     borderRadius:
//                                         //         BorderRadius.circular(20)),
//                                       ),
//                                       const SizedBox(
//                                         width: 10,
//                                       ),
//                                       if(!highPriorityAddress.toString().contains("No address added!"))
//                                       ElevatedButton(
//                                         onPressed: () {},
//                                         style: ElevatedButton.styleFrom(
//                                             primary: App24Colors
//                                                 .redRestaurantThemeColor,
//                                             shape: RoundedRectangleBorder(
//                                                 borderRadius:
//                                                 BorderRadius.circular(20))),
//                                         child: Text(
//                                           "Change",
//                                           style: TextStyle(color: Colors.white,fontSize: MediaQuery.of(context).size.width/30,fontWeight: FontWeight.w600),
//                                         ),
//                                         // color:
//                                         //     App24Colors.redRestaurantThemeColor,
//                                         // shape: RoundedRectangleBorder(
//                                         //     borderRadius:
//                                         //         BorderRadius.circular(20)),
//                                       )
//                                     ],
//                                   ),
//                                   Divider(),
//                                   if (cartListResponseData!.showEstimation == 1)
//                                     Helpers().getDetailsRow(
//                                         "Item Total",
//                                         "AED" +
//                                             double.parse(cartListResponseData!
//                                                 .totalItemPrice
//                                                 .toString())
//                                                 .toStringAsFixed(2),
//                                         context),
//                                   if (cartListResponseData!.showEstimation == 1)
//                                     const SizedBox(height: 5),
//                                   Helpers().getDetailsRow(
//                                       "Delivery charge",
//                                       "AED" +
//                                           double.parse(cartListResponseData!
//                                               .deliveryCharges
//                                               .toString())
//                                               .toStringAsFixed(2),
//                                       context),
//                                   const SizedBox(
//                                     height: 10,
//                                   ),
//                                   if (cartListResponseData!.showEstimation == 1)
//                                     Form(
//                                       key: _formKey,
//                                       child: TextFormField(
//                                         controller: couponController,
//                                         validator: (String? arg) {
//                                           if (arg!.length > 0)
//                                             return 'Invalid Coupon Code.';
//                                           else if (arg.length == 0)
//                                             return "Please enter coupon code";
//                                         },
//                                         // maxLines: 1,
//                                         decoration: InputDecoration(
//                                           suffixIcon: MaterialButton(
//                                             onPressed: () {
//                                               _formKey.currentState!.validate();
//                                             },
//                                             shape: RoundedRectangleBorder(
//                                                 borderRadius: BorderRadius.only(
//                                                     topRight:
//                                                     Radius.circular(10),
//                                                     bottomRight:
//                                                     Radius.circular(10))),
//                                             child: Text("Apply"),
//                                           ),
//                                           contentPadding: EdgeInsets.only(
//                                             /*bottom: 20,*/
//                                               left: 20),
//                                           hintText: "Enter coupon code here",
//                                           hintStyle: TextStyle(
//                                             fontSize: 13,
//                                           ),
//                                           focusedErrorBorder: OutlineInputBorder(
//                                               borderRadius:
//                                               BorderRadius.circular(10),
//                                               borderSide: BorderSide(
//                                                   color: App24Colors
//                                                       .redRestaurantThemeColor
//                                                       .withOpacity(0.2))),
//                                           errorBorder: OutlineInputBorder(
//                                               borderRadius:
//                                               BorderRadius.circular(10),
//                                               borderSide: BorderSide(
//                                                   color: App24Colors
//                                                       .redRestaurantThemeColor
//                                                       .withOpacity(0.2))),
//                                           enabledBorder: OutlineInputBorder(
//                                               borderRadius:
//                                               BorderRadius.circular(10),
//                                               borderSide: BorderSide(
//                                                   color: App24Colors
//                                                       .greenOrderEssentialThemeColor
//                                                       .withOpacity(0.2))),
//                                           focusedBorder: OutlineInputBorder(
//                                               borderRadius:
//                                               BorderRadius.circular(10),
//                                               borderSide: BorderSide(
//                                                   color: App24Colors
//                                                       .greenOrderEssentialThemeColor
//                                                       .withOpacity(0.2))),
//                                         ),
//                                       ),
//                                     ),
//                                   if (cartListResponseData!.showEstimation == 1)
//                                     Padding(
//                                       padding: const EdgeInsets.fromLTRB(
//                                           0, 10, 10, 0),
//                                       child: Row(
//                                         mainAxisAlignment:
//                                         MainAxisAlignment.end,
//                                         children: [
//                                           RichText(
//                                               text: TextSpan(
//                                                   text: "See all coupons",
//                                                   style: TextStyle(
//                                                     color: Colors.blue,
//                                                     fontSize: 11,
//                                                   ),
//                                                   recognizer:
//                                                   TapGestureRecognizer()
//                                                     ..onTap = () async {
//                                                       print(
//                                                           widget.promoCode);
//                                                       var selectedCoupon =
//                                                       await Helpers()
//                                                           .getCommonBottomSheet(
//                                                           context:
//                                                           context,
//                                                           content:
//                                                           AddCouponBottomSheet(
//                                                             cartShopID:
//                                                             cartListResponseData!.storeId,
//                                                             promoCode:
//                                                             widget.promoCode,
//                                                             promoCodeDetails:
//                                                             widget.promoCodeDetails,
//                                                           ),
//                                                           title:
//                                                           "Coupon Codes");
//                                                       if (selectedCoupon !=
//                                                           null) {
//                                                         PromoCodesResponseData
//                                                         selectedPromoCode =
//                                                             selectedCoupon;
//                                                         setState(() {
//                                                           couponController
//                                                               .text =
//                                                           selectedPromoCode
//                                                               .promoCode!;
//                                                         });
//                                                       }
//                                                     })),
//                                         ],
//                                       ),
//                                     ),
//                                   if (cartListResponseData!.showEstimation == 1)
//                                     if (cartListResponseData!
//                                         .promocode!.isNotEmpty)
//                                       Row(
//                                         children: <Widget>[
//                                           /* Text(
//                               "Promocode applied",
//                               style: TextStyle(
//                                   fontWeight: FontWeight.w600,
//                                   fontSize: 14),
//                             ),*/
//
//                                           Expanded(
//                                             child: Text(
//                                               "\"" +
//                                                   cartListResponseData!
//                                                       .promocode
//                                                       .toString()
//                                                       .toUpperCase() +
//                                                   "\" offer applied on this order",
//                                               textAlign: TextAlign.end,
//                                               style: TextStyle(
//                                                   fontWeight: FontWeight.w600,
//                                                   color: App24Colors.lightBlue,
//                                                   fontSize: 14),
//                                             ),
//                                           ),
//                                         ],
//                                       ),
//                                   if (cartListResponseData!.showEstimation == 1)
//                                     if (cartListResponseData!
//                                         .promocode!.isNotEmpty)
//                                       Padding(
//                                         padding: EdgeInsets.only(top: 5),
//                                         child: Row(
//                                           children: <Widget>[
//                                             /* Text(
//                               "Promocode applied",
//                               style: TextStyle(
//                                   fontWeight: FontWeight.w600,
//                                   fontSize: 14),
//                             ),*/
//
//                                             Expanded(
//                                               child: Text(
//                                                 "AED" +
//                                                     cartListResponseData!
//                                                         .promocodeAmount
//                                                         .toString() +
//                                                     " discounted on your bill",
//                                                 textAlign: TextAlign.end,
//                                                 style: TextStyle(
//                                                     fontWeight: FontWeight.w600,
//                                                     color:
//                                                     App24Colors.lightBlue,
//                                                     fontSize: 14),
//                                               ),
//                                             ),
//                                           ],
//                                         ),
//                                       ),
//                                   const SizedBox(
//                                     height: 10,
//                                   ),
//                                   if (cartListResponseData!.showEstimation !=
//                                       1 &&
//                                       cartListResponseData!.carts!.isNotEmpty)
//                                     Container(
//                                       padding: EdgeInsets.all(20),
//                                       decoration: BoxDecoration(
//                                         borderRadius: BorderRadius.circular(20),
//                                         color: Color(0xffFFFABD),
//                                       ),
//                                       child: Text(
//                                         "Prices will be updated once the purchase is completed at the shop. You shall be alerted for the payment when the bill is ready.",
//                                         textAlign: TextAlign.center,
//                                         style: TextStyle(
//                                             color: Color(0xffB7791B),
//                                             fontWeight: FontWeight.w800),
//                                       ),
//                                     ),
//                                   const SizedBox(
//                                     height: 10,
//                                   ),
//                                 ],
//                               ),
//                             ),
//                           ),
//                           Container(
//                             decoration: BoxDecoration(
//                                 border: Border(
//                                     top: BorderSide(color: Colors.grey[300]!))),
//                             padding:
//                             const EdgeInsets.only(top: 10.0, bottom: 10.0),
//                             child: Column(
//                               crossAxisAlignment: CrossAxisAlignment.stretch,
//                               // mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                               children: [
//                                 cartListResponseData!.showEstimation != 1
//                                     ? Spacer()
//                                     : Column(
//                                   // mainAxisSize: MainAxisSize.min,
//                                   // crossAxisAlignment:
//                                   // CrossAxisAlignment.start,
//                                   children: [
//                                     Row(
//                                       mainAxisAlignment:
//                                       MainAxisAlignment.spaceBetween,
//                                       children: [
//                                         Text(
//                                           "Total Amount",
//                                           style: TextStyle(
//                                               fontSize:
//                                               MediaQuery.of(context)
//                                                   .size
//                                                   .width /
//                                                   18,
//                                               fontWeight:
//                                               FontWeight.bold),
//                                         ),
//                                         Text(
//                                           "AED$totalAmount /-",
//                                           style: TextStyle(
//                                               fontSize:
//                                               MediaQuery.of(context)
//                                                   .size
//                                                   .width /
//                                                   18,
//                                               fontWeight: FontWeight.bold,
//                                               color: App24Colors
//                                                   .darkTextColor),
//                                         ),
//                                       ],
//                                     ),
//                                     if (cartListResponseData!
//                                         .showEstimation ==
//                                         1 &&
//                                         cartListResponseData!
//                                             .userWalletBalance !=
//                                             0)
//                                       Row(
//                                         mainAxisAlignment:
//                                         MainAxisAlignment.center,
//                                         // mainAxisAlignment: MainAxisAlignment.spaceBetween,              // crossAxisAlignment: CrossAxisAlignment.stretch,
//                                         // mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                                         children: [
//                                           Transform.scale(
//                                               scale: 1.1,
//                                               child: Checkbox(
//                                                 shape:
//                                                 RoundedRectangleBorder(
//                                                     borderRadius:
//                                                     BorderRadius
//                                                         .circular(
//                                                         15)),
//                                                 checkColor: Colors.white,
//                                                 activeColor: App24Colors
//                                                     .greenOrderEssentialThemeColor,
//                                                 value: lockWallet
//                                                     ? true
//                                                     : useWallet,
//                                                 onChanged:
//                                                 /*lockWallet
//                                       ? null
//                                       :*/
//                                                     (bool? value) {
//                                                   setState(() {
//                                                     // if(widget.cartList.userWalletBalance < 0){
//                                                     //   useWallet = true;
//                                                     // }else {
//                                                     useWallet = value!;
//                                                     // }
//                                                     // print(getTotalAmount());
//                                                   });
//                                                 },
//                                               )),
//
//                                           // Flexible(
//                                           //   child: Row(
//                                           //     children: [
//                                           //       Text("Use",
//                                           //           style: TextStyle(
//                                           //               fontSize: 15,
//                                           //               fontWeight: FontWeight.bold)),
//                                           //       const SizedBox(
//                                           //         width: 5,
//                                           //       ),
//                                           //       useWallet == false
//                                           //           ? Text("(AED0)",
//                                           //           style: TextStyle(
//                                           //               fontSize: 18,
//                                           //               fontWeight: FontWeight.bold,
//                                           //               color: App24Colors
//                                           //                   .redRestaurantThemeColor),
//                                           //       overflow: TextOverflow.clip,)
//                                           //           : Expanded(
//                                           //             child: Text(
//                                           //             " (AED " +
//                                           //                 (widget.cartList.totalPrice! +
//                                           //                     widget
//                                           //                         .cartList.shopGstAmount)
//                                           //                     .toString() +
//                                           //                 ")",
//                                           //             style: TextStyle(
//                                           //                 fontSize: 18,
//                                           //                 fontWeight: FontWeight.bold,
//                                           //                 color: App24Colors
//                                           //                     .redRestaurantThemeColor),
//                                           //       overflow: TextOverflow.clip,),
//                                           //           ),
//                                           //     ],
//                                           //   ),
//                                           // ),
//                                           // const SizedBox(
//                                           //   width: 15,
//                                           // ),
//
//                                           // Checkbox(
//                                           //   checkColor: Colors.greenAccent,
//                                           //   activeColor: Colors.red,
//                                           //   value: this.useWallet,
//                                           //   onChanged: (bool? value) {
//                                           //     setState(() {
//                                           //       this.useWallet = value;
//                                           //     });
//                                           //   },
//                                           // ),
//                                           Row(
//                                             children: [
//                                               Text(
//                                                 cartListResponseData!
//                                                     .userWalletBalance <
//                                                     0
//                                                     ? "Negative Wallet"
//                                                     : "Wallet Amount",
//                                                 style: TextStyle(
//                                                     fontSize: 15,
//                                                     fontWeight:
//                                                     FontWeight.bold),
//                                               ),
//                                               const SizedBox(
//                                                 width: 5,
//                                               ),
//                                               Text(
//                                                 "(AED${cartListResponseData!.userWalletBalance.toString()})",
//                                                 // useWallet == false
//                                                 //     ? "(AED" +
//                                                 //     widget.cartList.userWalletBalance
//                                                 //         .toString() +
//                                                 //     ")"
//                                                 //     : ("(AED" +
//                                                 //     (widget.cartList
//                                                 //         .userWalletBalance! -
//                                                 //         widget
//                                                 //             .cartList.totalPrice!)
//                                                 //         .toString()) +
//                                                 //     ")",
//                                                 style: TextStyle(
//                                                     fontSize: 18,
//                                                     fontWeight:
//                                                     FontWeight.bold,
//                                                     color: App24Colors
//                                                         .greenOrderEssentialThemeColor),
//                                                 overflow:
//                                                 TextOverflow.clip,
//                                               ),
//                                             ],
//                                           )
//                                         ],
//                                       ),
//                                   ],
//                                 ),
//                                 ElevatedButton(
//                                   style: ElevatedButton.styleFrom(
//                                       padding:
//                                       EdgeInsets.symmetric(vertical: 15),
//                                       primary: App24Colors
//                                           .redRestaurantThemeColor,
//                                       shape: RoundedRectangleBorder(
//                                         borderRadius:
//                                         BorderRadius.circular(25.0),
//                                       )),
//                                   onPressed:
//                                   cartListResponseData!.carts!.isEmpty
//                                       ? null
//                                       : () {
//                                     checkOut();
//                                   },
//                                   child: Text(
//                                     "Confirm & Continue",
//                                     style: TextStyle(
//                                         fontSize:
//                                         MediaQuery.of(context).size.width /
//                                             20,
//                                         color: Colors.white,
//                                         fontWeight: FontWeight.bold),
//                                   ),
//                                 )
//                               ],
//                             ),
//                           ),
//                         ],
//                       );
//                     } else {
//                       return Center(
//                         child: Padding(
//                           padding: const EdgeInsets.only(bottom: 20),
//                           child: Column(
//                             mainAxisAlignment: MainAxisAlignment.end,
//                             crossAxisAlignment: CrossAxisAlignment.stretch,
//                             children: [
//                               Center(
//                                 child: Column(
//                                   children: [
//                                     const Text(
//                                       "Drop Location too far",
//                                       style: TextStyle(
//                                           fontWeight: FontWeight.w800),
//                                     ),
//                                     const SizedBox(
//                                       height: 10,
//                                     ),
//                                     const Text(
//                                       "Please select a nearby drop point",
//                                       style: TextStyle(
//                                           fontWeight: FontWeight.w600),
//                                     ),
//                                   ],
//                                 ),
//                               ),
//                               const SizedBox(
//                                 height: 10,
//                               ),
//                               new MaterialButton(
//                                 padding: EdgeInsets.symmetric(vertical: 15),
//                                 color: App24Colors.redRestaurantThemeColor,
//                                 shape: RoundedRectangleBorder(
//                                     borderRadius: BorderRadius.circular(15)),
//                                 child: const Text(
//                                   "Change location",
//                                   style: TextStyle(color: Colors.white),
//                                 ),
//                                 onPressed: () {
//                                   // Navigator.pop(context);
//                                   selectLocation(
//                                       GlobalConstants.labelDropLocation);
//
//                                   // showMessage(
//                                   //     context, "Please change your location", true, false);
//                                 }
//                                 // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomePage()));}
//                                 /*  Navigator.push(context,
//                                       MaterialPageRoute(builder: (context) => LogInPage()))*/
//                                 ,
//                               ),
//                               const SizedBox(
//                                 height: 10,
//                               ),
//                               new MaterialButton(
//                                 padding: EdgeInsets.symmetric(vertical: 15),
//                                 color: App24Colors.redRestaurantThemeColor,
//                                 shape: RoundedRectangleBorder(
//                                     borderRadius: BorderRadius.circular(15)),
//                                 child: const Text(
//                                   "Clear cart",
//                                   style: TextStyle(color: Colors.white),
//                                 ),
//                                 onPressed: () {
//                                   onClearCart();
//                                   // Navigator.pop(context);
//                                   // context
//                                   //     .read(cartNotifierProvider.notifier)
//                                   //     .getClearCart(context: context);
//                                 }
//                                 /*  Navigator.push(context,
//                                       MaterialPageRoute(builder: (context) => LogInPage()))*/
//                                 ,
//                               )
//                             ],
//                           ),
//                         ),
//                       );
//                     }
//                   }),
//                 ),
//               ),
//             ),
//           ),
//         ),
//       ),
//     );
//   }
//   late final ApiRepository _apiRepository;
//   Future <void> getAddressWithHighestPriority() async{
//     print("get addresses cart");
//
//     String addressString='';
//     AddressDatabase addressDatabase = new AddressDatabase(
//         DatabaseService.instance);
//     //final list = await addressDatabase.fetchAddresses();
//     List<AddressResponseData> list = await addressDatabase.fetchAddresses();
//
//
//     // final address;
//     // if (list.isEmpty) {
//     //
//     //   address = await _apiRepository.fetchManageAddress();
//     // }
//     // else {
//     //   address = list;
//     // }
//
//     setState(() {
//       if(list.length>0) {
//         print("address string"+addressString);
//         addressString=list[0].flatNo!+" , "+list[0].street.toString()+" , "+list[0].city.toString();
//         highPriorityAddress=addressString;
//       }
//       else highPriorityAddress="No address added!";
//     });
//
//   }
// }
//
// // class CartTile {
// //   String cartItemImage;
// //   String cartItemName;
// //   String cartItemPrice;
// //   int cartItemCount;
// //
// //   CartTile(
// //       {this.cartItemImage,
// //       this.cartItemName,
// //       this.cartItemPrice,
// //       this.cartItemCount});
// // }
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
// // import 'dart:async';
// //
// // import 'package:app24_user_app/app24_user_icons.dart';
// // import 'package:app24_user_app/constants/asset_path.dart';
// // import 'package:app24_user_app/constants/color_path.dart';
// // import 'package:app24_user_app/constants/global_constants.dart';
// // import 'package:app24_user_app/constants/shared_preferences_path.dart';
// // import 'package:app24_user_app/helpers.dart';
// // import 'package:app24_user_app/models/home_delivery_order_insert_model.dart';
// // import 'package:app24_user_app/models/home_delivery_send_order_detail.dart';
// // import 'package:app24_user_app/models/location_model.dart';
// // import 'package:app24_user_app/modules/delivery/cart/cart_loading.dart';
// // import 'package:app24_user_app/modules/delivery/cart/cart_model.dart';
// // import 'package:app24_user_app/modules/my_account/address/manage_address_model.dart';
// // import 'package:app24_user_app/modules/restaurant/restaurant_cart_loading.dart';
// // import 'package:app24_user_app/utils/helpers/common_helpers.dart';
// // import 'package:app24_user_app/utils/helpers/common_widgets.dart';
// // import 'package:app24_user_app/utils/services/api/api_providers.dart';
// // import 'package:app24_user_app/utils/services/database/address_database.dart';
// // import 'package:app24_user_app/utils/services/database/database_service.dart';
// // import 'package:app24_user_app/utils/services/socket/socket_service.dart';
// // import 'package:app24_user_app/widgets/bottomsheets/addressConfirmation/addressConfirmation.dart';
// // import 'package:app24_user_app/widgets/bottomsheets/checkout_bottomSheet/checkout_bottomsheet.dart';
// // import 'package:app24_user_app/widgets/bottomsheets/make_payment_bottomsheet/make_payment_bottom_sheet.dart';
// // import 'package:app24_user_app/widgets/bottomsheets/order_success_bottomSheet.dart';
// // import 'package:app24_user_app/widgets/bottomsheets/promocodes/add_coupon_bottomSheet.dart';
// // import 'package:app24_user_app/widgets/bottomsheets/promocodes/promocodes_model.dart';
// // import 'package:app24_user_app/widgets/bottomsheets/schedule_bottomSheet.dart';
// // import 'package:app24_user_app/widgets/common_appbar.dart';
// // import 'package:app24_user_app/widgets/loading_error.dart';
// // import 'package:app24_user_app/widgets/payment_waiting_screen.dart';
// // import 'package:app24_user_app/widgets/track_order.dart';
// // import 'package:cached_network_image/cached_network_image.dart';
// // import 'package:flutter/cupertino.dart';
// // import 'package:flutter/gestures.dart';
// // import 'package:flutter/material.dart';
// // import 'package:flutter/scheduler.dart';
// // import 'package:flutter_rating_bar/flutter_rating_bar.dart';
// // import 'package:flutter_riverpod/flutter_riverpod.dart';
// // import 'package:razorpay_flutter/razorpay_flutter.dart';
// // import 'package:shared_preferences/shared_preferences.dart';
// // import 'package:socket_io_client/socket_io_client.dart';
// //
// // class RestaurantCartPage extends StatefulWidget {
// //   final String? itemID;
// //   final String? promoCode;
// //   final String? promoCodeDetails;
// //
// //   const RestaurantCartPage(
// //       {Key? key, this.itemID, this.promoCode, this.promoCodeDetails})
// //       : super(key: key);
// //
// //   @override
// //   _RestaurantCartPageState createState() => _RestaurantCartPageState();
// // }
// //
// // reloadData({required BuildContext context, required bool init}) {
// //   context.read(cartNotifierProvider.notifier).getCartList(
// //       context: context,
// //       init: init,
// //       lat: context
// //           .read(homeNotifierProvider.notifier)
// //           .currentLocation
// //           .latitude!
// //           .toDouble(),
// //       lng: context
// //           .read(homeNotifierProvider.notifier)
// //           .currentLocation
// //           .longitude!
// //           .toDouble());
// //   context
// //       .read(cartCountNotifierProvider.notifier)
// //       .getCartCount(context: context);
// // }
// //
// // class _RestaurantCartPageState extends State<RestaurantCartPage> {
// //   var totalAmount;
// //   int? totalCart;
// //   int? gstAmount;
// //   String? walletActive;
// //   late Map cartContentBody;
// //   late final SocketService _socketService;
// //   late Razorpay? _razorPay;
// //   TextEditingController couponController = new TextEditingController();
// //   var rating = 3.5;
// //   List<Carts>? cartTileList = [];
// //   CartListResponseData? cartListResponseData;
// //   var totalAmountToRazor1;
// //
// //   final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
// //   bool useWallet = false;
// //   bool lockWallet = false;
// //
// //   @override
// //   void initState() {
// //     _socketService = SocketService.instance;
// //     SchedulerBinding.instance!.addPostFrameCallback((_) {
// //       //API call after screen build
// //       // context.read(addToCartNotifierProvider).addCart(context: context,itemID: widget.itemID);
// //       context.read(cartNotifierProvider.notifier).getCartList(
// //           context: context,
// //           lat: context
// //               .read(homeNotifierProvider.notifier)
// //               .currentLocation
// //               .latitude!
// //               .toDouble(),
// //           lng: context
// //               .read(homeNotifierProvider.notifier)
// //               .currentLocation
// //               .longitude!
// //               .toDouble());
// //     });
// //     super.initState();
// //     _initRazorPay();
// //   }
// //
// //   @override
// //   void dispose() {
// //     _razorPay!.clear();
// //     super.dispose();
// //   }
// //
// //   _initRazorPay() {
// //     _razorPay = new Razorpay();
// //
// //     _razorPay!.on(Razorpay.EVENT_PAYMENT_SUCCESS, handlerPaymentSuccess);
// //     _razorPay!.on(Razorpay.EVENT_PAYMENT_ERROR, handlerErrorFailure);
// //     _razorPay!.on(Razorpay.EVENT_EXTERNAL_WALLET, handlerExternalWallet);
// //     return _razorPay;
// //   }
// //
// //   // paymentWaiting() async{
// //   //   await Future.delayed(const Duration(seconds: 3), (){
// //   //     Navigator.pushReplacement(
// //   //       context,
// //   //       MaterialPageRoute(
// //   //           builder: (context) =>
// //   //               PaymentWaiting()),
// //   //     );}
// //   //   );
// //   //   Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => TrackOrder(orderType: 'buy',id: context
// //   //       .read(orderBuyItemsNotifier.notifier)
// //   //       .state
// //   //       .response
// //   //       .responseData!
// //   //       .id.toString(),)));
// //   // }
// //
// //   void handlerPaymentSuccess(PaymentSuccessResponse response) async {
// //     var prefs = await SharedPreferences.getInstance();
// //     Navigator.pushReplacement(
// //         context,
// //         MaterialPageRoute(
// //             builder: (context) => PaymentWaiting(
// //                   orderID: prefs.getInt(SharedPreferencesPath.lastOrderId)!,
// //                   orderType: "buy",
// //                 )));
// //
// //     // Helpers().getCommonBottomSheet(context: context, content: OrderSuccess(orderType: 'buy',orderID: prefs.getInt(SharedPreferencesPath.lastOrderId),/*orderType: "buy",orderID: responseData.id,*/));
// //     print("Payment successful");
// // // paymentWaiting();
// //     // Navigator.push(
// //     //   context,
// //     //   MaterialPageRoute(
// //     //       builder: (context) => PaymentWaiting(
// //     //         orderID:  context
// //     //             .read(orderBuyItemsNotifier.notifier)
// //     //             .state
// //     //             .response
// //     //             .responseData!
// //     //             .id,orderType: "buy",
// //     //       )),
// //     // );
// //
// //     // Helpers().getCommonBottomSheet(context: context, content: OrderSuccess());
// //     //
// //     // emitSocket(
// //     //     orderID: context
// //     //         .read(orderBuyItemsNotifier.notifier)
// //     //         .state
// //     //         .response
// //     //         .responseData!
// //     //         .id);
// //   }
// //
// //   void handlerErrorFailure(
// //     PaymentFailureResponse response,
// //     /*HomeDeliveryOrderDetailResponseData responseData*/
// //   ) {
// //     context.read(orderDetailsNotifierProvider.notifier).checkOngoingOrders();
// //     _socketService.listenSocket();
// //
// //     // Helpers().getCommonBottomSheet(
// //     //     context: context,
// //     //     enableDrag: false,
// //     //     isDismissible: false,
// //     //     content: MakePaymentBottomSheet(
// //     //       detailResponseData: responseData,
// //     //     ),
// //     //     title: "Retry");
// //     print("Payment failed");
// //   }
// //
// //   void handlerExternalWallet(
// //     ExternalWalletResponse response,
// //     /* HomeDeliveryOrderDetailResponseData responseData*/
// //   ) {
// //     context.read(orderDetailsNotifierProvider.notifier).checkOngoingOrders();
// //     _socketService.listenSocket();
// //     // Helpers().getCommonBottomSheet(
// //     //     context: context,
// //     //     enableDrag: false,
// //     //     isDismissible: false,
// //     //     content: MakePaymentBottomSheet(
// //     //       detailResponseData: responseData,
// //     //     ),
// //     //     title: "Retry");
// //     print("Payment external");
// //   }
// //
// //   updateDetails({required CartListResponseData responseData}) {
// //     totalAmount = responseData.totalItemPrice;
// //     totalCart = responseData.totalCart;
// //   }
// //
// // /*  createOrder() {
// //     amountToCheckOut = cartContentBody!['total'];
// //     cartContentBody!.remove('total');
// //     context
// //         .read(orderPackageNotifier.notifier)
// //         .sendPackageSave(body: cartContentBody);
// //     if (cartContentBody!['wallet'] == '1') {
// //       //if wallet ? update the user wallet amount
// //       context.read(homeNotifierProvider.notifier).getProfile();
// //     }
// //   }*/
// //
// //   // insertOrder(HomeDeliveryOrderInsertResponseData responseData) async {
// //   //   emitSocket(orderID: responseData.id ?? 1);
// //   // }
// //
// //   emitSocket({required int orderID}) async {
// //     showLog("Socket emitting");
// //     try {
// //       Socket? socket = await _socketService.socket;
// //       socket!.emit('joinPrivateRoom', {'room_1_R${orderID}_ORDER'});
// //       showLog("Socket emitted");
// //       //_socketService.listenSocket();
// //       context.read(orderDetailsNotifierProvider.notifier).orderType = 'Buy';
// //
// //       // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => TrackOrder(orderType: 'buy',id: orderID.toString(),)));
// //
// //       // Navigator.pushReplacement(
// //       //   context,
// //       //   MaterialPageRoute(
// //       //       builder: (context) => PaymentWaiting(
// //       //         orderID: orderID,orderType: "buy",
// //       //       )),
// //       // );
// //
// //       // Navigator.pushReplacement(
// //       //   context,
// //       //   MaterialPageRoute(
// //       //       builder: (context) => TrackOrder(
// //       //         id: orderID.toString(),orderType: "buy",
// //       //       )),
// //       // );
// //
// //       if (use_wallet_checkout == true)
// //         Helpers().getCommonBottomSheet(
// //             context: context,
// //             isDismissible: false,
// //             content: OrderSuccess(
// //               orderID: orderID,
// //               orderType: "buy",
// //             ));
// //       // Navigator.push(
// //       //   context,
// //       //   MaterialPageRoute(
// //       //       builder: (context) => TrackOrder(
// //       //             id: orderID.toString(),
// //       //           )),
// //       // );
// //     } catch (e) {
// //       showLog("error in socket is : ${e.toString()}");
// //     }
// //   }
// //
// //   Future<void> buildCheckoutBottomSheet() async {
// //     // var result = await Helpers().getCommonBottomSheet(
// //     //     context: context,
// //     //     content: CheckoutBottomSheet(
// //     //       cartList: cartListResponseData!,
// //     //       promoCode: widget.promoCode,
// //     //       promoCodeDetails: widget.promoCodeDetails,
// //     //     ),
// //     //     title: "Checkout");
// //     // if (result != null) {
// //     //   Map checkOutResult = result;
// //     //   // / cartContentBody['promocode_id'] = checkOutResult['promo_code'];
// //     //   cartContentBody['wallet'] = checkOutResult['use_wallet'] ? 1 : 0;
// //
// //     var value = await Helpers().getCommonBottomSheet(
// //         context: context,
// //         content: ScheduleBottomSheet(
// //           checkoutType: "fromSend",
// //           fromRestaurantCart: "true",
// //           // orderItemBody: cartContentBody,
// //         ),
// //         title: "Schedule Delivery");
// //     // if (value != null) {
// //     // print("xavi" + value);
// //     if (value == 'deliver_now') {
// //       callConfirmAddressBottomSheet();
// //     } else {
// //       Map map = value;
// //       // cartContentBody['schedule_date'] = map['date'];
// //       // cartContentBody['schedule_time'] = map['time'];
// //       cartContentBody['delivery_date'] = map['date'] + " " + map['time'];
// //       print("map in restaurant cart page " +
// //           cartContentBody['delivery_date'].toString());
// //       callConfirmAddressBottomSheet(map: map);
// //     }
// //     // }
// //     // }
// //     // Helpers().getCommonBottomSheet(context: context, content: ScheduleBottomSheet());
// //   }
// //
// //   callConfirmAddressBottomSheet({Map<dynamic, dynamic>? map}) async {
// //     var addressResult = await Helpers().getCommonBottomSheet(
// //         context: context,
// //         content: AddressConfirmationBottomSheet(
// //           map: map,
// //         ),
// //         title: "Address confirmation");
// //     if (addressResult != null) {
// //       AddressResponseData confirmedLocation = addressResult;
// //       print("selected location id" + confirmedLocation.addressid.toString());
// //       updatePriority(confirmedLocation);
// //
// //       String? toAddress = (confirmedLocation.flatNo.toString() +
// //           "," +
// //           confirmedLocation.street.toString() +
// //           "," +
// //           confirmedLocation.city.toString() +
// //           "," +
// //           confirmedLocation.landmark.toString());
// //
// //       cartContentBody["to_adrs"] = toAddress;
// //       cartContentBody["to_lat"] = confirmedLocation.latitude;
// //       cartContentBody["to_lng"] = confirmedLocation.longitude;
// //
// //       var checkout = await Helpers().getCommonBottomSheet(
// //           enableDrag: true,
// //           isDismissible: true,
// //           context: context,
// //           content: CheckoutBottomSheet(
// //             toAddress: toAddress,
// //             cartList: cartListResponseData!,
// //             latitude: confirmedLocation.latitude,
// //             longitude: confirmedLocation.longitude,
// //             // promoCode: widget.promoCode,
// //             // promoCodeDetails: widget.promoCodeDetails,
// //           ));
// //
// //       if (checkout == "clearCart") {
// //         context.read(cartNotifierProvider.notifier).getCartList(
// //             context: context,
// //             init: true,
// //             lat: context
// //                 .read(homeNotifierProvider.notifier)
// //                 .currentLocation
// //                 .latitude!
// //                 .toDouble(),
// //             lng: context
// //                 .read(homeNotifierProvider.notifier)
// //                 .currentLocation
// //                 .longitude!
// //                 .toDouble());
// //       } else /* if (checkout != null)*/ {
// //         var useWallet = checkout;
// //         print("checknow");
// //         print(useWallet);
// //
// //         setState(() {
// //           walletActive = useWallet["wallet"];
// //           // totalAmountToRazor = useWallet["total_amount"];
// //           use_wallet_checkout = walletActive == "1" ? true : false;
// //           print("checknow wallet");
// //           print(useWallet["wallet"]);
// //           print("if wallet used :");
// //           print(use_wallet_checkout);
// //           // print("success payment razor"+handlerSuccess.toString());
// //         });
// //         cartContentBody["wallet"] = useWallet["wallet"];
// //         if (useWallet["to_address"] != null) {
// //           cartContentBody["to_adrs"] = useWallet["to_address"];
// //           cartContentBody["to_lat"] = useWallet["to_lat"];
// //           cartContentBody["to_lng"] = useWallet["to_lng"];
// //           cartContentBody["user_address_id"] = confirmedLocation.addressid;
// //         }
// //
// //         print("cartcontent body" + cartContentBody.toString());
// //         //commented for testing
// //         context
// //             .read(orderBuyItemsNotifier.notifier)
// //             .buyItemsOrderPlaceFromCart(body: cartContentBody);
// //       }
// //     }
// //   }
// //
// //   checkOut() async {
// //     var prefs = await SharedPreferences.getInstance();
// //     prefs.setString(SharedPreferencesPath.orderType, "buy");
// //     // LocationModel userLocation =
// //     //     context.read(homeNotifierProvider.notifier).currentLocation;
// //
// //     cartContentBody = {
// //       //"promocode_id": "",
// //       //"wallet" :  reqPlaceOrder.wallet.toString()) "1" ? checked : "0"
// //       "payment_mode": "RAZORPAY",
// //       //"payment_id" : razorID.value.toString())
// //       //"card_id": "",
// //       "user_address_id": '0',
// //       //"to_adrs" :  userLocation.description,
// //       //"to_lat" :  HomeLatLng!!.latitude)
// //       //"to_lng" :  HomeLatLng!!.longitude)
// //       //"delivery_date" :  Delivery_Date!!)
// //       "order_type": "DELIVERY"
// //     };
// //     // context
// //     //     .read(orderBuyItemsNotifier.notifier)
// //     //     .buyItemsOrderPlaceFromCart(
// //     //     body: cartContentBody, context: context);
// //     buildCheckoutBottomSheet();
// //
// //     // context
// //     //     .read(orderBuyItemsNotifier.notifier)
// //     //     .buyItemsOrderPlaceFromCart(body: cartContentBody, context: context);
// //   }
// //
// //   checkPaymentAndEmitSocket(
// //       {required HomeDeliveryOrderInsertResponseData responseData}) async {
// //     var prefs = await SharedPreferences.getInstance();
// //     prefs.setString(SharedPreferencesPath.lastOrderType, "buy");
// //     prefs.setInt(SharedPreferencesPath.lastOrderId, responseData.id);
// //     if (cartListResponseData!.showEstimation == 1 &&
// //         responseData.totalPayable != '0.00') {
// //       Map checkOutOptions = {
// //         'description': 'homedelivery${responseData.id}',
// //         'amount': double.parse(responseData.totalPayable.toString()),
// //         "notes": {
// //           "store_invoice_id": responseData.storeOrderInvoiceId,
// //           "type": "homedelivery",
// //           "Store_order_id": responseData.id
// //         }
// //       };
// //       try {
// //         print("RAZORPAY::::  " + checkOutOptions.toString());
// //         _razorPay!.open(await getRazorPayOptions(
// //             context: context, values: checkOutOptions));
// //         emitSocket(orderID: responseData.id!);
// //       } catch (e) {
// //         showLog(e.toString());
// //       }
// //     } else {
// //       emitSocket(orderID: responseData.id!);
// //     }
// //   }
// //
// //   priceDetails({String? title, String? value}) {
// //     return Row(
// //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
// //       children: [
// //         Text(
// //           title!,
// //           style: TextStyle(
// //               fontSize: 17,
// //               fontWeight: FontWeight.bold,
// //               color: App24Colors.darkTextColor),
// //         ),
// //         Text(
// //           value!,
// //           style: TextStyle(
// //               fontSize: 17,
// //               fontWeight: FontWeight.bold,
// //               color: App24Colors.lightBlue),
// //         )
// //       ],
// //     );
// //   }
// //
// //   onTapRemove(index) async {
// //     Map body = {"cart_id": cartTileList![index].id};
// //
// //     //print("deleted item:"+   cartTileList![index].product!.itemName.toString());
// //
// //     setState(() {
// //       if (context.read(cartNotifierProvider.notifier).cartIdList != null) {
// //         int b = context
// //             .read(cartNotifierProvider.notifier)
// //             .cartIdList!
// //             .removeAt(index);
// //         print("deleted ?" + b.toString());
// //
// //         for (int i = 0;
// //             i < context.read(cartNotifierProvider.notifier).cartIdList!.length;
// //             i++) {
// //           print("products id: " +
// //               context
// //                   .read(cartNotifierProvider.notifier)
// //                   .cartIdList![i]
// //                   .toString());
// //         }
// //       }
// //     });
// //
// //     await context
// //         .read(cartNotifierProvider.notifier)
// //         .removeItemCart(body: body, context: context);
// //
// //     context.read(cartNotifierProvider.notifier).getCartList(
// //         context: context,
// //         init: true,
// //         lat: context
// //             .read(homeNotifierProvider.notifier)
// //             .currentLocation
// //             .latitude!
// //             .toDouble(),
// //         lng: context
// //             .read(homeNotifierProvider.notifier)
// //             .currentLocation
// //             .longitude!
// //             .toDouble());
// //     int cartCount =
// //         context.read(cartCountNotifierProvider.notifier).state.response;
// //     print("cartcount on tap remove " + cartCount.toString());
// //     context.read(cartCountNotifierProvider.notifier).updateCartCount(
// //         count: cartCount - 1, storeID: cartTileList![index].storeId!);
// //     //context.read(cartCountNotifierProvider.notifier).getCartCount(context: context);
// //   }
// //
// //   selectLocation(String title) {
// //     showSelectLocationSheet(context: context, title: title).then((value) {
// //       if (value != null) {
// //         LocationModel model = value;
// //         setState(() {
// //           context.read(homeNotifierProvider.notifier).currentLocation = model;
// //           context.read(cartNotifierProvider.notifier).getCartList(
// //               lat: context
// //                   .read(homeNotifierProvider.notifier)
// //                   .currentLocation
// //                   .latitude!
// //                   .toDouble(),
// //               lng: context
// //                   .read(homeNotifierProvider.notifier)
// //                   .currentLocation
// //                   .longitude!
// //                   .toDouble(),
// //               context: context);
// //           // loadData(context: context);
// //         });
// //       }
// //     });
// //   }
// //
// //   onClearCart() async {
// //     context.read(cartNotifierProvider.notifier).getClearCart(context: context);
// //     print("cart list cleared" +
// //         context.read(cartNotifierProvider.notifier).isCartCleared.toString());
// //     // if(context
// //     //     .read(cartNotifierProvider.notifier)
// //     //     .isCartCleared==true) {
// //     //
// //     //   print("cal cartlist inside condition");
// //     //
// //     //   await context.read(cartNotifierProvider.notifier).getCartList(
// //     //       context: context,
// //     //       init: true,
// //     //       lat: context
// //     //           .read(homeNotifierProvider.notifier)
// //     //           .currentLocation
// //     //           .latitude!
// //     //           .toDouble(),
// //     //       lng: context
// //     //           .read(homeNotifierProvider.notifier)
// //     //           .currentLocation
// //     //           .longitude!
// //     //           .toDouble());
// //     //   context.read(cartCountNotifierProvider.notifier).updateCartCount(
// //     //       count: 0);
// //     // }
// //     // int cartCount =
// //     //     context.read(cartCountNotifierProvider.notifier).state.response;
// //
// //     context.read(cartCountNotifierProvider.notifier).updateCartCount(count: 0);
// //   }
// //
// //   updateCart({required Carts cartItem, String? method, index}) async {
// //     int? qty = cartItem.quantity;
// //     // print("qty "+method.toString()+" :"+qty.toString());
// //     if (method == 'remove') {
// //       if (qty != 0) qty = qty! - 1;
// //
// //       print("qty " + method.toString() + " :" + qty.toString());
// //
// //       if (qty == 0) onTapRemove(index);
// //     } else {
// //       qty = qty! + 1;
// //     }
// //
// //     Map body = {
// //       "qty": qty,
// //       // "repeat" : product.,
// //       "item_id": cartItem.product!.id,
// //       "to_lat":
// //           context.read(homeNotifierProvider.notifier).currentLocation.latitude,
// //       "to_lng":
// //           context.read(homeNotifierProvider.notifier).currentLocation.longitude,
// //     };
// //     if (qty != 0)
// //       context.read(cartNotifierProvider.notifier).addCart(
// //           itemAddons: '',
// //           itemQty: qty as int,
// //           context: context,
// //           itemID: cartItem.product!.id.toString(),
// //           bodys: body,
// //           init: true);
// //     //reloadData(context: context, init: true);
// //   }
// //
// //   buildCartItems(int index) {
// //     var totalItemPrice =
// //         cartTileList![index].quantity! * cartTileList![index].itemPrice!;
// //     return Container(
// //       padding: EdgeInsets.all(15),
// //       // height: MediaQuery.of(context).size.height / 7,
// //       margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),
// //       decoration: BoxDecoration(
// //           borderRadius: BorderRadius.circular(25),
// //           color: Colors.white,
// //           border: Border.all(color: Colors.grey[200]!),
// //           boxShadow: [
// //             BoxShadow(
// //               offset: const Offset(0.0, 1.0),
// //               color: Colors.grey[200]!,
// //               blurRadius: 4.0,
// //               spreadRadius: 4.0,
// //             )
// //           ]),
// //       child: Column(
// //         crossAxisAlignment: CrossAxisAlignment.start,
// //         mainAxisAlignment: MainAxisAlignment.center,
// //         children: [
// //           Row(
// //             children: [
// //               // CachedNetworkImage(imageUrl: cartTileList![index].product!.picture.toString(),width: 60,),
// //               Expanded(
// //                 child: Text(
// //                   cartTileList![index].product!.itemName!,
// //                   style: TextStyle(
// //                       color: App24Colors.shopAndItemNameColor,
// //                       fontSize: 18,
// //                       fontWeight: FontWeight.bold),
// //                   overflow: TextOverflow.clip,
// //                 ),
// //               ),
// //               Container(
// //                 // padding: EdgeInsets.symmetric(vertical: 5,horizontal: 10),
// //                 decoration: BoxDecoration(
// //                     color: Color(0xffFF533C).withOpacity(0.11),
// //                     borderRadius: BorderRadius.circular(15)),
// //                 child: Row(
// //                   // mainAxisAlignment: MainAxisAlignment.center,
// //                   mainAxisAlignment: MainAxisAlignment.spaceAround,
// //                   children: [
// //                     Material(
// //                       borderRadius: BorderRadius.all(Radius.circular(50)),
// //                       color: Colors.transparent,
// //                       // shadowColor: Colors.grey[400],
// //                       // elevation: 3,
// //                       child: InkWell(
// //                         splashColor: Colors.red,
// //                         borderRadius: BorderRadius.all(Radius.circular(50)),
// //                         onTap: () {
// //                           updateCart(
// //                               cartItem: cartTileList![index],
// //                               method: "remove",
// //                               index: index);
// //                         },
// //                         child: Padding(
// //                           padding: EdgeInsets.symmetric(
// //                               horizontal:
// //                                   MediaQuery.of(context).size.width / 45,
// //                               vertical:
// //                                   MediaQuery.of(context).padding.vertical / 30),
// //                           child: Text(
// //                             "-",
// //                             style: TextStyle(
// //                                 fontWeight: FontWeight.bold,
// //                                 fontSize: 19,
// //                                 color: Colors.red),
// //                           ),
// //                         ),
// //                       ),
// //                     ),
// //                     Center(
// //                       child: Container(
// //                         // width: 15,
// //                         alignment: Alignment.center,
// //                         child: Text(
// //                           cartTileList![index].quantity.toString(),
// //                           style: TextStyle(
// //                               fontWeight: FontWeight.w700, fontSize: 16),
// //                         ),
// //                       ),
// //                     ),
// //                     Material(
// //                       color: Colors.transparent,
// //                       child: InkWell(
// //                         splashColor: Colors.green,
// //                         borderRadius: BorderRadius.all(Radius.circular(50)),
// //                         onTap: () {
// //                           // print( cartTileList[index].product.itemsaddon.toString(),);
// //                           updateCart(
// //                               cartItem: cartTileList![index], method: "add");
// //                         },
// //                         child: Padding(
// //                           padding: EdgeInsets.symmetric(
// //                               horizontal:
// //                                   MediaQuery.of(context).size.width / 55,
// //                               vertical:
// //                                   MediaQuery.of(context).padding.vertical / 30),
// //                           child: Text(
// //                             "+",
// //                             style: TextStyle(
// //                                 fontWeight: FontWeight.bold,
// //                                 fontSize: 19,
// //                                 color:
// //                                     App24Colors.greenOrderEssentialThemeColor),
// //                           ),
// //                         ),
// //                       ),
// //                     ),
// //                   ],
// //                 ),
// //               ),
// //               const SizedBox(
// //                 width: 10,
// //               ),
// //               Material(
// //                 color: Colors.white,
// //                 child: InkWell(
// //                   onTap: () {
// //                     onTapRemove(index);
// //                     // showDialog(
// //                     //     context: context,
// //                     //     builder: (_) {
// //                     //       return SingleChildScrollView(
// //                     //         child: Center(
// //                     //           child: AlertDialog(
// //                     //             content: Column(
// //                     //               children: [
// //                     //                 Text(
// //                     //                     "Are you sure you want to delete this item?"),
// //                     //                 Row(
// //                     //                   children: [
// //                     //                     MaterialButton(
// //                     //                       child: Text("No"),
// //                     //                       onPressed:  (){Navigator.pop(context);},
// //                     //                     ),
// //                     //                     MaterialButton(
// //                     //                       child: Text("Yes"),
// //                     //                       onPressed: (){
// //                     //                         Navigator.pop(context);
// //                     //                         onTapRemove(index);},
// //                     //                     )
// //                     //                   ],
// //                     //                 )
// //                     //               ],
// //                     //             ),
// //                     //
// //                     //           ),
// //                     //         ),
// //                     //       );
// //                     //     });
// //
// //                     // onTapRemove(index);
// //                   },
// //                   child: Icon(
// //                     Icons.delete,
// //                     color: Colors.grey,
// //                     size: 20,
// //                   ),
// //                 ),
// //               )
// //             ],
// //           ),
// //           const SizedBox(
// //             height: 5,
// //           ),
// //           if (cartTileList![index].cartaddon!.isNotEmpty)
// //             Row(
// //               children: [
// //                 Expanded(
// //                   child: Text(
// //                     "Add Ons:",
// //                     style: TextStyle(
// //                         fontSize: 11,
// //                         color: Color(0xff9B9B9B),
// //                         fontWeight: FontWeight.bold),
// //                   ),
// //                 ),
// //                 // Expanded(
// //                 //   child: Text("Qty:",                    style: TextStyle(
// //                 //       fontSize: MediaQuery.of(context).size.width / 26,
// //                 //       color: Color(0xff9B9B9B),
// //                 //       fontWeight: FontWeight.bold)),
// //                 // ),
// //                 // Expanded(
// //                 //   child: Text("Price:",                    style: TextStyle(
// //                 //       fontSize: MediaQuery.of(context).size.width / 26,
// //                 //       color: Color(0xff9B9B9B),
// //                 //       fontWeight: FontWeight.bold)),
// //                 // ),
// //               ],
// //             ),
// //           if (cartListResponseData!.showEstimation == 1)
// //             Row(mainAxisAlignment: MainAxisAlignment.end, children: [
// //               if (cartTileList![index].cartaddon!.isNotEmpty)
// //                 Expanded(
// //                     child: buildAddonsList(cartTileList![index].cartaddon!)),
// //               Text(
// //                 "AED " + totalItemPrice.toString(),
// //                 style: TextStyle(
// //                     fontSize: 22,
// //                     fontWeight: FontWeight.bold,
// //                     color: App24Colors.redRestaurantThemeColor),
// //               )
// //             ]),
// //         ],
// //       ),
// //     );
// //   }
// //
// //   Widget buildAddonsList(List<Cartaddon> addonList) {
// //     List<Widget> list = [];
// //     addonList.forEach((element) {
// //       list.add(Row(
// //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
// //         children: [
// //           Expanded(
// //               child: Text(
// //             element.addonName!,
// //             style: TextStyle(
// //                 color: Color(0xff9B9B9B),
// //                 fontWeight: FontWeight.w500,
// //                 fontSize: 11),
// //           )),
// //           Expanded(
// //             child: Text(
// //               element.addonQuantity!
// //                   .substring(0, element.addonQuantity!.length - 3),
// //               style: TextStyle(
// //                   color: Color(0xffFF533C),
// //                   fontSize: 10,
// //                   fontWeight: FontWeight.w500),
// //             ),
// //           ),
// //           Expanded(
// //             child: Text(
// //               "AED " + element.addonPrice.toString(),
// //               style: TextStyle(
// //                   color: Color(0xffFF533C),
// //                   fontSize: 10,
// //                   fontWeight: FontWeight.w500),
// //             ),
// //           )
// //         ],
// //       ));
// //     });
// //     return Column(
// //       children: list,
// //     );
// //   }
// //
// //   void enterCoupon() {
// //     showDialog(
// //       context: context,
// //       // barrierDismissible: false,
// //       builder: (_) {
// //         // return CouponCode();
// //         return Container();
// //       },
// //     );
// //   }
// //
// //   bool? use_wallet_checkout = false;
// //
// //   String? getShopName(context, CartListResponseData shopName) {
// //     return shopName.carts!.first.store!.storeName;
// //   }
// //
// //   double? getShopRating(context, CartListResponseData shopName) {
// //     return double.parse(shopName.carts!.first.store!.rating.toString());
// //   }
// //
// //   @override
// //   Widget build(BuildContext context) {
// //     return Scaffold(
// //       // bottomNavigationBar: Column(),
// //       persistentFooterButtons: [
// //         // Padding(
// //         //   padding: const EdgeInsets.only(
// //         //       top: 10.0, bottom: 10.0, left: 10, right: 10),
// //         //   child: Column(
// //         //     children: [
// //         //       /**/
// //         //       Column(
// //         //         // mainAxisSize: MainAxisSize.min,
// //         //         // crossAxisAlignment:
// //         //         // CrossAxisAlignment.start,
// //         //         children: [
// //         //           Row(
// //         //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
// //         //             children: [
// //         //               Text(
// //         //                 "Total Amount",
// //         //                 style: TextStyle(
// //         //                     fontSize: MediaQuery.of(context).size.width / 18,
// //         //                     fontWeight: FontWeight.bold),
// //         //               ),
// //         //               Text(
// //         //                 "AED$totalAmount /-",
// //         //                 style: TextStyle(
// //         //                     fontSize: MediaQuery.of(context).size.width / 18,
// //         //                     fontWeight: FontWeight.bold,
// //         //                     color: App24Colors.darkTextColor),
// //         //               ),
// //         //             ],
// //         //           ),
// //         //           if (cartListResponseData!.showEstimation == 1 &&
// //         //               cartListResponseData!.userWalletBalance != 0)
// //         //             Row(
// //         //               mainAxisAlignment: MainAxisAlignment.center,
// //         //               // mainAxisAlignment: MainAxisAlignment.spaceBetween,              // crossAxisAlignment: CrossAxisAlignment.stretch,
// //         //               // mainAxisAlignment: MainAxisAlignment.spaceBetween,
// //         //               children: [
// //         //                 Transform.scale(
// //         //                     scale: 1.1,
// //         //                     child: Checkbox(
// //         //                       shape: RoundedRectangleBorder(
// //         //                           borderRadius: BorderRadius.circular(15)),
// //         //                       checkColor: Colors.white,
// //         //                       activeColor:
// //         //                           App24Colors.greenOrderEssentialThemeColor,
// //         //                       value: lockWallet ? true : useWallet,
// //         //                       onChanged:
// //         //                           /*lockWallet
// //         //                                 ? null
// //         //                                 :*/
// //         //                           (bool? value) {
// //         //                         setState(() {
// //         //                           // if(widget.cartList.userWalletBalance < 0){
// //         //                           //   useWallet = true;
// //         //                           // }else {
// //         //                           useWallet = value!;
// //         //                           // }
// //         //                           // print(getTotalAmount());
// //         //                         });
// //         //                       },
// //         //                     )),
// //         //
// //         //                 // Flexible(
// //         //                 //   child: Row(
// //         //                 //     children: [
// //         //                 //       Text("Use",
// //         //                 //           style: TextStyle(
// //         //                 //               fontSize: 15,
// //         //                 //               fontWeight: FontWeight.bold)),
// //         //                 //       const SizedBox(
// //         //                 //         width: 5,
// //         //                 //       ),
// //         //                 //       useWallet == false
// //         //                 //           ? Text("(AED0)",
// //         //                 //           style: TextStyle(
// //         //                 //               fontSize: 18,
// //         //                 //               fontWeight: FontWeight.bold,
// //         //                 //               color: App24Colors
// //         //                 //                   .redRestaurantThemeColor),
// //         //                 //       overflow: TextOverflow.clip,)
// //         //                 //           : Expanded(
// //         //                 //             child: Text(
// //         //                 //             " (AED " +
// //         //                 //                 (widget.cartList.totalPrice! +
// //         //                 //                     widget
// //         //                 //                         .cartList.shopGstAmount)
// //         //                 //                     .toString() +
// //         //                 //                 ")",
// //         //                 //             style: TextStyle(
// //         //                 //                 fontSize: 18,
// //         //                 //                 fontWeight: FontWeight.bold,
// //         //                 //                 color: App24Colors
// //         //                 //                     .redRestaurantThemeColor),
// //         //                 //       overflow: TextOverflow.clip,),
// //         //                 //           ),
// //         //                 //     ],
// //         //                 //   ),
// //         //                 // ),
// //         //                 // const SizedBox(
// //         //                 //   width: 15,
// //         //                 // ),
// //         //
// //         //                 // Checkbox(
// //         //                 //   checkColor: Colors.greenAccent,
// //         //                 //   activeColor: Colors.red,
// //         //                 //   value: this.useWallet,
// //         //                 //   onChanged: (bool? value) {
// //         //                 //     setState(() {
// //         //                 //       this.useWallet = value;
// //         //                 //     });
// //         //                 //   },
// //         //                 // ),
// //         //                 Row(
// //         //                   children: [
// //         //                     Text(
// //         //                       cartListResponseData!.userWalletBalance < 0
// //         //                           ? "Negative Wallet"
// //         //                           : "Wallet Amount",
// //         //                       style: TextStyle(
// //         //                           fontSize: 15, fontWeight: FontWeight.bold),
// //         //                     ),
// //         //                     const SizedBox(
// //         //                       width: 5,
// //         //                     ),
// //         //                     Text(
// //         //                       "(AED${cartListResponseData!.userWalletBalance.toString()})",
// //         //                       // useWallet == false
// //         //                       //     ? "(AED" +
// //         //                       //     widget.cartList.userWalletBalance
// //         //                       //         .toString() +
// //         //                       //     ")"
// //         //                       //     : ("(AED" +
// //         //                       //     (widget.cartList
// //         //                       //         .userWalletBalance! -
// //         //                       //         widget
// //         //                       //             .cartList.totalPrice!)
// //         //                       //         .toString()) +
// //         //                       //     ")",
// //         //                       style: TextStyle(
// //         //                           fontSize: 18,
// //         //                           fontWeight: FontWeight.bold,
// //         //                           color: App24Colors
// //         //                               .greenOrderEssentialThemeColor),
// //         //                       overflow: TextOverflow.clip,
// //         //                     ),
// //         //                   ],
// //         //                 )
// //         //               ],
// //         //             ),
// //         //         ],
// //         //       ),
// //         //       Row(
// //         //         children: [
// //         //           Expanded(
// //         //             child: ElevatedButton(
// //         //               onPressed: cartListResponseData!.carts!.isEmpty
// //         //                   ? null
// //         //                   : () {
// //         //                       checkOut();
// //         //                     },
// //         //               style: ElevatedButton.styleFrom(
// //         //                   padding: EdgeInsets.symmetric(vertical: 15),
// //         //                   shape: RoundedRectangleBorder(
// //         //                       borderRadius: BorderRadius.circular(25)),
// //         //                   primary: App24Colors.redRestaurantThemeColor),
// //         //               child: Text(
// //         //                 "Confirm & Continue",
// //         //                 style: TextStyle(
// //         //                     fontWeight: FontWeight.bold, fontSize: 16),
// //         //               ),
// //         //             ),
// //         //           ),
// //         //         ],
// //         //       )
// //         //     ],
// //         //   ),
// //         // )
// //       ],
// //       appBar: CustomAppBar(title: 'Cart',hideCartIcon: "true",),
// //       body: SafeArea(
// //         child: RefreshIndicator(
// //           onRefresh: () {
// //             return reloadData(init: true, context: context) ??
// //                 false as Future<void>;
// //           },
// //           child: Container(
// //             child: SingleChildScrollView(
// //               child: Column(
// //                 crossAxisAlignment: CrossAxisAlignment.start,
// //                 children: [
// //                   // ClipRRect(
// //                   //     borderRadius: BorderRadius.only(
// //                   //         bottomRight: Radius.circular(40),
// //                   //         bottomLeft: Radius.circular(40)),
// //                   //     child: Image.asset(
// //                   //       App24UserAppImages.cartDropOffer,
// //                   //     )),
// //                   // Positioned(
// //                   //   top: 180,
// //                   //   left: 25,
// //                   //   child: Consumer(builder: (context, watch, child) {
// //                   //     final state = watch(cartNotifierProvider);
// //                   //     if (state.isLoading) {
// //                   //       return CircularProgressIndicator();
// //                   //     } else if (state.isError) {
// //                   //       return LoadingError(
// //                   //         onPressed: (res) {
// //                   //           reloadData(init: true, context: res);
// //                   //         },
// //                   //         message: state.errorMessage.toString(),
// //                   //       );
// //                   //     } else {
// //                   //       if (state.response != 0 && state.response != 1) {
// //                   //         cartTileList = state.response.responseData.carts;
// //                   //         return cartTileList!.length == 0
// //                   //             ? Container()
// //                   //             : Row(
// //                   //                 mainAxisAlignment:
// //                   //                     MainAxisAlignment.spaceBetween,
// //                   //                 children: [
// //                   //                   Column(
// //                   //                     crossAxisAlignment:
// //                   //                         CrossAxisAlignment.start,
// //                   //                     children: [
// //                   //                       Text(
// //                   //                         // cartListResponseData!.carts!.first.store!.storeName.toString(),
// //                   //                         getShopName(context,
// //                   //                             state.response.responseData)!,
// //                   //                         style: TextStyle(
// //                   //                             color: Colors.white,
// //                   //                             fontSize: 26,
// //                   //                             fontWeight: FontWeight.bold),
// //                   //                       ),
// //                   //                       RatingBar.builder(
// //                   //                         initialRating: getShopRating(
// //                   //                             context,
// //                   //                             state.response.responseData)!,
// //                   //                         direction: Axis.horizontal,
// //                   //                         allowHalfRating: true,
// //                   //                         itemCount: 5,
// //                   //                         itemSize: 20,
// //                   //                         itemPadding: EdgeInsets.symmetric(
// //                   //                             horizontal: 4.0),
// //                   //                         itemBuilder: (context, _) => Icon(
// //                   //                           Icons.star,
// //                   //                           color: Colors.amber,
// //                   //                         ),
// //                   //                         onRatingUpdate: (rating) {
// //                   //                           print(rating);
// //                   //                         },
// //                   //                         ignoreGestures: true,
// //                   //                       ),
// //                   //                     ],
// //                   //                   ),
// //                   //                   // const SizedBox(
// //                   //                   //   width: 110,
// //                   //                   // ),
// //                   //                   // Image.asset(
// //                   //                   //   App24UserAppImages.roundRArrow,
// //                   //                   //   color: Colors.white,
// //                   //                   //   width: MediaQuery
// //                   //                   //       .of(context)
// //                   //                   //       .size
// //                   //                   //       .width /
// //                   //                   //       15,
// //                   //                   // )
// //                   //                 ],
// //                   //               );
// //                   //       } else {
// //                   //         return Container();
// //                   //       }
// //                   //     }
// //                   //   }),
// //                   // ),
// //                   // Positioned(
// //                   //   left: 10,
// //                   //   right: 15,
// //                   //   top: 10,
// //                   //   child: Row(
// //                   //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
// //                   //     children: [
// //                   //       Row(
// //                   //         children: [
// //                   //           Material(
// //                   //             borderRadius: BorderRadius.circular(20),
// //                   //             color: Colors.transparent,
// //                   //             child: InkWell(
// //                   //                 borderRadius: BorderRadius.circular(20),
// //                   //                 onTap: () {
// //                   //                   Navigator.pop(context);
// //                   //                 },
// //                   //                 child: Padding(
// //                   //                   padding: const EdgeInsets.all(8.0),
// //                   //                   child: Icon(
// //                   //                     Icons.chevron_left_rounded,
// //                   //                     color: Colors.black,
// //                   //                     size: 30,
// //                   //                   ),
// //                   //                 )),
// //                   //           ),
// //                   //           const SizedBox(
// //                   //             width: 10,
// //                   //           ),
// //                   //           Text(
// //                   //             "Cart",
// //                   //             style: TextStyle(
// //                   //                 fontSize: 23,
// //                   //                 fontWeight: FontWeight.bold,
// //                   //                 color: Colors.black),
// //                   //           )
// //                   //         ],
// //                   //       ),
// //                   //     ],
// //                   //   ),
// //                   // ),
// //                   ProviderListener(
// //                     provider: orderBuyItemsNotifier,
// //                     onChange: (context, dynamic state) {
// //                       if (state.isLoading!) {
// //                         showProgress(context);
// //                       } else if (state.isError!) {
// //                         Navigator.pop(context);
// //                         showMessage(context, state.errorMessage!, true, true);
// //                       } else {
// //                         Navigator.pop(context);
// //                         checkPaymentAndEmitSocket(
// //                             responseData: state.response.responseData);
// //                         // emitSocket(
// //                         //     orderID: context
// //                         //         .read(orderBuyItemsNotifier.notifier)
// //                         //         .state
// //                         //         .response
// //                         //         .responseData!
// //                         //         .id);
// //                         context.read(cartNotifierProvider.notifier).getCartList(
// //                               lat: context
// //                                   .read(homeNotifierProvider.notifier)
// //                                   .currentLocation
// //                                   .latitude!
// //                                   .toDouble(),
// //                               lng: context
// //                                   .read(homeNotifierProvider.notifier)
// //                                   .currentLocation
// //                                   .longitude!
// //                                   .toDouble(),
// //                               context: context,
// //                             );
// //                       }
// //                     },
// //                     child: Column(
// //                       children: [
// //                         Consumer(builder: (context, watch, child) {
// //                           final state = watch(cartNotifierProvider);
// //                           if (state.isLoading) {
// //                             return RestaurantCartLoading();
// //                           } else if (state.isError) {
// //                             return LoadingError(
// //                               onPressed: (res) {
// //                                 reloadData(init: true, context: res);
// //                               },
// //                               message: state.errorMessage.toString(),
// //                             );
// //                           } else {
// //                             if (state.response != 0 && state.response != 1) {
// //                               cartListResponseData =
// //                                   state.response.responseData;
// //                               cartTileList = cartListResponseData!.carts;
// //                               updateDetails(
// //                                   responseData: cartListResponseData!);
// //
// //                               return Column(
// //                                 crossAxisAlignment: CrossAxisAlignment.start,
// //                                 children: [
// //                                   Padding(
// //                                     padding: const EdgeInsets.only(
// //                                         left: 25, top: 20),
// //                                     child: Text(
// //                                       "There are " +
// //                                           (totalCart == 0
// //                                               ? "no"
// //                                               : totalCart.toString()) +
// //                                           " item(s) in your cart",
// //                                       style: TextStyle(
// //                                           color: App24Colors.darkTextColor,
// //                                           fontWeight: FontWeight.bold,
// //                                           fontSize: 17),
// //                                     ),
// //                                   ),
// //                                   (totalCart == 0)
// //                                       ? Center(
// //                                           child: Column(
// //                                             crossAxisAlignment:
// //                                                 CrossAxisAlignment.center,
// //                                             mainAxisAlignment:
// //                                                 MainAxisAlignment.center,
// //                                             children: [
// //                                               Icon(
// //                                                 CupertinoIcons.cart_badge_minus,
// //                                                 size: 150,
// //                                                 color: Colors.grey,
// //                                               ),
// //                                               const SizedBox(
// //                                                 height: 20,
// //                                               ),
// //                                               Text("Your Cart is Empty.")
// //                                             ],
// //                                           ),
// //                                         )
// //                                       : Padding(
// //                                           padding: const EdgeInsets.symmetric(
// //                                               horizontal: 15),
// //                                           child: ListView.builder(
// //                                             itemBuilder: (context, index) =>
// //                                                 buildCartItems(index),
// //                                             padding: EdgeInsets.only(
// //                                               top: 5,
// //                                             ),
// //                                             itemCount: cartTileList!.length,
// //                                             shrinkWrap: true,
// //                                             physics:
// //                                                 NeverScrollableScrollPhysics(),
// //                                           ),
// //                                         )
// //                                 ],
// //                               );
// //                             } else {
// //                               return Container();
// //                             }
// //                           }
// //                         }),
// //                         Container(
// //                           // height: MediaQuery.of(context).size.height * 0.65,
// //                           child: Padding(
// //                             padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
// //                             child: Column(
// //                               mainAxisAlignment: MainAxisAlignment.spaceBetween,
// //                               crossAxisAlignment: CrossAxisAlignment.start,
// //                               children: [
// //                                 const SizedBox(
// //                                   height: 10,
// //                                 ),
// //                                 // Helpers().getDetailsRow("Sub Total", "AED542.00", context),
// //                                 ////Helpers().getDetailsRow("GST", "AED52.00", context),
// //                                 //Helpers().getDetailsRow("Delivery", "FREE", context),
// //                                 const SizedBox(
// //                                   height: 20,
// //                                 ),
// //                                 // TextField(
// //                                 //   controller: couponController,
// //                                 //   // maxLines: 1,
// //                                 //   decoration: InputDecoration(
// //                                 //     suffix: RichText(
// //                                 //       text: TextSpan(
// //                                 //           recognizer: TapGestureRecognizer()
// //                                 //             ..onTap = () {},
// //                                 //           text: "Apply",
// //                                 //           style: TextStyle(
// //                                 //               color: App24Colors
// //                                 //                   .greenOrderEssentialThemeColor)),
// //                                 //     ),
// //                                 //     contentPadding:
// //                                 //     EdgeInsets.symmetric(horizontal: 15),
// //                                 //     // suffix: TextButton(
// //                                 //     //   onPressed: (){},
// //                                 //     //   child: Text("Apply"),
// //                                 //     // ),
// //                                 //     hintText: "Enter coupon code here",
// //                                 //     hintStyle: TextStyle(
// //                                 //       fontSize: 13,
// //                                 //     ),
// //                                 //     enabledBorder: OutlineInputBorder(
// //                                 //         borderRadius: BorderRadius.circular(10),
// //                                 //         borderSide: BorderSide(
// //                                 //             color: Theme.of(context)
// //                                 //                 .primaryColor
// //                                 //                 .withOpacity(0.2))),
// //                                 //     focusedBorder: OutlineInputBorder(
// //                                 //         borderRadius: BorderRadius.circular(10),
// //                                 //         borderSide: BorderSide(
// //                                 //             color: Theme.of(context)
// //                                 //                 .primaryColor
// //                                 //                 .withOpacity(0.2))),
// //                                 //   ),
// //                                 // ),
// //                                 // Padding(
// //                                 //   padding: const EdgeInsets.fromLTRB(0, 5, 5, 0),
// //                                 //   child: Row(
// //                                 //     // crossAxisAlignment: CrossAxisAlignment.end,
// //                                 //     mainAxisAlignment: MainAxisAlignment.end,
// //                                 //     children: [
// //                                 //       RichText(
// //                                 //           text: TextSpan(
// //                                 //               text: "See all coupons",
// //                                 //               style: TextStyle(
// //                                 //                 color: Colors.blue,
// //                                 //                 fontSize: 11,
// //                                 //               ),
// //                                 //               recognizer: TapGestureRecognizer()
// //                                 //                 ..onTap = () async {
// //                                 //                   var selectedCoupon = await Helpers()
// //                                 //                       .getCommonBottomSheet(
// //                                 //                       context: context,
// //                                 //                       content: AddCouponBottomSheet(),
// //                                 //                       title: "Coupon Codes");
// //                                 //                   if (selectedCoupon != null) {
// //                                 //                     PromoCodesResponseData selectedPromoCode =
// //                                 //                         selectedCoupon;
// //                                 //                     setState(() {
// //                                 //                       couponController.text =
// //                                 //                       selectedPromoCode.promoCode!;
// //                                 //                     });
// //                                 //                   }
// //                                 //                 })),
// //                                 //     ],
// //                                 //   ),
// //                                 // ),
// //                                 const SizedBox(
// //                                   height: 10,
// //                                 ),
// //
// //                                 Center(
// //                                   child: Text(
// //                                     "Item(s) will be delivered to:",
// //                                     style: TextStyle(
// //                                         color: App24Colors.darkTextColor,
// //                                         fontWeight: FontWeight.bold,
// //                                         fontSize:
// //                                             MediaQuery.of(context).size.width /
// //                                                 25),
// //                                   ),
// //                                 ),
// //                                 const SizedBox(
// //                                   height: 10,
// //                                 ),
// //
// //                                 Consumer(builder: (context, watch, child) {
// //                                   final state = watch(cartNotifierProvider);
// //                                   if (state.isLoading) {
// //                                     return CartListLoading();
// //                                   } else if (state.isError) {
// //                                     return LoadingError(
// //                                       onPressed: (res) {
// //                                         reloadData(init: true, context: res);
// //                                       },
// //                                       message: state.errorMessage.toString(),
// //                                     );
// //                                   } else {
// //                                     if (state.response != 0 &&
// //                                         state.response != 1) {
// //                                       cartListResponseData =
// //                                           state.response.responseData;
// //
// //                                       return Column(
// //                                         children: [
// //                                           if (cartListResponseData!
// //                                                   .showEstimation ==
// //                                               1)
// //                                             // Column(
// //                                             //   children: [
// //                                             Row(
// //                                               children: [
// //                                                 Expanded(
// //                                                     child: Text(
// //                                                         "Address heredfasdfzsvzsd sdjfhksjdn kjdhsfkjahdf kjhd fkjhdskfjhk")),
// //                                                 ElevatedButton(
// //                                                   style: ElevatedButton.styleFrom(
// //                                                       primary: App24Colors
// //                                                           .redRestaurantThemeColor,
// //                                                       shape:
// //                                                           RoundedRectangleBorder(
// //                                                               borderRadius:
// //                                                                   BorderRadius
// //                                                                       .circular(
// //                                                                           20))),
// //                                                   onPressed: () {},
// //                                                   child: Text(
// //                                                     "Add",
// //                                                     style: TextStyle(
// //                                                         color: Colors.white,
// //                                                         fontSize: MediaQuery.of(
// //                                                                     context)
// //                                                                 .size
// //                                                                 .width /
// //                                                             30,
// //                                                         fontWeight:
// //                                                             FontWeight.w600),
// //                                                   ),
// //                                                   // color:
// //                                                   //     App24Colors.redRestaurantThemeColor,
// //                                                   // shape: RoundedRectangleBorder(
// //                                                   //     borderRadius:
// //                                                   //         BorderRadius.circular(20)),
// //                                                 ),
// //                                                 const SizedBox(
// //                                                   width: 10,
// //                                                 ),
// //                                                 ElevatedButton(
// //                                                   onPressed: () {},
// //                                                   style: ElevatedButton.styleFrom(
// //                                                       primary: App24Colors
// //                                                           .redRestaurantThemeColor,
// //                                                       shape:
// //                                                           RoundedRectangleBorder(
// //                                                               borderRadius:
// //                                                                   BorderRadius
// //                                                                       .circular(
// //                                                                           20))),
// //                                                   child: Text(
// //                                                     "Change",
// //                                                     style: TextStyle(
// //                                                         color: Colors.white,
// //                                                         fontSize: MediaQuery.of(
// //                                                                     context)
// //                                                                 .size
// //                                                                 .width /
// //                                                             30,
// //                                                         fontWeight:
// //                                                             FontWeight.w600),
// //                                                   ),
// //                                                   // color:
// //                                                   //     App24Colors.redRestaurantThemeColor,
// //                                                   // shape: RoundedRectangleBorder(
// //                                                   //     borderRadius:
// //                                                   //         BorderRadius.circular(20)),
// //                                                 )
// //                                               ],
// //                                             ),
// //
// //                                           //     const SizedBox(
// //                                           //       height: 10,
// //                                           //     ),
// //                                           // if(totalCart != 0)
// //                                           // priceDetails(
// //                                           //     title: "GST",
// //                                           //     value: "AED" +
// //                                           //         double.parse(
// //                                           //             cartListResponseData!
// //                                           //                 .shopGstAmount
// //                                           //                 .toString())
// //                                           //             .toStringAsFixed(2)),
// //                                           // if(totalCart != 0)
// //                                           // priceDetails(
// //                                           //     title: "Total ",
// //                                           //     value: "AED" +
// //                                           //         (double.parse(
// //                                           //             cartListResponseData!
// //                                           //                 .totalItemPrice
// //                                           //                 .toString()) +
// //                                           //             double.parse(
// //                                           //                 cartListResponseData!
// //                                           //                     .shopGstAmount
// //                                           //                     .toString()))
// //                                           //             .toStringAsFixed(2)),
// //                                           //     const SizedBox(
// //                                           //       height: 10,
// //                                           //     ),
// //                                           //    /* priceDetails(
// //                                           //         title: "Delivery",
// //                                           //         value: "AED" +
// //                                           //             cartListResponseData!
// //                                           //                 .deliveryCharges
// //                                           //                 .toString()),*/
// //                                           //     const SizedBox(
// //                                           //       height: 10,
// //                                           //     ),
// //                                           //     priceDetails(
// //                                           //         title: "Discount",
// //                                           //         value: "AED" +
// //                                           //             cartListResponseData!
// //                                           //                 .promocodeAmount
// //                                           //                 .toString())
// //                                           //   ],
// //                                           // ),
// //
// //                                           // if(cartListResponseData!.showEstimation == 1)
// //                                           Divider(),
// //
// //                                           if (cartListResponseData!
// //                                                   .showEstimation ==
// //                                               1)
// //                                             Helpers().getDetailsRow(
// //                                                 "Item Total",
// //                                                 "AED" +
// //                                                     double.parse(
// //                                                             cartListResponseData!
// //                                                                 .totalItemPrice
// //                                                                 .toString())
// //                                                         .toStringAsFixed(2),
// //                                                 context),
// //                                           if (cartListResponseData!
// //                                                   .showEstimation ==
// //                                               1)
// //                                             const SizedBox(height: 5),
// //                                           Helpers().getDetailsRow(
// //                                               "Delivery charge",
// //                                               "AED" +
// //                                                   double.parse(
// //                                                           cartListResponseData!
// //                                                               .deliveryCharges
// //                                                               .toString())
// //                                                       .toStringAsFixed(2),
// //                                               context),
// //                                           const SizedBox(
// //                                             height: 10,
// //                                           ),
// //
// //                                           if (cartListResponseData!
// //                                                   .showEstimation ==
// //                                               1)
// //                                             Form(
// //                                               key: _formKey,
// //                                               child: TextFormField(
// //                                                 controller: couponController,
// //                                                 validator: (String? arg) {
// //                                                   if (arg!.length > 0)
// //                                                     return 'Invalid Coupon Code.';
// //                                                   else if (arg.length == 0)
// //                                                     return "Please enter coupon code";
// //                                                 },
// //                                                 // maxLines: 1,
// //                                                 decoration: InputDecoration(
// //                                                   suffixIcon: MaterialButton(
// //                                                     onPressed: () {
// //                                                       _formKey.currentState!
// //                                                           .validate();
// //                                                     },
// //                                                     shape: RoundedRectangleBorder(
// //                                                         borderRadius:
// //                                                             BorderRadius.only(
// //                                                                 topRight: Radius
// //                                                                     .circular(
// //                                                                         10),
// //                                                                 bottomRight: Radius
// //                                                                     .circular(
// //                                                                         10))),
// //                                                     child: Text("Apply"),
// //                                                   ),
// //                                                   contentPadding:
// //                                                       EdgeInsets.only(
// //                                                           /*bottom: 20,*/
// //                                                           left: 20),
// //                                                   hintText:
// //                                                       "Enter coupon code here",
// //                                                   hintStyle: TextStyle(
// //                                                     fontSize: 13,
// //                                                   ),
// //                                                   focusedErrorBorder:
// //                                                       OutlineInputBorder(
// //                                                           borderRadius:
// //                                                               BorderRadius
// //                                                                   .circular(10),
// //                                                           borderSide: BorderSide(
// //                                                               color: App24Colors
// //                                                                   .redRestaurantThemeColor
// //                                                                   .withOpacity(
// //                                                                       0.2))),
// //                                                   errorBorder: OutlineInputBorder(
// //                                                       borderRadius:
// //                                                           BorderRadius.circular(
// //                                                               10),
// //                                                       borderSide: BorderSide(
// //                                                           color: App24Colors
// //                                                               .redRestaurantThemeColor
// //                                                               .withOpacity(
// //                                                                   0.2))),
// //                                                   enabledBorder: OutlineInputBorder(
// //                                                       borderRadius:
// //                                                           BorderRadius.circular(
// //                                                               10),
// //                                                       borderSide: BorderSide(
// //                                                           color: App24Colors
// //                                                               .greenOrderEssentialThemeColor
// //                                                               .withOpacity(
// //                                                                   0.2))),
// //                                                   focusedBorder: OutlineInputBorder(
// //                                                       borderRadius:
// //                                                           BorderRadius.circular(
// //                                                               10),
// //                                                       borderSide: BorderSide(
// //                                                           color: App24Colors
// //                                                               .greenOrderEssentialThemeColor
// //                                                               .withOpacity(
// //                                                                   0.2))),
// //                                                 ),
// //                                               ),
// //                                             ),
// //
// //                                           if (cartListResponseData!
// //                                                   .showEstimation ==
// //                                               1)
// //                                             Padding(
// //                                               padding:
// //                                                   const EdgeInsets.fromLTRB(
// //                                                       0, 10, 10, 0),
// //                                               child: Row(
// //                                                 mainAxisAlignment:
// //                                                     MainAxisAlignment.end,
// //                                                 children: [
// //                                                   RichText(
// //                                                       text: TextSpan(
// //                                                           text:
// //                                                               "See all coupons",
// //                                                           style: TextStyle(
// //                                                             color: Colors.blue,
// //                                                             fontSize: 11,
// //                                                           ),
// //                                                           recognizer:
// //                                                               TapGestureRecognizer()
// //                                                                 ..onTap =
// //                                                                     () async {
// //                                                                   print(widget
// //                                                                       .promoCode);
// //                                                                   var selectedCoupon = await Helpers()
// //                                                                       .getCommonBottomSheet(
// //                                                                           context:
// //                                                                               context,
// //                                                                           content:
// //                                                                               AddCouponBottomSheet(
// //                                                                             cartShopID:
// //                                                                                 cartListResponseData!.storeId,
// //                                                                             promoCode:
// //                                                                                 widget.promoCode,
// //                                                                             promoCodeDetails:
// //                                                                                 widget.promoCodeDetails,
// //                                                                           ),
// //                                                                           title:
// //                                                                               "Coupon Codes");
// //                                                                   if (selectedCoupon !=
// //                                                                       null) {
// //                                                                     PromoCodesResponseData
// //                                                                         selectedPromoCode =
// //                                                                         selectedCoupon;
// //                                                                     setState(
// //                                                                         () {
// //                                                                       couponController
// //                                                                               .text =
// //                                                                           selectedPromoCode
// //                                                                               .promoCode!;
// //                                                                     });
// //                                                                   }
// //                                                                 })),
// //                                                 ],
// //                                               ),
// //                                             ),
// //
// //                                           if (cartListResponseData!
// //                                                   .showEstimation ==
// //                                               1)
// //                                             if (cartListResponseData!
// //                                                 .promocode!.isNotEmpty)
// //                                               Row(
// //                                                 children: <Widget>[
// //                                                   /* Text(
// //                               "Promocode applied",
// //                               style: TextStyle(
// //                                   fontWeight: FontWeight.w600,
// //                                   fontSize: 14),
// //                             ),*/
// //
// //                                                   Expanded(
// //                                                     child: Text(
// //                                                       "\"" +
// //                                                           cartListResponseData!
// //                                                               .promocode
// //                                                               .toString()
// //                                                               .toUpperCase() +
// //                                                           "\" offer applied on this order",
// //                                                       textAlign: TextAlign.end,
// //                                                       style: TextStyle(
// //                                                           fontWeight:
// //                                                               FontWeight.w600,
// //                                                           color: App24Colors
// //                                                               .lightBlue,
// //                                                           fontSize: 14),
// //                                                     ),
// //                                                   ),
// //                                                 ],
// //                                               ),
// //
// //                                           if (cartListResponseData!
// //                                                   .showEstimation ==
// //                                               1)
// //                                             if (cartListResponseData!
// //                                                 .promocode!.isNotEmpty)
// //                                               Padding(
// //                                                 padding:
// //                                                     EdgeInsets.only(top: 5),
// //                                                 child: Row(
// //                                                   children: <Widget>[
// //                                                     /* Text(
// //                               "Promocode applied",
// //                               style: TextStyle(
// //                                   fontWeight: FontWeight.w600,
// //                                   fontSize: 14),
// //                             ),*/
// //
// //                                                     Expanded(
// //                                                       child: Text(
// //                                                         "AED" +
// //                                                             cartListResponseData!
// //                                                                 .promocodeAmount
// //                                                                 .toString() +
// //                                                             " discounted on your bill",
// //                                                         textAlign:
// //                                                             TextAlign.end,
// //                                                         style: TextStyle(
// //                                                             fontWeight:
// //                                                                 FontWeight.w600,
// //                                                             color: App24Colors
// //                                                                 .lightBlue,
// //                                                             fontSize: 14),
// //                                                       ),
// //                                                     ),
// //                                                   ],
// //                                                 ),
// //                                               ),
// //                                           const SizedBox(
// //                                             height: 10,
// //                                           ),
// //                                           if (cartListResponseData!
// //                                                       .showEstimation !=
// //                                                   1 &&
// //                                               cartListResponseData!
// //                                                   .carts!.isNotEmpty)
// //                                             Container(
// //                                               padding: EdgeInsets.all(20),
// //                                               decoration: BoxDecoration(
// //                                                 borderRadius:
// //                                                     BorderRadius.circular(20),
// //                                                 color: Color(0xffFFFABD),
// //                                               ),
// //                                               child: Text(
// //                                                 "Prices will be updated once the purchase is completed at the shop. You shall be alerted for the payment when the bill is ready.",
// //                                                 textAlign: TextAlign.center,
// //                                                 style: TextStyle(
// //                                                     color: Color(0xffB7791B),
// //                                                     fontWeight:
// //                                                         FontWeight.w800),
// //                                               ),
// //                                             ),
// //                                           const SizedBox(
// //                                             height: 10,
// //                                           ),
// //
// //                                           Row(
// //                                             // crossAxisAlignment: CrossAxisAlignment.stretch,
// //                                             mainAxisAlignment:
// //                                                 MainAxisAlignment.spaceBetween,
// //                                             children: [
// //                                               // Text(
// //                                               //   "Use wallet amount",
// //                                               //   style: TextStyle(
// //                                               //       fontSize: 14,
// //                                               //       fontWeight: FontWeight.bold),
// //                                               // ),
// //                                               // Text(
// //                                               //   "( AED" +
// //                                               //       cartListResponseData!
// //                                               //           .userWalletBalance
// //                                               //           .toString() +
// //                                               //       ")",
// //                                               //   style: TextStyle(
// //                                               //       fontSize: 14,
// //                                               //       fontWeight: FontWeight.bold,
// //                                               //       color: App24Colors
// //                                               //           .greenOrderEssentialThemeColor),
// //                                               // ),
// //                                               // Checkbox(
// //                                               //   shape: RoundedRectangleBorder(
// //                                               //       borderRadius:
// //                                               //       BorderRadius.circular(15)),
// //                                               //   checkColor: Colors.white,
// //                                               //   activeColor:
// //                                               //   App24Colors.redRestaurantThemeColor,
// //                                               //   value: this.use_wallet_checkout,
// //                                               //   onChanged: (bool? value) {
// //                                               //     setState(() {
// //                                               //       this.use_wallet_checkout = value!;
// //                                               //     });
// //                                               //   },
// //                                               // ),
// //                                               // const SizedBox(
// //                                               //   width: 70,
// //                                               // ),
// //                                             ],
// //                                           ),
// //                                           // const SizedBox(
// //                                           //   height: 30,
// //                                           // ),
// //                                           // if (cartListResponseData!
// //                                           //         .showEstimation ==
// //                                           //     1)
// //                                           // Row(
// //                                           //   mainAxisAlignment:
// //                                           //       MainAxisAlignment.spaceBetween,
// //                                           //   children: [
// //                                           //     Text(
// //                                           //       "Total Payable Amount",
// //                                           //       style: TextStyle(
// //                                           //           fontSize: 19,
// //                                           //           fontWeight: FontWeight.bold,
// //                                           //           color: Color(0xff474747)),
// //                                           //     ),
// //                                           //     Text(
// //                                           //         "AED" +
// //                                           //             cartListResponseData!
// //                                           //                 .totalPrice
// //                                           //                 .toString(),
// //                                           //         style: TextStyle(
// //                                           //             fontSize: 19,
// //                                           //             fontWeight:
// //                                           //                 FontWeight.bold,
// //                                           //             color: Color(0xff5E7792)))
// //                                           //   ],
// //                                           // ),
// //                                           const SizedBox(
// //                                             height: 10,
// //                                           ),
// //                                         ],
// //                                       );
// //                                     } else {
// //                                       return Center(
// //                                         child: Column(
// //                                           mainAxisAlignment:
// //                                               MainAxisAlignment.end,
// //                                           crossAxisAlignment:
// //                                               CrossAxisAlignment.stretch,
// //                                           children: [
// //                                             Center(
// //                                               child: Column(
// //                                                 children: [
// //                                                   const Text(
// //                                                     "Drop Location too far",
// //                                                     style: TextStyle(
// //                                                         fontWeight:
// //                                                             FontWeight.w800),
// //                                                   ),
// //                                                   const SizedBox(
// //                                                     height: 10,
// //                                                   ),
// //                                                   const Text(
// //                                                     "Please select a nearby drop point",
// //                                                     style: TextStyle(
// //                                                         fontWeight:
// //                                                             FontWeight.w600),
// //                                                   ),
// //                                                 ],
// //                                               ),
// //                                             ),
// //                                             const SizedBox(
// //                                               height: 10,
// //                                             ),
// //                                             new MaterialButton(
// //                                               padding: EdgeInsets.symmetric(
// //                                                   vertical: 15),
// //                                               color: App24Colors
// //                                                   .redRestaurantThemeColor,
// //                                               shape: RoundedRectangleBorder(
// //                                                   borderRadius:
// //                                                       BorderRadius.circular(
// //                                                           15)),
// //                                               child: const Text(
// //                                                 "Change location",
// //                                                 style: TextStyle(
// //                                                     color: Colors.white),
// //                                               ),
// //                                               onPressed: () {
// //                                                 // Navigator.pop(context);
// //                                                 selectLocation(GlobalConstants
// //                                                     .labelDropLocation);
// //
// //                                                 // showMessage(
// //                                                 //     context, "Please change your location", true, false);
// //                                               }
// //                                               // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomePage()));}
// //                                               /*  Navigator.push(context,
// //                               MaterialPageRoute(builder: (context) => LogInPage()))*/
// //                                               ,
// //                                             ),
// //                                             const SizedBox(
// //                                               height: 10,
// //                                             ),
// //                                             new MaterialButton(
// //                                               padding: EdgeInsets.symmetric(
// //                                                   vertical: 15),
// //                                               color: App24Colors
// //                                                   .redRestaurantThemeColor,
// //                                               shape: RoundedRectangleBorder(
// //                                                   borderRadius:
// //                                                       BorderRadius.circular(
// //                                                           15)),
// //                                               child: const Text(
// //                                                 "Clear cart",
// //                                                 style: TextStyle(
// //                                                     color: Colors.white),
// //                                               ),
// //                                               onPressed: () {
// //                                                 // Navigator.pop(context);
// //
// //                                                 onClearCart();
// //                                               }
// //                                               /*  Navigator.push(context,
// //                               MaterialPageRoute(builder: (context) => LogInPage()))*/
// //                                               ,
// //                                             )
// //                                           ],
// //                                         ),
// //                                       );
// //                                     }
// //                                   }
// //                                 }),
// //                                 // ElevatedButton(
// //                                 //   shape: RoundedRectangleBorder(
// //                                 //     borderRadius: BorderRadius.circular(30.0),
// //                                 //   ),
// //                                 //   padding: EdgeInsets.symmetric(horizontal: 69, vertical: 10),
// //                                 //   onPressed: () {
// //                                 //     Helpers().getCommonBottomSheet(context, UpiBottomSheet());
// //                                 //   },
// //                                 //   color: App24Colors.greenOrderEssentialThemeColor,
// //                                 //   textColor: Colors.white,
// //                                 //   child: Text(
// //                                 //     "Confirm & Continue",
// //                                 //     style: TextStyle(fontSize: 15),
// //                                 //   ),
// //                                 // ),
// //                               ],
// //                             ),
// //                           ),
// //                         ),
// //                       ],
// //                     ),
// //                   ),
// //                 ],
// //               ),
// //             ),
// //           ),
// //         ),
// //       ),
// //     );
// //   }
// //
// //   void updatePriority(AddressResponseData confirmedLocation) async {
// //     AddressDatabase addressDatabase =
// //         new AddressDatabase(DatabaseService.instance);
// //     int i = await addressDatabase.updatePriority(confirmedLocation);
// //     print("updated rows ::: " + i.toString());
// //   }
// // }
