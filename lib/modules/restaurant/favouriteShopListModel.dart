FavouriteShopList favouriteShopListModelFromJson(str) {
  return FavouriteShopList.fromJson(str);
}


class FavouriteShopList {
  String? statusCode;
  String? title;
  String? message;
  List<FavouriteResponseData>? responseData;
  List<dynamic>? responseNewData;
  List<dynamic>? error;

  FavouriteShopList(
      {this.statusCode,
        this.title,
        this.message,
        this.responseData,
        this.responseNewData,
        this.error});

  FavouriteShopList.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    if (json['responseData'] != null) {
      responseData = [];
      json['responseData'].forEach((v) {
        responseData!.add(new FavouriteResponseData.fromJson(v));
      });
    }
    if (json['responseNewData'] != null) {
      responseNewData = [];
      json['responseNewData'].forEach((v) {
        responseNewData!.add(v);
      });
    }
    if (json['error'] != null) {
      error = [];
      json['error'].forEach((v) {
        error!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['title'] = this.title;
    data['message'] = this.message;
    if (this.responseData != null) {
      data['responseData'] = this.responseData!.map((v) => v.toJson()).toList();
    }
    if (this.responseNewData != null) {
      data['responseNewData'] =
          this.responseNewData!.map((v) => v.toJson()).toList();
    }
    if (this.error != null) {
      data['error'] = this.error!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class FavouriteResponseData {
  int? storeId;
  String? storeName;
  String? contactNumber;

  FavouriteResponseData({this.storeId, this.storeName, this.contactNumber});

  FavouriteResponseData.fromJson(Map<String, dynamic> json) {
    storeId = json['store_id'];
    storeName = json['store_name'];
    contactNumber = json['contact_number'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['store_id'] = this.storeId;
    data['store_name'] = this.storeName;
    data['contact_number'] = this.contactNumber;
    return data;
  }
}
