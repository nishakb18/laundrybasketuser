import 'dart:async';

import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class RestaurantItemLoading extends StatefulWidget {
  @override
  _RestaurantItemLoadingState createState() => _RestaurantItemLoadingState();
}

class _RestaurantItemLoadingState extends State<RestaurantItemLoading> {
  Timer? _timer;
  int _start = 10; // time to popup still working window /// in seconds

  @override
  void initState() {
    startTimer();
    super.initState();
  }

  @override
  void dispose() {
    if (_timer != null) {
      _timer!.cancel();
    }
    super.dispose();
  }

  void startTimer() {
    if (_timer != null) {
      _timer!.cancel();
      _timer = null;
    } else {
      _timer = new Timer.periodic(const Duration(seconds: 1), (Timer timer) {
        setState(() {
          if (_start < 1) {
            _timer!.cancel();
          } else {
            _start = _start - 1;
          }
        });
      });
    }
  }

  buildRestaurantItemShimmerItem(context) {
    List<Widget> list = [];

    for (var i = 0; i < 10; i++)
      list.add(Container(
        width: MediaQuery.of(context).size.width / 2.5,
        margin: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: MediaQuery.of(context).size.width / 4,
              height: MediaQuery.of(context).size.height / 7,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(25),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xffefefef),
                      blurRadius: 20,
                      spreadRadius: 3,
                    ),
                  ]),
            ),
            const SizedBox(
              width: 15,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 15,
                  width: MediaQuery.of(context).size.width / 3,
                  margin: EdgeInsets.only(right: 30, top: 15),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Color(0xffefefef),
                          blurRadius: 20,
                          spreadRadius: 3,
                        ),
                      ]),
                ),
                Container(
                  height: 10,
                  margin: EdgeInsets.only(top: 10),
                  width: 70,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Color(0xffefefef),
                          blurRadius: 20,
                          spreadRadius: 3,
                        ),
                      ]),
                ),
                Container(
                  height: 10,
                  margin: EdgeInsets.only(top: 5),
                  width: 70,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Color(0xffefefef),
                          blurRadius: 20,
                          spreadRadius: 3,
                        ),
                      ]),
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  height: 20,
                  margin: EdgeInsets.only(top: 5),
                  width: 30,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Color(0xffefefef),
                          blurRadius: 20,
                          spreadRadius: 3,
                        ),
                      ]),
                )
              ],
            ),
          ],
        ),
      ));

    return list;
  }

  buildStillLoadingWidget() {
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(15)),
      width: MediaQuery.of(context).size.width,
      child: ListTile(
        title: Text("Please wait..."),
        subtitle: Text(
            "The network seems to be too busy. We suggest you to wait for some more time."),
        leading: CircularProgressIndicator.adaptive(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height,
        child: Stack(
          fit: StackFit.expand,
          children: [
            Shimmer.fromColors(
                baseColor: Colors.grey[300]!,
                highlightColor: Colors.grey[100]!,
                child: ListView(
                    shrinkWrap: true,
                    children: buildRestaurantItemShimmerItem(context))),
            Positioned(
              left: 20,
              right: 20,
              top: 120,
              child: _start == 0 ? buildStillLoadingWidget() : Container(),
            )
          ],
        ));
  }
}
