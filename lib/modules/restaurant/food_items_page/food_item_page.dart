import 'dart:ui';

import 'package:app24_user_app/app24_user_icons.dart';
import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/constants/shared_preferences_path.dart';
import 'package:app24_user_app/helpers.dart';
import 'package:app24_user_app/modules/delivery/cart/cart_model.dart';
import 'package:app24_user_app/modules/delivery/item_list/item_list_category_loading.dart';
import 'package:app24_user_app/modules/delivery/item_list/item_list_model.dart';
import 'package:app24_user_app/modules/delivery/item_list/shop_details_loading.dart';
import 'package:app24_user_app/modules/restaurant/food_items_page/food_item_page_loading.dart';
import 'package:app24_user_app/modules/restaurant/restaurant_1st_page/order_food_page.dart';
import 'package:app24_user_app/modules/restaurant/restaurant_cart_page.dart';
import 'package:app24_user_app/modules/restaurant/restaurant_shops/shop_image_top_loading.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/widgets/alertboxs/cart_replace_alert_box.dart';
import 'package:app24_user_app/widgets/bottomsheets/addons_bottomsheet.dart';
import 'package:app24_user_app/widgets/common_appbar.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:app24_user_app/widgets/offline_builder_widget.dart';
import 'package:app24_user_app/widgets/view_cart_overlay.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:favorite_button/favorite_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

// import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FoodItemsPage extends StatefulWidget {
  final String? shopId;
  final String? restaurantName;
  final String? distance;
  final String? cuisinesTypes;
  final String? shopRating;
  final String? shopPic;
  final String? shopLocation;
  final String? estimateTime;

  const FoodItemsPage(
      {Key? key,
      this.shopId,
      this.restaurantName,
      this.distance,
      this.cuisinesTypes,
      this.shopRating, this.shopPic, this.shopLocation, this.estimateTime})
      : super(key: key);

  @override
  _FoodItemsPageState createState() => _FoodItemsPageState();
}

class _FoodItemsPageState extends State<FoodItemsPage> {
  var rating = 3.5;
  var top = 0.0;
  bool isFavourite = false;
  int? favouriteValue;
  bool status = false;
  double sliverAppbarHeight = 0.0;
  double collapsedHeight = 0.0;
  String? _sortByValue;
  List _sortBy = ["Relevance", "Low-High", "High-Low", "Popular"];
  List<Products>? restaurantItemList = new List.empty(growable: true);
  List<Products> restaurantItemUnSortedList = new List.empty(growable: true);
  bool addOns = false;
  List<Carts>? cartTileList = new List.empty(growable: true);
  String? searchText = '';
  String itemCategory = "all";
  int? categoryID;

  // List<Itemsaddon> addOnsList = [];

  // List<FoodItemsTile> foodItemsTile = [];

  reloadData({required BuildContext context, bool? init}) {
    return context
        .read(itemListNotifierProvider.notifier)
        .getItemList(storeID: widget.shopId.toString());
  }

  @override
  void initState() {
    SchedulerBinding.instance!.addPostFrameCallback((_) async {
      var prefs = await SharedPreferences.getInstance();
      var favouriteShopId =
          prefs.getStringList(SharedPreferencesPath.favouriteRestaurants);
      context
          .read(cartCountNotifierProvider.notifier)
          .getCartCount(context: context);

      //API call after screen build
      // context.read(homeNotifierProvider).getOffers(context: context);
      context
          .read(itemListNotifierProvider.notifier)
          .getItemList(storeID: widget.shopId!);
      // cartIdList = context.read(cartNotifierProvider.notifier).cartIdList!;
      if (favouriteShopId != null) if (favouriteShopId
          .contains(widget.shopId)) {
        isFavourite = true;
      }
    });

    super.initState();
  }

  List<Widget> getCategories(context, ItemListResponseData forCategories) {
    List<AllCategories> category = forCategories.allCategories!;
    List<Widget> _items = new List<Widget>.empty(growable: true);
    for (var i = 0; i < category.length; i++) {
      _items.add(Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 2),
        child: Container(
            padding: EdgeInsets.symmetric(horizontal: 25),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25),
                color: itemCategory == category[i].storeCategoryName!
                    ? App24Colors.greenOrderEssentialThemeColor
                    : Color(0xffF6F6F6)),
            child: Material(
                color: Colors.transparent,
                child: InkWell(
                  borderRadius: BorderRadius.circular(25),
                  onTap: () {
                    setState(() {
                      categoryID = category[i].id;
                      showLog("categoryID" + categoryID.toString());
                      // onCategory(category[i].id);
                      itemCategory = category[i].storeCategoryName!;
                    });
                  },
                  child: Center(
                    child: Text(
                      category[i].storeCategoryName!,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          color: itemCategory == category[i].storeCategoryName!
                              ? Colors.white
                              : App24Colors.darkTextColor),
                    ),
                  ),
                ))),
      ));
    }
    return _items;
  }

  onFavouriteClicked(favourite) {
    Map body = {
      "store_id": widget.shopId,
      "isfavourite": favourite == true ? 1 : 0
    };
    print(widget.shopId);
    context.read(favouriteShopNotifier.notifier).getFavouriteShop(body: body);
  }

  List<Widget> getFoodTiles(context, ItemListResponseData shopName) {
    List<Widget> _widgetFood = [];
    // ItemListResponseData shopName;
    if (status == true) {
      _widgetFood.clear();
      for (var i = 0; i < restaurantItemList!.length; i++) {
        if (restaurantItemList![i].isVeg == 'Pure Veg' &&
            restaurantItemList![i]
                .itemName!
                .toLowerCase()
                .contains(searchText!.toLowerCase())) if (categoryID == 0 ||
            categoryID == null) {
          _widgetFood.add(Padding(
            padding: const EdgeInsets.only(left: 1, top: 0),
            child: Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
              // width: MediaQuery
              //     .of(context)
              //     .size
              //     .width / 2.3,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
              ),
              child: Container(
                // padding: EdgeInsets.symmetric(vertical: 5),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        offset: const Offset(0, 3.0),
                        color: Color(0xfff0f0f0),
                        blurRadius: 10.0,
                        spreadRadius: 3.0,
                      )
                    ]),
                child: Row(
                  // crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(25.0),
                        child: CachedNetworkImage(
                          imageUrl: restaurantItemList![i].picture ?? '',
                          width: MediaQuery.of(context).size.width / 4,
                          height: MediaQuery.of(context).size.height / 9,
                          fit: BoxFit.fill,
                          placeholder: (context, url) => Image.asset(
                            App24UserAppImages.placeHolderImage,
                            width: MediaQuery.of(context).size.width / 4,
                            height: MediaQuery.of(context).size.height / 7,
                            fit: BoxFit.contain,
                          ),
                          errorWidget: (context, url, error) => Image.asset(
                            App24UserAppImages.placeHolderImage,
                            width: MediaQuery.of(context).size.width / 4,
                            height: MediaQuery.of(context).size.height / 7,
                            fit: BoxFit.contain,
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(top: 10),
                              child: Row(
                                // mainAxisAlignment: MainAxisAlignment.center,
                                // crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: Text(
                                      restaurantItemList![i].itemName ?? "",
                                      style: TextStyle(
                                          color:
                                              App24Colors.shopAndItemNameColor,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16),
                                      // textAlign: TextAlign.center,
                                      // maxLines: 2,
                                      overflow: TextOverflow.clip,
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 5,
                                  ),
                                  Padding(
                                      padding: const EdgeInsets.only(
                                          right: 15, top: 5),
                                      child: Row(
                                        children: [
                                          Icon(
                                            Icons.star,
                                            color: Colors.amber,
                                            size: 15,
                                          ),
                                          const SizedBox(
                                            width: 5,
                                          ),
                                          Text(
                                            restaurantItemList![i]
                                                .avgRating
                                                .toString(),
                                            style: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 10),
                                          )
                                        ],
                                      ))
                                ],
                              ),
                            ),

                            Text(
                              restaurantItemList![i].itemDescription ?? "",
                              style: TextStyle(
                                color: Color(0xffFF1616).withOpacity(0.62),
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Text(
                              restaurantItemList![i].offer.toString(),
                              overflow: TextOverflow.clip,
                              style: TextStyle(
                                  color: Color(0xff9B9B9B),
                                  fontSize: 9,
                                  fontWeight: FontWeight.w600),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  "AED" +
                                      restaurantItemList![i]
                                          .itemPrice
                                          .toString(),
                                  style: TextStyle(
                                      color: Color(0xffFF6F6F),
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20),
                                  overflow: TextOverflow.ellipsis,
                                ),
                                Padding(
                                    padding: const EdgeInsets.only(
                                        top: 5, right: 10),
                                    child: MaterialButton(
                                      onPressed: () {
                                        Helpers().getCommonBottomSheet(
                                            context: context,
                                            content: AddonsBottomSheet(
                                              itemID: restaurantItemList![i]
                                                  .id
                                                  .toString(),
                                              product: restaurantItemList![i],
                                              shopName:
                                                  shopName.storeName.toString(),
                                              context: context,
                                              shopID: widget.shopId,
                                            ),
                                            title: "Extras");
                                      },
                                      color:
                                          App24Colors.redRestaurantThemeColor,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(15)),
                                      child: Text(
                                        "Add",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w600,
                                            fontSize: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                27),
                                      ),
                                    )),
                              ],
                            )

                            // Row(
                            //   children: [
                            //     Expanded(
                            //       child: Padding(
                            //         padding: const EdgeInsets.symmetric(horizontal: 10),
                            //         child: Text(
                            //           foodItemsTile[i].foodEstimateTime,
                            //           style: TextStyle(
                            //               fontSize: MediaQuery.of(context).size.width/25,
                            //               fontWeight: FontWeight.w600,
                            //               color: Theme.of(context).primaryColor),
                            //         ),
                            //       ),
                            //     ),
                            //   ],
                            // )
                          ]),
                    ),
                  ],
                ),
              ),
            ),
          ));
        } else {
          if (restaurantItemList![i].storeCategoryId == categoryID)
            _widgetFood.add(Padding(
              padding: const EdgeInsets.only(left: 1, top: 0),
              child: Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                // width: MediaQuery
                //     .of(context)
                //     .size
                //     .width / 2.3,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Container(
                  // padding: EdgeInsets.symmetric(vertical: 5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          offset: const Offset(0, 3.0),
                          color: Color(0xfff0f0f0),
                          blurRadius: 10.0,
                          spreadRadius: 3.0,
                        )
                      ]),
                  child: Row(
                    // crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(25.0),
                          child: CachedNetworkImage(
                            imageUrl: restaurantItemList![i].picture ?? '',
                            width: MediaQuery.of(context).size.width / 4,
                            height: MediaQuery.of(context).size.height / 9,
                            fit: BoxFit.fill,
                            placeholder: (context, url) => Image.asset(
                              App24UserAppImages.placeHolderImage,
                              width: MediaQuery.of(context).size.width / 4,
                              height: MediaQuery.of(context).size.height / 7,
                              fit: BoxFit.contain,
                            ),
                            errorWidget: (context, url, error) => Image.asset(
                              App24UserAppImages.placeHolderImage,
                              width: MediaQuery.of(context).size.width / 4,
                              height: MediaQuery.of(context).size.height / 7,
                              fit: BoxFit.contain,
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 10),
                                child: Row(
                                  // mainAxisAlignment: MainAxisAlignment.center,
                                  // crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      child: Text(
                                        restaurantItemList![i].itemName ?? "",
                                        style: TextStyle(
                                            color: App24Colors
                                                .shopAndItemNameColor,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16),
                                        // textAlign: TextAlign.center,
                                        // maxLines: 2,
                                        overflow: TextOverflow.clip,
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Padding(
                                        padding: const EdgeInsets.only(
                                            right: 15, top: 5),
                                        child: Row(
                                          children: [
                                            Icon(
                                              Icons.star,
                                              color: Colors.amber,
                                              size: 15,
                                            ),
                                            const SizedBox(
                                              width: 5,
                                            ),
                                            Text(
                                              restaurantItemList![i]
                                                  .avgRating
                                                  .toString(),
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 10),
                                            )
                                          ],
                                        ))
                                  ],
                                ),
                              ),

                              Text(
                                restaurantItemList![i].itemDescription ?? "",
                                style: TextStyle(
                                  color: Color(0xffFF1616).withOpacity(0.62),
                                  fontSize: 10,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Text(
                                restaurantItemList![i].offer.toString(),
                                overflow: TextOverflow.clip,
                                style: TextStyle(
                                    color: Color(0xff9B9B9B),
                                    fontSize: 9,
                                    fontWeight: FontWeight.w600),
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    "AED" +
                                        restaurantItemList![i]
                                            .itemPrice
                                            .toString(),
                                    style: TextStyle(
                                        color: Color(0xffFF6F6F),
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                  Padding(
                                      padding: const EdgeInsets.only(
                                          top: 5, right: 10),
                                      child: MaterialButton(
                                        onPressed: () {
                                          Helpers().getCommonBottomSheet(
                                              context: context,
                                              content: AddonsBottomSheet(
                                                itemID: restaurantItemList![i]
                                                    .id
                                                    .toString(),
                                                product: restaurantItemList![i],
                                                shopName: shopName.storeName
                                                    .toString(),
                                                context: context,
                                                shopID: widget.shopId,
                                              ),
                                              title: "Extras");
                                        },
                                        color:
                                            App24Colors.redRestaurantThemeColor,
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(15)),
                                        child: Text(
                                          "Add",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.w600,
                                              fontSize: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  27),
                                        ),
                                      )),
                                ],
                              )

                              // Row(
                              //   children: [
                              //     Expanded(
                              //       child: Padding(
                              //         padding: const EdgeInsets.symmetric(horizontal: 10),
                              //         child: Text(
                              //           foodItemsTile[i].foodEstimateTime,
                              //           style: TextStyle(
                              //               fontSize: MediaQuery.of(context).size.width/25,
                              //               fontWeight: FontWeight.w600,
                              //               color: Theme.of(context).primaryColor),
                              //         ),
                              //       ),
                              //     ),
                              //   ],
                              // )
                            ]),
                      ),
                    ],
                  ),
                ),
              ),
            ));
        }
      }
    } else {
      _widgetFood.clear();
      for (var i = 0; i < restaurantItemList!.length; i++) {
        if (restaurantItemList![i]
            .itemName!
            .toLowerCase()
            .contains(searchText!.toLowerCase())) if (categoryID ==
                0 ||
            categoryID == null) {
          _widgetFood.add(Padding(
            padding: const EdgeInsets.only(left: 1, top: 0),
            child: Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
              // width: MediaQuery
              //     .of(context)
              //     .size
              //     .width / 2.3,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
              ),
              child: Container(
                // padding: EdgeInsets.symmetric(vertical: 5),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        offset: const Offset(0, 3.0),
                        color: Color(0xfff0f0f0),
                        blurRadius: 10.0,
                        spreadRadius: 3.0,
                      )
                    ]),
                child: Row(
                  // crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(25.0),
                        child: CachedNetworkImage(
                          imageUrl: restaurantItemList![i].picture ?? '',
                          width: MediaQuery.of(context).size.width / 4,
                          height: MediaQuery.of(context).size.height / 9,
                          fit: BoxFit.fill,
                          placeholder: (context, url) => Image.asset(
                            App24UserAppImages.placeHolderImage,
                            width: MediaQuery.of(context).size.width / 4,
                            height: MediaQuery.of(context).size.height / 7,
                            fit: BoxFit.contain,
                          ),
                          errorWidget: (context, url, error) => Image.asset(
                            App24UserAppImages.placeHolderImage,
                            width: MediaQuery.of(context).size.width / 4,
                            height: MediaQuery.of(context).size.height / 7,
                            fit: BoxFit.contain,
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(top: 10),
                              child: Row(
                                // mainAxisAlignment: MainAxisAlignment.center,
                                // crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: Text(
                                      restaurantItemList![i].itemName ?? "",
                                      style: TextStyle(
                                          color:
                                              App24Colors.shopAndItemNameColor,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16),
                                      // textAlign: TextAlign.center,
                                      // maxLines: 2,
                                      overflow: TextOverflow.clip,
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 5,
                                  ),
                                  Padding(
                                      padding: const EdgeInsets.only(
                                          right: 15, top: 5),
                                      child: Row(
                                        children: [
                                          Icon(
                                            Icons.star,
                                            color: Colors.amber,
                                            size: 15,
                                          ),
                                          const SizedBox(
                                            width: 5,
                                          ),
                                          Text(
                                            restaurantItemList![i]
                                                .avgRating
                                                .toString(),
                                            style: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 10),
                                          )
                                        ],
                                      ))
                                ],
                              ),
                            ),

                            Text(
                              restaurantItemList![i].itemDescription ?? "",
                              style: TextStyle(
                                color: Color(0xffFF1616).withOpacity(0.62),
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Text(
                              restaurantItemList![i].offer.toString(),
                              overflow: TextOverflow.clip,
                              style: TextStyle(
                                  color: Color(0xff9B9B9B),
                                  fontSize: 9,
                                  fontWeight: FontWeight.w600),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  "AED" +
                                      restaurantItemList![i]
                                          .itemPrice
                                          .toString(),
                                  style: TextStyle(
                                      color: Color(0xffFF6F6F),
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20),
                                  overflow: TextOverflow.ellipsis,
                                ),
                                Padding(
                                    padding: const EdgeInsets.only(
                                        top: 5, right: 10),
                                    child: MaterialButton(
                                      onPressed: () {
                                        Helpers().getCommonBottomSheet(
                                            context: context,
                                            content: AddonsBottomSheet(
                                              itemID: restaurantItemList![i]
                                                  .id
                                                  .toString(),
                                              product: restaurantItemList![i],
                                              shopName:
                                                  shopName.storeName.toString(),
                                              context: context,
                                              shopID: widget.shopId,
                                            ),
                                            title: "Extras");
                                      },
                                      color:
                                          App24Colors.redRestaurantThemeColor,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(15)),
                                      child: Text(
                                        "Add",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w600,
                                            fontSize: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                27),
                                      ),
                                    )),
                              ],
                            )

                            // Row(
                            //   children: [
                            //     Expanded(
                            //       child: Padding(
                            //         padding: const EdgeInsets.symmetric(horizontal: 10),
                            //         child: Text(
                            //           foodItemsTile[i].foodEstimateTime,
                            //           style: TextStyle(
                            //               fontSize: MediaQuery.of(context).size.width/25,
                            //               fontWeight: FontWeight.w600,
                            //               color: Theme.of(context).primaryColor),
                            //         ),
                            //       ),
                            //     ),
                            //   ],
                            // )
                          ]),
                    ),
                  ],
                ),
              ),
            ),
          ));
        } else {
          if (restaurantItemList![i].storeCategoryId == categoryID)
            _widgetFood.add(Padding(
              padding: const EdgeInsets.only(left: 1, top: 0),
              child: Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                // width: MediaQuery
                //     .of(context)
                //     .size
                //     .width / 2.3,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Container(
                  // padding: EdgeInsets.symmetric(vertical: 5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          offset: const Offset(0, 3.0),
                          color: Color(0xfff0f0f0),
                          blurRadius: 10.0,
                          spreadRadius: 3.0,
                        )
                      ]),
                  child: Row(
                    // crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(25.0),
                          child: CachedNetworkImage(
                            imageUrl: restaurantItemList![i].picture ?? '',
                            width: MediaQuery.of(context).size.width / 4,
                            height: MediaQuery.of(context).size.height / 9,
                            fit: BoxFit.fill,
                            placeholder: (context, url) => Image.asset(
                              App24UserAppImages.placeHolderImage,
                              width: MediaQuery.of(context).size.width / 4,
                              height: MediaQuery.of(context).size.height / 7,
                              fit: BoxFit.contain,
                            ),
                            errorWidget: (context, url, error) => Image.asset(
                              App24UserAppImages.placeHolderImage,
                              width: MediaQuery.of(context).size.width / 4,
                              height: MediaQuery.of(context).size.height / 7,
                              fit: BoxFit.contain,
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 10),
                                child: Row(
                                  // mainAxisAlignment: MainAxisAlignment.center,
                                  // crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      child: Text(
                                        restaurantItemList![i].itemName ?? "",
                                        style: TextStyle(
                                            color: App24Colors
                                                .shopAndItemNameColor,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16),
                                        // textAlign: TextAlign.center,
                                        // maxLines: 2,
                                        overflow: TextOverflow.clip,
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Padding(
                                        padding: const EdgeInsets.only(
                                            right: 15, top: 5),
                                        child: Row(
                                          children: [
                                            Icon(
                                              Icons.star,
                                              color: Colors.amber,
                                              size: 15,
                                            ),
                                            const SizedBox(
                                              width: 5,
                                            ),
                                            Text(
                                              restaurantItemList![i]
                                                  .avgRating
                                                  .toString(),
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 10),
                                            )
                                          ],
                                        ))
                                  ],
                                ),
                              ),

                              Text(
                                restaurantItemList![i].itemDescription ?? "",
                                style: TextStyle(
                                  color: Color(0xffFF1616).withOpacity(0.62),
                                  fontSize: 10,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Text(
                                restaurantItemList![i].offer.toString(),
                                overflow: TextOverflow.clip,
                                style: TextStyle(
                                    color: Color(0xff9B9B9B),
                                    fontSize: 9,
                                    fontWeight: FontWeight.w600),
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    "AED" +
                                        restaurantItemList![i]
                                            .itemPrice
                                            .toString(),
                                    style: TextStyle(
                                        color: Color(0xffFF6F6F),
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                  Padding(
                                      padding: const EdgeInsets.only(
                                          top: 5, right: 10),
                                      child: MaterialButton(
                                        onPressed: () {
                                          Helpers().getCommonBottomSheet(
                                              context: context,
                                              content: AddonsBottomSheet(
                                                itemID: restaurantItemList![i]
                                                    .id
                                                    .toString(),
                                                product: restaurantItemList![i],
                                                shopName: shopName.storeName
                                                    .toString(),
                                                context: context,
                                                shopID: widget.shopId,
                                              ),
                                              title: "Extras");
                                        },
                                        color:
                                            App24Colors.redRestaurantThemeColor,
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(15)),
                                        child: Text(
                                          "Add",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.w600,
                                              fontSize: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  27),
                                        ),
                                      )),
                                ],
                              )

                              // Row(
                              //   children: [
                              //     Expanded(
                              //       child: Padding(
                              //         padding: const EdgeInsets.symmetric(horizontal: 10),
                              //         child: Text(
                              //           foodItemsTile[i].foodEstimateTime,
                              //           style: TextStyle(
                              //               fontSize: MediaQuery.of(context).size.width/25,
                              //               fontWeight: FontWeight.w600,
                              //               color: Theme.of(context).primaryColor),
                              //         ),
                              //       ),
                              //     ),
                              //   ],
                              // )
                            ]),
                      ),
                    ],
                  ),
                ),
              ),
            ));
        }
      }
    }
    if (_widgetFood.isEmpty) {
      _widgetFood.add(Padding(
          padding: const EdgeInsets.only(left: 1, top: 10),
          child: Container(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              width: MediaQuery.of(context).size.width / 2.3,
              /*decoration: BoxDecoration(
    borderRadius: BorderRadius.circular(15),
    ),*/
              child: Text("No item found!"))));
    }

    return _widgetFood;
  }

  onSwitch(bool value) {
    if (value = true)
      restaurantItemList!.where((element) => element.isVeg == "Pure Veg");
  }

  onSortByChanged(String? value) {
    switch (value) {
      case 'Relevance':
        restaurantItemList!.sort((a, b) => a.id!.compareTo(b.id!));
        break;
      case 'Low-High':
        restaurantItemList!
            .sort((a, b) => a.itemPrice!.compareTo(b.itemPrice!));
        break;
      case 'High-Low':
        restaurantItemList!
            .sort((b, a) => a.itemPrice!.compareTo(b.itemPrice!));
        break;
      case 'Popular':
        restaurantItemList!.sort((a, b) => a.id!.compareTo(b.id!));
        break;
    }
    context.read(itemListNotifierProvider.notifier).state =
        context.read(itemListNotifierProvider.notifier).state;
    setState(() {
      _sortByValue = value;
    });
  }

  Widget buildSearchBar() {
    return Container(
      height: MediaQuery.of(context).size.width / 6,
      child: TextFormField(
        textCapitalization: TextCapitalization.sentences,
        onChanged: (String? s) {
          setState(() {
            searchText = s;
          });
        },
        style: TextStyle(fontWeight: FontWeight.w300, fontSize: 14),
        // keyboardType: TextInputType.number,
        decoration: InputDecoration(
          // contentPadding: EdgeInsets.symmetric(
          //              horizontal: 5, vertical: top > 80 ? 9 : 10),
          prefixIcon: Icon(
            Icons.search,
            color: App24Colors.redRestaurantThemeColor,
            size: MediaQuery.of(context).size.width / 20,
          ),
          hintText: "Your next delicacy..",
          hintStyle: TextStyle(
              fontSize: MediaQuery.of(context).size.width / 26,
              fontWeight: FontWeight.w600,
              color: App24Colors.redRestaurantThemeColor),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
            borderSide: BorderSide(
              width: 0,
              style: BorderStyle.none,
            ),
          ),
          fillColor: Color(0xffFCE7E5),
          filled: true,
          contentPadding: EdgeInsets.symmetric(horizontal: 5, vertical: 25),
        ),
      ),
    );
  }

  String? getShopName(context, ItemListResponseData shopName) {
    return shopName.storeName;
  }

  //get estimate time
  String? getEstimateTime(context, ItemListResponseData estimateTime) {
    return estimateTime.estimatedDeliveryTime;
  }

  String? getShopPicture(context, ItemListResponseData shopPicture) {
    return shopPicture.picture ?? "";
  }

  // String getFoodDetail(context, ItemListResponseData foodDetail) {
  //   return foodDetail.store;
  // }

  //get store location
  String? getStoreLocation(context, ItemListResponseData storeLocation) {
    return storeLocation.storeLocation;
  }

  @override
  Widget build(BuildContext context) {
    collapsedHeight = MediaQuery.of(context).size.height * 0.07;
    return Scaffold(
      body: SafeArea(
        child: OfflineBuilderWidget(
          RefreshIndicator(
            onRefresh: () {
              return reloadData(init: false, context: context) ??
                  false as Future<void>;
            },
            child: Container(
              // height: 900,
              child: Column(
                children: [
                  Expanded(
                    child: CustomScrollView(
                      slivers: <Widget>[
                        SliverAppBar(
                            backgroundColor: Colors.white,
                            automaticallyImplyLeading: true,
                            pinned: true,
                            toolbarHeight:
                                MediaQuery.of(context).size.height * 0.07,
                            elevation: 0.0,
                            iconTheme:
                                IconThemeData(color: App24Colors.darkTextColor),
                            // actions: [
                            //   Padding(
                            //     padding: const EdgeInsets.only(right: 10),
                            //     child: cartIcon(context:context,fromPage: "fromRestaurant"),
                            //   ),
                            // ],
                            title: Text(
                              widget.restaurantName!,
                              style: TextStyle(
                                  color: App24Colors.darkTextColor,
                                  fontSize:
                                      MediaQuery.of(context).size.width / 25,
                                  fontWeight: FontWeight.bold),
                            ),
                            collapsedHeight: collapsedHeight,
                            expandedHeight:
                                MediaQuery.of(context).size.height * 0.295,
                            flexibleSpace: LayoutBuilder(builder:
                                (BuildContext context,
                                    BoxConstraints constraints) {
                              // print('constraints=' + constraints.toString());
                              sliverAppbarHeight = constraints.biggest.height;
                              return FlexibleSpaceBar(
                                background:
                                ClipRRect(
                                    borderRadius: BorderRadius.only(
                                        bottomRight: Radius.circular(40),
                                        bottomLeft: Radius.circular(40)),
                                    child: Padding(
                                      padding: EdgeInsets.only(
                                          top: kToolbarHeight),
                                      child: CachedNetworkImage(
                                        imageUrl: widget.shopPic.toString()
/*                                            getShopPicture(context,
                                                state.response.responseData)!*/,
                                        fit: BoxFit.cover,
                                        color:
                                        Colors.black.withOpacity(0.4),
                                        colorBlendMode: BlendMode.overlay,
                                        placeholder: (context, url) =>
                                            Image.asset(
                                              App24UserAppImages
                                                  .placeHolderImage,
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              fit: BoxFit.cover,
                                            ),
                                        errorWidget:
                                            (context, url, error) =>
                                            Image.asset(
                                              App24UserAppImages
                                                  .placeHolderImage,
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              fit: BoxFit.cover,
                                            ),
                                      ),
                                    )),
                              );
                            })),
                        SliverList(
                          delegate: SliverChildListDelegate([
                            Container(
                              color: Colors.white,
                              child: Padding(
                                padding: const EdgeInsets.all(15.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                  Column(
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  children: [
                                    Text(widget.shopLocation.toString()
/*                                              getStoreLocation(context,
                                                  state.response.responseData)!*/,
                                      style: TextStyle(
                                          color: Color(0xff000000)
                                              .withOpacity(0.34),
                                          fontSize:
                                          MediaQuery.of(context)
                                              .size
                                              .width /
                                              30,
                                          fontWeight: FontWeight.bold),
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                    Row(
                                      children: [
                                        Expanded(
                                          child: Text(widget.restaurantName.toString()
/*                                                    getShopName(
                                                        context,
                                                        state.response
                                                            .responseData)!*/,
                                            style: TextStyle(
                                                fontSize: MediaQuery.of(
                                                    context)
                                                    .size
                                                    .width /
                                                    15,
                                                fontWeight:
                                                FontWeight.bold),
                                          ),
                                        ),
                                        const SizedBox(
                                          width: 20,
                                        ),
                                        FavoriteButton(
                                          iconSize: 30,
                                          isFavorite: isFavourite,
                                          valueChanged:
                                              (_isFavorite) async {
                                            onFavouriteClicked(
                                                _isFavorite);

                                            var prefs =
                                            await SharedPreferences
                                                .getInstance();
                                            List<String>
                                            restaurantFavouriteList =
                                                prefs.getStringList(
                                                    SharedPreferencesPath
                                                        .favouriteRestaurants) ??
                                                    [];

                                            if (restaurantFavouriteList
                                                .contains(
                                                widget.shopId)) {
                                              if (_isFavorite ==
                                                  false) {
                                                showMessage(
                                                    context,
                                                    "Removed from favourites",
                                                    false,
                                                    true);
                                                restaurantFavouriteList
                                                    .remove(widget
                                                    .shopId
                                                    .toString());
                                                prefs.setStringList(
                                                    SharedPreferencesPath
                                                        .favouriteRestaurants,
                                                    restaurantFavouriteList);

                                                print(prefs.getStringList(
                                                    SharedPreferencesPath
                                                        .favouriteRestaurants));
                                              }
                                            } else {
                                              if (_isFavorite == true) {
                                                showMessage(
                                                    context,
                                                    "Added to favourites",
                                                    false,
                                                    true);
                                                restaurantFavouriteList
                                                    .add(widget.shopId
                                                    .toString());
                                                prefs.setStringList(
                                                    SharedPreferencesPath
                                                        .favouriteRestaurants,
                                                    restaurantFavouriteList);

                                                prefs
                                                    .getStringList(
                                                    SharedPreferencesPath
                                                        .favouriteRestaurants)!
                                                    .toList();
                                                print(prefs.getStringList(
                                                    SharedPreferencesPath
                                                        .favouriteRestaurants));
                                              }
                                            }
                                          },
                                        ),
                                      ],
                                    ),
                                    const SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                      widget.cuisinesTypes.toString(),
                                      style: TextStyle(
                                          color: Color(0xffFF1616)
                                              .withOpacity(0.62),
                                          fontWeight: FontWeight.bold,
                                          fontSize:
                                          MediaQuery.of(context)
                                              .size
                                              .width /
                                              32),
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment
                                          .spaceBetween,
                                      children: [
                                        Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "50+ Ratings",
                                              style: TextStyle(
                                                  fontSize: 12,
                                                  fontWeight:
                                                  FontWeight.w600,
                                                  color: Color(
                                                      0xffC3C3C3)),
                                            ),
                                            RatingBar.builder(
                                              initialRating:
                                              double.parse(widget
                                                  .shopRating
                                                  .toString()),
                                              direction:
                                              Axis.horizontal,
                                              allowHalfRating: true,
                                              itemCount: 5,
                                              itemSize: 20,
                                              itemPadding:
                                              EdgeInsets.symmetric(
                                                  horizontal: 1.0),
                                              itemBuilder:
                                                  (context, _) => Icon(
                                                Icons.star,
                                                color: Colors.amber,
                                              ),
                                              onRatingUpdate: (rating) {
                                                print(rating);
                                              },
                                              ignoreGestures: true,
                                            ),
                                          ],
                                        ),
                                        Column(
                                          children: [
                                            Text("Delivery Time",
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    fontWeight:
                                                    FontWeight.w600,
                                                    color: Color(
                                                        0xffC3C3C3))),
                                            Text(widget.estimateTime!
/*                                                        getEstimateTime(
                                                                context,
                                                                state.response
                                                                    .responseData)!*/ +
                                                " min",
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    fontWeight:
                                                    FontWeight.w600,
                                                    color: App24Colors
                                                        .darkTextColor))
                                          ],
                                        ),
                                        Column(
                                          children: [
                                            Text("Distance",
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    fontWeight:
                                                    FontWeight.w600,
                                                    color: Color(
                                                        0xffC3C3C3))),
                                            Text("4.2 kms",
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    fontWeight:
                                                    FontWeight.w600,
                                                    color: App24Colors
                                                        .darkTextColor))
                                          ],
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    buildSearchBar(),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 15),
                                      child: Row(
                                        // mainAxisAlignment: MainAxisAlignment.end,
                                        children: [
                                          // TextButton.icon(
                                          //     onPressed: () {
                                          //       Navigator.push(
                                          //           context,
                                          //           MaterialPageRoute(
                                          //               builder: (context) =>
                                          //                   FoodFilterPage()));
                                          //     },
                                          //     icon: Icon(
                                          //       Icons.filter_alt,
                                          //       color: Color(0xffFF533C),
                                          //       size: 20,
                                          //     ),
                                          //     label: Text(
                                          //       "Filter",
                                          //       style: TextStyle(
                                          //           color: App24Colors.darkTextColor,
                                          //           fontWeight: FontWeight.w600),
                                          //     )),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 10),
                                      child: Container(
                                        // padding: EdgeInsets.symmetric(horizontal: 10),
                                        height: 40,
                                        // width: 200,
                                        decoration: BoxDecoration(
                                            // color: Theme.of(context).accentColor,
                                            borderRadius:
                                                BorderRadius.circular(18)),
                                        // margin: EdgeInsets.symmetric(horizontal: 20),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: Consumer(builder:
                                                  (context, watch, child) {
                                                final state = watch(
                                                    itemListNotifierProvider);
                                                if (state.isLoading) {
                                                  return ItemListCategoryLoading();
                                                } else if (state.isError) {
                                                  {
                                                    return LoadingError(
                                                      onPressed: (res) {
                                                        reloadData(
                                                            init: true,
                                                            context: res);
                                                      },
                                                      message:
                                                          state.errorMessage,
                                                    );
                                                  }
                                                } else {
                                                  return ListView(
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        horizontal: 10),
                                                    scrollDirection:
                                                        Axis.horizontal,
                                                    children: getCategories(
                                                        context,
                                                        state.response
                                                            .responseData),
                                                  );
                                                }
                                              }),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Transform.scale(
                                          scale: 0.7,
                                          child: CupertinoSwitch(
                                            activeColor: App24Colors
                                                .greenOrderEssentialThemeColor,
                                            value: status,
                                            onChanged: (value) {
                                              onSwitch(value);

                                              // print("VALUE : $value");
                                              setState(() {
                                                status = value;
                                                print(onSwitch(value));
                                              });
                                              // vegOnlyTrue();
                                            },
                                          ),
                                        ),
                                        Text(
                                          "Veg only",
                                          style: TextStyle(
                                              color: App24Colors
                                                  .greenOrderEssentialThemeColor,
                                              fontWeight: FontWeight.bold,
                                              fontSize: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  26),
                                        ),
                                      ],
                                    ),
                                    Consumer(builder: (context, watch, child) {
                                      final state =
                                          watch(itemListNotifierProvider);
                                      // final state1 = watch()
                                      if (state.isLoading) {
                                        return RestaurantItemLoading();
                                      } else if (state.isError) {
                                        return LoadingError(
                                          onPressed: (res) {
                                            reloadData(
                                                init: true, context: res);
                                          },
                                          message:
                                              state.errorMessage.toString(),
                                        );
                                      } else {
                                        // print("resopnseData" + state.response.responseData.products);
                                        restaurantItemList = state
                                            .response.responseData.products;
                                        // restaurantItemUnSortedList
                                        //     .addAll(restaurantItemList!);
                                        return Column(
                                          children: getFoodTiles(context,
                                              state.response.responseData),
                                        );
                                      }
                                    }),
                                  ],
                                ),
                              ),
                            )
                          ]),
                        )
                      ],
                    ),
                  ),

                  // Stack(children: [
                  //   Padding(
                  //     padding: const EdgeInsets.only(bottom: 35),
                  //     child:                               Consumer(builder: (context, watch, child) {
                  //       final state = watch(itemListNotifierProvider);
                  //       if (state.isLoading) {
                  //         return RestaurantItemLoading();
                  //       } else if (state.isError) {
                  //         return LoadingError(
                  //           onPressed: (res) {
                  //             reloadData(init: true, context: res);
                  //           },
                  //           message: state.errorMessage.toString(),
                  //         );
                  //       } else {
                  //         restaurantItemList =
                  //             state.response.responseData.products;
                  //         // restaurantItemUnSortedList.addAll(restaurantItemList);
                  //         return Hero(
                  //           tag: "20",
                  //           child: ClipRRect(
                  //               borderRadius: BorderRadius.only(
                  //                   bottomRight: Radius.circular(40),
                  //                   bottomLeft: Radius.circular(40)),
                  //               child: CachedNetworkImage(
                  //                 imageUrl:
                  //                 getShopPicture(context,state.response.responseData)!,
                  //                 fit: BoxFit.cover,
                  //                 color: Colors.black.withOpacity(0.4),
                  //                 colorBlendMode: BlendMode.overlay,
                  //                 placeholder: (context, url) => Image.asset(
                  //                   App24UserAppImages.placeHolderImage,
                  //                   width: MediaQuery.of(context).size.width ,
                  //                   fit: BoxFit.cover,
                  //                 ),
                  //                 errorWidget: (context, url, error) => Image.asset(
                  //                   App24UserAppImages.placeHolderImage,
                  //                   width: MediaQuery.of(context).size.width ,
                  //                   fit: BoxFit.cover,
                  //                 ),
                  //               )),
                  //         );
                  //       }
                  //     })
                  //
                  //
                  //
                  //   ),
                  //   Positioned(
                  //     left: 30,
                  //     right: 30,
                  //     top: 385,
                  //     // top: 50,
                  //     // bottom: 20,
                  //     child: Container(
                  //       padding: EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                  //       // height: 20,
                  //       width: MediaQuery
                  //           .of(context)
                  //           .size
                  //           .width / 1,
                  //       decoration: BoxDecoration(
                  //         borderRadius: BorderRadius.circular(15),
                  //         color: Color(0xffE3F7FF),
                  //       ),
                  //       child: Row(
                  //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //         children: [
                  //           Image.asset(
                  //             App24UserAppImages.shopOfferPaytm,
                  //             width: 50,
                  //           ),
                  //           Column(
                  //             crossAxisAlignment: CrossAxisAlignment.start,
                  //             children: [
                  //               Text(
                  //                 "15% off on using Paytm UPI",
                  //                 style: TextStyle(
                  //                     color: Color(0xff233266),
                  //                     fontWeight: FontWeight.bold,
                  //                     fontSize:
                  //                     MediaQuery
                  //                         .of(context)
                  //                         .size
                  //                         .width / 27),
                  //               ),
                  //               Text(
                  //                 "On an order of AED100 or more",
                  //                 style: TextStyle(
                  //                     color: Color(0xff6D83CC),
                  //                     fontWeight: FontWeight.bold,
                  //                     fontSize:
                  //                     MediaQuery
                  //                         .of(context)
                  //                         .size
                  //                         .width / 34),
                  //               )
                  //             ],
                  //           ),
                  //           Icon(
                  //             Icons.chevron_right_rounded,
                  //             color: Colors.black,
                  //           )
                  //         ],
                  //       ),
                  //     ),
                  //   ),
                  //   Positioned(left: 10, right: 15, top: 10,
                  //       child: Row(
                  //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //         children: [
                  //           Row(
                  //             children: [
                  //               Material(
                  //                 borderRadius: BorderRadius.circular(20),
                  //                 color: Colors.transparent,
                  //                 child: InkWell(
                  //                     borderRadius: BorderRadius.circular(20),
                  //                     onTap: () {
                  //                       Navigator.pop(context);
                  //                     },
                  //                     child:
                  //                     Padding(
                  //                       padding: const EdgeInsets.all(8.0),
                  //                       child: Icon(Icons.chevron_left_rounded,
                  //                         color: Colors.white, size: 30,),
                  //                     )),
                  //               ),
                  //               const SizedBox(width: 10,),
                  //
                  //               Consumer(builder: (context, watch, child) {
                  //                 final state = watch(itemListNotifierProvider);
                  //                 if (state.isLoading) {
                  //                   return RestaurantItemLoading();
                  //                 } else if (state.isError) {
                  //                   return LoadingError(
                  //                     onPressed: (res) {
                  //                       reloadData(init: true, context: res);
                  //                     },
                  //                     message: state.errorMessage.toString(),
                  //                   );
                  //                 } else {
                  //                   restaurantItemList =
                  //                       state.response.responseData.products;
                  //                   // restaurantItemUnSortedList.addAll(restaurantItemList);
                  //                   return Text(getShopName(context, state.response.responseData)!, style: TextStyle(
                  //                       color: Colors.white, fontSize: MediaQuery
                  //                       .of(context)
                  //                       .size
                  //                       .width / 25, fontWeight: FontWeight.bold),);
                  //               }
                  //               })
                  //
                  //
                  //             ],
                  //           ),
                  //           Material(
                  //             color: Colors.transparent,
                  //             child: InkWell(
                  //                 borderRadius: BorderRadius.circular(15),
                  //                 onTap: () {
                  //                   Navigator.push(
                  //                       context,
                  //                       MaterialPageRoute(
                  //                           builder: (context) =>
                  //                               RestaurantCartPage() /*TrackOrder()*/));
                  //                 },
                  //                 child: Padding(
                  //                     padding: const EdgeInsets.only(
                  //                         right: 8, left: 8),
                  //                     child: Icon(
                  //                       CupertinoIcons.shopping_cart,
                  //                       color: Colors.white,
                  //                       size: MediaQuery
                  //                           .of(context)
                  //                           .size
                  //                           .width / 18,
                  //                     ))),
                  //           ),
                  //         ],
                  //       ))
                  // ]),
                  ViewCartOverlay()
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
