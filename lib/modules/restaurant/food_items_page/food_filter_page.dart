import 'package:app24_user_app/widgets/common_appbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FoodFilterPage extends StatefulWidget {
  const FoodFilterPage({Key? key}) : super(key: key);

  @override
  _FoodFilterPageState createState() => _FoodFilterPageState();
}

class _FoodFilterPageState extends State<FoodFilterPage> {
  List<FoodFilter> foodFilterTile = [];

  @override
  void initState() {
    super.initState();

    foodFilterTile.add(new FoodFilter(foodFilterCuisine: "North Indian"));
    foodFilterTile.add(new FoodFilter(foodFilterCuisine: "Mughalai"));
    foodFilterTile.add(new FoodFilter(foodFilterCuisine: "South Indian"));
    foodFilterTile.add(new FoodFilter(foodFilterCuisine: "Chinese"));
    foodFilterTile.add(new FoodFilter(foodFilterCuisine: "Momos"));
    foodFilterTile.add(new FoodFilter(foodFilterCuisine: "Fast Food"));
  }

  List<Widget> getFoodFilter() {
    List<Widget> _widgetFood = [];

    for (var i = 0; i < foodFilterTile.length; i++) {
      _widgetFood.add(Padding(
        padding: const EdgeInsets.only(left: 1, top: 0),
        child: Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Color(0xfff1f1f1)),
          child: Text(
            foodFilterTile[i].foodFilterCuisine!,
            style: TextStyle(
                color: Color(0xffFF533C),
                fontWeight: FontWeight.w600,
                fontSize: MediaQuery.of(context).size.width / 29),
          ),
        ),
      ));
    }
    return _widgetFood;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        title: "Filter",
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Cuisines"),
                const SizedBox(
                  height: 10,
                ),
                Wrap(
                  spacing: 10,
                  runSpacing: 10,
                  children: getFoodFilter(),
                ),
                const SizedBox(
                  height: 20,
                ),
                Text("Cuisines"),
                const SizedBox(
                  height: 10,
                ),
                Wrap(
                  spacing: 10,
                  runSpacing: 10,
                  children: getFoodFilter(),
                ),
                const SizedBox(
                  height: 20,
                ),
                Text("Cuisines"),
                const SizedBox(
                  height: 10,
                ),
                Wrap(
                  spacing: 10,
                  runSpacing: 10,
                  children: getFoodFilter(),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class FoodFilter {
  String? foodFilterCuisine;

  FoodFilter({
    this.foodFilterCuisine,
  });
}
