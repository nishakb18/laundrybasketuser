import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/api/api_response_state2.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class FavouriteNotifier extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  FavouriteNotifier(this._apiRepository)
      : super(ResponseState2(isLoading: true));

  Future<void> getFavouriteShopList(
      {bool init = true, required BuildContext context}) async {
    try {
      if (init) state = state.copyWith(isLoading: true);

      final favouriteShopList = await _apiRepository.fetchFavouriteShopList();
      state = state.copyWith(
          response: favouriteShopList, isLoading: false, isError: false);
    } catch (e) {
      state = state.copyWith(
          errorMessage: e.toString(), isLoading: false, isError: true);
    }
  }

  Future<void> getFavouriteShop({bool init = true, required Map body}) async {
    try {
      if (init) state = state.copyWith(isLoading: true);

      final favouriteShop = await _apiRepository.fetchFavouriteShop(body: body);

      state = state.copyWith(
          response: favouriteShop, isLoading: false, isError: false);
    } catch (e) {
      state = state.copyWith(
          errorMessage: e.toString(), isLoading: false, isError: true);
    }
  }
}
