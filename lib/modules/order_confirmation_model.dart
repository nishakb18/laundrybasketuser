OrderConfirmationModel orderConfirmationModelFromJson(str) {
  return OrderConfirmationModel.fromJson(str);
}

class OrderConfirmationModel {
  String? statusCode;
  String? title;
  String? message;
  OrderConfirmationResponseData? responseData;
  List<dynamic>? error;

  OrderConfirmationModel(
      {this.statusCode,
      this.title,
      this.message,
      this.responseData,
      this.error});

  OrderConfirmationModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    responseData = json['responseData'] != null
        ? new OrderConfirmationResponseData.fromJson(json['responseData'])
        : null;
    if (json['error'] != null) {
      error = [];
      json['error'].forEach((v) {
        error!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['title'] = this.title;
    data['message'] = this.message;
    if (this.responseData != null) {
      data['responseData'] = this.responseData!.toJson();
    }
    if (this.error != null) {
      data['error'] = this.error!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class OrderConfirmationResponseData {
  String? estimationCost;
  int? distance;
  int? negativeWallet;
  int? iCartId;
  String? iCartType;
  List<ACategories>? aCategories;

  OrderConfirmationResponseData(
      {this.estimationCost,
      this.distance,
      this.negativeWallet,
      this.iCartId,
      this.iCartType,
      this.aCategories});

  OrderConfirmationResponseData.fromJson(Map<String, dynamic> json) {
    estimationCost = json['estimation_cost'];
    distance = json['distance'];
    negativeWallet = json['negative_wallet'];
    iCartId = json['iCartId'];
    iCartType = json['iCartType'];
    if (json['aCategories'] != null) {
      aCategories = [];
      json['aCategories'].forEach((v) {
        aCategories!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['estimation_cost'] = this.estimationCost;
    data['distance'] = this.distance;
    data['negative_wallet'] = this.negativeWallet;
    data['iCartId'] = this.iCartId;
    data['iCartType'] = this.iCartType;
    if (this.aCategories != null) {
      data['aCategories'] = this.aCategories!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ACategories {
  int? id;
  String? name;
  String? category;
  String? cartCatDetais;

  ACategories({this.id, this.name, this.category, this.cartCatDetais});

  ACategories.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    category = json['category'];
    cartCatDetais = json['cartCatDetais'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['category'] = this.category;
    data['cartCatDetais'] = this.cartCatDetais;
    return data;
  }
}
