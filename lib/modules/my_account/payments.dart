import 'package:app24_user_app/app24_user_icons.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/widgets/common_appbar.dart';
import 'package:app24_user_app/widgets/offline_builder_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PaymentsPage extends StatefulWidget {
  @override
  _PaymentsPageState createState() => _PaymentsPageState();
}

class _PaymentsPageState extends State<PaymentsPage> {
  String payment_mode = "cash";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomAppBar(
          title: "Payments",
        ),
        backgroundColor: Colors.white,
        body: OfflineBuilderWidget(
           SafeArea(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  const SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: getSubTitle("Available Payments", context),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Material(
                    color: Colors.transparent,
                    child: ListTile(
                      onTap: () {
                        setState(() {
                          payment_mode = "cash";
                        });
                      },
                      trailing: Icon(
                        payment_mode == "cash"
                            ? Icons.check_circle
                            : Icons.radio_button_unchecked,
                        color: App24Colors.greenOrderEssentialThemeColor,
                      ),
                      leading: Container(
                        width: 35,
                        height: 35,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Color(0xffb6e3fc)),
                        child: Icon(App24User.wallet),
                      ),
                      title: Text(
                        "Cash",
                        style: TextStyle(fontWeight: FontWeight.w500),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Divider(
                      height: 1,
                    ),
                  ),
                  Material(
                    color: Colors.transparent,
                    child: ListTile(
                      onTap: () {
                        setState(() {
                          payment_mode = "card";
                        });
                      },
                      trailing: Icon(
                        payment_mode == "card"
                            ? Icons.check_circle
                            : Icons.radio_button_unchecked,
                        color: App24Colors.greenOrderEssentialThemeColor,
                      ),
                      leading: Container(
                        width: 35,
                        height: 35,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Color(0xfffbebbb)),
                        child: Icon(App24User.wallet),
                      ),
                      title: Text(
                        "Razorpay",
                        style: TextStyle(fontWeight: FontWeight.w500),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Divider(
                      height: 1,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  )
                ],
              ),
            ),
          ),
        ) // This trailing comma makes auto-formatting nicer for build methods.
        );
  }
}
