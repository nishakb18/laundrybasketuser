import 'dart:io';

import 'package:app24_user_app/Laundry-app/views/ServicesScreens/services_home_screen.dart';
import 'package:app24_user_app/Laundry-app/views/widgets/custum_appbar.dart';
import 'package:app24_user_app/app24_user_icons.dart';
import 'package:app24_user_app/constants/app_text.dart';
import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/constants/textstyle.dart';
import 'package:app24_user_app/widgets/common_appbar.dart';
import 'package:app24_user_app/widgets/offline_builder_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:url_launcher/url_launcher.dart';
// import 'package:webview_flutter/webview_flutter.dart';

class SupportPage extends StatefulWidget {
  const SupportPage({super.key, this.isLaundryApp = false});
  final bool? isLaundryApp;


  @override
  _SupportPageState createState() => _SupportPageState();
}

class _SupportPageState extends State<SupportPage> {
  String supportText =
      "You can contact our team anytime you require assistance towards your enquiries!";
  String phoneNumber = "+91 9895227724";
  String website = "app24.co.in/pages/help";
  String mailID = "support@app24india.com";

  @override
  void initState() {
    super.initState();
    // Enable virtual display.
    // if (Platform.isAndroid) WebView.platform = AndroidWebView();
  }

  // Future<void> _makePhoneCall(String number) async {
  //   final Uri launchUri = Uri(
  //     scheme: 'tel',
  //     path: number,
  //   );
  //   await launch(launchUri.toString());
  //   // if (await canLaunch(url)) {
  //   //   await launch(url);
  //   // } else {
  //   //   throw 'Could not launch $url';
  //   // }
  // }

  // Future<void> _openInBrowser(String url) async {
  //   if (!await launch(
  //     url,
  //     forceSafariVC: false,
  //     forceWebView: false,
  //     headers: <String, String>{'my_header_key': 'my_header_value'},
  //   )) {
  //     throw 'Could not launch $url';
  //   }
  //
  //   // if (await canLaunch(url)) {
  //   //   await launch(
  //   //     url,
  //   //     forceSafariVC: false,
  //   //     forceWebView: false,
  //   //   );
  //   // } else {
  //   //   throw 'Could not launch $url';
  //   // }
  // }

  //Mail navigation//
  // Future<void> _sendMail() async {
  //   final Uri emailLaunchUri = Uri(
  //     scheme: 'mailto',
  //     path: mailID,
  //     // query: encodeQueryParameters(<String, String>{
  //     //   'subject': 'Example Subject & Symbols are allowed!'
  //     // }),
  //   );
  //
  //   launch(emailLaunchUri.toString());
  //
  //   // final Uri _emailLaunchUri = Uri(
  //   //     scheme: 'mailto',
  //   //     path: mailID,
  //   //     queryParameters: {'subject': 'Support queries for App24'});
  //   // if (await canLaunch(_emailLaunchUri.toString())) {
  //   //   await launch(_emailLaunchUri.toString());
  //   // } else {
  //   //   throw 'Could not launch ${_emailLaunchUri.toString()}';
  //   // }
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: (widget.isLaundryApp == true)
            ? PreferredSize(
          preferredSize: Size(double.infinity, 80),
          child: CusturmAppbar(
              child: ListTile(
                leading: backarrow(
                  context: context,
                  onTap: () {
                    Navigator.pop(context);
                  },
                ),
                title: Center(
                  child: Text(Apptext.support, style: AppTextStyle().appbathead),
                ),
              )),
        )
            :CustomAppBar(
          hideCartIcon: "true",
          title: "Support",
        ),
        backgroundColor: Colors.white,
        body: OfflineBuilderWidget(
          SafeArea(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  // const SizedBox(
                  //   height: 20,
                  // ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 40),
                    child: Image.asset(
                      App24UserAppImages.supportPageCover,
                      width: 10,
                      height: 300,
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                    margin: EdgeInsets.symmetric(horizontal: 20),
                    child: Row(
                      children: [
                        Expanded(
                          child: Text(
                            supportText,
                            style: TextStyle(
                                color: App24Colors.darkTextColor,
                                fontWeight: FontWeight.w600,
                                fontSize: 16),
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Material(
                      color: Colors.transparent,
                      child: ListTile(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8)),
                        onTap: () {
                          Clipboard.setData(
                              ClipboardData(text: phoneNumber));
                          ScaffoldMessenger.of(context)
                              .showSnackBar(SnackBar(
                            content: Text(
                                "Phone Number copied to clipboard."),
                          ));
                          // _makePhoneCall('tel:$phoneNumber');
                        },
                        leading: Container(
                          width: 35,
                          height: 35,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: Icon(Icons.call,
                              color: App24Colors.darkTextColor),
                        ),
                        title: Text(
                          "Support on Call",
                          style: TextStyle(fontWeight: FontWeight.w600),
                        ),
                        subtitle: Text(
                          phoneNumber,
                          style: TextStyle(fontWeight: FontWeight.w600),
                        ),
                        trailing: Image.asset(
                          App24UserAppImages.roundRArrow,
                          width: 20,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Divider(
                      height: 1,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Material(
                      color: Colors.transparent,
                      child: ListTile(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8)),
                        onTap: () {
                          Clipboard.setData(
                              ClipboardData(text: mailID));
                          ScaffoldMessenger.of(context)
                              .showSnackBar(SnackBar(
                            content: Text(
                                "Mail ID copied to clipboard."),
                          ));
                          // _sendMail();
                        },
                        leading: Container(
                          width: 35,
                          height: 35,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: Icon(Icons.mail,
                              color: App24Colors.darkTextColor),
                        ),
                        title: Text(
                          "Mail",
                          style: TextStyle(fontWeight: FontWeight.w600),
                        ),
                        subtitle: Text(
                          mailID,
                          style: TextStyle(fontWeight: FontWeight.w600),
                        ),
                        trailing: Image.asset(
                          App24UserAppImages.roundRArrow,
                          width: 20,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Divider(
                      height: 1,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Material(
                      color: Colors.transparent,
                      child: ListTile(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8)),
                        onTap: () {
                          // Navigator.push(
                          //     context,
                          //     MaterialPageRoute(
                          //         builder: (context) => WebView(
                          //               initialUrl: "https://$website",
                          //             )));

                          // _openInBrowser("https://$website");
                        },
                        leading: Container(
                          width: 35,
                          height: 35,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: Icon(
                            App24User.website,
                            color: App24Colors.darkTextColor,
                          ),
                        ),
                        title: Text(
                          "Website",
                          style: TextStyle(fontWeight: FontWeight.w600),
                        ),
                        subtitle: Text(
                          website,
                          style: TextStyle(fontWeight: FontWeight.w600),
                        ),
                        trailing: Image.asset(
                          App24UserAppImages.roundRArrow,
                          width: 20,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Divider(
                      height: 1,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ) // This trailing comma makes auto-formatting nicer for build methods.
        );
  }
}
