import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/widgets/common_appbar.dart';
import 'package:app24_user_app/widgets/offline_builder_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LanguagePage extends StatefulWidget {
  @override
  _LanguagePageState createState() => _LanguagePageState();
}

class _LanguagePageState extends State<LanguagePage> {
  String language = "english";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomAppBar(
          title: "Language",
          // actions: cartIcon(context),
        ),
        backgroundColor: Colors.white,
        body: OfflineBuilderWidget(
          SafeArea(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: getSubTitle(
                        "Please select your desired language", context),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Material(
                    color: Colors.transparent,
                    child: ListTile(
                      onTap: () {
                        setState(() {
                          language = "english";
                        });
                      },
                      leading: Icon(
                        language == "english"
                            ? Icons.check_circle
                            : Icons.radio_button_unchecked,
                        color: App24Colors.darkTextColor,
                      ),
                      // leading: Container(
                      //   width: 35,
                      //   height: 35,
                      //   decoration: BoxDecoration(
                      //       borderRadius: BorderRadius.circular(5),
                      //       color: Color(0xffb6e3fc)),
                      //   child: Icon(App24User.wallet),
                      // ),
                      title: Text(
                        "English",
                        style: TextStyle(fontWeight: FontWeight.w600),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Divider(
                      height: 1,
                    ),
                  ),
                  Material(
                    color: Colors.transparent,
                    child: ListTile(
                      onTap: () {
                        setState(() {
                          language = "arabic";
                        });
                      },
                      leading: Icon(
                        language == "arabic"
                            ? Icons.check_circle
                            : Icons.radio_button_unchecked,
                        color: App24Colors.darkTextColor,
                      ),
                      // leading: Container(
                      //   width: 35,
                      //   height: 35,
                      //   decoration: BoxDecoration(
                      //       borderRadius: BorderRadius.circular(5),
                      //       color: Color(0xfffbebbb)),
                      //   child: Icon(App24User.wallet),
                      // ),
                      title: Text(
                        "Arabic",
                        style: TextStyle(fontWeight: FontWeight.w600),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Divider(
                      height: 1,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: SizedBox(
                      width: double.infinity,
                      child: ElevatedButton(
                          onPressed: () {},
                          style: ElevatedButton.styleFrom(
                            padding: EdgeInsets.symmetric(vertical: 20), backgroundColor: App24Colors.darkTextColor,
                            shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(15.0),
                            ),
                          ),
                          child: Text(
                            ' Save your preferences',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontWeight: FontWeight.bold),
                          )),
                    ),
                  ),

                  // Padding(
                  //   padding: const EdgeInsets.symmetric(horizontal: 20),
                  //   child: SizedBox(
                  //     width: double.infinity,
                  //     child: FlatButton(
                  //       padding: EdgeInsets.symmetric(vertical: 20),
                  //       shape: RoundedRectangleBorder(
                  //         borderRadius: BorderRadius.circular(15)
                  //       ),
                  //       onPressed: () {},
                  //       child: Text(
                  //         "Save your preferences",
                  //         style: TextStyle(color: Colors.white,fontSize: 17,fontWeight: FontWeight.bold),
                  //       ),
                  //       color: App24Colors.darkTextColor,
                  //     ),
                  //   ),
                  // )
                ],
              ),
            ),
          ),
        ) // This trailing comma makes auto-formatting nicer for build methods.
        );
  }
}
