ProfileModel profileModelFromJson(str) {
  return ProfileModel.fromJson(str);
}


class ProfileModel {
  String? statusCode;
  String? title;
  String? message;
  ProfileResponseData? responseData;
  List<dynamic>? error;

  ProfileModel(
      {this.statusCode,
        this.title,
        this.message,
        this.responseData,
        this.error});

  ProfileModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    responseData = json['responseData'] != null
        ? new ProfileResponseData.fromJson(json['responseData'])
        : null;
    if (json['error'] != null) {
      error = [];
      json['error'].forEach((v) {
        error!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['title'] = this.title;
    data['message'] = this.message;
    if (this.responseData != null) {
      data['responseData'] = this.responseData!.toJson();
    }
    if (this.error != null) {
      data['error'] = this.error!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ProfileResponseData {
  int? id;
  String? firstName;
  String? lastName;
  String? paymentMode;
  String? userType;
  String? email;
  String? mobile;
  String? gender;
  String? countryCode;
  String? currencySymbol;
  String? picture;
  String? address;
  int? pincode;
  String? loginBy;
  double? latitude;
  double? longitude;
  dynamic walletBalance;
  double? rating;
  String? language;
  String? referralUniqueId;
  int? countryId;
  int? stateId;
  int? cityId;
  int? companyId;
  int? status;
  String? appVersion;
  String? createdAt;
  Referral? referral;
  int? walletPay;
  Country? country;
  State? state;
  City? city;

  ProfileResponseData(
      {this.id,
        this.firstName,
        this.lastName,
        this.paymentMode,
        this.userType,
        this.email,
        this.mobile,
        this.gender,
        this.countryCode,
        this.currencySymbol,
        this.picture,
        this.address,
        this.pincode,
        this.loginBy,
        this.latitude,
        this.longitude,
        this.walletBalance,
        this.rating,
        this.language,
        this.referralUniqueId,
        this.countryId,
        this.stateId,
        this.cityId,
        this.companyId,
        this.status,
        this.appVersion,
        this.createdAt,
        this.referral,
        this.walletPay,
        this.country,
        this.state,
        this.city});

  ProfileResponseData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    paymentMode = json['payment_mode'];
    userType = json['user_type'];
    email = json['email'];
    mobile = json['mobile'];
    gender = json['gender'];
    countryCode = json['country_code'];
    currencySymbol = json['currency_symbol'];
    picture = json['picture'];
    address = json['address'];
    pincode = json['pincode'];
    loginBy = json['login_by'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    walletBalance = json['wallet_balance'];
    rating = json['rating'].toDouble();
    language = json['language'];
    referralUniqueId = json['referral_unique_id'];
    countryId = json['country_id'];
    stateId = json['state_id'];
    cityId = json['city_id'];
    companyId = json['company_id'];
    status = json['status'];
    appVersion = json['app_version'];
    createdAt = json['created_at'];
    referral = json['referral'] != null
        ? new Referral.fromJson(json['referral'])
        : null;
    walletPay = json['wallet_pay'];
    country =
    json['country'] != null ? new Country.fromJson(json['country']) : null;
    state = json['state'] != null ? new State.fromJson(json['state']) : null;
    city = json['city'] != null ? new City.fromJson(json['city']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['payment_mode'] = this.paymentMode;
    data['user_type'] = this.userType;
    data['email'] = this.email;
    data['mobile'] = this.mobile;
    data['gender'] = this.gender;
    data['country_code'] = this.countryCode;
    data['currency_symbol'] = this.currencySymbol;
    data['picture'] = this.picture;
    data['address'] = this.address;
    data['pincode'] = this.pincode;
    data['login_by'] = this.loginBy;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['wallet_balance'] = this.walletBalance;
    data['rating'] = this.rating;
    data['language'] = this.language;
    data['referral_unique_id'] = this.referralUniqueId;
    data['country_id'] = this.countryId;
    data['state_id'] = this.stateId;
    data['city_id'] = this.cityId;
    data['company_id'] = this.companyId;
    data['status'] = this.status;
    data['app_version'] = this.appVersion;
    data['created_at'] = this.createdAt;
    if (this.referral != null) {
      data['referral'] = this.referral!.toJson();
    }
    data['wallet_pay'] = this.walletPay;
    if (this.country != null) {
      data['country'] = this.country!.toJson();
    }
    if (this.state != null) {
      data['state'] = this.state!.toJson();
    }
    if (this.city != null) {
      data['city'] = this.city!.toJson();
    }
    return data;
  }
}

class Referral {
  String? referralCode;
  int? referralAmount;
  int? referralCount;
  int? userReferralCount;
  int? userTotalReferralCount;
  int? userReferralAmount;

  Referral(
      {this.referralCode,
        this.referralAmount,
        this.referralCount,
        this.userReferralCount,
        this.userTotalReferralCount,
        this.userReferralAmount});

  Referral.fromJson(Map<String, dynamic> json) {
    referralCode = json['referral_code'];
    referralAmount = json['referral_amount'];
    referralCount = json['referral_count'];
    userReferralCount = json['user_referral_count'];
    userTotalReferralCount = json['user_total_referral_count'];
    userReferralAmount = json['user_referral_amount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['referral_code'] = this.referralCode;
    data['referral_amount'] = this.referralAmount;
    data['referral_count'] = this.referralCount;
    data['user_referral_count'] = this.userReferralCount;
    data['user_total_referral_count'] = this.userTotalReferralCount;
    data['user_referral_amount'] = this.userReferralAmount;
    return data;
  }
}

class Country {
  int? id;
  String? countryName;
  String? countryCode;
  String? countryPhonecode;
  String? countryCurrency;
  String? countrySymbol;
  String? status;
  Null timezone;

  Country(
      {this.id,
        this.countryName,
        this.countryCode,
        this.countryPhonecode,
        this.countryCurrency,
        this.countrySymbol,
        this.status,
        this.timezone});

  Country.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    countryName = json['country_name'];
    countryCode = json['country_code'];
    countryPhonecode = json['country_phonecode'];
    countryCurrency = json['country_currency'];
    countrySymbol = json['country_symbol'];
    status = json['status'];
    timezone = json['timezone'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['country_name'] = this.countryName;
    data['country_code'] = this.countryCode;
    data['country_phonecode'] = this.countryPhonecode;
    data['country_currency'] = this.countryCurrency;
    data['country_symbol'] = this.countrySymbol;
    data['status'] = this.status;
    data['timezone'] = this.timezone;
    return data;
  }
}

class State {
  int? id;
  int? countryId;
  String? stateName;
  String? timezone;
  String? utcOffset;
  int? status;

  State(
      {this.id,
        this.countryId,
        this.stateName,
        this.timezone,
        this.utcOffset,
        this.status});

  State.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    countryId = json['country_id'];
    stateName = json['state_name'];
    timezone = json['timezone'];
    utcOffset = json['utc_offset'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['country_id'] = this.countryId;
    data['state_name'] = this.stateName;
    data['timezone'] = this.timezone;
    data['utc_offset'] = this.utcOffset;
    data['status'] = this.status;
    return data;
  }
}

class City {
  int? id;
  int? countryId;
  int? stateId;
  String? cityName;
  int? status;

  City({this.id, this.countryId, this.stateId, this.cityName, this.status});

  City.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    countryId = json['country_id'];
    stateId = json['state_id'];
    cityName = json['city_name'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['country_id'] = this.countryId;
    data['state_id'] = this.stateId;
    data['city_name'] = this.cityName;
    data['status'] = this.status;
    return data;
  }
}


// class ProfileModel {
//   String? statusCode;
//   String? title;
//   String? message;
//   ProfileResponseData? responseData;
//   List<dynamic>? error;
//
//   ProfileModel(
//       {this.statusCode,
//       this.title,
//       this.message,
//       this.responseData,
//       this.error});
//
//   ProfileModel.fromJson(Map<String, dynamic> json) {
//     statusCode = json['statusCode'];
//     title = json['title'];
//     message = json['message'];
//     responseData = json['responseData'] != null
//         ? new ProfileResponseData.fromJson(json['responseData'])
//         : null;
//     if (json['error'] != null) {
//       error = [];
//       json['error'].forEach((v) {
//         error!.add(v);
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['statusCode'] = this.statusCode;
//     data['title'] = this.title;
//     data['message'] = this.message;
//     if (this.responseData != null) {
//       data['responseData'] = this.responseData!.toJson();
//     }
//     if (this.error != null) {
//       data['error'] = this.error!.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }
//
// class ProfileResponseData {
//   int? id;
//   String? firstName;
//   String? lastName;
//   String? paymentMode;
//   String? userType;
//   String? email;
//   String? mobile;
//   String? gender;
//   String? countryCode;
//   String? currencySymbol;
//   String? picture;
//   String? address;
//   int? pincode;
//   String? loginBy;
//   double? latitude;
//   double? longitude;
//   int? walletBalance;
//   int? rating;
//   String? language;
//   String? referralUniqueId;
//   int? countryId;
//   int? stateId;
//   int? cityId;
//   int? companyId;
//   int? status;
//   String? appVersion;
//   String? createdAt;
//   Referral? referral;
//   int? walletPay;
//   Country? country;
//   State? state;
//   City? city;
//
//   ProfileResponseData(
//       {this.id,
//       this.firstName,
//       this.lastName,
//       this.paymentMode,
//       this.userType,
//       this.email,
//       this.mobile,
//       this.gender,
//       this.countryCode,
//       this.currencySymbol,
//       this.picture,
//       this.address,
//       this.pincode,
//       this.loginBy,
//       this.latitude,
//       this.longitude,
//       this.walletBalance,
//       this.rating,
//       this.language,
//       this.referralUniqueId,
//       this.countryId,
//       this.stateId,
//       this.cityId,
//       this.companyId,
//       this.status,
//       this.appVersion,
//       this.createdAt,
//       this.referral,
//       this.walletPay,
//       this.country,
//       this.state,
//       this.city});
//
//   ProfileResponseData.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     firstName = json['first_name'];
//     lastName = json['last_name'];
//     paymentMode = json['payment_mode'];
//     userType = json['user_type'];
//     email = json['email'];
//     mobile = json['mobile'];
//     gender = json['gender'];
//     countryCode = json['country_code'];
//     currencySymbol = json['currency_symbol'];
//     picture = json['picture'];
//     address = json['address'];
//     pincode = json['pincode'];
//     loginBy = json['login_by'];
//     latitude = json['latitude'];
//     longitude = json['longitude'];
//     walletBalance = json['wallet_balance'];
//     rating = json['rating'];
//     language = json['language'];
//     referralUniqueId = json['referral_unique_id'];
//     countryId = json['country_id'];
//     stateId = json['state_id'];
//     cityId = json['city_id'];
//     companyId = json['company_id'];
//     status = json['status'];
//     appVersion = json['app_version'];
//     createdAt = json['created_at'];
//     referral = json['referral'] != null
//         ? new Referral.fromJson(json['referral'])
//         : null;
//     walletPay = json['wallet_pay'];
//     country =
//         json['country'] != null ? new Country.fromJson(json['country']) : null;
//     state = json['state'] != null ? new State.fromJson(json['state']) : null;
//     city = json['city'] != null ? new City.fromJson(json['city']) : null;
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['first_name'] = this.firstName;
//     data['last_name'] = this.lastName;
//     data['payment_mode'] = this.paymentMode;
//     data['user_type'] = this.userType;
//     data['email'] = this.email;
//     data['mobile'] = this.mobile;
//     data['gender'] = this.gender;
//     data['country_code'] = this.countryCode;
//     data['currency_symbol'] = this.currencySymbol;
//     data['picture'] = this.picture;
//     data['address'] = this.address;
//     data['pincode'] = this.pincode;
//     data['login_by'] = this.loginBy;
//     data['latitude'] = this.latitude;
//     data['longitude'] = this.longitude;
//     data['wallet_balance'] = this.walletBalance;
//     data['rating'] = this.rating;
//     data['language'] = this.language;
//     data['referral_unique_id'] = this.referralUniqueId;
//     data['country_id'] = this.countryId;
//     data['state_id'] = this.stateId;
//     data['city_id'] = this.cityId;
//     data['company_id'] = this.companyId;
//     data['status'] = this.status;
//     data['app_version'] = this.appVersion;
//     data['created_at'] = this.createdAt;
//     if (this.referral != null) {
//       data['referral'] = this.referral!.toJson();
//     }
//     data['wallet_pay'] = this.walletPay;
//     if (this.country != null) {
//       data['country'] = this.country!.toJson();
//     }
//     if (this.state != null) {
//       data['state'] = this.state!.toJson();
//     }
//     if (this.city != null) {
//       data['city'] = this.city!.toJson();
//     }
//     return data;
//   }
// }
//
// class Referral {
//   String? referralCode;
//   int? referralAmount;
//   int? referralCount;
//   int? userReferralCount;
//   int? userTotalReferralCount;
//   int? userReferralAmount;
//
//   Referral(
//       {this.referralCode,
//       this.referralAmount,
//       this.referralCount,
//       this.userReferralCount,
//       this.userTotalReferralCount,
//       this.userReferralAmount});
//
//   Referral.fromJson(Map<String, dynamic> json) {
//     referralCode = json['referral_code'];
//     referralAmount = json['referral_amount'];
//     referralCount = json['referral_count'];
//     userReferralCount = json['user_referral_count'];
//     userTotalReferralCount = json['user_total_referral_count'];
//     userReferralAmount = json['user_referral_amount'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['referral_code'] = this.referralCode;
//     data['referral_amount'] = this.referralAmount;
//     data['referral_count'] = this.referralCount;
//     data['user_referral_count'] = this.userReferralCount;
//     data['user_total_referral_count'] = this.userTotalReferralCount;
//     data['user_referral_amount'] = this.userReferralAmount;
//     return data;
//   }
// }
//
// class Country {
//   int? id;
//   String? countryName;
//   String? countryCode;
//   String? countryPhonecode;
//   String? countryCurrency;
//   String? countrySymbol;
//   String? status;
//   int? timezone;
//
//   Country(
//       {this.id,
//       this.countryName,
//       this.countryCode,
//       this.countryPhonecode,
//       this.countryCurrency,
//       this.countrySymbol,
//       this.status,
//       this.timezone});
//
//   Country.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     countryName = json['country_name'];
//     countryCode = json['country_code'];
//     countryPhonecode = json['country_phonecode'];
//     countryCurrency = json['country_currency'];
//     countrySymbol = json['country_symbol'];
//     status = json['status'];
//     timezone = json['timezone'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['country_name'] = this.countryName;
//     data['country_code'] = this.countryCode;
//     data['country_phonecode'] = this.countryPhonecode;
//     data['country_currency'] = this.countryCurrency;
//     data['country_symbol'] = this.countrySymbol;
//     data['status'] = this.status;
//     data['timezone'] = this.timezone;
//     return data;
//   }
// }
//
// class State {
//   int? id;
//   int? countryId;
//   String? stateName;
//   String? timezone;
//   String? utcOffset;
//   int? status;
//
//   State(
//       {this.id,
//       this.countryId,
//       this.stateName,
//       this.timezone,
//       this.utcOffset,
//       this.status});
//
//   State.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     countryId = json['country_id'];
//     stateName = json['state_name'];
//     timezone = json['timezone'];
//     utcOffset = json['utc_offset'];
//     status = json['status'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['country_id'] = this.countryId;
//     data['state_name'] = this.stateName;
//     data['timezone'] = this.timezone;
//     data['utc_offset'] = this.utcOffset;
//     data['status'] = this.status;
//     return data;
//   }
// }
//
// class City {
//   int? id;
//   int? countryId;
//   int? stateId;
//   String? cityName;
//   int? status;
//
//   City({this.id, this.countryId, this.stateId, this.cityName, this.status});
//
//   City.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     countryId = json['country_id'];
//     stateId = json['state_id'];
//     cityName = json['city_name'];
//     status = json['status'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['country_id'] = this.countryId;
//     data['state_id'] = this.stateId;
//     data['city_name'] = this.cityName;
//     data['status'] = this.status;
//     return data;
//   }
// }
