// To parse this JSON data, do
//
//     final countryModel = countryModelFromJson(jsonString);

import 'dart:convert';

CountryModel countryModelFromJson(String str) => CountryModel.fromJson(json.decode(str));

String countryModelToJson(CountryModel data) => json.encode(data.toJson());

class CountryModel {
  String? statusCode;
  String? title;
  String? message;
  List<ResponseDatum>? responseData;
  List<dynamic>? responseNewData;
  List<dynamic>? error;

  CountryModel({
    this.statusCode,
    this.title,
    this.message,
    this.responseData,
    this.responseNewData,
    this.error,
  });

  factory CountryModel.fromJson(Map<String, dynamic> json) => CountryModel(
    statusCode: json["statusCode"],
    title: json["title"],
    message: json["message"],
    responseData: json["responseData"] == null ? [] : List<ResponseDatum>.from(json["responseData"]!.map((x) => ResponseDatum.fromJson(x))),
    responseNewData: json["responseNewData"] == null ? [] : List<dynamic>.from(json["responseNewData"]!.map((x) => x)),
    error: json["error"] == null ? [] : List<dynamic>.from(json["error"]!.map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "statusCode": statusCode,
    "title": title,
    "message": message,
    "responseData": responseData == null ? [] : List<dynamic>.from(responseData!.map((x) => x.toJson())),
    "responseNewData": responseNewData == null ? [] : List<dynamic>.from(responseNewData!.map((x) => x)),
    "error": error == null ? [] : List<dynamic>.from(error!.map((x) => x)),
  };
}

class ResponseDatum {
  int? id;
  String? countryName;
  String? countryCode;
  String? countryPhonecode;
  String? countryCurrency;
  String? countrySymbol;
  String? status;
  dynamic timezone;
  List<City>? city;

  ResponseDatum({
    this.id,
    this.countryName,
    this.countryCode,
    this.countryPhonecode,
    this.countryCurrency,
    this.countrySymbol,
    this.status,
    this.timezone,
    this.city,
  });

  factory ResponseDatum.fromJson(Map<String, dynamic> json) => ResponseDatum(
    id: json["id"],
    countryName: json["country_name"],
    countryCode: json["country_code"],
    countryPhonecode: json["country_phonecode"],
    countryCurrency: json["country_currency"],
    countrySymbol: json["country_symbol"],
    status: json["status"],
    timezone: json["timezone"],
    city: json["city"] == null ? [] : List<City>.from(json["city"]!.map((x) => City.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "country_name": countryName,
    "country_code": countryCode,
    "country_phonecode": countryPhonecode,
    "country_currency": countryCurrency,
    "country_symbol": countrySymbol,
    "status": status,
    "timezone": timezone,
    "city": city == null ? [] : List<dynamic>.from(city!.map((x) => x.toJson())),
  };
}

class City {
  int? id;
  int? countryId;
  int? stateId;
  String? cityName;
  int? status;
  int? isState;

  City({
    this.id,
    this.countryId,
    this.stateId,
    this.cityName,
    this.status,
    this.isState,
  });

  factory City.fromJson(Map<String, dynamic> json) => City(
    id: json["id"],
    countryId: json["country_id"],
    stateId: json["state_id"],
    cityName: json["city_name"],
    status: json["status"],
    isState: json["is_state"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "country_id": countryId,
    "state_id": stateId,
    "city_name": cityName,
    "status": status,
    "is_state": isState,
  };
}
