import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ProfileLoading extends StatefulWidget {
  @override
  _ProfileLoadingState createState() => _ProfileLoadingState();
}

class _ProfileLoadingState extends State<ProfileLoading> {
  Timer? _timer;
  int _start = 10; // time to popup still working window /// in seconds

  @override
  void initState() {
    startTimer();
    super.initState();
  }

  @override
  void dispose() {
    if (_timer != null) {
      _timer!.cancel();
    }
    super.dispose();
  }

  void startTimer() {
    if (_timer != null) {
      _timer!.cancel();
      _timer = null;
    } else {
      _timer = new Timer.periodic(const Duration(seconds: 1), (Timer timer) {
        setState(() {
          if (_start < 1) {
            _timer!.cancel();
          } else {
            _start = _start - 1;
          }
        });
      });
    }
  }

  buildRestaurantCartShimmerItem(context) {
    List<Widget> list = [];

    for (var i = 0; i < 1; i++)
      list.add(Container(
        // width: MediaQuery.of(context).size.width / 1,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // Container(
            //   height: 120,
            //
            //
            //   width: 120,
            //   margin: EdgeInsets.only(top: 30, left: 100),
            //   decoration: BoxDecoration(
            //       borderRadius: BorderRadius.circular(60),
            //       color: Colors.white,
            //       boxShadow: [
            //         BoxShadow(
            //           color: Color(0xffefefef),
            //           blurRadius: 20,
            //           spreadRadius: 3,
            //         ),
            //       ]),
            // ),
            const SizedBox(height: 30,),
            Container(
              height: 40,
              //
              width: MediaQuery.of(context).size.width /1.0,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xffefefef),
                      blurRadius: 20,
                      spreadRadius: 3,
                    ),
                  ]),
            ),
            const SizedBox(height: 30,),
            Container(
              height: 40,

              width: MediaQuery.of(context).size.width /1.2,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xffefefef),
                      blurRadius: 20,
                      spreadRadius: 3,
                    ),
                  ]),
            ),
            const SizedBox(height: 30,),
            Container(
              height: 40,

              width: MediaQuery.of(context).size.width /3,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xffefefef),
                      blurRadius: 20,
                      spreadRadius: 3,
                    ),
                  ]),
            ),
            const SizedBox(height: 30,),
            Row(
              children: [
                Container(
                  height: 40,

                  width: MediaQuery.of(context).size.width /6,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Color(0xffefefef),
                          blurRadius: 20,
                          spreadRadius: 3,
                        ),
                      ]),
                ),
                const SizedBox(width: 10,),
                Container(
                  height: 40,

                  width: MediaQuery.of(context).size.width /1.58,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Color(0xffefefef),
                          blurRadius: 20,
                          spreadRadius: 3,
                        ),
                      ]),
                )
              ],
            ),
            const SizedBox(height: 30,),
            Container(
              height: 40,

              width: MediaQuery.of(context).size.width /1.2,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xffefefef),
                      blurRadius: 20,
                      spreadRadius: 3,
                    ),
                  ]),
            ),
            const SizedBox(height: 30,),
            Container(
              height: 40,

              width: MediaQuery.of(context).size.width /1.2,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xffefefef),
                      blurRadius: 20,
                      spreadRadius: 3,
                    ),
                  ]),
            ),
            const SizedBox(height: 30,),
            Container(
              height: 40,

              width: MediaQuery.of(context).size.width /1.2,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xffefefef),
                      blurRadius: 20,
                      spreadRadius: 3,
                    ),
                  ]),
            ),
            const SizedBox(height: 30,),
            Container(
              height: 40,

              width: MediaQuery.of(context).size.width /1.2,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xffefefef),
                      blurRadius: 20,
                      spreadRadius: 3,
                    ),
                  ]),
            ),
            const SizedBox(height: 40,),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  height: 20,

                  width: MediaQuery.of(context).size.width /3,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Color(0xffefefef),
                          blurRadius: 20,
                          spreadRadius: 3,
                        ),
                      ]),
                )
              ],
            )

          ],
        ),
      ));

    return list;
  }

  buildStillLoadingWidget() {
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(15)),
      width: MediaQuery.of(context).size.width,
      child: ListTile(
        title: Text("Please wait..."),
        subtitle: Text(
            "The network seems to be too busy. We suggest you to wait for some more time."),
        leading: CircularProgressIndicator.adaptive(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        // padding: EdgeInsets.symmetric(vertical: 10),
          height: MediaQuery.of(context).size.height,
          child: Stack(
            fit: StackFit.expand,
            children: [
              Shimmer.fromColors(
                  baseColor: Colors.grey[300]!,
                  highlightColor: Colors.grey[100]!,
                  child: ListView(
                      shrinkWrap: true,
                      children: buildRestaurantCartShimmerItem(context))),
              // Positioned(
              //   left: 20,
              //   right: 20,
              //   top: 230,
              //   child: _start == 0 ? buildStillLoadingWidget() : Container(),
              // )
            ],
          )),
    );
  }
}
