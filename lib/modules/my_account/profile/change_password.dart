import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/widgets/common_appbar.dart';
import 'package:app24_user_app/widgets/common_main_button.dart';
import 'package:app24_user_app/widgets/offline_builder_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ChangePasswordPage extends StatefulWidget {
  @override
  _ChangePasswordPageState createState() => _ChangePasswordPageState();
}

class _ChangePasswordPageState extends State<ChangePasswordPage> {
  bool isOldPasswordVisible = false;
  bool isNewPasswordVisible = false;
  bool isRepeatPasswordVisible = false;

  TextEditingController oldPasswordController = new TextEditingController();
  TextEditingController newPasswordController = new TextEditingController();
  TextEditingController confirmPasswordController = new TextEditingController();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    InputDecoration _inputDecoration = InputDecoration(
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(8),
        borderSide: BorderSide(
          width: 0,
          style: BorderStyle.none,
        ),
      ),
      fillColor: Theme.of(context).cardColor,
      filled: true,
      contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 20),
    );
    return Scaffold(
        appBar: CustomAppBar(
          title: "Change Password",
        ),
        backgroundColor: Colors.white,
        body: OfflineBuilderWidget(
          SafeArea(
            child: SingleChildScrollView(
                child: Container(
              margin: EdgeInsets.symmetric(horizontal: 15),
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    SizedBox(
                      height: 20,
                    ),
                    getSubTitle("Current Password", context),
                    TextFormField(
                      textInputAction: TextInputAction.next,
                      controller: oldPasswordController,
                      obscureText: !isOldPasswordVisible,
                      style: TextStyle(fontWeight: FontWeight.w500),
                      decoration: _inputDecoration.copyWith(
                          labelText: "Old Password",
                          suffixIcon: Material(
                              color: Colors.transparent,
                              child: InkWell(
                                  customBorder: CircleBorder(),
                                  onTap: () {
                                    setState(() {
                                      isOldPasswordVisible =
                                          !isOldPasswordVisible;
                                    });
                                  },
                                  child: Container(
                                    padding: EdgeInsets.all(15),
                                    child: Icon(
                                      isOldPasswordVisible
                                          ? Icons.visibility_off
                                          : Icons.visibility,
                                      color: App24Colors
                                          .greenOrderEssentialThemeColor,
                                    ),
                                  )))),
                      validator: (String? arg) {
                        if (arg!.length == 0)
                          return 'old password can\'t be empty!';
                        else
                          return null;
                      },
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    getSubTitle("New Password", context),
                    TextFormField(
                      textInputAction: TextInputAction.next,
                      obscureText: !isNewPasswordVisible,
                      controller: newPasswordController,
                      style: TextStyle(fontWeight: FontWeight.w500),
                      decoration: _inputDecoration.copyWith(
                          labelText: "New Password",
                          suffixIcon: Material(
                              color: Colors.transparent,
                              child: InkWell(
                                  customBorder: CircleBorder(),
                                  onTap: () {
                                    setState(() {
                                      isNewPasswordVisible =
                                          !isNewPasswordVisible;
                                    });
                                  },
                                  child: Container(
                                    padding: EdgeInsets.all(15),
                                    child: Icon(
                                      isNewPasswordVisible
                                          ? Icons.visibility_off
                                          : Icons.visibility,
                                      color: App24Colors
                                          .greenOrderEssentialThemeColor,
                                    ),
                                  )))),
                      validator: (String? arg) {
                        if (arg!.length == 0)
                          return 'new password can\'t be empty!';
                        else
                          return null;
                      },
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    TextFormField(
                      textInputAction: TextInputAction.next,
                      obscureText: !isRepeatPasswordVisible,
                      controller: confirmPasswordController,
                      style: TextStyle(fontWeight: FontWeight.w500),
                      keyboardType: TextInputType.text,
                      decoration: _inputDecoration.copyWith(
                          labelText: "Confirm New Password",
                          suffixIcon: Material(
                              color: Colors.transparent,
                              child: InkWell(
                                  customBorder: CircleBorder(),
                                  onTap: () {
                                    setState(() {
                                      isRepeatPasswordVisible =
                                          !isRepeatPasswordVisible;
                                    });
                                  },
                                  child: Container(
                                    padding: EdgeInsets.all(15),
                                    child: Icon(
                                      isRepeatPasswordVisible
                                          ? Icons.visibility_off
                                          : Icons.visibility,
                                      color: App24Colors
                                          .greenOrderEssentialThemeColor,
                                    ),
                                  )))),
                      validator: (String? arg) {
                        if (arg != newPasswordController.text)
                          return 'password mismatch!';
                        else
                          return null;
                      },
                    ),
                    SizedBox(
                      height: 50,
                    ),
                    CommonButton(
                      onTap: () {},
                      bgColor: App24Colors.darkTextColor,
                      text: "SAVE",
                    ),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ),
            )),
          ),
        ) // This trailing comma makes auto-formatting nicer for build methods.
        );
  }
}
