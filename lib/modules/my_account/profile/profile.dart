import 'dart:io';
import 'dart:io' as IO;
import 'package:app24_user_app/Laundry-app/views/ServicesScreens/services_home_screen.dart';
import 'package:app24_user_app/Laundry-app/views/widgets/custum_appbar.dart';
import 'package:app24_user_app/constants/app_text.dart';
import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_coman.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/constants/textstyle.dart';
import 'package:app24_user_app/helpers.dart';
import 'package:app24_user_app/modules/home.dart';
import 'package:app24_user_app/modules/my_account/profile/country_model.dart';
import 'package:app24_user_app/modules/my_account/profile/profile_loading.dart';
import 'package:app24_user_app/modules/tabs/account/my_account_tab.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/utils/services/api/api_response_state.dart';
import 'package:app24_user_app/utils/services/api/api_services.dart';
import 'package:app24_user_app/widgets/bottomsheets/pick_image_bottom_sheet.dart';
import 'package:app24_user_app/widgets/common_appbar.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:app24_user_app/widgets/offline_builder_widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'change_password.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({super.key, this.isLaundryApp = false});
final bool? isLaundryApp;
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  reloadData({required BuildContext context, bool? init}) {
    return context.read(profileNotifierProvider).getProfile(init: init);
  }

  String title = "DropDown";
  String? _genderValue;
  City? city;
  List _gender = ["Male", "Female",/* "Non-binary", "Prefer not to say"*/];
  List<File> shopPictureList = [];
  List<File> documentPictureList = [];

  // late IO.File? selectedImage;
  bool isImageSelected = false;
  late FormData saveDetails = FormData();

  // String dropdownValue = 'Male';
  String selectedSex = "male";
 // TextEditingController countryController = new TextEditingController();

  @override
  void initState() {
    SchedulerBinding.instance!.addPostFrameCallback((_) {
      context.read(profileNotifierProvider.notifier).getProfile();
      // print("gender" + _genderValue.toString());
    });
    super.initState();
   // countryController.text = "+971";
  }

  onSaveDetails() async {
    saveDetails = FormData.fromMap({
      "first_name": context
          .read(profileNotifierProvider.notifier)
          .firstNameController
          .text,
      "last_name": context
          .read(profileNotifierProvider.notifier)
          .lastNameController
          .text,
      "mobile": context
          .read(profileNotifierProvider.notifier)
          .phoneNumberController
          .text,
      "email":
          context.read(profileNotifierProvider.notifier).emailController.text,
      "address":
          context.read(profileNotifierProvider.notifier).addressController.text,
      // "picture": context.read(profileNotifierProvider.notifier).profilePicture,
      "gender": _genderValue,
      // "id" : context.read(profileNotifierProvider.notifier).id.text,
      "country_name": context
          .read(profileNotifierProvider.notifier)
          .countrySelectController.text,
      "city_name":
          context.read(profileNotifierProvider.notifier).cityController.text,
    });

    // saveDetails.fields
    //   ..add(MapEntry(
    //     'type',
    //     'image',
    //   ));
    // saveDetails.files.add(MapEntry(
    //   'picture',
    //   await MultipartFile.fromFile(
    //     selectedImage!.path,
    //     filename: selectedImage!.path.split('/').last,
    //   ),
    // ));
    // showLog("file path : ${selectedImage!.path}");
    // showLog("file name : ${selectedImage!.path.split('/').last}");
    Logger.appLogs("body:: ${saveDetails.fields.toString()}");
    context.read(profileNotifierProvider.notifier).saveProfile(
          body: saveDetails,
        );
    Navigator.pop(context);
    /*Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => HomePage(currentIndex: 3,)),
    );*/
  }

  // pickImage() async {
  //   var res = await Helpers().getCommonBottomSheet(
  //     context: context,
  //     content: PickImageBottomSheet(
  //       crop: false,
  //     ),
  //     title: "Pick image from",
  //   );
  //
  //   if (res != null) {
  //     showLog(res.toString());
  //     setState(() {
  //       selectedImage = res;
  //       isImageSelected = true;
  //     });
  //   }
  // }

  // Widget buildPreviewImage() {
  //   return Container(
  //     decoration: BoxDecoration(
  //       color: Theme.of(context).accentColor,
  //       borderRadius: BorderRadius.circular(20.0),
  //       border: Border.all(color: Colors.grey.shade300, width: 1),
  //       image:
  //           DecorationImage(image: FileImage(selectedImage!), fit: BoxFit.fill),
  //     ),
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    InputDecoration _inputDecoration = InputDecoration(
      // fillColor: Colors.grey[200],
      // filled: true,
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
        borderSide: BorderSide(
          width: 0,
          style: BorderStyle.none,
        ),
      ),
      // fillColor: Theme.of(context).accentColor,
      // filled: true,
      contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 8),
    );
    return Scaffold(
      appBar: (widget.isLaundryApp ==true)
      ? PreferredSize(
        preferredSize: Size(double.infinity, 80),
        child: CusturmAppbar(
            child: ListTile(
              leading: backarrow(context: context,
                onTap: () {
                  Navigator.pop(context);
                },),
              title: Center(
                child: Text(Apptext.editProfile,
                    style: AppTextStyle().appbathead),
              ),
            )),
      )
      :CustomAppBar(
        hideCartIcon: "true",
        title: "Edit Profile",
      ),
      backgroundColor: Colors.white,
      body: OfflineBuilderWidget(
        SafeArea(
          child: Container(
            height: 10,
            padding: EdgeInsets.symmetric(horizontal: 25),
            decoration: BoxDecoration(
                // borderRadius: BorderRadius.only(
                //     topRight: Radius.circular(30), topLeft: Radius.circular(30)),
                color: Colors.white),
            child: SingleChildScrollView(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Consumer(
                      builder: (context, watch, child) {
                        final state = watch(profileNotifierProvider);
                        if (state is ResponseInitial) {
                          return Container(
                            child: Text("Initial"),
                          );
                        } else if (state.isLoading) {
                          return ProfileLoading();
                        } else if (state.isError) {
                          return LoadingError(
                            onPressed: (res) {
                              reloadData(init: true, context: res);
                            },
                            message: state.errorMessage.toString(),
                          );
                        } else {
                          return Column(children: [
                            // Row(
                            //   mainAxisAlignment: MainAxisAlignment.start,
                            //   children: <Widget>[
                            //     Stack(children: [
                            //       // Container(
                            //       //     margin:
                            //       //         EdgeInsets.only(top: 30, left: 100),
                            //       //     decoration: BoxDecoration(
                            //       //       borderRadius: BorderRadius.circular(70),
                            //       //       color: Colors.white,
                            //       //       boxShadow: [
                            //       //         BoxShadow(
                            //       //             color: Theme.of(context)
                            //       //                 .primaryColorLight
                            //       //                 .withOpacity(0.2),
                            //       //             blurRadius: 2,
                            //       //             spreadRadius: 4),
                            //       //       ],
                            //       //     ),
                            //       //     child: Material(
                            //       //       color: Colors.transparent,
                            //       //       child: InkWell(
                            //       //           borderRadius:
                            //       //               BorderRadius.circular(70),
                            //       //           onTap: () {
                            //       //             pickImage();
                            //       //           },
                            //       //           child: Container(
                            //       //             width: 120,
                            //       //             height: 120,
                            //       //             child: ClipRRect(
                            //       //               borderRadius:
                            //       //                   BorderRadius.circular(70),
                            //       //               child: isImageSelected == false
                            //       //                   ? CachedNetworkImage(
                            //       //                       imageUrl: context
                            //       //                           .read(
                            //       //                               profileNotifierProvider
                            //       //                                   .notifier)
                            //       //                           .profilePicture
                            //       //                           .toString(),
                            //       //                       fit: BoxFit.cover,
                            //       //                     )
                            //       //                   // Icon(
                            //       //                   //         Icons.person,
                            //       //                   //         size: 80,
                            //       //                   //         color: App24Colors
                            //       //                   //             .darkTextColor
                            //       //                   //             .withOpacity(0.6),
                            //       //                   //       )
                            //       //                   : buildPreviewImage(),
                            //       //             ),
                            //       //           )),
                            //       //     )),
                            //       // if (isImageSelected)
                            //         // Positioned(
                            //         //   right: 0,
                            //         //   top: 35,
                            //         //   child: Material(
                            //         //     color: Colors.transparent,
                            //         //     child: InkWell(
                            //         //       customBorder: CircleBorder(),
                            //         //       onTap: () {
                            //         //         setState(() {
                            //         //           selectedImage = null;
                            //         //           isImageSelected = false;
                            //         //         });
                            //         //       },
                            //         //       child: Container(
                            //         //         decoration: BoxDecoration(
                            //         //             shape: BoxShape.circle,
                            //         //             color: Colors.red),
                            //         //         child: Padding(
                            //         //           padding:
                            //         //               const EdgeInsets.all(5.0),
                            //         //           child: Icon(
                            //         //             Icons.close,
                            //         //             color: Colors.white,
                            //         //             size: 15,
                            //         //           ),
                            //         //         ),
                            //         //       ),
                            //         //     ),
                            //         //   ),
                            //         // )
                            //         // Positioned(
                            //         //   right: 0,
                            //         //   top:0,
                            //         //
                            //         //     child: Material(
                            //         //       color:Colors.transparent,
                            //         //   child: InkWell(
                            //         //     onTap: () {},
                            //         //     child: Container(
                            //         //       decoration: BoxDecoration(
                            //         //         color: Colors.white,
                            //         //         boxShadow: [BoxShadow(
                            //         //           color: Color(0xffefefef),
                            //         //           blurRadius: 3,
                            //         //           spreadRadius: 3
                            //         //         )
                            //         //
                            //         //         ],
                            //         //         borderRadius:
                            //         //             BorderRadius.circular(10),
                            //         //       ),
                            //         //       child: Row(
                            //         //         children: [
                            //         //           Icon(Icons.edit),
                            //         //           Text("Edit profile picture")
                            //         //         ],
                            //         //       ),
                            //         //     ),
                            //         //   ),
                            //         // ))
                            //     ]),
                            //   ],
                            // ),
                            const SizedBox(
                              height: 20,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                const SizedBox(
                                  height: 10,
                                ),
                                TextField(
                                    textInputAction: TextInputAction.done,
                                    controller: context
                                        .read(profileNotifierProvider.notifier)
                                        .firstNameController,
                                    style:
                                        TextStyle(fontWeight: FontWeight.w600),
                                    decoration: InputDecoration(
                                        focusColor: App24Colors.darkTextColor,
                                        labelText: "First Name",
                                        prefixIcon:
                                            Icon(Icons.account_circle_sharp))),
                                const SizedBox(
                                  height: 10,
                                ),
                                TextField(
                                    textInputAction: TextInputAction.done,
                                    controller: context
                                        .read(profileNotifierProvider.notifier)
                                        .lastNameController,
                                    style:
                                        TextStyle(fontWeight: FontWeight.w600),
                                    decoration: InputDecoration(
                                      prefixIcon:
                                          Icon(Icons.account_circle_sharp),
                                      labelText: "Last Name",
                                      enabledBorder: new UnderlineInputBorder(
                                        borderSide: BorderSide(
                                          color: App24Colors.darkTextColor,
                                          width: 1.0,
                                        ),
                                      ),
                                    )),
                                SizedBox(
                                  height: 20,
                                ),
                                Container(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 0, horizontal: 10),
                                  decoration: BoxDecoration(
                                    // color: Colors.grey[200],
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  child: DropdownButton(
                                      isExpanded: false,
                                      hint: Text(
                                        context
                                                    .read(
                                                        profileNotifierProvider
                                                            .notifier)
                                                    .gender
                                                    .toString() ==
                                                "MALE"
                                            ? "Male"
                                            : context
                                                        .read(
                                                            profileNotifierProvider
                                                                .notifier)
                                                        .gender
                                                        .toString() ==
                                                    "FEMALE"
                                                ? "Female"
                                                : context
                                                            .read(
                                                                profileNotifierProvider
                                                                    .notifier)
                                                            .gender
                                                            .toString() ==
                                                        ""
                                                    ? "Gender"
                                                    : context
                                                        .read(
                                                            profileNotifierProvider
                                                                .notifier)
                                                        .gender
                                                        .toString(),
                                        // "Gender",
                                        style: TextStyle(
                                            color: App24Colors.darkTextColor),
                                      ),
                                      elevation: 2,
                                      dropdownColor: Colors.white,
                                      iconEnabledColor:
                                          App24Colors.darkTextColor,
                                      style: TextStyle(
                                          color: App24Colors.darkTextColor,
                                          fontWeight: FontWeight.bold),
                                      value: _genderValue,
                                      onChanged: (dynamic value) {
                                        // print(_genderValue);
                                        setState(() {
                                          _genderValue = value;
                                        });
                                        // print(_genderValue);
                                      },
                                      items: _gender.map((value) {
                                        return DropdownMenuItem(
                                            value: value, child: Text(value));
                                      }).toList()),
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  children: <Widget>[
                                    //Image.asset(India),
                                   /* SizedBox(
                                      width: 100,
                                      child: TextField(
                                        toolbarOptions: ToolbarOptions(
                                          copy: false,
                                          cut: false,
                                          selectAll: false,
                                          paste: false
                                        ),
                                          textInputAction: TextInputAction.done,
                                          readOnly: true,
                                          controller: countryController,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600),
                                          textAlign: TextAlign.center,
                                          decoration: _inputDecoration.copyWith(
                                            labelText: "",
                                            prefix: Image.asset(
                                              App24UserAppImages.india,
                                              width: 27,
                                            ),
                                          )),
                                    ),*/
                                    countryPicker(),
                                    SizedBox(
                                      width: 15,
                                    ),
                                    Expanded(
                                      child: TextFormField(
                                        enabled: false,
                                          textInputAction: TextInputAction.done,
                                          controller: context
                                              .read(profileNotifierProvider
                                                  .notifier)
                                              .phoneNumberController,
                                          validator: (String? arg) {
                                            if (arg!.length == 0)
                                              return 'Contact Number can\'t be empty!';
                                            else if (arg.length > 10) {
                                              return "Contact number can\'t be more than 10 digits";
                                            } else if (arg.length < 10) {
                                              return "Contact number can\'t be less than 10 digits";
                                            }
                                            return null;
                                          },
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600),
                                          keyboardType: TextInputType.number,
                                          inputFormatters: <TextInputFormatter>[
                                            FilteringTextInputFormatter.allow(
                                                RegExp("[0-9]"))
                                          ],
                                          decoration: InputDecoration(
                                            prefixIcon: Icon(
                                                Icons.phone_android_rounded),
                                            labelText: "Phone Number",
                                          )),
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                TextField(
                                    textInputAction: TextInputAction.done,
                                    controller: context
                                        .read(profileNotifierProvider.notifier)
                                        .emailController,
                                    style:
                                        TextStyle(fontWeight: FontWeight.w600),
                                    keyboardType: TextInputType.emailAddress,
                                    decoration: InputDecoration(
                                      prefixIcon:
                                          Icon(Icons.alternate_email_rounded),
                                      labelText: "Email ID",
                                    )),
                                if(!widget.isLaundryApp!) SizedBox(
                                  height: 10,
                                ),
                                if(!widget.isLaundryApp!) TextField(
                                    textInputAction: TextInputAction.done,
                                    controller: context
                                        .read(profileNotifierProvider.notifier)
                                        .addressController,
                                    maxLines: 2,
                                    style:
                                        TextStyle(fontWeight: FontWeight.w600),
                                    decoration: InputDecoration(
                                      prefixIcon: Icon(Icons.location_pin),
                                      labelText: "Address",
                                    )),
                                SizedBox(
                                  height: 10,
                                ),
                                TextField(
                                  enabled: false,
                                    textInputAction: TextInputAction.done,
                                    controller: context
                                        .read(profileNotifierProvider.notifier)
                                        .countrySelectController,
                                    style:
                                        TextStyle(fontWeight: FontWeight.w600),
                                    decoration: InputDecoration(
                                        labelText: "Country",
                                        /*prefixIcon: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Image.asset(
                                            App24UserAppImages.india,
                                            width: 10,
                                          ),
                                        )*/
                                    )),
                                SizedBox(
                                  height: 10,
                                ),
                                TextField(
                                  enabled: false,
                                    textInputAction: TextInputAction.done,
                                    controller: context
                                        .read(profileNotifierProvider.notifier)
                                        .cityController,
                                    style:
                                        TextStyle(fontWeight: FontWeight.w600),
                                    decoration: InputDecoration(
                                      prefixIcon:
                                          Icon(Icons.location_city_rounded),
                                      labelText: "City",
                                    )),
                                SizedBox(
                                  height: 20,
                                ),
                                // Row(
                                //   mainAxisAlignment: MainAxisAlignment.end,
                                //   children: <Widget>[
                                //     TextButton(
                                //       onPressed: () {
                                //         Navigator.push(
                                //           context,
                                //           MaterialPageRoute(
                                //               builder: (context) =>
                                //                   ChangePasswordPage()),
                                //         );
                                //       },
                                //       child: Text(
                                //         "Change Password?",
                                //         style: TextStyle(
                                //             color: App24Colors.darkTextColor,
                                //             fontWeight: FontWeight.w600,
                                //             fontSize: 15),
                                //       ),
                                //     )
                                //   ],
                                // ),
                                if(!widget.isLaundryApp!) Container(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 0, horizontal: 10),
                                  decoration: BoxDecoration(
                                    // color: Colors.grey[200],
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  child: DropdownButton(
                                      isExpanded: true,
                                      hint: Text(
                                        "Select city",
                                        // "Gender",
                                        style: TextStyle(
                                            color: App24Colors.darkTextColor),
                                      ),
                                      elevation: 2,
                                      dropdownColor: Colors.white,
                                      iconEnabledColor:
                                      App24Colors.darkTextColor,
                                      style: TextStyle(
                                          color: App24Colors.darkTextColor,
                                          fontWeight: FontWeight.bold),
                                      value: city,
                                      onChanged: ( selectedCity) {
                                        // print(_genderValue);
                                        setState(() {
                                          city = selectedCity;
                                        });
                                        // print(_genderValue);
                                      },
                                      items: context
                                          .read(profileNotifierProvider.notifier)
                                        .cityList?.map((City city) {
                                        return DropdownMenuItem(
                                            value: city, child: Text(city.cityName ?? ''));
                                      }).toList()),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                              ],
                            ),
                          ]);
                        }
                      },
                    ),
                    Row(
                      children: [
                        Expanded(
                            child: MaterialButton(
                                onPressed: () {
                                  onSaveDetails();
                                },
                                color: App24Colors.darkTextColor,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                child: Padding(
                                  padding: EdgeInsets.symmetric(vertical: 17),
                                  // child:
                                  // ProviderListener(
                                  //   provider: profileNotifierProvider,
                                  //   onChange: (context, state) {
                                  //     if (state is ResponseLoaded) {
                                  //
                                  //
                                  //       showMessage(context, "Created Successfully");
                                  //       context.read(profileNotifierProvider.notifier).saveProfile(context: context,init: false);
                                  //       Navigator.pop(context);
                                  //     }
                                  //   },

                                  child: Consumer(
                                    builder: (context, watch, child) {
                                      final state =
                                          watch(profileNotifierProvider);
                                      if (state is ResponseInitial) {
                                        return Text(
                                          "Save Details",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold),
                                        );
                                      } else if (state.isLoading) {
                                        return SizedBox(
                                          child: CircularProgressIndicator
                                              .adaptive(),
                                          height: 30,
                                        );
                                      } else if (state.isError) {
                                        return Text(
                                          "Save Details",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold),
                                        );
                                      } else {
                                        return Text(
                                          "Save Details",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold),
                                        );
                                      }
                                    },
                                  ),
                                ))),
                        // ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                  ]),
            ),
          ),
        ),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  Widget countryPicker(){
    print("context.read(profileNotifierProvider.notifier).countryController.text :: ${context.read(profileNotifierProvider.notifier).countryController.text}");
    return CountryCodePicker(
      showCountryOnly: true,
      enabled: false,
      initialSelection: "+${context.read(profileNotifierProvider.notifier).countryController.text}",
      dialogBackgroundColor: Colors.white,
      padding: const EdgeInsets.all(0),
      textStyle: TextStyle(
        color: Colors.grey,
        fontSize: 16,
        fontWeight: FontWeight.w400,
      ),
    searchDecoration: InputDecoration(
        enabled: false,
        enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(color: AppColor.transparent)),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(color: AppColor.transparent)),
        focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(color: AppColor.transparent)),
        hintText: '',
      ),

      flagDecoration: BoxDecoration(
        borderRadius: BorderRadius.circular(1),
      ),
    );
  }

}
