import 'package:app24_user_app/constants/api_path.dart';
import 'package:app24_user_app/modules/my_account/profile/country_model.dart';
import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/api/api_response_state2.dart';
import 'package:app24_user_app/utils/services/api/api_services.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class ProfileNotifier extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  ProfileNotifier(this._apiRepository) : super(ResponseState2.initial());

  TextEditingController firstNameController = new TextEditingController();
  TextEditingController lastNameController = new TextEditingController();
  TextEditingController phoneNumberController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  TextEditingController addressController = new TextEditingController();
  TextEditingController countrySelectController = new TextEditingController();
  TextEditingController cityController = new TextEditingController();
  TextEditingController countryController = new TextEditingController();
  String? profilePicture;
  String? referralCode;
  int? referralAmount;
  int? referralCount;
  List<City>? cityList;

  // TextEditingController id = new TextEditingController();
  String? gender;

  Future<void> getProfile({bool init = true,}) async {
    try {
      if (init) state = state.copyWith(isLoading: true);
      final profile = await _apiRepository.fetchProfile();
      firstNameController.text = profile.responseData!.firstName ?? "";
      lastNameController.text = profile.responseData!.lastName ?? "";
      phoneNumberController.text = profile.responseData!.mobile ?? "";
      emailController.text = profile.responseData!.email.toString();
      addressController.text = profile.responseData!.address ?? "";
      countrySelectController.text = profile.responseData!.country!.countryName ?? "";
      cityController.text = profile.responseData!.city!.cityName ?? "";
      // profilePicture = profile.responseData!.picture ?? "";
      countryController.text = profile.responseData?.countryCode ?? '+971';
      referralCode = profile.responseData!.referral!.referralCode ?? "";
      referralAmount = (profile.responseData!.referral!.referralAmount ?? "") as int?;
      referralCount = (profile.responseData!.referral!.referralCount ?? "") as int?;
      // id.text = profile.responseData.id.toString();
      gender = profile.responseData!.gender!;
      if(profile.statusCode == "200"){
        getCity(init: init,countryId: profile.responseData!.countryId);
      }
      state =
          state.copyWith(response: profile, isLoading: false, isError: false);
    } catch (e) {
      print(e.toString());
      state = state.copyWith(
          errorMessage: e.toString(), isError: true, isLoading: false);
    }
  }

  Future<void> saveProfile(
      {bool init = true, FormData? body}) async {
    try {
      if (init) state = state.copyWith(isLoading: true);

      final saveProfile = await _apiRepository.fetchSaveProfile( body);

      state = state.copyWith(
          response: saveProfile, isLoading: false, isError: false);
    } catch (e) {
      print(e.toString());
      state = state.copyWith(
          errorMessage: e.toString(), isError: true, isLoading: false);
    }
  }

  Future<CountryModel> fetchProfileCountry() async {
    RestAPIService restAPIService = RestAPIService(Dio());
    var result = await restAPIService.postService(
        url: App24UserAppAPI.countries, body: {'salt_key': 'MQ=='});
    return CountryModel.fromJson(result as Map<String, dynamic>);
  }

  Future<void> getCity({bool init = true, int? countryId})async {
    try {
      if (init) state = state.copyWith(isLoading: true);

      final profileCountry = await fetchProfileCountry();
      if(profileCountry.responseData?.length != null) {
        for (int i = 0; i < (profileCountry.responseData?.length ?? 0); i++) {
          if (countryId == profileCountry.responseData?[i].id) {
            cityList = profileCountry.responseData?[i].city;
            cityList?.insert(0,City(id: 0,cityName: 'Select City'));
          }
        }
      }
      Logger.appLogs("cityList :: ${cityList.toString()}");
      state = state.copyWith(
          response: profileCountry, isLoading: false, isError: false);
    } catch (e) {
      print(e.toString());
      state = state.copyWith(
          errorMessage: e.toString(), isError: true, isLoading: false);
    }
  }

}
