import 'package:app24_user_app/Laundry-app/views/ServicesScreens/services_home_screen.dart';
import 'package:app24_user_app/Laundry-app/views/widgets/custum_appbar.dart';
import 'package:app24_user_app/app24_user_icons.dart';
import 'package:app24_user_app/constants/api_path.dart';
import 'package:app24_user_app/constants/app_text.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/constants/global_constants.dart';
import 'package:app24_user_app/constants/textstyle.dart';
import 'package:app24_user_app/helpers.dart';
import 'package:app24_user_app/modules/my_account/address/manage_address_loading.dart';
import 'package:app24_user_app/modules/my_account/address/manage_address_model.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/widgets/common_appbar.dart';
import 'package:app24_user_app/widgets/common_main_button.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:app24_user_app/widgets/offline_builder_widget.dart';
import 'package:app24_user_app/widgets/select_location.dart';
import 'package:app24_user_app/widgets/select_location_address.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'add_update_address.dart';

class ManageAddressPage extends StatefulWidget {
  final String? callFrom;
  final bool? isLaundryApp;

  const ManageAddressPage({super.key, this.callFrom, this.isLaundryApp = false});

  @override
  _ManageAddressPageState createState() => _ManageAddressPageState();
}

class _ManageAddressPageState extends State<ManageAddressPage> {

  reloadData({required BuildContext context, bool? init}) {
    return context.read(addressNotifierProvider.notifier).getAddresses();
  }

  // List<SavedAddress> _addressList = [];
  int selectedDriver = -1;

  @override
  void initState() {
    /*context
        .read(addUpdateAddressNotifierProvider.notifier)
        .getAddresses( init: false);*/
   // context.read(addressNotifierProvider.notifier).getAddresses();
    SchedulerBinding.instance.addPostFrameCallback((_) {
    //  print("in callback");
      //reloadData(context: context);
      context.read(addressNotifierProvider.notifier).getAddresses();

    });
    super.initState();
  }

  List<AddressResponseData>? addressList;

  // List<AddressResponseData>? addressID;

  removeAddress(index, String addressID) async {
    await context
        .read(addressNotifierProvider.notifier)
        .deleteAddress(addressID: addressID);

    context.read(addressNotifierProvider.notifier).getAddresses();
    // context.read(cartNotifierProvider.notifier).getCartList(init: false);
  }

  // context.read(addressNotifierProvider.notifier).deleteAddress(addressID: addressID)

  List<Widget> getAddress(context, List<AddressResponseData> addressModel) {
    List<Widget> _widgetServices = [];

    _widgetServices.add(ListView.builder(
        shrinkWrap: true,
        padding: EdgeInsets.all(10),
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (context, i) {
          return Stack(children: [
            Container(
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey[200]!,
                      blurRadius: 10,
                      spreadRadius: 5,
                    )
                  ],
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(20),
                      bottomLeft: Radius.circular(20),
                      bottomRight: Radius.circular(20)),
                  color: Colors.white),
              margin: EdgeInsets.symmetric(vertical: 20, horizontal: 18),
              child: Material(
                borderRadius: BorderRadius.circular(15),
                child: InkWell(
                  borderRadius: BorderRadius.circular(15),
                  onTap: () {
                    if (widget.callFrom != null)
                      Navigator.pop(context,
                          addressModel[i].flatNo! + addressModel[i].street!);
                  },
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    // crossAxisAlignment:
                    //     CrossAxisAlignment.stretch,
                    children: [
                      const SizedBox(
                        height: 5,
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            left: 25,
                            right: 17,
                            top: 25,
                            bottom: widget.callFrom == null ? 0 : 10),
                        child: Text(
                          addressModel[i].flatNo.toString() +
                              " , " +
                              addressModel[i].landmark.toString() +
                              " , " +
                              addressModel[i].mapAddress.toString() +
                              " , " +
                              addressModel[i].street.toString(),
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize:
                                  15 /*MediaQuery.of(context).size.width / 25*/),
                        ),
                      ),
                      if (widget.callFrom == null)
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            TextButton(
                              style: TextButton.styleFrom(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20))),
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          AddUpdateAddressPage(
                                            isLaundryApp: widget.isLaundryApp,
                                            addressResponseData:
                                                addressModel[i],


                                          )),
                                );
                              },
                              child: Row(
                                children: [
                                  Icon(
                                    App24User.pencil,
                                    color: App24Colors.darkTextColor,
                                    size:
                                        MediaQuery.of(context).size.width / 25,
                                  ),
                                  const SizedBox(
                                    width: 5,
                                  ),
                                  Text(
                                    "Edit",
                                    style: TextStyle(
                                      color: App24Colors.darkTextColor,
                                      fontSize:
                                          MediaQuery.of(context).size.width /
                                              25,
                                    ),
                                  )
                                ],
                              ),
                            ),
                            TextButton(
                              style: TextButton.styleFrom(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20))),
                              onPressed: () {
                                // context.read(addressNotifierProvider.notifier).deleteAddress(addressID: addressModel[i].id.toString());
                                // setState(() {

                                Helpers().getCommonAlertDialogBox(
                                    content: AlertDialog(
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(15)),
                                      title: Text("Delete"),
                                      content: Text(
                                          "Are you sure you want to delete this address?"),
                                      actions: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          // crossAxisAlignment: CrossAxisAlignment.stretch,
                                          children: [
                                            Expanded(
                                                child: MaterialButton(
                                              onPressed: () {
                                                print("delete : " +
                                                    addressModel[i]
                                                        .addressid
                                                        .toString());

                                                Navigator.pop(context);
                                                removeAddress(
                                                    i,
                                                    addressModel[i]
                                                        .addressid
                                                        .toString());
                                              },
                                              child: Text(
                                                "Yes",
                                                style: TextStyle(
                                                    color: Colors.white),
                                              ),
                                              color: App24Colors
                                                  .greenOrderEssentialThemeColor,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10)),
                                            )),
                                            const SizedBox(width: 10),
                                            Expanded(
                                                child: MaterialButton(
                                              onPressed: () {
                                                Navigator.pop(context);
                                              },
                                              child: Text(
                                                "No",
                                                style: TextStyle(
                                                    color: Colors.white),
                                              ),
                                              color: App24Colors
                                                  .redRestaurantThemeColor,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10)),
                                            )),
                                          ],
                                        )
                                      ],
                                    ),
                                    context: context);

                                //

                                //   // addressModel.removeAt(index);
                                //   // getAddress(context, addressModel).removeAt(i);
                                // });
                              },
                              child: Row(
                                children: [
                                  Icon(
                                    App24User.trash,
                                    color: Color(0xffFF5D44),
                                    size:
                                        MediaQuery.of(context).size.width / 25,
                                  ),
                                  const SizedBox(
                                    width: 5,
                                  ),
                                  Text(
                                    "Delete",
                                    style: TextStyle(
                                      color: Color(0xffFF5D44),
                                      fontSize:
                                          MediaQuery.of(context).size.width /
                                              25,
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        )
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 18,right: 18),
              child: Container(
                width: MediaQuery.of(context).size.width/2,
                padding: EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                decoration: BoxDecoration(
                    color: App24Colors.darkTextColor,
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(15),
                        topLeft: Radius.circular(15),
                        bottomRight: Radius.circular(15))),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Icon(
                      addressModel[i].addressType == "Home"
                          ? App24User.home_1
                          : addressModel[i].addressType == "Work"
                              ? App24User.apartment
                              : App24User.flag,
                      color: Colors.white,
                      size: MediaQuery.of(context).size.width / 25,
                    ),
                    const SizedBox(
                      width: 9,
                    ),
                    Flexible(
                      child: Text(
                        // _addressList[index].label,
                        addressModel[i].title!,
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          color: Colors.white,
                          fontSize: MediaQuery.of(context).size.width / 27,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ]);
        },
        itemCount: addressModel.length));

    return _widgetServices;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: (widget.isLaundryApp == true)
            ? PreferredSize(
          preferredSize: Size(double.infinity, 80),
          child: CusturmAppbar(
              child: ListTile(
                leading: backarrow(
                  context: context,
                  onTap: () {
                    Navigator.pop(context);
                  },
                ),
                title: Center(
                  child: Text(Apptext.manageAddress, style: AppTextStyle().appbathead),
                ),
              )),
        )
            :CustomAppBar(
          hideCartIcon: "true",
          title: "Manage Address",
        ),
        resizeToAvoidBottomInset: true,
        backgroundColor: Colors.white,
        body: OfflineBuilderWidget(
          SafeArea(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  const SizedBox(
                    height: 10,
                  ),
                  Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            const SizedBox(
                              height: 5,
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 20),
                              child: getSubTitle("Saved Addresses", context),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Consumer(
                              builder: (context, watch, child) {
                                final state = watch(addressNotifierProvider);
                                print("agan state: "+state.toString());
                                if (state.isLoading) {
                                  return ManageAddressLoading();
                                } else if (state.isError) {
                                  return LoadingError(
                                    onPressed: (res) {
                                      reloadData(init: true, context: res);
                                    },
                                    message: state.errorMessage.toString(),
                                  );
                                } else {
                                  addressList = state.response;
                                  if (addressList == null ||
                                      addressList!.length == 0)
                                    return Container(
                                      child: Center(
                                        child: Column(
                                          children: [
                                            // Icon(Icons.),
                                            Text("No addresses added."),
                                          ],
                                        ),
                                      ),
                                    );
                                  // print(addressList!.length);
                                  return Column(
                                      children:
                                          getAddress(context, addressList!));
                                }
                              },
                            )
                          ]),
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Divider(
                        height: 1,
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Padding(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: CommonButton(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        SelectLocationAddressPage(
                                          searchUserLocation: false,
                                          title: GlobalConstants.labelPickupLocation,
                                          fromManageAddress: "fromManageAddress",
                                          isCurrentLocation: true,
                                          fromCart: "false",
                                        )
                                )

                                  // SelectLocationPage(
                                        //   isSavedAddress: false,
                                        //   title: 'Enter Location',
                                        //   fromManageAddress:
                                        //       "fromManageAddress",
                                        //   isCurrentLocation: true,
                                        // )
                                    // AddUpdateAddressPage()
                                    );

                            },
                            bgColor: App24Colors.darkTextColor,
                            text: "ADD NEW ADDRESS",
                          )),
                      const SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ]),
          ),
        ) // This trailing comma makes auto-formatting nicer for build methods.
        );
  }
}

// class SavedAddress {
//   String address, house_name, street, landmark, label;
//
//   SavedAddress(
//       {this.label, this.address, this.house_name, this.landmark, this.street});
// }
