import 'dart:convert';

import 'package:app24_user_app/modules/my_account/address/manage_address_model.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/api/api_response_state2.dart';
import 'package:app24_user_app/utils/services/database/address_database.dart';
import 'package:app24_user_app/utils/services/database/database_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'manage_add_address_model.dart';
import 'manage_edit_address_model.dart';

class AddressNotifier extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  AddressNotifier(this._apiRepository) : super(ResponseState2.initial());

  // String addressType;
  // var flatNo;
  // var streetName;
  // var landmark;
  var addressID;
  AddressDatabase addressDatabase =
  new AddressDatabase(DatabaseService.instance);

  Future<void> getAddresses({bool init = true}) async {
    try {
      if (init) state = state.copyWith(isLoading: true);

      AddressDatabase addressDatabase = AddressDatabase(
          DatabaseService.instance);
      //final list = await addressDatabase.fetchAddresses();
      List<AddressResponseData>? list = await addressDatabase.fetchAddresses();


      final address;
      if (list.isEmpty) {
        print("AddressResponseData work if" );
        address = await _apiRepository.fetchManageAddress();
      }
      else {
        print("AddressResponseData work else");
        address = list;
      }


      // addressType =address.responseData.first.addressType;
      // flatNo = address.responseData.first.flatNo;
      // streetName = address.responseData.first.street;

      state =
          state.copyWith(response: list, isLoading: false, isError: false);
    } catch (e) {
      print(e.toString());
      state = state.copyWith(
          errorMessage: e.toString(), isError: true, isLoading: false);
    }
  }


  saveAddress(
      {bool init = true, required Map body, required int? addressId,BuildContext? context}) async {
    try {
      if (init) state = state.copyWith(isLoading: true);


    if (addressId == 0) {
      final saveAddress = await _apiRepository.fetchSaveAddress(body);
      ManageAddedAddressModel? addressList = ManageAddedAddressModel.fromJson(
          saveAddress);
      addressID=addressList.responseData!.id;
      addressDatabase.addUserAddress(new AddressResponseData(
        userId: addressList.responseData!.userId,
        state: addressList.responseData!.state,
        companyId: addressList.responseData!.companyId,
        addressid: addressList.responseData!.id,
        addressType: addressList.responseData!.title,
        landmark: addressList.responseData!.landmark!,
        flatNo: addressList.responseData!.flatNo,
        street: addressList.responseData!.street,
        city: addressList.responseData!.city,
        county: addressList.responseData!.county!,
        title: addressList.responseData!.title,
        zipcode: addressList.responseData!.zipcode,
        latitude: addressList.responseData!.latitude,
        longitude: addressList.responseData!.longitude,
        mapAddress: addressList.responseData!.mapAddress,
     ));
      showMessage(context!, "Completed successfully", false, false);

    }
    else {
      final saveAddress = await _apiRepository.fetchEditAddress(body);
      ManageEditedAddressModel? addressList = ManageEditedAddressModel.fromJson(
          saveAddress);

      addressDatabase.updateAddress(new AddressResponseData(
        userId: addressList.responseData!.userId,
        state: addressList.responseData!.state,
        companyId: addressList.responseData!.companyId,
        addressid: addressList.responseData!.id,
        addressType: addressList.responseData!.title,
        landmark: addressList.responseData!.landmark!,
        flatNo: addressList.responseData!.flatNo,
        street: addressList.responseData!.street,
        city: addressList.responseData!.city,
        county: addressList.responseData!.county!,
        title: addressList.responseData!.title,
        zipcode: addressList.responseData!.zipcode,
        latitude: addressList.responseData!.latitude,
        longitude: addressList.responseData!.longitude,
        mapAddress: addressList.responseData!.mapAddress,));
      showMessage(context!, "Completed successfully", false, false);
    }
    state = state.copyWith(
        response: saveAddress, isLoading: false, isError: false);

  }
catch (e) {

      state = state.copyWith(
          errorMessage: e.toString(), isError: true, isLoading: false);
    }

 }

  Future<void> deleteAddress({required String addressID}) async {
    // showLog("addressID" + addressID);
    try {
      state = state.copyWith(isLoading: true);

      final deleteAddress =
          await _apiRepository.fetchDeleteAddress(addressID: addressID);

      addressDatabase.deleteUserAddress(addressID: addressID);

      state = state.copyWith(
          response: deleteAddress, isLoading: false, isError: false);
    } catch (e) {

      state = state.copyWith(
          errorMessage: e.toString(), isError: true, isLoading: false);
    }
  }

  Future<int>  getAddressTypes({required String? addressTypeSelected,required BuildContext context}) async{

    AddressDatabase addressDatabase = new AddressDatabase(
        DatabaseService.instance);
    bool b = await addressDatabase.fetchAddressTypesInDB( addressTypeSelected: addressTypeSelected);
    if(b)
    {
      // showMessage(context, "already there", true, false);
      return 1;
    }
    else {
      //showMessage(context, "not there", false, false);

      return 0;

    }


  }



}