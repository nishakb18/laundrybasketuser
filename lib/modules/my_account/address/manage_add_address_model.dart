class ManageAddedAddressModel {
  String? statusCode;
  String? title;
  String? message;
  ManageAddedResponseData? responseData;
  ManageAddedResponseData? responseNewData;
  List<dynamic>? error;

  ManageAddedAddressModel(
      {this.statusCode,
        this.title,
        this.message,
        this.responseData,
        this.responseNewData,
        this.error});

  ManageAddedAddressModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    responseData = json['responseData'] != null
        ? new ManageAddedResponseData.fromJson(json['responseData'])
        : null;
    responseNewData = json['responseNewData'] != null
        ? new ManageAddedResponseData.fromJson(json['responseNewData'])
        : null;
    if (json['error'] != null) {
      error = [];
      json['error'].forEach((v) {
        error!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['title'] = this.title;
    data['message'] = this.message;
    if (this.responseData != null) {
      data['responseData'] = this.responseData!.toJson();
    }
    if (this.responseNewData != null) {
      data['responseNewData'] = this.responseNewData!.toJson();
    }
    if (this.error != null) {
      data['error'] = this.error!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ManageAddedResponseData {
  int? id;
  int? userId;
  int? companyId;
  String? addressType;
  String? landmark;
  String? flatNo;
  String? street;
  String? city;
  String? state;
  String? county;
  String? title;
  String? zipcode;
  double? latitude;
  double? longitude;
  String? mapAddress;
  int? addressid;
  int? priority;

  ManageAddedResponseData(
      {this.id,
        this.userId,
        this.companyId,
        this.addressType,
        this.landmark,
        this.flatNo,
        this.street,
        this.city,
        this.state,
        this.county,
        this.title,
        this.zipcode,
        this.latitude,
        this.longitude,
        this.mapAddress,
        this.addressid,
      this.priority});

  ManageAddedResponseData.fromJson(Map<String, dynamic> json) {
   // print("id in here to json from json"+json['id'].toString());
    id = json['id'];
    userId = json['user_id'];
    companyId = json['company_id'];
    addressType = json['address_type'];
    landmark = json['landmark'];
    flatNo = json['flat_no'];
    street = json['street'];
    city = json['city'];
    state = json['state'];
    county = json['county'];
    title = json['title'];
    zipcode = json['zipcode'];
    latitude =json['latitude'];
    longitude =json['longitude'];
    mapAddress = json['map_address'];
    addressid = json['addressid'];
    priority = json['priority'];
  }
  Map<String, dynamic> toMap() {
    return {
      // 'id' :id,
      'user_id':userId,
      'company_id':companyId,
      'address_type':addressType,
      'landmark' :landmark,
      'flat_no' :flatNo,
      'street' :street,
      'city' :city,
      'state' :state,
      'county' :county,
      'title' :title,
      'zipcode' :zipcode,
      'latitude' :latitude,
      'longitude' :longitude,
      'map_address' :mapAddress,
      'address_table_id' :addressid,
      'priority' :priority
    };
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
   // print("id in here to json"+this.id.toString());
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['company_id'] = this.companyId;
    data['address_type'] = this.addressType;
    data['landmark'] = this.landmark;
    data['flat_no'] = this.flatNo;
    data['street'] = this.street;
    data['city'] = this.city;
    data['state'] = this.state;
    data['county'] = this.county;
    data['title'] = this.title;
    data['zipcode'] = this.zipcode;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['map_address'] = this.mapAddress;
    data['addressid'] = this.addressid;
    data['priority'] = this.priority;
    return data;
  }
}