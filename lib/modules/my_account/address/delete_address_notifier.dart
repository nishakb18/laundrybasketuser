import 'package:app24_user_app/models/history_model.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/api/api_response_state2.dart';
import 'package:app24_user_app/utils/services/database/database_service.dart';
import 'package:app24_user_app/utils/services/database/history_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class DeleteAddressNotifier extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  DeleteAddressNotifier(this._apiRepository)
      : super(ResponseState2(isLoading: true));

  HistoryDatabase historyDatabase = HistoryDatabase(DatabaseService.instance);

  Future<void> deleteAddress({bool init = true,required String addressID}) async {
    try {
      if (init) state = state.copyWith(isLoading: true);
      final deleteAddress = await _apiRepository.fetchDeleteAddress(addressID: addressID);
      state = state.copyWith(
          response: deleteAddress, isLoading: false, isError: false);
    } catch (e) {
      showLog(e.toString());
      state = state.copyWith(
          isError: true, isLoading: false, errorMessage: e.toString());
    }
  }

}
