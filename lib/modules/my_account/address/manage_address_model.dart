ManageAddressModel manageAddressModelFromJson(str) {
  return ManageAddressModel.fromJson(str);
}
AddressResponseData addressResponseDataFromJson(str) {
  return AddressResponseData.fromJson(str);
}

class ManageAddressModel {
  String? statusCode;
  String? title;
  String? message;
  List<AddressResponseData>? responseData;
  List<dynamic>? error;

  ManageAddressModel(
      {this.statusCode,
      this.title,
      this.message,
      this.responseData,
      this.error});

  ManageAddressModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    if (json['responseData'] != null) {
      responseData = [];
      json['responseData'].forEach((v) {
        responseData!.add(new AddressResponseData.fromJson(v));
      });
    }
    if (json['error'] != null) {
      error = [];
      json['error'].forEach((v) {
        error!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['title'] = this.title;
    data['message'] = this.message;
    if (this.responseData != null) {
      data['responseData'] = this.responseData!.map((v) => v.toJson()).toList();
    }
    if (this.error != null) {
      data['error'] = this.error!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class AddressResponseData {
  int? id;
  int? userId;
  int? companyId;
  String? addressType;
  String? landmark;
  String? flatNo;
  String? street;
  String? city;
  String? state;
  String? county;
  String? title;
  String? zipcode;
  double? latitude;
  double? longitude;
  String? mapAddress;
  int? addressid;
  int? priority;

  AddressResponseData({
      this.id,
      this.userId,
      this.companyId,
      this.addressType,
      this.landmark,
      this.flatNo,
      this.street,
      this.city,
      this.state,
      this.county,
      this.title,
      this.zipcode,
      this.latitude,
      this.longitude,
      this.mapAddress,
      this.addressid,
  this.priority});

  AddressResponseData.fromJson(Map<String, dynamic> json) {
    print("id here in  fromJson"+json['address_table_id'].toString());
  //  id = json['id'];
    userId = json['user_id'];
    companyId = json['company_id'];
    addressType = json['address_type'];
    landmark = json['landmark'];
    flatNo = json['flat_no'];
    street = json['street'];
    city = json['city'];
    state = json['state'];
    county = json['county'];
    title = json['title'];
    zipcode = json['zipcode'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    mapAddress = json['map_address'];
    addressid = json['id'];
    priority = json['priority'];
  }

  AddressResponseData.fromJsonDB(Map<String, dynamic> json) {
    print("id here in  fromJson"+json['address_table_id'].toString());
    //id = json['id'];
    userId = json['user_id'];
    companyId = json['company_id'];
    addressType = json['address_type'];
    landmark = json['landmark'];
    flatNo = json['flat_no'];
    street = json['street'];
    city = json['city'];
    state = json['state'];
    county = json['county'];
    title = json['title'];
    zipcode = json['zipcode'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    mapAddress = json['map_address'];
    addressid = json['address_table_id'];
    priority = json['priority'];
  }


  Map<String, dynamic> toMap() {
    return {
   // 'id' :id,
    'user_id':userId,
    'company_id':companyId,
    'address_type':addressType,
    'landmark' :landmark,
   'flat_no' :flatNo,
    'street' :street,
    'city' :city,
    'state' :state,
    'county' :county,
    'title' :title,
    'zipcode' :zipcode,
    'latitude' :latitude,
   'longitude' :longitude,
    'map_address' :mapAddress,
    'address_table_id' :addressid,
    'priority' :priority
    };
  }
  Map<String, dynamic> toJson() {
    print("id here in toJson"+addressid.toString());

    final Map<String, dynamic> data = new Map<String, dynamic>();
    //data['id'] = this.id;
    data['user_id'] = this.userId;
    data['company_id'] = this.companyId;
    data['address_type'] = this.addressType;
    data['landmark'] = this.landmark;
    data['flat_no'] = this.flatNo;
    data['street'] = this.street;
    data['city'] = this.city;
    data['state'] = this.state;
    data['county'] = this.county;
    data['title'] = this.title;
    data['zipcode'] = this.zipcode;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['map_address'] = this.mapAddress;
    data['address_table_id'] = this.addressid;
    data['priority'] = this.priority;
    return data;
  }
}
