import 'package:app24_user_app/Laundry-app/views/ServicesScreens/services_home_screen.dart';
import 'package:app24_user_app/Laundry-app/views/widgets/custum_appbar.dart';
import 'package:app24_user_app/app24_user_icons.dart';
import 'package:app24_user_app/constants/app_text.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/constants/textstyle.dart';
import 'package:app24_user_app/helpers.dart';
import 'package:app24_user_app/models/location_model.dart';
import 'package:app24_user_app/modules/delivery/cart/cart.dart';
import 'package:app24_user_app/modules/delivery/cart/cart_model.dart';
import 'package:app24_user_app/modules/delivery/order_essentials/order_essentials_model.dart';
import 'package:app24_user_app/modules/delivery/order_essentials/order_essentials_page.dart';
import 'package:app24_user_app/modules/delivery/shop_list/shop_list_model.dart';
import 'package:app24_user_app/modules/my_account/address/manage_address.dart';
import 'package:app24_user_app/modules/my_account/address/manage_address_model.dart';
import 'package:app24_user_app/modules/tabs/home/home_tab.dart';
import 'package:app24_user_app/modules/tabs/account/my_account_tab.dart';
import 'package:app24_user_app/providers/location_picker_provider.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/api/api_response_state.dart';
import 'package:app24_user_app/utils/services/api/api_services.dart';
import 'package:app24_user_app/utils/services/database/address_database.dart';
import 'package:app24_user_app/utils/services/database/database_service.dart';
import 'package:app24_user_app/utils/services/database/location_database.dart';
import 'package:app24_user_app/widgets/bottomsheets/addressConfirmation/addressConfirmation.dart';
import 'package:app24_user_app/widgets/bottomsheets/checkout_bottomSheet/checkout_bottomsheet.dart';
import 'package:app24_user_app/widgets/common_appbar.dart';
import 'package:app24_user_app/widgets/common_main_button.dart';
import 'package:app24_user_app/widgets/offline_builder_widget.dart';
import 'package:app24_user_app/widgets/select_location_address.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../home.dart';

class AddUpdateAddressPage extends StatefulWidget {
/*  AddUpdateAddressPage({Key key, this.address1,this.landmark,this.city,this.country,this.address2,this.pinCode}) : super(key: key);
  final String address1;
  final String address2;
  final String landmark;
  final String city;
  final String country;
  final int pinCode;*/
  final AddressResponseData? addressResponseData;
  final String? fromCart;
  final ShopNamesModelResponseData? shopResponseData;
  final String? storeTypeIdFromAddList;
  final String? addListText;
  final String? addListFilePath;
  final bool? isImageSelectedAddList;
  final bool? searchUserLocation;
  final String? fromManageAddress;
  final bool? isLaundryApp;

  const AddUpdateAddressPage(
      {Key? key,
      this.addressResponseData,
      this.selectedLocation,
      this.fromCart,
      this.map,
      this.shopResponseData,
      this.storeTypeIdFromAddList,
      this.addListText,
      this.addListFilePath,
      this.isImageSelectedAddList,
      this.searchUserLocation,
      this.fromManageAddress, this.isLaundryApp = false})
      : super(key: key);

  final LocationModel? selectedLocation;
  final Map<dynamic, dynamic>? map;

  @override
  _AddUpdateAddressPageState createState() => _AddUpdateAddressPageState();
}

class _AddUpdateAddressPageState extends State<AddUpdateAddressPage> {
  TextEditingController? _labelController = new TextEditingController();
  TextEditingController? _saveAsController = new TextEditingController(text: "Others");
  TextEditingController? addressLine1 = new TextEditingController();
  TextEditingController? addressLine2 = new TextEditingController();
  TextEditingController? landmark = new TextEditingController();
  TextEditingController? pinCode = new TextEditingController();
  TextEditingController? city ;
  TextEditingController? country = new TextEditingController();
  TextEditingController? id = new TextEditingController();
  TextEditingController? latitude = new TextEditingController();
  TextEditingController? longitude = new TextEditingController();
  int? addressId = 0;
  late LocationNotifier _locationPickerProvider;

  // TextEditingController mapAddress = new TextEditingController();
  late Map cartContentBody;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _formKeySaveAs = GlobalKey<FormState>();
  String? cityName;

  @override
  void initState() {
    print("from cart value: " + widget.fromCart.toString());
    print("from cart secondary_text: ${widget.selectedLocation!.secondary_text.toString()}" );
    cityName = widget.selectedLocation!.secondary_text;
    print("from cart secondary_text: ${cityName}" );
    city = new TextEditingController(text: cityName);
    loadData();

    super.initState();
  }

  loadData() async {
    if (widget.addressResponseData != null) {
      addressLine1!.text = widget.addressResponseData!.flatNo ?? "";
      addressLine2!.text = widget.addressResponseData!.street ?? "";
      landmark!.text = widget.addressResponseData!.landmark ?? "";
      city!.text = widget.addressResponseData!.mapAddress ?? "";
      country!.text = widget.addressResponseData!.county ?? "";
      pinCode!.text = widget.addressResponseData!.zipcode.toString();
      id!.text = widget.addressResponseData!.id.toString();
      latitude!.text = widget.addressResponseData!.latitude.toString();
      longitude!.text = widget.addressResponseData!.longitude.toString();
      _saveAsController!.text = widget.addressResponseData!.title.toString();
      addressId = widget.addressResponseData!.addressid;
      // mapAddress.text = widget.addressResponseData.mapAddress;
    }
    if (widget.selectedLocation != null) {
      //print("city" + widget.selectedLocation!.priority.toString());

      //  city!.text = widget.selectedLocation!.secondary_text!;
      //  addressLine1!.text = widget.selectedLocation!.main_text!;
      addressLine2!.text = widget.selectedLocation!.description!;
      latitude!.text = widget.selectedLocation!.latitude!.toString();
      longitude!.text = widget.selectedLocation!.longitude!.toString();

      // city!.text = widget
      // landmark!.text = widget.selectedLocation!.landmark ?? "";
      // city!.text = widget.selectedLocation!.secondary_text ?? "" ;
      // country!.text = widget.selectedLocation!.description ?? "";
      // pinCode!.text = widget.selectedLocation!.placeID ?? "";
      // id!.text = widget.selectedLocation!.type.toString();
    }
  }

  void saveToDbAndServer(LocationModel? location) async {
    print("in save to db and server");

    LocationDatabase locationDatabase =
        LocationDatabase(DatabaseService.instance);

    await locationDatabase.insertLocation(LocationModel(
        placeID: location?.placeID ?? '',
        latitude: location!.latitude,
        longitude: location.longitude,
        description: location.description,
        main_text: location.main_text,
        secondary_text: location.secondary_text,
        type: 3));

    if (location.pinColor != null && location.pinColor == Colors.red) {
      var body = {
        'mPlaceId': location.placeID,
        'mPrimary': location.main_text,
        'mSecondary': location.secondary_text == ""
            ? location.secondary_text == "unnamed"
            : location.secondary_text,
        'mFullAddress': location.description!,
        'lat': location.latitude,
        'lng': location.longitude
      };
      print("dony body: " + body.toString());
      await context
          .read(locationNotifierProvider.notifier)
          .saveAutoCompleteLocationToServer(body);
    }
  }

  navigateToMap(
      {LocationModel? location,
      bool isCurrentLocation = false,
      required bool isSavedAddress,
      required bool shouldNavigateToMap}) {
    print("navigating to map");

    // if (!widget.initialCall!) {
    print("agandve");

    // if (value != null) {
    LocationModel model = location!;
    setState(() {
      context.read(homeNotifierProvider.notifier).currentLocation = model;
      context
          .read(landingPagePromoCodeProvider.notifier)
          .getLandingPromoCodes(context: context);
      // loadData(context: context);
    });
    // }

// Navigator.push(context, MaterialPageRoute(builder: (context) => ShopEssentials()));
//       Navigator.pop(context, isCurrentLocation ? 'current_location' : location);
    // }
    if (shouldNavigateToMap == false) {
      saveToDbAndServer(location);
      LocationModel model = location;
      setState(() {
        context.read(homeNotifierProvider.notifier).currentLocation = model;
        context
            .read(landingPagePromoCodeProvider.notifier)
            .getLandingPromoCodes(context: context);
        // loadData(context: context);
      });
      // print("desc "+location!.description.toString());
      Navigator.pop(
          context, /*isCurrentLocation ? 'current_location' :*/ location);
    } else {
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => SelectLocationAddressPage(
                    // searchUserLocation: widget.searchUserLocation,
                    title: 'Enter Location',
                    fromManageAddress: "fromManageAddress",
                    isCurrentLocation: true,
                    fromCart: "true",
                  )));
      // Navigator.push(
      //   context,
      //   MaterialPageRoute(
      //       builder: (context) => SelectLocationPage(
      //             isSavedAddress: isSavedAddress,
      //             mainText: location?.main_text ?? "",
      //             locationAutoCompleteModel: location,
      //             title: widget.title!,
      //             isCurrentLocation: isCurrentLocation,
      //           )),
      // );
    }
  }

  updateLatLong(
      LocationModel location, bool isSavedAddress, bool shouldNavigateToMap) {
    print("checking location captured");
    if (location.latitude == null && location.longitude == null) {
      _locationPickerProvider
          .setLatLongFromPlaceId(location.placeID)
          .then((value) {
        if (value != null) {
          location.latitude = value['lat'];
          location.longitude = value['long'];

          navigateToMap(
              location: location,
              isSavedAddress: isSavedAddress,
              shouldNavigateToMap: shouldNavigateToMap);
        } else {
          showMessage(context, "Error occurred. Please try again.", true, true);
        }
      });
    } else {
      navigateToMap(
          location: location,
          isSavedAddress: isSavedAddress,
          shouldNavigateToMap: shouldNavigateToMap);
    }
  }

  // onSaveAddress(){
  //   if(widget.searchUserLocation == true){
  //     updateLatLong(
  //         new LocationModel(
  //             latitude:widget.addressResponseData != null
  //                 ? latitude!.text
  //                 : widget.selectedLocation!.latitude,
  //             longitude:
  //             widget.addressResponseData != null
  //                 ? longitude!.text
  //                 : widget.selectedLocation!.longitude,
  //             description: (widget.selectedLocation!.main_text.toString() +
  //                 " , " +
  //                 addressLine2!.text.toString() +
  //                 " , " +
  //                 widget.selectedLocation!.secondary_text.toString() +
  //                 " , " +
  //                 landmark!.text.toString())),
  //         true,false);
  //
  //   }else{
  //     onSavingAddress();
  //   }
  //
  // }

  onSaveAddress() {
    print("from cart ?? : " + widget.fromCart.toString());
    print("fromManageAddress ?? : " + widget.fromManageAddress.toString());
    if ((widget.fromManageAddress == "fromManageAddress" &&
            widget.fromCart.toString() == "false") ||
        (widget.fromManageAddress == null &&
            widget.fromCart.toString() == 'null')) {
      if (_formKey.currentState!.validate()) {
        Map body = {
          'address_type': _saveAsController?.text ?? "Others",
          'landmark': landmark!.text,
          'flat_no': addressLine1!.text,
          'street': addressLine2!.text,
          'zipcode': pinCode!.text,
          'map_address': city?.text ?? cityName,
          'latitude': widget.addressResponseData != null
              ? latitude!.text
              : widget.selectedLocation!.latitude,
          'longitude': widget.addressResponseData != null
              ? longitude!.text
              : widget.selectedLocation!.longitude,
          'county': country!.text,
          'title': _saveAsController!.text,
          'city': city!.text,
          'id': addressId
        };
        // body['id'] = addressId;
Logger.appLogs("add address map body :: ${body.toString()}");
        context
            .read(addUpdateAddressNotifierProvider.notifier)
            .saveAddress(body: body, addressId: addressId, context: context);
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => HomePage(currentIndex: 3, )));
      }
    } else if (widget.fromManageAddress == "fromManageAddress" &&
        widget.fromCart.toString() == "true") {
      onSavingAddress();
    }
  }

  onSavingAddress() async {
    if (widget.addListText == null) {
      // print("from cart");
      if (_formKey.currentState!.validate()) {
        Map cartContentBody = {};
        // print("address type " + );
        Map body = {
          'address_type': _saveAsController!.text,
          'landmark': landmark!.text,
          'flat_no': addressLine1!.text,
          'street': addressLine2!.text,
          'zipcode': pinCode!.text,
          'map_address': city?.text ?? cityName,
          'latitude': widget.addressResponseData != null
              ? latitude!.text
              : widget.selectedLocation!.latitude,
          'longitude': widget.addressResponseData != null
              ? longitude!.text
              : widget.selectedLocation!.longitude,
          'county': country!.text,
          'title': _saveAsController!.text,
          'city': city!.text,
          'id': addressId
        };
        // body['id'] = addressId;

        context
            .read(addUpdateAddressNotifierProvider.notifier)
            .saveAddress(body: body, addressId: addressId, context: context);

        int count = 1;
        if (widget.fromCart.toString() == 'true') {
          print("from cart add " + widget.fromCart.toString());
          context.read(cartNotifierProvider.notifier).getCartList(
                fromLat: widget.addressResponseData != null
                    ? latitude!.text
                    : widget.selectedLocation!.latitude,
                fromLng: widget.addressResponseData != null
                    ? longitude!.text
                    : widget.selectedLocation!.longitude,
                context: context,
              );
          CartListModel cartListModel =
              context.read(cartNotifierProvider.notifier).crtlistModel;

          String? toAddress = (addressLine1!.text +
              " , " +
              addressLine2!.text +
              " , " +
              landmark!.text.toString());
          if (widget.map == null) {
            // cartContentBody['delivery_date'] = widget.map!['date'];
          } else {
            cartContentBody['delivery_date'] = widget.map!['date'];
          }
          cartContentBody["to_adrs"] = toAddress;
          cartContentBody["to_lat"] = widget.addressResponseData != null
              ? latitude!.text
              : widget.selectedLocation!.latitude;
          cartContentBody["to_lng"] = widget.addressResponseData != null
              ? longitude!.text
              : widget.selectedLocation!.longitude;

          Navigator.pop(context);
          Navigator.pop(context, cartContentBody);

          // var checkout = await Helpers().getCommonBottomSheet(
          //     context: context,
          //     enableDrag: true,
          //     isDismissible: true,
          //     content: CheckoutBottomSheet(
          //       fromAddList: false,
          //       map: widget.map,
          //       toAddress: toAddress,
          //       cartList: cartListModel.responseData!,
          //       latitude: widget.addressResponseData != null
          //           ? latitude!.text
          //           : widget.selectedLocation!.latitude,
          //       longitude: widget.addressResponseData != null
          //           ? longitude!.text
          //           : widget.selectedLocation!.longitude,
          //       // promoCode: widget.promoCode,
          //       // promoCodeDetails: widget.promoCodeDetails,
          //     ));

//           if (checkout == "clearCart") {
//             context.read(cartNotifierProvider.notifier).getCartList(
//                   context: context,
//                   init: true,
//                   lat: widget.addressResponseData != null
//                       ? latitude!.text
//                       : widget.selectedLocation!.latitude,
//                   lng: widget.addressResponseData != null
//                       ? longitude!.text
//                       : widget.selectedLocation!.longitude,
//                 );
//           } else
//           // if (checkout != null)
//           {
//             var useWallet = checkout;
//             print("usewallet" + useWallet["total_amount"]);
//
//             cartContentBody["wallet"] = useWallet["wallet"];
//             cartContentBody["payment_mode"] = "RAZORPAY";
//             cartContentBody["order_type"] = "DELIVERY";
//             cartContentBody["user_address_id"] = context
//                 .read(addUpdateAddressNotifierProvider.notifier)
//                 .addressID;
//
//             print("cart content body add adress:" + cartContentBody.toString());
// //Navigate.pop()
//
//             // context
//             //     .read(orderBuyItemsNotifier.notifier)
//             //     .buyItemSave(body: cartContentBody, context: context);
//
//             context
//                 .read(orderBuyItemsNotifier.notifier)
//                 .buyItemsOrderPlaceFromCart(body: cartContentBody);
//             Navigator.pop(context);
//           }
        }

/*
      if(widget.fromCart == null) {
      //  Navigator.pop(context);

        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => HomePage()),
        );
       // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => ManageAddressPage()));
       */
/* Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => HomePage()));/ /

      }else{
        Navigator.push(context, MaterialPageRoute(builder: (context) => Cart()));
        // Helpers().getCommonBottomSheet(context: context, content: AddressConfirmationBottomSheet());
      }
*/
        // Navigator.pushAndRemoveUntil(context,
        //     MaterialPageRoute(builder: (context) => ManageAddressPage()),ModalRoute.withName('/manageAddressScreen'));
      }
    } else {
      if (_formKey.currentState!.validate()) {
        FormData cartContentBody = FormData();
        // print("address type " + );
        Map body = {
          'address_type': _saveAsController!.text,
          'landmark': landmark!.text,
          'flat_no': addressLine1!.text,
          'street': addressLine2!.text,
          'zipcode': pinCode!.text,
          'map_address': city?.text ?? cityName,
          'latitude': widget.addressResponseData != null
              ? latitude!.text
              : widget.selectedLocation!.latitude,
          'longitude': widget.addressResponseData != null
              ? longitude!.text
              : widget.selectedLocation!.longitude,
          'county': country!.text,
          'title': _saveAsController!.text,
          'city': city!.text,
          'id': addressId
        };
        // body['id'] = addressId;

        context
            .read(addUpdateAddressNotifierProvider.notifier)
            .saveAddress(body: body, addressId: addressId, context: context);

        // if (widget.fromCart.toString() == 'true') {
        print("from cart add " + widget.fromCart.toString());
        context.read(cartNotifierProvider.notifier).getCartList(
              fromLat: widget.addressResponseData != null
                  ? latitude!.text
                  : widget.selectedLocation!.latitude,
              fromLng: widget.addressResponseData != null
                  ? longitude!.text
                  : widget.selectedLocation!.longitude,
              context: context,
            );
        CartListModel cartListModel =
            context.read(cartNotifierProvider.notifier).crtlistModel;

        // Navigator.of(context).popUntil((_) => count++ >= 2);

        //  Navigator.pop(context);
        // Navigator.pop(context);

        String? toAddress = (widget.selectedLocation!.main_text.toString() +
            " , " +
            addressLine2!.text.toString() +
            " , " +
            widget.selectedLocation!.secondary_text.toString() +
            " , " +
            landmark!.text.toString());
        if (widget.map == null) {
          // cartContentBody['delivery_date'] = widget.map!['date'];
        } else {
          // cartContentBody['delivery_date'] = widget.map!['date'];
        }
        // cartContentBody["to_adrs"] = toAddress;
        // cartContentBody["to_lat"] = widget.addressResponseData != null
        //     ? latitude!.text
        //     : widget.selectedLocation!.latitude;
        // cartContentBody["to_lng"] = widget.addressResponseData != null
        //     ? longitude!.text
        //     : widget.selectedLocation!.longitude;

        var checkout = await Helpers().getCommonBottomSheet(
            context: context,
            enableDrag: true,
            isDismissible: true,
            content: CheckoutBottomSheet(
              fromAddList: true,
              map: widget.map,
              toAddress: toAddress,
              cartList: cartListModel.responseData!,
              latitude: widget.addressResponseData != null
                  ? latitude!.text
                  : widget.selectedLocation!.latitude,
              longitude: widget.addressResponseData != null
                  ? longitude!.text
                  : widget.selectedLocation!.longitude,
              // promoCode: widget.promoCode,
              // promoCodeDetails: widget.promoCodeDetails,
            ));

        if (checkout == "clearCart") {
          context.read(cartNotifierProvider.notifier).getCartList(
                context: context,
                init: true,
                fromLat: widget.addressResponseData != null
                    ? latitude!.text
                    : widget.selectedLocation!.latitude,
                fromLng: widget.addressResponseData != null
                    ? longitude!.text
                    : widget.selectedLocation!.longitude,
              );
        } else /* if (checkout != null)*/ {
          print("toaddres");
          print(toAddress);

          cartContentBody = FormData.fromMap({
            "to_lat": widget.addressResponseData != null
                ? latitude!.text
                : widget.selectedLocation!.latitude,
            "to_lng": widget.addressResponseData != null
                ? longitude!.text
                : widget.selectedLocation!.longitude,
            "to_address": toAddress,
            "shop_name": widget.shopResponseData!.storeName,
            "shop_id": widget.shopResponseData!.id,
            "shop_lat": widget.shopResponseData!.latitude,
            "shop_lng": widget.shopResponseData!.longitude,
            "shop_adrs": widget.shopResponseData!.storeLocation,
            "category_id": widget.storeTypeIdFromAddList,
            "type": widget.isImageSelectedAddList == true ? "image" : "text",
            "cat_value": widget.isImageSelectedAddList == true
                ? widget.addListFilePath
                : widget.addListText

            // "shop_contact_num" :
            // "schedule_date"
            // "schedule_time"
            // "type":TEXT
            // "cat_value"
            // "cart_id"
          });

          // var useWallet = checkout;
          // print("usewallet" + useWallet["total_amount"]);

          // cartContentBody["wallet"] = useWallet["wallet"];
          // cartContentBody["payment_mode"] = "RAZORPAY";
          // cartContentBody["order_type"] = "DELIVERY";
          // cartContentBody["user_address_id"] =
          //     context
          //         .read(addUpdateAddressNotifierProvider.notifier)
          //         .addressID;

          print("cart content body add adress:" +
              cartContentBody.fields[10].toString());
//Navigate.pop()

          context.read(orderBuyItemsNotifier.notifier).buyItemSave(
              body: cartContentBody, context: context, fromUpdateAddress: true);

          // context
          //     .read(orderBuyItemsNotifier.notifier)
          //     .buyItemsOrderPlaceFromCart(body: cartContentBody);
          Navigator.pop(context);
        }
        // }

/*
      if(widget.fromCart == null) {
      //  Navigator.pop(context);

        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => HomePage()),
        );
       // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => ManageAddressPage()));
       */
/* Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => HomePage()));/ /

      }else{
        Navigator.push(context, MaterialPageRoute(builder: (context) => Cart()));
        // Helpers().getCommonBottomSheet(context: context, content: AddressConfirmationBottomSheet());
      }
*/
        // Navigator.pushAndRemoveUntil(context,
        //     MaterialPageRoute(builder: (context) => ManageAddressPage()),ModalRoute.withName('/manageAddressScreen'));
      }
    }
  }

  void _settingModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30.0), topRight: Radius.circular(30.0)),
        ),
        isScrollControlled: true,
        backgroundColor: Colors.white,
        builder: (BuildContext bc) {
          return SingleChildScrollView(
              child: Container(
                  padding: EdgeInsets.only(
                      top: 10,
                      bottom: MediaQuery.of(context).viewInsets.bottom),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Padding(
                          padding:
                              EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Create New Label",
                                style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Container(
                                  decoration:
                                      BoxDecoration(shape: BoxShape.circle),
                                  child: Material(
                                    color: Colors.transparent,
                                    child: InkWell(
                                      customBorder: CircleBorder(),
                                      onTap: () {
                                        setState(() {
                                          Navigator.pop(context);
                                        });
                                      },
                                      child: Padding(
                                        padding: EdgeInsets.all(10),
                                        child: Icon(
                                          Icons.close,
                                        ),
                                      ),
                                    ),
                                  )),
                            ],
                          ),
                        ),
                        Divider(),
                        const SizedBox(
                          height: 10,
                        ),
                        Padding(
                            padding: EdgeInsets.symmetric(horizontal: 20),
                            child: Form(
                                key: _formKeySaveAs,
                                child: Column(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.stretch,
                                  children: [
                                    TextFormField(
                                      textCapitalization:
                                          TextCapitalization.sentences,
                                      controller: _labelController,
                                      textInputAction: TextInputAction.next,
                                      validator: (String? arg) {
                                        if (arg!.length == 0)
                                          return 'Label can\'t be empty!';
                                        else
                                          return null;
                                      },
                                      onFieldSubmitted: (term) {
                                        confirmLabel();
                                      },
                                      style: TextStyle(
                                          fontWeight: FontWeight.w600),
                                      decoration: InputDecoration(
                                        labelText: "Label",
                                        border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(8),
                                          borderSide: BorderSide(
                                            width: 0,
                                            style: BorderStyle.none,
                                          ),
                                        ),
                                        fillColor: Theme.of(context).cardColor,
                                        filled: true,
                                        contentPadding: EdgeInsets.symmetric(
                                            vertical: 0, horizontal: 20),
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 40,
                                    ),
                                    CommonButton(
                                      onTap: () {
                                        confirmLabel();
                                      },
                                      bgColor: App24Colors.darkTextColor,
                                      text: "SUBMIT",
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                  ],
                                )))
                      ])));
        });
  }

  Future<void> confirmLabel() async {
    if (_formKeySaveAs.currentState!.validate()) {
      int i = await checkAddressTypeAlreadySaved(_labelController!.text.trim());
      setState(() {
        if (i == 0) {
          _saveAsController!.text = _labelController!.text.trim();
        } else {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text(
                  "You already have a " + _labelController!.text + " address"),
              duration: Duration(seconds: 1)));
        }

        Navigator.pop(context);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar:(widget.isLaundryApp == true)
            ? PreferredSize(
          preferredSize: Size(double.infinity, 80),
          child: CusturmAppbar(
              child: ListTile(
                leading: backarrow(
                  context: context,
                  onTap: () {
                    Navigator.pop(context);
                  },
                ),
                title: Center(
                  child: Text(widget.addressResponseData != null
                      ? "Update Address"
                      : "Add Address", style: AppTextStyle().appbathead),
                ),
              )),
        )
            : CustomAppBar(
            hideCartIcon: "true",
            title: widget.addressResponseData != null
                ? "Update Address"
                : "Add Address"),
        backgroundColor: Colors.white,
        body: OfflineBuilderWidget(
          SafeArea(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 25),
                child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      const SizedBox(
                        height: 20,
                      ),
                      Text(
                        "Please provide your detailed address so that our Delivery Partner can reach you easily.",
                        style: TextStyle(
                            fontSize: MediaQuery.of(context).size.width / 30,
                            fontWeight: FontWeight.w400),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: Theme.of(context).cardColor,
                        ),
                        // padding: EdgeInsets.symmetric(vertical: 13),
                        alignment: Alignment.centerLeft,
                        child: TextFormField(
                            textCapitalization: TextCapitalization.sentences,
                            validator: (String? arg) {
                              if (arg!.length == 0)
                                return 'Flat no./Building/House name is required';
                            },
                            controller: addressLine1,
                            style: TextStyle(
                                fontWeight: FontWeight.w600, fontSize: 16),
                            maxLines: 2,
                            decoration: getDecoration(context).copyWith(
                                labelText: "Flat no./Building/House name*",
                                // floatingLabelStyle:  TextStyle(fontWeight: FontWeight.w100),
                                // hintText: "Flat no./Building/House name",
                                labelStyle: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize:
                                        MediaQuery.of(context).size.width / 25))
                            // overflow: TextOverflow.ellipsis,
                            ),
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      // CommonHelpers().getSubTitle("Details", context),
                      TextFormField(
                          maxLines: 2,
                          readOnly:
                              widget.addressResponseData != null ? true : false,
                          textCapitalization: TextCapitalization.sentences,
                          validator: (String? arg) {
                            if (arg!.length == 0)
                              return 'Street/Apartment/Area is required';
                          },
                          controller: addressLine2,
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              color: widget.addressResponseData != null
                                  ? App24Colors.lightTextColor
                                  : App24Colors.darkTextColor),
                          decoration: getDecoration(context).copyWith(
                              labelText: "Street/Apartment/Area",
                              labelStyle: TextStyle(
                                  fontSize:
                                      MediaQuery.of(context).size.width / 25))),
                      const SizedBox(
                        height: 30,
                      ),
                      TextFormField(
                          textCapitalization: TextCapitalization.sentences,
                         /* validator: (String? arg) {
                            if (arg!.length == 0) return 'landmark is required';
                          },*/
                          controller: landmark,
                          style: TextStyle(fontWeight: FontWeight.w500),
                          decoration: getDecoration(context).copyWith(
                              labelText: "Landmark",
                              hintText: "eg. Opposite to Police Station",
                              hintStyle: TextStyle(fontWeight: FontWeight.w100),
                              labelStyle: TextStyle(
                                  fontSize:
                                      MediaQuery.of(context).size.width / 25))),
                      const SizedBox(
                        height: 30,
                      ),
                      /*      TextFormField(
                          keyboardType: TextInputType.number,
                          inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                          controller: pinCode,
                          style: TextStyle(fontWeight: FontWeight.w500),
                          decoration: getDecoration(context)
                              .copyWith(labelText: "Pin code*",labelStyle: TextStyle(fontSize: MediaQuery.of(context).size.width/25))),
                      const SizedBox(
                        height: 15,
                      ),*/
                      TextFormField(
                          readOnly:
                              widget.addressResponseData != null ? true : false,
                          textCapitalization: TextCapitalization.sentences,
                          controller: city,
                          /*validator: (String? arg) {
                            if (arg!.length == 0)
                              return "City  can't be empty";
                            else
                              return null;
                          },*/
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              color: widget.addressResponseData != null
                                  ? App24Colors.lightTextColor
                                  : App24Colors.darkTextColor),
                          decoration: getDecoration(context).copyWith(
                              labelText: "City",
                              labelStyle: TextStyle(
                                fontSize:
                                    MediaQuery.of(context).size.width / 25,
                              ))),
                      const SizedBox(
                        height: 15,
                      ),
                      /* TextFormField(
                        // validator: (String? arg) {
                        //   if (arg!.length == 0)
                        //     return 'Field cannot be empty';
                        // },
                          controller: country,
                          style: TextStyle(fontWeight: FontWeight.w500),
                          decoration: getDecoration(context)
                              .copyWith(labelText: "Country",labelStyle: TextStyle(fontSize: MediaQuery.of(context).size.width/25))),*/
                      // Visibility(
                      //   visible: false,
                      //   child: TextFormField(
                      //     // validator: (String? arg) {
                      //     //   if (arg!.length == 0)
                      //     //     return 'Field cannot be empty';
                      //     // },
                      //       controller: latitude,
                      //       style: TextStyle(fontWeight: FontWeight.w500),
                      //       decoration: getDecoration(context)
                      //           .copyWith(labelText: "Latitude",labelStyle: TextStyle(fontSize: MediaQuery.of(context).size.width/25))),
                      // ),
                      // Visibility(
                      //   visible: false,
                      //   child: TextFormField(
                      //     // validator: (String? arg) {
                      //     //   if (arg!.length == 0)
                      //     //     return 'Field cannot be empty';
                      //     // },
                      //       controller: longitude,
                      //       style: TextStyle(fontWeight: FontWeight.w500),
                      //       decoration: getDecoration(context)
                      //           .copyWith(labelText: "Longitude",labelStyle: TextStyle(fontSize: MediaQuery.of(context).size.width/25))),
                      // ),
                      SizedBox(
                        height: 15,
                      ),
                      Visibility(
                        visible: false,
                        child: TextFormField(
                            readOnly: true,
                            validator: (String? arg) {
                              if (arg!.length == 0)
                                return 'Please select any of these address names:';
                            },
                            style: TextStyle(fontWeight: FontWeight.w500),
                            controller: _saveAsController,
                            decoration: getDecoration(context).copyWith(
                                labelText: "Save as",
                                labelStyle: TextStyle(
                                    fontSize:
                                        MediaQuery.of(context).size.width /
                                            25))),
                      ),
                      SizedBox(
                        height: 7,
                      ),
                      Text(
                        "Save as :",
                        style: TextStyle(
                            fontSize: MediaQuery.of(context).size.width / 25,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 7,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          InputChip(
                            padding: EdgeInsets.symmetric(
                                horizontal: 10, vertical: 10),
                            onPressed: () async {
                              if (widget.addressResponseData == null) {
                                int i =
                                    await checkAddressTypeAlreadySaved("Home");
                                setState(() {
                                  if (i == 0) {
                                    _saveAsController!.text = "Home";
                                  } else {
                                    ScaffoldMessenger.of(context).showSnackBar(
                                        SnackBar(
                                            content: Text(
                                                "You already have a Home address"),
                                            duration: Duration(seconds: 1)));
                                    // ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("You have already added"+_saveAsController!.text)));
                                  }
                                  /* else {

      }*/
                                });
                              } else {
                                showMessage(context,
                                    "you cant edit address type", true, true);
                              }
                            },
                            label: Text("Home"),
                            labelStyle: TextStyle(
                                fontWeight: FontWeight.w500,
                                color:
                                    App24Colors.greenOrderEssentialThemeColor,
                                fontSize:
                                    MediaQuery.of(context).size.width / 29),
                            avatar: Icon(
                              App24User.home_1,
                              color: App24Colors.greenOrderEssentialThemeColor,
                              size: MediaQuery.of(context).size.width / 29,
                            ),
                            backgroundColor: _saveAsController!.text == 'Home'
                                ? Colors.black
                                : Theme.of(context)
                                    .colorScheme
                                    .secondary, //Theme.of(context).cardColor,
                          ),
                          InputChip(
                            padding: EdgeInsets.symmetric(
                                horizontal: 10, vertical: 10),
                            onPressed: () async {
                              if (widget.addressResponseData == null) {
                                int i =
                                    await checkAddressTypeAlreadySaved("Work");
                                //checkAddressTypeAlreadySaved(_saveAsController!.text);
                                setState(() {
                                  if (i == 0) {
                                    _saveAsController!.text = "Work";
                                  } else {
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(SnackBar(
                                      content: Text(
                                          "You already have a Work address"),
                                      duration: Duration(seconds: 1),
                                    ));
                                  }
                                });
                              } else {
                                showMessage(context,
                                    "you cant edit address type", true, true);
                              }
                            },
                            label: Text("Work"),
                            labelStyle: TextStyle(
                                fontWeight: FontWeight.w500,
                                color:
                                    App24Colors.greenOrderEssentialThemeColor,
                                fontSize:
                                    MediaQuery.of(context).size.width / 29),
                            avatar: Icon(
                              App24User.apartment,
                              color: App24Colors.greenOrderEssentialThemeColor,
                              size: MediaQuery.of(context).size.width / 29,
                            ),
                            backgroundColor: _saveAsController!.text == 'Work'
                                ? Colors.black
                                : Theme.of(context).colorScheme.secondary,
                          ),
                          _saveAsController!.text != 'Home' &&
                                  _saveAsController!.text != 'Work'
                              ? InputChip(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 10),
                                  onPressed: () {
                                    // setState(() {
                                    //   _saveAsController!.text="Others";
                                    // });
                                    if (widget.addressResponseData == null)
                                      _settingModalBottomSheet(context);
                                    else {
                                      showMessage(
                                          context,
                                          "you cant edit address type",
                                          true,
                                          true);
                                    }
                                  },
                                  labelStyle: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      color: App24Colors
                                          .greenOrderEssentialThemeColor,
                                      fontSize:
                                          MediaQuery.of(context).size.width /
                                              29),
                                  label: Text(_saveAsController!.text.isEmpty
                                      ? 'Others'
                                      : _saveAsController!.text),
                                  avatar: Icon(
                                    App24User.flag,
                                    color: App24Colors
                                        .greenOrderEssentialThemeColor,
                                    size:
                                        MediaQuery.of(context).size.width / 29,
                                  ),
                                  // backgroundColor: selectedLabel=='Home'|| selectedLabel=='Work'||selectedLabel!=_saveAsController!.text? Colors.black:Theme.of(context).colorScheme.secondary,
                                  backgroundColor: _saveAsController!.text !=
                                              'Home' &&
                                          _saveAsController!.text != 'Work' &&
                                          _saveAsController!.text.isNotEmpty
                                      ? Colors.black
                                      : Theme.of(context).colorScheme.secondary,
                                )
                              : InputChip(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 10),
                                  onPressed: () {
                                    if (widget.addressResponseData == null) {
                                      setState(() {
                                        _saveAsController!.text = "Others";
                                      });

                                      _settingModalBottomSheet(context);
                                    } else {
                                      showMessage(
                                          context,
                                          "you cant edit address type",
                                          true,
                                          true);
                                    }
                                  },
                                  labelStyle: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      color: App24Colors
                                          .greenOrderEssentialThemeColor,
                                      fontSize:
                                          MediaQuery.of(context).size.width /
                                              29),
                                  label: Text("Others"),
                                  avatar: Icon(
                                    App24User.flag,
                                    color: App24Colors
                                        .greenOrderEssentialThemeColor,
                                    size:
                                        MediaQuery.of(context).size.width / 29,
                                  ),
                                  // backgroundColor: selectedLabel=='Home'|| selectedLabel=='Work'||selectedLabel!=_saveAsController!.text? Colors.black:Theme.of(context).colorScheme.secondary,
                                  backgroundColor:
                                      Theme.of(context).colorScheme.secondary,
                                ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      MaterialButton(
                        onPressed: () {
                          /*if (_saveAsController!.text == '' ||
                              _saveAsController!.text.isEmpty) {
                            print("no address type selected");
                            showMessage(context, "Please select address type",
                                true, false);
                          } else {
                            onSaveAddress();
                          }*/
                          onSaveAddress();
                        },
                        color: App24Colors.darkTextColor,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15)),
                        padding:
                            EdgeInsets.symmetric(vertical: 17, horizontal: 40),
                        child: ProviderListener(
                          provider: addUpdateAddressNotifierProvider,
                          onChange: (context, dynamic state) {
                            if (state is ResponseLoaded) {
                              showMessage(
                                  context, "Created Successfully", false, true);

                              Navigator.pop(context);
                            }
                          },
                          child: Consumer(
                            builder: (context, watch, child) {
                              final state =
                                  watch(addUpdateAddressNotifierProvider);
                              if (state is ResponseInitial) {
                                return Text(
                                  "Save Address",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize:
                                          MediaQuery.of(context).size.width /
                                              25),
                                );
                              } else if (state.isLoading) {
                                return SizedBox(
                                  child: CircularProgressIndicator.adaptive(),
                                  height: 30,
                                );
                              } else if (state.isError) {
                                return Text(
                                  "Save Address",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize:
                                          MediaQuery.of(context).size.width /
                                              25),
                                );
                              } else {
                                return Text(
                                  "Save Address",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize:
                                          MediaQuery.of(context).size.width /
                                              25),
                                );
                              }
                            },
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ) // This trailing comma makes auto-formatting nicer for build methods.
        );
  }

  Future<int> checkAddressTypeAlreadySaved(String? addressTypeSelected) async {
    int j = await context
        .read(addressNotifierProvider.notifier)
        .getAddressTypes(
            addressTypeSelected: addressTypeSelected, context: context);

    return j;
  }

  // callConfirmAddressBottomSheet({Map<dynamic, dynamic>? map}) async {
  //   print("map in restaurant cart page 2 " + map.toString());
  //
  //   var addressResult = await Helpers().getCommonBottomSheet(
  //       context: context,
  //       content: AddressConfirmationBottomSheet(
  //         map: map,
  //       ),
  //       title: "Address confirmation");
  //   if (addressResult != null) {
  //     AddressResponseData confirmedLocation = addressResult;
  //     print("selected location id" + confirmedLocation.addressid.toString());
  //     updatePriority(confirmedLocation);
  //
  //     String? toAddress = (confirmedLocation.landmark.toString() +
  //         confirmedLocation.flatNo.toString() +
  //         confirmedLocation.street.toString() +
  //         confirmedLocation.city.toString());
  //
  //     cartContentBody["to_adrs"] = toAddress;
  //     cartContentBody["to_lat"] = confirmedLocation.latitude;
  //     cartContentBody["to_lng"] = confirmedLocation.longitude;
  //
  //     context.read(cartNotifierProvider.notifier).getCartList(
  //         lat: confirmedLocation.latitude!.toDouble(),
  //         lng: confirmedLocation.longitude!.toDouble(),
  //         context: context);
  //
  //     var checkout = await Helpers().getCommonBottomSheet(
  //         context: context,
  //         enableDrag: true,
  //         isDismissible: true,
  //         content: CheckoutBottomSheet(
  //           toAddress: toAddress,
  //           cartList: context
  //               .read(cartNotifierProvider.notifier)
  //               .crtlistModel
  //               .responseData!,
  //           latitude: confirmedLocation.latitude,
  //           longitude: confirmedLocation.longitude,
  //           // promoCode: widget.promoCode,
  //           // promoCodeDetails: widget.promoCodeDetails,
  //         ));
  //
  //     if (checkout == "clearCart") {
  //       context.read(cartNotifierProvider.notifier).getCartList(
  //           context: context,
  //           init: true,
  //           lat: context
  //               .read(homeNotifierProvider.notifier)
  //               .currentLocation
  //               .latitude!
  //               .toDouble(),
  //           lng: context
  //               .read(homeNotifierProvider.notifier)
  //               .currentLocation
  //               .longitude!
  //               .toDouble());
  //     } else /* if (checkout != null)*/ {
  //       var useWallet = checkout;
  //       print("usewallet" + useWallet["total_amount"]);
  //
  //       cartContentBody["wallet"] = useWallet["wallet"];
  //       context
  //           .read(orderBuyItemsNotifier.notifier)
  //           .buyItemsOrderPlaceFromCart(body: cartContentBody);
  //     }
  //   }
  // }

  void updatePriority(AddressResponseData confirmedLocation) async {
    AddressDatabase addressDatabase =
        new AddressDatabase(DatabaseService.instance);
    int i = await addressDatabase.updatePriority(confirmedLocation);
    print("updated rows ::: " + i.toString());
  }
}
