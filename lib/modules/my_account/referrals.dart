//
//
// import 'package:app24_user_app/constants/asset_path.dart';
// import 'package:app24_user_app/constants/color_path.dart';
// import 'package:app24_user_app/utils/services/api/api_providers.dart';
// import 'package:app24_user_app/widgets/common_appbar.dart';
// import 'package:app24_user_app/widgets/loading_error.dart';
// import 'package:app24_user_app/widgets/offline_builder_widget.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/scheduler.dart';
// import 'package:flutter/services.dart';
// import 'package:flutter_riverpod/flutter_riverpod.dart';
// import 'package:flutter_riverpod/src/provider.dart';
// import 'package:share_plus/share_plus.dart';
//
// class ReferralPage extends StatefulWidget {
//   const ReferralPage({Key? key}) : super(key: key);
//
//   @override
//   _ReferralPageState createState() => _ReferralPageState();
// }
//
// class _ReferralPageState extends State<ReferralPage> {
//
//
//     reloadData({required BuildContext context, bool? init}) {
//     return context.read(profileNotifierProvider).getProfile(init: init);
//   }
//
//   String referralInviteText =
//       "Invite your friends and earn AED1000 for every 100 new users.";
//   String referralCode = "0D4E4T";
//   final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
//   String? referralAmount;
//   String? referralCount;
//   // String? referralAmount;
//
//   @override
//   void initState() {
//     SchedulerBinding.instance!.addPostFrameCallback((_) {
//       context.read(profileNotifierProvider.notifier).getProfile();
//       //API call after screen build
//    referralAmount =  context.read(profileNotifierProvider.notifier).referralAmount.toString();
//    referralCount = context.read(profileNotifierProvider.notifier).referralCount.toString();
//       // context.read(cartNotifierProvider.notifier).getCartList();
//     });
//     super.initState();
//   }
//
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         appBar: CustomAppBar(
//           hideCartIcon: "true",
//           title: "Invite Referrals",
//         ),
//         key: _scaffoldKey,
//         backgroundColor: Colors.white,
//         body: OfflineBuilderWidget(
//           SafeArea(
//             child: SingleChildScrollView(
//               child: RefreshIndicator(
//                 onRefresh: () {
//                   return reloadData(init: false, context: context) ??
//                       false as Future<void>;
//                 },
//                 child: Padding(
//                   padding: const EdgeInsets.symmetric(horizontal: 20),
//                   child: Column(
//                     crossAxisAlignment: CrossAxisAlignment.stretch,
//                     children: <Widget>[
//                       const SizedBox(
//                         height: 20,
//                       ),
//                       Row(
//                         children: [
//                           Image.asset(
//                             App24UserAppImages.referralPageCover,
//                             width: 170,
//                           ),
//                           Flexible(
//                               child: Text(
//                                 "Refer & Earn!",
//                                 style: TextStyle(
//                                     fontSize: 30, fontWeight: FontWeight.bold),
//                                 textAlign: TextAlign.center,
//                               ))
//                         ],
//                       ),
//                       const SizedBox(
//                         height: 20,
//                       ),
//                       Container(
//                         decoration: BoxDecoration(
//                           borderRadius: BorderRadius.circular(10),
//                         ),
//                         // padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
//                         child: Row(
//                           children: [
//                             Expanded(
//                               child: Text(
//                                 referralInviteText,
//                                 style: TextStyle(
//                                     color: App24Colors.darkTextColor,
//                                     fontWeight: FontWeight.w600, fontSize: 17),
//                               ),
//                             ),
//                             const SizedBox(
//                               width: 10,
//                             ),
//                             // Image.asset(
//                             //   "assets/images/referrals.png",
//                             //   height: 100,
//                             // )
//                           ],
//                         ),
//                       ),
//                       const SizedBox(
//                         height: 20,
//                       ),
//                       Text("Your Referral Code", style: TextStyle(
//                           fontSize: 17, fontWeight: FontWeight.bold,color: App24Colors.darkTextColor),),
//                       // getSubTitle("Your Referral Code", context),
//                       const SizedBox(
//                         height: 10,
//                       ),
//                       Row(
//                         mainAxisAlignment: MainAxisAlignment.center,
//                         children: [
//                           Container(
//                               decoration: BoxDecoration(
//                                   borderRadius: BorderRadius.circular(8),
//                                   color: Theme
//                                       .of(context)
//                                       .accentColor),
//                               child: Material(
//                                 color: Colors.transparent,
//                                 child: InkWell(
//                                     borderRadius: BorderRadius.circular(8),
//                                     onTap: () {
//                                       Clipboard.setData(
//                                           ClipboardData(text: referralCode));
//                                       ScaffoldMessenger.of(context)
//                                           .showSnackBar(SnackBar(
//                                         content: Text(
//                                             "Referral Code copied to clipboard."),
//                                       ));
//                                     },
//                                     child: Container(
//                                       padding: EdgeInsets.symmetric(
//                                           vertical: 14, horizontal: 100),
//                                       child:
//                                       Consumer(
//                                           builder: (context, watch, child) {
//                                             final state =
//                                             watch(profileNotifierProvider);
//                                             if (state.isLoading) {
//                                               return CircularProgressIndicator();
//                                             } else if (state.isError) {
//                                               return LoadingError(
//                                                 onPressed: (res) {
//                                                   reloadData(
//                                                       init: true,
//                                                       context: res);
//                                                 },
//                                                 message:
//                                                 state.errorMessage.toString(),
//                                               );
//                                             } else {
//                                              // var referral = state.response.responseData;
//                                               return  Text(
//                                                 context.read(profileNotifierProvider.notifier).referralCode.toString(),
//                                                 style: TextStyle(
//                                                     fontWeight: FontWeight.bold,
//                                                     fontSize: 18),
//                                                 textAlign: TextAlign.center,
//                                               );
//                                             }
//                                           })
//
//
//
//                                     )),
//                               )),
//                           const SizedBox(
//                             width: 10,
//                           ),
//                           Container(
//                               decoration: BoxDecoration(
//                                   borderRadius: BorderRadius.circular(50),
//                                   color: Color(0xffe6e6e6)),
//                               child: Material(
//                                   color: Colors.transparent,
//                                   child: InkWell(
//                                       borderRadius: BorderRadius.circular(50),
//                                       onTap: () {
//                                         Share.share(
//                                             'My referral code is $referralCode');
//                                       },
//                                       child: Padding(
//                                         padding: EdgeInsets.symmetric(
//                                             vertical: 15, horizontal: 15),
//                                         child: Icon(
//                                           Icons.share,
//                                           color: App24Colors.darkTextColor,
//                                         ),
//                                       )))),
//                         ],
//                       ),
//                       const SizedBox(
//                         height: 30,
//                       ),
//                       Text("Referral Details", style: TextStyle(
//                           fontSize: 17, fontWeight: FontWeight.bold,color: App24Colors.darkTextColor),),
//                       const SizedBox(
//                         height: 8,
//                       ),
//                       Column(
//                         children: <Widget>[
//                           Row(
//                             // crossAxisAlignment: CrossAxisAlignment.center,
//                             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                             children: <Widget>[
//                               Text(
//                                 "Referrals",
//                                 style: TextStyle(
//                                     fontWeight: FontWeight.w600, fontSize: 16),
//                               ),
//                               // SizedBox(
//                               //   height: 5,
//                               // ),
//                               Text(
//                                 context.read(profileNotifierProvider.notifier).referralCount.toString(),
//                                 style: TextStyle(
//                                     color: App24Colors.darkTextColor,
//                                     fontWeight: FontWeight.w600,
//                                     fontSize: 17),
//                               ),
//                             ],
//                           ),
//                           // VerticalDivider(),
//                           Column(
//                             // mainAxisAlignment: MainAxisAlignment.center,
//                             children: <Widget>[
//                               Row(
//                                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                                 // crossAxisAlignment: CrossAxisAlignment.end,
//                                 // crossAxisAlignment: CrossAxisAlignment.center,
//                                 children: <Widget>[
//                                   Text(
//                                     "Amount",
//                                     style: TextStyle(
//                                         fontWeight: FontWeight.w600,
//                                         fontSize: 16),
//                                   ),
//                                   // SizedBox(
//                                   //   height: 5,
//                                   // ),
//                                   Text(
//                                     "\u20B9" + context.read(profileNotifierProvider.notifier).referralAmount.toString(),
//                                     style: TextStyle(
//                                         color: App24Colors.darkTextColor,
//                                         fontWeight: FontWeight.w600,
//                                         fontSize: 17),
//                                   ),
//                                 ],
//                               ),
//                             ],
//                           ),
//                         ],
//                       ),
//                     ],
//                   ),
//                 ),
//               ),
//             ),
//           ),
//         ) // This trailing comma makes auto-formatting nicer for build methods.
//     );
//   }
// }
