import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/api/api_response_state2.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';

class WalletNotifier extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  WalletNotifier(this._apiRepository) : super(ResponseState2.initial());

  late Razorpay _razorPay;
  var walletAmount;
  var userID;
  String? currencySymbol;

  Future<void> getWallet({bool init = true,}) async {
    try {
      if (init) state = state.copyWith(isLoading: true);
      final wallet = await _apiRepository.fetchWallet();
      walletAmount = wallet.message;
      if(wallet.responseData!.data!.length > 0) {
        userID = wallet.responseData!.data![0].id;
        currencySymbol = wallet.responseData!.data![0].user!.currencySymbol;
      }
      showLog("wallet.message :: ${wallet.message.toString()}");
      state =
          state.copyWith(response: wallet, isLoading: false, isError: false);
    } catch (e) {
     showLog( e.toString());
       state = state.copyWith(
          errorMessage: e.toString(), isError: true, isLoading: false);
    }
  }

  void updateWallet() {
    _razorPay = Razorpay();
    _razorPay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorPay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorPay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
  }

  void _handlePaymentSuccess(PaymentSuccessResponse response) {}

  void _handlePaymentError(PaymentFailureResponse response) {}

  void _handleExternalWallet(ExternalWalletResponse response) {}
}
