WalletAddMoney walletAddMoneyModelFromJson(str) {
  return WalletAddMoney.fromJson(str);
}


class WalletAddMoney {
  String? statusCode;
  String? title;
  String? message;
  WalletAddMoneyResponseData? responseData;
  List<dynamic>? error;

  WalletAddMoney(
      {this.statusCode,
        this.title,
        this.message,
        this.responseData,
        this.error});

  WalletAddMoney.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    responseData = json['responseData'] != null
        ? new WalletAddMoneyResponseData.fromJson(json['responseData'])
        : null;
    if (json['error'] != null) {
      error = [];
      json['error'].forEach((v) {
        error!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['title'] = this.title;
    data['message'] = this.message;
    if (this.responseData != null) {
      data['responseData'] = this.responseData!.toJson();
    }
    if (this.error != null) {
      data['error'] = this.error!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class WalletAddMoneyResponseData {
  int? walletBalance;

  WalletAddMoneyResponseData({this.walletBalance});

  WalletAddMoneyResponseData.fromJson(Map<String, dynamic> json) {
    walletBalance = json['wallet_balance'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['wallet_balance'] = this.walletBalance;
    return data;
  }
}
