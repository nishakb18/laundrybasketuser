import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/api/api_response_state2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class WalletAddMoneyNotifier extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  WalletAddMoneyNotifier(this._apiRepository) : super(ResponseState2.initial());

  Future<void> walletAddMoney(
      {bool init = true, required Map body}) async {
    try {
      if (init) state = state.copyWith(isLoading: true);

      final walletAdd = await _apiRepository.fetchWalletAddMoney(body: body);

      state = state.copyWith(
          response: walletAdd, isLoading: false, isError: false);
    } catch (e) {
      print(e.toString());
      state = state.copyWith(
          errorMessage: e.toString(), isError: true, isLoading: false);
    }
  }


}
