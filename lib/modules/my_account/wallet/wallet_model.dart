WalletModel walletModelFromJson(str) {
  return WalletModel.fromJson(str);
}

class WalletModel {
  String? statusCode;
  String? title;
  dynamic message;
  WalletResponseData? responseData;
  List<dynamic>? error;

  WalletModel(
      {this.statusCode,
      this.title,
      this.message,
      this.responseData,
      this.error});

  WalletModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    responseData = json['responseData'] != null
        ? new WalletResponseData.fromJson(json['responseData'])
        : null;
    if (json['error'] != null) {
      error = [];
      json['error'].forEach((v) {
        error!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['title'] = this.title;
    data['message'] = this.message;
    if (this.responseData != null) {
      data['responseData'] = this.responseData!.toJson();
    }
    if (this.error != null) {
      data['error'] = this.error!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class WalletResponseData {
  int? currentPage;
  List<Data>? data;
  String? firstPageUrl;
  int? from;
  int? lastPage;
  String? lastPageUrl;
  String? nextPageUrl;
  String? path;
  int? perPage;
  dynamic prevPageUrl;
  int? to;
  int? total;

  WalletResponseData(
      {this.currentPage,
      this.data,
      this.firstPageUrl,
      this.from,
      this.lastPage,
      this.lastPageUrl,
      this.nextPageUrl,
      this.path,
      this.perPage,
      this.prevPageUrl,
      this.to,
      this.total});

  WalletResponseData.fromJson(Map<String, dynamic> json) {
    currentPage = json['current_page'];
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data!.add(new Data.fromJson(v));
      });
    }
    firstPageUrl = json['first_page_url'];
    from = json['from'];
    lastPage = json['last_page'];
    lastPageUrl = json['last_page_url'];
    nextPageUrl = json['next_page_url'];
    path = json['path'];
    perPage = json['per_page'];
    prevPageUrl = json['prev_page_url'];
    to = json['to'];
    total = json['total'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['current_page'] = this.currentPage;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['first_page_url'] = this.firstPageUrl;
    data['from'] = this.from;
    data['last_page'] = this.lastPage;
    data['last_page_url'] = this.lastPageUrl;
    data['next_page_url'] = this.nextPageUrl;
    data['path'] = this.path;
    data['per_page'] = this.perPage;
    data['prev_page_url'] = this.prevPageUrl;
    data['to'] = this.to;
    data['total'] = this.total;
    return data;
  }
}

class Data {
  int? id;
  int? userId;
  int? transactionId;
  String? transactionAlias;
  String? transactionDesc;
  String? type;
  dynamic amount;
  String? createdAt;
  String? createdTime;
  PaymentLog? paymentLog;
  User? user;

  Data(
      {this.id,
      this.userId,
      this.transactionId,
      this.transactionAlias,
      this.transactionDesc,
      this.type,
      this.amount,
      this.createdAt,
      this.createdTime,
      this.paymentLog,
      this.user});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    transactionId = json['transaction_id'];
    transactionAlias = json['transaction_alias'];
    transactionDesc = json['transaction_desc'];
    type = json['type'];
    amount = json['amount'];
    createdAt = json['created_at'];
    createdTime = json['created_time'];
    paymentLog = json['payment_log'] != null
        ? new PaymentLog.fromJson(json['payment_log'])
        : null;
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['transaction_id'] = this.transactionId;
    data['transaction_alias'] = this.transactionAlias;
    data['transaction_desc'] = this.transactionDesc;
    data['type'] = this.type;
    data['amount'] = this.amount;
    data['created_at'] = this.createdAt;
    data['created_time'] = this.createdTime;
    if (this.paymentLog != null) {
      data['payment_log'] = this.paymentLog!.toJson();
    }
    if (this.user != null) {
      data['user'] = this.user!.toJson();
    }
    return data;
  }
}

class PaymentLog {
  int? id;
  int? companyId;
  int? isWallet;
  String? userType;
  String? paymentMode;
  int? userId;
  dynamic amount;
  String? transactionCode;

  PaymentLog(
      {this.id,
      this.companyId,
      this.isWallet,
      this.userType,
      this.paymentMode,
      this.userId,
      this.amount,
      this.transactionCode});

  PaymentLog.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    companyId = json['company_id'];
    isWallet = json['is_wallet'];
    userType = json['user_type'];
    paymentMode = json['payment_mode'];
    userId = json['user_id'];
    amount = json['amount'];
    transactionCode = json['transaction_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['company_id'] = this.companyId;
    data['is_wallet'] = this.isWallet;
    data['user_type'] = this.userType;
    data['payment_mode'] = this.paymentMode;
    data['user_id'] = this.userId;
    data['amount'] = this.amount;
    data['transaction_code'] = this.transactionCode;
    return data;
  }
}

class User {
  int? id;
  String? currencySymbol;

  User({this.id, this.currencySymbol});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    currencySymbol = json['currency_symbol'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['currency_symbol'] = this.currencySymbol;
    return data;
  }
}
