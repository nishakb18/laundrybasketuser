import 'package:app24_user_app/Laundry-app/views/ServicesScreens/services_home_screen.dart';
import 'package:app24_user_app/Laundry-app/views/widgets/custum_appbar.dart';
import 'package:app24_user_app/constants/app_text.dart';
import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/constants/shared_preferences_path.dart';
import 'package:app24_user_app/constants/textstyle.dart';
import 'package:app24_user_app/helpers.dart';
import 'package:app24_user_app/models/home_delivery_send_order_detail.dart';
import 'package:app24_user_app/modules/my_account/wallet/wallet_loading.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/utils/services/api/api_response_state.dart';
import 'package:app24_user_app/utils/services/socket/socket_service.dart';
import 'package:app24_user_app/widgets/bottomsheets/make_payment_bottomsheet/make_payment_bottom_sheet.dart';
import 'package:app24_user_app/widgets/common_appbar.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:app24_user_app/widgets/offline_builder_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:socket_io_client/socket_io_client.dart';

class WalletPage extends StatefulWidget {
  const WalletPage({
    super.key, this.isLaundryApp = false,
  });
final bool? isLaundryApp;

  @override
  State<WalletPage> createState() => _WalletPageState();
}

class _WalletPageState extends State<WalletPage> {
  TextEditingController? amountController = TextEditingController();
  // WalletResponseData? wallet;
  late Razorpay? _razorPay;
  late final SocketService _socketService;
  String? amountAdded = "0";
  String? razorPayKey;

  @override
  void initState() {
    // _socketService = SocketService.instance;
    SchedulerBinding.instance.addPostFrameCallback((_) {
      loadData();
    });
    amountAdded = amountController!.text;

    super.initState();
    _socketService = SocketService.instance;
    _initRazorPay();
  }

  loadData() async {
    context
        .read(homeNotifierProvider.notifier)
        .profile!
        .responseData!
        .walletBalance!;
    context.read(walletNotifierProvider.notifier).getWallet();
    context.read(walletNotifierProvider.notifier).userID.toString();
    showLog("user id from wallet :: ${context.read(walletNotifierProvider.notifier).userID.toString()}");
    context.read(walletNotifierProvider.notifier).walletAmount.toString();
  }

  @override
  void dispose() {
    _razorPay!.clear();
    super.dispose();
  }

  onClickAddMoney() {
    checkPaymentAndEmitSocket();
    // Map body = {
    //   "amount": int.parse(amountController!.text),
    //   "user_type": "user",
    //   "payment_mode": "UPI",
    // };
    // context.read(walletAddMoneyProvider.notifier).walletAddMoney(body: body);
    // showMessage(context, "\u20B9" + amountController!.text +" has been added to your account");
  }

  _initRazorPay() {
    _razorPay = Razorpay();

    _razorPay!.on(Razorpay.EVENT_PAYMENT_SUCCESS, handlerPaymentSuccess);
    _razorPay!.on(Razorpay.EVENT_PAYMENT_ERROR, handlerErrorFailure);
    _razorPay!.on(Razorpay.EVENT_EXTERNAL_WALLET, handlerExternalWallet);
    return _razorPay;
  }

  void handlerPaymentSuccess(PaymentSuccessResponse response) async {
    var prefs = await SharedPreferences.getInstance();
    Map body = {
      "amount": double.parse(amountController!.text),
      "user_type": "user",
      "payment_mode": "UPI",
    };
    paymentSuccessApi(body,prefs);
    // onClickAddMoney();
  }

  paymentSuccessApi(Map<dynamic, dynamic> body,prefs){
    context.read(walletAddMoneyProvider.notifier).walletAddMoney(body: body);
    showLog("Payment successful");
    emitSocket(userID: prefs.getInt(SharedPreferencesPath.userID)!.toInt());
    showMessage(
        context,
        "${context.read(walletNotifierProvider.notifier).currencySymbol ?? '\u20B9' } ${ amountController!.text} has been added to your account",
        false,
        true);
    reloadData(init: true, context: context);
  }

  void handlerErrorFailure(PaymentFailureResponse response,
      HomeDeliveryOrderDetailResponseData responseData) {
    Helpers().getCommonBottomSheet(
        context: context,
        enableDrag: false,
        isDismissible: false,
        content: MakePaymentBottomSheet(
          detailResponseData: responseData,
        ),
        title: "Retry");
    showLog("Payment failed");
    reloadData(init: true, context: context);
  }

  void handlerExternalWallet(ExternalWalletResponse response,
      HomeDeliveryOrderDetailResponseData responseData) {
    Helpers().getCommonBottomSheet(
        context: context,
        enableDrag: false,
        isDismissible: false,
        content: MakePaymentBottomSheet(
          detailResponseData: responseData,
        ),
        title: "Retry");
    showLog("Payment external");
  }

  emitSocket({required int userID}) async {
    showLog("Socket emitting");
    try {
      Socket? socket = await _socketService.socket;
      socket!
          .emit('joinPrivateRoom', {'room_1_R' + userID.toString() + '_ORDER'});
      showLog("Socket emitted");
      //_socketService.listenSocket();
      // context.read(orderDetailsNotifierProvider.notifier).orderType = 'Buy';
      // Navigator.push(
      //   context,
      //   MaterialPageRoute(
      //       builder: (context) => MyAccountTabPage()),
      // );
    } catch (e) {
      showLog("error in socket is : ${e.toString()}");
    }
  }

  // getRazorPayKey() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   razorPayKey = prefs.getString('${SharedPreferencesPath.razorPayKey}');
  // }

  checkPaymentAndEmitSocket() async {
    var prefs = await SharedPreferences.getInstance();
    Map walletAddOptions = {
      // "key" : razorPayKey,
      'description':
          'userId ${prefs.getInt(SharedPreferencesPath.userID)}',
      'amount': double.parse(amountController!.text),
      "notes": {
        // "store_invoice_id": responseData.storeOrderInvoiceId,
        // "type": "addWallet",
        // "Store_order_id": responseData.id
      }
    };
    try {
      _razorPay!.open(
          await getRazorPayOptions(context: context, values: walletAddOptions));
    } catch (e) {
      showLog(e.toString());
    }

    emitSocket(userID: prefs.getInt(SharedPreferencesPath.userID)!.toInt());
  }

  reloadData({required BuildContext context, required bool init}) {
    return context.read(walletNotifierProvider.notifier).getWallet(init: init);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: (widget.isLaundryApp == true)
            ? PreferredSize(
          preferredSize: const Size(double.infinity, 80),
          child: CusturmAppbar(
              child: ListTile(
                leading: backarrow(
                  context: context,
                  onTap: () {
                      Navigator.pop(context);
                  },
                ),
                title: Center(
                  child: Text(Apptext.myWallet, style: AppTextStyle().appbathead),
                ),
              )),
        )
            : CustomAppBar(
          hideCartIcon: "true",
          title: "My Wallet",
        ),
        backgroundColor: Colors.white,
        body: OfflineBuilderWidget(
          ProviderListener(
            provider: walletAddMoneyProvider,
            onChange: (context, dynamic state) {
              if (state.isLoading!) {
                showProgress(context);
              } else if (state.isError!) {
                Navigator.pop(context);
                showMessage(context, state.errorMessage!, true, true);
              } else {
                Navigator.pop(context);
                reloadData(context: context, init: false);
                // loadData();
              }
            },
            child: SafeArea(
              child: RefreshIndicator(
                onRefresh: () {
                  return reloadData(init: false, context: context) ??
                      false as Future<void>;
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    const SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Image.asset(
                            App24UserAppImages.wallet,
                            width: 50,
                          ),
                          const SizedBox(width: 20),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              const Text(
                                "Your available balance : ",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Color(0xffa9a9a9),
                                    fontSize: 16),
                              ),
                              const SizedBox(
                                width: 10,
                              ),

                              Consumer(builder: (context, watch, child) {
                                final state = watch(walletNotifierProvider);

                                if (state.isLoading) {
                                  return const CircularProgressIndicator.adaptive();
                                } else if (state.isError) {
                                  return LoadingError(
                                    onPressed: (res) {
                                      reloadData(init: true, context: res);
                                    },
                                    message: state.errorMessage.toString(),
                                  );
                                } else {
                                  // cartCount = state.response;

                                  return Text(
                                    "${context.read(walletNotifierProvider.notifier).currencySymbol ?? '\u20B9' } ${context.read(walletNotifierProvider.notifier).walletAmount.toString() == '' ? "0" : context.read(walletNotifierProvider.notifier).walletAmount}",
                                    // "\u20B9 140.00",
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: App24Colors.darkTextColor,
                                        fontSize: 21),
                                  );
                                }
                              }),

                              // Consumer(builder: (context, watch, child) {
                              //   final state = watch(homeNotifierProvider);
                              //   if (state.isLoading) {
                              //     return CircularProgressIndicator.adaptive();
                              //   } else if (state.isError) {
                              //     {
                              //       return LoadingError(
                              //         onPressed: (res) {
                              //           reloadData(init: true, context: res);
                              //         },
                              //         message: state.errorMessage,
                              //       );
                              //     }
                              //   } else {
                              //     return                             Text(
                              //       "\u20B9 " + context.read(homeNotifierProvider.notifier).profile!.responseData!.walletBalance!.toString(),
                              //       // "\u20B9 140.00",
                              //       style: TextStyle(
                              //           fontWeight: FontWeight.bold,
                              //           color: App24Colors.darkTextColor,
                              //           fontSize: 21),
                              //     );
                              //   }
                              // }),
                            ],
                          ),
                        ],
                      ),
                    ),
                    const ListTile(
                      contentPadding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                      title: Text(
                        "Add Money",
                        style: TextStyle(
                            color: App24Colors.darkTextColor,
                            fontWeight: FontWeight.bold),
                      ),
                      // trailing: Container(
                      //   width: 35,
                      //   height: 35,
                      //   decoration: BoxDecoration(
                      //       borderRadius: BorderRadius.circular(5),
                      //       color: Colors.purpleAccent.withOpacity(0.3)),
                      //   child: Icon(Icons.account_balance_wallet),
                      // ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        children: [
                          Flexible(
                            child: TextField(
                                style: const TextStyle(fontWeight: FontWeight.w600),
                                controller: amountController,
                                decoration: InputDecoration(
                                    hintText: "Enter Amount",
                                    contentPadding:
                                        const EdgeInsets.symmetric(horizontal: 20),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(25),
                                      borderSide: const BorderSide(
                                        width: 0,
                                        style: BorderStyle.none,
                                      ),
                                    ),
                                    fillColor: Theme.of(context).cardColor,
                                    filled: true,
                                    prefix: Text(
                                      "${context.read(walletNotifierProvider.notifier).currencySymbol ?? 'AED' } ",
                                      style: const TextStyle(
                                          fontWeight: FontWeight.w600,
                                          fontSize: 16),
                                    )),
                                keyboardType: TextInputType.number,
                                // inputFormatters: <TextInputFormatter>[
                                  // FilteringTextInputFormatter.digitsOnly

                                  // CommonHelpers().getDecoration(context).copyWith(
                                  //   contentPadding: EdgeInsets.symmetric(horizontal: 20),
                                  //     hintText: "Enter Amount",
                                  //     border: OutlineInputBorder(
                                  //       borderRadius: BorderRadius.circular(25),
                                  //       borderSide: BorderSide(
                                  //         width: 0,
                                  //         style: BorderStyle.none,
                                  //       ),
                                  //     ),
                                  //     prefix: Text(
                                  //       "\u20B9 ",
                                  //       style: TextStyle(
                                  //           fontWeight: FontWeight.w600, fontSize: 16),
                                  //     )),
                                // ]
                            ),
                          ),
                          TextButton(
                              onPressed:
                                  // amountController!.text == "" ? null :
                                  () {
                                onClickAddMoney();
                              },
                              child: Container(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 10),
                                  decoration: BoxDecoration(
                                      color: App24Colors.darkTextColor,
                                      borderRadius: BorderRadius.circular(30)),
                                  child: const Icon(
                                    Icons.chevron_right,
                                    color: Colors.white,
                                  )))
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 7,
                    ),
                    // Wrap(
                    //   alignment: WrapAlignment.center,
                    //   runSpacing: 10,
                    //   children: <Widget>[
                    //     getPreAmount("100"),
                    //     getPreAmount("500"),
                    //     getPreAmount("1000"),
                    //   ],
                    // ),
                    // SizedBox(
                    //   height: 15,
                    // ),
                    // Padding(
                    //   padding: EdgeInsets.symmetric(horizontal: 20),
                    //   child: MaterialButton(
                    //       onPressed: () {
                    //
                    //       },
                    //       color: App24Colors.greenOrderEssentialThemeColor,
                    //       shape: RoundedRectangleBorder(
                    //           borderRadius: BorderRadius.circular(10)),
                    //       child: Padding(
                    //         padding:
                    //             EdgeInsets.symmetric(vertical: 17, horizontal: 40),
                    //         child: Text(
                    //           "ADD MONEY",
                    //           style: TextStyle(
                    //               color: Colors.white, fontWeight: FontWeight.bold),
                    //         ),
                    //       )),
                    // ),
                    // SizedBox(
                    //   height: 10,
                    // ),
                    // Divider(
                    //   height: 1,
                    // ),
                    const ListTile(
                      contentPadding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                      // title: Text(
                      //   "Recent",
                      //   style: TextStyle(
                      //       color: App24Colors.greenOrderEssentialThemeColorLight,
                      //       fontWeight: FontWeight.w600),
                      // ),
                      title: Text(
                        "Transaction History",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 17,
                            color: Colors.black87),
                      ),
                      // trailing: Container(
                      //   width: 35,
                      //   height: 35,
                      //   decoration: BoxDecoration(
                      //       borderRadius: BorderRadius.circular(5),
                      //       color: Colors.amberAccent.withOpacity(0.3)),
                      //   child: Icon(Icons.history),
                      // ),
                    ),

                    Expanded(
                      child: Consumer(
                        builder: (context, watch, child) {
                          final state = watch(walletNotifierProvider);
                          if (state is ResponseInitial) {
                            return Container(
                              color: Colors.transparent,
                              child: const Text("Initial"),
                            );
                          } else if (state.isLoading) {
                            return WalletLoading();
                          } else if (state.isError) {
                            showLog("asfafsa");
                            return LoadingError(
                              onPressed: (res) {
                                reloadData(init: true, context: res);
                              },
                              message: state.errorMessage.toString(),
                            );
                          } else {
                            if (state.response.responseData.data.length == 0) {
                              return Container(
                                color: Colors.transparent,
                                child: const Center(
                                  child: Text(
                                    "No transactions yet",
                                    style:
                                        TextStyle(fontWeight: FontWeight.w600),
                                  ),
                                ),
                              );
                            }

                            return ListView.separated(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 10),
                              itemCount:
                                  state.response.responseData.data.length,
                              shrinkWrap: true,
                              itemBuilder: (context, index) {
                                showLog(state.response.responseData.data.length);
                                return Container(
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(25),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey[200]!,
                                          blurRadius: 3,
                                          spreadRadius: 3,
                                        )
                                      ]),
                                  child: Material(
                                    // borderRadius: BorderRadius.circular(50),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(25),
                                    ),
                                    color: Colors.transparent,
                                    child: ListTile(
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(25)),
                                        onTap: () {},
                                        title: Text(
                                          state.response.responseData
                                              .data[index].createdAt,
                                          style: const TextStyle(
                                              color: Color(0xffa2a2a2),
                                              fontWeight: FontWeight.w600,
                                              fontSize: 12),
                                        ),
                                        subtitle: Row(
                                          children: [
                                            Expanded(
                                              child: Text(
                                                state
                                                    .response
                                                    .responseData
                                                    .data[index]
                                                    .transactionAlias,
                                                overflow: TextOverflow.ellipsis,
                                                style: const TextStyle(
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 15,
                                                    color: App24Colors
                                                        .darkTextColor),
                                              ),
                                            ),
                                            // leading:
                                            // Container(
                                            //   width: 40,
                                            //   height: 40,
                                            //   decoration: BoxDecoration(
                                            //       borderRadius: BorderRadius.circular(5),
                                            //       color: Theme.of(context).cardColor),
                                            //   child: Column(
                                            //     mainAxisAlignment: MainAxisAlignment.center,
                                            //     children: <Widget>[
                                            //       Text(
                                            //         _transaction_list[index].day,
                                            //         style: TextStyle(
                                            //             fontWeight: FontWeight.w600,
                                            //             fontSize: 13),
                                            //       ),
                                            //       Text(
                                            //         _transaction_list[index].month,
                                            //         style: TextStyle(
                                            //             fontWeight: FontWeight.w600,
                                            //             fontSize: 11),
                                            //       ),
                                            //     ],
                                            //   ),
                                            // ),
                                            Text(
                                              "${state.response.responseData.data[index].user.currencySymbol} "
                                              "${state.response.responseData.data[index].amount}",
                                              style: const TextStyle(
                                                  color:
                                                      App24Colors.darkTextColor,
                                                  fontWeight: FontWeight.w600),
                                            )

                                            // Text(
                                            //   "${_transaction_list[index].status == "success" ? "- " : "+ "}\u20B9${_transaction_list[index].amount}",
                                            //   style: TextStyle(
                                            //       fontWeight: FontWeight.w600,
                                            //       fontSize: 15,
                                            //       color: _transaction_list[index].status ==
                                            //               "success"
                                            //           ? Colors.redAccent
                                            //           : Colors.green),
                                            // ),
                                          ],
                                        ),
                                        trailing: buildTransactionIcon(state
                                            .response
                                            .responseData
                                            .data[index]
                                            .type)),
                                  ),
                                );
                              },
                              separatorBuilder: (context, index) {
                                return const Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 5),
                                );
                              },
                            );
                          }
                        },
                      ),
                    ),

                    /*   walletTransactionList.when(data: (service) {
                        return Expanded(
                          child: ListView.separated(
                            padding: EdgeInsets.symmetric(horizontal: 20,
                                vertical: 10),
                            itemCount: state.Wallet.responseData.data.length,
                            shrinkWrap: true,
                            itemBuilder: (context, index) {
                              return Container(
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(25),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey[200],
                                        blurRadius: 3,
                                        spreadRadius: 3,
                                      )
                                    ]),
                                child: Material(
                                  // borderRadius: BorderRadius.circular(50),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(25),
                                  ),
                                  color: Colors.transparent,
                                  child: ListTile(
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(25)),
                                    onTap: () {},
                                    title: Text(
                                      state.Wallet.responseData.data[index].createdAt,
                                      style: TextStyle(
                                          color: Color(0xffa2a2a2),
                                          fontWeight: FontWeight.w600,
                                          fontSize: 12),
                                    ),
                                    subtitle: Row(
                                      children: [
                                        Expanded(
                                          child: Text(
                                            state.Wallet.responseData.data[index]
                                                .transactionAlias,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 15,
                                                color: App24Colors.darkTextColor),
                                          ),
                                        ),
                                        // leading:
                                        // Container(
                                        //   width: 40,
                                        //   height: 40,
                                        //   decoration: BoxDecoration(
                                        //       borderRadius: BorderRadius.circular(5),
                                        //       color: Theme.of(context).cardColor),
                                        //   child: Column(
                                        //     mainAxisAlignment: MainAxisAlignment.center,
                                        //     children: <Widget>[
                                        //       Text(
                                        //         _transaction_list[index].day,
                                        //         style: TextStyle(
                                        //             fontWeight: FontWeight.w600,
                                        //             fontSize: 13),
                                        //       ),
                                        //       Text(
                                        //         _transaction_list[index].month,
                                        //         style: TextStyle(
                                        //             fontWeight: FontWeight.w600,
                                        //             fontSize: 11),
                                        //       ),
                                        //     ],
                                        //   ),
                                        // ),
                                        Text(
                                          "\u20B9 "
                                              "${ state.Wallet.responseData.data[index]
                                              .amount}",
                                          style: TextStyle(
                                              color: App24Colors.darkTextColor,
                                              fontWeight: FontWeight.w600),
                                        )

                                        // Text(
                                        //   "${_transaction_list[index].status == "success" ? "- " : "+ "}\u20B9${_transaction_list[index].amount}",
                                        //   style: TextStyle(
                                        //       fontWeight: FontWeight.w600,
                                        //       fontSize: 15,
                                        //       color: _transaction_list[index].status ==
                                        //               "success"
                                        //           ? Colors.redAccent
                                        //           : Colors.green),
                                        // ),
                                      ],
                                    ),
                                    trailing: buildTransactionIcon(
                                          state.Wallet.responseData.data[index].type)),
                                  ),
                              );
                            },
                            separatorBuilder: (context, index) {
                              return Padding(
                                padding:
                                EdgeInsets.symmetric(horizontal: 10, vertical: 5),);
                            },
                          ),
                        );
                      },
                          loading: () => ServicesShimmer(),
                          error: (err, stack) => Text("Error")),*/
                  ],
                ),
              ),
            ),
          ),
        ) // This trailing comma makes auto-formatting nicer for build methods.
        );
  }

  buildTransactionIcon(String type) {
    if (type.toLowerCase() == "c") {
      return const Text(
        "Cr",
        style: TextStyle(
            fontWeight: FontWeight.bold,
            color: App24Colors.greenOrderEssentialThemeColor),
      );
      //   Icon(
      //   Icons.check_circle,
      //   color: Colors.green,
      // );
    } else if (type.toLowerCase() == "d") {
      return const Text(
        "Dr",
        style: TextStyle(
            fontWeight: FontWeight.bold,
            color: App24Colors.redRestaurantThemeColor),
      );
      //   Icon(
      //   Icons.cancel,
      //   color: Colors.red,
      // );
    } else {
      return const Icon(
        CupertinoIcons.time_solid,
        color: Colors.grey,
      );
    }
  }
}

class TransactionList {
  TransactionList(
      {this.day,
      this.month,
      this.transactionId,
      this.amount,
      this.status,
      this.details});

  String? day;
  String? month;
  String? transactionId;
  String? amount;
  String? status;
  String? details;
}
