import 'dart:async';

import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class WalletLoading extends StatefulWidget {
  @override
  _WalletLoadingState createState() => _WalletLoadingState();
}

class _WalletLoadingState extends State<WalletLoading> {
  Timer? _timer;
  int _start = 10; // time to popup still working window /// in seconds
  List<Widget> list = [];

  @override
  void initState() {
    buildWalletShimmerItem(context);
    startTimer();
    super.initState();
  }

  @override
  void dispose() {
    if (_timer != null) {
      _timer!.cancel();
    }
    super.dispose();
  }

  void startTimer() {
    if (_timer != null) {
      _timer!.cancel();
      _timer = null;
    } else {
      _timer = new Timer.periodic(const Duration(seconds: 1), (Timer timer) {
        setState(() {
          if (_start < 1) {
            _timer!.cancel();
          } else {
            _start = _start - 1;
          }
        });
      });
    }
  }

  buildWalletShimmerItem(context) {
    for (var i = 0; i < 6; i++)
      list.add(Container(
        height: 90,
        margin: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(25),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Color(0xffefefef),
                blurRadius: 20,
                spreadRadius: 3,
              ),
            ]),
      ));
  }

  buildStillLoadingWidget() {
    return ListTile(
      title: Text("Please wait..."),
      subtitle: Text(
          "The network seems to be too busy. We suggest you to wait for some more time."),
      leading: CircularProgressIndicator.adaptive(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _start == 0 ? buildStillLoadingWidget() : Container(),
        Expanded(
          child: Shimmer.fromColors(
              baseColor: Colors.grey[300]!,
              highlightColor: Colors.grey[100]!,
              child: ListView(shrinkWrap: true, children: list)),
        ),
      ],
    );
  }
}
