import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/api/api_response_state2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class OrderConfirmationNotifier extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  OrderConfirmationNotifier(this._apiRepository)
      : super(ResponseState2.initial());

  Future<void> orderConfirmation(
      {bool init = true, BuildContext? context, Map? body}) async {
    try {
      if (init) state = state.copyWith(isLoading: true);

      final orderConfirm =
          await _apiRepository.fetchOrderConfirmation(context, body);

      state = state.copyWith(
          response: orderConfirm, isLoading: false, isError: false);
    } catch (e) {
      print(e.toString());
      state = state.copyWith(
          errorMessage: e.toString(), isError: true, isLoading: false);
    }
  }
}
