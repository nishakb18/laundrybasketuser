import 'dart:async';
import 'dart:convert';
import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_coman.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/constants/shared_preferences_path.dart';
import 'package:app24_user_app/helpers.dart';
import 'package:app24_user_app/modules/home.dart';
import 'package:app24_user_app/modules/my_account/address/manage_address_model.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/database/address_database.dart';
import 'package:app24_user_app/utils/services/database/database_service.dart';
import 'package:app24_user_app/utils/services/firebase/firebase_service.dart';
import 'package:app24_user_app/utils/services/socket/socket_service.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:pin_input_text_field/pin_input_text_field.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';


class OTPVerificationPage extends StatefulWidget {
  final String? mobile;
  final String? countryCode;

  const OTPVerificationPage({
    super.key,
    this.mobile, this.countryCode,
  });
  @override
  State<OTPVerificationPage> createState() => _OTPVerificationPageState();
}

class _OTPVerificationPageState extends State<OTPVerificationPage> {
  CrossFadeState _crossFadeState = CrossFadeState.showFirst;
  final TextEditingController _pinPutController = TextEditingController();

  // SmsUserConsent? smsUserConsent;
  var otp = '';
  bool isOTPSend = false;
  var mobileNumber = '+91 ';
  Timer? _timer;
  int _start = 59;
  late final SocketService _socketService;
  final ApiRepository _apiRepository = ApiRepository();
  String signature = "{{ app signature }}";

  String? commingSms = 'Unknown';

  // Future<void> initSmsListener() async {
  //   // String? commingSms;
  //   try {
  //     // commingSms = await AltSmsAutofill().listenForSms;
  //     if(commingSms!.toLowerCase().contains("App24 Account".toString().toLowerCase()))
  //     {
  //       if (kDebugMode) {
  //         print(" msg of app24");
  //       }
  //       final intRegex = RegExp(r'\d+', multiLine: true);
  //
  //       setState(() {
  //         _commingOTPSms = intRegex.allMatches(commingSms!).elementAt(1).group(0);
  //         _pinPutController.text = _commingOTPSms!;
  //         showProgress("verify");
  //         // showLog(_commingOTPSms!);
  //       });
  //
  //     }
  //     else {
  //       if (kDebugMode) {
  //         print("no msg of app24");
  //       }
  //     }
  //
  //   } on PlatformException {
  //     commingSms = 'Failed to get Sms.';
  //   }
  //   if (!mounted) return;
  //
  //
  // }

  @override
  void initState() {
    // initSmsListener();
    // _pinPutController.text = _commingOTPSms!;
    // print("otp = $_commingOTPSms");
    if (widget.mobile != null) mobileNumber = widget.mobile!;
    showLog("mobileNumber :: $mobileNumber :: 4 widget.mobile :: ${widget.mobile}");
    startTimer();
    FirebaseNotifications(context).setUpFirebase();

    // smsUserConsent = SmsUserConsent(
    //     phoneNumberListener: () => setState(() {}),
    //     smsListener: () => setState(() {
    //       String pin = getCode(smsUserConsent!.receivedSms.toString());
    //       if (pin != "NO SMS") {
    //         setState(() {
    //           _pinPutController.text = pin;
    //           otp = pin;
    //           showProgress("verify");
    //         });
    //       }
    //     }));
    // smsUserConsent!.requestSms();
    super.initState();
  }

  // BoxDecoration get _pinPutDecoration {
  //   return BoxDecoration(
  //     // color: Color(0xffF0F0F0),
  //     // border: Border.all(color: Colors.deepPurpleAccent),
  //     borderRadius: BorderRadius.circular(10.0),
  //   );
  // }

  getCode(String sms) {
    if (sms != "") {
      final intRegex = RegExp(r'\d+', multiLine: true);
      final code = intRegex.allMatches(sms).elementAt(1).group(0);
      return code;
    }
    return "NO SMS";
  }

  @override
  void dispose() {
    // AltSmsAutofill().unregisterListener();
    showLog("dispose of sms");
    // smsUserConsent!.dispose();
    showLog("dispose of sms 1");
    _timer!.cancel();
    super.dispose();
  }

  void startTimer() {
    const oneSec = Duration(seconds: 1);
    _timer = Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          if (_start < 1) {
            timer.cancel();
            setState(() {
              _crossFadeState = CrossFadeState.showSecond;
            });
          } else {
            setState(() {
              _start = _start - 1;
            });
          }
        },
      ),
    );
  }

  // @override
  // void dispose() {
  //   _timer!.cancel();
  //   super.dispose();
  //
  // }

  Future<void> showProgress(type) async {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return const Center(
            child: CupertinoActivityIndicator(radius: 20),
          );
        });
    if (type == "verify") {
      await verifyOTP();
    } else {
      setState(() {
        isOTPSend = false;
      });
      await sendOTP();
    }
  }
  String? removePlus(String phoneNumber) {
    return phoneNumber.replaceAll('+', '');
  }
  
  Future<bool> verifyOTP() async {
    FirebaseNotifications(context).firebaseCloudMessagingListeners();

    var  prefs = await SharedPreferences.getInstance();
    var deviceToken =
    prefs.getString(SharedPreferencesPath.firebaseTokenKey);
    var appVersion = prefs.getString(SharedPreferencesPath.playStoreVersionKey);
    showLog("appVersion ${appVersion.toString()} \ndeviceToken :: $deviceToken ");

    Map<String, dynamic> body = {
      'country_code': removePlus(widget.countryCode ??'91') ,
      'mobile': widget.mobile,
      'salt_key': "MQ==",
      'otp': _pinPutController.text,
      'device_token': deviceToken,
      'version': appVersion,
      // 'device_token': prefs.getString(SharedPreferencesPath.firebaseTokenKey),
    };
    showLog("body :: ${body.toString()}");
    // String boddy =json.encode(body);
    Helpers()
        .getPostDataForOTP("/v1/user/verify-otp", body, null)
        .then((value) async {
      showLog('verify otp:response $value');
      Navigator.pop(context);
      if (value == "error") {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          behavior: SnackBarBehavior.floating,
          content: Text("Please check your connection and try again!"),
        ));
      } else if (json.decode(value)['statusCode'] == "200") {
        var result = await _apiRepository.fetchLogin(context, body);
        showLog("$result");
        Navigator.pop(context);
        if (result.responseData != null) {
          _socketService = SocketService.instance;
          prefs.setBool(SharedPreferencesPath.isUserRegistered, true);
          prefs.setBool(SharedPreferencesPath.isUserRegisteredKey, true);
          final paramsAccess = prefs.setString(SharedPreferencesPath.accessToken, result.responseData!.accessToken!);
          showLog("paramsAccess :: $paramsAccess");
          _socketService.listenSocket();
          // prefs.setString(
          //     'token', json.decode(value)['responseData']['access_token']);
          // // prefs.setString(
          // //     'name', json.decode(value)['responseData']['user']['store_name']);
          // // prefs.setString(
          // //     'email', json.decode(value)['responseData']['user']['email']);
          // prefs.setString(
          //     'id', json.decode(value)['responseData']['user']['id'].toString());
          // // prefs.setString(
          // //     'shopId',
          // //     json
          // //         .decode(value)['responseData']['user']['store_type_id']
          // //         .toString());
          // prefs.setString(
          //     'picture', json.decode(value)['responseData']['user']['picture']);
          // prefs.setString('status',
          //     json.decode(value)['responseData']['user']['status'].toString());
          // prefs.setString('mobile', json.decode(value)['responseData']['mobile']);
          otpVerifyApiLoad();
        } else {
          showMessage(context, result.message!, false, true);
        }
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          behavior: SnackBarBehavior.floating,
          content: Text(json.decode(value)['message']),
        ));
      }
    });
    return isOTPSend;
  }
 otpVerifyApiLoad(){
   context.read(homeNotifierProvider.notifier).getAppConfigurations();
   context.read(homeNotifierProvider.notifier).getProfile();
   context.read(homeNotifierProvider.notifier).getCurrentLocation();
   context
       .read(orderDetailsNotifierProvider.notifier)
       .checkOngoingOrders();
   getSavedAddresses();
   Navigator.of(context).pushAndRemoveUntil(
       MaterialPageRoute(builder: (c) => const HomePage()
         //LaundryMain()
       ),
           (route) => false);
 }


  Future<bool> sendOTP() async {
    var body = {
      'country_code': '91',
      'mobile': widget.mobile,
      'salt_key': "MQ=="
    };

    Helpers()
        .getPostDataForOTP("/v1/user/resend-otp", body, null)
        .then((value) {
      Navigator.pop(context);
      if (value == "error") {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          behavior: SnackBarBehavior.floating,
          content: Text("Please check your connection and try again!"),
        ));
      } else if (json.decode(value)['statusCode'] == "200") {
        // smsUserConsent!.requestSms();
        if (json.decode(value)['message'] == 'OTP sent!') {
          setState(() {
            isOTPSend = true;
          });
        }
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          behavior: SnackBarBehavior.floating,
          content: Text(json.decode(value)['message']),
        ));
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          behavior: SnackBarBehavior.floating,
          content: Text(json.decode(value)['message']),
        ));
      }
    });
    return isOTPSend;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // appBar: CustomAppBar(
        //   title: "OTP Verification",
        // ),
        backgroundColor: Colors.white,
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                const SizedBox(
                  height: 40,
                ),
                Image.asset(
                  App24UserAppImages.otpVerificationImage,
                  width: 70,
                ),
                const SizedBox(
                  height: 30,
                ),
                const Text(
                  "Verify OTP ",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                ),
                const SizedBox(
                  height: 8,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      text: 'Enter the verification code sent to you on ',
                      style: const TextStyle(
                          color: Colors.grey, fontWeight: FontWeight.w600),
                      children: <TextSpan>[
                        TextSpan(
                            text: "${widget.countryCode}  ${widget.mobile}",
                            style: const TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Color(0xff1976D2))),
                      ],
                    ),
                  ),
                ),
                Consumer(builder: (context, watch, child) {
                  final state = watch(sendOtpProvider);
                  if(state.isLoading){
                    return const SizedBox.shrink();
                  }
                  else if(state.isError){
                    return const SizedBox.shrink();
                  }
                  else {
                  return ((state.response?.responseData?.yourOtp != null) || (state.response?.responseData?.yourOtp != ""))?
                    Center(child: Text(" your OTP is ${state.response?.responseData?.yourOtp ?? ""}")) :const SizedBox.shrink();
                  }
                  }
                ),
                const SizedBox(
                  height: 30,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25),
                  child: PinInputTextField(
                    // decoration: PinDecoration(
                    //   textStyle:
                    // ),
                    controller: _pinPutController,
                    pinLength: 4,
                    onSubmit: (String pin) {
                      setState(() {
                        otp = pin;
                        showProgress("verify");
                      });
                    },
                    decoration: BoxLooseDecoration(strokeColorBuilder: PinListenColorBuilder(AppColor.carvebarcolor.withOpacity(0.6), AppColor.smokewhite,),
                    ),
                  ),
                ),
               /* TextFormField(
                  controller: _pinPutController,
                  decoration: const InputDecoration(hintText: "Enter the OTP"),
                ),*/
                const SizedBox(height: 10,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    const Text(
                      "Didn't receive OTP ? ",
                      style: TextStyle(
                          color: Colors.grey, fontWeight: FontWeight.w600),
                    ),
                    AnimatedCrossFade(
                        crossFadeState: _crossFadeState,
                        duration: const Duration(seconds: 1),
                        firstChild: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 15),
                          child: Text(_start.toString()),
                        ),
                        secondChild: GestureDetector(
                          onTap: () {
                            showProgress("resend");
                          },
                          child: Text(
                            " RESEND",
                            style: TextStyle(
                                color: Theme.of(context).primaryColor),
                          ),
                        ))
                  ],
                ),
                const SizedBox(
                  height: 60,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      MaterialButton(
                          onPressed: () {
                            showProgress("verify");
                          },
                          color: App24Colors.darkTextColor,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          child: const Padding(
                            padding: EdgeInsets.symmetric(
                                vertical: 17, horizontal: 50),
                            child: Text(
                              "VERIFY",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          )),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 10,
                )
              ],
            ),
          ),
        ) // This trailing comma makes auto-formatting nicer for build methods.
        );
  }

  void getSavedAddresses() async {
    AddressDatabase addressDatabase = AddressDatabase(DatabaseService.instance);
    List<AddressResponseData> list = await addressDatabase.fetchAddresses();
    if (list.isEmpty) {
      showLog("no data in db");
      //context.read(addressNotifierProvider.notifier).getAddressesFromServer();

      showLog("in getAddressesFromServer");

      final addressesResponseData = await _apiRepository.fetchManageAddress();
      showLog("addresses length ::${addressesResponseData.responseData!.length.toString()}");

      for (int i = 0; i < addressesResponseData.responseData!.length; i++) {
        addressDatabase.addUserAddress(addressesResponseData.responseData![i]);
      }
    }
  }
}
