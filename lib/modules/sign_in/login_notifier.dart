import 'package:app24_user_app/modules/sign_in/otp_verification.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/api/api_response_state2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class LoginProvider extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  LoginProvider(this._apiRepository) : super(ResponseState2(isLoading: true,response: []));


  Future<void> sendOtp({bool init = true, required Map body}) async {
    try {
      if (init) state = state.copyWith(isLoading: true);

      final sendOtp = await _apiRepository.fetchSendOtp(body: body,);


      state = state.copyWith(
          response: sendOtp, isLoading: false, isError: false);
    } catch (e) {
      showLog("error here is ${e.toString()}");
      state = state.copyWith(
          errorMessage: e.toString(), isLoading: false, isError: true);
    }
  }


}
