import 'package:app24_user_app/Laundry-app/views/DashbordScreens/dashbord_screen.dart';
import 'package:app24_user_app/app24_user_icons.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/constants/global_constants.dart';
import 'package:app24_user_app/models/location_model.dart';
import 'package:app24_user_app/modules/tabs/history/history_tab.dart';
import 'package:app24_user_app/modules/tabs/account/my_account_tab.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

import 'tabs/notifications/notifications_tab.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key, this.currentIndex = 0});
  final int? currentIndex;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentIndex = 0;
  final List<Widget> _children = [
   // HomeTabPage(),
    const DashboardScreen(),
    const HistoryTabPage(isNewApp: true,),
   // HomeTabPage(),
    const NotificationsTabPage(isLaundryApp: true,),
    const MyAccountTabPage(isLaundryApp: true,)
  ];

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  void initState() {
    SchedulerBinding.instance.addPostFrameCallback((_){
      _currentIndex = widget.currentIndex ?? 0;
      getCurrentLocation();
    });
    super.initState();
  }

  late bool _serviceEnabled;
  final Location location = Location();
  PermissionStatus? _permissionGranted;
  LocationModel? selectedLocation;
  GoogleMapController? mapController;

  selectLocation(String title) {
    showSelectLocationSheet(
            context: context,
            title: title,
            isDismissible: false,
            willPopDismissible: false)
        .then((value) {
      if (value != null) {
        LocationModel model = value;
        setState(() { 
          context.read(homeNotifierProvider.notifier).currentLocation = model;
          context
              .read(landingPagePromoCodeProvider.notifier)
              .getLandingPromoCodes(context: context);
          // loadData(context: context);
        });
      }
    });
  }

  getCurrentLocation() async {
    showLog("location from home");
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }
    if (_serviceEnabled) showLog("_getCurrentLocation()");

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return showLog("location enabling");
      }
    }

    await location.getLocation().then((position) async {
      LocationData currentLocation = position;
      LocationModel? lm;
      selectedLocation = LocationModel(
          latitude: currentLocation.latitude,
          longitude: currentLocation.longitude);

      //setLocation(selectedLocation!.latitude, selectedLocation!.longitude);

      lm = await getAddress(
          selectedLocation!.latitude!, selectedLocation!.longitude!);

      setState(() {
        context.read(homeNotifierProvider.notifier).currentLocation = lm!;
        context
            .read(landingPagePromoCodeProvider.notifier)
            .getLandingPromoCodes(context: context);
        // print("main text::: "+lm.main_text.toString());
        // loadData(context: context);
      });
    });
    locationBtmSheetShow();
  }
locationBtmSheetShow(){
  if (context
      .read(homeNotifierProvider.notifier)
      .currentLocation
      .main_text ==
      "Loading..") {
    selectLocation(GlobalConstants.labelGlobalLocation);
    // Helpers().getCommonBottomSheet(isDismissible: false,enableDrag: false,
    //     title: "Location", context: context, content: WillPopScope(
    //       onWillPop: (){return Future.value(false);},
    //       child: Container(
    //   child: Column(
    //       children: [
    //         Text("Loc")
    //       ],
    //   ),
    // ),
    //     ));
  }
}
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          //     Helpers().getCommonBottomSheet(title: "Location",context: context, content: Container(
          //   child: Column(
          //     children: [
          //       Text("Loc")
          //     ],
          //   ),
          // )),
          Expanded(
            child: _children[_currentIndex],
          ),

         // OrderDetailsOverlay()
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        onTap: onTabTapped,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        selectedItemColor: App24Colors.darkTextColor,
        unselectedItemColor: Colors.grey,
        backgroundColor: Colors.white,
        currentIndex: _currentIndex,
        items: const [
          BottomNavigationBarItem(
            icon: Icon(
              App24User.home_2,
              size: 22,
            ),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              App24User.file_alt,
              size: 22,
            ),
            label: 'History',
          ),
          BottomNavigationBarItem(
            icon: Icon(App24User.bell),
            label: 'Notifications',
          ),
          BottomNavigationBarItem(
              icon: Icon(App24User.account), label: 'Profile')
        ],
      ),
    );
  }
}

class HomeScreenTile {
  String? image;
  String? title;
  String? id;

  HomeScreenTile({this.id, this.image, this.title});
}
