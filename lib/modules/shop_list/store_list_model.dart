
import 'dart:convert';

StoreListModel storeListModelFromJson(String str) => StoreListModel.fromJson(json.decode(str));

String storeListModelToJson(StoreListModel data) => json.encode(data.toJson());

class StoreListModel {
  String? statusCode;
  String? title;
  String? message;
  ResponseData? responseData;
  List<dynamic>? responseNewData;
  List<dynamic>? error;

  StoreListModel({
    this.statusCode,
    this.title,
    this.message,
    this.responseData,
    this.responseNewData,
    this.error,
  });

  factory StoreListModel.fromJson(Map<String, dynamic> json) => StoreListModel(
    statusCode: json["statusCode"],
    title: json["title"],
    message: json["message"],
    responseData: json["responseData"] == null ? null : ResponseData.fromJson(json["responseData"]),
    responseNewData: json["responseNewData"] == null ? [] : List<dynamic>.from(json["responseNewData"]!.map((x) => x)),
    error: json["error"] == null ? [] : List<dynamic>.from(json["error"]!.map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "statusCode": statusCode,
    "title": title,
    "message": message,
    "responseData": responseData?.toJson(),
    "responseNewData": responseNewData == null ? [] : List<dynamic>.from(responseNewData!.map((x) => x)),
    "error": error == null ? [] : List<dynamic>.from(error!.map((x) => x)),
  };
}

class ResponseData {
  int? currentPage;
  List<Datum>? data;
  String? firstPageUrl;
  int? from;
  int? lastPage;
  String? lastPageUrl;
  dynamic nextPageUrl;
  String? path;
  int? perPage;
  dynamic prevPageUrl;
  int? to;
  int? total;

  ResponseData({
    this.currentPage,
    this.data,
    this.firstPageUrl,
    this.from,
    this.lastPage,
    this.lastPageUrl,
    this.nextPageUrl,
    this.path,
    this.perPage,
    this.prevPageUrl,
    this.to,
    this.total,
  });

  factory ResponseData.fromJson(Map<String, dynamic> json) => ResponseData(
    currentPage: json["current_page"],
    data: json["data"] == null ? [] : List<Datum>.from(json["data"]!.map((x) => Datum.fromJson(x))),
    firstPageUrl: json["first_page_url"],
    from: json["from"],
    lastPage: json["last_page"],
    lastPageUrl: json["last_page_url"],
    nextPageUrl: json["next_page_url"],
    path: json["path"],
    perPage: json["per_page"],
    prevPageUrl: json["prev_page_url"],
    to: json["to"],
    total: json["total"],
  );

  Map<String, dynamic> toJson() => {
    "current_page": currentPage,
    "data": data == null ? [] : List<dynamic>.from(data!.map((x) => x.toJson())),
    "first_page_url": firstPageUrl,
    "from": from,
    "last_page": lastPage,
    "last_page_url": lastPageUrl,
    "next_page_url": nextPageUrl,
    "path": path,
    "per_page": perPage,
    "prev_page_url": prevPageUrl,
    "to": to,
    "total": total,
  };
}

class Datum {
  int? id;
  String? storeName;
  String? storeLocation;
  dynamic picture;
  String? estimatedDeliveryTime;
  int? rating;
  double? distance;
  int? cartPrice;

  Datum({
    this.id,
    this.storeName,
    this.storeLocation,
    this.picture,
    this.estimatedDeliveryTime,
    this.rating,
    this.distance,
    this.cartPrice,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"],
    storeName: json["store_name"],
    storeLocation: json["store_location"],
    picture: json["picture"],
    estimatedDeliveryTime: json["estimated_delivery_time"],
    rating: json["rating"],
    distance: json["distance"]?.toDouble(),
    cartPrice: json["cart_price"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "store_name": storeName,
    "store_location": storeLocation,
    "picture": picture,
    "estimated_delivery_time": estimatedDeliveryTime,
    "rating": rating,
    "distance": distance,
    "cart_price": cartPrice,
  };
}
