import 'package:app24_user_app/Laundry-app/views/ServicesScreens/services_home_screen.dart';
import 'package:app24_user_app/Laundry-app/views/widgets/custum_appbar.dart';
import 'package:app24_user_app/constants/app_text.dart';
import 'package:app24_user_app/constants/color_coman.dart';
import 'package:app24_user_app/constants/textstyle.dart';
import 'package:app24_user_app/modules/delivery/cart/cart.dart';
import 'package:app24_user_app/modules/shop_list/store_list_loading_screen.dart';
import 'package:app24_user_app/modules/shop_list/store_list_model.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../utils/services/api/api_providers.dart';

class StoreListScreen extends StatefulWidget {
  const StoreListScreen({super.key, this.lat, this.long, this.collectionAddress});

  final double? lat;
  final double? long;
  final String? collectionAddress;

  @override
  State<StoreListScreen> createState() => _StoreListScreenState();
}

class _StoreListScreenState extends State<StoreListScreen> {
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      //API call after screen build
      context.read(storeListProvider.notifier).fetchStoreList(
            lat: widget.lat,
            long: widget.long,
            init: true,
          );
    });
  }

  loadData({bool init = true, required BuildContext context}) {
    context.read(storeListProvider.notifier).fetchStoreList(
          lat: widget.lat,
          long: widget.long,
          init: true,
        );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CusturmAppbar(
        child: ListTile(
          leading: backarrow(context: context,
          onTap: () {
            Navigator.pop(context);
          },),
          title: Center(
            child: Text(Apptext.shoplist, style: AppTextStyle().appbathead),
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 14.0, vertical: 14.0),
        child: Consumer(builder: (context, watch, child) {
          final state = watch(storeListProvider);
          if (state.isLoading) {
            return StoreLoadingScreen();
          } else if (state.isError) {
            return LoadingError(
              onPressed: (res) {
                loadData(init: true, context: res);
              },
              message: state.errorMessage.toString(),
            );
          } else {
            return (state.response.responseData.data.length == 0)? SizedBox()
             : ListView.builder(
              itemCount: state.response.responseData.data.length,
              itemBuilder: (context, index) {
                return shopCardWidget(
                  model: state.response.responseData.data[index]
                );
              },
            );
          }
        }),
      ),
    );
  }

  Widget shopCardWidget({Datum? model}) {
    print("model?.storeName :: ${model?.id}");
    return InkWell(

      child: Card(
          child: Row(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(14),
            child: Container(
              height: 100,
              width: 100,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(14),
              ),
              child: Image.network(
                model?.picture ?? '',
                errorBuilder: (context, error, stackTrace) {
                  return Image.asset("assets/images/emptyImage.png");
                },
              ),
            ),
          ),
          SizedBox(
            width: 18,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text(
                model?.storeName ?? '',
                style: AppTextStyle().servicescreenhead,
              ),
              Text(
                "${Apptext.distance} ${model?.estimatedDeliveryTime ?? 0}",
                style: TextStyle(
                  color: AppColor.servicetext.withOpacity(0.27),
                  fontWeight: FontWeight.w500,
                  fontSize: 14.0,
                ),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  RatingBar.builder(
                    itemSize: 12,
                    initialRating: model?.rating?.toDouble() ?? 0.0,
                    direction: Axis.horizontal,
                    allowHalfRating: true,
                    itemCount: 5,
                    itemPadding: EdgeInsets.symmetric(horizontal: 1.0),
                    itemBuilder: (context, _) => Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
                    onRatingUpdate: (rating) {
                      print(rating);
                    },
                    ignoreGestures: true,
                  ),
                  SizedBox(
                    width: 14.0,
                  ),
                  Text.rich(TextSpan(
                      text: 'Total ',
                      style: TextStyle(
                          height: 0.9,
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                          color: AppColor.servicetext1),
                      children: <InlineSpan>[
                        TextSpan(
                          text: "${model?.cartPrice}",
                          style: TextStyle(
                              fontSize: 24,
                              fontWeight: FontWeight.bold,
                              color: AppColor.servicetext),
                        )
                      ])),
                ],
              )
            ],
          ),
        ],
      )),
      onTap: () => {Navigator.of(context).push(MaterialPageRoute(builder:
      (context) {
        return Cart(
          fromAddress: widget.collectionAddress,
          isMainStoreID: model?.id,
          lat: widget.lat,long: widget.long,
        newCartPage: true,);
      },))},
    );
  }
}
