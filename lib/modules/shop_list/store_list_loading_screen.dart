import 'dart:async';

import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class StoreLoadingScreen extends StatefulWidget {
  const StoreLoadingScreen({super.key});

  @override
  State<StoreLoadingScreen> createState() => _StoreLoadingScreenState();
}

class _StoreLoadingScreenState extends State<StoreLoadingScreen> {
  Timer? _timer;
  int _start = 16; // time to popup still working window /// in seconds

  @override
  void initState() {
    startTimer();
    super.initState();
  }

  @override
  void dispose() {
    if (_timer != null) {
      _timer!.cancel();
    }
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height,
        child: Stack(
          fit: StackFit.expand,
          children: [
            Shimmer.fromColors(
                baseColor: Colors.grey[300]!,
                highlightColor: Colors.grey[100]!,
                child: ListView(
                    shrinkWrap: true,
                    children: buildRestaurantCartShimmerItem(context))),
            Positioned(
              left: 20,
              right: 20,
              top: 230,
              child: _start == 0 ? buildStillLoadingWidget() : Container(),
            )
          ],
        ));
  }

  void startTimer() {
    if (_timer != null) {
      _timer!.cancel();
      _timer = null;
    } else {
      _timer = Timer.periodic(const Duration(seconds: 1), (Timer timer) {
        setState(() {
          if (_start < 1) {
            _timer!.cancel();
          } else {
            _start = _start - 1;
          }
        });
      });
    }
  }

  buildRestaurantCartShimmerItem(context) {
    List<Widget> list = [];

    for (var i = 0; i < 10; i++) {
      list.add(Container(
        child: Row(
          children: [
            Container(
              height: 100,
              margin: EdgeInsets.only(top: 10, left: 20),
              width: 100,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xffefefef),
                      blurRadius: 20,
                      spreadRadius: 3,
                    ),
                  ]),
            ),
            SizedBox(width: 14.0,),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  height: 20,
                  width: MediaQuery.of(context).size.width *0.4,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Color(0xffefefef),
                          blurRadius: 20,
                          spreadRadius: 3,
                        ),
                      ]),
                ),
                SizedBox(height: 14.0,),
                Container(
                  height: 14,
                  width: MediaQuery.of(context).size.width *0.3,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Color(0xffefefef),
                          blurRadius: 20,
                          spreadRadius: 3,
                        ),
                      ]),
                ),
                SizedBox(height: 14.0,),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(
                      height: 12,
                      width: MediaQuery.of(context).size.width *0.2,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Color(0xffefefef),
                              blurRadius: 20,
                              spreadRadius: 3,
                            ),
                          ]),
                    ),
                    SizedBox(width: 6,),
                    Container(
                      height: 14,
                      width: MediaQuery.of(context).size.width *0.14,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Color(0xffefefef),
                              blurRadius: 20,
                              spreadRadius: 3,
                            ),
                          ]),
                    ),
                    SizedBox(width: 6,),
                    Container(
                      height: 24,
                      width: MediaQuery.of(context).size.width *0.14,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Color(0xffefefef),
                              blurRadius: 20,
                              spreadRadius: 3,
                            ),
                          ]),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ));
    }

    return list;
  }

  buildStillLoadingWidget() {
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(15)),
      width: MediaQuery.of(context).size.width,
      child: const ListTile(
        title: Text("Please wait..."),
        subtitle: Text(
            "The network seems to be too busy. We suggest you to wait for some more time."),
        leading: CircularProgressIndicator.adaptive(),
      ),
    );
  }


}
