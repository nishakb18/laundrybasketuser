import 'package:app24_user_app/modules/shop_list/store_list_model.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/api/api_response_state2.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class StoreListProvider extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  StoreListProvider(this._apiRepository) : super(ResponseState2(isLoading: true,response: {}));

  Future<void> fetchStoreList({bool init = true,double? lat, double? long}) async {
    StoreListModel storeList = StoreListModel();
    try {
      if (init) state = state.copyWith(isLoading: true);

      storeList = await _apiRepository.fetchStoreList(
        lat: lat,
        long: long,
      );

      state = state.copyWith(
          response: storeList, isLoading: false, isError: false);
    } catch (e) {
      showLog("error here is ${e.toString()}");
      state = state.copyWith(
          errorMessage: e.toString(), isLoading: false, isError: true);
    }
  }

  Future<void> fetchStoreListToCart({bool init = true,double? lat, double? long}) async {
    try {
      if (init) state = state.copyWith(isLoading: true);

      StoreListModel storeList = await _apiRepository.fetchStoreList(
        lat: lat,
        long: long,
      );

      state = state.copyWith(
          response: storeList, isLoading: false, isError: false);
    } catch (e) {
      showLog("error here is ${e.toString()}");
      state = state.copyWith(
          errorMessage: e.toString(), isLoading: false, isError: true);
    }
  }

}