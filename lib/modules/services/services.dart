import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/widgets/common_appbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ServicesPage extends StatefulWidget {
  @override
  _ServicesPageState createState() => _ServicesPageState();
}

class _ServicesPageState extends State<ServicesPage> {
  List<String> services_list = [
    "False Sealing Work",
    "Quick Tailoring",
    "Wood Polishing",
    "Smokeless Oven",
    "Mobile Repair Service",
    "Puncture Service",
    "False Sealing Work",
    "Quick Tailoring",
    "Wood Polishing",
    "Smokeless Oven",
    "Mobile Repair Service",
    "Puncture Service",
    "False Sealing Work",
    "Quick Tailoring",
    "Wood Polishing",
    "Smokeless Oven",
    "Mobile Repair Service",
    "Puncture Service"
  ];
  List<Color> colorList = [
    Color(0xffb6e3fc),
    Color(0xfffbebbb),
    Color(0xffd7f9cd),
    Color(0xffe3cfff),
    Color(0xfffcc3d1),
    Color(0xffc3fcf6),
    Color(0xfff4a592)
  ];

  String? filter;
  TextEditingController controller = new TextEditingController();

  initState() {
    controller.addListener(() {
      setState(() {
        filter = controller.text;
      });
    });
    super.initState();
  }

  dispose() {
    super.dispose();
  }

  Widget _buildListView(list) {
    return ListView.separated(
      itemCount: list.length,
      shrinkWrap: true,
      padding: EdgeInsets.all(0),
      itemBuilder: (BuildContext context, int index) {
        if (filter == null || filter == "") {
          return _buildRow(list[index], index);
        } else {
          if (list[index].toLowerCase().contains(filter!.toLowerCase())) {
            return _buildRow(list[index], index);
          } else {
            return new Container();
          }
        }
      },
      separatorBuilder: (context, index) {
        if (filter == null || filter == "") {
          return Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Divider(
                height: 1,
              ));
        } else {
          if (list[index].toLowerCase().contains(filter!.toLowerCase())) {
            return Divider(
              height: 1,
            );
          } else {
            return new Container();
          }
        }
      },
    );
  }

  Widget _buildRow(title, index) {
    return Container(
        decoration: BoxDecoration(
            color: Colors.transparent, borderRadius: BorderRadius.circular(10)),
        child: Material(
            color: Colors.transparent,
            child: InkWell(
                onTap: () {},
                child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
                    child: Row(
                      children: <Widget>[
                        Container(
                          width: 35,
                          height: 35,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              color: colorList[index > 3
                                  ? index % colorList.length
                                  : index]),
                          alignment: Alignment.center,
                          child: Text(
                            title.substring(0, 1),
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                                color: Colors.black54),
                          ),
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        Expanded(
                          child: Text(
                            title,
                            style: TextStyle(
                                color: App24Colors.darkTextColor,
                                fontWeight: FontWeight.w500,
                                fontSize: 16),
                          ),
                        )
                      ],
                    )))));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomAppBar(
          title: "Services",
        ),
        backgroundColor: Colors.white,
        body: SafeArea(
            child: Column(children: <Widget>[
          SizedBox(
            height: 10,
          ),
          SizedBox(
            height: 50,
            child: Container(
                margin: EdgeInsets.symmetric(vertical: 6, horizontal: 20),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: Theme.of(context).cardColor,
                ),
                child: TextField(
                  controller: controller,
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      contentPadding:
                          EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                      prefixIcon: Icon(
                        Icons.search,
                        color: App24Colors.greenOrderEssentialThemeColor,
                      ),
                      hintText: "Search"),
                )),
          ),
          Expanded(child: _buildListView(services_list)),
        ])) // This trailing comma makes auto-formatting nicer for build methods.
        );
  }
}
