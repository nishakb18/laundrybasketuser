import 'dart:async';
import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/route_path.dart';
import 'package:app24_user_app/constants/shared_preferences_path.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/utils/services/socket/socket_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return SplashScreenState();
  }
}

class SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  AnimationController? motionController;

  double size = 20;

  late final SocketService _socketService;

  @override
  void initState() {
    super.initState();

    // FirebaseNotifications(context).setUpFirebase();
    loadData();
    motionController = AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
      lowerBound: 0.9,
    );
    motionController!.forward();
    motionController!.repeat(reverse: true);
    motionController!.addListener(() {
      setState(() {
        size = motionController!.value * 250;
      });
    });
  }

  @override
  void dispose() {
    motionController!.dispose();

    super.dispose();
  }

  Future<Timer> loadData() async {
    return Timer(const Duration(seconds: 0), onDoneLoading);
  }
  //2024 commented line
  // nextscreen() {
  //   Navigator.push(
  //       // LogInPage() SignUpPage()
  //       context,
  //       MaterialPageRoute(builder: (context) => OTPVerificationPage()));
  // }

  Future<void> onDoneLoading() async {
    var prefs = await SharedPreferences.getInstance();
    var userRegStatus =
        prefs.getBool(SharedPreferencesPath.isUserRegisteredKey) ?? false;
    showLog('is registered $userRegStatus');
    showLog("user is register ----------------$userRegStatus");
    pageLoad(userRegStatus);
  }

  pageLoad(bool userRegStatus){
    if (userRegStatus) {
      _socketService = SocketService.instance;
      context.read(homeNotifierProvider.notifier).getAppConfigurations();
      context.read(homeNotifierProvider.notifier).getProfile();

      context.read(orderDetailsNotifierProvider.notifier).checkOngoingOrders();
      _socketService.listenSocket();
      // SocketService().connectAndListen();
      //   Navigator.pushReplacementNamed(
      //       context, userRegStatus ? laundryhomescreen : loginScreen);
      // }
      showLog("userRegStatus was :: $userRegStatus");
      Navigator.pushReplacementNamed(
          context, userRegStatus ? homeScreen : loginScreen);

    }
    else {
      Navigator.pushReplacementNamed(
          context, loginScreen);
    }
  }

  // onFinished(){
  //   return Text("Powered by App24",style: TextStyle(color: Colors.white),);
  // }
  bool visible = true;
  Future<bool> onWillPop() async {
    return (await SystemChannels.platform
            .invokeMethod('SystemNavigator.pop') // showDialog(
        //     context: context,
        //     builder: (BuildContext context) => exitDialogWidget(context))
        ) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          color: Colors.black,
          alignment: Alignment.center,
          // margin: EdgeInsets.only(top: 300),
          child: Image.asset(App24UserAppImages.splashScreen,
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              fit: BoxFit.fill)),
    );
  }
}
