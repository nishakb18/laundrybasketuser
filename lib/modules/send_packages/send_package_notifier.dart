import 'dart:convert';

import 'package:app24_user_app/constants/api_path.dart';
import 'package:app24_user_app/constants/shared_preferences_path.dart';
import 'package:app24_user_app/helpers.dart';
import 'package:app24_user_app/models/history_model.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/api/api_response_state2.dart';
import 'package:app24_user_app/utils/services/database/database_service.dart';
import 'package:app24_user_app/utils/services/database/history_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SendPackageNotifier extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  SendPackageNotifier(this._apiRepository)
      : super(ResponseState2(isLoading: true));

  HistoryDatabase historyDatabase = HistoryDatabase(DatabaseService.instance);

  Future<void> getSelectPackages({bool init = true}) async {
    try {
      if (init) state = state.copyWith(isLoading: true);
      final selectPackage = await _apiRepository.fetchSelectPackages();
      state = state.copyWith(
          response: selectPackage, isLoading: false, isError: false);
    } catch (e) {
      showLog(e.toString());
      state = state.copyWith(
          isError: true, isLoading: false, errorMessage: e.toString());
    }
  }

  Future<void> getVehicleInfo(
      {bool init = true, BuildContext? context, Map? body}) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String? token = prefs.getString(SharedPreferencesPath.accessToken);
      Map<String, String> headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer $token"
      };
      Map charges = {
        'from_lat': body!['from_lat'],
        'from_lng': body['from_lng'],
        'to_lat': body['to_lat'],
        'to_lng': body['to_lng'],
        'weight': body['weight']
      };

      if (init) state = state.copyWith(isLoading: true);

      if (context!.read(homeNotifierProvider.notifier).profile == null) {
        await context.read(homeNotifierProvider.notifier).getProfile();
      }
      // dynamic vehicleInfo =
      //     await _apiRepository.fetchSendPackageVehicleInfo(charges);

      // state=state.copyWith(
      //     response: vehicleInfo.toString(), isLoading: false, isError: false);

      Helpers()
          .getPostData(App24UserAppAPI.vehicleInfoUrl, charges,headers)
          .then((value) {
        final vehicleInfo =value;
        print("status code: "+json.decode(value!)['status'].toString());
        if (json.decode(value!)['status'] == 200) {
          state = state.copyWith(
              response: vehicleInfo, isLoading: false, isError: false);
        }
        else if(json.decode(value!)['status'] == 400)
        {
          print("sgwrg");
          state = state.copyWith(
              errorMessage: "Sorry, our service is not available in this area!", isLoading: false, isError: true);
        }
      });
      // state = state.copyWith(
      //     response: vehicleInfo, isLoading: false, isError: false);

    }
    catch (e) {
      showLog("zvzvv"+e.toString());
      state = state.copyWith(
          isError: true, isLoading: false, errorMessage: "ssss");
    }
  }
  Future<void> sendPackageSave({bool init = true, Map? body}) async {
    try {
      if (init) state = state.copyWith(isLoading: true);

      final addCartResponse =
          await _apiRepository.fetchCourierServiceSave(body);
      final orderCreateResponse =
          await _apiRepository.fetchDeliveryOrderInsert();
//9447852227
//9447868531
// insert into local DB
      print("adding to db");

     int i=await historyDatabase.addHistory(HistoryData(
          id: orderCreateResponse.responseData!.id,
          status: orderCreateResponse.responseData!.status,
          booking_type: 'Send',
          desc: 'To: ${orderCreateResponse.responseData!.deliveryContactNum}',
          dateTime: orderCreateResponse.responseData!.createdAt,
          amount: orderCreateResponse.responseData!.totalPayable,
          adminService: orderCreateResponse.responseData!.adminService));

      print("added to db rows: "+i.toString());
      state = state.copyWith(
          response: orderCreateResponse, isLoading: false, isError: false);
    } catch (e) {
      showLog(e.toString());
      state = state.copyWith(
          isError: true, isLoading: false, errorMessage: e.toString());
    }
  }
}
