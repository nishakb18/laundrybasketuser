
import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/constants/global_constants.dart';
import 'package:app24_user_app/constants/shared_preferences_path.dart';
import 'package:app24_user_app/helpers.dart';
import 'package:app24_user_app/models/home_delivery_order_insert_model.dart';
import 'package:app24_user_app/models/home_delivery_send_order_detail.dart';
import 'package:app24_user_app/models/location_model.dart';
import 'package:app24_user_app/modules/send_packages/select_package_model.dart';
import 'package:app24_user_app/modules/send_packages/send_package_notifier.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/utils/services/socket/socket_service.dart';
import 'package:app24_user_app/widgets/bottomsheets/confirm_send_package_bsheet/confirm_send_package_order_bottom_sheet.dart';
import 'package:app24_user_app/widgets/bottomsheets/make_payment_bottomsheet/make_payment_bottom_sheet.dart';
import 'package:app24_user_app/widgets/bottomsheets/order_success_bottomSheet.dart';
import 'package:app24_user_app/widgets/bottomsheets/retry_cancel_bottomSheet.dart';
import 'package:app24_user_app/widgets/bottomsheets/schedule_bottomSheet.dart';
import 'package:app24_user_app/widgets/common_appbar.dart';
import 'package:app24_user_app/widgets/common_main_button.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:app24_user_app/widgets/payment_waiting_screen.dart';
import 'package:app24_user_app/widgets/track_order.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:socket_io_client/socket_io_client.dart';

class SendPackage extends StatefulWidget {
  SendPackage({Key? key}) : super(key: key);

  @override
  _SendPackageState createState() => _SendPackageState();
}

// enum wheeler {bike, cars,}
class _SendPackageState extends State<SendPackage> {
  TextEditingController contactNumberPickUp = new TextEditingController();
  TextEditingController contactNumberDelivery = new TextEditingController();

  TextEditingController totalWeight = new TextEditingController();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  late LocationModel? pickUpLocation;
  late LocationModel? deliveryLocation;

  String? pickup = "Search pickup location";
  String? delivery = "Search delivery location";

  String title = "DropDown";

  SelectPackageResponseData? selectedPackageValue;

  late Map? sendPackageContentBody;

  late SendPackageNotifier packageContentsNotifier;
  var amountToCheckOut;
  bool use_wallet_checkout = false;
  int? globalOrderId;

  late Razorpay? _razorPay;

  late final SocketService _socketService;

  @override
  void initState() {
    SchedulerBinding.instance!.addPostFrameCallback((_) {
      if (context.read(selectPackageNotifier.notifier).state.response == null)
        context
            .read(selectPackageNotifier.notifier)
            .getSelectPackages();
    });
    super.initState();
    _socketService = SocketService.instance;
    _initRazorPay();
  }

  @override
  void dispose(){
    _razorPay!.clear();
    super.dispose();
  }

  _initRazorPay() {
    _razorPay = new Razorpay();

    _razorPay!.on(Razorpay.EVENT_PAYMENT_SUCCESS, handlerPaymentSuccess);
    _razorPay!.on(Razorpay.EVENT_PAYMENT_ERROR, handlerErrorFailure);
    _razorPay!.on(Razorpay.EVENT_EXTERNAL_WALLET, handlerExternalWallet);
    return _razorPay;
  }

  Future<void> handlerPaymentSuccess(PaymentSuccessResponse response) async {
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => PaymentWaiting(orderID: globalOrderId!,orderType: "send",)));

    // Helpers().getCommonBottomSheet(context: context, content: OrderSuccess(orderType: "send",orderID: globalOrderId,));
    showLog("Payment successful");
/*    emitSocket(
        orderID: context
            .read(orderPackageNotifier.notifier)
            .state
            .response
            .responseData!
            .id);*/
    // context
    //     .read(orderDetailsNotifierProvider.notifier)
    //     .isWaitingForPaymentResponse = true;
    // // var value = await Navigator.push(
    // //     context, MaterialPageRoute(builder: (context) => PaymentWaiting()));
    // // if (value == null) {
    //   navigateToOrderDetails(
    //       orderID: context
    //           .read(orderPackageNotifier.notifier)
    //           .state
    //           .response
    //           .responseData!
    //           .id);
    // }
  }

  void handlerErrorFailure(PaymentFailureResponse response) {
    context.read(orderDetailsNotifierProvider.notifier).checkOngoingOrders();
    _socketService.listenSocket();
    print("Payment failed");
  }

  void handlerExternalWallet(ExternalWalletResponse response) {
    context.read(orderDetailsNotifierProvider.notifier).checkOngoingOrders();
    _socketService.listenSocket();
    print("Payment external");
  }

  Future<void> scheduleBooking() async {
    var value = await Helpers().getCommonBottomSheet(
        context: context,
        content: ScheduleBottomSheet(checkoutType: "fromSend"),
        title: "Schedule Delivery");
    if (value != null) {
      if (value == 'deliver_now') {
        createOrder();
      } else {
        Map map = value;
        sendPackageContentBody!['schedule_date'] = map['date'];
        sendPackageContentBody!['schedule_time'] = map['time'];
        createOrder();
      }
    }
  }

  onConfirmOrderPressed() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString(SharedPreferencesPath.orderType, "send");
    sendPackageContentBody = {
      "type": "send",
      "to_adrs": delivery,
      "to_lat": deliveryLocation!.latitude,
      "to_lng": deliveryLocation!.longitude,
      "from_adrs": pickup,
      "from_lat": pickUpLocation!.latitude,
      "from_lng": pickUpLocation!.longitude,
      "category_id": selectedPackageValue!.id,
      //"vehicle" :
      //"schedule_date"
      //"schedule_time"
      //"is_return" : is_return
      "from_contact_no": contactNumberPickUp.text.trim(),
      "to_contact_num": contactNumberDelivery.text.trim(),
      "weight": totalWeight.text
    };

    var result = await Helpers().getCommonBottomSheet(
        context: context,
        content: ConfirmSendPackageOrderBottomSheet(
          sendItemBody: sendPackageContentBody!,
        ),
        title: "Choose vehicle");
    if (result != null) {
      sendPackageContentBody = result;
      setState(() {
        use_wallet_checkout = sendPackageContentBody!["wallet"] == "1" ? true :false;
      });
      print("if wallet used in send pakcage");
      print(use_wallet_checkout);
      scheduleBooking();
    }
  }

  reloadData({required BuildContext context, bool? init}) {
    return context
        .read(selectPackageNotifier)
        .getSelectPackages(init: init, context: context);
  }

  selectLocation(String title,bool searchUserLocation) {
    showSelectLocationSheet(context: context, title: title,searchUserLocation:searchUserLocation).then((value) {
      if (value != null) {
        LocationModel model = value;
        setState(() {
          if (title == GlobalConstants.labelPickupLocation) {
            pickUpLocation = model;
            pickup = model.description;
          } else {
            deliveryLocation = model;
            delivery = model.description;
          }
        });
      }
    });
  }

  createOrder() {
    print("dony send body:: " +sendPackageContentBody!.toString());

    amountToCheckOut = sendPackageContentBody!['total'];
    // sendPackageContentBody!.remove('total');
    context
        .read(orderPackageNotifier.notifier)
        .sendPackageSave(body: sendPackageContentBody);
    // if (sendPackageContentBody!['wallet'] == '1') {
    //   //if wallet ? update the user wallet amount
    //   context.read(homeNotifierProvider.notifier).getProfile();
    // }
  }

  checkOut(HomeDeliveryOrderInsertResponseData responseData) async {
    var prefs =
    await SharedPreferences
        .getInstance();
    prefs.setString(SharedPreferencesPath.lastOrderType, "send");
    prefs.setInt(SharedPreferencesPath.lastOrderId, responseData.id);

    print(    prefs.getInt(SharedPreferencesPath.lastOrderId));
    print("checkout asdasd" + responseData.id.toString());
    print("checkout" + responseData.storeOrderInvoiceId.toString());
    print("checkout" + amountToCheckOut.toString());

    if(amountToCheckOut != "0") {
      Map checkOutOptions = {
        'description': 'homedelivery${responseData.id}',
        'amount': double.parse(responseData.estimationCost.toString()),
        "notes": {
          "store_invoice_id": responseData.storeOrderInvoiceId,
          "type": "homedelivery",
          "negative_wallet" : responseData.negativeWallet,
          "Store_order_id": responseData.id,

        }
      };
      try {
        print("amouto checkout"+amountToCheckOut.toString());
        // emitSocket(orderID: responseData.id ?? 1);
        _razorPay!.open(await getRazorPayOptions(
            context: context, values: checkOutOptions));
        emitSocket(orderID: responseData.id ?? 1);
        setState(() {
          globalOrderId = responseData.id;
        });
      } catch (e) {
        showLog(e.toString());
        showLog(e.toString());
      }
    }else{
      print("amouto checkout"+amountToCheckOut.toString());
      emitSocket(orderID: responseData.id ?? 1);
      // navigateToOrderDetails(orderID: responseData.id ?? 1);
    }
  }

  emitSocket({required int orderID}) async {
    showLog("Socket emitting");
    try {
      context.read(orderDetailsNotifierProvider.notifier).orderType = 'Send';

      Socket? socket = await _socketService.socket;
      socket!.emit('joinPrivateRoom', {'room_1_R${orderID}_ORDER'});
      showLog("Socket emitted");

      if(use_wallet_checkout == true)
        Helpers().getCommonBottomSheet(
            context: context,
            isDismissible: false,
            content: OrderSuccess(
              orderID: orderID, orderType: "send",

            ));

      // Navigator.pushReplacement(
      //   context,
      //   MaterialPageRoute(
      //       builder: (context) => TrackOrder(
      //         id: orderID.toString(),orderType: "send",
      //       )),
      // );

      // await Helpers().getCommonBottomSheet(
      //     context: context,
      //     isDismissible: false,
      //     content: OrderSuccess(
      //       orderID: orderID,
      //     ));

      //_socketService.listenSocket();
    } catch (e) {
      showLog("error in socket is : ${e.toString()}");
    }
  }

  navigateToOrderDetails({required int orderID}) {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => TrackOrder(
                id: orderID.toString(),orderType: "send",
              )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
          appBar: CustomAppBar(
            hideCartIcon: "true",
            title: "Send packages",

          ),
          body: ProviderListener(
              provider: orderPackageNotifier,
              onChange: (context, dynamic state) {
                if (state.isLoading!) {
                  showProgress(context);
                  //showMessage(context, "loading");
                } else if (state.isError!) {
                  Navigator.pop(context);
                  showMessage(context, state.errorMessage!,true,true);
                } else {
                  Navigator.pop(context);
                  checkOut(state.response.responseData);
                  // emitSocket(
                  //     orderID: context
                  //         .read(orderPackageNotifier.notifier)
                  //         .state
                  //         .response
                  //         .responseData!
                  //         .id);

                  //showMessage(context, "Order Created");
                }
              },
              child: Consumer(builder: (context, watch, child) {
                final state = watch(selectPackageNotifier);
                packageContentsNotifier = watch(selectPackageNotifier.notifier);

                return SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Center(
                              child: Image.asset(
                            App24UserAppImages.sendPackageBG,
                            width: 250,
                          )),
                          const SizedBox(
                            height: 20,
                          ),
                          Text(
                            "Get packages delivered safely and securely",
                            style: TextStyle(
                                color: App24Colors.brownSendPackageThemeColor,
                                fontWeight: FontWeight.bold,
                                fontSize: MediaQuery.of(context).size.width/22),
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          Text(
                            "Pickup Address",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.width/24),
                          ),
                          const SizedBox(
                            height: 10,
                          ),

                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(14.0),
                                color: Color(0xfff5e9e0),
                                border: Border.all(color: Colors.grey[200]!)),
                            child: Material(
                              color: Colors.transparent,
                              child: InkWell(
                                borderRadius: BorderRadius.circular(14.0),
                                onTap: () {
                                  selectLocation(
                                      GlobalConstants.labelPickupLocation,true);
                                },
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 12, horizontal: 15),
                                  child: Row(
                                    children: [
                                      Icon(
                                        Icons.search,
                                        color: App24Colors
                                            .brownSendPackageThemeColor,
                                      ),
                                      const SizedBox(
                                        width: 4.0,
                                      ),
                                      Expanded(
                                        child: Text(
                                          pickup ?? "Search pickup location",
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                            color: App24Colors
                                                .brownSendPackageThemeColor,
                                            fontSize: MediaQuery.of(context).size.width/26,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),

                          // SearchBarDes(
                          //   title: "Search pickup location",
                          // ),
                          const SizedBox(
                            height: 10,
                          ),
                          TextFormField(
                            controller: contactNumberPickUp,
                            validator: (String? arg) {
                              if (arg!.length == 0)
                                return 'Contact Number can\'t be empty!';
                              else if (arg.length > 10) {
                                return "Contact Number can\'t be more than 10 digits";
                              } else if (arg.length < 10) {
                                return "Contact Number can\'t be less than 10 digits";
                              }
                              return null;
                            },
                            keyboardType: TextInputType.number,
                            textInputAction: TextInputAction.done,
                            inputFormatters: <TextInputFormatter>[
                              FilteringTextInputFormatter.allow(RegExp("[0-9]"))
                            ],
                            decoration: InputDecoration(
                              hintText: "Contact Number",
                              hintStyle: TextStyle(fontSize: MediaQuery.of(context).size.width/26)
                            ),
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          Text(
                            "Delivery Address",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.width/24),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(14.0),
                                color: Color(0xfff5e9e0),
                                border: Border.all(color: Colors.grey[200]!)),
                            child: Material(
                              color: Colors.transparent,
                              child: InkWell(
                                borderRadius: BorderRadius.circular(14.0),
                                onTap: () {
                                  print(delivery);
                                  selectLocation(
                                      GlobalConstants.labelDropLocation,true);
                                },
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 12, horizontal: 15),
                                  child: Row(
                                    children: [
                                      Icon(
                                        Icons.search,
                                        color: App24Colors
                                            .brownSendPackageThemeColor,
                                      ),
                                      const SizedBox(
                                        width: 4.0,
                                      ),
                                      Expanded(
                                        child: Text(
                                          delivery ?? "Search delivery location",
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                            color: App24Colors
                                                .brownSendPackageThemeColor,
                                            fontSize: MediaQuery.of(context).size.width/26,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),

                          // if(delivery == null)
                          //   Container(
                          //     child: Text("field required"),
                          //   ),
                          const SizedBox(
                            height: 10,
                          ),
                          TextFormField(
                            validator: (String? arg) {
                              if (arg!.length == 0)
                                return 'Contact Number can\'t be empty!';
                              else if (arg.length > 10) {
                                return "Contact Number can\'t be more than 10 digits";
                              } else if (arg.length < 10) {
                                return "Contact Number can\'t be less than 10 digits";
                              }
                              return null;
                            },
                            controller: contactNumberDelivery,
                            keyboardType: TextInputType.number,
                            textInputAction: TextInputAction.done,
                            inputFormatters: <TextInputFormatter>[
                              FilteringTextInputFormatter.allow(RegExp("[0-9]"))
                            ],
                            decoration: InputDecoration(
                              hintText: "Contact Number",
                              hintStyle: TextStyle(fontSize: MediaQuery.of(context).size.width/26)
                            ),
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                           Text(
                            "Select package content(s)",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.width/24),
                          ),
                          const SizedBox(
                            height: 15,
                          ),
                          (state.isLoading)
                              ? Center(
                                  child: CupertinoActivityIndicator(
                                  radius: 15,
                                ))
                              : (state.isError)
                                  ? LoadingError(
                                      onPressed: (res) {
                                        reloadData(init: true, context: res);
                                      },
                                      message: state.errorMessage,
                                    )
                                  : DropdownButtonFormField(
                                      isExpanded: true,
                                      hint: Text(
                                        "e.g. food, Documents",
                                        style: TextStyle(
                                            color: App24Colors.darkTextColor,
                                            fontWeight: FontWeight.w300,fontSize: MediaQuery.of(context).size.width/26),
                                      ),
                                      elevation: 2,
                                      dropdownColor: Colors.white,
                                      iconEnabledColor: App24Colors.darkTextColor,
                                      style: TextStyle(
                                          color: App24Colors.darkTextColor,
                                          fontWeight: FontWeight.bold),
                                      value: selectedPackageValue,
                                      onChanged: (dynamic value) {
                                        selectedPackageValue = value;
                                        packageContentsNotifier.state = state;
                                      },
                                      // validator: ( arg) {
                                      //   if (arg.length == 0)
                                      //     return 'Weight can\'t be empty!';
                                      //   else
                                      //     return null;
                                      // },
                                      validator: (dynamic value) =>
                                          value == null ? 'field required' : null,
                                      items: buildDropDown(
                                          state.response.responseData)),
                          const SizedBox(
                            height: 15,
                          ),
                          Text(
                            "Total package weight (kg)",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.width/24),
                          ),
                          TextFormField(
                            textInputAction: TextInputAction.done,
                            inputFormatters: <TextInputFormatter>[
                              FilteringTextInputFormatter.allow(RegExp("[0-9.]"))
                            ],
                            controller: totalWeight,
                            validator: (String? arg) {
                              if (arg!.length == 0)
                                return 'Weight can\'t be empty!';
                              else
                                return null;
                            },
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              hintText: "enter total package weight",
                              hintStyle: TextStyle(fontSize: MediaQuery.of(context).size.width/26)
                            ),
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: CommonButton(
                                  onTap: () {
                                    if (_formKey.currentState!.validate()) {
                                      onConfirmOrderPressed();
                                      //Navigator.pop(context, scheduleItems);
                                    }
                                    // scheduleBooking();
                                    // onConfirmOrder();
                                  },
                                  bgColor: App24Colors.brownSendPackageThemeColor,
                                  text: "Confirm Order",
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                );
              }))),
    );
  }

  buildDropDown(List<SelectPackageResponseData> packageList) {
    return packageList.map((value) {
      return DropdownMenuItem(value: value, child: Text(value.itemName!));
    }).toList();
  }
}