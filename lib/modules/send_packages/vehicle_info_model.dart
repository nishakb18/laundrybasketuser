VehicleInfoModel vehicleInfoModelFromJson(str) {
  return VehicleInfoModel.fromJson(str);
}

class VehicleInfoModel {
  String? statusCode;
  String? title;
  String? message;
  List<DeliveryVehicleResponseData>? responseData;
  List<dynamic>? error;

  VehicleInfoModel(
      {this.statusCode,
      this.title,
      this.message,
      this.responseData,
      this.error});

  VehicleInfoModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    if (json['responseData'] != null) {
      responseData = [];
      json['responseData'].forEach((v) {
        responseData!.add(new DeliveryVehicleResponseData.fromJson(v));
      });
    }
    if (json['error'] != null) {
      error = [];
      json['error'].forEach((v) {
        error!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['title'] = this.title;
    data['message'] = this.message;
    if (this.responseData != null) {
      data['responseData'] = this.responseData!.map((v) => v.toJson()).toList();
    }
    if (this.error != null) {
      data['error'] = this.error!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DeliveryVehicleResponseData {
  String? deliveryMode;
  String? distance;
  dynamic estimationCost;
  dynamic estimationCostWithReturn;

  DeliveryVehicleResponseData(
      {this.deliveryMode,
      this.distance,
      this.estimationCost,
      this.estimationCostWithReturn});

  DeliveryVehicleResponseData.fromJson(Map<String, dynamic> json) {
    deliveryMode = json['delivery_mode'];
    distance = json['distance'];
    estimationCost = json['estimation_cost'];
    estimationCostWithReturn = json['estimation_cost_with_return'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['delivery_mode'] = this.deliveryMode;
    data['distance'] = this.distance;
    data['estimation_cost'] = this.estimationCost;
    data['estimation_cost_with_return'] = this.estimationCostWithReturn;
    return data;
  }
}
