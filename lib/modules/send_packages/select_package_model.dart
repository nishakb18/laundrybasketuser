SelectPackageModel selectPackageModelFromJson(str) {
  return SelectPackageModel.fromJson(str);
}

class SelectPackageModel {
  String? statusCode;
  String? title;
  String? message;
  List<SelectPackageResponseData>? responseData;
  List<dynamic>? error;

  SelectPackageModel(
      {this.statusCode,
      this.title,
      this.message,
      this.responseData,
      this.error});

  SelectPackageModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    if (json['responseData'] != null) {
      responseData = [];
      json['responseData'].forEach((v) {
        responseData!.add(new SelectPackageResponseData.fromJson(v));
      });
    }
    if (json['error'] != null) {
      error = [];
      json['error'].forEach((v) {
        error!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['title'] = this.title;
    data['message'] = this.message;
    if (this.responseData != null) {
      data['responseData'] = this.responseData!.map((v) => v.toJson()).toList();
    }
    if (this.error != null) {
      data['error'] = this.error!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class SelectPackageResponseData {
  int? id;
  String? itemName;

  SelectPackageResponseData({this.id, this.itemName});

  SelectPackageResponseData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    itemName = json['item_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['item_name'] = this.itemName;
    return data;
  }
}
