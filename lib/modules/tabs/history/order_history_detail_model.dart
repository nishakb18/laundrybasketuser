OrderDetailsModel orderDetailsModelFromJson(str) {
  return OrderDetailsModel.fromJson(str);
}



class OrderDetailsModel {
  String? statusCode;
  String? title;
  String? message;
  OrderDetailsResponseData? responseData;
  List<dynamic>? responseNewData;
  List<dynamic>? error;

  OrderDetailsModel(
      {this.statusCode,
        this.title,
        this.message,
        this.responseData,
        this.responseNewData,
        this.error});

  OrderDetailsModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    responseData = json['responseData'] != null
        ? OrderDetailsResponseData.fromJson(json['responseData'])
        : null;
    if (json['responseNewData'] != null) {
      responseNewData = [];
      json['responseNewData'].forEach((v) {
        responseNewData!.add(v);
      });
    }
    if (json['error'] != null) {
      error = [];
      json['error'].forEach((v) {
        error!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['statusCode'] = statusCode;
    data['title'] = title;
    data['message'] = message;
    if (responseData != null) {
      data['responseData'] = responseData!.toJson();
    }
    if (responseNewData != null) {
      data['responseNewData'] =
          responseNewData!.map((v) => v.toJson()).toList();
    }
    if (error != null) {
      data['error'] = error!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class OrderDetailsResponseData {
  String? storeOrderInvoiceId;
  String? type;
  String? status;
  String? storeName;
  String? pickupAddress;
  String? deliveryAddress;
  String? storetype;
  String? deliveryMode;
  String? providerName;
  dynamic userComment;
  dynamic  providerRating;
  dynamic totalDistance;
  dynamic totalTimetaken;
  dynamic  shopAmount;
  dynamic  deliveryCharge;
  dynamic  currencySymbol;
  dynamic  taxAmount;
  dynamic  totalPayable;
  List<HomeDelCatDetails>? homeDelCatDetails;

  OrderDetailsResponseData(
      {this.storeOrderInvoiceId,
        this.type,
        this.status,
        this.storeName,
        this.pickupAddress,
        this.deliveryAddress,
        this.storetype,
        this.deliveryMode,
        this.providerName,
        this.userComment,
        this.providerRating,
        this.totalDistance,
        this.totalTimetaken,
        this.shopAmount,
        this.deliveryCharge,
        this.currencySymbol,
        this.taxAmount,
        this.totalPayable,
        this.homeDelCatDetails});

  OrderDetailsResponseData.fromJson(Map<String, dynamic> json) {
    storeOrderInvoiceId = json['store_order_invoice_id'];
    type = json['type'];
    status = json['status'];
    storeName = json['store_name'];
    pickupAddress = json['pickup_address'];
    deliveryAddress = json['delivery_address'];
    storetype = json['storetype'];
    deliveryMode = json['delivery_mode'];
    providerName = json['provider_name'];
    userComment = json['user_comment'];
    providerRating = json['provider_rating'];
    totalDistance = json['total_distance'];
    totalTimetaken = json['total_timetaken'];
    shopAmount = json['shop_amount'];
    deliveryCharge = json['delivery_charge'];
    currencySymbol = json['currency_symbol'];
    taxAmount = json['tax_amount'];
    totalPayable = json['total_payable'];
    if (json['home_del_cat_details'] != null) {
      homeDelCatDetails = [];
      json['home_del_cat_details'].forEach((v) {
        homeDelCatDetails!.add(new HomeDelCatDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['store_order_invoice_id'] = storeOrderInvoiceId;
    data['type'] = type;
    data['status'] = status;
    data['store_name'] = storeName;
    data['pickup_address'] = pickupAddress;
    data['delivery_address'] = deliveryAddress;
    data['storetype'] = storetype;
    data['delivery_mode'] = deliveryMode;
    data['provider_name'] = providerName;
    data['user_comment'] = userComment;
    data['provider_rating'] = providerRating;
    data['total_distance'] = totalDistance;
    data['total_timetaken'] = totalTimetaken;
    data['shop_amount'] = shopAmount;
    data['delivery_charge'] = deliveryCharge;
    data['currency_symbol'] = currencySymbol;
    data['tax_amount'] = taxAmount;
    data['total_payable'] = totalPayable;
    if (homeDelCatDetails != null) {
      data['home_del_cat_details'] =
          homeDelCatDetails!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class HomeDelCatDetails {
  dynamic  id;
  String? type;
  String? catValue;

  HomeDelCatDetails({this.id, this.type, this.catValue});

  HomeDelCatDetails.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    type = json['type'];
    catValue = json['cat_value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = id;
    data['type'] = type;
    data['cat_value'] = catValue;
    return data;
  }
}






// class OrderDetailsModel {
//   String? statusCode;
//   String? title;
//   String? message;
//   OrderDetailsResponseData? responseData;
//   List<dynamic>? responseNewData;
//   List<dynamic>? error;
//
//   OrderDetailsModel(
//       {this.statusCode,
//         this.title,
//         this.message,
//         this.responseData,
//         this.responseNewData,
//         this.error});
//
//   OrderDetailsModel.fromJson(Map<String, dynamic> json) {
//     statusCode = json['statusCode'];
//     title = json['title'];
//     message = json['message'];
//     responseData = json['responseData'] != null
//         ? new OrderDetailsResponseData.fromJson(json['responseData'])
//         : null;
//     if (json['responseNewData'] != null) {
//       responseNewData = [];
//       json['responseNewData'].forEach((v) {
//         responseNewData!.add(v);
//       });
//     }
//     if (json['error'] != null) {
//       error = [];
//       json['error'].forEach((v) {
//         error!.add(v);
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['statusCode'] = this.statusCode;
//     data['title'] = this.title;
//     data['message'] = this.message;
//     if (this.responseData != null) {
//       data['responseData'] = this.responseData!.toJson();
//     }
//     if (this.responseNewData != null) {
//       data['responseNewData'] =
//           this.responseNewData!.map((v) => v.toJson()).toList();
//     }
//     if (this.error != null) {
//       data['error'] = this.error!.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }
//
// class OrderDetailsResponseData {
//   String? storeOrderInvoiceId;
//   String? type;
//   String? status;
//   String? storeName;
//   String? pickupAddress;
//   String? deliveryAddress;
//   String? storetype;
//   String? deliveryMode;
//   String? providerName;
//   dynamic totalDistance;
//   double? totalTimetaken;
//   dynamic shopAmount;
//   dynamic deliveryCharge;
//   dynamic taxAmount;
//   dynamic totalPayable;
//   List<HomeDelCatDetails>? homeDelCatDetails;
//
//   OrderDetailsResponseData(
//       {this.storeOrderInvoiceId,
//         this.type,
//         this.status,
//         this.storeName,
//         this.pickupAddress,
//         this.deliveryAddress,
//         this.storetype,
//         this.deliveryMode,
//         this.providerName,
//         this.totalDistance,
//         this.totalTimetaken,
//         this.shopAmount,
//         this.deliveryCharge,
//         this.taxAmount,
//         this.totalPayable,
//         this.homeDelCatDetails});
//
//   OrderDetailsResponseData.fromJson(Map<String, dynamic> json) {
//     storeOrderInvoiceId = json['store_order_invoice_id'];
//     type = json['type'];
//     status = json['status'];
//     storeName = json['store_name'];
//     pickupAddress = json['pickup_address'];
//     deliveryAddress = json['delivery_address'];
//     storetype = json['storetype'];
//     deliveryMode = json['delivery_mode'];
//     providerName = json['provider_name'];
//     totalDistance = json['total_distance'];
//     totalTimetaken = json['total_timetaken'];
//     shopAmount = json['shop_amount'];
//     deliveryCharge = json['delivery_charge'];
//     taxAmount = json['tax_amount'];
//     totalPayable = json['total_payable'];
//     if (json['home_del_cat_details'] != null) {
//       homeDelCatDetails = [];
//       json['home_del_cat_details'].forEach((v) {
//         homeDelCatDetails!.add(new HomeDelCatDetails.fromJson(v));
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['store_order_invoice_id'] = this.storeOrderInvoiceId;
//     data['type'] = this.type;
//     data['status'] = this.status;
//     data['store_name'] = this.storeName;
//     data['pickup_address'] = this.pickupAddress;
//     data['delivery_address'] = this.deliveryAddress;
//     data['storetype'] = this.storetype;
//     data['delivery_mode'] = this.deliveryMode;
//     data['provider_name'] = this.providerName;
//     data['total_distance'] = this.totalDistance;
//     data['total_timetaken'] = this.totalTimetaken;
//     data['shop_amount'] = this.shopAmount;
//     data['delivery_charge'] = this.deliveryCharge;
//     data['tax_amount'] = this.taxAmount;
//     data['total_payable'] = this.totalPayable;
//     if (this.homeDelCatDetails != null) {
//       data['home_del_cat_details'] =
//           this.homeDelCatDetails!.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }
//
// class HomeDelCatDetails {
//   dynamic id;
//   String? type;
//   String? catValue;
//
//   HomeDelCatDetails({this.id, this.type, this.catValue});
//
//   HomeDelCatDetails.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     type = json['type'];
//     catValue = json['cat_value'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['type'] = this.type;
//     data['cat_value'] = this.catValue;
//     return data;
//   }
// }
