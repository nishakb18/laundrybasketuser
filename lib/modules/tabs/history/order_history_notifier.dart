

import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/api/api_response_state2.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';


class OrderHistoryNotifier extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  OrderHistoryNotifier(this._apiRepository) : super(ResponseState2(isLoading: true));

  Future<void> getOrderDetails({bool init = true,required BuildContext context,required String orderId}) async {
    try {
      if (init) state = state.copyWith(isLoading: true);
      final orderDetails = await _apiRepository.fetchOrderDetails(orderId: orderId);
      state =
          state.copyWith(response: orderDetails, isLoading: false, isError: false);
    } catch (e) {
      state = state.copyWith(
          errorMessage: e.toString(), isLoading: false, isError: true);
    }
  }



}
