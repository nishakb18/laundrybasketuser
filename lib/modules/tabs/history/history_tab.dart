import 'package:app24_user_app/constants/app_text.dart';
import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/helpers.dart';
import 'package:app24_user_app/models/history_model.dart';
import 'package:app24_user_app/modules/tabs/history/history_loading.dart';
import 'package:app24_user_app/modules/tabs/history/order_history_detail_pop_up.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:app24_user_app/widgets/offline_builder_widget.dart';
import 'package:app24_user_app/widgets/pagination_builder.dart';
import 'package:app24_user_app/widgets/track_order.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../../../Laundry-app/views/widgets/custum_appbar.dart';
import '../../../constants/textstyle.dart';

class HistoryTabPage extends StatefulWidget {
  const HistoryTabPage({super.key, this.isNewApp = false});
  final bool? isNewApp;

  @override
  State<HistoryTabPage> createState() => _HistoryTabPageState();
}

class _HistoryTabPageState extends State<HistoryTabPage> {
  String? selectedHistoryTab = "past";

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      //API call after screen build
      context.read(historyNotifierProvider.notifier).getPastHistory();
    });
  }

  Future<bool> onWillPop() async {
    // return (await     Helpers().getCommonBottomSheet(
    //     context: context,
    //     content: ExitAppBottomSheet(
    //     ),
    //     title: "Come back soon!")
    //     // showDialog(
    //     //     context: context,
    //     //     builder: (BuildContext context) => exitDialogWidget(context))
    // ) ??
    //     false;
    Navigator.pushNamedAndRemoveUntil(
        context, '/homeScreen', (Route<dynamic> route) => false);
    return false;
  }

  reloadData({required BuildContext context, bool? init}) {
    return context.read(historyNotifierProvider.notifier).getPastHistory();
  }

  List<HistoryData>? history;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onWillPop,
      child: Scaffold(
          backgroundColor: Colors.white,
          appBar: (widget.isNewApp == true)?
          CusturmAppbar(
            child: ListTile(
              title: Center(
                child: Text(Apptext.history, style: AppTextStyle().appbathead),
              ),
            ),
          )
          :AppBar(
            automaticallyImplyLeading: false,
            title: Center(
                child: Text(
              "History",
              style: TextStyle(
                  color: App24Colors.darkTextColor,
                  fontSize: MediaQuery.of(context).size.width / 20),
            )),
            backgroundColor: Colors.white,
            elevation: 1,
          ),
          body: SafeArea(
            child: OfflineBuilderWidget(
              Stack(children: <Widget>[
                Container(
                  decoration: const BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(30),
                          topLeft: Radius.circular(30)),
                      color: Colors.white),
                  child: Column(
                    // crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      const SizedBox(
                        height: 10,
                      ),
                      Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(18)),
                        margin: const EdgeInsets.symmetric(horizontal: 20),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Container(
                        decoration: BoxDecoration(
                            color: Theme.of(context).cardColor,
                            borderRadius: BorderRadius.circular(18)),
                        margin: const EdgeInsets.symmetric(horizontal: 20),
                        child: Row(
                          children: <Widget>[
                            buildHistoryTab(tab: "Current"),
                            buildHistoryTab(tab: "Upcoming"),
                            buildHistoryTab(tab: "Past"),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Expanded(
                        child: Consumer(builder: (context, watch, child) {
                          final state = watch(selectedHistoryTab == 'past'
                              ? historyNotifierProvider
                              : selectedHistoryTab == 'current'
                                  ? currentHistoryNotifierProvider
                                  : upcomingHistoryNotifierProvider);
                          if (state.isLoading) {
                            return HistoryLoading();
                          } else if (state.isError) {
                            return LoadingError(
                              onPressed: (res) {
                                reloadData(init: true, context: res);
                              },
                              message: state.errorMessage,
                            );
                          } else {
                            return PaginationBuilder(
                                listLength: state.response.length,
                                // limit: 2,

                                onNextPageRequested: (int pageNo) {
                                  // state.response.clear();
                                  if (selectedHistoryTab == 'past') {
                                    //state.response.clear();
                                    context
                                        .read(historyNotifierProvider.notifier)
                                        .getPastHistory(
                                            init: false, page: pageNo);
                                  } else if (selectedHistoryTab == 'current') {
                                    //state.response.clear();
                                    context
                                        .read(currentHistoryNotifierProvider
                                            .notifier)
                                        .getCurrentUpcomingHistory(
                                            type: selectedHistoryTab!);
                                  } else {
                                    //state.response.clear();
                                    context
                                        .read(upcomingHistoryNotifierProvider
                                            .notifier)
                                        .getCurrentUpcomingHistory(
                                            type: selectedHistoryTab!);
                                  }
                                },
                                builder: (_, controller) {
                                  if(state.response?.isEmpty) {
                                    return Center(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment
                                            .center,
                                        mainAxisAlignment: MainAxisAlignment
                                            .center,
                                        children: [
                                          Image.asset(
                                            App24UserAppImages
                                                .noNotificationPageIcon,
                                            width: 100,
                                          ),
                                          const SizedBox(
                                            height: 20,
                                          ),
                                          Text("No ${(selectedHistoryTab?.toLowerCase() == "upcoming")? "upcoming": selectedHistoryTab?.toLowerCase()} orders to list")
                                        ],
                                      ),
                                    );
                                  }
                                  else {
                                    return ListView.builder(
                                      shrinkWrap: true,
                                      controller: controller,
                                      itemCount: state.response.length,
                                      itemBuilder: (context, index) {
                                        // print("index: "+index.toString());
                                        return HistoryListItem(
                                          isLaundryApp: widget.isNewApp,
                                          historyData: state.response[index], //
                                        );
                                      });
                                  }
                                });
                          }
                        }),
                      ),
                    ],
                  ),
                ),
              ]),
            ),
          ) // This trailing comma makes auto-formatting nicer for build methods.
          ),
    );
  }

  buildHistoryTab({required String tab}) {
    return Expanded(
      child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(25),
              color: selectedHistoryTab == tab.toLowerCase()
                  ? App24Colors.darkTextColor
                  : Theme.of(context).cardColor),
          child: Material(
              color: Colors.transparent,
              child: InkWell(
                borderRadius: BorderRadius.circular(25),
                onTap: () {
                  setState(() {
                    selectedHistoryTab = tab.toLowerCase();
                    if (selectedHistoryTab == 'past') {
                      context
                          .read(historyNotifierProvider.notifier)
                          .getPastHistory(
                            init: false,
                          );
                    } else if (selectedHistoryTab == 'current') {
                      context
                          .read(currentHistoryNotifierProvider.notifier)
                          .getCurrentUpcomingHistory(type: selectedHistoryTab!);
                    } else {
                      context
                          .read(upcomingHistoryNotifierProvider.notifier)
                          .getCurrentUpcomingHistory(type: selectedHistoryTab!);
                    }
                  });
                },
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8),
                  child: Text(
                    tab,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        color: selectedHistoryTab == tab.toLowerCase()
                            ? Colors.white
                            : App24Colors.darkTextColor,
                        fontSize: MediaQuery.of(context).size.width / 25),
                  ),
                ),
              ))),
    );
  }
}

class HistoryListItem extends StatelessWidget {
  const HistoryListItem({super.key, this.historyData, this.historyType, this.isLaundryApp = false});

  final String? historyType;
  final HistoryData? historyData;
  final bool? isLaundryApp;

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child: Stack(children: [
          Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
            Padding(
              padding: const EdgeInsets.only(top: 25, bottom: 10),
              child: Material(
                  borderRadius: const BorderRadius.only(
                      topRight: Radius.circular(25),
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25)),
                  elevation: 10,
                  shadowColor: const Color(0xfff0f0f0).withOpacity(0.4),
                  color: Colors.white,
                  child: ListTile(
                    contentPadding:
                        const EdgeInsets.symmetric(vertical: 5, horizontal: 20),
                    shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(25),
                            bottomLeft: Radius.circular(25),
                            bottomRight: Radius.circular(25))),
                    onTap: () {
                      if (historyData!.status != "CANCELLED" &&
                          historyData!.status != "ORDERCOMPLETED") {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => TrackOrder(
                                  isLaundryApp: isLaundryApp,
                                      id: historyData!.id.toString(),
                                      orderType:
                                          historyData!.booking_type.toString(),
                                    )));
                      } else {
                        Helpers().getCommonAlertDialogBox(
                            content:
                                OrderDetailPopUp(historyData: historyData!),
                            context: context);
                      } // Navigator.push(context, MaterialPageRoute(builder: (context) => OrderDetailPopUp(historyData: historyData!,)));
                    },
                    isThreeLine: false,
                    title: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Order No.",
                          style: TextStyle(
                              fontSize: MediaQuery.of(context).size.width / 26,
                              fontWeight: FontWeight.w600,
                              color: const Color(0xffc4c4c4)),
                        ),
                        Text(
                          historyData!.id.toString(),
                          style: TextStyle(
                              color: App24Colors.darkTextColor,
                              fontWeight: FontWeight.w700,
                              fontSize: MediaQuery.of(context).size.width / 25),
                        ),
                      ],
                    ),
                    subtitle: Text(
                      historyData!.adminService!,
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: MediaQuery.of(context).size.width / 27),
                    ),
                    trailing: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          "Status",
                          style: TextStyle(
                              fontSize: MediaQuery.of(context).size.width / 26,
                              fontWeight: FontWeight.w600,
                              color: const Color(0xffc4c4c4)),
                        ),
                        Text(
                          historyData!.status!,
                          style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: MediaQuery.of(context).size.width / 25,
                              color: historyData!.status == "CANCELLED"
                                  ? const Color(0xffFF5D44)
                                  : historyData!.status == "ORDERCOMPLETED"
                                      ? const Color(0xff2BD38D)
                                      : historyData!.status == "PENDING"
                                          ? const Color(0xffFFB92E)
                                          : Colors.black),
                        ),
                      ],
                    ),
                  )),
            )
          ]),
          Positioned(
            top: 4,
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(15),
                      topLeft: Radius.circular(15),
                      topRight: Radius.circular(15)),
                  color: App24Colors.darkTextColor),
              child: Text(
                historyData!.dateTime.toString().substring(0, 16),
                style: const TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
          )
        ]));
  }
}
