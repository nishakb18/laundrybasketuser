import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/models/history_model.dart';
import 'package:app24_user_app/modules/my_account/support.dart';
import 'package:app24_user_app/modules/tabs/history/order_history_detail_model.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class OrderDetailPopUp extends StatefulWidget {
  const OrderDetailPopUp({super.key, this.orderID, required this.historyData});
  final String? orderID;
  final HistoryData historyData;

  @override
  State<OrderDetailPopUp> createState() => _OrderDetailPopUpState();
}

class _OrderDetailPopUpState extends State<OrderDetailPopUp> {
  OrderDetailsModel? orderHistory;

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      //API call after screen build
      context.read(orderHistoryDetails.notifier).getOrderDetails(
          context: context, orderId: widget.historyData.id.toString());
    });
  }
  reloadData({BuildContext? context, bool? init}){
   return context!.read(orderHistoryDetails.notifier).getOrderDetails(
        context: context, orderId: widget.historyData.id.toString());
  }


  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
      title: Center(child: Text(widget.historyData.status == "CANCELLED" ? "Cancelled Order" : "Completed Order")),
      content: SingleChildScrollView(
        child: Container(
          color: Colors.transparent,
          child: Consumer(builder: (context, watch, child) {
            final state = watch(orderHistoryDetails);
            if (state.isLoading) {
              return const CircularProgressIndicator.adaptive();
            } else if (state.isError) {
              return LoadingError(
                onPressed: (res) {
                  reloadData(init: true, context: res);
                },
                message: state.errorMessage.toString(),
              );
            } else {
              orderHistory = state.response;
              return Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  const SizedBox(
                    height: 10,
                  ),
                  // const SizedBox(
                  //   height: 10,
                  // ),
                  // Text(
                  //   "Leave a feedback for your driver below.",
                  //   style: TextStyle(
                  //       fontSize: 14,
                  //       fontWeight: FontWeight.w500,
                  //       color: App24Colors.darkTextColor),
                  // ),
                  // const SizedBox(
                  //   height: 10,
                  // ),
                  // Row(
                  //   mainAxisAlignment: MainAxisAlignment.end,
                  //   children: [
                  //     TextButton(
                  //       onPressed: () {
                  //         // Navigator.push(
                  //         //   context,
                  //         //   MaterialPageRoute(
                  //         //       builder: (context) => RideDetailsPage(
                  //         //         type: "details",
                  //         //       )),
                  //         // );
                  //       },
                  //       child: Text(
                  //         "More Details",
                  //         style: TextStyle(
                  //             color:
                  //             App24Colors.darkTextColor),
                  //       ),
                  //     )
                  //   ],
                  // ),
                  Text(
                    "Delivery Details",
                    style: TextStyle(
                        fontSize: MediaQuery.of(context).size.width/25, fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                            color: const Color(0xff1F79FF),
                            shape: BoxShape.circle,
                            border: Border.all(
                                color: Theme.of(context)
                                    .primaryColorLight,
                                width: 5.0)),
                        padding: const EdgeInsets.all(5),
                      ),
                      const SizedBox(
                        width: 15,
                      ),
                      Expanded(
                        child: Text(
                          orderHistory!.responseData!.pickupAddress.toString(),
                          style: TextStyle(
                              fontSize: MediaQuery.of(context).size.width/30,
                              fontWeight: FontWeight.w500),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ],
                  ),
                  Container(
                    height: 20,
                    padding: const EdgeInsets.only(left: 2),
                    alignment: Alignment.centerLeft,
                    child: const VerticalDivider(
                      color: App24Colors.greenOrderEssentialThemeColor,
                    ),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                            color: Colors.red[500],
                            shape: BoxShape.circle,
                            border: Border.all(
                                color: Colors.red[200]!, width: 5.0)),
                        padding: const EdgeInsets.all(5),
                      ),
                      const SizedBox(
                        width: 15,
                      ),
                      Expanded(
                        child: Text(
                          orderHistory!.responseData!.deliveryAddress.toString(),
                          style: TextStyle(
                              fontSize: MediaQuery.of(context).size.width/30,
                              fontWeight: FontWeight.w500),
                          overflow: TextOverflow.ellipsis,
                        ),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Container(
                    color: Colors.transparent,
                    child: Column(
                      children: <Widget>[
                        getDetailsRow("Booking ID", orderHistory!.responseData!.storeOrderInvoiceId,context),
                        const SizedBox(
                          height: 5,
                        ),
                        getDetailsRow("Shop Name", orderHistory!.responseData!.storeName,context),
                        const SizedBox(
                          height: 5,
                        ),
                        // Text(orderHistory!.responseData!.deliveryCharge.toString()),
                        getDetailsRow("Delivery Charge","${orderHistory?.responseData?.currencySymbol ?? 'AED'} ${orderHistory!.responseData!.deliveryCharge.toString()}",context),
                        const SizedBox(
                          height: 5,
                        ),
                        if(orderHistory!.responseData!.type != "Send")
                        // Text(orderHistory!.responseData!.shopAmount.toString()),
                          getDetailsRow("Sub Total","${orderHistory?.responseData?.currencySymbol ?? 'AED'} ${orderHistory!.responseData!.shopAmount.toString()}",context),
                        const SizedBox(
                          height: 5,
                        ),
                        const Divider(),
                        const SizedBox(
                          height: 5,
                        ),
                        // Text(orderHistory!.responseData!.totalPayable.toString()),
                        getDetailsRow("Total","${orderHistory?.responseData?.currencySymbol ?? 'AED'} ${orderHistory!.responseData!.totalPayable.toString()}",context),
                      ],
                    ),
                  ),
                  const SizedBox(height: 10,),
                  if( orderHistory!.responseData!.providerName.toString() != "")
                    Row(
                      children: [
                        Image.asset(
                          App24UserAppImages.rideCompleteDriverImage,
                          width: 50,
                          height: 50,
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        // if( orderHistory!.responseData!.providerName.toString() != "")
                        Flexible(
                          child: Column(
                            crossAxisAlignment:
                            CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Flexible(
                                    child: Text(
                                      orderHistory!.responseData!.providerName.toString(),
                                      overflow: TextOverflow.clip,
                                      style: const TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 15),
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 15,
                                  ),
                                  const Icon(
                                    Icons.star,
                                    color: Color(0xffFFC300),
                                  ),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  const Text(
                                    "4.8",
                                    style: TextStyle(
                                        color: App24Colors.darkTextColor,
                                        fontWeight: FontWeight.w500),
                                  )
                                ],
                              ),
                              // Text("Honda Civic"),
                              // Text("KL 45 L 2834"),
                            ],
                          ),
                        ),
                      ],
                    ),
                  const SizedBox(
                    height: 15,
                  ),

                  // MaterialButton(
                  //     onPressed: () {
                  //       /*Navigator.push(
                  //         context,
                  //         MaterialPageRoute(
                  //             builder: (context) => RideCompletedPage()),
                  //       );*/
                  //     },
                  //     color: App24Colors.darkTextColor,
                  //     shape: RoundedRectangleBorder(
                  //         borderRadius: BorderRadius.circular(10)),
                  //     child: Padding(
                  //       padding: EdgeInsets.symmetric(
                  //           vertical: 15, horizontal: 20),
                  //       child: Text(
                  //         "SUBMIT",
                  //         style: TextStyle(
                  //             color: Colors.white,
                  //             fontWeight: FontWeight.bold),
                  //       ),
                  //     )),
                  // const SizedBox(
                  //   height: 10,
                  // ),
                  TextButton(
                      onPressed: () {},
                      child: Text(
                        "Any questions or feedback?",
                        style: TextStyle(
                            fontSize: MediaQuery.of(context).size.width/35,
                            color: App24Colors.darkTextColor,
                            fontStyle: FontStyle.italic),
                      )),
                  Material(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30)),
                    color: const Color(0xffebebeb),
                    child: ListTile(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15)),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const SupportPage()),
                        );
                      },
                      leading: Image.asset(
                        App24UserAppImages.helpAndSupportIcon,
                        width: MediaQuery.of(context).size.width/20,
                        height: MediaQuery.of(context).size.width/20,
                      ),
                      title: Text(
                        "Help and Support",
                        style: TextStyle(
                            fontSize: MediaQuery.of(context).size.width/30,
                            color: App24Colors.darkTextColor,
                            fontWeight: FontWeight.w500),
                      ),
                      trailing: Image.asset(
                        App24UserAppImages.roundRArrow,
                        width: MediaQuery.of(context).size.width/25,
                        height: MediaQuery.of(context).size.width/25,
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                ],
              );
            }
          })
          ,
        ),
      ),
    );
  }
}
