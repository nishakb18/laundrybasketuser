NotificationModel notificationModelFromJson(str) {
  return NotificationModel.fromJson(str);
}

class NotificationModel {
  String? statusCode;
  String? title;
  String? message;
  NotificationResponseData? responseData;
  List<dynamic>? error;

  NotificationModel(
      {this.statusCode,
      this.title,
      this.message,
      this.responseData,
      this.error});

  NotificationModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    responseData = json['responseData'] != null
        ? new NotificationResponseData.fromJson(json['responseData'])
        : null;
    if (json['error'] != null) {
      error = [];
      json['error'].forEach((v) {
        error!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['title'] = this.title;
    data['message'] = this.message;
    if (this.responseData != null) {
      data['responseData'] = this.responseData!.toJson();
    }
    if (this.error != null) {
      data['error'] = this.error!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class NotificationResponseData {
  int? totalRecords;
  List<Notifications>? notification;

  NotificationResponseData({this.totalRecords, this.notification});

  NotificationResponseData.fromJson(Map<String, dynamic> json) {
    totalRecords = json['total_records'];
    if (json['notification'] != null) {
      notification = [];
      json['notification'].forEach((v) {
        notification!.add(new Notifications.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total_records'] = this.totalRecords;
    if (this.notification != null) {
      data['notification'] = this.notification!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Notifications {
  int? id;
  String? notifyType;
  String? service;
  String? title;
  String? image;
  String? descriptions;
  String? expiryDate;
  String? status;
  int? companyId;
  String? createdType;
  int? createdBy;
  String? modifiedType;
  int? modifiedBy;
  String? deletedType;
  String? deletedBy;
  String? createdAt;
  String? updatedAt;
  String? expiryTime;

  Notifications(
      {this.id,
      this.notifyType,
      this.service,
      this.title,
      this.image,
      this.descriptions,
      this.expiryDate,
      this.status,
      this.companyId,
      this.createdType,
      this.createdBy,
      this.modifiedType,
      this.modifiedBy,
      this.deletedType,
      this.deletedBy,
      this.createdAt,
      this.updatedAt,
      this.expiryTime});

  Notifications.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    notifyType = json['notify_type'];
    service = json['service'];
    title = json['title'];
    image = json['image'];
    descriptions = json['descriptions'];
    expiryDate = json['expiry_date'];
    status = json['status'];
    companyId = json['company_id'];
    createdType = json['created_type'];
    createdBy = json['created_by'];
    modifiedType = json['modified_type'];
    modifiedBy = json['modified_by'];
    deletedType = json['deleted_type'];
    deletedBy = json['deleted_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    expiryTime = json['expiry_time'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['notify_type'] = this.notifyType;
    data['service'] = this.service;
    data['title'] = this.title;
    data['image'] = this.image;
    data['descriptions'] = this.descriptions;
    data['expiry_date'] = this.expiryDate;
    data['status'] = this.status;
    data['company_id'] = this.companyId;
    data['created_type'] = this.createdType;
    data['created_by'] = this.createdBy;
    data['modified_type'] = this.modifiedType;
    data['modified_by'] = this.modifiedBy;
    data['deleted_type'] = this.deletedType;
    data['deleted_by'] = this.deletedBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['expiry_time'] = this.expiryTime;
    return data;
  }
}
