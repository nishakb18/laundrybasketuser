import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/api/api_response_state.dart';
import 'package:app24_user_app/utils/services/api/api_response_state2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class NotificationNotifier extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  NotificationNotifier(this._apiRepository) : super(ResponseState2.initial());

  Future<void> getNotifications(
      {bool init = true,}) async {
    try {
      if (init) state = state.copyWith(isLoading: true);
      final offer = await _apiRepository.fetchNotifications();
      state = state.copyWith(response: offer,isLoading: false,isError: false);
    } catch (e) {
      state = state.copyWith(errorMessage: e.toString(),isError: true,isLoading: false);
    }
  }
}
