import 'package:app24_user_app/Laundry-app/views/widgets/custum_appbar.dart';
import 'package:app24_user_app/constants/app_text.dart';
import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/constants/textstyle.dart';
import 'package:app24_user_app/modules/tabs/notifications/notification_loading.dart';
import 'package:app24_user_app/modules/tabs/notifications/notification_model.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:app24_user_app/widgets/offline_builder_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class NotificationsTabPage extends StatefulWidget {
  const NotificationsTabPage({super.key, this.isLaundryApp = false});
final bool? isLaundryApp;
  @override
  State<NotificationsTabPage> createState() => _NotificationsTabPageState();
}

class _NotificationsTabPageState extends State<NotificationsTabPage> {
  @override
  void initState() {
    SchedulerBinding.instance.addPostFrameCallback((_) {
      //API call after screen build
      context.read(notificationNotifierProvider.notifier).getNotifications();
    });
    super.initState();
  }

  Future<bool> onWillPop() async {
    // return (await     Helpers().getCommonBottomSheet(
    //     context: context,
    //     content: ExitAppBottomSheet(
    //     ),
    //     title: "Come back soon!")
    //     // showDialog(
    //     //     context: context,
    //     //     builder: (BuildContext context) => exitDialogWidget(context))
    // ) ??
    //     false;
    Navigator.pushNamedAndRemoveUntil(context, '/homeScreen', (Route<dynamic> route) => false);
    return false;
  }


  reloadData({required BuildContext context, required bool init}) {
    return context
        .read(notificationNotifierProvider.notifier)
        .getNotifications(init: init);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onWillPop,
      child: Scaffold(
        appBar: (widget.isLaundryApp == true)?
        CusturmAppbar(
          child: ListTile(
            title: Center(
              child: Text(Apptext.notifications, style: AppTextStyle().appbathead),
            ),
          ),
        )
            :AppBar(
          automaticallyImplyLeading: false,
          title: const Center(
              child: Text(
            "Notifications",
            style: TextStyle(color: App24Colors.darkTextColor),
          )),
          backgroundColor: Colors.white,
          elevation: 1,
        ),
        backgroundColor: Colors.white,
        body: SafeArea(
          child: OfflineBuilderWidget(
             SingleChildScrollView(
              child: RefreshIndicator(
                onRefresh: () {
                  return reloadData(init: false, context: context) ??
                      false as Future<void>;
                },
                child: Consumer(builder: (context, watch, child) {
                  final state = watch(notificationNotifierProvider);
                  if (state.isLoading) {
                    return const NotificationsLoading();
                  } else if (state.isError) {
                    return LoadingError(
                      onPressed: (res) {
                        reloadData(init: true, context: res);
                      },
                      message: state.errorMessage.toString(),
                    );
                  } else {
                    if (state.response.responseData.notification.length == 0) {
                      return Padding(
                        padding: const EdgeInsets.only(top: 100),
                        child: Center(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset(
                                App24UserAppImages.noNotificationPageIcon,
                                width: 100,
                              ),
                              const SizedBox(
                                height: 20,
                              ),
                              const Text("You have no notification(s).")
                            ],
                          ),
                        ),
                      );
                    }
                    return ListView.builder(
                        shrinkWrap: true,
                        padding: const EdgeInsets.only(top: 10),
                        itemCount: state.response.responseData.notification.length,
                        itemBuilder: (context, index) {
                          return NotificationListItem(
                            notification: state.response.responseData.notification[index],
                          );
                        });
                  }
                }),
              ),
            ),
          ),
        )
      ),
    );
  }
}

// import 'package:flutter_riverpod/flutter_riverpod.dart';
//
// class NotificationsTabPage extends StatefulWidget {
//   @override
//   _NotificationsTabPageState createState() => _NotificationsTabPageState();
// }
//
// class _NotificationsTabPageState extends State<NotificationsTabPage>
//     with AutomaticKeepAliveClientMixin<NotificationsTabPage> {
//   @override
//   void initState() {
//     SchedulerBinding.instance!.addPostFrameCallback((_) {
//       //API call after screen build
//       context
//           .read(notificationNotifierProvider.notifier)
//           .getNotifications();
//     });
//     super.initState();
//   }
//
//   reloadData({required BuildContext context, required bool init}) {
//     return context
//         .read(notificationNotifierProvider.notifier)
//         .getNotifications(init: init);
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         automaticallyImplyLeading: false,
//         title: Center(
//             child: Text(
//           "Notifications",
//           style: TextStyle(color: App24Colors.darkTextColor),
//         )),
//         backgroundColor: Colors.white,
//         elevation: 1,
//       ),
//       backgroundColor: Colors.white,
//       body: OfflineBuilderWidget(SafeArea(
//           child: Column(children: <Widget>[
//         Expanded(
//           child: Consumer(builder: (context, watch, child) {
//             final state = watch(notificationNotifierProvider);
//             if (state is ResponseLoading) {
//               return WalletLoading();
//             } else if (state is ResponseLoaded) {
//               if (state.response.responseData.notification.length == 0)
//                 return Center(
//                   child: Column(
//                     crossAxisAlignment: CrossAxisAlignment.center,
//                     mainAxisAlignment: MainAxisAlignment.center,
//                     children: [
//                       Image.asset(
//                         App24UserAppImages.noNotificationPageIcon,
//                         width: 100,
//                       ),
//                       const SizedBox(
//                         height: 20,
//                       ),
//                       Text("You have no notifications.")
//                     ],
//                   ),
//                 );
//
//               return RefreshIndicator(
//                 onRefresh: () {
//                   return reloadData(init: false, context: context) ?? false as Future<void>;
//                 },
//                 child: ListView.builder(
//                     padding: EdgeInsets.only(top: 10),
//                     itemCount: state.response.responseData.notification.length,
//                     shrinkWrap: true,
//                     itemBuilder: (context, index) {
//                       return NotificationListItem(
//                         notification:
//                             state.response.responseData.notification[index],
//                       );
//                     }),
//               );
//             } else if (state is ResponseError) {
//               return LoadingError(
//                 onPressed: (res) {
//                   reloadData(init: true, context: res);
//                 },
//                 message: state.message.toString(),
//               );
//             } else {
//               return Container();
//             }
//           }),
//         ),
//       ]))), // This trailing comma makes auto-formatting nicer for build methods.
//     );
//   }
//
//   @override
//   // TODO: implement wantKeepAlive
//   bool get wantKeepAlive => true;
// }
//
class NotificationListItem extends StatelessWidget {
  const NotificationListItem({super.key, this.notification});

  final Notifications? notification;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
        child: Material(
            color: Colors.transparent,
            child: InkWell(
                onTap: () {
                  // detailsPage(_notification_list[index].id);
                },
                borderRadius: BorderRadius.circular(10),
                child: Column(
                  children: <Widget>[
                    // Padding(
                    //   padding: EdgeInsets.only(right: 10),
                    // ),
                    Padding(
                        padding: const EdgeInsets.only(
                          left: 15,
                          right: 15,
                        ),
                        child: Row(
                          children: <Widget>[
                            Container(
                              width: 50,
                              height: 50,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(35),
                                  color: App24Colors.darkTextColor
                                  // _notification_list[index]
                                  //     .isViewed
                                  //     ? App24Colors.darkTextColor
                                  //     : Colors.white
                                  ),
                              alignment: Alignment.center,
                              child: Text(
                                notification!.title!
                                    .substring(0, 1)
                                    .toUpperCase(),
                                style: const TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 16,
                                    color: Colors.white),
                              ),
                            ),
                            const SizedBox(
                              width: 15,
                            ),
                            Expanded(
                                child: Container(
                              padding:
                                  const EdgeInsets.only(left: 30, top: 15, right: 10),
                              margin: const EdgeInsets.symmetric(vertical: 5),
                              decoration: BoxDecoration(
                                  color: const Color(0xffeaeaea),
                                  // _notification_list[index]
                                  //         .isViewed
                                  //     ? Color(0xffeaeaea)
                                  //     : Theme.of(context)
                                  //         .primaryColorLight,
                                  borderRadius: BorderRadius.circular(50)),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: <Widget>[
                                  Text(
                                    notification!.title!,
                                    style: const TextStyle(
                                        color: App24Colors.darkTextColor,
                                        fontWeight: FontWeight.w600,
                                        fontSize: 16),
                                  ),
                                  Text(
                                    notification!.service!,
                                    style: const TextStyle(
                                        color: Colors.black45,
                                        fontWeight: FontWeight.w600),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        right: 20, bottom: 5),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[
                                        const Icon(
                                          Icons.access_time,
                                          size: 14,
                                          color: App24Colors.darkTextColor,
                                        ),
                                        const SizedBox(
                                          width: 5,
                                        ),
                                        Text(
                                          notification!.updatedAt!,
                                          style: const TextStyle(
                                              fontSize: 13,
                                              color: App24Colors.darkTextColor,
                                              fontWeight: FontWeight.w600),
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ))
                          ],
                        )),
                  ],
                ))));
  }
}
