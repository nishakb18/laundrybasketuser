import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class NotificationsLoading extends StatefulWidget {
  const NotificationsLoading({super.key});

  @override
  _NotificationsLoadingState createState() => _NotificationsLoadingState();
}

class _NotificationsLoadingState extends State<NotificationsLoading> {
  Timer? _timer;
  int _start = 10; // time to popup still working window /// in seconds

  @override
  void initState() {
    startTimer();
    super.initState();
  }

  @override
  void dispose() {
    if (_timer != null) {
      _timer!.cancel();
    }
    super.dispose();
  }

  void startTimer() {
    if (_timer != null) {
      _timer!.cancel();
      _timer = null;
    } else {
      _timer = new Timer.periodic(const Duration(seconds: 1), (Timer timer) {
        setState(() {
          if (_start < 1) {
            _timer!.cancel();
          } else {
            _start = _start - 1;
          }
        });
      });
    }
  }

  buildRestaurantCartShimmerItem(context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 8),
      width: MediaQuery.of(context).size.width / 2.5,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 50,
            width: 50,
            margin: EdgeInsets.only(right: 10, top: 15),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Color(0xffefefef),
                    blurRadius: 20,
                    spreadRadius: 3,
                  ),
                ]),
          ),
          // const SizedBox(
          //   height: 5,
          // ),
          Container(
            height: 70,
            // margin: EdgeInsets.only(top: 0, left: 0),
            width: MediaQuery.of(context).size.width / 1.3,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Color(0xffefefef),
                    blurRadius: 20,
                    spreadRadius: 3,
                  ),
                ]),
          ),
          // Container(
          //   height: 10,
          //   margin: EdgeInsets.only(top: 5, left: 20),
          //   width: MediaQuery.of(context).size.width / 1.5,
          //   decoration: BoxDecoration(
          //       borderRadius: BorderRadius.circular(5),
          //       color: Colors.white,
          //       boxShadow: [
          //         BoxShadow(
          //           color: Color(0xffefefef),
          //           blurRadius: 20,
          //           spreadRadius: 3,
          //         ),
          //       ]),
          // ),
          // Container(
          //   height: 10,
          //   margin: EdgeInsets.only(top: 5, left: 20, right: 00),
          //   width: MediaQuery.of(context).size.width / 3,
          //   decoration: BoxDecoration(
          //       borderRadius: BorderRadius.circular(5),
          //       color: Colors.white,
          //       boxShadow: [
          //         BoxShadow(
          //           color: Color(0xffefefef),
          //           blurRadius: 20,
          //           spreadRadius: 3,
          //         ),
          //       ]),
          // ),
          // const SizedBox(
          //   height: 10,
          // ),
        ],
      ),
    );
  }

  buildStillLoadingWidget() {
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(15)),
      width: MediaQuery.of(context).size.width,
      child: ListTile(
        title: Text("Please wait..."),
        subtitle: Text(
            "The network seems to be too busy. We suggest you to wait for some more time."),
        leading: CircularProgressIndicator.adaptive(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        // padding: EdgeInsets.symmetric(vertical: 10),
          height: MediaQuery.of(context).size.height,
          child: Stack(
            fit: StackFit.expand,
            children: [
              Shimmer.fromColors(
                  baseColor: Colors.grey[300]!,
                  highlightColor: Colors.grey[100]!,
                  child: ListView.builder(
                    itemCount: 10,
                    itemBuilder: (context, index) {
                      return buildRestaurantCartShimmerItem(context);
                    },
                      shrinkWrap: true,
                      )),
              Positioned(
                left: 20,
                right: 20,
                top: 230,
                child: _start == 0 ? buildStillLoadingWidget() : Container(),
              )
            ],
          )),
    );
  }
}
