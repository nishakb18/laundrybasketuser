
import 'dart:collection';

import 'package:app24_user_app/Laundry-app/views/ServicesScreens/services_home_screen.dart';
import 'package:app24_user_app/Laundry-app/views/widgets/custum_appbar.dart';
import 'package:app24_user_app/constants/app_text.dart';
import 'package:app24_user_app/constants/textstyle.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

class LaundryPrivacyPolicyScreen extends StatefulWidget {
  const LaundryPrivacyPolicyScreen({super.key, this.isSupportPage = false});
  final bool? isSupportPage;

  @override
  State<LaundryPrivacyPolicyScreen> createState() => _LaundryPrivacyPolicyScreenState();
}

class _LaundryPrivacyPolicyScreenState extends State<LaundryPrivacyPolicyScreen> {
  late InAppWebViewController _webViewController;

  var progress;
  bool _isLoading = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      backgroundColor: Colors.white,
      body: _buildContentBody(),
    );
  }

  //------------------------------------------------------------------------------------------------->>

  _buildContentBody() {
    return Container(
      child: Stack(
        children: [
          InAppWebView(
            initialUrlRequest:
            URLRequest(url: WebUri.uri(Uri.parse((widget.isSupportPage!)?"https://laundrybasketuae.com/pages/about_us":'https://laundrybasketuae.com/pages/page_privacy'))),
            onWebViewCreated: (InAppWebViewController controller) {
              print("iscreated");
              _webViewController = controller;
            },
            onLoadStart: (controller, url) async {
              _webViewController
                  .evaluateJavascript(source:"javascript:(function() { var head = document.getElementsByTagName('header')[0];head.parentNode.removeChild(head);var footer = document.getElementsByTagName('footer')[0];footer.parentNode.removeChild(footer);})()", )
                  .then((value) => debugPrint('Page finished loading Javascript'))
                  .catchError((onError) => debugPrint(' error $onError'));
              setState(() {
                _isLoading = true;
              });
              print("onLoadStart :: $url");
            },
            onLoadStop: (controller, url) {

              _webViewController
                  .evaluateJavascript(source:"javascript:(function() { var head = document.getElementsByTagName('header')[0];head.parentNode.removeChild(head);var footer = document.getElementsByTagName('footer')[0];footer.parentNode.removeChild(footer);})()", )
                  .then((value) => debugPrint('Page finished loading Javascript'))
                  .catchError((onError) => debugPrint(' error $onError'));
              setState(() {
                _isLoading = false;
              });
              print("Payment#Url: " + url.toString());
            },
            onProgressChanged:
                (InAppWebViewController controller, int progress) {
              setState(() {
                this.progress = progress / 100;
              });
              //loadData();
            },
          ),
          if (_isLoading)
            Center(
              child: CircularProgressIndicator(),
            ),
        ],
      ),
    );
  }

  //------------------------------------------------------------------------------------------------->>

  _buildAppBar() {
    return PreferredSize(
      preferredSize: Size(double.infinity, 80),
      child: CusturmAppbar(
          child: ListTile(
            leading: backarrow(
              context: context,
              onTap: () {
                Navigator.pop(context);
              },
            ),
            title: Center(
              child: Text((widget.isSupportPage!)? 'About Us':Apptext.privacyPolicy, style: AppTextStyle().appbathead),
            ),
          )),
    );
  }

//------------------------------------------------------------------------------------------------->>
}
