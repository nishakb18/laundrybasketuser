import 'package:app24_user_app/Laundry-app/views/widgets/custum_appbar.dart';
import 'package:app24_user_app/constants/app_text.dart';
import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/constants/textstyle.dart';
import 'package:app24_user_app/helpers.dart';
import 'package:app24_user_app/modules/my_account/address/manage_address.dart';
import 'package:app24_user_app/modules/my_account/profile/profile.dart';
import 'package:app24_user_app/modules/my_account/support.dart';
import 'package:app24_user_app/modules/my_account/wallet/wallet_screen.dart';
import 'package:app24_user_app/modules/tabs/account/privacy_policy.dart';
import 'package:app24_user_app/widgets/bottomsheets/logout_bottomsheet.dart';
import 'package:app24_user_app/widgets/offline_builder_widget.dart';
import 'package:flutter/material.dart';

import 'laundry_basket_privacy_policy.dart';

class MyAccountTabPage extends StatefulWidget {
  const MyAccountTabPage({super.key, this.isLaundryApp = false});
final bool? isLaundryApp;
  @override
  State<MyAccountTabPage> createState() => _MyAccountTabPageState();
}

class _MyAccountTabPageState extends State<MyAccountTabPage> {
  final List<MyAccountList> myAccountList = [];
  String website = "app24.co.in/pages/page_privacy";

  @override
  void initState() {
    super.initState();

    myAccountList.add( MyAccountList(
      title: "Edit Profile",
      icon: App24UserAppImages.accountIcon,
      id: "1",
      next: App24UserAppImages.roundRArrow,
    ));
    myAccountList.add( MyAccountList(
      title: "Manage Address",
      icon: App24UserAppImages.locationIcon,
      id: "2",
      next: App24UserAppImages.roundRArrow,
    ));
    // _my_account_list.add(new MyAccountList(
    //     title: "Payment",
    //     icon: App24UserAppImages.bSheetDebitCard,
    //     id: "3",
    //     next: App24UserAppImages.roundRArrow,
    // ));
    myAccountList.add( MyAccountList(
      title: "Wallet",
      icon: App24UserAppImages.wallet,
      id: "4",
      next: App24UserAppImages.roundRArrow,
    ));
    // _my_account_list.add(new MyAccountList(
    //     title: "Language",
    //     // icon: App24User.privacy_policy,
    //     next: App24UserAppImages.roundRArrow,
    //     icon: App24UserAppImages.languageIcon,
    //     id: "5"));
/*    _my_account_list.add(new MyAccountList(
        title: "Referrals",
        // icon: App24User.support,
        next: App24UserAppImages.roundRArrow,
        icon: App24UserAppImages.referralsIcon,
        id: "8"));*/
    // if (Platform.isAndroid) WebView.platform = AndroidWebView();
  }

  // Future<void> _openInBrowser(String url) async {
  //   if (await canLaunch(url)) {
  //     await launch(
  //       url,
  //       forceSafariVC: false,
  //       forceWebView: false,
  //     );
  //   } else {
  //     throw 'Could not launch $url';
  //   }
  // }



  void onTilesPressed(id) {
    if (id == "1") {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => ProfilePage(isLaundryApp: widget.isLaundryApp,)),
      );
    } else if (id == "2") {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => ManageAddressPage(isLaundryApp: widget.isLaundryApp,)),
      );
    } else if (id == "6") {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) {
          if(!widget.isLaundryApp!) {
            return SupportPage(isLaundryApp: widget.isLaundryApp,);
          } else {
            return LaundryPrivacyPolicyScreen(isSupportPage: widget.isLaundryApp,);
          }

        }),
      );
    } else if (id == "4") {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => WalletPage(isLaundryApp: widget.isLaundryApp,)),
      );
    }
    // else if (id == "5") {
    //   Navigator.push(
    //     context,
    //     MaterialPageRoute(builder: (context) => LanguagePage()),
    //   );
    // }

    // Referral Page
    // else if (id == "8") {
    //   Navigator.push(
    //     context,
    //     MaterialPageRoute(builder: (context) => ReferralPage()),
    //   );
    // }
  }

  Future<bool> onWillPop() async {
    // return (await     Helpers().getCommonBottomSheet(
    //     context: context,
    //     content: ExitAppBottomSheet(
    //     ),
    //     title: "Come back soon!")
    //     // showDialog(
    //     //     context: context,
    //     //     builder: (BuildContext context) => exitDialogWidget(context))
    // ) ??
    //     false;
    Navigator.pushNamedAndRemoveUntil(context, '/homeScreen', (Route<dynamic> route) => false);
    return false;
    // return Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage()));
  }


  onLogoutPressed() {
    Helpers().getCommonBottomSheet(
        context: context,
        content: const LogoutBottomSheet(),
        title: "Come back soon!");
    // SharedPreferences prefs = await SharedPreferences.getInstance();
    // prefs.clear();
    // RestartWidget.restartApp(context);
    // Navigator.push(context, MaterialPageRoute(builder: (context) => LogInPage()));
  }

  @override
  Widget build(BuildContext context) {
    TextStyle tileTextStyle = const TextStyle(
        color: App24Colors.darkTextColor,
        fontWeight: FontWeight.w600,
        fontSize: 16);
    return WillPopScope(
      onWillPop: onWillPop,
      child: Scaffold(
          appBar: (widget.isLaundryApp == true)?
          CusturmAppbar(
            child: ListTile(
              title: Center(
                child: Text(Apptext.myAccount, style: AppTextStyle().appbathead),
              ),
            ),
          )
              :AppBar(
            automaticallyImplyLeading: false,
            title: const Center(
                child: Text(
              "My Account",
              style: TextStyle(color: App24Colors.darkTextColor),
            )),
            backgroundColor: Colors.white,
            elevation: 1,
          ),
          backgroundColor: Colors.white,
          body: SafeArea(
            child: OfflineBuilderWidget(
               Container(
                 color: Colors.transparent,
                  child: Column(children: <Widget>[
                const SizedBox(
                  height: 10,
                ),
                Expanded(
                  child: ListView.separated(
                    padding: const EdgeInsets.all(0),
                    itemCount: myAccountList.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return Material(
                          color: Colors.transparent,
                          child: ListTile(
                            onTap: () {
                              onTilesPressed(myAccountList[index].id);
                            },
                            contentPadding:
                                const EdgeInsets.symmetric(vertical: 6, horizontal: 20),
                            leading: Container(
                              color: Colors.transparent,
                                width: 25,
                                height: 25,
                                child: Image.asset(
                                  myAccountList[index].icon!,
                                )),
                            title: Text(myAccountList[index].title!,
                                style: tileTextStyle),
                            trailing: Image.asset(
                              myAccountList[index].next!,
                              width: 20,
                            ),
                          ));
                    },
                    separatorBuilder: (context, index) {
                      return const Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: Divider(
                            height: 1,
                          ));
                    },
                  ),
                ),
                const Divider(),
                ListTile(
                  title: Text(
                    "Help and Support",
                    style: tileTextStyle,
                  ),
                  onTap: () {
                    // Navigator.push(context, MaterialPageRoute(builder: (context) => SelectLocationPage()));
                    // Helpers().getCommonBottomSheet(context: context, content: OrderSuccess());
                    // Navigator.push(context, MaterialPageRoute(builder: (context) => Cart()));
                    onTilesPressed("6");
                  },
                ),
                const Divider(),
                ListTile(
                  title: Text(
                    "Privacy Policy",
                    style: tileTextStyle.copyWith(
                        color: const Color(0xff0359BB),
                        decoration: TextDecoration.underline),
                  ),
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) {
                       if(!widget.isLaundryApp!) {
                         return const PrivacyPolicy(isLaundryApp: true,);
                       } else {
                         return const LaundryPrivacyPolicyScreen();
                       }
                    }));
                    // Navigator.push(
                    //     context,
                    //     MaterialPageRoute(
                    //         builder: (context) => WebView(
                    //           initialUrl: "https://$website",
                    //         )));
                    // _openInBrowser("https://$website");
                    // onTilesPressed("7");
                    //Navigator.push(context, MaterialPageRoute(builder: (context) => TrackOrder()));
                  },
                ),
                const Divider(),
                TextButton.icon(
                  onPressed: onLogoutPressed,
                  icon: const Icon(Icons.logout, color: Color(0xffFF5D44)),
                  label: Text(
                    "Logout",
                    style: tileTextStyle.copyWith(color: const Color(0xffFF5D44)),
                  ),
                ),
                // const Divider(),
                const SizedBox(
                  height: 10,
                )
              ])),
            ),
          ) // This trailing comma makes auto-formatting nicer for build methods.
          ),
    );
  }
}

class MyAccountList {
  MyAccountList({this.icon, this.title, this.id, this.next});

  String? icon;
  String? title;
  String? id;
  String? next;
}
