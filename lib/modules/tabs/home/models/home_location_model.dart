HomeLocationModel homeLocationModelFromJson(str) {
  return HomeLocationModel.fromJson(str);
}

class HomeLocationModel {
  String? statusCode;
  String? title;
  String? message;
  List<ResponseData>? responseData;
  List<dynamic>? error;

  HomeLocationModel(
      {this.statusCode,
      this.title,
      this.message,
      this.responseData,
      this.error});

  HomeLocationModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    if (json['responseData'] != null) {
      responseData = [];
      json['responseData'].forEach((v) {
        responseData!.add(v);
      });
    }
    if (json['error'] != null) {
      error = [];
      json['error'].forEach((v) {
        error!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['title'] = this.title;
    data['message'] = this.message;
    if (this.responseData != null) {
      data['responseData'] = this.responseData!.map((v) => v.toJson()).toList();
    }
    if (this.error != null) {
      data['error'] = this.error!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ResponseData {
  int? id;
  String? countryName;
  String? countryCode;
  String? countryPhonecode;
  String? countryCurrency;
  String? countrySymbol;
  String? status;
  Null timezone;

  ResponseData(
      {this.id,
      this.countryName,
      this.countryCode,
      this.countryPhonecode,
      this.countryCurrency,
      this.countrySymbol,
      this.status,
      this.timezone});

  ResponseData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    countryName = json['country_name'];
    countryCode = json['country_code'];
    countryPhonecode = json['country_phonecode'];
    countryCurrency = json['country_currency'];
    countrySymbol = json['country_symbol'];
    status = json['status'];
    timezone = json['timezone'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['country_name'] = this.countryName;
    data['country_code'] = this.countryCode;
    data['country_phonecode'] = this.countryPhonecode;
    data['country_currency'] = this.countryCurrency;
    data['country_symbol'] = this.countrySymbol;
    data['status'] = this.status;
    data['timezone'] = this.timezone;
    return data;
  }
}
