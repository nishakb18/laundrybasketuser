HomeScreenTileModel homeScreenTileModelFromJson(str) {
  return HomeScreenTileModel.fromJson(str);
}


class HomeScreenTileModel {
  String? statusCode;
  String? title;
  String? message;
  HomeScreenTileModelResponseData? responseData;
  List<dynamic>? error;

  HomeScreenTileModel(
      {this.statusCode,
        this.title,
        this.message,
        this.responseData,
        this.error});

  HomeScreenTileModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    responseData = json['responseData'] != null
        ? new HomeScreenTileModelResponseData.fromJson(json['responseData'])
        : null;
    if (json['error'] != null) {
      error = [];
      json['error'].forEach((v) {
        error?.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['title'] = this.title;
    data['message'] = this.message;
    if (this.responseData != null) {
      data['responseData'] = this.responseData?.toJson();
    }
    if (this.error != null) {
      data['error'] = this.error?.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class HomeScreenTileModelResponseData {
  List<Services>? services;
  List<Promocodes>? promocodes;

  HomeScreenTileModelResponseData({this.services, this.promocodes});

  HomeScreenTileModelResponseData.fromJson(Map<String, dynamic> json) {
    if (json['services'] != null) {
      services = [];
      json['services'].forEach((v) {
        services?.add(new Services.fromJson(v));
      });
    }
    if (json['promocodes'] != null) {
      promocodes = [];
      json['promocodes'].forEach((v) {
        promocodes?.add(new Promocodes.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.services != null) {
      data['services'] = this.services?.map((v) => v.toJson()).toList();
    }
    if (this.promocodes != null) {
      data['promocodes'] = this.promocodes?.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Services {
  int? id;
  String? bgColor;
  String? icon;
  int? isFeatured;
  int? comingSoon;
  String? featuredImage;
  String? title;
  String? adminService;
  int? menuTypeId;
  int? sortOrder;
  int? status;
  List<Submenus>? submenus;
  Service? service;

  Services(
      {this.id,
        this.bgColor,
        this.icon,
        this.isFeatured,
        this.comingSoon,
        this.featuredImage,
        this.title,
        this.adminService,
        this.menuTypeId,
        this.sortOrder,
        this.status,
        this.submenus,
        this.service});

  Services.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    bgColor = json['bg_color'];
    icon = json['icon'];
    isFeatured = json['is_featured'];
    comingSoon = json['coming_soon'];
    featuredImage = json['featured_image'];
    title = json['title'];
    adminService = json['admin_service'];
    menuTypeId = json['menu_type_id'];
    sortOrder = json['sort_order'];
    status = json['status'];
    if (json['submenus'] != null) {
      submenus =[];
      json['submenus'].forEach((v) {
        submenus?.add(new Submenus.fromJson(v));
      });
    }
    service =
    json['service'] != null ? new Service.fromJson(json['service']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['bg_color'] = this.bgColor;
    data['icon'] = this.icon;
    data['is_featured'] = this.isFeatured;
    data['coming_soon'] = this.comingSoon;
    data['featured_image'] = this.featuredImage;
    data['title'] = this.title;
    data['admin_service'] = this.adminService;
    data['menu_type_id'] = this.menuTypeId;
    data['sort_order'] = this.sortOrder;
    data['status'] = this.status;
    if (this.submenus != null) {
      data['submenus'] = this.submenus?.map((v) => v.toJson()).toList();
    }
    if (this.service != null) {
      data['service'] = this.service?.toJson();
    }
    return data;
  }
}

class Submenus {
  int? id;
  String? bgColor;
  String? icon;
  int? isFeatured;
  int? comingSoon;
  String? featuredImage;
  String? title;
  int? shopType;
  int? shopTypeId;
  String? adminService;
  int? menuTypeId;
  int? sortOrder;
  int? status;

  Submenus(
      {this.id,
        this.bgColor,
        this.icon,
        this.isFeatured,
        this.comingSoon,
        this.featuredImage,
        this.title,
        this.shopType,
        this.shopTypeId,
        this.adminService,
        this.menuTypeId,
        this.sortOrder,
        this.status});

  Submenus.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    bgColor = json['bg_color'];
    icon = json['icon'];
    isFeatured = json['is_featured'];
    comingSoon = json['coming_soon'];
    featuredImage = json['featured_image'];
    title = json['title'];
    shopType = json['shop_type'];
    shopTypeId = json['shop_type_id'];
    adminService = json['admin_service'];
    menuTypeId = json['menu_type_id'];
    sortOrder = json['sort_order'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['bg_color'] = this.bgColor;
    data['icon'] = this.icon;
    data['is_featured'] = this.isFeatured;
    data['coming_soon'] = this.comingSoon;
    data['featured_image'] = this.featuredImage;
    data['title'] = this.title;
    data['shop_type'] = this.shopType;
    data['shop_type_id'] = this.shopTypeId;
    data['admin_service'] = this.adminService;
    data['menu_type_id'] = this.menuTypeId;
    data['sort_order'] = this.sortOrder;
    data['status'] = this.status;
    return data;
  }
}

class Service {
  int? id;
  String? adminService;
  String? displayName;
  String? baseUrl;
  int? status;

  Service(
      {this.id,
        this.adminService,
        this.displayName,
        this.baseUrl,
        this.status});

  Service.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    adminService = json['admin_service'];
    displayName = json['display_name'];
    baseUrl = json['base_url'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['admin_service'] = this.adminService;
    data['display_name'] = this.displayName;
    data['base_url'] = this.baseUrl;
    data['status'] = this.status;
    return data;
  }
}

class Promocodes {
  int? id;
  String? promoCode;
  dynamic lat;
  dynamic lng;
  int? rad;
  String? placeText;
  String? placeid;
  String? service;
  String? picture;
  int? percentage;
  int? maxAmount;
  int? minimumPurchase;
  String? promoDescription;
  String? startDate;
  String? expiration;
  int? timeApply;
  int? totalCount;
  int? userCount;
  String? storeMultiTypeId;
  int? allShops;
  String? selectedShops;
  int? shopPercentage;
  String? status;
  dynamic menuTypeId;
  int? shopId;
  String? title;
  Service? services;

  Promocodes(
      {this.id,
        this.promoCode,
        this.lat,
        this.lng,
        this.rad,
        this.placeText,
        this.placeid,
        this.service,
        this.picture,
        this.percentage,
        this.maxAmount,
        this.minimumPurchase,
        this.promoDescription,
        this.startDate,
        this.expiration,
        this.timeApply,
        this.totalCount,
        this.userCount,
        this.storeMultiTypeId,
        this.allShops,
        this.selectedShops,
        this.shopPercentage,
        this.status,
        this.menuTypeId,
        this.shopId,
        this.title,
        this.services});

  Promocodes.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    promoCode = json['promo_code'];
    lat = json['lat'];
    lng = json['lng'];
    rad = json['rad'];
    placeText = json['place_text'];
    placeid = json['placeid'];
    service = json['service'];
    picture = json['picture'];
    percentage = json['percentage'];
    maxAmount = json['max_amount'];
    minimumPurchase = json['minimum_purchase'];
    promoDescription = json['promo_description'];
    startDate = json['start_date'];
    expiration = json['expiration'];
    timeApply = json['time_apply'];
    totalCount = json['total_count'];
    userCount = json['user_count'];
    storeMultiTypeId = json['store_multi_type_id'];
    allShops = json['all_shops'];
    selectedShops = json['selected_shops'];
    shopPercentage = json['shop_percentage'];
    status = json['status'];
    menuTypeId = json['menu_type_id'];
    shopId = json['shop_id'];
    title = json['title'];
    services = json['services'] != null
        ? new Service.fromJson(json['services'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['promo_code'] = this.promoCode;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['rad'] = this.rad;
    data['place_text'] = this.placeText;
    data['placeid'] = this.placeid;
    data['service'] = this.service;
    data['picture'] = this.picture;
    data['percentage'] = this.percentage;
    data['max_amount'] = this.maxAmount;
    data['minimum_purchase'] = this.minimumPurchase;
    data['promo_description'] = this.promoDescription;
    data['start_date'] = this.startDate;
    data['expiration'] = this.expiration;
    data['time_apply'] = this.timeApply;
    data['total_count'] = this.totalCount;
    data['user_count'] = this.userCount;
    data['store_multi_type_id'] = this.storeMultiTypeId;
    data['all_shops'] = this.allShops;
    data['selected_shops'] = this.selectedShops;
    data['shop_percentage'] = this.shopPercentage;
    data['status'] = this.status;
    data['menu_type_id'] = this.menuTypeId;
    data['shop_id'] = this.shopId;
    data['title'] = this.title;
    if (this.services != null) {
      data['services'] = this.services?.toJson();
    }
    return data;
  }
}


// class HomeScreenTileModel {
//   String? statusCode;
//   String? title;
//   String? message;
//   HomeScreenTileModelResponseData? responseData;
//   List<dynamic>? error;
//
//   HomeScreenTileModel(
//       {this.statusCode,
//       this.title,
//       this.message,
//       this.responseData,
//       this.error});
//
//   HomeScreenTileModel.fromJson(Map<String, dynamic> json) {
//     statusCode = json['statusCode'];
//     title = json['title'];
//     message = json['message'];
//     responseData = json['responseData'] != null
//         ? new HomeScreenTileModelResponseData.fromJson(json['responseData'])
//         : null;
//     if (json['error'] != null) {
//       error = [];
//       json['error'].forEach((v) {
//         error!.add(v);
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['statusCode'] = this.statusCode;
//     data['title'] = this.title;
//     data['message'] = this.message;
//     if (this.responseData != null) {
//       data['responseData'] = this.responseData!.toJson();
//     }
//     if (this.error != null) {
//       data['error'] = this.error!.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }

// class HomeScreenTileModelResponseData {
//   List<Services>? services;
//   List<Promocodes>? promocodes;
//
//   HomeScreenTileModelResponseData({this.services, this.promocodes});
//
//   HomeScreenTileModelResponseData.fromJson(Map<String, dynamic> json) {
//     if (json['services'] != null) {
//       services = [];
//       json['services'].forEach((v) {
//         services!.add(new Services.fromJson(v));
//       });
//     }
//     if (json['promocodes'] != null) {
//       promocodes = [];
//       json['promocodes'].forEach((v) {
//         promocodes!.add(new Promocodes.fromJson(v));
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     if (this.services != null) {
//       data['services'] = this.services!.map((v) => v.toJson()).toList();
//     }
//     if (this.promocodes != null) {
//       data['promocodes'] = this.promocodes!.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }
//
// class Services {
//   int? id;
//   String? bgColor;
//   String? icon;
//   int? isFeatured;
//   int? comingSoon;
//   String? featuredImage;
//   String? title;
//   String? adminService;
//   int? menuTypeId;
//   int? sortOrder;
//   int? status;
//   List<Submenus>? submenus;
//   Service? service;
//
//   Services(
//       {this.id,
//       this.bgColor,
//       this.icon,
//       this.isFeatured,
//       this.comingSoon,
//       this.featuredImage,
//       this.title,
//       this.adminService,
//       this.menuTypeId,
//       this.sortOrder,
//       this.status,
//       this.submenus,
//       this.service});
//
//   Services.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     bgColor = json['bg_color'];
//     icon = json['icon'];
//     isFeatured = json['is_featured'];
//     comingSoon = json['coming_soon'];
//     featuredImage = json['featured_image'];
//     title = json['title'];
//     adminService = json['admin_service'];
//     menuTypeId = json['menu_type_id'];
//     sortOrder = json['sort_order'];
//     status = json['status'];
//     if (json['submenus'] != null) {
//       submenus = [];
//       json['submenus'].forEach((v) {
//         submenus!.add(new Submenus.fromJson(v));
//       });
//     }
//     service =
//         json['service'] != null ? new Service.fromJson(json['service']) : null;
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['bg_color'] = this.bgColor;
//     data['icon'] = this.icon;
//     data['is_featured'] = this.isFeatured;
//     data['coming_soon'] = this.comingSoon;
//     data['featured_image'] = this.featuredImage;
//     data['title'] = this.title;
//     data['admin_service'] = this.adminService;
//     data['menu_type_id'] = this.menuTypeId;
//     data['sort_order'] = this.sortOrder;
//     data['status'] = this.status;
//     if (this.submenus != null) {
//       data['submenus'] = this.submenus!.map((v) => v.toJson()).toList();
//     }
//     if (this.service != null) {
//       data['service'] = this.service!.toJson();
//     }
//     return data;
//   }
// }
//
// class Submenus {
//   int? id;
//   String? bgColor;
//   String? icon;
//   int? isFeatured;
//   int? comingSoon;
//   String? featuredImage;
//   String? title;
//   int? shopType;
//   int? shopTypeId;
//   String? adminService;
//   int? menuTypeId;
//   int? sortOrder;
//   int? status;
//
//   Submenus(
//       {this.id,
//       this.bgColor,
//       this.icon,
//       this.isFeatured,
//       this.comingSoon,
//       this.featuredImage,
//       this.title,
//       this.shopType,
//       this.shopTypeId,
//       this.adminService,
//       this.menuTypeId,
//       this.sortOrder,
//       this.status});
//
//   Submenus.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     bgColor = json['bg_color'];
//     icon = json['icon'];
//     isFeatured = json['is_featured'];
//     comingSoon = json['coming_soon'];
//     featuredImage = json['featured_image'];
//     title = json['title'];
//     shopType = json['shop_type'];
//     shopTypeId = json['shop_type_id'];
//     adminService = json['admin_service'];
//     menuTypeId = json['menu_type_id'];
//     sortOrder = json['sort_order'];
//     status = json['status'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['bg_color'] = this.bgColor;
//     data['icon'] = this.icon;
//     data['is_featured'] = this.isFeatured;
//     data['coming_soon'] = this.comingSoon;
//     data['featured_image'] = this.featuredImage;
//     data['title'] = this.title;
//     data['shop_type'] = this.shopType;
//     data['shop_type_id'] = this.shopTypeId;
//     data['admin_service'] = this.adminService;
//     data['menu_type_id'] = this.menuTypeId;
//     data['sort_order'] = this.sortOrder;
//     data['status'] = this.status;
//     return data;
//   }
// }
//
// class Service {
//   int? id;
//   String? adminService;
//   String? displayName;
//   String? baseUrl;
//   int? status;
//
//   Service(
//       {this.id,
//       this.adminService,
//       this.displayName,
//       this.baseUrl,
//       this.status});
//
//   Service.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     adminService = json['admin_service'];
//     displayName = json['display_name'];
//     baseUrl = json['base_url'];
//     status = json['status'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['admin_service'] = this.adminService;
//     data['display_name'] = this.displayName;
//     data['base_url'] = this.baseUrl;
//     data['status'] = this.status;
//     return data;
//   }
// }
//
// class Promocodes {
//   int? id;
//   String? promoCode;
//   String? service;
//   String? picture;
//   int? percentage;
//   int? maxAmount;
//   int? minimumPurchase;
//   String? promoDescription;
//   String? startDate;
//   String? expiration;
//   int? timeApply;
//   int? totalCount;
//   int? userCount;
//   String? storeMultiTypeId;
//   int? allShops;
//   String? selectedShops;
//   int? shopPercentage;
//   String? status;
//   String? menuTypeId;
//   int? shopId;
//   String? title;
//   Service? services;
//
//   Promocodes(
//       {this.id,
//       this.promoCode,
//       this.service,
//       this.picture,
//       this.percentage,
//       this.maxAmount,
//       this.minimumPurchase,
//       this.promoDescription,
//       this.startDate,
//       this.expiration,
//       this.timeApply,
//       this.totalCount,
//       this.userCount,
//       this.storeMultiTypeId,
//       this.allShops,
//       this.selectedShops,
//       this.shopPercentage,
//       this.status,
//       this.menuTypeId,
//       this.shopId,
//       this.title,
//       this.services});
//
//   Promocodes.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     promoCode = json['promo_code'];
//     service = json['service'];
//     picture = json['picture'];
//     percentage = json['percentage'];
//     maxAmount = json['max_amount'];
//     minimumPurchase = json['minimum_purchase'];
//     promoDescription = json['promo_description'];
//     startDate = json['start_date'];
//     expiration = json['expiration'];
//     timeApply = json['time_apply'];
//     totalCount = json['total_count'];
//     userCount = json['user_count'];
//     storeMultiTypeId = json['store_multi_type_id'];
//     allShops = json['all_shops'];
//     selectedShops = json['selected_shops'];
//     shopPercentage = json['shop_percentage'];
//     status = json['status'];
//     menuTypeId = json['menu_type_id'];
//     shopId = json['shop_id'];
//     title = json['title'];
//     services = json['services'] != null
//         ? new Service.fromJson(json['services'])
//         : null;
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['promo_code'] = this.promoCode;
//     data['service'] = this.service;
//     data['picture'] = this.picture;
//     data['percentage'] = this.percentage;
//     data['max_amount'] = this.maxAmount;
//     data['minimum_purchase'] = this.minimumPurchase;
//     data['promo_description'] = this.promoDescription;
//     data['start_date'] = this.startDate;
//     data['expiration'] = this.expiration;
//     data['time_apply'] = this.timeApply;
//     data['total_count'] = this.totalCount;
//     data['user_count'] = this.userCount;
//     data['store_multi_type_id'] = this.storeMultiTypeId;
//     data['all_shops'] = this.allShops;
//     data['selected_shops'] = this.selectedShops;
//     data['shop_percentage'] = this.shopPercentage;
//     data['status'] = this.status;
//     data['menu_type_id'] = this.menuTypeId;
//     data['shop_id'] = this.shopId;
//     data['title'] = this.title;
//     if (this.services != null) {
//       data['services'] = this.services!.toJson();
//     }
//     return data;
//   }
// }
