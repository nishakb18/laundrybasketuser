ServicesModel servicesModelFromJson(str) {
  return ServicesModel.fromJson(str);
}

class ServicesModel {
  String? statusCode;
  String? title;
  String? message;
  List<ServicesModelResponseData>? responseData;
  List<dynamic>? error;

  ServicesModel(
      {this.statusCode,
      this.title,
      this.message,
      this.responseData,
      this.error});

  ServicesModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    if (json['responseData'] != null) {
      responseData = [];
      json['responseData'].forEach((v) {
        responseData!.add(new ServicesModelResponseData.fromJson(v));
      });
    }
    if (json['error'] != null) {
      error = [];
      json['error'].forEach((v) {
        error!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['title'] = this.title;
    data['message'] = this.message;
    if (this.responseData != null) {
      data['responseData'] = this.responseData!.map((v) => v.toJson()).toList();
    }
    if (this.error != null) {
      data['error'] = this.error!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ServicesModelResponseData {
  int? id;
  int? serviceCategoryId;
  String? serviceSubcategoryName;
  String? picture;
  int? serviceSubcategoryOrder;
  int? serviceSubcategoryStatus;

  ServicesModelResponseData(
      {this.id,
      this.serviceCategoryId,
      this.serviceSubcategoryName,
      this.picture,
      this.serviceSubcategoryOrder,
      this.serviceSubcategoryStatus});

  ServicesModelResponseData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    serviceCategoryId = json['service_category_id'];
    serviceSubcategoryName = json['service_subcategory_name'];
    picture = json['picture'] ?? '';
    serviceSubcategoryOrder = json['service_subcategory_order'];
    serviceSubcategoryStatus = json['service_subcategory_status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['service_category_id'] = this.serviceCategoryId;
    data['service_subcategory_name'] = this.serviceSubcategoryName;
    data['picture'] = this.picture;
    data['service_subcategory_order'] = this.serviceSubcategoryOrder;
    data['service_subcategory_status'] = this.serviceSubcategoryStatus;
    return data;
  }
}
