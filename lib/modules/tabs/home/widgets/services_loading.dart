import 'dart:async';

import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ServicesLoading extends StatefulWidget {
  @override
  _ServicesLoadingState createState() => _ServicesLoadingState();
}

class _ServicesLoadingState extends State<ServicesLoading> {
  Timer? _timer;
  int _start = 10; // time to popup still working window /// in seconds
  List<Widget> list = [];

  @override
  void initState() {
    startTimer();
    super.initState();
  }

  @override
  void dispose() {
    if (_timer != null) {
      _timer!.cancel();
    }
    super.dispose();
  }

  void startTimer() {
    if (_timer != null) {
      _timer!.cancel();
      _timer = null;
    } else {
      _timer = new Timer.periodic(const Duration(seconds: 1), (Timer timer) {
        setState(() {
          if (_start < 1) {
            _timer!.cancel();
          } else {
            _start = _start - 1;
          }
        });
      });
    }
  }

  List<Widget> buildServiceShimmerItem(context) {
    List<Widget> list = [];
    for (var i = 0; i < 6; i++)
      list.add(Container(
        width: MediaQuery.of(context).size.width / 2.5,
        height: MediaQuery.of(context).size.height / 4.5,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(25),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Color(0xffefefef),
                blurRadius: 20,
                spreadRadius: 3,
              ),
            ]),
      ));
    return list;
  }

  buildStillLoadingWidget() {
    return ListTile(
      title: Text("Please wait..."),
      subtitle: Text(
          "The network seems to be too busy. We suggest you to wait for some more time."),
      leading: CircularProgressIndicator.adaptive(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _start == 0 ? buildStillLoadingWidget() : Container(),
        Shimmer.fromColors(
            baseColor: Colors.grey[300]!,
            highlightColor: Colors.grey[100]!,
            child: Wrap(
                alignment: WrapAlignment.center,
                spacing: 20,
                runSpacing: 20,
                children: buildServiceShimmerItem(context))),
      ],
    );
  }
}
