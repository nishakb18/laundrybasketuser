import 'dart:async';

import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class OffersLoading extends StatefulWidget {
  @override
  _OffersLoadingState createState() => _OffersLoadingState();
}

class _OffersLoadingState extends State<OffersLoading> {
  Timer? _timer;
  int _start = 10; // time to popup still working window /// in seconds
  List<Widget> list = [];

  @override
  void initState() {
    startTimer();
    super.initState();
  }

  @override
  void dispose() {
    if (_timer != null) {
      _timer!.cancel();
    }
    super.dispose();
  }

  void startTimer() {
    if (_timer != null) {
      _timer!.cancel();
      _timer = null;
    } else {
      _timer = new Timer.periodic(const Duration(seconds: 1), (Timer timer) {
        setState(() {
          if (_start < 1) {
            _timer!.cancel();
          } else {
            _start = _start - 1;
          }
        });
      });
    }
  }

  buildStillLoadingWidget() {
    return ListTile(
      title: Text("Please wait..."),
      subtitle: Text(
          "The network seems to be too busy. We suggest you to wait for some more time."),
      leading: CircularProgressIndicator.adaptive(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _start == 0 ? buildStillLoadingWidget() : Container(),
        Shimmer.fromColors(
          baseColor: Colors.grey[300]!,
          highlightColor: Colors.grey[100]!,
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 40),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30),
              color: Colors.white,
            ),
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height / 5,
          ),
        ),
      ],
    );
  }
}
