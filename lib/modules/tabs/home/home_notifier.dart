import 'package:app24_user_app/config/routes.dart';
import 'package:app24_user_app/constants/shared_preferences_path.dart';
import 'package:app24_user_app/models/app_configuration_model.dart';
import 'package:app24_user_app/models/location_model.dart';
import 'package:app24_user_app/modules/my_account/profile/profile_model.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/api/api_response_state2.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeNotifier extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  HomeNotifier(this._apiRepository) : super(ResponseState2(isLoading: true));

  // @override
  // loadData(){
  //   return getCurrentLocation();
  // }
  // void initState(){
  //   super.initState();
  // }

  AppConfigurationModel? appConfiguration;
  ProfileModel? profile;
  String? mapKey;
  String? razorPayKey;
  late bool _serviceEnabled;
  final Location location = new Location();
  PermissionStatus? _permissionGranted;
  LocationModel? selectedLocation;
  GoogleMapController? mapController;

  // LocationModel? abc;

  // void initState() {
  //   _getCurrentLocation();
  // }

  // // @override
  // void initState() {
  //   _getCurrentLocation(););
  //   super.initState();
  // }

  LocationModel currentLocation = LocationModel(
      latitude: 0.0,
      longitude: 0.0,
      secondary_text: "Loading..",
      main_text: "Loading..",
      description: "Loading..");

  Future<void> getCurrentLocation() async {
    print("location from home notifier");
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }

    await location.getLocation().then((position) async {
      LocationData _currentLocation = position;

      selectedLocation = LocationModel(
          latitude: _currentLocation.latitude,
          longitude: _currentLocation.longitude);
      // setLocation(selectedLocation!.latitude, selectedLocation!.longitude);

      getAddress(selectedLocation!.latitude!, selectedLocation!.longitude!);
    });
  }
  // Future<void> setLocation(lat, long) async {
  //   setState(() {
  //     // mapController = LatLng(lat, long) as MapController?;
  //     // mapController!.move(LatLng(lat, long), 3.0);
  //     mapController!.animateCamera(
  //       CameraUpdate.newCameraPosition(
  //         CameraPosition(
  //           target: LatLng(lat, long),
  //           zoom: 14.0,
  //         ),
  //       ),
  //     );
  //   });
  // }

  Future<void> getOffers({bool init = true}) async {
    try {
      if (init) state = state.copyWith(isLoading: true);
      final offer = await _apiRepository.fetchOffers();
      state = state.copyWith(response: offer, isLoading: false, isError: false);
    } catch (e) {
      state = state.copyWith(
          errorMessage: e.toString(), isLoading: false, isError: true);
    }
  }

  Future<void> getServices({
    bool init = true,
  }) async {
    try {
      if (init) state = state.copyWith(isLoading: true);
      final services = await _apiRepository.fetchServices();

      state =
          state.copyWith(response: services, isLoading: false, isError: false);
    } catch (e) {
      state = state.copyWith(
          errorMessage: e.toString(), isLoading: false, isError: true);
    }
  }

  Future<void> getAppConfigurations() async {
    // glblSrchLastUpdatedAt = appConfiguration!.responseData!.appsetting!.stortypeLast;

    var prefs = await SharedPreferences.getInstance();

    String storeTypeLastUpdated = '';
    String serviceSubCategoryLastUpdated = '';
    String rideVehicleLastUpdated = '';

    appConfiguration = await _apiRepository.fetchAppConfigurations();
    // location = _apiRepository.fetchLocation(url, from)
    mapKey = appConfiguration!.responseData!.appsetting!.androidKey;
    razorPayKey = appConfiguration!
        .responseData!.appsetting!.payments![1].credentials![1].value;
    showLog("configurations ====>>>>>>" +
        appConfiguration!.responseData!.appsetting!.androidKey!);

    storeTypeLastUpdated =
        prefs.getString(SharedPreferencesPath.storeTypeKeywordUpdatedKey) ?? '';
    serviceSubCategoryLastUpdated = prefs.getString(
            SharedPreferencesPath.serviceSubCategoryKeywordUpdatedKey) ??
        '';
    rideVehicleLastUpdated =
        prefs.getString(SharedPreferencesPath.rideVehicleKeywordUpdatedKey) ??
            '';

    if (storeTypeLastUpdated == '' ||
        storeTypeLastUpdated !=
            appConfiguration!.responseData!.appsetting!.stortypeLast!) {
      prefs.setString(SharedPreferencesPath.storeTypeKeywordUpdatedKey,
          appConfiguration!.responseData!.appsetting!.stortypeLast!);
      navigatorKey.currentContext!
          .read(globalSearchProvider.notifier)
          .isStoreTypeKeyWordUpdated = true;
    } else {
      navigatorKey.currentContext!
          .read(globalSearchProvider.notifier)
          .isStoreTypeKeyWordUpdated = false;
    }

    if (serviceSubCategoryLastUpdated == '' ||
        serviceSubCategoryLastUpdated !=
            appConfiguration!.responseData!.appsetting!.servsubcatLast!) {
      prefs.setString(SharedPreferencesPath.serviceSubCategoryKeywordUpdatedKey,
          appConfiguration!.responseData!.appsetting!.servsubcatLast!);
      navigatorKey.currentContext!
          .read(globalSearchProvider.notifier)
          .isServiceSubCategoryKeyWordUpdated = true;
    } else {
      navigatorKey.currentContext!
          .read(globalSearchProvider.notifier)
          .isServiceSubCategoryKeyWordUpdated = false;
    }

    if (rideVehicleLastUpdated == '' ||
        rideVehicleLastUpdated !=
            appConfiguration!.responseData!.appsetting!.ridedelvehLast!) {
      prefs.setString(SharedPreferencesPath.rideVehicleKeywordUpdatedKey,
          appConfiguration!.responseData!.appsetting!.ridedelvehLast!);
      navigatorKey.currentContext!
          .read(globalSearchProvider.notifier)
          .isRideVehicleKeyWordUpdated = true;
    } else {
      navigatorKey.currentContext!
          .read(globalSearchProvider.notifier)
          .isRideVehicleKeyWordUpdated = false;
    }
  }

  Future<void> getProfile() async {
    // var prefs =
    // await SharedPreferences
    //     .getInstance();
    // prefs.setInt(SharedPreferencesPath.userID, profile!.responseData!.id!.toInt());
    profile = await _apiRepository.fetchProfile();
    var prefs = await SharedPreferences.getInstance();
    prefs.setInt(
        SharedPreferencesPath.userID, profile!.responseData!.id!.toInt());
    if (profile!.responseData!.address != null &&
        profile!.responseData!.latitude != null &&
        profile!.responseData!.longitude != null) {
      currentLocation = LocationModel(
        latitude: profile?.responseData?.latitude,
        longitude: profile?.responseData?.longitude,
        main_text: profile?.responseData?.address,
      );
    }
  }
}
