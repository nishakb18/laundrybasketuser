import 'dart:io';

import 'package:another_flushbar/flushbar.dart';
import 'package:app24_user_app/app24_user_icons.dart';
import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/constants/global_constants.dart';
import 'package:app24_user_app/helpers.dart';
import 'package:app24_user_app/main.dart';
import 'package:app24_user_app/models/location_model.dart';
import 'package:app24_user_app/modules/delivery/order_essentials/order_essentials_page.dart';
import 'package:app24_user_app/modules/delivery/shop_list/shop_categories.dart';
import 'package:app24_user_app/modules/restaurant/restaurant_1st_page/order_food_page.dart';
import 'package:app24_user_app/modules/ride/ride_1st_page.dart';
import 'package:app24_user_app/modules/send_packages/send_package.dart';
import 'package:app24_user_app/modules/tabs/home/models/services_model.dart';
import 'package:app24_user_app/modules/tabs/home/widgets/offers_loading.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/widgets/bottomsheets/chat_bottomSheet.dart';
import 'package:app24_user_app/widgets/bottomsheets/exit_app_bottomsheet.dart';
import 'package:app24_user_app/widgets/bottomsheets/logout_bottomsheet.dart';
import 'package:app24_user_app/widgets/bottomsheets/promocodes/landing_page_promo_code/landing_page_promo_codes_model.dart';
import 'package:app24_user_app/widgets/bottomsheets/retry_cancel_bottomSheet.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:app24_user_app/widgets/offline_builder_widget.dart';
import 'package:app24_user_app/widgets/payment_waiting_screen.dart';
import 'package:app24_user_app/widgets/search_bar.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:upgrader/upgrader.dart';

import 'models/home_screen_tiles_model.dart';

class HomeTabPage extends StatefulWidget {
  const HomeTabPage({super.key});

  @override
  _HomeTabPageState createState() => _HomeTabPageState();
}

class _HomeTabPageState extends State<HomeTabPage>
    with AutomaticKeepAliveClientMixin<HomeTabPage> {
  late bool _serviceEnabled;
  final Location location = new Location();
  PermissionStatus? _permissionGranted;
  LocationModel? selectedLocation;
  GoogleMapController? mapController;
  String? currentLocationMainText;

  final List<HomeScreenTile> _homeScreenTileList = [
    HomeScreenTile(
        id: "2",
        image: App24UserAppImages.homeTileOrderEssentials,
        homeTileText: "Order Essentials"),
    HomeScreenTile(
        id: "4",
        image: App24UserAppImages.homeTilePickUp,
        homeTileText: "Delivery Services"),
    HomeScreenTile(
        id: "3",
        image: App24UserAppImages.homeTileFood,
        homeTileText: "Food Delivery"),
    HomeScreenTile(
        id: "1",
        image: App24UserAppImages.homeTileRide,
        homeTileText: "Ride a Cab")
  ];

  // final int _current = 0;
  int _current = 0;

  @override
  void initState() {
    SchedulerBinding.instance.addPostFrameCallback((_) {
      // if(context.read(homeNotifierProvider.notifier).currentLocation.main_text == "Loading..")
      //   AlertDialog(
      //     title: Text("No"),
      //   );
      // context.read(homeNotifierProvider.notifier).getCurrentLocation();
      //API call after screen build
      // context.read(homeNotifierProvider.notifier).getOffers();
      // context.read(homeLocationNotifierProvider.notifier).getHomeLocation(context: context);

      _getCurrentLocation();
      context
          .read(landingPagePromoCodeProvider.notifier)
          .getLandingPromoCodes(context: context);
      context.read(servicesNotifierProvider.notifier).getServices();
      // _getCurrentLocation();
    });
    super.initState();
  }

  final ApiRepository _apiRepository = ApiRepository();
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  // Future<Null> checkMobileNumber() async {
  //     var prefs = await SharedPreferences.getInstance();
  //
  //
  //     try {
  //       var result = await _apiRepository.fetchAppConfigurations();
  //       Navigator.pop(context);
  //       if (result.responseData != null) {
  //         // _socketService = SocketService.instance;
  //         SharedPreferences prefs = await SharedPreferences.getInstance();
  //         // prefs.setString(SharedPreferencesPath.glblSrchLclTimeStamp,)
  //         // prefs.setBool(SharedPreferencesPath.isGlobalSearchDone,true);
  //
  //             .read(orderDetailsNotifierProvider.notifier)
  //             .checkOngoingOrders();
  //         // _socketService.listenSocket();
  //         Navigator.of(context).pushReplacementNamed(homeScreen);
  //       } else {
  //         showMessage(context, result.message!);
  //       }
  //     } catch (e) {
  //       Navigator.pop(context);
  //       showMessage(context, e.toString());
  //       print(e);
  //     }
  //
  // }

  _getCurrentLocation() async {
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }

    await location.getLocation().then((position) async {
      LocationData _currentLocation = position;
      LocationModel? lm;
      selectedLocation = new LocationModel(
          latitude: _currentLocation.latitude,
          longitude: _currentLocation.longitude);

      //setLocation(selectedLocation!.latitude, selectedLocation!.longitude);

      lm = await getAddress(
          selectedLocation!.latitude!, selectedLocation!.longitude!);

      setState(() {
        context.read(homeNotifierProvider.notifier).currentLocation = lm!;
        context
            .read(landingPagePromoCodeProvider.notifier)
            .getLandingPromoCodes(context: context);
        // print("main text::: "+lm.main_text.toString());
        // loadData(context: context);
      });
    });
  }

  Future<void> setLocation(lat, long) async {
    setState(() {
      // mapController = LatLng(lat, long) as MapController?;
      // mapController!.move(LatLng(lat, long), 3.0);
      mapController!.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(
            target: LatLng(lat, long),
            zoom: 14.0,
          ),
        ),
      );
    });
  }

  // int _current = 0;
  reloadData({BuildContext? context, bool? init, String? type}) {
    if (type == "offers") {
      return context!
          .read(landingPagePromoCodeProvider.notifier)
          .getLandingPromoCodes(init: init!, context: context);
    } else if (type == "services") {
      return context!.read(servicesNotifierProvider.notifier).getServices(
            init: init!,
          );
    } else {
      context!
          .read(landingPagePromoCodeProvider.notifier)
          .getLandingPromoCodes(init: init!, context: context);
      return context.read(servicesNotifierProvider.notifier).getServices(
            init: init,
          );
    }
  }

  //Home Screen Tiles
  Widget getHomeScreenTiles(context) {
    List<Widget> _widgetList = [];

    for (var i = 0; i < _homeScreenTileList.length; i++) {
      _widgetList.add(Container(
          width: MediaQuery.of(context).size.width / 2.4,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
          ),
          child: Container(
            height: 150,
            width: 150,
            decoration: BoxDecoration(boxShadow: [
              BoxShadow(
                color: Colors.grey[200]!,
                blurRadius: 10,
                spreadRadius: 5,
              )
            ], borderRadius: BorderRadius.circular(25)),
            child: Stack(children: [
              Stack(children: [
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25),
                    color: Colors.white,
                  ),
                  height: 200,
                  width: 200,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 51),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(25),
                      child: Image.asset(
                        _homeScreenTileList[i].image!,
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ),
                Positioned(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Expanded(
                        child: Padding(
                      padding: const EdgeInsets.only(left: 15, top: 10),
                      child: Text(
                        _homeScreenTileList[i].homeTileText ?? "",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: MediaQuery.of(context).size.width / 27),
                      ),
                    )),
                    Padding(
                      padding: const EdgeInsets.only(right: 15, top: 10),
                      child: Image.asset(
                        App24UserAppImages.roundRArrow,
                        width: MediaQuery.of(context).size.width / 21,
                        height: MediaQuery.of(context).size.height / 21,
                      ),
                    ),
                  ],
                ))
              ]),
              Material(
                color: Colors.transparent,
                child: InkWell(
                  // splashColor: App24Colors.yellowRideThemeColor.withOpacity(0.2),
                  borderRadius: BorderRadius.circular(25),
                  onTap: () {
                    getSelectedHomeTile(_homeScreenTileList[i].id, context);
                  },
                ),
              )
            ]),
          )));
    }
    return Wrap(
      runSpacing: 15,
      spacing: 15,
      alignment: WrapAlignment.center,
      runAlignment: WrapAlignment.center,
      children: _widgetList,
    );
  }

  //Navigators of the Home tiles
  Future<void> getSelectedHomeTile(id, context) async {
    if (id == "1") {
      // showMessage(context, "Coming Soon!", false, false);
      /*showDialog(
           context: context,
           builder: (_) => AlertDialog(
             shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
                 titlePadding: EdgeInsets.all(0),
                 title: Container(
                   decoration: BoxDecoration(
                       color: App24Colors.yellowRideThemeColor,
                       borderRadius: BorderRadius.only(
                           topRight: Radius.circular(15),
                           topLeft: Radius.circular(15))),
                  child: Icon(CupertinoIcons.gift_fill),
                ),
               ));*/
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => RidePageOpen()),
      );
    } else if (id == "2") {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => ShopEssentials()),
      );
    } else if (id == "3") {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => OrderFoodPage()),
      );
    } else if (id == "4") {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => SendPackage()),
      );
    } else {
      // print(res['error']);
      /* Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => SignInPage()),
      );*/
    }
  }

  // Widget getTitle(title) {
  //   return Padding(
  //     padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
  //     child: Text(
  //       title,
  //       style: TextStyle(
  //           color: Colors.black87, fontSize: 22, fontWeight: FontWeight.bold),
  //     ),
  //   );
  // }

  // List<String> foodOfferCarousel = [
  //   App24UserAppImages.LandingCoupon1,
  //   App24UserAppImages.LandingCoupon2,
  //   App24UserAppImages.LandingCoupon1,
  //   App24UserAppImages.LandingCoupon2,
  // ];

  // List<Widget> getCarouselImage() {
  //   final List<Widget> imageSliders = foodOfferCarousel
  //       .map((item) => Container(
  //     decoration: BoxDecoration(
  //       borderRadius: BorderRadius.circular(20),
  //       // border: Border.all(
  //       //   color: Colors.grey[200],
  //       // )
  //     ),
  //     margin: EdgeInsets.all(15.0),
  //     child: ClipRRect(
  //       borderRadius: BorderRadius.all(Radius.circular(20.0)),
  //       child: Image.asset(
  //         item,
  //         fit: BoxFit.contain,
  //         width: MediaQuery.of(context).size.width,
  //       ),
  //     ),
  //   ))
  //       .toList();
  //
  //   return imageSliders;
  // }

  //Offer banners carousel
  List<Widget> getCarouselImages(
      context, List<LandingPagePromoResponseData> promoCodeList) {
    final List<Widget> imageSliders = promoCodeList
        .map((item) => Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Stack(children: [
                Container(
                  // padding: EdgeInsets.symmetric(horizontal: 10),
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(30),
                      child: FadeInImage.assetNetwork(
                        placeholder: 'assets/images/placeholder.png',
                        image: item.picture!,
                        fit: BoxFit.fill,
                        width: MediaQuery.of(context).size.width,
                      )),
                ),
                Material(
                  borderRadius: BorderRadius.circular(25),
                  color: Colors.transparent,
                  child: InkWell(
                      borderRadius: BorderRadius.circular(25),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ShopCategoryPage(
                                      promoCodeDetails:
                                          item.promoDescription.toString(),
                                      promoCode: item.promoCode,
                                      storeTypeID:
                                          item.storeMultiTypeId.toString(),
                                    )));
                      }),
                )
              ]),
            ))
        .toList();

    return imageSliders;
  }

  //More Services Tiles
  List<Widget> getServiceList(
      context, List<ServicesModelResponseData>? servicesModel) {
    List<Widget> _widgetServices = [];

    for (var i = 0; i < 6; i++) {
      _widgetServices.add(InkWell(
        borderRadius: BorderRadius.circular(25),
        onTap: () {},
        child: Container(
          width: MediaQuery.of(context).size.width / 2.5,
          // height: MediaQuery.of(context).size.height / 4.5,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(25),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Color(0xffefefef).withOpacity(0.8),
                  blurRadius: 10,
                  spreadRadius: 5,
                ),
              ]),
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Container(
              child: Column(children: [
                Container(
                    child: FadeInImage.assetNetwork(
                  placeholder: App24UserAppImages.placeHolderImage,
                  image:
                      'https://api.app24.co.in/storage/1/shops/types/a282c61025c3b0d06568db4ff208ca4063df870f.png',
                  //servicesModel[i].picture
                  // fit: BoxFit.contain,
                  width: MediaQuery.of(context).size.width / 4.5,
                  height: MediaQuery.of(context).size.height / 7.5,
                )),
                const SizedBox(
                  height: 10,
                ),
                Row(crossAxisAlignment: CrossAxisAlignment.end, children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          servicesModel![i].serviceSubcategoryName!,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: MediaQuery.of(context).size.width / 26),
                        ),
                        Text(
                          servicesModel[i].serviceCategoryId.toString(),
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: MediaQuery.of(context).size.width / 30,
                              color: Color(0xffcacaca)),
                        )
                      ],
                    ),
                  ),
                  // const SizedBox(
                  //   width: 30,
                  // ),
                  Icon(
                    App24User.call,
                    size: MediaQuery.of(context).size.width / 20,
                    color: App24Colors.darkTextColor,
                  ),
                ])
              ]),
            ),
          ),
        ),
      ));
    }
    return _widgetServices;
  }

  Future<bool> onWillPop() async {
    return (await SystemChannels.platform
            .invokeMethod('SystemNavigator.pop')) ??
        false;
    // return  true;
  }

  selectLocation(String title, bool searchUserLocation) {
    showSelectLocationSheet(
            context: context,
            title: title,
            searchUserLocation: searchUserLocation)
        .then((value) {
      if (value != null) {
        LocationModel model = value;
        setState(() {
          // context
          //     .read(
          //     homeNotifierProvider.notifier)
          //     .currentLocation
          //     .main_text = "Loading...";
          context.read(homeNotifierProvider.notifier).currentLocation = model;
          context
              .read(landingPagePromoCodeProvider.notifier)
              .getLandingPromoCodes(context: context);
          // loadData(context: context);
        });
      }
    });
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    final appCastURL = 'https://api.app24.co.in/drop_release_notes/';
    final cfg = AppcastConfiguration(url: appCastURL, supportedOS: ['android']);
    super.build(context);
    return WillPopScope(
        onWillPop: onWillPop,
        child: Scaffold(
          //persistentFooterButtons: [Column(children: [Text("dfb"),Text("df")],)],
          key: _scaffoldKey,
          // appBar: AppBar(
          //   automaticallyImplyLeading: false,
          //   title: Container(
          //     width: MediaQuery.of(context).size.width,
          //     height: 60,
          //     child: TextButton(
          //       onPressed: () {
          //         selectLocation(GlobalConstants.labelDropLocation);
          //       },
          //       child: Row(
          //         children: [
          //           Icon(
          //             App24User.path_2568_3x,
          //             size: 35,
          //             color: Color(0xffFF8282),
          //           ),
          //           Expanded(
          //             child: Column(
          //               crossAxisAlignment: CrossAxisAlignment.start,
          //               mainAxisAlignment: MainAxisAlignment.center,
          //               children: [
          //                 Text(
          //                   context
          //                           .read(homeNotifierProvider.notifier)
          //                           .currentLocation
          //                           .main_text ??
          //                       '',
          //                   style: TextStyle(
          //                     fontSize: MediaQuery.of(context).size.width / 20,
          //                     fontWeight: FontWeight.bold,
          //                     color: Color(0xffFF8282),
          //                   ),
          //                   overflow: TextOverflow.ellipsis,
          //                 ),
          //                 Text(
          //                   "Change Location",
          //                   style: TextStyle(
          //                       fontSize: 12,
          //                       fontWeight: FontWeight.w600,
          //                       color: Color(0xff373737).withOpacity(0.3)),
          //                 )
          //               ],
          //             ),
          //           ),
          //         ],
          //       ),
          //     ),
          //   ),
          //   backgroundColor: Colors.white,
          //   elevation: 0,
          // ),
          backgroundColor: Colors.white,

          body: UpgradeAlert(
            // durationToAlertAgain: Duration(seconds: 1),
            // debugLogging: true,
            // canDismissDialog: false,
            // appcastConfig: cfg,
            child: SafeArea(
              child: OfflineBuilderWidget(
                RefreshIndicator(
                  onRefresh: () {
                    return reloadData(init: false, context: context) ??
                        false as Future<void>;
                  },
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        const SizedBox(
                          height: 10,
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 15),
                          width: MediaQuery.of(context).size.width,
                          height: 60,
                          child: Row(
                            children: [
                              const Icon(
                                App24User.path_2568_3x,
                                size: 35,
                                color: App24Colors.redRestaurantThemeColor,
                              ),
                              Expanded(
                                child: Material(
                                  color: Colors.transparent,
                                  child: InkWell(
                                    onTap: () {
                                      selectLocation(
                                          GlobalConstants.labelGlobalLocation,
                                          true);
                                    },
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          context
                                                  .read(homeNotifierProvider
                                                      .notifier)
                                                  .currentLocation
                                                  .main_text ??
                                              '',
                                          style: TextStyle(
                                            fontSize: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                20,
                                            fontWeight: FontWeight.bold,
                                            color: App24Colors
                                                .redRestaurantThemeColor,
                                          ),
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                        Text(
                                          context
                                                  .read(homeNotifierProvider
                                                      .notifier)
                                                  .currentLocation
                                                  .description ??
                                              '',
                                          style: const TextStyle(
                                              fontSize: 13,
                                              fontWeight: FontWeight.w600,
                                              color:
                                                  App24Colors.lightTextColor),
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                        //  Text(
                                        //    "Change Location",
                                        //    style: TextStyle(
                                        //        fontSize: MediaQuery.of(context).size.width/36,
                                        //        fontWeight: FontWeight.w600,
                                        //       color: Color(0xff373737)
                                        //            .withOpacity(0.3)),
                                        // )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        // const SizedBox(
                        //   height: 10,
                        // ),
                        Container(
                          margin: EdgeInsets.only(left: 20.0, right: 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              // Text(
                              //   'Explore',
                              //   style: TextStyle(
                              //       fontSize: 35,
                              //       fontWeight: FontWeight.w800,
                              //       color: Color(0xff373737)),
                              // ),
                              const SizedBox(
                                height: 10,
                              ),
                              Container(

                                  // width: 350.0,
                                  child: SearchBarDes(
                                bgColor: Color(0xff676767).withOpacity(0.09),
                                textColor: Color(0xff36506B).withOpacity(0.53),
                                iconColor: Color(0xff36506B).withOpacity(0.53),
                                title: "Look up services in your area",
                              )),
                              const SizedBox(height: 20.0),
                              Text(
                                "What would you like to do today?",
                                style: TextStyle(
                                    fontWeight: FontWeight.w700,
                                    fontSize:
                                        MediaQuery.of(context).size.width / 20,
                                    color: Color(0xff373737)),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        getHomeScreenTiles(context),
                        const SizedBox(
                          height: 20,
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(30, 0, 0, 0),
                          child: Text(
                            "Offers",
                            style: TextStyle(
                                fontWeight: FontWeight.w700,
                                fontSize:
                                    MediaQuery.of(context).size.width / 17,
                                color: App24Colors.darkTextColor),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Consumer(builder: (context, watch, child) {
                          final state = watch(landingPagePromoCodeProvider);
                          if (state.isLoading) {
                            return OffersLoading();
                          } else if (state.isError) {
                            return LoadingError(
                              onPressed: (res) {
                                reloadData(init: true, context: res);
                              },
                              message: state.errorMessage,
                            );
                          } else {
                            if (state.response.responseData.length == 0) {
                              return Container(
                                child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.stretch,
                                    children: [
                                      CarouselSlider(
                                        items:
                                            Helpers().getCarouselImage(context),
                                        options: CarouselOptions(
                                            height: 145,
                                            autoPlay: true,
                                            enlargeCenterPage: false,
                                            onPageChanged: (index, reason) {
                                              setState(() {
                                                _current = index;
                                              });
                                            }),
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          ...List.generate(
                                              Helpers()
                                                  .foodOfferCarousel
                                                  .length,
                                              (index) => Container(
                                                    width: 8.0,
                                                    height: 8.0,
                                                    margin:
                                                        EdgeInsets.symmetric(
                                                            vertical: 10.0,
                                                            horizontal: 2.0),
                                                    decoration: BoxDecoration(
                                                      shape: BoxShape.circle,
                                                      color: _current == index
                                                          ? Theme.of(context)
                                                              .primaryColor
                                                          : Colors.grey[300],
                                                    ),
                                                  )),
                                        ],
                                      )
                                    ]),
                              );
                            }
                            else {
                              return Container(
                              child: Column(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.stretch,
                                  children: [
                                    CarouselSlider(
                                      items: getCarouselImages(
                                          context, state.response.responseData),
                                      options: CarouselOptions(
                                          height: 145,
                                          autoPlay: true,
                                          enlargeCenterPage: false,
                                          onPageChanged: (index, reason) {
                                            setState(() {
                                              _current = index;
                                            });
                                          }),
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        ...List.generate(
                                            state.response.responseData.length,
                                            (index) => Container(
                                                  width: 8.0,
                                                  height: 8.0,
                                                  margin: EdgeInsets.symmetric(
                                                      vertical: 10.0,
                                                      horizontal: 2.0),
                                                  decoration: BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    color: _current == index
                                                        ? Theme.of(context)
                                                            .primaryColor
                                                        : Colors.grey[300],
                                                  ),
                                                )),
                                      ],
                                    )
                                  ]),
                            );
                            }
                          }
                        })
                        // Padding(
                        //   padding: const EdgeInsets.fromLTRB(30, 0, 0, 0),
                        //   child: Text(
                        //     "More Services",
                        //     style: TextStyle(
                        //         fontWeight: FontWeight.w700,
                        //         fontSize: MediaQuery.of(context).size.width / 17,
                        //         color: App24Colors.darkTextColor),
                        //   ),
                        // ),
                        // const SizedBox(
                        //   height: 20,
                        // ),
                        // Consumer(builder: (context, watch, child) {
                        //   final state = watch(servicesNotifierProvider);
                        //   if (state.isLoading) {
                        //     return ServicesLoading();
                        //   } else if (state.isError) {
                        //     return LoadingError(
                        //       onPressed: (res) {
                        //         reloadData(
                        //             init: true, context: res, type: "services");
                        //       },
                        //       message: state.errorMessage,
                        //     );
                        //   } else {
                        //     return Column(
                        //       crossAxisAlignment: CrossAxisAlignment.center,
                        //       children: [
                        //         Wrap(
                        //           spacing: 20,
                        //           runSpacing: 20,
                        //           children: getServiceList(
                        //               context, state.response.responseData),
                        //         ),
                        //       ],
                        //     );
                        //   }
                        // }),
                        // const SizedBox(
                        //   height: 10,
                        // ),
                        // const SizedBox(
                        //   height: 20,
                        // )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
          // This trailing comma makes auto-formatting nicer for build methods.
        ));
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
  late DateTime currentBackPressTime;
  static const snackBarDuration = Duration(seconds: 2);
  final snackBar = SnackBar(
    content: Text(
      'Tap again to exit!',
      style: TextStyle(color: Colors.white, fontSize: 17, fontFamily: 'Muli'),
    ),
    duration: snackBarDuration,
  );

// Future<bool> onWillPop() {
//   // Navigator.pop(context);
//   SystemNavigator.pop();
//   /*   DateTime now = DateTime.now();
//     if (currentBackPressTime == null ||
//         now.difference(currentBackPressTime) > Duration(seconds: 2)) {
//       currentBackPressTime = now;
//
//       //Fluttertoast.showToast(msg: exit_warning);
//       return Future.value(false);
//     }*/
//   return Future.value(false);
// }
}

class HomeScreenTile {
  String? image;
  String? title1;
  String? title2;
  String? id;
  String? homeTileText;

  HomeScreenTile(
      {this.id, this.image, this.title1, this.title2, this.homeTileText});
}
