import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/api/api_response_state2.dart';
import 'package:app24_user_app/utils/services/database/database_service.dart';
import 'package:app24_user_app/utils/services/database/global_search_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class GlobalSearchNotifier extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  GlobalSearchNotifier(this._apiRepository) : super(ResponseState2(isLoading: true));

  GlobalSearchDataBase globalSearchDataBase = GlobalSearchDataBase(DatabaseService.instance);

  bool isStoreTypeKeyWordUpdated = true;
  bool isServiceSubCategoryKeyWordUpdated = true;
  bool isRideVehicleKeyWordUpdated = true;

  Future<void> getGlobalSearch(
      {bool init = true, BuildContext? context, required String keyword}) async {
    try {
      if (init) state = state.copyWith(isLoading: true);

      final globalSearchData = await globalSearchDataBase.fetchLocalGlobalSearch(keyword: keyword);

        state = state.copyWith(
            response: globalSearchData, isLoading: false, isError: false);

    } catch (e) {
      print(e.toString());
      state = state.copyWith(
          errorMessage: e.toString(), isError: true, isLoading: false);
    }
  }

  Future<void> getGlobalSearchKeywordsFromServer() async {
    try {
      final globalSearch = await _apiRepository.fetchGlobalSearch();
      if (globalSearch.responseData != null) {

        if (isStoreTypeKeyWordUpdated) {
        globalSearch.responseData!.order!.forEach((element) {
          globalSearchDataBase.addGlobalSearchRslt(element);
        });
      }
        if(isRideVehicleKeyWordUpdated) {
          globalSearch.responseData!.ride!.forEach((element) {
            globalSearchDataBase.addGlobalSearchRslt(element);
          });
        }
        //For future purpose

        // if(isServiceSubCategoryKeyWordUpdated) {
        //   globalSearch.responseData!.service!.forEach((element) {
        //     globalSearchDataBase.addGlobalSearchRslt(element);
        //   });
        // }
      } else {
        state = state.copyWith(response: []);
      }
    } catch (e) {
      // state = ResponseError(e.toString());
      state = state.copyWith(
          errorMessage: e.toString(), isLoading: false, isError: true);
    }
  }



}
