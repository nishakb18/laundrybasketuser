import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/modules/delivery/order_essentials/order_essentials_page.dart';
import 'package:app24_user_app/modules/delivery/shop_list/shop_categories.dart';
import 'package:app24_user_app/modules/restaurant/restaurant_1st_page/order_food_page.dart';
import 'package:app24_user_app/modules/ride/ride.dart';
import 'package:app24_user_app/modules/ride/ride_1st_page.dart';
import 'package:app24_user_app/modules/tabs/home/global_search/global_search_model.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_riverpod/src/provider.dart';

class HomeSearchBar extends StatefulWidget {
  // const HomeSearchBar({Key? key}) : super(key: key);

  @override
  _HomeSearchBarState createState() => _HomeSearchBarState();
}



class _HomeSearchBarState extends State<HomeSearchBar> {


  Widget getGlobalSearchResults(context,List<SearchCategory> globalSearchItems) {
    List<Widget> _widgetList = [];
if(globalSearchItems.length!=0)
  {
    for (var i = 0; i < globalSearchItems.length; i++) {
      _widgetList.add(Container(
        height: 60,
          width: MediaQuery.of(context).size.width / 1,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
          ),
          child: Stack(
              children:[ Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: CachedNetworkImage(
                        imageUrl: globalSearchItems[i].picture.toString(),
                        fit: BoxFit.contain,
                        width: MediaQuery.of(context).size.width / 7,
                        // MediaQuery.of(context).size.width / 2 - 20,
                        height: MediaQuery.of(context).size.height / 13,
                        placeholder: (context, url) => Image.asset(
                          App24UserAppImages.placeHolderImage,
                          width: MediaQuery.of(context).size.width / 7,
                          height: MediaQuery.of(context).size.height / 13,
                          fit: BoxFit.contain,
                        ),
                        errorWidget: (context, url, error) => Image.asset(
                          App24UserAppImages.placeHolderImage,
                          width: MediaQuery.of(context).size.width / 7,
                          height: MediaQuery.of(context).size.height / 13,
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                  ),
//                     ClipRRect(
// borderRadius: BorderRadius.circular(10),
//                       child: Image.asset(globalSearchItems[i].picture ?? ""),
//                     ),
                  Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 15),
                        child: Text(
                          globalSearchItems[i].name ?? "",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize:
                              MediaQuery.of(context).size.width / 27),
                        ),
                      )),
                  // Padding(
                  //   padding: const EdgeInsets.only(right: 15),
                  //   child: Icon(CupertinoIcons.right_chevron),
                  // ),
                ],
              ),
                // Divider(),
                Material(
                  color: Colors.transparent,
                  child: InkWell(
                    // splashColor: App24Colors.yellowRideThemeColor.withOpacity(0.2),
                    borderRadius: BorderRadius.circular(15),

                    onTap: () {
                      // globalSearchNavigation(globalSearchItems[i].adminService,context);
                      if(globalSearchItems[i].adminService == "TRANSPORT"){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => RidePageOpen()));
                      }
                      if(globalSearchItems[i].adminService == "ORDER"){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => ShopCategoryPage(storeTypeID: globalSearchItems[i].id.toString(),)));
                      }
                      if(globalSearchItems[i].adminService == "SERVICE"){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => RidePageOpen()));
                      }
                      // getSelectedHomeTile(globalSearchResultList[i].id, context);
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 70),
                  child: Divider(),
                )
              ]))

      );
    }}
else{
  return Container(child:Text("sorry, no service found with given name!"));

}
    return Wrap(
      runSpacing: 20,
      children: _widgetList,
    );
  }


  TextEditingController globalSearch = new TextEditingController();

  reloadData({required BuildContext context, bool? init}) {
    return context.read(globalSearchProvider.notifier).getGlobalSearch(keyword: globalSearch.text);
  }

  @override
  void initState() {
    SchedulerBinding.instance!.addPostFrameCallback((_) {
      //API call after screen build

      if(context.read(globalSearchProvider.notifier).isRideVehicleKeyWordUpdated ||
          context.read(globalSearchProvider.notifier).isServiceSubCategoryKeyWordUpdated ||
          context.read(globalSearchProvider.notifier).isStoreTypeKeyWordUpdated) {
        context.read(globalSearchProvider.notifier).getGlobalSearchKeywordsFromServer();
      }
    });
    super.initState();
  }

  searchWithKeyWord({required String keyword}){
    // globalSearch.text
context.read(globalSearchProvider.notifier).getGlobalSearch(keyword: globalSearch.text);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                // color: Colors.grey,
                decoration: BoxDecoration(color: Colors.white, boxShadow: [
                  BoxShadow(
                      color: Colors.grey[200]!,
                      spreadRadius: 0,
                      blurRadius: 3,
                      offset: Offset(0, 5))
                ]),
                height: 70,
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                child: Hero(
                  tag: "10",
                  child: Material(
                    child: TextField(
                      autofocus: true,
                      controller: globalSearch,
                      style: TextStyle(fontWeight: FontWeight.w500),
                      // keyboardType: TextInputType.number,
onChanged: (val){
                        searchWithKeyWord(keyword: val);
},
                      decoration: InputDecoration(
                        prefixIcon: Icon(
                          Icons.search,
                          color: Color(0xff36506B).withOpacity(0.53),
                        ),
                        hintText: "Look up services in your area",
                        hintStyle: TextStyle(color: Color(0xff36506B).withOpacity(0.53),fontSize: 16,fontWeight: FontWeight.w600),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide(
                            width: 0,
                            style: BorderStyle.none,
                          ),
                        ),
                        fillColor: Color(0xff676767).withOpacity(0.09),
                        filled: true,
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 5, horizontal: 30),
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 20),
                child: Consumer(builder: (context, watch, child) {
                  final state = watch(globalSearchProvider);
                  if (state.isLoading) {
                    return Container();
                  } else if (state.isError) {
                    return LoadingError(
                      onPressed: (res) {
                        reloadData(init: true, context: res);
                      },
                      message: state.errorMessage.toString(),
                    );
                  } else {

                    return
                      state.response == null ? Container(child:Text("no items found")) :
                      Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15),
                      child: getGlobalSearchResults(context,state.response)
                      // ListView.builder(
                      //   itemBuilder: (context, index) =>
                      //       getGlobalSearchResults(context,state.response),
                      //   padding: EdgeInsets.only(
                      //     top: 5,
                      //   ),
                      //   // itemCount: !.length,
                      //   shrinkWrap: true,
                      //   physics: NeverScrollableScrollPhysics(),
                      // )
                      // ListView(
                      //   children: [
                      //     getGlobalSearchResults(context,state.response),
                      //   ],
                      // )

                    );

                  }
                }),
              )
            ],
          ),
        ),
      ),
    );
  }
}


