GlobalSearchModel globalSearchModelFromJson(str) {
  return GlobalSearchModel.fromJson(str);
}


class GlobalSearchModel  {
  String? statusCode;
  String? title;
  String? message;
  ResponseData? responseData;
  List<dynamic>? responseNewData;
  List<dynamic>? error;

  GlobalSearchModel (
      {this.statusCode,
        this.title,
        this.message,
        this.responseData,
        this.responseNewData,
        this.error});

  GlobalSearchModel .fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    responseData = json['responseData'] != null
        ? new ResponseData.fromJson(json['responseData'])
        : null;
    if (json['responseNewData'] != null) {
      responseNewData = [];
      json['responseNewData'].forEach((v) {
        responseNewData!.add(v);
      });
    }
    if (json['error'] != null) {
      error = [];
      json['error'].forEach((v) {
        error!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['title'] = this.title;
    data['message'] = this.message;
    if (this.responseData != null) {
      data['responseData'] = this.responseData!.toJson();
    }
    if (this.responseNewData != null) {
      data['responseNewData'] =
          this.responseNewData!.map((v) => v.toJson()).toList();
    }
    if (this.error != null) {
      data['error'] = this.error!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ResponseData {
  List<SearchCategory>? order;
  List<SearchCategory>? service;
  List<SearchCategory>? ride;

  ResponseData({this.order, this.service, this.ride});

  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['order'] != null) {
      order = [];
      json['order'].forEach((v) {
        order!.add(new SearchCategory.fromJson(v));
      });
    }
    if (json['service'] != null) {
      service = [];
      json['service'].forEach((v) {
        service!.add(new SearchCategory.fromJson(v));
      });
    }
    if (json['ride'] != null) {
      ride = [];
      json['ride'].forEach((v) {
        ride!.add(new SearchCategory.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.order != null) {
      data['order'] = this.order!.map((v) => v.toJson()).toList();
    }
    if (this.service != null) {
      data['service'] = this.service!.map((v) => v.toJson()).toList();
    }
    if (this.ride != null) {
      data['ride'] = this.ride!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class SearchCategory {
  String? adminService;
  int? id;
  String? keywords;
  String? name;
  String? picture;

  SearchCategory({this.adminService, this.id, this.keywords, this.name, this.picture});

  SearchCategory.fromJson(Map<String, dynamic> json) {
    adminService = json['admin_service'];
    id = json['id'];
    keywords = json['keywords'];
    name = json['name'];
    picture = json['picture'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['admin_service'] = this.adminService;
    data['id'] = this.id;
    data['keywords'] = this.keywords;
    data['name'] = this.name;
    data['picture'] = this.picture;
    return data;
  }
}
