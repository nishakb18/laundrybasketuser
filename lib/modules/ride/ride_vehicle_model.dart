RideVehiclesModel rideVehicleModelFromJson(str) {
  return RideVehiclesModel.fromJson(str);
}



class RideVehiclesModel {
  String? statusCode;
  String? title;
  String? message;
  RideVehiclesResponseData? responseData;
  List<dynamic>? error;

  RideVehiclesModel(
      {this.statusCode,
        this.title,
        this.message,
        this.responseData,
        this.error});

  RideVehiclesModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    responseData = json['responseData'] != null
        ? new RideVehiclesResponseData.fromJson(json['responseData'])
        : null;
    if (json['error'] != null) {
      error = [];
      json['error'].forEach((v) {
        error!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['title'] = this.title;
    data['message'] = this.message;
    if (this.responseData != null) {
      data['responseData'] = this.responseData!.toJson();
    }
    if (this.error != null) {
      data['error'] = this.error!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class RideVehiclesResponseData {
  List<VehicleServices>? services;
  List<Providers>? providers;
  List<dynamic>? promocodes;

  RideVehiclesResponseData({this.services, this.providers, this.promocodes});

  RideVehiclesResponseData.fromJson(Map<String, dynamic> json) {
    if (json['services'] != null) {
      services = [];
      json['services'].forEach((v) {
        services!.add(new VehicleServices.fromJson(v));
      });
    }
    if (json['providers'] != null) {
      providers =[];
      json['providers'].forEach((v) {
        providers!.add(new Providers.fromJson(v));
      });
    }
    if (json['promocodes'] != null) {
      promocodes =[];
      json['promocodes'].forEach((v) {
        promocodes!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.services != null) {
      data['services'] = this.services!.map((v) => v.toJson()).toList();
    }
    if (this.providers != null) {
      data['providers'] = this.providers!.map((v) => v.toJson()).toList();
    }
    if (this.promocodes != null) {
      data['promocodes'] = this.promocodes!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class VehicleServices {
  String? estimatedTime;
  int? id;
  int? rideTypeId;
  String? vehicleType;
  String? vehicleName;
  String? vehicleImage;
  String? vehicleMarker;
  int? capacity;
  int? status;
  PriceDetails? priceDetails;

  VehicleServices(
      {this.estimatedTime,
      this.id,
      this.rideTypeId,
      this.vehicleType,
      this.vehicleName,
      this.vehicleImage,
      this.vehicleMarker,
      this.capacity,
      this.status,
      this.priceDetails});

  VehicleServices.fromJson(Map<String, dynamic> json) {
    estimatedTime = json['estimated_time'];
    id = json['id'];
    rideTypeId = json['ride_type_id'];
    vehicleType = json['vehicle_type'];
    vehicleName = json['vehicle_name'];
    vehicleImage = json['vehicle_image'];
    vehicleMarker = json['vehicle_marker'];
    capacity = json['capacity'];
    status = json['status'];
    priceDetails = json['price_details'] != null
        ? new PriceDetails.fromJson(json['price_details'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['estimated_time'] = this.estimatedTime;
    data['id'] = this.id;
    data['ride_type_id'] = this.rideTypeId;
    data['vehicle_type'] = this.vehicleType;
    data['vehicle_name'] = this.vehicleName;
    data['vehicle_image'] = this.vehicleImage;
    data['vehicle_marker'] = this.vehicleMarker;
    data['capacity'] = this.capacity;
    data['status'] = this.status;
    if (this.priceDetails != null) {
      data['price_details'] = this.priceDetails!.toJson();
    }
    return data;
  }
}

class PriceDetails {
  int? rideDeliveryVehicleId;
  String? calculator;
  int? fixed;
  int? price;
  int? minute;
  int? hour;
  int? distance;

  PriceDetails(
      {this.rideDeliveryVehicleId,
        this.calculator,
        this.fixed,
        this.price,
        this.minute,
        this.hour,
        this.distance});

  PriceDetails.fromJson(Map<String, dynamic> json) {
    rideDeliveryVehicleId = json['ride_delivery_vehicle_id'];
    calculator = json['calculator'];
    fixed = json['fixed'];
    price = json['price'];
    minute = json['minute'];
    hour = json['hour'];
    distance = json['distance'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ride_delivery_vehicle_id'] = this.rideDeliveryVehicleId;
    data['calculator'] = this.calculator;
    data['fixed'] = this.fixed;
    data['price'] = this.price;
    data['minute'] = this.minute;
    data['hour'] = this.hour;
    data['distance'] = this.distance;
    return data;
  }
}

class Providers {
  int? id;
  String? firstName;
  String? lastName;
  String? email;
  String? countryCode;
  String? mobile;
  String? gender;
  double? latitude;
  double? longitude;
  double? distance;
  dynamic currentRideVehicle;
  dynamic currentStore;
  Service? service;
  int? serviceId;

  Providers(
      {this.id,
        this.firstName,
        this.lastName,
        this.email,
        this.countryCode,
        this.mobile,
        this.gender,
        this.latitude,
        this.longitude,
        this.distance,
        this.currentRideVehicle,
        this.currentStore,
        this.service,
        this.serviceId});

  Providers.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    email = json['email'];
    countryCode = json['country_code'];
    mobile = json['mobile'];
    gender = json['gender'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    distance = json['distance'];
    currentRideVehicle = json['current_ride_vehicle'];
    currentStore = json['current_store'];
    service =
    json['service'] != null ? new Service.fromJson(json['service']) : null;
    serviceId = json['service_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['email'] = this.email;
    data['country_code'] = this.countryCode;
    data['mobile'] = this.mobile;
    data['gender'] = this.gender;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['distance'] = this.distance;
    data['current_ride_vehicle'] = this.currentRideVehicle;
    data['current_store'] = this.currentStore;
    if (this.service != null) {
      data['service'] = this.service!.toJson();
    }
    data['service_id'] = this.serviceId;
    return data;
  }
}

class Service {
  int? id;
  int? providerId;
  String? adminService;
  int? providerVehicleId;
  int? rideDeliveryId;
  int? serviceId;
  int? categoryId;
  int? subCategoryId;
  int? companyId;
  int? baseFare;
  int? perMiles;
  int? perMins;
  String? status;
  RideVehicle? rideVehicle;

  Service(
      {this.id,
        this.providerId,
        this.adminService,
        this.providerVehicleId,
        this.rideDeliveryId,
        this.serviceId,
        this.categoryId,
        this.subCategoryId,
        this.companyId,
        this.baseFare,
        this.perMiles,
        this.perMins,
        this.status,
        this.rideVehicle});

  Service.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    providerId = json['provider_id'];
    adminService = json['admin_service'];
    providerVehicleId = json['provider_vehicle_id'];
    rideDeliveryId = json['ride_delivery_id'];
    serviceId = json['service_id'];
    categoryId = json['category_id'];
    subCategoryId = json['sub_category_id'];
    companyId = json['company_id'];
    baseFare = json['base_fare'];
    perMiles = json['per_miles'];
    perMins = json['per_mins'];
    status = json['status'];
    rideVehicle = json['ride_vehicle'] != null
        ? new RideVehicle.fromJson(json['ride_vehicle'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['provider_id'] = this.providerId;
    data['admin_service'] = this.adminService;
    data['provider_vehicle_id'] = this.providerVehicleId;
    data['ride_delivery_id'] = this.rideDeliveryId;
    data['service_id'] = this.serviceId;
    data['category_id'] = this.categoryId;
    data['sub_category_id'] = this.subCategoryId;
    data['company_id'] = this.companyId;
    data['base_fare'] = this.baseFare;
    data['per_miles'] = this.perMiles;
    data['per_mins'] = this.perMins;
    data['status'] = this.status;
    if (this.rideVehicle != null) {
      data['ride_vehicle'] = this.rideVehicle!.toJson();
    }
    return data;
  }
}

class RideVehicle {
  int? id;
  int? rideTypeId;
  String? vehicleType;
  String? vehicleName;
  String? vehicleImage;
  String? vehicleMarker;
  int? capacity;
  int? status;

  RideVehicle(
      {this.id,
        this.rideTypeId,
        this.vehicleType,
        this.vehicleName,
        this.vehicleImage,
        this.vehicleMarker,
        this.capacity,
        this.status});

  RideVehicle.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    rideTypeId = json['ride_type_id'];
    vehicleType = json['vehicle_type'];
    vehicleName = json['vehicle_name'];
    vehicleImage = json['vehicle_image'];
    vehicleMarker = json['vehicle_marker'];
    capacity = json['capacity'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['ride_type_id'] = this.rideTypeId;
    data['vehicle_type'] = this.vehicleType;
    data['vehicle_name'] = this.vehicleName;
    data['vehicle_image'] = this.vehicleImage;
    data['vehicle_marker'] = this.vehicleMarker;
    data['capacity'] = this.capacity;
    data['status'] = this.status;
    return data;
  }
}
