import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/modules/home.dart';
import 'package:app24_user_app/modules/my_account/support.dart';
import 'package:app24_user_app/modules/ride/ride_details.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/widgets/common_appbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class RideCompletedPage extends StatefulWidget {
  @override
  _RideCompletedPageState createState() => _RideCompletedPageState();
}

class _RideCompletedPageState extends State<RideCompletedPage> {
  String rideCompleteText =
      "Congratulations! You have reached your destination. Hope you enjoyed the ride!";

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () {
          return Navigator.of(context)
              .pushAndRemoveUntil(
                  MaterialPageRoute(builder: (context) => HomePage()),
                  (Route<dynamic> route) => false)
              .then((value) => value as bool);
        },
        child: Scaffold(
            appBar: CustomAppBar(
              title: "Ride Completed",
            ),
            backgroundColor: Colors.white,
            body: SafeArea(
                child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    const SizedBox(
                      height: 10,
                    ),
                    Expanded(
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            const SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                Expanded(
                                  child: Text(
                                    rideCompleteText,
                                    style: TextStyle(
                                        color: App24Colors.darkTextColor,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 16),
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Text(
                              "Leave a feedback for your driver below.",
                              style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500,
                                  color: App24Colors.darkTextColor),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                TextButton(
                                  onPressed: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => RideDetailsPage(
                                                type: "details",
                                              )),
                                    );
                                  },
                                  child: Text(
                                    "More Details",
                                    style: TextStyle(
                                        color:
                                            App24Colors.darkTextColor),
                                  ),
                                )
                              ],
                            ),
                            Text(
                              "Ride Details",
                              style: TextStyle(
                                  fontSize: 17, fontWeight: FontWeight.bold),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  decoration: BoxDecoration(
                                      color: Color(0xff1F79FF),
                                      shape: BoxShape.circle,
                                      border: Border.all(
                                          color: Theme.of(context)
                                              .primaryColorLight,
                                          width: 5.0)),
                                  padding: EdgeInsets.all(5),
                                ),
                                SizedBox(
                                  width: 15,
                                ),
                                Expanded(
                                  child: Text(
                                    "Ministry of Commerce & Industry Administrative Building, Seaport - Airport Rd, Kochi, Kerala 682037",
                                    style: TextStyle(
                                        fontSize: 15,
                                        fontWeight: FontWeight.w500),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              ],
                            ),
                            Container(
                              height: 20,
                              padding: EdgeInsets.only(left: 2),
                              alignment: Alignment.centerLeft,
                              child: VerticalDivider(
                                color: App24Colors.greenOrderEssentialThemeColor,
                              ),
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  decoration: BoxDecoration(
                                      color: Colors.red[500],
                                      shape: BoxShape.circle,
                                      border: Border.all(
                                          color: Colors.red[200]!, width: 5.0)),
                                  padding: EdgeInsets.all(5),
                                ),
                                SizedBox(
                                  width: 15,
                                ),
                                Expanded(
                                  child: Text(
                                    "TOG Rd, South Kalamassery, Kalamassery, Kochi, Kerala 682033",
                                    style: TextStyle(
                                        fontSize: 15,
                                        fontWeight: FontWeight.w500),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                )
                              ],
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            Row(
                              children: [
                                Image.asset(
                                  App24UserAppImages.rideCompleteDriverImage,
                                  width: 50,
                                  height: 50,
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                Flexible(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          Text(
                                            "Ramesh Kumar",
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 15),
                                          ),
                                          const SizedBox(
                                            width: 15,
                                          ),
                                          Icon(
                                            Icons.star,
                                            color: Color(0xffFFC300),
                                          ),
                                          const SizedBox(
                                            width: 10,
                                          ),
                                          Text(
                                            "4.8",
                                            style: TextStyle(
                                                color: App24Colors.darkTextColor,
                                                fontWeight: FontWeight.w500),
                                          )
                                        ],
                                      ),
                                      Text("Honda Civic"),
                                      Text("KL 45 L 2834"),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 15,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [getTitle("How was your Ride?")],
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  RatingBar.builder(
                                    initialRating: 0,
                                    direction: Axis.horizontal,
                                    allowHalfRating: true,
                                    itemCount: 5,
                                    itemSize: 40,
                                    itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                                    itemBuilder: (context, _) => Icon(
                                      Icons.star,
                                      color: Colors.amber,
                                    ),
                                    onRatingUpdate: (rating) {
                                      print(rating);
                                    },
                                  ),
                                ]),
                            const SizedBox(
                              height: 30,
                            ),
                            MaterialButton(
                                onPressed: () {
                                  /*Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => RideCompletedPage()),
                        );*/
                                },
                                color: App24Colors.darkTextColor,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                child: Padding(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 15, horizontal: 20),
                                  child: Text(
                                    "SUBMIT",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                  ),
                                )),
                            const SizedBox(
                              height: 10,
                            ),
                            TextButton(
                                onPressed: () {},
                                child: Text(
                                  "Any questions or feedback?",
                                  style: TextStyle(
                                      color: App24Colors.darkTextColor,
                                      fontStyle: FontStyle.italic),
                                )),
                            Material(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15)),
                              color: Color(0xffebebeb),
                              child: ListTile(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15)),
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => SupportPage()),
                                  );
                                },
                                leading: Image.asset(
                                  App24UserAppImages.helpAndSupportIcon,
                                  width: 25,
                                  height: 25,
                                ),
                                title: Text(
                                  "Help and Support",
                                  style: TextStyle(
                                      color: App24Colors.darkTextColor,
                                      fontWeight: FontWeight.w500),
                                ),
                                trailing: Image.asset(
                                  App24UserAppImages.roundRArrow,
                                  width: 20,
                                  height: 20,
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ]),
            )) // This trailing comma makes auto-formatting nicer for build methods.
            ));
  }
}
