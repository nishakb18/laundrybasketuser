import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/api/api_response_state2.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class RideVehiclesRequestNotifier extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  RideVehiclesRequestNotifier(this._apiRepository)
      : super(ResponseState2(isLoading: true));

  /* Future<void> getEstimateFare( {bool init = true, required double sLat, required double sLong, required double dLat, required double dLong,  required int serviceType, String payMode="CASH", int cardId=1})
  async {
    try {
      if (init) state = state.copyWith(isLoading: true);
      final rideVehicles =
      await _apiRepository.fetchEstimateFare(dLat: dLat,dLong: dLong,sLat: sLat,sLong: sLong,payMode:  payMode,cardId: cardId,serviceTypeId: serviceType);
      state = state.copyWith(
          response: rideVehicles, isLoading: false, isError: false);
    } catch (e) {
      state = state.copyWith(
          errorMessage: e.toString(), isLoading: false, isError: true);
    }
  }*/
  Future<void> sendRideRequest(
      {bool init = true,
      required double sLat,
      required double sLong,
      required double dLat,
      required double dLong,
      required String sAddress,
      required String dAddress,
      required String estimatedKm,
      required int serviceType,
      String payMode = "CASH",
      int cardId = 1 // value is 1 for taxi service
      }) async {
    try {
      Map body = {
        's_latitude': sLat,
        's_longitude': sLong,
        'service_type': serviceType,
        'd_latitude': dLat,
        'd_longitude': dLong,
        's_address': sAddress,
        'd_address': dAddress,
        'distance': estimatedKm,
        'ride_type_id': 1, // value is 1 for taxi
      };
      print("getProvidersBidFares");
      print("body of ride" + body.toString());
      //if (init) state = state.copyWith(isLoading: true);
      final rideVehicles = await _apiRepository.fetchRideRequest(body: body);

      state = state.copyWith(
          response: rideVehicles, isLoading: false, isError: false);
    } catch (e) {
      showLog(e.toString());
      /* state = state.copyWith(
          errorMessage: e.toString(), isLoading: false, isError: true);*/
    }
  }

  Future<void> getRiderDetails({
    bool init = true,
  }) async {
    try {
      //if (init) state = state.copyWith(isLoading: true);
      final riderDetails = await _apiRepository.fetchTaxiRiders();

      state = state.copyWith(
          response: riderDetails, isLoading: false, isError: false);
    } catch (e) {
      showLog("error is :::::::::"+e.toString());

      state = state.copyWith(
          errorMessage: e.toString(), isLoading: false, isError: true);
    }
  }
}
