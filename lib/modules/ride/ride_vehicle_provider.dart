import 'package:app24_user_app/config/routes.dart';
import 'package:app24_user_app/constants/global_constants.dart';
import 'package:app24_user_app/helpers.dart';
import 'package:app24_user_app/models/history_model.dart';
import 'package:app24_user_app/models/home_delivery_send_order_detail.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/api/api_response_state2.dart';
import 'package:app24_user_app/utils/services/database/database_service.dart';
import 'package:app24_user_app/utils/services/database/history_database.dart';
import 'package:app24_user_app/utils/services/socket/socket_service.dart';
import 'package:app24_user_app/widgets/bottomsheets/make_payment_bottomsheet/make_payment_bottom_sheet.dart';
import 'package:app24_user_app/widgets/bottomsheets/rating/rate_delivery.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:socket_io_client/socket_io_client.dart';

class RideVehicleDetailsNotifier extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  RideVehicleDetailsNotifier(this._apiRepository)
      : super(ResponseState2(isLoading: true));

  //HistoryDatabase historyDatabase = HistoryDatabase(DatabaseService.instance);

  late final SocketService _socketService = SocketService.instance;

  //String orderUpdate = "Order Created";
 /* bool onGoingOrderAvailable = false;
  bool hasMultipleOnGoingOrdersAvailable = false;*/
  bool isWaitingForPaymentResponse = false;

  String orderStatus = '';
  String orderStatusDescription = '';
  String orderType = 'Send';

  Future<void> getOrderDetails({String? id, bool isInitialLoad = false,}) async {
    try {
      final orderDetails = await _apiRepository.fetchDeliveryOrderDetail(
          );
      if (orderDetails.responseData != null
          ) {
        int onGoingOrdersLength = 0;
        orderDetails.responseData!.forEach((element) async {
          if (element.status != GlobalConstants.orderStatusOrderCompleted &&
              element.status != GlobalConstants.orderStatusCancelled) {
            onGoingOrdersLength++;
            if (element.status == GlobalConstants.orderStatusProviderRejected) {
              element.status = GlobalConstants.orderStatusProcessing;
            }
            if (isInitialLoad) {
              emitSocket(orderID: element.id!);
            }
          }
          /*var res = await historyDatabase.updateHistory(
              status: element.status!, id: element.id!);*/
        });
        /*    if (onGoingOrdersLength == 0) {
          onGoingOrderAvailable = false;
        } else {
          if (onGoingOrdersLength > 1) {
            hasMultipleOnGoingOrdersAvailable = true;
          } else {
            hasMultipleOnGoingOrdersAvailable = false;
          }

          onGoingOrderAvailable = true;
*/
        if (double.parse(
            orderDetails.responseData![0].toPayAmount!.replaceAll(",", "")) !=
            0) {
          openPaymentBottomSheet(responseData: orderDetails.responseData![0]);
        }

        if (isWaitingForPaymentResponse) {
          // isWaitingForPaymentResponse = false;
          if (orderDetails.responseData![0].paid == 1) {
            Navigator.pop(navigatorKey.currentContext!);
          }
        }

        if (orderDetails.responseData![0].status == "ARRIVED ") {
          print(
              "order status" + orderDetails.responseData![0].status.toString());
          Helpers().getCommonBottomSheet(
              context: navigatorKey.currentContext!,
              content: RateDeliveryBoy(
                deliveryBoyName:
                orderDetails.responseData![0].providerName,
                orderId: orderDetails.responseData![0].id!,
              ),
              title: "Your opinion matters to us");
        }
        print("order status" + orderDetails.responseData![0].status.toString());
        orderStatus = orderDetails.responseData![0].status!;
        orderDetails.responseData![0].orderDetails!.aStatus!
            .forEach((element) {
          if (element.status == "Completed") {
            orderStatusDescription = element.statusText!;
          }

          // if(element.status == "Completed"){
          //
          // }
        });
      }


     // showLog(onGoingOrderAvailable.toString());
      showLog(orderStatusDescription);
      state = state.copyWith(
          response: orderDetails, isLoading: false, isError: false);
    } catch (e) {
      state = state.copyWith(
          errorMessage: e.toString(), isLoading: false, isError: true);
    }
  }

  openPaymentBottomSheet(
      {required HomeDeliveryOrderDetailResponseData responseData}) async {
    var result = await Helpers().getCommonBottomSheet(
        enableDrag: false,
        isDismissible: false,
        context: navigatorKey.currentContext!,
        content: MakePaymentBottomSheet(
          detailResponseData: responseData,
        ),
        title: "Make Payment");
    if (result != null) {}
  }

  /*checkOngoingOrders() async {
    List<HistoryData> list = await historyDatabase.fetchNonCompleted();
    if (list.isNotEmpty) {
      orderType = list[0].booking_type!;
      getOrderDetails(id: list[0].id.toString(), isInitialLoad: true);
    }
  }*/

  emitSocket({required int orderID}) async {
    showLog("Socket emitting");
    try {
      Socket? socket = await _socketService.socket;
      socket!.emit('joinPrivateRoom', {'room_1_R${orderID}_ORDER'});
      showLog("Socket emitted");
    } catch (e) {
      showLog("error in socket is : ${e.toString()}");
    }
  }
}
