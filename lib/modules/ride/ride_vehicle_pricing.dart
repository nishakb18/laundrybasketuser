import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/helpers.dart';
import 'package:app24_user_app/modules/ride/ride_vehicle_model.dart';
import 'package:app24_user_app/modules/ride/track_ride.dart';
import 'package:app24_user_app/widgets/bottomsheets/booking_for_someone.dart';
import 'package:app24_user_app/widgets/bottomsheets/cancellation_bottomSheet.dart';
import 'package:app24_user_app/widgets/bottomsheets/change_payment_bottomSheet.dart';
import 'package:app24_user_app/widgets/common_appbar.dart';
import 'package:app24_user_app/widgets/common_main_button.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class VehiclePricingPage extends StatefulWidget {
  final VehicleServices selectedVehicle;

  const VehiclePricingPage({Key? key, required this.selectedVehicle})
      : super(key: key);

  @override
  _VehiclePricingPageState createState() => _VehiclePricingPageState();
}

class _VehiclePricingPageState extends State<VehiclePricingPage> {
  bool _isBookedForSomeone = false;
  String _isUseWallet = "Cash";
  late VehicleServices selectedVehicle;

//   void changePaymentMode() {
//     showDialog(
//       context: context,
// // barrierDismissible: false,
//       builder: (_) {
//         return PaymentChangeAlertBox();
//       },
//     );
//   }

  // void confirmBookForSomeone() {
  //   if (_formKey.currentState!.validate()) {
  //     setState(() {
  //       _isBookedForSomeone = true;
  //       Navigator.pop(context);
  //     });
  //   }
  // }

  @override
  void initState() {
    super.initState();
    initialize();
  }

  initialize() {
    selectedVehicle = widget.selectedVehicle;
  }

  String? bookForSomeone;

  void bookingForSomeone(BuildContext context) async {
    final result = await Helpers().getCommonBottomSheet(
        context: context,
        content: BookingForSomeone(),
        title: "Booking For Someone");
    if (result != null) {
      setState(() {
        bookForSomeone = result['name'];
        _isBookedForSomeone = true;
      });
    }
  }

  Widget buildHeading({required String title}) {
    return Row(
      children: [
        Text(
          title,
          style: TextStyle(
              color: App24Colors.darkTextColor,
              fontSize: 16,
              fontWeight: FontWeight.bold),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomAppBar(
          title: "Confirm Ride Details",
        ),
        resizeToAvoidBottomInset: true,
        backgroundColor: Colors.white,
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                const SizedBox(
                  height: 20,
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                  ),
                  // padding: EdgeInsets.all(15),
                  child: Column(
                    children: <Widget>[
                      buildHeading(title: "Vehicle Details"),
                      Container(
                          width: MediaQuery.of(context).size.width / 2.6,
                          height: 120,
                          padding: EdgeInsets.all(10),
                          child: Hero(
                            tag: selectedVehicle.id!,
                            child: CachedNetworkImage(
                              imageUrl: selectedVehicle.vehicleImage ?? '',
                              fit: BoxFit.fill,
                              placeholder: (context, url) => Image.asset(
                                App24UserAppImages.placeHolderImage,
                                fit: BoxFit.cover,
                              ),
                              errorWidget: (context, url, error) => Image.asset(
                                App24UserAppImages.placeHolderImage,
                                fit: BoxFit.cover,
                              ),
                            ),
                          )),
                      Text(
                        selectedVehicle.vehicleName!,
                        style: TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 15),
                      ),
                      Row(
                        children: <Widget>[
                          Text(
                            "Estimated KM",
                            style: TextStyle(
                                fontWeight: FontWeight.w500, fontSize: 15),
                          ),
                          SizedBox(
                            width: 15,
                          ),
                          Expanded(
                            child: Text(
                              "14 kms",
                              textAlign: TextAlign.end,
                              style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  color: App24Colors.yellowRideThemeColor,
                                  fontSize: 15),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        children: <Widget>[
                          Text(
                            "Standard Fare",
                            style: TextStyle(
                                fontWeight: FontWeight.w500, fontSize: 15),
                          ),
                          SizedBox(
                            width: 15,
                          ),
                          Expanded(
                            child: Text(
                              "\u20B9105.80",
                              textAlign: TextAlign.end,
                              style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  color: App24Colors.yellowRideThemeColor,
                                  fontSize: 15),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        children: <Widget>[
                          Text(
                            "Vehicle Model",
                            style: TextStyle(
                                fontWeight: FontWeight.w500, fontSize: 15),
                          ),
                          SizedBox(
                            width: 15,
                          ),
                          Expanded(
                            child: Text(
                              "Auto",
                              textAlign: TextAlign.end,
                              style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  color: App24Colors.yellowRideThemeColor,
                                  fontSize: 15),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                // CommonHelpers().getSubTitle("Mode of Payment", context),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Text(
                    "Mode of Payment",
                    style: TextStyle(
                        color: App24Colors.darkTextColor,
                        fontSize: 16,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    // color: Theme.of(context).accentColor
                  ),
                  child: IntrinsicHeight(
                    child: Row(
                      children: <Widget>[
                        Expanded(
                            child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                                child: Material(
                                    color: Colors.transparent,
                                    child: InkWell(
                                      borderRadius: BorderRadius.circular(15),
                                      onTap: () {
                                        setState(() {
                                          _isUseWallet = "Cash";
                                        });
                                      },
                                      onLongPress: () {
                                        Helpers().getCommonBottomSheet(
                                            context: context,
                                            content: ChangePaymentBottomSheet(),
                                            title: "Change payment method");
                                      },
                                      child: Padding(
                                        padding: EdgeInsets.symmetric(
                                            vertical: 15, horizontal: 10),
                                        child: Row(
                                          children: <Widget>[
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Icon(
                                              _isUseWallet == "Cash"
                                                  ? Icons.check_circle
                                                  : Icons
                                                      .radio_button_unchecked,
                                              color: App24Colors.darkTextColor,
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Expanded(
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.stretch,
                                                children: <Widget>[
                                                  Text(
                                                    "Cash",
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        fontSize: 16),
                                                  ),
                                                  Text(
                                                    "Change",
                                                    style: TextStyle(
                                                        color: App24Colors
                                                            .yellowRideThemeColor,
                                                        fontWeight:
                                                            FontWeight.w500),
                                                  ),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    )))
                          ],
                        )),
                        VerticalDivider(
                          width: 1,
                        ),
                        Expanded(
                            child: Material(
                                color: Colors.transparent,
                                child: InkWell(
                                  borderRadius: BorderRadius.circular(15),
                                  onTap: () {
                                    setState(() {
                                      _isUseWallet = "Wallet";
                                    });
                                  },
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                        vertical: 15, horizontal: 10),
                                    child: Row(
                                      children: <Widget>[
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Icon(
                                          _isUseWallet == "Wallet"
                                              ? Icons.check_circle
                                              : Icons.radio_button_unchecked,
                                          color: App24Colors.darkTextColor,
                                        ),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.stretch,
                                            children: <Widget>[
                                              Text(
                                                "Use Wallet",
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 16),
                                              ),
                                              Text(
                                                "\u20B9140.00",
                                                style: TextStyle(
                                                    color: App24Colors
                                                        .yellowRideThemeColor,
                                                    fontWeight:
                                                        FontWeight.w500),
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ))),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Material(
                    color: Colors.transparent,
                    child: ListTile(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15)),
                      leading: Icon(
                        !_isBookedForSomeone
                            ? Icons.radio_button_unchecked
                            : Icons.check_circle,
                        color: App24Colors.darkTextColor,
                      ),
                      title: Text(
                        "Booked for ${_isBookedForSomeone ? bookForSomeone : "Someone"}",
                        style: TextStyle(fontWeight: FontWeight.w600),
                      ),
                      onTap: () {
                        if (!_isBookedForSomeone) {
                          bookingForSomeone(context);
                        } else {
                          setState(() {
                            _isBookedForSomeone = false;
                          });
                        }
                      },
                    ),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: CommonButton(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => TrackRidePage()),
                        );
                      },
                      bgColor: App24Colors.yellowRideThemeColor,
                      text: "CONFIRM YOUR BOOKING",
                    )),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: TextButton(
                      style: TextButton.styleFrom(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10))),
                      onPressed: () {
                        Helpers().getCommonBottomSheet(
                            context: context,
                            content: CancellationBottomSheet(),
                            title: "Reason for Cancellation");
                      },
                      child: Text(
                        "Or cancel your order",
                        style: TextStyle(
                            color: Color(0xffFF4755),
                            decoration: TextDecoration.underline),
                      )),
                ),
                SizedBox(
                  height: 20,
                ),
              ],
            ),
          ),
        ) // This trailing comma makes auto-formatting nicer for build methods.
        );
  }
}

class Driver {
  String? driverName;
  String? driverEta;
  String? driverQuote;

  Driver({this.driverEta, this.driverName, this.driverQuote});
}
