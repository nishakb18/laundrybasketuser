import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/widgets/common_appbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RideDetailsPage extends StatefulWidget {
  RideDetailsPage({Key? key, this.type, this.pickupAddress, this.dropAddress, this.selectedVehicleType, this.estimatedKm}) : super(key: key);

  final String? pickupAddress;
  final String? dropAddress;
  final String? selectedVehicleType;
  final String? estimatedKm;

  final String? type;

  @override
  _RideDetailsPageState createState() => _RideDetailsPageState();
}

class _RideDetailsPageState extends State<RideDetailsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        hideCartIcon: "true",
        title:
            widget.type == "payment" ? "Payment Confirmation" : "Ride Details",
      ),
      backgroundColor: Colors.white,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: Stack(children: <Widget>[
          SafeArea(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: 20,
                  ),
                  // getSubTitle("Ride Details", context),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Text(
                      "Ride Details",
                      style:
                          TextStyle(fontWeight: FontWeight.w700, fontSize: 17),
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                            color: App24Colors.greenOrderEssentialThemeColor,
                            shape: BoxShape.circle,
                            border: Border.all(
                                color: Theme.of(context).primaryColorLight,
                                width: 5.0)),
                        padding: EdgeInsets.all(5),
                      ),
                      SizedBox(
                        width: 15,
                      ),
                      Expanded(
                        child: Text(
                          widget.pickupAddress.toString(),
                          style: TextStyle(
                              fontSize: 15, fontWeight: FontWeight.w600),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ],
                  ),
                  Container(
                    height: 25,
                    padding: EdgeInsets.only(left: 2),
                    alignment: Alignment.centerLeft,
                    child: VerticalDivider(
                      color: App24Colors.greenOrderEssentialThemeColor,
                    ),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                            color: Colors.red[500],
                            shape: BoxShape.circle,
                            border:
                                Border.all(color: Colors.red[200]!, width: 5.0)),
                        padding: EdgeInsets.all(5),
                      ),
                      SizedBox(
                        width: 15,
                      ),
                      Expanded(
                        child: Text(
                          widget.dropAddress.toString(),
                          style: TextStyle(
                              fontSize: 15, fontWeight: FontWeight.w600),
                          overflow: TextOverflow.ellipsis,
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    child: Column(
                      children: <Widget>[
                        getDetailsRow("Booking ID", "BOOK235RT",context),
                        SizedBox(
                          height: 5,
                        ),
                        getDetailsRow("Vehicle Type", widget.selectedVehicleType ?? "",context),
                        SizedBox(
                          height: 5,
                        ),
                        getDetailsRow("Driver", "Driver Name",context),
                        SizedBox(
                          height: 5,
                        ),
                        getDetailsRow("Distance",widget.estimatedKm ?? "",context),
                        SizedBox(
                          height: 5,
                        ),
                        getDetailsRow("Time", "30 mins",context),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  // getSubTitle("Fare Details", context),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Text(
                      "Fare Details",
                      style:
                          TextStyle(fontWeight: FontWeight.w700, fontSize: 17),
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    child: Column(
                      children: <Widget>[
                        getDetailsRow("Base Fare", "\u20B9205.00",context),
                        SizedBox(
                          height: 5,
                        ),
                        getDetailsRow("Distance Fare", "\u20B90.00",context),
                        SizedBox(
                          height: 5,
                        ),
                        getDetailsRow("Tax", "\u20B90.00",context),
                        SizedBox(
                          height: 5,
                        ),
                        getDetailsRow("Tips", "\u20B90.00",context),
                        SizedBox(
                          height: 5,
                        ),
                        getDetailsRow("Waiting Charge", "\u20B90.00",context),
                        SizedBox(
                          height: 5,
                        ),
                        getDetailsRow("Convenience Charge", "\u20B95.00",context),
                        SizedBox(
                          height: 25,
                        ),
                        Row(
                          children: <Widget>[
                            Text(
                              "Total Amount",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 17),
                            ),
                            SizedBox(
                              width: 15,
                            ),
                            Expanded(
                              child: Text(
                                "\u20B9210.00",
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 17,
                                    color: App24Colors.lightBlue),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  )
                ],
              ),
            ),
          )
        ]),
      ),
      bottomNavigationBar: widget.type == "payment"
          ? Container(
              padding: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
              color: App24Colors.greenOrderEssentialThemeColor,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Amount to be paid",
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.w500),
                  ),
                  Text(
                    "\u20B9210.00",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w700,
                        fontSize: 22),
                  )
                ],
              ),
            )
          : null, // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
