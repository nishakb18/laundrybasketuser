import 'dart:math' show cos, sqrt, asin;
import 'dart:ui';

import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/constants/global_constants.dart';
import 'package:app24_user_app/models/location_model.dart';
import 'package:app24_user_app/modules/ride/confirm_ride_booking.dart';
import 'package:app24_user_app/modules/ride/ride_vehicle_model.dart';
import 'package:app24_user_app/providers/ride_page_variables_provider.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/widgets/bottomsheets/vehicle_details_bottomsheet.dart';
import 'package:app24_user_app/widgets/common_main_button.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:app24_user_app/widgets/offline_builder_widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

final rideVariablesNotifierProvider =
    StateNotifierProvider((ref) => RideVariablesNotifier());

class RidePage extends StatefulWidget {
  final LocationModel? initializedDropLocation;

  const RidePage({Key? key,  this.initializedDropLocation})
      : super(key: key);

  @override
  _RidePageState createState() => _RidePageState();
}

class _RidePageState extends State<RidePage> with TickerProviderStateMixin {
  CameraPosition _initialLocation =
      CameraPosition(target: LatLng(20.5937, 78.9629), zoom: 4.0);
  late GoogleMapController mapController;

  late LocationModel pickUpLocation;
  late LocationModel dropLocation;

  final pickUpAddressController = TextEditingController();
  final dropAddressController = TextEditingController();

  //late RideVariablesNotifier rideVariablesNotifier;

  Set<Marker> markers = {};
  Marker? pickUpMarker;
  Marker? dropMarker;

  PolylinePoints? polylinePoints;
  Map<PolylineId, Polyline>? polyLines = {};
  List<LatLng> polylineCoordinates = [];

  bool _showGoogleMaps = false;
  int selectedVehicleIndex = -1;
  VehicleServices? selectedVehicle;

  bool isPickUpLocationFetching = true;

  String distance = '';

  //String time = '15 mins'; we can get estimated time from polyline package from override function

  double totalDistance = 0.0;

  BitmapDescriptor? pickUpIcon;
  BitmapDescriptor? dropIcon;

  CarouselController vehicleCarouselController = CarouselController();

  @override
  void initState() {
    Future.delayed(const Duration(milliseconds: 500), () {
      setState(() {
        _showGoogleMaps = true;
        initializeDropLocation();
        initializePickUpLocation();
      });
    });
    super.initState();
  }

  loadData1({required BuildContext context, bool init = true}) {
    return context.read(rideVehicleListNotifier.notifier).getRideVehicles(
        init: init,
        lat: pickUpLocation.latitude!,
        long: pickUpLocation.longitude!);
  }

  selectLocation(String title) {
    showSelectLocationSheet(context: context, title: title).then((value) {
      if (value != null) {
        LocationModel model = value;
        setState(() {
          if (title == GlobalConstants.labelPickupLocation) {
            pickUpLocation = model;
            pickUpAddressController.text = model.description!;
            loadData1(context: context);
            updateMapWithLocations();
            setPickUpMarker();
          } else {
            dropLocation = model;
            dropAddressController.text = model.description!;
            updateMapWithLocations();
            setDropMarker();
          }
        });
      }
    });
  }

  initializeDropLocation() {
    dropLocation = widget.initializedDropLocation!;
    dropAddressController.text = dropLocation.description!;
    setDropMarker();
  }

  initializePickUpLocation() {
    getCurrentLocation().then((value) {
      //showLog(value);
      if (value != null && value.description!.isNotEmpty) {
        LocationModel locationModel = value;
        setState(() {
          pickUpLocation = locationModel;
          pickUpAddressController.text = pickUpLocation.description!;
          context.read(homeNotifierProvider.notifier).currentLocation =
              locationModel;
          isPickUpLocationFetching = false;
          setPickUpMarker();
          loadData1(context: context);
          updateMapWithLocations();
        });
      } else {
        setState(() {
          isPickUpLocationFetching = false;
        });
      }
    });
  }

  updateMapWithLocations() async {
    await updateCameraLocation(
        LatLng(pickUpLocation.latitude!, pickUpLocation.longitude!),
        LatLng(dropLocation.latitude!, dropLocation.longitude!),
        mapController);
    createPolyLines();
  }

  Future<void> setPickUpMarker() async {
    pickUpIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(
          devicePixelRatio: 2.5,
        ),
        App24UserAppImages.pickUpIcon);

    setState(() {
      pickUpMarker = Marker(
          markerId: MarkerId('pickup_marker'),
          position: LatLng(
            pickUpLocation.latitude!,
            pickUpLocation.longitude!,
          ),
          infoWindow: InfoWindow(
            title: 'Start',
            snippet: pickUpAddressController.text,
          ),
          icon: pickUpIcon!);
      if (markers.contains("pickup_marker")) {
        markers.remove("pickup_marker");
      }
      markers.add(pickUpMarker!);
    });
  }

  Future<void> setDropMarker() async {
    dropIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 2.5), App24UserAppImages.dropIcon);

    setState(() {
      dropMarker = Marker(
          markerId: MarkerId('drop_marker'),
          position: LatLng(
            dropLocation.latitude!,
            dropLocation.longitude!,
          ),
          infoWindow: InfoWindow(
            title: 'Drop',
            snippet: dropAddressController.text,
          ),
          icon: dropIcon!);
      if (markers.contains("drop_marker")) {
        markers.remove("drop_marker");
      }
      markers.add(dropMarker!);
    });
  }

  Future<void> updateCameraLocation(
    LatLng source,
    LatLng destination,
    GoogleMapController mapController,
  ) async {
    LatLngBounds bounds;

    if (source.latitude > destination.latitude &&
        source.longitude > destination.longitude) {
      bounds = LatLngBounds(southwest: destination, northeast: source);
    } else if (source.longitude > destination.longitude) {
      bounds = LatLngBounds(
          southwest: LatLng(source.latitude, destination.longitude),
          northeast: LatLng(destination.latitude, source.longitude));
    } else if (source.latitude > destination.latitude) {
      bounds = LatLngBounds(
          southwest: LatLng(destination.latitude, source.longitude),
          northeast: LatLng(source.latitude, destination.longitude));
    } else {
      bounds = LatLngBounds(southwest: source, northeast: destination);
    }

    CameraUpdate cameraUpdate = CameraUpdate.newLatLngBounds(bounds, 150);

    return checkCameraLocation(cameraUpdate, mapController);
  }

  Future<void> checkCameraLocation(
      CameraUpdate cameraUpdate, GoogleMapController mapController) async {
    mapController.animateCamera(cameraUpdate);
    LatLngBounds l1 = await mapController.getVisibleRegion();
    LatLngBounds l2 = await mapController.getVisibleRegion();

    if (l1.southwest.latitude == -90 || l2.southwest.latitude == -90) {
      return checkCameraLocation(cameraUpdate, mapController);
    }
  }

  void createPolyLines() async {
    // Initializing PolylinePoints
    polylinePoints = PolylinePoints();

    // Generating the list of coordinates to be used for
    // drawing the polyLines
    late PolylineResult result;
    late String mapKey;

    if (context.read(homeNotifierProvider.notifier).appConfiguration == null) {
      await context.read(homeNotifierProvider.notifier).getAppConfigurations();
    }

    mapKey = context.read(homeNotifierProvider.notifier).mapKey!;

    showLog("key is : $mapKey");

    result = await polylinePoints!.getRouteBetweenCoordinates(
      mapKey, // Google Maps API Key
      PointLatLng(pickUpLocation.latitude!, pickUpLocation.longitude!),
      PointLatLng(dropLocation.latitude!, dropLocation.longitude!),
    );

    // Adding the coordinates to the list
    if (result.points.isNotEmpty) {
      polylineCoordinates.clear();
      result.points.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
      getDistance();
    }

    // Defining an ID
    PolylineId id = PolylineId('poly');

    // Initializing Polyline
    Polyline polyline = Polyline(
      polylineId: id,
      color: Colors.black87,
      points: polylineCoordinates,
      width: 3,
    );

    // Adding the polyline to the map
    setState(() {
      polyLines!.clear();
      polyLines![id] = polyline;
    });
  }

  List<Widget> getVehicleList(context, List<VehicleServices>? rideVehicles) {
    List<Widget> vehicleSliders = [];

    for (var i = 0; i < rideVehicles!.length; i++) {
      vehicleSliders.add(Container(
          margin: EdgeInsets.symmetric(vertical: 5, horizontal: 0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: i == selectedVehicleIndex
                ? App24Colors.darkTextColor
                : Colors.transparent,
            boxShadow: [
              BoxShadow(
                  color: i == selectedVehicleIndex
                      ? Colors.grey[200]!
                      : Colors.transparent,
                  blurRadius: 2,
                  spreadRadius: 2,
                  offset: Offset(3, 3)),
            ],
          ),
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              borderRadius: BorderRadius.circular(20),
              onTap: () {
                showVehicleDetailBottomSheet(
                    context: context, vehicleDetail: rideVehicles[i]);
                setState(() {
                  selectedVehicleIndex = i;
                  selectedVehicle = rideVehicles[i];
                });
              },
              onDoubleTap: () {
                showVehicleDetailBottomSheet(
                    context: context, vehicleDetail: rideVehicles[i]);
              },
              onLongPress: () {
                showVehicleDetailBottomSheet(
                    context: context, vehicleDetail: rideVehicles[i]);
              },
              child: Container(
                  padding: EdgeInsets.all(3),
                  child: IntrinsicHeight(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        const SizedBox(
                          height: 5,
                        ),
                        Text(
                          rideVehicles[i].estimatedTime!,
                          style: TextStyle(
                              color: i == selectedVehicleIndex
                                  ? App24Colors.yellowRideThemeColor
                                  : App24Colors.darkTextColor,
                              fontWeight: FontWeight.w500,
                              fontSize: 12),
                          textAlign: TextAlign.center,
                          maxLines: 1,
                        ),
                        const SizedBox(
                          height: 8,
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 5),
                          height: 50,
                          width: 100,
                          child: Hero(
                            tag: rideVehicles[i].id!,
                            child: CachedNetworkImage(
                              imageUrl: rideVehicles[i].vehicleImage ?? '',
                              fit: BoxFit.contain,
                              color: i == selectedVehicleIndex
                                  ? App24Colors.yellowRideThemeColor
                                  : App24Colors.darkTextColor,
                              placeholder: (context, url) => Image.asset(
                                App24UserAppImages.placeHolderImage,
                                fit: BoxFit.contain,
                              ),
                              errorWidget: (context, url, error) => Image.asset(
                                App24UserAppImages.placeHolderImage,
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 7,
                        ),
                        Text(
                          rideVehicles[i].vehicleName!,
                          style: TextStyle(
                              color: i == selectedVehicleIndex
                                  ? App24Colors.yellowRideThemeColor
                                  : App24Colors.darkTextColor,
                              fontWeight: FontWeight.w500),
                          textAlign: TextAlign.center,
                          maxLines: 1,
                        ),
                      ],
                    ),
                  )),
            ),
          )));
    }
    return vehicleSliders;
  }

  void showVehicleDetailBottomSheet(
      {required BuildContext context, required VehicleServices vehicleDetail}) {
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        builder: (BuildContext bc) {
          return VehicleDetailsBottomSheet(vehicleDetail: vehicleDetail);
        });
  }

  double _coordinateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return 12742 * asin(sqrt(a));
  }

  void getDistance() {
    totalDistance = 0.0;
    for (int i = 0; i < polylineCoordinates.length - 1; i++) {
      totalDistance += _coordinateDistance(
        polylineCoordinates[i].latitude,
        polylineCoordinates[i].longitude,
        polylineCoordinates[i + 1].latitude,
        polylineCoordinates[i + 1].longitude,
      );
    }
    showLog(totalDistance.toStringAsFixed(2) + "calculated distance");
    setState(() {
      distance = totalDistance.toStringAsFixed(2) + " Kms";
    });
  }

  getPricingButtonClicked() {
    if (selectedVehicleIndex != -1) {
      //Map rideDetails = {};



       context
          .read(rideVehiclesRequestNotifier.notifier)
          .sendRideRequest(
          serviceType: selectedVehicle!.id!.toInt(),
          sLong:pickUpLocation.longitude!.toDouble(),
          sLat:pickUpLocation.latitude!.toDouble(),
          dLong: dropLocation.longitude!.toDouble(),
          dLat: dropLocation.latitude!.toDouble(),
          dAddress: dropAddressController.text,
          estimatedKm: distance,
          sAddress: pickUpAddressController.text,
          init:true );



         Navigator.push(
      context,
        MaterialPageRoute(
       builder: (context) => ConfirmRideBookingPage(dropLocation: dropLocation,pickUpLocation: pickUpLocation,
                selectedVehicle: selectedVehicle!.vehicleName,selectedVehicleId: selectedVehicle!.id,
                estimatedKm: distance,
            pickUpAddress: pickUpAddressController.text,
        dropAddress: dropAddressController.text,
            )),
       );
    } else {
      showMessage(context, "Please select Vehicle!", true,false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: Consumer(builder: (context, watch, child) {
          //rideVariablesNotifier = watch(rideVariablesNotifierProvider.notifier);
          return SafeArea(
            child: OfflineBuilderWidget(
              Column(
                children: [
                  Expanded(
                    child: Stack(
                      children: <Widget>[
                        _showGoogleMaps
                            ? Container(
                                child: GoogleMap(
                                markers: Set<Marker>.from(markers),
                                initialCameraPosition: _initialLocation,
                                myLocationEnabled: true,
                                myLocationButtonEnabled: false,
                                mapType: MapType.normal,
                                zoomGesturesEnabled: true,
                                zoomControlsEnabled: false,
                                polylines: Set<Polyline>.of(polyLines!.values),
                                onMapCreated: (GoogleMapController controller) {
                                  mapController = controller;
                                  //_getCurrentLocation();
                                },
                              ))
                            : Center(
                                child: CupertinoActivityIndicator(
                                  radius: 20,
                                ),
                              ),
                        Container(
                          margin: EdgeInsets.only(top: 20, left: 20, right: 20),
                          child: FloatingActionButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: Icon(Icons.arrow_back),
                            mini: true,
                            backgroundColor: Theme.of(context).colorScheme.secondary,
                          ),
                        ),
                        Positioned(
                            bottom: 10,
                            right: 10,
                            child: distance == ""
                                ? Container()
                                : Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(7),
                                        color: Theme.of(context).colorScheme.secondary,
                                        boxShadow: [
                                          BoxShadow(
                                              color: Colors.grey.withOpacity(0.4),
                                              blurRadius: 1,
                                              spreadRadius: 1,
                                              offset: Offset(1, 1))
                                        ]),
                                    padding: EdgeInsets.symmetric(
                                        vertical: 10, horizontal: 15),
                                    child: Text(
                                      "${distance} away",
                                      style:
                                          TextStyle(fontWeight: FontWeight.w500),
                                    ),
                                  )),
                      ],
                    ),
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.symmetric(vertical: 10),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(40),
                                  topLeft: Radius.circular(40)),
                              color: Colors.white),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              const SizedBox(
                                height: 7,
                              ),
                              Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 20),
                                  child: Container(
                                    color: Colors.white,
                                    child: Row(
                                      children: <Widget>[
                                        Column(
                                          children: <Widget>[
                                            Container(
                                              decoration: BoxDecoration(
                                                  color: Theme.of(context)
                                                      .primaryColor,
                                                  shape: BoxShape.circle,
                                                  border: Border.all(
                                                      color: Theme.of(context)
                                                          .primaryColorLight,
                                                      width: 5.0)),
                                              padding: EdgeInsets.all(5),
                                            ),
                                            const SizedBox(
                                              height: 35,
                                              child: VerticalDivider(
                                                thickness: 1.3,
                                              ),
                                            ),
                                            Container(
                                              decoration: BoxDecoration(
                                                  color: Colors.red[500],
                                                  shape: BoxShape.circle,
                                                  border: Border.all(
                                                      color: Colors.red[200]!,
                                                      width: 5.0)),
                                              padding: EdgeInsets.all(5),
                                            ),
                                          ],
                                        ),
                                        const SizedBox(
                                          width: 10,
                                        ),
                                        Expanded(
                                          child: Column(
                                            children: <Widget>[
                                              TextField(
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w500),
                                                controller:
                                                    pickUpAddressController,
                                                decoration: getDecoration(context)
                                                    .copyWith(
                                                  labelText: GlobalConstants
                                                      .labelPickupLocation,
                                                  suffixIcon: isPickUpLocationFetching
                                                      ? CupertinoActivityIndicator(
                                                          radius: 10,
                                                        )
                                                      : Icon(Icons.search),
                                                ),
                                                readOnly: true,
                                                onTap: () {
                                                  selectLocation(GlobalConstants
                                                      .labelPickupLocation);
                                                },
                                              ),
                                              const SizedBox(
                                                height: 7,
                                              ),
                                              TextField(
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w500),
                                                controller: dropAddressController,
                                                decoration: getDecoration(context)
                                                    .copyWith(
                                                  labelText: GlobalConstants
                                                      .labelDropLocation,
                                                  suffixIcon: Icon(Icons.search),
                                                ),
                                                readOnly: true,
                                                onTap: () {
                                                  selectLocation(GlobalConstants
                                                      .labelDropLocation);
                                                },
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  )),
                              const SizedBox(
                                height: 5,
                              ),
                              if (pickUpAddressController.text.isNotEmpty)
                                Container(
                                  child: Row(children: [
                                    Container(
                                      padding: EdgeInsets.all(5),
                                      margin: EdgeInsets.symmetric(horizontal: 5),
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Theme.of(context).colorScheme.secondary),
                                      child: Material(
                                        color: Colors.transparent,
                                        child: InkWell(
                                          customBorder: CircleBorder(),
                                          onTap: () {
                                            vehicleCarouselController
                                                .previousPage(
                                                    duration: Duration(
                                                        milliseconds: 300),
                                                    curve: Curves.linear);
                                          },
                                          child: Icon(
                                            Icons.arrow_back_ios,
                                            size: 15,
                                            color: App24Colors
                                                .greenOrderEssentialThemeColor,
                                          ),
                                        ),
                                      ),
                                    ),
                                    /*if(pickUpAddressController.text.isNotEmpty)*/
                                    Expanded(child: Consumer(
                                        builder: (context, watch, child) {
                                      final state =
                                          watch(rideVehicleListNotifier);
                                      if (state.isLoading) {
                                        return Center(
                                            child: CircularProgressIndicator(
                                          color: App24Colors.yellowRideThemeColor,
                                        ));
                                      } else if (state.isError) {
                                        {
                                          return LoadingError(
                                            onPressed: (res) {
                                              loadData1(init: true, context: res);
                                            },
                                            message: state.errorMessage,
                                          );
                                        }
                                      } else {
                                        return Container(
                                          child: CarouselSlider(
                                            carouselController:
                                                vehicleCarouselController,
                                            items: getVehicleList(
                                                context,
                                                state.response.responseData
                                                    .services),
                                            options: CarouselOptions(
                                              height: 120,
                                              autoPlay: false,
                                              viewportFraction: 0.40,
                                              initialPage: 1,
                                              enableInfiniteScroll: false,
                                            ),
                                          ),
                                        );
                                      }
                                    })),
                                    Container(
                                      padding: EdgeInsets.all(5),
                                      margin: EdgeInsets.symmetric(horizontal: 5),
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Theme.of(context).colorScheme.secondary),
                                      child: Material(
                                        color: Colors.transparent,
                                        child: InkWell(
                                          customBorder: CircleBorder(),
                                          onTap: () {
                                            vehicleCarouselController.nextPage(
                                                duration:
                                                    Duration(milliseconds: 300),
                                                curve: Curves.linear);
                                          },
                                          child: Icon(
                                            Icons.arrow_forward_ios,
                                            size: 15,
                                            color: App24Colors
                                                .greenOrderEssentialThemeColor,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ]),
                                ),
                              const SizedBox(
                                height: 10,
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 20),
                                child: CommonButton(
                                  onTap: () {
                                    getPricingButtonClicked();
                                  },
                                  bgColor: App24Colors.yellowRideThemeColor,
                                  text: "Get Pricing",
                                ),
                              )
                            ],
                          ))
                    ],
                  )
                ],
              ),
            ),
          );
        }) // This trailing comma makes auto-formatting nicer for build methods.
        );
  }
}
