// import 'package:app24_user_app/constants/asset_path.dart';
// import 'package:app24_user_app/common_CommonHelpers.dart';
// import 'package:app24_user_app/modules/ride/track_ride.dart';
// import 'package:app24_user_app/widgets/common_appbar.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
//
// class ChooseDriverPage extends StatefulWidget {
//   @override
//   _ChooseDriverPageState createState() => _ChooseDriverPageState();
// }
//
// class _ChooseDriverPageState extends State<ChooseDriverPage> {
//   List<Driver> _driverList = [];
//   int selectedDriver = -1;
//
//   @override
//   void initState() {
//     super.initState();
//
//     _driverList
//         .add(Driver(name: "Driver 1", distance: "10 Km", fare: "150.00"));
//     _driverList
//         .add(Driver(name: "Sathish Kumar", distance: "<1 Km", fare: "120.00"));
//     _driverList
//         .add(Driver(name: "Driver 3", distance: "2.4 Km", fare: "100.00"));
//     _driverList.add(Driver(name: "Santhosh", distance: "5 Km", fare: "180.00"));
//     _driverList
//         .add(Driver(name: "Driver 3", distance: "2.4 Km", fare: "100.00"));
//     _driverList.add(Driver(name: "Santhosh", distance: "5 Km", fare: "180.00"));
//     _driverList
//         .add(Driver(name: "Driver 3", distance: "2.4 Km", fare: "100.00"));
//     _driverList.add(Driver(name: "Santhosh", distance: "5 Km", fare: "180.00"));
//   }
//
//   void cancelRide() {
//     showDialog(
//       context: context,
//       // barrierDismissible: false,
//       builder: (_) {
//         return CancellationAlertBox();
//       },
//     );
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         appBar: CustomAppBar(
//           title: "Choose Driver",
//         ),
//         resizeToAvoidBottomInset: true,
//         backgroundColor: Colors.white,
//         body: SafeArea(
//           child: Column(
//               crossAxisAlignment: CrossAxisAlignment.stretch,
//               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//               children: <Widget>[
//                 const SizedBox(
//                   height: 10,
//                 ),
//                 Expanded(
//                   child: SingleChildScrollView(
//                     child: Column(
//                         crossAxisAlignment: CrossAxisAlignment.stretch,
//                         children: <Widget>[
//                           const SizedBox(
//                             height: 10,
//                           ),
//                           Padding(
//                             padding: EdgeInsets.symmetric(horizontal: 20),
//                             child: Text(
//                               "Please choose your driver from the available list. Note that the price is not decided by App24.",
//                               textAlign: TextAlign.center,
//                               style: TextStyle(
//                                   color: Colors.black45,
//                                   fontWeight: FontWeight.w500,
//                                   fontSize: 14),
//                             ),
//                           ),
//                           SizedBox(
//                             height: 20,
//                           ),
//                           Padding(
//                             padding: EdgeInsets.symmetric(horizontal: 20),
//                             child:
//                                 CommonHelpers().getSubTitle("Drivers List", context),
//                           ),
//                           SizedBox(
//                             height: 5,
//                           ),
//                           ListView.separated(
//                               shrinkWrap: true,
//                               padding: EdgeInsets.all(0),
//                               physics: NeverScrollableScrollPhysics(),
//                               itemBuilder: (context, index) {
//                                 return Material(
//                                   color: Colors.transparent,
//                                   child: ListTile(
//                                     shape: RoundedRectangleBorder(
//                                         borderRadius:
//                                             BorderRadius.circular(10)),
//                                     onTap: () {
//                                       setState(() {
//                                         selectedDriver = index;
//                                       });
//                                     },
//                                     leading: Icon(
//                                       selectedDriver == index
//                                           ? Icons.check_circle
//                                           : Icons.radio_button_unchecked,
//                                       color: App24Colors.greenOrderEssentialThemeColor,
//                                     ),
//                                     title: Text(
//                                       _driverList[index].name,
//                                       style: TextStyle(
//                                           fontWeight: FontWeight.w500),
//                                     ),
//                                     subtitle: Text(
//                                       _driverList[index].distance + " away",
//                                       style: TextStyle(
//                                           fontWeight: FontWeight.w500),
//                                     ),
//                                     trailing: Text(
//                                       "\u20B9${_driverList[index].fare}",
//                                       style: TextStyle(
//                                           fontWeight: FontWeight.w500,
//                                           fontSize: 15),
//                                     ),
//                                   ),
//                                 );
//                               },
//                               separatorBuilder: (context, index) {
//                                 return Padding(
//                                     padding:
//                                         EdgeInsets.symmetric(horizontal: 10),
//                                     child: Divider(
//                                       height: 1,
//                                     ));
//                               },
//                               itemCount: _driverList.length)
//                         ]),
//                   ),
//                 ),
//                 Column(
//                   crossAxisAlignment: CrossAxisAlignment.stretch,
//                   children: <Widget>[
//                     Divider(
//                       height: 1,
//                     ),
//                     SizedBox(
//                       height: 10,
//                     ),
//                     Padding(
//                       padding: EdgeInsets.symmetric(horizontal: 20),
//                       child: MaterialButton(
//                           onPressed: () {
//                             Navigator.push(
//                               context,
//                               MaterialPageRoute(
//                                   builder: (context) => TrackRidePage()),
//                             );
//                           },
//                           color: App24Colors.greenOrderEssentialThemeColor,
//                           shape: RoundedRectangleBorder(
//                               borderRadius: BorderRadius.circular(10)),
//                           child: Padding(
//                             padding: EdgeInsets.symmetric(
//                                 vertical: 15, horizontal: 20),
//                             child: Text(
//                               "RIDE NOW",
//                               style: TextStyle(
//                                   color: Colors.white,
//                                   fontWeight: FontWeight.bold),
//                             ),
//                           )),
//                     ),
//                     SizedBox(
//                       height: 10,
//                     ),
//                     Padding(
//                       padding: EdgeInsets.symmetric(horizontal: 20),
//                       child: MaterialButton(
//                           onPressed: () {
//                             /* Navigator.push(
//                         context,
//                         MaterialPageRoute(
//                             builder: (context) => OTPVerificationPage()),
//                       );*/
//                             cancelRide();
//                           },
//                           color: Colors.redAccent,
//                           shape: RoundedRectangleBorder(
//                               borderRadius: BorderRadius.circular(10)),
//                           child: Padding(
//                             padding: EdgeInsets.symmetric(
//                                 vertical: 15, horizontal: 20),
//                             child: Text(
//                               "CANCEL",
//                               style: TextStyle(
//                                   color: Colors.white,
//                                   fontWeight: FontWeight.bold),
//                             ),
//                           )),
//                     ),
//                     SizedBox(
//                       height: 10,
//                     ),
//                   ],
//                 ),
//               ]),
//         ) // This trailing comma makes auto-formatting nicer for build methods.
//         );
//   }
// }
//
// class Driver {
//   String name, distance, fare;
//
//   Driver({this.name, this.distance, this.fare});
// }
