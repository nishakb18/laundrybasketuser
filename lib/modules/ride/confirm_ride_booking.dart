import 'dart:async';

import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/helpers.dart';
import 'package:app24_user_app/models/location_model.dart';
import 'package:app24_user_app/modules/ride/ride_vehicle_model.dart';
import 'package:app24_user_app/modules/ride/rider_detail_model.dart';
import 'package:app24_user_app/modules/ride/track_ride.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/utils/services/socket/socket_service.dart';
import 'package:app24_user_app/widgets/bottomsheets/booking_for_someone.dart';
import 'package:app24_user_app/widgets/bottomsheets/cancellation_bottomSheet.dart';
import 'package:app24_user_app/widgets/bottomsheets/change_payment_bottomSheet.dart';
import 'package:app24_user_app/widgets/common_appbar.dart';
import 'package:app24_user_app/widgets/common_main_button.dart';
import 'package:app24_user_app/widgets/loading_error.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:flutter_riverpod/src/provider.dart';
import 'package:loading_animations/loading_animations.dart';
import 'package:socket_io_client/socket_io_client.dart';

class ConfirmRideBookingPage extends StatefulWidget {
  const ConfirmRideBookingPage({
    Key? key,
    this.selectedVehicle,
    this.estimatedKm,
    this.pickUpAddress,
    this.dropAddress,
    this.pickUpLocation,
    this.dropLocation,
    this.selectedVehicleId,
  }) : super(key: key);
  final String? selectedVehicle;
  final int? selectedVehicleId;
  final String? estimatedKm;
  final String? pickUpAddress;
  final String? dropAddress;
  final LocationModel? pickUpLocation;
  final LocationModel? dropLocation;

  @override
  _ConfirmRideBookingPageState createState() => _ConfirmRideBookingPageState();
}

class _ConfirmRideBookingPageState extends State<ConfirmRideBookingPage> {
  List<Driver> _driverList = [];

  int selectDriver = -1;
  bool _isBookedForSomeone = false;
  String _isUseWallet = "Cash";
  TextEditingController _nameController = new TextEditingController();
  TextEditingController _phoneController = new TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  int? rideId;
  int? providerId;
  dynamic providerRating;
  String? providerVehicleNumber;
  String? providerNumber;
  String? providerVehicleModel;


//   void changePaymentMode() {
//     showDialog(
//       context: context,
// // barrierDismissible: false,
//       builder: (_) {
//         return PaymentChangeAlertBox();
//       },
//     );
//   }

  // void confirmBookForSomeone() {
  //   if (_formKey.currentState!.validate()) {
  //     setState(() {
  //       _isBookedForSomeone = true;
  //       Navigator.pop(context);
  //     });
  //   }
  // }

  @override
  void initState() {
    super.initState();
    context.read(rideVehiclesRequestNotifier.notifier).getRiderDetails();
    SchedulerBinding.instance!.addPostFrameCallback((_) {
      _socketService.listenSocket();
    });

    super.initState();

  }

  Timer? timer;
  String? riderName;
  int? riderId;
  double? pickUpLat;
  double? pickUpLng;
  double? dropLng;
  double? dropLat;
  late final SocketService _socketService = SocketService.instance;

  emitSocket({required int orderID}) async {
    showLog("Socket emitting");
    try {
      Socket? socket = await _socketService.socket;
      socket!.emit('joinPrivateRoom', {'room_1_R${orderID}_TRANSPORT'});
      showLog("Socket emitted");
    } catch (e) {
      showLog("error in socket is : ${e.toString()}");
    }
  }
  autoClickButton() {
    MaterialButton buttons = MaterialButton(onPressed: () { print("asd"); },);
    buttons.onPressed!();
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  List<Widget> getRideDetails(context, RiderResponseData riderResponse) {
    List<Data>? data = riderResponse.data;
    print(data!.length);
    print(riderResponse.data![0].status + "status of ride");


    pickUpLat = data[0].sLatitude;
    pickUpLng = data[0].sLongitude;
    dropLat = data[0].dLatitude;
    dropLng = data[0].dLongitude;
    // List<ProviderBiddingDetails>? providerBiddingDetails ;
    List<Widget> _riderDetails = [];
    for (var index = 0; index < riderResponse.data!.length; index++) {
      rideId = riderResponse.data![index].id;
      print(riderResponse.data![index].status + "fromGetRiderDetails");
     /* if(riderResponse.data![0].status != "BIDDING" || riderResponse.data![0].status != null)
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => TrackRidePage(
                pickUpAddress:
                widget.pickUpAddress,
                dropAddress:
                widget.dropAddress,
                selectedVehicle:
                widget.selectedVehicle,
                estimatedKm:
                widget.estimatedKm,
                providerName: riderName,
                providerId: providerId,
                dropLat: dropLat,
                dropLng: dropLng,
                pickUpLat: pickUpLat,
                pickUpLng: pickUpLng,
              )),
        );*/
      // if(riderResponse.data![index].status == "")
      for (var i = 0;
          i < riderResponse.data![index].providerBiddingDetails!.length;
          i++) {
        // providerId = riderResponse
        //     .data![index].providerBiddingDetails![i].providerId;
        print(i);
        _riderDetails.add(Padding(
          padding: const EdgeInsets.only(left: 1, top: 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Material(
                child: ListTile(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  onTap: () {
                    setState(() {
                      riderName = riderResponse
                          .data![index].providerBiddingDetails![i].providerName;
                      providerId = riderResponse
                          .data![index].providerBiddingDetails![i].providerId;
                      providerNumber = riderResponse.data![index]
                          .providerBiddingDetails![i].providerMobile;
                      providerRating = riderResponse.data![index]
                          .providerBiddingDetails![i].providerRating;
                      providerVehicleNumber = riderResponse
                          .data![index].providerBiddingDetails![i].vehicleNo;
                      providerVehicleModel = riderResponse
                          .data![index].providerBiddingDetails![i].vehicleModel;

                      // paymentMode = "cash";
                    });
                  },
                  leading: Icon(
                    riderName ==
                            riderResponse.data![index]
                                .providerBiddingDetails![i].providerName
                        ? Icons.check_circle
                        : Icons.radio_button_unchecked,
                    color: App24Colors.darkTextColor,
                  ),
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(riderResponse
                          .data![index].providerBiddingDetails![i].providerName
                          .toString()),
                      Text(riderResponse
                          .data![index].providerBiddingDetails![i].eta
                          .toString())
                    ],
                  ),
                  trailing: Text(double.parse(riderResponse
                          .data![index].providerBiddingDetails![i].bidAmount
                          .toString())
                      .toString()),
                ),
              ),

              const SizedBox(
                height: 20,
              ),
              // MaterialButton(
              //   onPressed: () {},
              //   padding: EdgeInsets.symmetric( vertical: 15),
              //   shape: RoundedRectangleBorder(
              //       borderRadius: BorderRadius.circular(15)),
              //   color: App24Colors.yellowRideThemeColor,
              //   child: Text(
              //     "Select",
              //     style: TextStyle(color: Colors.white),
              //   ),
              // )
            ],
          ),
        ));
      }
    }
    if (_riderDetails.isEmpty) {
      _riderDetails.add(Padding(
          padding: const EdgeInsets.only(left: 1, top: 10),
          child: Container(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              width: MediaQuery.of(context).size.width / 2.3,
              /*decoration: BoxDecoration(
    borderRadius: BorderRadius.circular(15),
    ),*/
              child: Text("No item found!"))));
    }

    return _riderDetails;
  }

  String? bookForSomeone;

  void bookingForSomeone(BuildContext context) async {
    final result = await Helpers().getCommonBottomSheet(
        context: context,
        content: BookingForSomeone(),
        title: "Booking For Someone");
    if (result != null) {
      setState(() {
        bookForSomeone = result['name'];
        _isBookedForSomeone = true;
      });
    }
  }
  statusOfRide (RiderResponseData responseData) {
    print("statusOfRide entered");
    print(responseData.data![0].status);
    context.read(rideVehiclesRequestNotifier.notifier).getRiderDetails();
    getRideDetails(context,responseData);
    print(responseData.data![0].status);
    if(responseData.data![0].status == "STARTED")
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => TrackRidePage(
              pickUpAddress:
              widget.pickUpAddress,
              dropAddress:
              widget.dropAddress,
              selectedVehicle:
              widget.selectedVehicle,
              estimatedKm:
              widget.estimatedKm,
              providerName: riderName,
              providerId: providerId,
              dropLat: dropLat,
              dropLng: dropLng,
              pickUpLat: pickUpLat,
              pickUpLng: pickUpLng,
            )),
      );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomAppBar(
          hideCartIcon: "true",
          title: "Confirm Booking",
        ),
        resizeToAvoidBottomInset: true,
        backgroundColor: Colors.white,
        body: ProviderListener(
          provider: rideVehiclesRequestNotifier,
          onChange: (context, dynamic state) {
            if (state.isLoading!) {
              showProgress(context);
            } else if (state.isError!) {
              Navigator.pop(context);
              showMessage(context, state.errorMessage!, true, true);
            } else {
              // Navigator.pop(context);
              print("provider listner worked");
              print(state.response.responseData.data![0].id);
              emitSocket(orderID: state.response.responseData.data![0].id);
              _socketService.listenSocket();
              // reloadData(context: context, init: false);
              // loadData();
            }
          },
          child: SafeArea(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  /* SizedBox(
                    height: 20,
                  ),*/
                  Container(
                    padding: EdgeInsets.only(right: 20, left: 20, top: 20),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                    ),
                    // padding: EdgeInsets.all(15),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Text(
                              "Estimated KM",
                              style: TextStyle(
                                  fontWeight: FontWeight.w500, fontSize: 15),
                            ),
                            SizedBox(
                              width: 15,
                            ),
                            Expanded(
                              child: Text(
                                widget.estimatedKm.toString(),
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    color: App24Colors.yellowRideThemeColor,
                                    fontSize: 15),
                              ),
                            )
                          ],
                        ),

                        /*      SizedBox(
                          height: 5,
                        ),
                      Row(
                          children: <Widget>[
                            Text(
                              "Standard Fare",
                              style: TextStyle(
                                  fontWeight: FontWeight.w500, fontSize: 15),
                            ),
                            SizedBox(
                              width: 15,
                            ),
                            Expanded(
                              child: Text(
                                "\u20B9105.80",
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    color: App24Colors.yellowRideThemeColor,
                                    fontSize: 15),
                              ),
                            )
                          ],
                        ),*/
                        SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: <Widget>[
                            Text(
                              "Vehicle Model",
                              style: TextStyle(
                                  fontWeight: FontWeight.w500, fontSize: 15),
                            ),
                            SizedBox(
                              width: 15,
                            ),
                            Expanded(
                              child: Text(
                                widget.selectedVehicle.toString(),
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    color: App24Colors.yellowRideThemeColor,
                                    fontSize: 15),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Divider(),
                      ],
                    ),
                  ),
                  // if(selectDriver != -1)
                  SizedBox(
                    height: 10,
                  ),

                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Text(
                      // "Please negotiate your price with the service provider. Note that the price is not decided by App24.",
                      "Please select any from below list and confirm the booking",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.black45,
                          fontWeight: FontWeight.w500,
                          fontSize: 14),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      const SizedBox(
                        width: 40,
                      ),
                      Text(
                        "Choose Driver",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 16),
                      ),
                      const SizedBox(
                        width: 85,
                      ),
                      Text(
                        "ETA",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 16),
                      ),
                      const SizedBox(
                        width: 55,
                      ),
                      Text(
                        "Quote",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 16),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Consumer(builder: (context, watch, child) {
                    final state = watch(rideVehiclesRequestNotifier);
                    if (state.isLoading) {
                      return CircularProgressIndicator.adaptive();
                    } else if (state.isError) {
                      return LoadingError(
                        onPressed: (res) {
                          // reloadData(init: true, context: res);
                        },
                        message: state.errorMessage,
                      );
                    } else {
                      RiderResponseData riderResponseData =
                          state.response.responseData;
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Column(
                            children: getRideDetails(
                                context, state.response.responseData),
                          ),

                          ListView.separated(
                            shrinkWrap: true,
                            padding: EdgeInsets.all(0),
                            physics: NeverScrollableScrollPhysics(),
                            itemBuilder: (context, index) {
                              return Visibility(
                                /* maintainAnimation: false,
                        maintainSize: true,
                        maintainState: false,*/
                                replacement: Material(
                                  color: Colors.transparent,
                                  child: ListTile(
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(10)),
                                    onTap: () {
                                      setState(() {
                                        //if (selectDriver == -1) {
                                        selectDriver = index;
                                        //  } else {
                                        //    selectDriver = -1;
                                        //  }

                                        print(selectDriver);
                                      });
                                    },
                                    leading: Icon(
                                      selectDriver == index
                                          ? Icons.check_circle
                                          : Icons.radio_button_unchecked,
                                      color: Colors.grey[400],
                                    ),
                                    title: Row(
                                      children: [
                                        Expanded(
                                            child: Text(
                                          _driverList[index].driverName!,
                                          style: TextStyle(
                                              color: Colors.grey[400],
                                              fontWeight: FontWeight.w500),
                                        )),
                                        Expanded(
                                          child: Text(
                                            _driverList[index].driverEta!,
                                            style: TextStyle(
                                                color: Colors.grey[400],
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                        Expanded(
                                          child: Text(
                                            _driverList[index].driverQuote!,
                                            style: TextStyle(
                                                color: Colors.grey[400],
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                visible: /*selectDriver == -1
                            ? true
                            :*/
                                    selectDriver == index ? true : false,
                                child: Material(
                                  color: Colors.transparent,
                                  child: ListTile(
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(10)),
                                    onTap: () {
                                      setState(() {
                                        if (selectDriver == -1) {
                                          selectDriver = index;
                                        } else {
                                          selectDriver = -1;
                                        }

                                        print(selectDriver);
                                      });
                                    },
                                    leading: Icon(
                                      selectDriver == index
                                          ? Icons.check_circle
                                          : Icons.radio_button_unchecked,
                                      color: App24Colors.darkTextColor,
                                    ),
                                    title: Row(
                                      children: [
                                        Expanded(
                                            child: Text(
                                          _driverList[index].driverName!,
                                          style: TextStyle(
                                              color: App24Colors.darkTextColor,
                                              fontWeight: FontWeight.w500),
                                        )),
                                        Expanded(
                                          child: Text(
                                            _driverList[index].driverEta!,
                                            style: TextStyle(
                                                color:
                                                    App24Colors.darkTextColor,
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                        Expanded(
                                          child: Text(
                                            _driverList[index].driverQuote!,
                                            style: TextStyle(
                                                color: App24Colors
                                                    .yellowRideThemeColor,
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            },
                            separatorBuilder: (context, index) {
                              if (selectDriver == -1) {
                                return Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 10),
                                    child: Divider(
                                      height: 1,
                                    ));
                              } else {
                                return Container();
                              }
                            },
                            itemCount: _driverList.length,
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          // if(selectDriver != -1)

                          //  if(selectDriver != -1)
/*                Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Text(
                      "Mode of Payment",
                      style: TextStyle(
                          color: App24Colors.darkTextColor,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                  )*/
                          // SizedBox(
                          //   height: 5,
                          // ),
                          //  if(selectDriver != -1)
/*                Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      // color: Theme.of(context).accentColor
                    ),
                    child: IntrinsicHeight(
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                  child: Material(
                                      color: Colors.transparent,
                                      child: InkWell(
                                        borderRadius: BorderRadius.circular(15),
                                        onTap: () {
                                          setState(() {
                                            _isUseWallet = "Cash";
                                          });
                                          // Helpers().getCommonBottomSheet(
                                          //     context: context,
                                          //     content: ChangePaymentBottomSheet(),
                                          //     title: "Change payment method");
                                        },
                                        // onLongPress: () {
                                        //   Helpers().getCommonBottomSheet(
                                        //       context: context,
                                        //       content: ChangePaymentBottomSheet(),
                                        //       title: "Change payment method");
                                        // },
                                        child: Padding(
                                          padding: EdgeInsets.symmetric(
                                              vertical: 15, horizontal: 10),
                                          child: Row(
                                            children: <Widget>[
                                              SizedBox(
                                                width: 10,
                                              ),
                                              Icon(
                                                _isUseWallet == "Cash"
                                                    ? Icons.check_circle
                                                    : Icons
                                                        .radio_button_unchecked,
                                                color: App24Colors.darkTextColor,
                                              ),
                                              SizedBox(
                                                width: 10,
                                              ),
                                              Expanded(
                                                child: Text(
                                                  "Cash",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      fontSize: 16),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      )))
                            ],
                          )),
                        //  if(selectDriver != -1)
                          VerticalDivider(
                            width: 1,
                          ),
                          Expanded(
                              child: Material(
                                  color: Colors.transparent,
                                  child: InkWell(
                                    borderRadius: BorderRadius.circular(15),
                                    onTap: () {
                                      setState(() {
                                        _isUseWallet = "Wallet";
                                      });
                                    },
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(
                                          vertical: 15, horizontal: 10),
                                      child: Row(
                                        children: <Widget>[
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Icon(
                                            _isUseWallet == "Wallet"
                                                ? Icons.check_circle
                                                : Icons.radio_button_unchecked,
                                            color: App24Colors.darkTextColor,
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Expanded(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.stretch,
                                              children: <Widget>[
                                                Text(
                                                  "Use Wallet",
                                                  style: TextStyle(
                                                      fontWeight: FontWeight.w600,
                                                      fontSize: 16),
                                                ),
                                                Text(
                                                  "\u20B9",
                                                  style: TextStyle(
                                                      color: App24Colors
                                                          .yellowRideThemeColor,
                                                      fontWeight:
                                                          FontWeight.w500),
                                                ),
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ))),
                        ],
                      ),
                    ),
                  )*/
                          // if(selectDriver != -1)
                          SizedBox(
                            height: 10,
                          ),
                          //  if(selectDriver != -1)
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 20),
                            child: Material(
                              color: Colors.transparent,
                              child: ListTile(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15)),
                                leading: Icon(
                                  !_isBookedForSomeone
                                      ? Icons.radio_button_unchecked
                                      : Icons.check_circle,
                                  color: App24Colors.darkTextColor,
                                ),
                                title: Text(
                                  "Booked for ${_isBookedForSomeone ? bookForSomeone : "Someone"}",
                                  style: TextStyle(fontWeight: FontWeight.w600),
                                ),
                                onTap: () {
                                  if (!_isBookedForSomeone) {
                                    bookingForSomeone(context);
                                  } else {
                                    setState(() {
                                      _isBookedForSomeone = false;
                                    });
                                  }
                                },
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          if (riderName != null)
                            Padding(
                                padding: EdgeInsets.symmetric(horizontal: 20),
                                child: CommonButton(
                                  onTap: () {
                                    Map? body = {
                                      "id": rideId,
                                      "provider_id": providerId,
                                      "admin_service": "TRANSPORT"
                                    };
                                    print(riderResponseData.data![0].status);
                                    context
                                        .read(rideSelectBidNotifier.notifier)
                                        .selectBid(body: body);
                                    // context.read(rideVehiclesRequestNotifier.notifier).getRiderDetails();
                                    if (riderResponseData.data![0].status ==
                                        "BIDDING") {
                                    //   // context.read(rideVehiclesRequestNotifier.notifier).getRiderDetails();
                                      Helpers().getCommonAlertDialogBox(
                                          content: AlertDialog(
                                            content: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              mainAxisSize: MainAxisSize.min,
                                              children: [
                                                LoadingBouncingGrid.square(
                                                    backgroundColor:
                                                        Colors.white),
                                                const SizedBox(
                                                  height: 10,
                                                ),
                                                Text(
                                                  "Please wait..",
                                                  style: TextStyle(
                                                      color: Colors.white),
                                                )
                                              ],
                                            ),

                                            backgroundColor: Colors.transparent,
                                            elevation: 0,
                                          ),
                                          context: context,
                                          barrierDismissible: false);

                                    }

                                    // timer = Timer.periodic(Duration(seconds: 5), (Timer t) => statusOfRide(riderResponseData));

                                    // if (riderResponseData.data![0].status !=
                                    //     "BIDDING")
                                      // Navigator.push(
                                      //   context,
                                      //   MaterialPageRoute(
                                      //       builder: (context) => TrackRidePage(
                                      //             pickUpAddress:
                                      //                 widget.pickUpAddress,
                                      //             dropAddress:
                                      //                 widget.dropAddress,
                                      //             selectedVehicle:
                                      //                 widget.selectedVehicle,
                                      //             estimatedKm:
                                      //                 widget.estimatedKm,
                                      //             providerName: riderName,
                                      //             providerId: providerId,
                                      //             dropLat: dropLat,
                                      //             dropLng: dropLng,
                                      //             pickUpLat: pickUpLat,
                                      //             pickUpLng: pickUpLng,
                                      //           )),
                                      // );
                                    // if (riderResponseData.data![0].status !=
                                    //         "BIDDING")
                                    // autoClickButton();

                                    _socketService.listenSocket();
                                  },
                                  bgColor: App24Colors.yellowRideThemeColor,
                                  text: "Confirm Booking",
                                )),
                        ],
                      );
                    }
                  }),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: TextButton(
                        style: TextButton.styleFrom(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10))),
                        onPressed: () {
                          Helpers().getCommonBottomSheet(
                              context: context,
                              content: CancellationBottomSheet(
                                rideId: rideId,
                              ),
                              title: "Reason for Cancellation");
                        },
                        child: Text(
                          "Or cancel your order",
                          style: TextStyle(
                              color: Color(0xffFF4755),
                              decoration: TextDecoration.underline),
                        )),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                ],
              ),
            ),
          ),
        ) // This trailing comma makes auto-formatting nicer for build methods.
        );
  }

}

class Driver {
  String? driverName;
  String? driverEta;
  String? driverQuote;

  Driver({this.driverEta, this.driverName, this.driverQuote});
}
