
RiderDetailsModel riderDetailsModelFromJson(str) {
  return RiderDetailsModel.fromJson(str);
}


class RiderDetailsModel {
  dynamic statusCode;
  dynamic title;
  dynamic message;
  RiderResponseData? responseData;
  // List<Null>? responseNewData;
  List<Null>? error;

  RiderDetailsModel(
      {this.statusCode,
        this.title,
        this.message,
        this.responseData,
        // this.responseNewData,
        this.error});

  RiderDetailsModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    responseData = json['responseData'] != null
        ? new RiderResponseData.fromJson(json['responseData'])
        : null;
    // if (json['responseNewData'] != null) {
    //   responseNewData = <Null>[];
    //   json['responseNewData'].forEach((v) {
    //     responseNewData!.add(new Null.fromJson(v));
    //   });
    // }
    if (json['error'] != null) {
      error = <Null>[];
      json['error'].forEach((v) {
        error!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['title'] = this.title;
    data['message'] = this.message;
    if (this.responseData != null) {
      data['responseData'] = this.responseData!.toJson();
    }
    // if (this.responseNewData != null) {
    //   data['responseNewData'] =
    //       this.responseNewData!.map((v) => v.toJson()).toList();
    // }
    // if (this.error != null) {
    //   data['error'] = this.error!.map((v) => v.toJson()).toList();
    // }
    return data;
  }
}

class RiderResponseData {
  dynamic responseTime;
  List<Data>? data;
  dynamic sos;
  List<Emergency>? emergency;

  RiderResponseData({this.responseTime, this.data, this.sos, this.emergency});

  RiderResponseData.fromJson(Map<String, dynamic> json) {
    responseTime = json['response_time'];
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data!.add(new Data.fromJson(v));
      });
    }
    sos = json['sos'];
    if (json['emergency'] != null) {
      emergency = <Emergency>[];
      json['emergency'].forEach((v) {
        emergency!.add(new Emergency.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['response_time'] = this.responseTime;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['sos'] = this.sos;
    if (this.emergency != null) {
      data['emergency'] = this.emergency!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  dynamic id;
  dynamic stateId;
  dynamic agencyId;
  dynamic tatMin;
  dynamic isRestricted;
  dynamic vehicleType;
  dynamic providerId;
  dynamic finishedVehicleType;
  dynamic bookingId;
  dynamic adminService;
  dynamic status;
  dynamic userId;
  List<ProviderBiddingDetails>? providerBiddingDetails;
  dynamic abaId;
  dynamic providerVehicleId;
  dynamic providerServiceId;
  dynamic rideTypeId;
  dynamic geofenceId;
  dynamic rentalPackageId;
  dynamic rideDeliveryId;
  dynamic cityId;
  dynamic countryId;
  dynamic promocodeId;
  dynamic someone;
  dynamic providerRejectId;
  dynamic cancelledBy;
  dynamic cancelReason;
  dynamic paymentMode;
  dynamic paid;
  dynamic isTrack;
  dynamic calculator;
  dynamic distance;
  dynamic someoneMobile;
  dynamic locationPoints;
  dynamic timezone;
  dynamic someoneEmail;
  dynamic travelTime;
  dynamic sAddress;
  dynamic sLatitude;
  dynamic sLongitude;
  dynamic dAddress;
  dynamic dLatitude;
  dynamic dLongitude;
  dynamic trackDistance;
  dynamic estimateFare;
  dynamic minFare;
  dynamic maxFare;
  dynamic isDropLocation;
  dynamic destinationLog;
  dynamic unit;
  dynamic currency;
  dynamic trackLatitude;
  dynamic trackLongitude;
  dynamic otp;
  dynamic assignedAt;
  dynamic scheduleAt;
  dynamic startedAt;
  dynamic finishedAt;
  dynamic returnDate;
  dynamic isScheduled;
  dynamic requestType;
  dynamic travelledDistance;
  dynamic extraDistance;
  dynamic extraDistancePrice;
  dynamic peakHourId;
  dynamic userRated;
  dynamic providerRated;
  dynamic useWallet;
  dynamic surge;
  dynamic routeKey;
  dynamic adminId;
  dynamic createdType;
  dynamic createdAt;
  dynamic createdTime;
  dynamic assignedTime;
  dynamic scheduleTime;
  dynamic startedTime;
  dynamic finishedTime;
  User? user;
  Provider? provider;
  dynamic serviceType;
  Ride? ride;
  dynamic payment;
  Rating? rating;
  dynamic chat;
  dynamic rideOtp;
  dynamic peak;
  List<Reasons>? reasons;

  Data(
      {this.id,
        this.stateId,
        this.agencyId,
        this.tatMin,
        this.isRestricted,
        this.vehicleType,
        this.providerId,
        this.finishedVehicleType,
        this.bookingId,
        this.adminService,
        this.status,
        this.userId,
        this.providerBiddingDetails,
        this.abaId,
        this.providerVehicleId,
        this.providerServiceId,
        this.rideTypeId,
        this.geofenceId,
        this.rentalPackageId,
        this.rideDeliveryId,
        this.cityId,
        this.countryId,
        this.promocodeId,
        this.someone,
        this.providerRejectId,
        this.cancelledBy,
        this.cancelReason,
        this.paymentMode,
        this.paid,
        this.isTrack,
        this.calculator,
        this.distance,
        this.someoneMobile,
        this.locationPoints,
        this.timezone,
        this.someoneEmail,
        this.travelTime,
        this.sAddress,
        this.sLatitude,
        this.sLongitude,
        this.dAddress,
        this.dLatitude,
        this.dLongitude,
        this.trackDistance,
        this.estimateFare,
        this.minFare,
        this.maxFare,
        this.isDropLocation,
        this.destinationLog,
        this.unit,
        this.currency,
        this.trackLatitude,
        this.trackLongitude,
        this.otp,
        this.assignedAt,
        this.scheduleAt,
        this.startedAt,
        this.finishedAt,
        this.returnDate,
        this.isScheduled,
        this.requestType,
        this.travelledDistance,
        this.extraDistance,
        this.extraDistancePrice,
        this.peakHourId,
        this.userRated,
        this.providerRated,
        this.useWallet,
        this.surge,
        this.routeKey,
        this.adminId,
        this.createdType,
        this.createdAt,
        this.createdTime,
        this.assignedTime,
        this.scheduleTime,
        this.startedTime,
        this.finishedTime,
        this.user,
        this.provider,
        this.serviceType,
        this.ride,
        this.payment,
        this.rating,
        this.chat,
        this.rideOtp,
        this.peak,
        this.reasons});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    stateId = json['state_id'];
    agencyId = json['agency_id'];
    tatMin = json['tat_min'];
    isRestricted = json['is_restricted'];
    vehicleType = json['vehicle_type'];
    providerId = json['provider_id'];
    finishedVehicleType = json['finished_vehicle_type'];
    bookingId = json['booking_id'];
    adminService = json['admin_service'];
    status = json['status'];
    userId = json['user_id'];
    if (json['provider_bidding_details'] != null) {
      providerBiddingDetails = <ProviderBiddingDetails>[];
      json['provider_bidding_details'].forEach((v) {
        providerBiddingDetails!.add(new ProviderBiddingDetails.fromJson(v));
      });
    }
    abaId = json['aba_id'];
    providerVehicleId = json['provider_vehicle_id'];
    providerServiceId = json['provider_service_id'];
    rideTypeId = json['ride_type_id'];
    geofenceId = json['geofence_id'];
    rentalPackageId = json['rental_package_id'];
    rideDeliveryId = json['ride_delivery_id'];
    cityId = json['city_id'];
    countryId = json['country_id'];
    promocodeId = json['promocode_id'];
    someone = json['someone'];
    providerRejectId = json['provider_reject_id'];
    cancelledBy = json['cancelled_by'];
    cancelReason = json['cancel_reason'];
    paymentMode = json['payment_mode'];
    paid = json['paid'];
    isTrack = json['is_track'];
    calculator = json['calculator'];
    distance = json['distance'];
    someoneMobile = json['someone_mobile'];
    locationPoints = json['location_points'];
    timezone = json['timezone'];
    someoneEmail = json['someone_email'];
    travelTime = json['travel_time'];
    sAddress = json['s_address'];
    sLatitude = json['s_latitude'];
    sLongitude = json['s_longitude'];
    dAddress = json['d_address'];
    dLatitude = json['d_latitude'];
    dLongitude = json['d_longitude'];
    trackDistance = json['track_distance'];
    estimateFare = json['estimate_fare'];
    minFare = json['min_fare'];
    maxFare = json['max_fare'];
    isDropLocation = json['is_drop_location'];
    destinationLog = json['destination_log'];
    unit = json['unit'];
    currency = json['currency'];
    trackLatitude = json['track_latitude'];
    trackLongitude = json['track_longitude'];
    otp = json['otp'];
    assignedAt = json['assigned_at'];
    scheduleAt = json['schedule_at'];
    startedAt = json['started_at'];
    finishedAt = json['finished_at'];
    returnDate = json['return_date'];
    isScheduled = json['is_scheduled'];
    requestType = json['request_type'];
    travelledDistance = json['travelled_distance'];
    extraDistance = json['extra_distance'];
    extraDistancePrice = json['extra_distance_price'];
    peakHourId = json['peak_hour_id'];
    userRated = json['user_rated'];
    providerRated = json['provider_rated'];
    useWallet = json['use_wallet'];
    surge = json['surge'];
    routeKey = json['route_key'];
    adminId = json['admin_id'];
    createdType = json['created_type'];
    createdAt = json['created_at'];
    createdTime = json['created_time'];
    assignedTime = json['assigned_time'];
    scheduleTime = json['schedule_time'];
    startedTime = json['started_time'];
    finishedTime = json['finished_time'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
    provider = json['provider'] != null
        ? new Provider.fromJson(json['provider'])
        : null;
    serviceType = json['service_type'];
    ride = json['ride'] != null ? new Ride.fromJson(json['ride']) : null;
    payment = json['payment'];
    rating =
    json['rating'] != null ? new Rating.fromJson(json['rating']) : null;
    chat = json['chat'];
    rideOtp = json['ride_otp'];
    peak = json['peak'];
    if (json['reasons'] != null) {
      reasons = <Reasons>[];
      json['reasons'].forEach((v) {
        reasons!.add(new Reasons.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['state_id'] = this.stateId;
    data['agency_id'] = this.agencyId;
    data['tat_min'] = this.tatMin;
    data['is_restricted'] = this.isRestricted;
    data['vehicle_type'] = this.vehicleType;
    data['provider_id'] = this.providerId;
    data['finished_vehicle_type'] = this.finishedVehicleType;
    data['booking_id'] = this.bookingId;
    data['admin_service'] = this.adminService;
    data['status'] = this.status;
    data['user_id'] = this.userId;
    if (this.providerBiddingDetails != null) {
      data['provider_bidding_details'] =
          this.providerBiddingDetails!.map((v) => v.toJson()).toList();
    }
    data['aba_id'] = this.abaId;
    data['provider_vehicle_id'] = this.providerVehicleId;
    data['provider_service_id'] = this.providerServiceId;
    data['ride_type_id'] = this.rideTypeId;
    data['geofence_id'] = this.geofenceId;
    data['rental_package_id'] = this.rentalPackageId;
    data['ride_delivery_id'] = this.rideDeliveryId;
    data['city_id'] = this.cityId;
    data['country_id'] = this.countryId;
    data['promocode_id'] = this.promocodeId;
    data['someone'] = this.someone;
    data['provider_reject_id'] = this.providerRejectId;
    data['cancelled_by'] = this.cancelledBy;
    data['cancel_reason'] = this.cancelReason;
    data['payment_mode'] = this.paymentMode;
    data['paid'] = this.paid;
    data['is_track'] = this.isTrack;
    data['calculator'] = this.calculator;
    data['distance'] = this.distance;
    data['someone_mobile'] = this.someoneMobile;
    data['location_points'] = this.locationPoints;
    data['timezone'] = this.timezone;
    data['someone_email'] = this.someoneEmail;
    data['travel_time'] = this.travelTime;
    data['s_address'] = this.sAddress;
    data['s_latitude'] = this.sLatitude;
    data['s_longitude'] = this.sLongitude;
    data['d_address'] = this.dAddress;
    data['d_latitude'] = this.dLatitude;
    data['d_longitude'] = this.dLongitude;
    data['track_distance'] = this.trackDistance;
    data['estimate_fare'] = this.estimateFare;
    data['min_fare'] = this.minFare;
    data['max_fare'] = this.maxFare;
    data['is_drop_location'] = this.isDropLocation;
    data['destination_log'] = this.destinationLog;
    data['unit'] = this.unit;
    data['currency'] = this.currency;
    data['track_latitude'] = this.trackLatitude;
    data['track_longitude'] = this.trackLongitude;
    data['otp'] = this.otp;
    data['assigned_at'] = this.assignedAt;
    data['schedule_at'] = this.scheduleAt;
    data['started_at'] = this.startedAt;
    data['finished_at'] = this.finishedAt;
    data['return_date'] = this.returnDate;
    data['is_scheduled'] = this.isScheduled;
    data['request_type'] = this.requestType;
    data['travelled_distance'] = this.travelledDistance;
    data['extra_distance'] = this.extraDistance;
    data['extra_distance_price'] = this.extraDistancePrice;
    data['peak_hour_id'] = this.peakHourId;
    data['user_rated'] = this.userRated;
    data['provider_rated'] = this.providerRated;
    data['use_wallet'] = this.useWallet;
    data['surge'] = this.surge;
    data['route_key'] = this.routeKey;
    data['admin_id'] = this.adminId;
    data['created_type'] = this.createdType;
    data['created_at'] = this.createdAt;
    data['created_time'] = this.createdTime;
    data['assigned_time'] = this.assignedTime;
    data['schedule_time'] = this.scheduleTime;
    data['started_time'] = this.startedTime;
    data['finished_time'] = this.finishedTime;
    if (this.user != null) {
      data['user'] = this.user!.toJson();
    }
    if (this.provider != null) {
      data['provider'] = this.provider!.toJson();
    }
    data['service_type'] = this.serviceType;
    if (this.ride != null) {
      data['ride'] = this.ride!.toJson();
    }
    data['payment'] = this.payment;
    if (this.rating != null) {
      data['rating'] = this.rating!.toJson();
    }
    data['chat'] = this.chat;
    data['ride_otp'] = this.rideOtp;
    data['peak'] = this.peak;
    if (this.reasons != null) {
      data['reasons'] = this.reasons!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ProviderBiddingDetails {
  dynamic providerId;
  dynamic providerMobile;
  dynamic providerRating;
  dynamic providerPicture;
  dynamic vehicleNo;
  dynamic vehicleModel;
  dynamic bidAmount;
  dynamic bidAmountRs;
  dynamic eta;
  dynamic providerName;

  ProviderBiddingDetails(
      {this.providerId,
        this.providerMobile,
        this.providerRating,
        this.providerPicture,
        this.vehicleNo,
        this.vehicleModel,
        this.bidAmount,
        this.bidAmountRs,
        this.eta,
        this.providerName});

  ProviderBiddingDetails.fromJson(Map<String, dynamic> json) {
    providerId = json['provider_id'];
    providerMobile = json['provider_mobile'];
    providerRating = json['provider_rating'];
    providerPicture = json['provider_picture'];
    vehicleNo = json['vehicle_no'];
    vehicleModel = json['vehicle_model'];
    bidAmount = json['bid_amount'];
    bidAmountRs = json['bid_amount_rs'];
    eta = json['eta'];
    providerName = json['provider_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['provider_id'] = this.providerId;
    data['provider_mobile'] = this.providerMobile;
    data['provider_rating'] = this.providerRating;
    data['provider_picture'] = this.providerPicture;
    data['vehicle_no'] = this.vehicleNo;
    data['vehicle_model'] = this.vehicleModel;
    data['bid_amount'] = this.bidAmount;
    data['bid_amount_rs'] = this.bidAmountRs;
    data['eta'] = this.eta;
    data['provider_name'] = this.providerName;
    return data;
  }
}

class User {
  dynamic id;
  dynamic firstName;
  dynamic lastName;
  dynamic walletBalance;
  dynamic userType;
  dynamic email;
  dynamic paymentMode;
  dynamic mobile;
  dynamic gender;
  dynamic countryCode;
  dynamic currencySymbol;
  dynamic picture;
  dynamic address;
  dynamic pincode;
  dynamic loginBy;
  dynamic latitude;
  dynamic longitude;
  dynamic rating;
  dynamic language;
  dynamic referralUniqueId;
  dynamic countryId;
  dynamic stateId;
  dynamic cityId;
  dynamic companyId;
  dynamic status;
  dynamic favouriteStores;
  dynamic appVersion;
  dynamic createdAt;

  User(
      {this.id,
        this.firstName,
        this.lastName,
        this.walletBalance,
        this.userType,
        this.email,
        this.paymentMode,
        this.mobile,
        this.gender,
        this.countryCode,
        this.currencySymbol,
        this.picture,
        this.address,
        this.pincode,
        this.loginBy,
        this.latitude,
        this.longitude,
        this.rating,
        this.language,
        this.referralUniqueId,
        this.countryId,
        this.stateId,
        this.cityId,
        this.companyId,
        this.status,
        this.favouriteStores,
        this.appVersion,
        this.createdAt});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    walletBalance = json['wallet_balance'];
    userType = json['user_type'];
    email = json['email'];
    paymentMode = json['payment_mode'];
    mobile = json['mobile'];
    gender = json['gender'];
    countryCode = json['country_code'];
    currencySymbol = json['currency_symbol'];
    picture = json['picture'];
    address = json['address'];
    pincode = json['pincode'];
    loginBy = json['login_by'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    rating = json['rating'];
    language = json['language'];
    referralUniqueId = json['referral_unique_id'];
    countryId = json['country_id'];
    stateId = json['state_id'];
    cityId = json['city_id'];
    companyId = json['company_id'];
    status = json['status'];
    favouriteStores = json['favourite_stores'];
    appVersion = json['app_version'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['wallet_balance'] = this.walletBalance;
    data['user_type'] = this.userType;
    data['email'] = this.email;
    data['payment_mode'] = this.paymentMode;
    data['mobile'] = this.mobile;
    data['gender'] = this.gender;
    data['country_code'] = this.countryCode;
    data['currency_symbol'] = this.currencySymbol;
    data['picture'] = this.picture;
    data['address'] = this.address;
    data['pincode'] = this.pincode;
    data['login_by'] = this.loginBy;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['rating'] = this.rating;
    data['language'] = this.language;
    data['referral_unique_id'] = this.referralUniqueId;
    data['country_id'] = this.countryId;
    data['state_id'] = this.stateId;
    data['city_id'] = this.cityId;
    data['company_id'] = this.companyId;
    data['status'] = this.status;
    data['favourite_stores'] = this.favouriteStores;
    data['app_version'] = this.appVersion;
    data['created_at'] = this.createdAt;
    return data;
  }
}

class Provider {
  dynamic rating;

  Provider({this.rating});

  Provider.fromJson(Map<String, dynamic> json) {
    rating = json['rating'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['rating'] = this.rating;
    return data;
  }
}

class Ride {
  dynamic id;
  dynamic rideTypeId;
  dynamic vehicleType;
  dynamic vehicleName;
  dynamic vehicleImage;
  dynamic vehicleMarker;
  dynamic capacity;
  dynamic status;
  dynamic keywords;
  dynamic homeDelivery;

  Ride(
      {this.id,
        this.rideTypeId,
        this.vehicleType,
        this.vehicleName,
        this.vehicleImage,
        this.vehicleMarker,
        this.capacity,
        this.status,
        this.keywords,
        this.homeDelivery});

  Ride.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    rideTypeId = json['ride_type_id'];
    vehicleType = json['vehicle_type'];
    vehicleName = json['vehicle_name'];
    vehicleImage = json['vehicle_image'];
    vehicleMarker = json['vehicle_marker'];
    capacity = json['capacity'];
    status = json['status'];
    keywords = json['keywords'];
    homeDelivery = json['home_delivery'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['ride_type_id'] = this.rideTypeId;
    data['vehicle_type'] = this.vehicleType;
    data['vehicle_name'] = this.vehicleName;
    data['vehicle_image'] = this.vehicleImage;
    data['vehicle_marker'] = this.vehicleMarker;
    data['capacity'] = this.capacity;
    data['status'] = this.status;
    data['keywords'] = this.keywords;
    data['home_delivery'] = this.homeDelivery;
    return data;
  }
}

class Rating {
  dynamic id;
  dynamic adminService;
  dynamic requestId;
  dynamic userId;
  dynamic providerId;
  dynamic storeId;
  dynamic companyId;
  dynamic userRating;
  dynamic providerRating;
  dynamic storeRating;
  dynamic userComment;
  dynamic providerComment;
  dynamic storeComment;
  dynamic createdType;
  dynamic createdBy;
  dynamic modifiedType;
  dynamic modifiedBy;
  dynamic deletedType;
  dynamic deletedBy;
  dynamic createdAt;
  dynamic updatedAt;
  dynamic deletedAt;

  Rating(
      {this.id,
        this.adminService,
        this.requestId,
        this.userId,
        this.providerId,
        this.storeId,
        this.companyId,
        this.userRating,
        this.providerRating,
        this.storeRating,
        this.userComment,
        this.providerComment,
        this.storeComment,
        this.createdType,
        this.createdBy,
        this.modifiedType,
        this.modifiedBy,
        this.deletedType,
        this.deletedBy,
        this.createdAt,
        this.updatedAt,
        this.deletedAt});

  Rating.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    adminService = json['admin_service'];
    requestId = json['request_id'];
    userId = json['user_id'];
    providerId = json['provider_id'];
    storeId = json['store_id'];
    companyId = json['company_id'];
    userRating = json['user_rating'];
    providerRating = json['provider_rating'];
    storeRating = json['store_rating'];
    userComment = json['user_comment'];
    providerComment = json['provider_comment'];
    storeComment = json['store_comment'];
    createdType = json['created_type'];
    createdBy = json['created_by'];
    modifiedType = json['modified_type'];
    modifiedBy = json['modified_by'];
    deletedType = json['deleted_type'];
    deletedBy = json['deleted_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['admin_service'] = this.adminService;
    data['request_id'] = this.requestId;
    data['user_id'] = this.userId;
    data['provider_id'] = this.providerId;
    data['store_id'] = this.storeId;
    data['company_id'] = this.companyId;
    data['user_rating'] = this.userRating;
    data['provider_rating'] = this.providerRating;
    data['store_rating'] = this.storeRating;
    data['user_comment'] = this.userComment;
    data['provider_comment'] = this.providerComment;
    data['store_comment'] = this.storeComment;
    data['created_type'] = this.createdType;
    data['created_by'] = this.createdBy;
    data['modified_type'] = this.modifiedType;
    data['modified_by'] = this.modifiedBy;
    data['deleted_type'] = this.deletedType;
    data['deleted_by'] = this.deletedBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    return data;
  }
}

class Reasons {
  dynamic id;
  dynamic service;
  dynamic type;
  dynamic reason;
  dynamic status;
  dynamic createdType;
  dynamic createdBy;
  dynamic modifiedType;
  dynamic modifiedBy;
  dynamic deletedType;
  dynamic deletedBy;

  Reasons(
      {this.id,
        this.service,
        this.type,
        this.reason,
        this.status,
        this.createdType,
        this.createdBy,
        this.modifiedType,
        this.modifiedBy,
        this.deletedType,
        this.deletedBy});

  Reasons.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    service = json['service'];
    type = json['type'];
    reason = json['reason'];
    status = json['status'];
    createdType = json['created_type'];
    createdBy = json['created_by'];
    modifiedType = json['modified_type'];
    modifiedBy = json['modified_by'];
    deletedType = json['deleted_type'];
    deletedBy = json['deleted_by'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['service'] = this.service;
    data['type'] = this.type;
    data['reason'] = this.reason;
    data['status'] = this.status;
    data['created_type'] = this.createdType;
    data['created_by'] = this.createdBy;
    data['modified_type'] = this.modifiedType;
    data['modified_by'] = this.modifiedBy;
    data['deleted_type'] = this.deletedType;
    data['deleted_by'] = this.deletedBy;
    return data;
  }
}

class Emergency {
  dynamic number;

  Emergency({this.number});

  Emergency.fromJson(Map<String, dynamic> json) {
    number = json['number'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['number'] = this.number;
    return data;
  }
}



// class RiderDetailsModel {
//   dynamic  statusCode;
//   dynamic  title;
//   dynamic  message;
//   RiderResponseData? responseData;
//   // List<Null>? responseNewData;
//   List<Null>? error;
//
//   RiderDetailsModel(
//       {this.statusCode,
//         this.title,
//         this.message,
//         this.responseData,
//         // this.responseNewData,
//         this.error});
//
//   RiderDetailsModel.fromJson(Map<String, dynamic> json) {
//     statusCode = json['statusCode'];
//     title = json['title'];
//     message = json['message'];
//     responseData = json['responseData'] != null
//         ? new RiderResponseData.fromJson(json['responseData'])
//         : null;
//     // if (json['responseNewData'] != null) {
//     //   responseNewData = <Null>[];
//     //   json['responseNewData'].forEach((v) {
//     //     responseNewData!.add(v);
//     //   });
//     // }
//     if (json['error'] != null) {
//       error = <Null>[];
//       json['error'].forEach((v) {
//         error!.add(v);
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['statusCode'] = this.statusCode;
//     data['title'] = this.title;
//     data['message'] = this.message;
//     if (this.responseData != null) {
//       data['responseData'] = this.responseData!.toJson();
//     }
//     // if (this.responseNewData != null) {
//     //   data['responseNewData'] =
//     //       this.responseNewData!.map((v) => v.toJson()).toList();
//     // }
//     // if (this.error != null) {
//     //   data['error'] = this.error!.map((v) => v.toJson()).toList();
//     // }
//     return data;
//   }
// }
//
// class RiderResponseData {
//   dynamic  responseTime;
//   List<Data>? data;
//   dynamic  sos;
//   List<Emergency>? emergency;
//
//   RiderResponseData({this.responseTime, this.data, this.sos, this.emergency});
//
//   RiderResponseData.fromJson(Map<String, dynamic> json) {
//     responseTime = json['response_time'];
//     if (json['data'] != null) {
//       data = <Data>[];
//       json['data'].forEach((v) {
//         data!.add(new Data.fromJson(v));
//       });
//     }
//     sos = json['sos'];
//     if (json['emergency'] != null) {
//       emergency = <Emergency>[];
//       json['emergency'].forEach((v) {
//         emergency!.add(new Emergency.fromJson(v));
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['response_time'] = this.responseTime;
//     if (this.data != null) {
//       data['data'] = this.data!.map((v) => v.toJson()).toList();
//     }
//     data['sos'] = this.sos;
//     if (this.emergency != null) {
//       data['emergency'] = this.emergency!.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }
//
// class Data {
//   dynamic id;
//   dynamic  vehicleType;
//   dynamic finishedVehicleType;
//   dynamic  bookingId;
//   dynamic  adminService;
//   dynamic userId;
//   List<ProviderBiddingDetails>? providerBiddingDetails;
//   dynamic providerId;
//   dynamic abaId;
//   dynamic providerVehicleId;
//   dynamic providerServiceId;
//   dynamic rideTypeId;
//   dynamic geofenceId;
//   dynamic rentalPackageId;
//   dynamic rideDeliveryId;
//   dynamic cityId;
//   dynamic countryId;
//   dynamic promocodeId;
//   dynamic someone;
//   dynamic  status;
//   dynamic  providerRejectId;
//   dynamic cancelledBy;
//   dynamic cancelReason;
//   dynamic  paymentMode;
//   dynamic paid;
//   dynamic  isTrack;
//   dynamic  calculator;
//   dynamic  distance;
//   dynamic someoneMobile;
//   dynamic  locationPoints;
//   dynamic  timezone;
//   dynamic someoneEmail;
//   dynamic travelTime;
//   dynamic  sAddress;
//   dynamic  sLatitude;
//   dynamic  sLongitude;
//   dynamic  dAddress;
//   dynamic  dLatitude;
//   dynamic  dLongitude;
//   dynamic trackDistance;
//   dynamic  estimateFare;
//   dynamic  minFare;
//   dynamic  maxFare;
//   dynamic isDropLocation;
//   dynamic  destinationLog;
//   dynamic  unit;
//   dynamic  currency;
//   dynamic  trackLatitude;
//   dynamic  trackLongitude;
//   dynamic  otp;
//   dynamic  assignedAt;
//   dynamic scheduleAt;
//   dynamic startedAt;
//   dynamic finishedAt;
//   dynamic returnDate;
//   dynamic  isScheduled;
//   dynamic  requestType;
//   dynamic travelledDistance;
//   dynamic extraDistance;
//   dynamic extraDistancePrice;
//   dynamic peakHourId;
//   dynamic userRated;
//   dynamic providerRated;
//   dynamic useWallet;
//   dynamic surge;
//   dynamic  routeKey;
//   dynamic adminId;
//   dynamic  createdType;
//   dynamic  createdAt;
//   dynamic  createdTime;
//   dynamic  assignedTime;
//   dynamic  scheduleTime;
//   dynamic  startedTime;
//   dynamic  finishedTime;
//   User? user;
//   Provider? provider;
//   dynamic serviceType;
//   Ride? ride;
//   dynamic payment;
//   Rating? rating;
//   dynamic chat;
//   dynamic rideOtp;
//   dynamic peak;
//   List<Reasons>? reasons;
//
//   Data(
//       {this.id,
//         this.vehicleType,
//         this.finishedVehicleType,
//         this.bookingId,
//         this.adminService,
//         this.userId,
//         this.providerBiddingDetails,
//         this.providerId,
//         this.abaId,
//         this.providerVehicleId,
//         this.providerServiceId,
//         this.rideTypeId,
//         this.geofenceId,
//         this.rentalPackageId,
//         this.rideDeliveryId,
//         this.cityId,
//         this.countryId,
//         this.promocodeId,
//         this.someone,
//         this.status,
//         this.providerRejectId,
//         this.cancelledBy,
//         this.cancelReason,
//         this.paymentMode,
//         this.paid,
//         this.isTrack,
//         this.calculator,
//         this.distance,
//         this.someoneMobile,
//         this.locationPoints,
//         this.timezone,
//         this.someoneEmail,
//         this.travelTime,
//         this.sAddress,
//         this.sLatitude,
//         this.sLongitude,
//         this.dAddress,
//         this.dLatitude,
//         this.dLongitude,
//         this.trackDistance,
//         this.estimateFare,
//         this.minFare,
//         this.maxFare,
//         this.isDropLocation,
//         this.destinationLog,
//         this.unit,
//         this.currency,
//         this.trackLatitude,
//         this.trackLongitude,
//         this.otp,
//         this.assignedAt,
//         this.scheduleAt,
//         this.startedAt,
//         this.finishedAt,
//         this.returnDate,
//         this.isScheduled,
//         this.requestType,
//         this.travelledDistance,
//         this.extraDistance,
//         this.extraDistancePrice,
//         this.peakHourId,
//         this.userRated,
//         this.providerRated,
//         this.useWallet,
//         this.surge,
//         this.routeKey,
//         this.adminId,
//         this.createdType,
//         this.createdAt,
//         this.createdTime,
//         this.assignedTime,
//         this.scheduleTime,
//         this.startedTime,
//         this.finishedTime,
//         this.user,
//         this.provider,
//         this.serviceType,
//         this.ride,
//         this.payment,
//         this.rating,
//         this.chat,
//         this.rideOtp,
//         this.peak,
//         this.reasons});
//
//   Data.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     vehicleType = json['vehicle_type'];
//     finishedVehicleType = json['finished_vehicle_type'];
//     bookingId = json['booking_id'];
//     adminService = json['admin_service'];
//     userId = json['user_id'];
//     if (json['provider_bidding_details'] != null) {
//       providerBiddingDetails = <ProviderBiddingDetails>[];
//       json['provider_bidding_details'].forEach((v) {
//         providerBiddingDetails!.add(new ProviderBiddingDetails.fromJson(v));
//       });
//     }
//     providerId = json['provider_id'];
//     abaId = json['aba_id'];
//     providerVehicleId = json['provider_vehicle_id'];
//     providerServiceId = json['provider_service_id'];
//     rideTypeId = json['ride_type_id'];
//     geofenceId = json['geofence_id'];
//     rentalPackageId = json['rental_package_id'];
//     rideDeliveryId = json['ride_delivery_id'];
//     cityId = json['city_id'];
//     countryId = json['country_id'];
//     promocodeId = json['promocode_id'];
//     someone = json['someone'];
//     status = json['status'];
//     providerRejectId = json['provider_reject_id'];
//     cancelledBy = json['cancelled_by'];
//     cancelReason = json['cancel_reason'];
//     paymentMode = json['payment_mode'];
//     paid = json['paid'];
//     isTrack = json['is_track'];
//     calculator = json['calculator'];
//     distance = json['distance'];
//     someoneMobile = json['someone_mobile'];
//     locationPoints = json['location_points'];
//     timezone = json['timezone'];
//     someoneEmail = json['someone_email'];
//     travelTime = json['travel_time'];
//     sAddress = json['s_address'];
//     sLatitude = json['s_latitude'];
//     sLongitude = json['s_longitude'];
//     dAddress = json['d_address'];
//     dLatitude = json['d_latitude'];
//     dLongitude = json['d_longitude'];
//     trackDistance = json['track_distance'];
//     estimateFare = json['estimate_fare'];
//     minFare = json['min_fare'];
//     maxFare = json['max_fare'];
//     isDropLocation = json['is_drop_location'];
//     destinationLog = json['destination_log'];
//     unit = json['unit'];
//     currency = json['currency'];
//     trackLatitude = json['track_latitude'];
//     trackLongitude = json['track_longitude'];
//     otp = json['otp'];
//     assignedAt = json['assigned_at'];
//     scheduleAt = json['schedule_at'];
//     startedAt = json['started_at'];
//     finishedAt = json['finished_at'];
//     returnDate = json['return_date'];
//     isScheduled = json['is_scheduled'];
//     requestType = json['request_type'];
//     travelledDistance = json['travelled_distance'];
//     extraDistance = json['extra_distance'];
//     extraDistancePrice = json['extra_distance_price'];
//     peakHourId = json['peak_hour_id'];
//     userRated = json['user_rated'];
//     providerRated = json['provider_rated'];
//     useWallet = json['use_wallet'];
//     surge = json['surge'];
//     routeKey = json['route_key'];
//     adminId = json['admin_id'];
//     createdType = json['created_type'];
//     createdAt = json['created_at'];
//     createdTime = json['created_time'];
//     assignedTime = json['assigned_time'];
//     scheduleTime = json['schedule_time'];
//     startedTime = json['started_time'];
//     finishedTime = json['finished_time'];
//     user = json['user'] != dynamic ? new User.fromJson(json['user']) : null;
//     provider = json['provider'] != null
//         ? new Provider.fromJson(json['provider'])
//         : null;
//     serviceType = json['service_type'];
//     ride = json['ride'] != dynamic ? new Ride.fromJson(json['ride']) : null;
//     payment = json['payment'];
//     rating =
//     json['rating'] != dynamic ? new Rating.fromJson(json['rating']) : null;
//     chat = json['chat'];
//     rideOtp = json['ride_otp'];
//     peak = json['peak'];
//     if (json['reasons'] != null) {
//       reasons = <Reasons>[];
//       json['reasons'].forEach((v) {
//         reasons!.add(new Reasons.fromJson(v));
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['vehicle_type'] = this.vehicleType;
//     data['finished_vehicle_type'] = this.finishedVehicleType;
//     data['booking_id'] = this.bookingId;
//     data['admin_service'] = this.adminService;
//     data['user_id'] = this.userId;
//     if (this.providerBiddingDetails != null) {
//       data['provider_bidding_details'] =
//           this.providerBiddingDetails!.map((v) => v.toJson()).toList();
//     }
//     data['provider_id'] = this.providerId;
//     data['aba_id'] = this.abaId;
//     data['provider_vehicle_id'] = this.providerVehicleId;
//     data['provider_service_id'] = this.providerServiceId;
//     data['ride_type_id'] = this.rideTypeId;
//     data['geofence_id'] = this.geofenceId;
//     data['rental_package_id'] = this.rentalPackageId;
//     data['ride_delivery_id'] = this.rideDeliveryId;
//     data['city_id'] = this.cityId;
//     data['country_id'] = this.countryId;
//     data['promocode_id'] = this.promocodeId;
//     data['someone'] = this.someone;
//     data['status'] = this.status;
//     data['provider_reject_id'] = this.providerRejectId;
//     data['cancelled_by'] = this.cancelledBy;
//     data['cancel_reason'] = this.cancelReason;
//     data['payment_mode'] = this.paymentMode;
//     data['paid'] = this.paid;
//     data['is_track'] = this.isTrack;
//     data['calculator'] = this.calculator;
//     data['distance'] = this.distance;
//     data['someone_mobile'] = this.someoneMobile;
//     data['location_points'] = this.locationPoints;
//     data['timezone'] = this.timezone;
//     data['someone_email'] = this.someoneEmail;
//     data['travel_time'] = this.travelTime;
//     data['s_address'] = this.sAddress;
//     data['s_latitude'] = this.sLatitude;
//     data['s_longitude'] = this.sLongitude;
//     data['d_address'] = this.dAddress;
//     data['d_latitude'] = this.dLatitude;
//     data['d_longitude'] = this.dLongitude;
//     data['track_distance'] = this.trackDistance;
//     data['estimate_fare'] = this.estimateFare;
//     data['min_fare'] = this.minFare;
//     data['max_fare'] = this.maxFare;
//     data['is_drop_location'] = this.isDropLocation;
//     data['destination_log'] = this.destinationLog;
//     data['unit'] = this.unit;
//     data['currency'] = this.currency;
//     data['track_latitude'] = this.trackLatitude;
//     data['track_longitude'] = this.trackLongitude;
//     data['otp'] = this.otp;
//     data['assigned_at'] = this.assignedAt;
//     data['schedule_at'] = this.scheduleAt;
//     data['started_at'] = this.startedAt;
//     data['finished_at'] = this.finishedAt;
//     data['return_date'] = this.returnDate;
//     data['is_scheduled'] = this.isScheduled;
//     data['request_type'] = this.requestType;
//     data['travelled_distance'] = this.travelledDistance;
//     data['extra_distance'] = this.extraDistance;
//     data['extra_distance_price'] = this.extraDistancePrice;
//     data['peak_hour_id'] = this.peakHourId;
//     data['user_rated'] = this.userRated;
//     data['provider_rated'] = this.providerRated;
//     data['use_wallet'] = this.useWallet;
//     data['surge'] = this.surge;
//     data['route_key'] = this.routeKey;
//     data['admin_id'] = this.adminId;
//     data['created_type'] = this.createdType;
//     data['created_at'] = this.createdAt;
//     data['created_time'] = this.createdTime;
//     data['assigned_time'] = this.assignedTime;
//     data['schedule_time'] = this.scheduleTime;
//     data['started_time'] = this.startedTime;
//     data['finished_time'] = this.finishedTime;
//     if (this.user != null) {
//       data['user'] = this.user!.toJson();
//     }
//     if (this.provider != null) {
//       data['provider'] = this.provider!.toJson();
//     }
//     data['service_type'] = this.serviceType;
//     if (this.ride != null) {
//       data['ride'] = this.ride!.toJson();
//     }
//     data['payment'] = this.payment;
//     if (this.rating != null) {
//       data['rating'] = this.rating!.toJson();
//     }
//     data['chat'] = this.chat;
//     data['ride_otp'] = this.rideOtp;
//     data['peak'] = this.peak;
//     if (this.reasons != null) {
//       data['reasons'] = this.reasons!.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }
//
// class ProviderBiddingDetails {
//   dynamic providerId;
//   dynamic  bidAmount;
//   dynamic  bidAmountRs;
//   dynamic  eta;
//   dynamic  providerName;
//
//   ProviderBiddingDetails(
//       {this.providerId,
//         this.bidAmount,
//         this.bidAmountRs,
//         this.eta,
//         this.providerName});
//
//   ProviderBiddingDetails.fromJson(Map<String, dynamic> json) {
//     providerId = json['provider_id'];
//     bidAmount = json['bid_amount'];
//     bidAmountRs = json['bid_amount_rs'];
//     eta = json['eta'];
//     providerName = json['provider_name'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['provider_id'] = this.providerId;
//     data['bid_amount'] = this.bidAmount;
//     data['bid_amount_rs'] = this.bidAmountRs;
//     data['eta'] = this.eta;
//     data['provider_name'] = this.providerName;
//     return data;
//   }
// }
//
// class User {
//   dynamic id;
//   dynamic  firstName;
//   dynamic  lastName;
//   dynamic walletBalance;
//   dynamic  userType;
//   dynamic  email;
//   dynamic  paymentMode;
//   dynamic  mobile;
//   dynamic  gender;
//   dynamic  countryCode;
//   dynamic  currencySymbol;
//   dynamic picture;
//   dynamic address;
//   dynamic pincode;
//   dynamic  loginBy;
//   dynamic latitude;
//   dynamic longitude;
//   dynamic rating;
//   dynamic  language;
//   dynamic  referralUniqueId;
//   dynamic countryId;
//   dynamic stateId;
//   dynamic cityId;
//   dynamic companyId;
//   dynamic status;
//   dynamic favouriteStores;
//   dynamic  appVersion;
//   dynamic  createdAt;
//
//   User(
//       {this.id,
//         this.firstName,
//         this.lastName,
//         this.walletBalance,
//         this.userType,
//         this.email,
//         this.paymentMode,
//         this.mobile,
//         this.gender,
//         this.countryCode,
//         this.currencySymbol,
//         this.picture,
//         this.address,
//         this.pincode,
//         this.loginBy,
//         this.latitude,
//         this.longitude,
//         this.rating,
//         this.language,
//         this.referralUniqueId,
//         this.countryId,
//         this.stateId,
//         this.cityId,
//         this.companyId,
//         this.status,
//         this.favouriteStores,
//         this.appVersion,
//         this.createdAt});
//
//   User.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     firstName = json['first_name'];
//     lastName = json['last_name'];
//     walletBalance = json['wallet_balance'];
//     userType = json['user_type'];
//     email = json['email'];
//     paymentMode = json['payment_mode'];
//     mobile = json['mobile'];
//     gender = json['gender'];
//     countryCode = json['country_code'];
//     currencySymbol = json['currency_symbol'];
//     picture = json['picture'];
//     address = json['address'];
//     pincode = json['pincode'];
//     loginBy = json['login_by'];
//     latitude = json['latitude'];
//     longitude = json['longitude'];
//     rating = json['rating'];
//     language = json['language'];
//     referralUniqueId = json['referral_unique_id'];
//     countryId = json['country_id'];
//     stateId = json['state_id'];
//     cityId = json['city_id'];
//     companyId = json['company_id'];
//     status = json['status'];
//     favouriteStores = json['favourite_stores'];
//     appVersion = json['app_version'];
//     createdAt = json['created_at'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['first_name'] = this.firstName;
//     data['last_name'] = this.lastName;
//     data['wallet_balance'] = this.walletBalance;
//     data['user_type'] = this.userType;
//     data['email'] = this.email;
//     data['payment_mode'] = this.paymentMode;
//     data['mobile'] = this.mobile;
//     data['gender'] = this.gender;
//     data['country_code'] = this.countryCode;
//     data['currency_symbol'] = this.currencySymbol;
//     data['picture'] = this.picture;
//     data['address'] = this.address;
//     data['pincode'] = this.pincode;
//     data['login_by'] = this.loginBy;
//     data['latitude'] = this.latitude;
//     data['longitude'] = this.longitude;
//     data['rating'] = this.rating;
//     data['language'] = this.language;
//     data['referral_unique_id'] = this.referralUniqueId;
//     data['country_id'] = this.countryId;
//     data['state_id'] = this.stateId;
//     data['city_id'] = this.cityId;
//     data['company_id'] = this.companyId;
//     data['status'] = this.status;
//     data['favourite_stores'] = this.favouriteStores;
//     data['app_version'] = this.appVersion;
//     data['created_at'] = this.createdAt;
//     return data;
//   }
// }
//
// class Provider {
//   dynamic rating;
//
//   Provider({this.rating});
//
//   Provider.fromJson(Map<String, dynamic> json) {
//     rating = json['rating'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['rating'] = this.rating;
//     return data;
//   }
// }
//
// class Ride {
//   dynamic id;
//   dynamic rideTypeId;
//   dynamic  vehicleType;
//   dynamic  vehicleName;
//   dynamic  vehicleImage;
//   dynamic  vehicleMarker;
//   dynamic capacity;
//   dynamic status;
//   dynamic  keywords;
//   dynamic  homeDelivery;
//
//   Ride(
//       {this.id,
//         this.rideTypeId,
//         this.vehicleType,
//         this.vehicleName,
//         this.vehicleImage,
//         this.vehicleMarker,
//         this.capacity,
//         this.status,
//         this.keywords,
//         this.homeDelivery});
//
//   Ride.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     rideTypeId = json['ride_type_id'];
//     vehicleType = json['vehicle_type'];
//     vehicleName = json['vehicle_name'];
//     vehicleImage = json['vehicle_image'];
//     vehicleMarker = json['vehicle_marker'];
//     capacity = json['capacity'];
//     status = json['status'];
//     keywords = json['keywords'];
//     homeDelivery = json['home_delivery'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['ride_type_id'] = this.rideTypeId;
//     data['vehicle_type'] = this.vehicleType;
//     data['vehicle_name'] = this.vehicleName;
//     data['vehicle_image'] = this.vehicleImage;
//     data['vehicle_marker'] = this.vehicleMarker;
//     data['capacity'] = this.capacity;
//     data['status'] = this.status;
//     data['keywords'] = this.keywords;
//     data['home_delivery'] = this.homeDelivery;
//     return data;
//   }
// }
//
// class Rating {
//   dynamic id;
//   dynamic  adminService;
//   dynamic requestId;
//   dynamic userId;
//   dynamic providerId;
//   dynamic storeId;
//   dynamic companyId;
//   dynamic userRating;
//   dynamic providerRating;
//   dynamic storeRating;
//   dynamic userComment;
//   dynamic providerComment;
//   dynamic storeComment;
//   dynamic  createdType;
//   dynamic createdBy;
//   dynamic  modifiedType;
//   dynamic modifiedBy;
//   dynamic deletedType;
//   dynamic deletedBy;
//   dynamic  createdAt;
//   dynamic  updatedAt;
//   dynamic deletedAt;
//
//   Rating(
//       {this.id,
//         this.adminService,
//         this.requestId,
//         this.userId,
//         this.providerId,
//         this.storeId,
//         this.companyId,
//         this.userRating,
//         this.providerRating,
//         this.storeRating,
//         this.userComment,
//         this.providerComment,
//         this.storeComment,
//         this.createdType,
//         this.createdBy,
//         this.modifiedType,
//         this.modifiedBy,
//         this.deletedType,
//         this.deletedBy,
//         this.createdAt,
//         this.updatedAt,
//         this.deletedAt});
//
//   Rating.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     adminService = json['admin_service'];
//     requestId = json['request_id'];
//     userId = json['user_id'];
//     providerId = json['provider_id'];
//     storeId = json['store_id'];
//     companyId = json['company_id'];
//     userRating = json['user_rating'];
//     providerRating = json['provider_rating'];
//     storeRating = json['store_rating'];
//     userComment = json['user_comment'];
//     providerComment = json['provider_comment'];
//     storeComment = json['store_comment'];
//     createdType = json['created_type'];
//     createdBy = json['created_by'];
//     modifiedType = json['modified_type'];
//     modifiedBy = json['modified_by'];
//     deletedType = json['deleted_type'];
//     deletedBy = json['deleted_by'];
//     createdAt = json['created_at'];
//     updatedAt = json['updated_at'];
//     deletedAt = json['deleted_at'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['admin_service'] = this.adminService;
//     data['request_id'] = this.requestId;
//     data['user_id'] = this.userId;
//     data['provider_id'] = this.providerId;
//     data['store_id'] = this.storeId;
//     data['company_id'] = this.companyId;
//     data['user_rating'] = this.userRating;
//     data['provider_rating'] = this.providerRating;
//     data['store_rating'] = this.storeRating;
//     data['user_comment'] = this.userComment;
//     data['provider_comment'] = this.providerComment;
//     data['store_comment'] = this.storeComment;
//     data['created_type'] = this.createdType;
//     data['created_by'] = this.createdBy;
//     data['modified_type'] = this.modifiedType;
//     data['modified_by'] = this.modifiedBy;
//     data['deleted_type'] = this.deletedType;
//     data['deleted_by'] = this.deletedBy;
//     data['created_at'] = this.createdAt;
//     data['updated_at'] = this.updatedAt;
//     data['deleted_at'] = this.deletedAt;
//     return data;
//   }
// }
//
// class Reasons {
//   dynamic id;
//   dynamic  service;
//   dynamic  type;
//   dynamic  reason;
//   dynamic  status;
//   dynamic createdType;
//   dynamic createdBy;
//   dynamic modifiedType;
//   dynamic modifiedBy;
//   dynamic deletedType;
//   dynamic deletedBy;
//
//   Reasons(
//       {this.id,
//         this.service,
//         this.type,
//         this.reason,
//         this.status,
//         this.createdType,
//         this.createdBy,
//         this.modifiedType,
//         this.modifiedBy,
//         this.deletedType,
//         this.deletedBy});
//
//   Reasons.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     service = json['service'];
//     type = json['type'];
//     reason = json['reason'];
//     status = json['status'];
//     createdType = json['created_type'];
//     createdBy = json['created_by'];
//     modifiedType = json['modified_type'];
//     modifiedBy = json['modified_by'];
//     deletedType = json['deleted_type'];
//     deletedBy = json['deleted_by'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['service'] = this.service;
//     data['type'] = this.type;
//     data['reason'] = this.reason;
//     data['status'] = this.status;
//     data['created_type'] = this.createdType;
//     data['created_by'] = this.createdBy;
//     data['modified_type'] = this.modifiedType;
//     data['modified_by'] = this.modifiedBy;
//     data['deleted_type'] = this.deletedType;
//     data['deleted_by'] = this.deletedBy;
//     return data;
//   }
// }
//
// class Emergency {
//   dynamic  number;
//
//   Emergency({this.number});
//
//   Emergency.fromJson(Map<String, dynamic> json) {
//     number = json['number'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['number'] = this.number;
//     return data;
//   }
// }
