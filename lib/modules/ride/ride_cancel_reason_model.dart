RideCancelReasonsModel rideCancelReasonsModelFromJson(str) {
  return RideCancelReasonsModel.fromJson(str);
}

class RideCancelReasonsModel {
  String? statusCode;
  String? title;
  String? message;
  List<CancelReasonResponseData>? responseData;
  // List<Null>? responseNewData;
  List<Null>? error;

  RideCancelReasonsModel(
      {this.statusCode,
        this.title,
        this.message,
        this.responseData,
        // this.responseNewData,
        this.error});

  RideCancelReasonsModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    if (json['responseData'] != null) {
      responseData = <CancelReasonResponseData>[];
      json['responseData'].forEach((v) {
        responseData!.add(new CancelReasonResponseData.fromJson(v));
      });
    }
    // if (json['responseNewData'] != null) {
    //   responseNewData = <Null>[];
    //   json['responseNewData'].forEach((v) {
    //     responseNewData!.add(new Null.fromJson(v));
    //   });
    // }
    if (json['error'] != null) {
      error = <Null>[];
      json['error'].forEach((v) {
        error!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['title'] = this.title;
    data['message'] = this.message;
    if (this.responseData != null) {
      data['responseData'] = this.responseData!.map((v) => v.toJson()).toList();
    }
    // if (this.responseNewData != null) {
    //   data['responseNewData'] =
    //       this.responseNewData!.map((v) => v.toJson()).toList();
    // }
    // if (this.error != null) {
    //   data['error'] = this.error!.map((v) => v.toJson()).toList();
    // }
    return data;
  }
}

class CancelReasonResponseData {
  int? id;
  String? service;
  String? type;
  String? reason;
  String? status;
  Null? createdType;
  Null? createdBy;
  Null? modifiedType;
  Null? modifiedBy;
  Null? deletedType;
  Null? deletedBy;

  CancelReasonResponseData(
      {this.id,
        this.service,
        this.type,
        this.reason,
        this.status,
        this.createdType,
        this.createdBy,
        this.modifiedType,
        this.modifiedBy,
        this.deletedType,
        this.deletedBy});

  CancelReasonResponseData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    service = json['service'];
    type = json['type'];
    reason = json['reason'];
    status = json['status'];
    createdType = json['created_type'];
    createdBy = json['created_by'];
    modifiedType = json['modified_type'];
    modifiedBy = json['modified_by'];
    deletedType = json['deleted_type'];
    deletedBy = json['deleted_by'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['service'] = this.service;
    data['type'] = this.type;
    data['reason'] = this.reason;
    data['status'] = this.status;
    data['created_type'] = this.createdType;
    data['created_by'] = this.createdBy;
    data['modified_type'] = this.modifiedType;
    data['modified_by'] = this.modifiedBy;
    data['deleted_type'] = this.deletedType;
    data['deleted_by'] = this.deletedBy;
    return data;
  }
}
