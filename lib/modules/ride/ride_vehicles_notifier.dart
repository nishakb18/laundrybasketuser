
import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/api/api_response_state2.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class RideVehiclesNotifier extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  RideVehiclesNotifier(this._apiRepository) : super(ResponseState2(isLoading: true));

  Future<void> getRideVehicles(
      {bool init = true, required double lat, required double long}) async {
    try {
      if (init) state = state.copyWith(isLoading: true);
      final rideVehicles =
          await _apiRepository.fetchRideVehicles(lat: lat, long: long);
      state = state.copyWith(
          response: rideVehicles, isLoading: false, isError: false);
    } catch (e) {
      state = state.copyWith(
          errorMessage: e.toString(), isLoading: false, isError: true);
    }
  }
 /* Future<void> getEstimateFare( {bool init = true, required double sLat, required double sLong, required double dLat, required double dLong,  required int serviceType, String payMode="CASH", int cardId=1})
  async {
    try {
      if (init) state = state.copyWith(isLoading: true);
      final rideVehicles =
      await _apiRepository.fetchEstimateFare(dLat: dLat,dLong: dLong,sLat: sLat,sLong: sLong,payMode:  payMode,cardId: cardId,serviceTypeId: serviceType);
      state = state.copyWith(
          response: rideVehicles, isLoading: false, isError: false);
    } catch (e) {
      state = state.copyWith(
          errorMessage: e.toString(), isLoading: false, isError: true);
    }
  }*/

}
