import 'dart:ui';

import 'package:app24_user_app/constants/global_constants.dart';
import 'package:app24_user_app/models/location_model.dart';
import 'package:app24_user_app/modules/ride/ride_vehicle_model.dart';
import 'package:app24_user_app/modules/ride/ride_vehicle_pricing.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class RidePageNew extends StatefulWidget {
  final LocationModel initializedDropLocation;

  const RidePageNew({Key? key, required this.initializedDropLocation})
      : super(key: key);

  @override
  _RidePageNewState createState() => _RidePageNewState();
}

class _RidePageNewState extends State<RidePageNew> {
  double _panelHeightOpen = 0;
  double _panelHeightClosed = 100.0;
  PanelController _pc = new PanelController();

  late LocationModel pickUpLocation;
  late LocationModel dropLocation;

  final pickUpAddressController = TextEditingController();
  final dropAddressController = TextEditingController();
  late VehicleServices selectedVehicle;

  //late RideVariablesNotifier rideVariablesNotifier;

  //Set<Marker> markers = {};
  //Marker? pickUpMarker;
  //Marker? dropMarker;

  //PolylinePoints? polylinePoints;
  //Map<PolylineId, Polyline>? polyLines = {};
  //List<LatLng> polylineCoordinates = [];

  // bool _showGoogleMaps = false;
  int selectedVehicleIndex = -1;
  // VehicleServices? selectedVehicle;

  bool isPickUpLocationFetching = true;

  String distance = '';

  //String time = '15 mins'; we can get estimated time from polyline package from override function

  double totalDistance = 0.0;

  // BitmapDescriptor? pickUpIcon;
  // BitmapDescriptor? dropIcon;

  List<Marker>? markersList = [];
  late final MapController mapController;

  @override
  void initState() {
    mapController = MapController();
    initializeDropLocation();
    initializePickUpLocation();
    super.initState();
  }

  initializeDropLocation() {
    dropLocation = widget.initializedDropLocation;
    dropAddressController.text = dropLocation.description!;
    addMarker(
        lat: dropLocation.latitude!,
        long: dropLocation.longitude!,
        type: "drop");
  }

  initializePickUpLocation() {
    getCurrentLocation().then((value) {
      //showLog(value);
      if (value != null && value.description!.isNotEmpty) {
        LocationModel locationModel = value;
        setState(() {
          pickUpLocation = locationModel;
          pickUpAddressController.text = pickUpLocation.description!;
          context.read(homeNotifierProvider.notifier).currentLocation =
              locationModel;
          isPickUpLocationFetching = false;
          addMarker(
              lat: pickUpLocation.latitude!,
              long: pickUpLocation.longitude!,
              type: "pick_up");
          //loadData(context: context);
          updateMapBounds();
        });
      } else {
        setState(() {
          isPickUpLocationFetching = false;
        });
      }
    });
  }

  addMarker({required String type, required double lat, required double long}) {
    markersList!.add(Marker(
        point: LatLng(lat, long),
        child:
            // builder: (ctx) =>
            type == "pick_up" ? buildPickUpIconMarker() : buildDropIconMarker(),
        height: 25));
  }

  selectLocation(String title) {
    showSelectLocationSheet(context: context, title: title).then((value) {
      if (value != null) {
        LocationModel model = value;
        setState(() {
          if (title == GlobalConstants.labelPickupLocation) {
            pickUpLocation = model;
            pickUpAddressController.text = model.description!;
            //loadData(context: context);
            //updateMapWithLocations();
            //setPickUpMarker();
          } else {
            dropLocation = model;
            dropAddressController.text = model.description!;
            //updateMapWithLocations();
            //setDropMarker();
          }
        });
      }
    });
  }

  updateMapBounds() {
    try {
      var bounds = LatLngBounds(
          LatLng(pickUpLocation.latitude!, pickUpLocation.longitude!),
          LatLng(dropLocation.latitude!, dropLocation.longitude!));
      bounds
          .extend(LatLng(pickUpLocation.latitude!, pickUpLocation.longitude!));
      bounds.extend(LatLng(dropLocation.latitude!, dropLocation.longitude!));

      mapController.fitBounds(
        bounds,
        options: FitBoundsOptions(
          padding: EdgeInsets.only(left: 15.0, right: 15.0),
        ),
      );
    } catch (e) {
      print("error is : ${e.toString()}");
    }
  }

  @override
  Widget build(BuildContext context) {
    _panelHeightOpen = MediaQuery.of(context).size.height * .80;
    _panelHeightClosed = MediaQuery.of(context).size.height * .20;

    return Material(
      child: Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[
          SlidingUpPanel(
            maxHeight: _panelHeightOpen,
            minHeight: _panelHeightClosed,
            parallaxEnabled: true,
            parallaxOffset: .5,
            panelSnapping: true,
            body: _body(),
            controller: _pc,
            panelBuilder: (sc) => _panel(sc),
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(18.0),
                topRight: Radius.circular(18.0)),
          ),

          Positioned(
              top: 0,
              child: ClipRRect(
                  child: BackdropFilter(
                      filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).padding.top,
                        color: Colors.transparent,
                      )))),

          //the SlidingUpPanel Back
          Positioned(
            top: 52.0,
            child: InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => VehiclePricingPage(
                            selectedVehicle: selectedVehicle)));
              },
              child: Container(
                padding: const EdgeInsets.fromLTRB(24.0, 18.0, 24.0, 18.0),
                child: Text(
                  "Back",
                  style: TextStyle(fontWeight: FontWeight.w500),
                ),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(24.0),
                  boxShadow: [
                    BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, .25), blurRadius: 16.0)
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _panel(ScrollController sc) {
    return MediaQuery.removePadding(
        context: context,
        removeTop: true,
        child: ListView(
          controller: sc,
          children: <Widget>[
            const SizedBox(
              height: 12.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: 30,
                  height: 5,
                  decoration: BoxDecoration(
                      color: Colors.grey[300],
                      borderRadius: BorderRadius.all(Radius.circular(12.0))),
                ),
              ],
            ),
            const SizedBox(
              height: 18.0,
            ),
            buildTextFields(),
            const SizedBox(
              height: 24,
            ),
          ],
        ));
  }

  Widget _body() {
    return FlutterMap(
        mapController: mapController,
        options: MapOptions(
          center: LatLng(20.5937, 78.9629),
          zoom: 13,
          maxZoom: 15,
        ),
        children: [
          // layers: [
          //   TileLayerOptions(
          //       urlTemplate: "https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png"),
          //   MarkerLayerOptions(markers: markersList != null ? markersList! : []),
        ]);
  }

  Widget buildPickUpIconMarker() {
    return Container(
      decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          shape: BoxShape.circle,
          border: Border.all(
              color: Theme.of(context).primaryColorLight, width: 5.0)),
      padding: EdgeInsets.all(5),
    );
  }

  Widget buildDropIconMarker() {
    return Container(
      decoration: BoxDecoration(
          color: Colors.red[500],
          shape: BoxShape.circle,
          border: Border.all(color: Colors.red[200]!, width: 5.0)),
      padding: EdgeInsets.all(5),
    );
  }

  Widget buildTextFields() {
    return Padding(
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Container(
          color: Colors.white,
          child: Row(
            children: <Widget>[
              Column(
                children: <Widget>[
                  buildPickUpIconMarker(),
                  const SizedBox(
                    height: 35,
                    child: VerticalDivider(
                      thickness: 1.3,
                    ),
                  ),
                  buildDropIconMarker(),
                ],
              ),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                child: Column(
                  children: <Widget>[
                    TextField(
                      style: TextStyle(fontWeight: FontWeight.w500),
                      controller: pickUpAddressController,
                      decoration: getDecoration(context).copyWith(
                        labelText: GlobalConstants.labelPickupLocation,
                        suffixIcon: isPickUpLocationFetching
                            ? CupertinoActivityIndicator(
                                radius: 10,
                              )
                            : Icon(Icons.search),
                      ),
                      readOnly: true,
                      onTap: () {
                        selectLocation(GlobalConstants.labelPickupLocation);
                      },
                    ),
                    const SizedBox(
                      height: 7,
                    ),
                    TextField(
                      style: TextStyle(fontWeight: FontWeight.w500),
                      controller: dropAddressController,
                      decoration: getDecoration(context).copyWith(
                        labelText: GlobalConstants.labelDropLocation,
                        suffixIcon: Icon(Icons.search),
                      ),
                      readOnly: true,
                      onTap: () {
                        selectLocation(GlobalConstants.labelDropLocation);
                      },
                    ),
                    /*  Container(
                        color:Colors.yellowAccent,
                        height:100,child:Text("ahdfvbajehfb"))*/
                  ],
                ),
              )
            ],
          ),
        ));
  }
}
