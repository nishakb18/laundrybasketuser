SelectBidModel selectBidModelFromJson(str) {
  return SelectBidModel.fromJson(str);
}



class SelectBidModel {
  String? statusCode;
  String? title;
  String? message;
  ResponseData? responseData;

  List<Null>? error;

  SelectBidModel(
      {this.statusCode,
        this.title,
        this.message,
        this.responseData,

        this.error});

  SelectBidModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    responseData = json['responseData'] != null
        ? new ResponseData.fromJson(json['responseData'])
        : null;
    if (json['error'] != null) {
      error = <Null>[];
      json['error'].forEach((v) {
        error!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['title'] = this.title;
    data['message'] = this.message;
    if (this.responseData != null) {
      data['responseData'] = this.responseData!.toJson();
    }
    // if (this.error != null) {
    //   data['error'] = this.error!.map((v) => v.toJson()).toList();
    // }
    return data;
  }
}

class ResponseData {
  int? id;
  String? vehicleType;
  Null finishedVehicleType;
  String? bookingId;
  String? adminService;
  int? userId;
  List<ProviderBiddingDetails>? providerBiddingDetails;
  String? providerId;
  int? abaId;
  int? providerVehicleId;
  int? providerServiceId;
  int? rideTypeId;
  int? geofenceId;
  Null? rentalPackageId;
  int? rideDeliveryId;
  int? cityId;
  int? countryId;
  int? promocodeId;
  int? someone;
  String? status;
  String? providerRejectId;
  Null? cancelledBy;
  Null? cancelReason;
  String? paymentMode;
  int? paid;
  String? isTrack;
  String? calculator;
  double? distance;
  Null? someoneMobile;
  String? locationPoints;
  String? timezone;
  Null? someoneEmail;
  Null? travelTime;
  String? sAddress;
  double? sLatitude;
  double? sLongitude;
  String? dAddress;
  double? dLatitude;
  double? dLongitude;
  int? trackDistance;
  String? estimateFare;
  String? minFare;
  String? maxFare;
  int? isDropLocation;
  String? destinationLog;
  String? unit;
  String? currency;
  double? trackLatitude;
  double? trackLongitude;
  String? otp;
  String? assignedAt;
  Null? scheduleAt;
  Null? startedAt;
  Null? finishedAt;
  Null? returnDate;
  String? isScheduled;
  String? requestType;
  int? travelledDistance;
  int? extraDistance;
  int? extraDistancePrice;
  Null? peakHourId;
  int? userRated;
  int? providerRated;
  int? useWallet;
  int? surge;
  String? routeKey;
  Null? adminId;
  String? createdType;
  String? createdAt;
  String? createdTime;
  String? assignedTime;
  String? scheduleTime;
  String? startedTime;
  String? finishedTime;
  User? user;

  ResponseData(
      {this.id,
        this.vehicleType,
        this.finishedVehicleType,
        this.bookingId,
        this.adminService,
        this.userId,
        this.providerBiddingDetails,
        this.providerId,
        this.abaId,
        this.providerVehicleId,
        this.providerServiceId,
        this.rideTypeId,
        this.geofenceId,
        this.rentalPackageId,
        this.rideDeliveryId,
        this.cityId,
        this.countryId,
        this.promocodeId,
        this.someone,
        this.status,
        this.providerRejectId,
        this.cancelledBy,
        this.cancelReason,
        this.paymentMode,
        this.paid,
        this.isTrack,
        this.calculator,
        this.distance,
        this.someoneMobile,
        this.locationPoints,
        this.timezone,
        this.someoneEmail,
        this.travelTime,
        this.sAddress,
        this.sLatitude,
        this.sLongitude,
        this.dAddress,
        this.dLatitude,
        this.dLongitude,
        this.trackDistance,
        this.estimateFare,
        this.minFare,
        this.maxFare,
        this.isDropLocation,
        this.destinationLog,
        this.unit,
        this.currency,
        this.trackLatitude,
        this.trackLongitude,
        this.otp,
        this.assignedAt,
        this.scheduleAt,
        this.startedAt,
        this.finishedAt,
        this.returnDate,
        this.isScheduled,
        this.requestType,
        this.travelledDistance,
        this.extraDistance,
        this.extraDistancePrice,
        this.peakHourId,
        this.userRated,
        this.providerRated,
        this.useWallet,
        this.surge,
        this.routeKey,
        this.adminId,
        this.createdType,
        this.createdAt,
        this.createdTime,
        this.assignedTime,
        this.scheduleTime,
        this.startedTime,
        this.finishedTime,
        this.user});

  ResponseData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    vehicleType = json['vehicle_type'];
    finishedVehicleType = json['finished_vehicle_type'];
    bookingId = json['booking_id'];
    adminService = json['admin_service'];
    userId = json['user_id'];
    if (json['provider_bidding_details'] != null) {
      providerBiddingDetails = <ProviderBiddingDetails>[];
      json['provider_bidding_details'].forEach((v) {
        providerBiddingDetails!.add(new ProviderBiddingDetails.fromJson(v));
      });
    }
    providerId = json['provider_id'];
    abaId = json['aba_id'];
    providerVehicleId = json['provider_vehicle_id'];
    providerServiceId = json['provider_service_id'];
    rideTypeId = json['ride_type_id'];
    geofenceId = json['geofence_id'];
    rentalPackageId = json['rental_package_id'];
    rideDeliveryId = json['ride_delivery_id'];
    cityId = json['city_id'];
    countryId = json['country_id'];
    promocodeId = json['promocode_id'];
    someone = json['someone'];
    status = json['status'];
    providerRejectId = json['provider_reject_id'];
    cancelledBy = json['cancelled_by'];
    cancelReason = json['cancel_reason'];
    paymentMode = json['payment_mode'];
    paid = json['paid'];
    isTrack = json['is_track'];
    calculator = json['calculator'];
    distance = json['distance'];
    someoneMobile = json['someone_mobile'];
    locationPoints = json['location_points'];
    timezone = json['timezone'];
    someoneEmail = json['someone_email'];
    travelTime = json['travel_time'];
    sAddress = json['s_address'];
    sLatitude = json['s_latitude'];
    sLongitude = json['s_longitude'];
    dAddress = json['d_address'];
    dLatitude = json['d_latitude'];
    dLongitude = json['d_longitude'];
    trackDistance = json['track_distance'];
    estimateFare = json['estimate_fare'];
    minFare = json['min_fare'];
    maxFare = json['max_fare'];
    isDropLocation = json['is_drop_location'];
    destinationLog = json['destination_log'];
    unit = json['unit'];
    currency = json['currency'];
    trackLatitude = json['track_latitude'];
    trackLongitude = json['track_longitude'];
    otp = json['otp'];
    assignedAt = json['assigned_at'];
    scheduleAt = json['schedule_at'];
    startedAt = json['started_at'];
    finishedAt = json['finished_at'];
    returnDate = json['return_date'];
    isScheduled = json['is_scheduled'];
    requestType = json['request_type'];
    travelledDistance = json['travelled_distance'];
    extraDistance = json['extra_distance'];
    extraDistancePrice = json['extra_distance_price'];
    peakHourId = json['peak_hour_id'];
    userRated = json['user_rated'];
    providerRated = json['provider_rated'];
    useWallet = json['use_wallet'];
    surge = json['surge'];
    routeKey = json['route_key'];
    adminId = json['admin_id'];
    createdType = json['created_type'];
    createdAt = json['created_at'];
    createdTime = json['created_time'];
    assignedTime = json['assigned_time'];
    scheduleTime = json['schedule_time'];
    startedTime = json['started_time'];
    finishedTime = json['finished_time'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['vehicle_type'] = this.vehicleType;
    data['finished_vehicle_type'] = this.finishedVehicleType;
    data['booking_id'] = this.bookingId;
    data['admin_service'] = this.adminService;
    data['user_id'] = this.userId;
    if (this.providerBiddingDetails != null) {
      data['provider_bidding_details'] =
          this.providerBiddingDetails!.map((v) => v.toJson()).toList();
    }
    data['provider_id'] = this.providerId;
    data['aba_id'] = this.abaId;
    data['provider_vehicle_id'] = this.providerVehicleId;
    data['provider_service_id'] = this.providerServiceId;
    data['ride_type_id'] = this.rideTypeId;
    data['geofence_id'] = this.geofenceId;
    data['rental_package_id'] = this.rentalPackageId;
    data['ride_delivery_id'] = this.rideDeliveryId;
    data['city_id'] = this.cityId;
    data['country_id'] = this.countryId;
    data['promocode_id'] = this.promocodeId;
    data['someone'] = this.someone;
    data['status'] = this.status;
    data['provider_reject_id'] = this.providerRejectId;
    data['cancelled_by'] = this.cancelledBy;
    data['cancel_reason'] = this.cancelReason;
    data['payment_mode'] = this.paymentMode;
    data['paid'] = this.paid;
    data['is_track'] = this.isTrack;
    data['calculator'] = this.calculator;
    data['distance'] = this.distance;
    data['someone_mobile'] = this.someoneMobile;
    data['location_points'] = this.locationPoints;
    data['timezone'] = this.timezone;
    data['someone_email'] = this.someoneEmail;
    data['travel_time'] = this.travelTime;
    data['s_address'] = this.sAddress;
    data['s_latitude'] = this.sLatitude;
    data['s_longitude'] = this.sLongitude;
    data['d_address'] = this.dAddress;
    data['d_latitude'] = this.dLatitude;
    data['d_longitude'] = this.dLongitude;
    data['track_distance'] = this.trackDistance;
    data['estimate_fare'] = this.estimateFare;
    data['min_fare'] = this.minFare;
    data['max_fare'] = this.maxFare;
    data['is_drop_location'] = this.isDropLocation;
    data['destination_log'] = this.destinationLog;
    data['unit'] = this.unit;
    data['currency'] = this.currency;
    data['track_latitude'] = this.trackLatitude;
    data['track_longitude'] = this.trackLongitude;
    data['otp'] = this.otp;
    data['assigned_at'] = this.assignedAt;
    data['schedule_at'] = this.scheduleAt;
    data['started_at'] = this.startedAt;
    data['finished_at'] = this.finishedAt;
    data['return_date'] = this.returnDate;
    data['is_scheduled'] = this.isScheduled;
    data['request_type'] = this.requestType;
    data['travelled_distance'] = this.travelledDistance;
    data['extra_distance'] = this.extraDistance;
    data['extra_distance_price'] = this.extraDistancePrice;
    data['peak_hour_id'] = this.peakHourId;
    data['user_rated'] = this.userRated;
    data['provider_rated'] = this.providerRated;
    data['use_wallet'] = this.useWallet;
    data['surge'] = this.surge;
    data['route_key'] = this.routeKey;
    data['admin_id'] = this.adminId;
    data['created_type'] = this.createdType;
    data['created_at'] = this.createdAt;
    data['created_time'] = this.createdTime;
    data['assigned_time'] = this.assignedTime;
    data['schedule_time'] = this.scheduleTime;
    data['started_time'] = this.startedTime;
    data['finished_time'] = this.finishedTime;
    if (this.user != null) {
      data['user'] = this.user!.toJson();
    }
    return data;
  }
}

class ProviderBiddingDetails {
  int? providerId;
  double? bidAmount;
  String? bidAmountRs;
  String? eta;

  ProviderBiddingDetails(
      {this.providerId, this.bidAmount, this.bidAmountRs, this.eta});

  ProviderBiddingDetails.fromJson(Map<String, dynamic> json) {
    providerId = json['provider_id'];
    bidAmount = json['bid_amount'];
    bidAmountRs = json['bid_amount_rs'];
    eta = json['eta'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['provider_id'] = this.providerId;
    data['bid_amount'] = this.bidAmount;
    data['bid_amount_rs'] = this.bidAmountRs;
    data['eta'] = this.eta;
    return data;
  }
}

class User {
  int? id;
  String? firstName;
  String? lastName;
  int? walletBalance;
  String? userType;
  bool? email;
  String? paymentMode;
  String? mobile;
  String? gender;
  String? countryCode;
  String? currencySymbol;
  Null? picture;
  Null? address;
  Null? pincode;
  String? loginBy;
  Null? latitude;
  Null? longitude;
  int? rating;
  String? language;
  String? referralUniqueId;
  int? countryId;
  int? stateId;
  int? cityId;
  int? companyId;
  int? status;
  Null? favouriteStores;
  String? appVersion;
  String? createdAt;

  User(
      {this.id,
        this.firstName,
        this.lastName,
        this.walletBalance,
        this.userType,
        this.email,
        this.paymentMode,
        this.mobile,
        this.gender,
        this.countryCode,
        this.currencySymbol,
        this.picture,
        this.address,
        this.pincode,
        this.loginBy,
        this.latitude,
        this.longitude,
        this.rating,
        this.language,
        this.referralUniqueId,
        this.countryId,
        this.stateId,
        this.cityId,
        this.companyId,
        this.status,
        this.favouriteStores,
        this.appVersion,
        this.createdAt});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    walletBalance = json['wallet_balance'];
    userType = json['user_type'];
    email = json['email'];
    paymentMode = json['payment_mode'];
    mobile = json['mobile'];
    gender = json['gender'];
    countryCode = json['country_code'];
    currencySymbol = json['currency_symbol'];
    picture = json['picture'];
    address = json['address'];
    pincode = json['pincode'];
    loginBy = json['login_by'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    rating = json['rating'];
    language = json['language'];
    referralUniqueId = json['referral_unique_id'];
    countryId = json['country_id'];
    stateId = json['state_id'];
    cityId = json['city_id'];
    companyId = json['company_id'];
    status = json['status'];
    favouriteStores = json['favourite_stores'];
    appVersion = json['app_version'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['wallet_balance'] = this.walletBalance;
    data['user_type'] = this.userType;
    data['email'] = this.email;
    data['payment_mode'] = this.paymentMode;
    data['mobile'] = this.mobile;
    data['gender'] = this.gender;
    data['country_code'] = this.countryCode;
    data['currency_symbol'] = this.currencySymbol;
    data['picture'] = this.picture;
    data['address'] = this.address;
    data['pincode'] = this.pincode;
    data['login_by'] = this.loginBy;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['rating'] = this.rating;
    data['language'] = this.language;
    data['referral_unique_id'] = this.referralUniqueId;
    data['country_id'] = this.countryId;
    data['state_id'] = this.stateId;
    data['city_id'] = this.cityId;
    data['company_id'] = this.companyId;
    data['status'] = this.status;
    data['favourite_stores'] = this.favouriteStores;
    data['app_version'] = this.appVersion;
    data['created_at'] = this.createdAt;
    return data;
  }
}
