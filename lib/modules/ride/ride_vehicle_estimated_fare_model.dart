
EstimatedFareModel rideEstimatedFareModelFromJson(str) {
  return EstimatedFareModel.fromJson(str);
}



class EstimatedFareModel {
  String? statusCode;
  String? title;
  String? message;
  ResponseData? responseData;
  List<dynamic>? responseNewData;
  List<dynamic>? error;

  EstimatedFareModel(
      { this.statusCode,
       this.title,
       this.message,
       this.responseData,
       this.responseNewData,
       this.error});

  EstimatedFareModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    responseData = (json['responseData'] != null
        ? new ResponseData.fromJson(json['responseData'])
        : null)!;
    if (json['responseNewData'] != null) {
      responseNewData = new List.empty(growable: true);
      json['responseNewData'].forEach((v) {
        responseNewData!.add(v);
      });
    }
    if (json['error'] != null) {
      error = new List.empty(growable: true);
      json['error'].forEach((v) {
        error!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['title'] = this.title;
    data['message'] = this.message;
    if (this.responseData != null) {
      data['responseData'] = this.responseData!.toJson();
    }
    if (this.responseNewData != null) {
      data['responseNewData'] =
          this.responseNewData!.map((v) => v.toJson()).toList();
    }
    if (this.error != null) {
      data['error'] = this.error!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ResponseData {
    Fare? fare;
    Service? service;
  List<dynamic>? promocodes;
  String? unit;
  String? currency;

  ResponseData(
      {
        this.fare,
        this.service,
        this.promocodes,
        this.unit,
        this.currency});

  ResponseData.fromJson(Map<String, dynamic> json) {
    fare = json['fare'] != null ? new Fare.fromJson(json['fare']) : null;
    service =
        json['service'] != null ? new Service.fromJson(json['service']) : null;
    if (json['promocodes'] != null) {
      promocodes = new List.empty(growable: true);
      json['promocodes'].forEach((v) {
        promocodes!.add(v);
      });
    }
    unit = json['unit'];
    currency = json['currency'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.fare != null) {
      data['fare'] = this.fare!.toJson();
    }
    if (this.service != null) {
      data['service'] = this.service!.toJson();
    }
    if (this.promocodes != null) {
      data['promocodes'] = this.promocodes!.map((v) => v.toJson()).toList();
    }
    data['unit'] = this.unit;
    data['currency'] = this.currency;
    return data;
  }
}

class Fare {
  double? estimatedFare;
  double? distance;
  String? time;
  double? taxPrice;
  int? basePrice;
  int? serviceType;
  String? service;
  int? peak;
  String? peakPercentage;
  int? walletBalance;

  Fare(
      {this.estimatedFare,
      this.distance,
      this.time,
      this.taxPrice,
      this.basePrice,
      this.serviceType,
      this.service,
      this.peak,
      this.peakPercentage,
      this.walletBalance});

  Fare.fromJson(Map<String, dynamic> json) {
    estimatedFare = json['estimated_fare'];
    distance = json['distance'];
    time = json['time'];
    taxPrice = json['tax_price'];
    basePrice = json['base_price'];
    serviceType = json['service_type'];
    service = json['service'];
    peak = json['peak'];
    peakPercentage = json['peak_percentage'];
    walletBalance = json['wallet_balance'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['estimated_fare'] = this.estimatedFare;
    data['distance'] = this.distance;
    data['time'] = this.time;
    data['tax_price'] = this.taxPrice;
    data['base_price'] = this.basePrice;
    data['service_type'] = this.serviceType;
    data['service'] = this.service;
    data['peak'] = this.peak;
    data['peak_percentage'] = this.peakPercentage;
    data['wallet_balance'] = this.walletBalance;
    return data;
  }
}

class Service {
  int? id;
  int? rideTypeId;
  String? vehicleType;
  String? vehicleName;
  String? vehicleImage;
  String? vehicleMarker;
  int? capacity;
  int? status;
  String? keywords;

  Service(
      {this.id,
      this.rideTypeId,
      this.vehicleType,
      this.vehicleName,
      this.vehicleImage,
      this.vehicleMarker,
      this.capacity,
      this.status,
      this.keywords});

  Service.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    rideTypeId = json['ride_type_id'];
    vehicleType = json['vehicle_type'];
    vehicleName = json['vehicle_name'];
    vehicleImage = json['vehicle_image'];
    vehicleMarker = json['vehicle_marker'];
    capacity = json['capacity'];
    status = json['status'];
    keywords = json['keywords'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['ride_type_id'] = this.rideTypeId;
    data['vehicle_type'] = this.vehicleType;
    data['vehicle_name'] = this.vehicleName;
    data['vehicle_image'] = this.vehicleImage;
    data['vehicle_marker'] = this.vehicleMarker;
    data['capacity'] = this.capacity;
    data['status'] = this.status;
    data['keywords'] = this.keywords;
    return data;
  }
}
