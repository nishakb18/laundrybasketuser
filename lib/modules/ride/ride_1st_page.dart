import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/constants/global_constants.dart';
import 'package:app24_user_app/models/location_model.dart';
import 'package:app24_user_app/modules/ride/ride.dart';
import 'package:app24_user_app/modules/ride/ride_new.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/widgets/common_appbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RidePageOpen extends StatefulWidget {
  @override
  _RidePageOpenState createState() => _RidePageOpenState();
}

class _RidePageOpenState extends State<RidePageOpen> {
  var top = 0.0;

  selectLocation() {
    showSelectLocationSheet(
            context: context, title: GlobalConstants.labelDropLocation)
        .then((value) {
      if (value != null) {
        LocationModel model = value;
        navigateToRidePage(model: model);
      }
    });
  }

  navigateToRidePage({required LocationModel model}) {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => RidePage(
                initializedDropLocation: model,
              )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: CustomAppBar(hideCartIcon: "true", title: "Taxi Services"),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Hero(
                tag: "1",
                child: Stack(
                  children: [
                    Container(
                        margin: const EdgeInsets.only(bottom: 30, top: 20),
                        child: Stack(children: [
                          ClipRRect(
                            borderRadius: const BorderRadius.only(
                                bottomLeft: Radius.circular(25),
                                bottomRight: Radius.circular(25)),
                            child: Image.asset(
                              App24UserAppImages.rideBG,
                              fit: BoxFit.fill,
                            ),
                          ),
                          Positioned(
                              bottom: 40,
                              left: 20,
                              child: Text(
                                "Ride a cab, auto, bike or bus",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w600,
                                    fontSize:
                                        MediaQuery.of(context).size.width / 17),
                              ))
                        ])),
                    Positioned(
                      left: 25,
                      right: 25,
                      bottom: 0,
                      child: Container(
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                                blurRadius: 15,
                                spreadRadius: 1,
                                color: Colors.grey[200]!,
                                offset: Offset(0, 9))
                          ],
                          borderRadius: BorderRadius.circular(20.0),
                          color: Colors.white,
                          // border: Border.all(color: Color(0xffF5EDDA))
                        ),
                        child: Material(
                          color: Colors.transparent,
                          child: InkWell(
                              onTap: () {
                                //select location
                                selectLocation();
                              },
                              borderRadius: BorderRadius.circular(20.0),
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 5, vertical: top > 80 ? 9 : 14),
                                child: Row(
                                  children: [
                                    Icon(
                                      Icons.location_on,
                                      color: App24Colors.yellowRideThemeColor,
                                      size: top > 80 ? 18 : 24,
                                    ),
                                    const SizedBox(
                                      width: 4.0,
                                    ),
                                    Expanded(
                                        child: Text(
                                      "Enter Drop location",
                                      style: TextStyle(
                                          color:
                                              App24Colors.yellowRideThemeColor,
                                          fontWeight: FontWeight.w600,
                                          fontSize: top > 80 ? 13 : 16),
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                    ))
                                  ],
                                ),
                              )),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(
                      height: 20,
                    ),
                    Text(
                      "Saved Locations",
                      style: TextStyle(
                          fontWeight: FontWeight.w800,
                          fontSize: MediaQuery.of(context).size.width / 19),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    ListTile(
                      contentPadding: const EdgeInsets.only(
                          bottom: 10, left: 15, right: 15),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15)),
                      onTap: () {},
                      title: Text(
                        "Home",
                        style: TextStyle(
                            color: App24Colors.yellowRideThemeColor,
                            fontWeight: FontWeight.w600,
                            fontSize: MediaQuery.of(context).size.width / 23),
                      ),
                      subtitle: Text(
                        "D-285,D-Block,Near Radix Malik Health Care,Nirman Vihar ",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: MediaQuery.of(context).size.width / 30),
                      ),
                      trailing: const Icon(
                        Icons.home,
                        color: App24Colors.yellowRideThemeColor,
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    ListTile(
                      contentPadding: const EdgeInsets.only(
                          bottom: 10, left: 15, right: 15),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15)),
                      onTap: () {},
                      title: Text(
                        "Work",
                        style: TextStyle(
                            color: App24Colors.yellowRideThemeColor,
                            fontWeight: FontWeight.w600,
                            fontSize: MediaQuery.of(context).size.width / 23),
                      ),
                      subtitle: Text(
                        "D-285,D-Block,Near Radix Malik Health Care,Nirman Vihar ",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: MediaQuery.of(context).size.width / 30),
                      ),
                      trailing: const Icon(
                        Icons.location_city_rounded,
                        color: App24Colors.yellowRideThemeColor,
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 20,
              )
            ],
          ),
        ),
      ),
    );
  }
}
