

import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/services/api/api_repository.dart';
import 'package:app24_user_app/utils/services/api/api_response_state2.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class RideSelectBidNotifier extends StateNotifier<ResponseState2> {
  final ApiRepository _apiRepository;

  RideSelectBidNotifier(this._apiRepository) : super(ResponseState2(isLoading: true));


  /* Future<void> getEstimateFare( {bool init = true, required double sLat, required double sLong, required double dLat, required double dLong,  required int serviceType, String payMode="CASH", int cardId=1})
  async {
    try {
      if (init) state = state.copyWith(isLoading: true);
      final rideVehicles =
      await _apiRepository.fetchEstimateFare(dLat: dLat,dLong: dLong,sLat: sLat,sLong: sLong,payMode:  payMode,cardId: cardId,serviceTypeId: serviceType);
      state = state.copyWith(
          response: rideVehicles, isLoading: false, isError: false);
    } catch (e) {
      state = state.copyWith(
          errorMessage: e.toString(), isLoading: false, isError: true);
    }
  }*/
  Future<void> selectBid(
      {bool init = true, Map? body}) async {
    try {
      if (init) state = state.copyWith(isLoading: true);

      final saveProfile = await _apiRepository.fetchSelectBid(body: body);

      state = state.copyWith(
          response: saveProfile, isLoading: false, isError: false);
    } catch (e) {
      print(e.toString());
      state = state.copyWith(
          errorMessage: e.toString(), isError: true, isLoading: false);
    }
  }





}
