import 'dart:async';
import 'dart:math';

import 'package:app24_user_app/app24_user_icons.dart';
import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/constants/global_constants.dart';
import 'package:app24_user_app/helpers.dart';
import 'package:app24_user_app/models/location_model.dart';
import 'package:app24_user_app/modules/ride/ride_completed.dart';
import 'package:app24_user_app/modules/ride/ride_details.dart';
import 'package:app24_user_app/providers/location_picker_provider.dart';
import 'package:app24_user_app/providers/ride_page_variables_provider.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/helpers/common_widgets.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/widgets/bottomsheets/cancellation_bottomSheet.dart';
import 'package:app24_user_app/widgets/common_main_button.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animarker/flutter_map_marker_animation.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

final rideVariablesNotifierProvider =
    StateNotifierProvider((ref) => RideVariablesNotifier());

// const kStartPosition = LatLng(10.024783, 76.308127);
// const kSantoDomingo = CameraPosition(target: kStartPosition, zoom: 15);
const kMarkerId = MarkerId('MarkerId1');
const kDuration = Duration(seconds: 2);

class TrackRidePage extends StatefulWidget {
  const TrackRidePage({
    Key? key,
    this.selectedVehicle,
    this.estimatedKm,
    this.pickUpAddress,
    this.dropAddress,
    this.providerName,
    this.pickUpLat,
    this.pickUpLng,
    this.dropLng,
    this.dropLat,
    this.providerId,
    this.providerVehicleModel,
    this.providerVehicleNumber,
    this.providerRating,
    this.providerNumber,
  }) : super(key: key);
  final String? selectedVehicle;
  final String? estimatedKm;
  final String? pickUpAddress;
  final String? dropAddress;
  final String? providerName;
  final String? providerVehicleModel;
  final int? providerVehicleNumber;
  final int? providerNumber;
  final int? providerRating;
  final int? providerId;
  final double? pickUpLat;
  final double? pickUpLng;
  final double? dropLng;
  final double? dropLat;

  @override
  _TrackRidePageState createState() => _TrackRidePageState();
}

class _TrackRidePageState extends State<TrackRidePage> {
  CameraPosition _initialLocation =
      CameraPosition(target: LatLng(20.5937, 78.9629), zoom: 4.0);
  GoogleMapController? mapController;

  final Location location = new Location();

  bool _showGoogleMaps = false;
  bool isMapLoaded = false;

  LocationNotifier? _locationPickerProvider;
  var _isLoadingForFirstTime = true;

  BitmapDescriptor? pickUpIcon;
  BitmapDescriptor? dropIcon;
  BitmapDescriptor? vehicleIcon;
  double totalDistance = 0.0;
  String distance = '';

  // String distance = '';

  Set<Marker?> markers = {};
  double? pickupLat;
  double? pickupLng;
  double? dropLat;
  double? dropLng;
  Marker? pickUpMarker;
  Marker? dropMarker;
  Marker? vehicleMarker;
  Timer? timer;
  var stream1;
  final database = FirebaseDatabase.instance.ref();
  String? providerLat;
  bool isPickUpLocationFetching = true;
  late LocationModel pickUpLocation;

  Map<PolylineId, Polyline>? polyLines = {};
  PolylinePoints? polylinePoints;
  Map<PolylineId, Polyline> polylines = {};
  List<LatLng> polylineCoordinates = [];
  LocationModel vehicleLocationModel =
      LocationModel(latitude: 10.416471, longitude: 76.279892);
  final controller = Completer<GoogleMapController>();
  final markers1 = <MarkerId, Marker>{};

  @override
  void initState() {
    super.initState();
    getIcons();
    Future.delayed(const Duration(milliseconds: 500), () {
      setState(() {
        _showGoogleMaps = true;
        initializeDropLocation();
        initializePickUpLocation();
        // selectLocation(GlobalConstants.labelPickupLocation);
        // selectLocation(GlobalConstants.labelDropLocation);
      });
      _activateListeners();
      selectLocation(GlobalConstants.labelDropLocation);
      selectLocation(GlobalConstants.labelPickupLocation);
    });

    timer = Timer.periodic(Duration(seconds: 10),
        (Timer t) => selectLocation(GlobalConstants.labelPickupLocation));
    timer = Timer.periodic(Duration(seconds: 10), (Timer t) {
      print("timer working");
      print(kLocations.toString());

      // stream1.forEach((value) => newLocationUpdate(value));
    });
  }

  static var kLocations = [
    // kStartPosition,
    // LatLng(10.023791, 76.307546),
    // LatLng(10.022955, 76.307001),
    // LatLng(10.022458, 76.306765),
    // LatLng(10.011663, 76.303335)
  ];

  selectLocation(String title) async {
    print("providerId" + widget.providerId.toString());

    DatabaseReference ref =
        FirebaseDatabase.instance.ref("Provider/${widget.providerId}/l/0");
    DatabaseReference refLng =
        FirebaseDatabase.instance.ref("Provider/${widget.providerId}/l/1");

    DatabaseEvent event = await ref.once();
    DatabaseEvent eventLng = await refLng.once();

    setState(() {
      // if (title == GlobalConstants.labelPickupLocation) {
      // DatabaseReference child = ref.child("0");

      Stream<DatabaseEvent> stream = ref.onValue;
      Stream<DatabaseEvent> streamLng = refLng.onValue;

// Subscribe to the stream!
      stream.listen((DatabaseEvent event) {
        print('Event Type: ${event.type}'); // DatabaseEventType.value;
        print('Snapshot: ${event.snapshot.value}'); // DataSnapshot
      });
      streamLng.listen((DatabaseEvent eventLng) {
        print('Event Type: ${eventLng.type}'); // DatabaseEventType.value;
        print('Snapshot: ${eventLng.snapshot.value}'); // DataSnapshot
      });

      setState(() {
        pickupLat = double.parse(event.snapshot.value.toString());
        pickupLng = double.parse(eventLng.snapshot.value.toString());

        if (kLocations.contains(LatLng(pickupLat!, pickupLng!))) {
        } else {
          kLocations.add(LatLng(pickupLat!, pickupLng!));
          if (kLocations.length > 1) {
            kLocations.removeRange(0, 1);
            print(kLocations);
          }
          print("printing klocation");
          print(kLocations);
          stream1 = Stream.periodic(kDuration, (count) => kLocations[count])
              .take(kLocations.length);
          stream1.forEach((value) => newLocationUpdate(value));
          print(kLocations.length);

          updateMapWithLocations();
          setPickUpMarker();
        }
      });

      //
      // } else {
      //   updateMapWithLocations();
      //   setDropMarker();
      // }
    });

    // }
    // });
  }

  updateMapWithLocations() async {
    double? lat = pickupLat;
    double? lng = pickupLng;
    await updateCameraLocation(
        LatLng(lat!, lng!), LatLng(dropLat!, dropLng!), mapController!);
    createPolyLines();
  }

  void newLocationUpdate(LatLng latLng) async {
    pickUpIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(
          devicePixelRatio: 2.5,
        ),
        App24UserAppImages.pickUpIcon);
    var marker = RippleMarker(
        icon: pickUpIcon,
        markerId: kMarkerId,
        position: latLng,
        ripple: false,
        flat: false);
    setState(() => markers1[kMarkerId] = marker);
  }

  void _activateListeners() {
    // database.once().then((DataSnapshot snapshot) => print(snapshot.value));
    database.child('Provider/${widget.providerId}').onValue.listen((event) {
      final Object? description = event.snapshot.value;
      setState(() {
        providerLat = description.toString();
      });
    });
  }

  initializeDropLocation() {
    dropLat = /*10.0463102*/ widget.dropLat;
    dropLng = /*76.3173948*/ widget.dropLng;
    // dropLocation = widget.initializedDropLocation!;
    // dropAddressController.text = dropLocation.description!;
    setDropMarker();
  }

  initializePickUpLocation() {
    print("entered initializePickUpLocation");
    getCurrentLocation().then((value) {
      // print(ente)
      //showLog(valyue);
      if (value != null && value.description!.isNotEmpty) {
        LocationModel locationModel = value;
        var lat = widget.pickUpLat;
        var lng = widget.pickUpLng;
        setState(() {
          print("lat inside setstate" + lat.toString());
          pickUpLocation = locationModel;
          pickupLat = lat;
          pickupLng = lng;
          // pickUpAddressController.text = pickUpLocation.description!;
          context.read(homeNotifierProvider.notifier).currentLocation =
              locationModel;
          isPickUpLocationFetching = false;
          setPickUpMarker();
          // loadData1(context: context);
          updateMapWithLocations();
        });
      } else {
        setState(() {
          isPickUpLocationFetching = false;
        });
      }
    });
  }

  Future<void> setDropMarker() async {
    dropIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 2.5), App24UserAppImages.dropIcon);

    setState(() {
      dropMarker = Marker(
          markerId: MarkerId('drop_marker'),
          position: LatLng(
            dropLat!,
            dropLng!,
          ),
          infoWindow: InfoWindow(
            title: 'Drop',
            // snippet: dropAddressController.text,
          ),
          icon: dropIcon!);
      if (markers.contains("drop_marker")) {
        markers.remove("drop_marker");
      }
      markers.add(dropMarker!);
    });
  }

  @override
  void didChangeDependencies() {
    if (_isLoadingForFirstTime) {
      //_locationPickerProvider.setAPIKey();
    }
    _isLoadingForFirstTime = false;
    super.didChangeDependencies();
  }

//   setInitialMapValues() {
//     setState(() {
//       isMapLoaded = true;
//       setPickUpMarker();
//       setDropMarker();
//       setVehicleMarker();
// /*      updateCameraLocation(
//           LatLng(vehicleLocationModel.latitude, vehicleLocationModel.longitude),
//           LatLng(_locationPickerProvider.dropLocation.latitude,
//               _locationPickerProvider.dropLocation.longitude),
//           mapController);*/
//       createVehiclePolyLines();
//       //createPolyLines();
//     });
//   }

  double _coordinateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return 12742 * asin(sqrt(a));
  }

  void getDistance() {
    totalDistance = 0.0;
    for (int i = 0; i < polylineCoordinates.length - 1; i++) {
      totalDistance += _coordinateDistance(
        polylineCoordinates[i].latitude,
        polylineCoordinates[i].longitude,
        polylineCoordinates[i + 1].latitude,
        polylineCoordinates[i + 1].longitude,
      );
    }
    showLog(totalDistance.toStringAsFixed(2) + "calculated distance");
    setState(() {
      distance = totalDistance.toStringAsFixed(2) + " Kms";
    });
  }

  void createPolyLines() async {
    // Initializing PolylinePoints
    polylinePoints = PolylinePoints();

    // Generating the list of coordinates to be used for
    // drawing the polyLines
    late PolylineResult result;
    late String mapKey;

    if (context.read(homeNotifierProvider.notifier).appConfiguration == null) {
      await context.read(homeNotifierProvider.notifier).getAppConfigurations();
    }

    mapKey = context.read(homeNotifierProvider.notifier).mapKey!;

    showLog("key is : $mapKey");

    result = await polylinePoints!.getRouteBetweenCoordinates(
      mapKey, // Google Maps API Key
      PointLatLng(widget.pickUpLat!, widget.pickUpLng!),
      PointLatLng(widget.dropLat!, widget.dropLng!),
    );

    // Adding the coordinates to the list
    if (result.points.isNotEmpty) {
      polylineCoordinates.clear();
      result.points.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
      getDistance();
    }

    // Defining an ID
    PolylineId id = PolylineId('poly');

    // Initializing Polyline
    Polyline polyline = Polyline(
      polylineId: id,
      color: Colors.black87,
      points: polylineCoordinates,
      width: 3,
    );

    // Adding the polyline to the map
    setState(() {
      polyLines!.clear();
      polyLines![id] = polyline;
    });
  }

//   void createVehiclePolyLines() async {
//     // Initializing PolylinePoints
//     polylinePoints = PolylinePoints();
//
//     // Generating the list of coordinates to be used for
//     // drawing the polylines
//     late PolylineResult result;
// /*    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
//         _locationPickerProvider.mapKey,
//         PointLatLng(
//             vehicleLocationModel.latitude, vehicleLocationModel.longitude),
//         PointLatLng(_locationPickerProvider.dropLocation.latitude,
//             _locationPickerProvider.dropLocation.longitude),
//         wayPoints: [
//           PolylineWayPoint(
//               location:
//                   "${_locationPickerProvider.pickUpLocation.latitude},${_locationPickerProvider.pickUpLocation.longitude}",
//               stopOver: false)
//         ]);*/
//     // Adding the coordinates to the list
// /*    if (result.points.isNotEmpty) {
//       */ /* setState(() {
//         distance = result.distance;
//       });*/ /*
//       // print(_locationPickerProvider.distance);
//       //polylineCoordinates.clear();
//       result.points.forEach((PointLatLng point) {
//         polylineCoordinates.add(LatLng(point.latitude, point.longitude));
//       });
//       //getDistance();
//     }*/
//
//     // Defining an ID
//     PolylineId id = PolylineId('poly');
//
//     // Initializing Polyline
//     Polyline polyline = Polyline(
//       polylineId: id,
//       color: Colors.black87,
//       points: polylineCoordinates,
//       width: 3,
//     );
//
//     // Adding the polyline to the map
//     setState(() {
//       polylines.clear();
//       polylines[id] = polyline;
//     });
//   }

//   void createPolyLines() async {
//     // Initializing PolylinePoints
//     // polylinePoints = PolylinePoints();
//
//     // Generating the list of coordinates to be used for
//     // drawing the polylines
//     late PolylineResult result;
// /*    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
//       _locationPickerProvider.mapKey, // Google Maps API Key
//       PointLatLng(_locationPickerProvider.pickUpLocation.latitude,
//           _locationPickerProvider.pickUpLocation.longitude),
//       PointLatLng(_locationPickerProvider.dropLocation.latitude,
//           _locationPickerProvider.dropLocation.longitude),
//     );*/
//     // Adding the coordinates to the list
// /*    if (result.points.isNotEmpty) {
//       */ /* setState(() {
//         distance = result.distance;
//       });*/ /*
//       //print(_locationPickerProvider.distance);
//       //polylineCoordinates.clear();
//       result.points.forEach((PointLatLng point) {
//         polylineCoordinates.add(LatLng(point.latitude, point.longitude));
//       });
//       //getDistance();
//     }*/
//
//     // Defining an ID
//     PolylineId id = PolylineId('polyline_2');
//
//     // Initializing Polyline
//     Polyline polyline = Polyline(
//       polylineId: id,
//       color: Colors.black87,
//       points: polylineCoordinates,
//       width: 3,
//     );
//
//     // Adding the polyline to the map
//     setState(() {
//       //polylines.clear();
//       polylines[id] = polyline;
//     });
//   }

  Future<void> setPickUpMarker() async {
    // pickUpIcon = await BitmapDescriptor.fromAssetImage(
    //     ImageConfiguration(
    //       devicePixelRatio: 2.5,
    //     ),
    //     App24UserAppImages.pickUpIcon);

    setState(() {
      print("pickup lat agan" + pickupLat.toString());
      // pickUpMarker = Marker(
      //     markerId: MarkerId('pickup_marker'),
      //     visible: true,
      //     position: LatLng(
      //       pickupLat!,
      //       pickupLng!,
      //     ),
      //     infoWindow: InfoWindow(
      //       title: "Delivery Partner",
      //       snippet: pickUpAddressController.text,
      //     ),
      //     icon: pickUpIcon!);
      if (markers.contains("pickup_marker")) {
        markers.remove("pickup_marker");
      }
      markers.add(pickUpMarker!);
    });
  }

//   void setVehicleMarker() {
//     setState(() {
//       vehicleMarker = Marker(
//           markerId: MarkerId('vehicle_marker'),
//           position: LatLng(
//             vehicleLocationModel.latitude!,
//             vehicleLocationModel.longitude!,
//           ),
// /*        infoWindow: InfoWindow(
//           title: 'Drop',
//           snippet: dropAddressController.text,
//         ),*/
//           icon: BitmapDescriptor.defaultMarker);
//       if (markers.contains("vehicle_marker")) {
//         markers.remove("vehicle_marker");
//       }
//       markers.add(vehicleMarker);
//     });
//   }

  getIcons() async {
    var pick_icon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(
          devicePixelRatio: 2.5,
        ),
        "assets/images/pick_up_icon.png");
    var drop_icon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 2.5),
        "assets/images/drop_icon.png");
    var vehicle_icon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 2.5), "assets/images/jeep_1.png");
    setState(() {
      pickUpIcon = pick_icon;
      dropIcon = drop_icon;
      vehicleIcon = vehicle_icon;
    });
  }

  Future<void> updateCameraLocation(
    LatLng source,
    LatLng destination,
    GoogleMapController mapController,
  ) async {
    if (mapController == null) return;

    LatLngBounds bounds;

    if (source.latitude > destination.latitude &&
        source.longitude > destination.longitude) {
      bounds = LatLngBounds(southwest: destination, northeast: source);
    } else if (source.longitude > destination.longitude) {
      bounds = LatLngBounds(
          southwest: LatLng(source.latitude, destination.longitude),
          northeast: LatLng(destination.latitude, source.longitude));
    } else if (source.latitude > destination.latitude) {
      bounds = LatLngBounds(
          southwest: LatLng(destination.latitude, source.longitude),
          northeast: LatLng(source.latitude, destination.longitude));
    } else {
      bounds = LatLngBounds(southwest: source, northeast: destination);
    }

    CameraUpdate cameraUpdate = CameraUpdate.newLatLngBounds(bounds, 150);

    return checkCameraLocation(cameraUpdate, mapController);
  }

  Future<void> checkCameraLocation(
      CameraUpdate cameraUpdate, GoogleMapController mapController) async {
    mapController.animateCamera(cameraUpdate);
    LatLngBounds l1 = await mapController.getVisibleRegion();
    LatLngBounds l2 = await mapController.getVisibleRegion();

    if (l1.southwest.latitude == -90 || l2.southwest.latitude == -90) {
      return checkCameraLocation(cameraUpdate, mapController);
    }
  }

  // List<Widget> get _content => [
  //
  //       Container(
  //           width: MediaQuery.of(context).size.width,
  //           decoration: BoxDecoration(
  //               borderRadius: BorderRadius.only(
  //                   topRight: Radius.circular(30),
  //                   topLeft: Radius.circular(30)),
  //               color: Colors.white),
  //           child: Column(
  //             crossAxisAlignment: CrossAxisAlignment.stretch,
  //             mainAxisSize: MainAxisSize.min,
  //             children: <Widget>[
  //               Row(mainAxisAlignment: MainAxisAlignment.center, children: [
  //                 Container(
  //                   margin: EdgeInsets.all(10),
  //                   alignment: Alignment.center,
  //                   width: 55,
  //                   height: 1.8,
  //                   decoration: BoxDecoration(
  //                     borderRadius: BorderRadius.circular(5),
  //                     color: Colors.grey[300],
  //                   ),
  //                 ),
  //               ]),
  //               Container(
  //                   alignment: Alignment.center,
  //                   padding: EdgeInsets.symmetric(vertical: 5, horizontal: 20),
  //                   child: getTitle("Driver on the way")),
  //               SizedBox(
  //                 height: 5,
  //               ),
  //               ListTile(
  //                 leading: Container(
  //                   height: 55,
  //                   width: 55,
  //                   decoration: BoxDecoration(
  //                       borderRadius: BorderRadius.circular(10),
  //                       boxShadow: [
  //                         BoxShadow(
  //                             color: Theme.of(context)
  //                                 .primaryColorLight
  //                                 .withOpacity(0.2),
  //                             blurRadius: 1,
  //                             spreadRadius: 1),
  //                       ],
  //                       color: Theme.of(context).cardColor),
  //                   child: Image.asset(
  //                     App24UserAppImages.rideCompleteDriverImage,
  //                     width: 50,
  //                     height: 50,
  //                   ),
  //                 ),
  //                 title: Row(
  //                   children: [
  //                     Flexible(
  //                       child: Text(
  //                         "Driver Name",
  //                         style: TextStyle(fontWeight: FontWeight.w600),
  //                       ),
  //                     ),
  //                     SizedBox(
  //                       width: 10,
  //                     ),
  //                     Row(
  //                       crossAxisAlignment: CrossAxisAlignment.end,
  //                       children: [
  //                         Text(
  //                           "4.8",
  //                           style: TextStyle(
  //                               fontWeight: FontWeight.w600, fontSize: 14),
  //                         ),
  //                         SizedBox(
  //                           width: 5,
  //                         ),
  //                         Icon(
  //                           Icons.star,
  //                           color: App24Colors.greenOrderEssentialThemeColor,
  //                           size: 17,
  //                         )
  //                       ],
  //                     )
  //                   ],
  //                 ),
  //                 subtitle: Column(
  //                   crossAxisAlignment: CrossAxisAlignment.stretch,
  //                   children: [
  //                     Text(
  //                       "Honda Civic",
  //                       style: TextStyle(fontWeight: FontWeight.w500),
  //                     ),
  //                     Text(
  //                       "KL 45 L 2836",
  //                       style: TextStyle(fontWeight: FontWeight.w500),
  //                     ),
  //                   ],
  //                 ),
  //                 trailing: FloatingActionButton(
  //                   mini: true,
  //                   backgroundColor: App24Colors.darkTextColor,
  //                   heroTag: null,
  //                   child: Icon(
  //                     Icons.call,
  //                     color: Colors.white,
  //                   ),
  //                   onPressed: () {
  //                     Navigator.push(
  //                       context,
  //                       MaterialPageRoute(
  //                           builder: (context) => RideDetailsPage(
  //                                 pickupAddress: widget.pickUpAddress,
  //                                 dropAddress: widget.dropAddress,
  //                                 selectedVehicleType: widget.selectedVehicle,
  //                                 estimatedKm: widget.estimatedKm,
  //                                 type: "payment",
  //                               )),
  //                     );
  //                   },
  //                 ),
  //                 isThreeLine: true,
  //               ),
  //               Divider(),
  //               Row(
  //                 mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  //                 children: [
  //                   TextButton(
  //                     child: Column(
  //                       children: [
  //                         Icon(
  //                           App24User.alarm_light,
  //                           size: 24,
  //                           color: App24Colors.darkTextColor,
  //                         ),
  //                         Text(
  //                           "SoS",
  //                           style: TextStyle(fontWeight: FontWeight.w500),
  //                         ),
  //                       ],
  //                     ),
  //                     onPressed: () {
  //                       Navigator.push(
  //                         context,
  //                         MaterialPageRoute(
  //                             builder: (context) => RideCompletedPage()),
  //                       );
  //                     },
  //                   ),
  //                   TextButton(
  //                     child: Column(
  //                       children: [
  //                         Icon(
  //                             // App24User.share,
  //                             // size: 24,
  //                             Icons.share),
  //                         Text(
  //                           "Share",
  //                           style: TextStyle(fontWeight: FontWeight.w500),
  //                         ),
  //                       ],
  //                     ),
  //                     onPressed: () {
  //                       Navigator.push(
  //                         context,
  //                         MaterialPageRoute(
  //                             builder: (context) => RideCompletedPage()),
  //                       );
  //                     },
  //                   ),
  //                   TextButton(
  //                     child: Column(
  //                       children: [
  //                         Icon(
  //                           Icons.close,
  //                           size: 24,
  //                           color: Colors.red,
  //                         ),
  //                         Text(
  //                           "Cancel",
  //                           style: TextStyle(fontWeight: FontWeight.w500),
  //                         ),
  //                       ],
  //                     ),
  //                     onPressed: () {
  //                       Helpers().getCommonBottomSheet(
  //                           context: context,
  //                           content: CancellationBottomSheet(),
  //                           title: "Reason for Cancellation");
  //                       // Navigator.push(
  //                       //   context,
  //                       //   MaterialPageRoute(
  //                       //       builder: (context) => RideCompletedPage()),
  //                       // );
  //                     },
  //                   ),
  //                 ],
  //               )
  //             ],
  //           ))
  //     ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: Consumer(builder: (context, watch, child) {
          //rideVariablesNotifier = watch(rideVariablesNotifierProvider.notifier);
          return SafeArea(
            child: Stack(children: <Widget>[
              Column(
                children: [
                  Expanded(
                    child:
                        // _showGoogleMaps
                        //     ? Container(
                        //         child: GoogleMap(
                        //         markers: Set<Marker>.from(markers),
                        //         initialCameraPosition: _initialLocation,
                        //         myLocationEnabled: true,
                        //         myLocationButtonEnabled: false,
                        //         mapType: MapType.normal,
                        //         zoomGesturesEnabled: true,
                        //         zoomControlsEnabled: false,
                        //         // polylines: Set<Polyline>.of(polyLines!.values),
                        //         onMapCreated: (GoogleMapController controller) {
                        //           mapController = controller;
                        //           //_getCurrentLocation();
                        //         },
                        //       ))
                        //     : Center(
                        //         child: CupertinoActivityIndicator(
                        //           radius: 20,
                        //         ),
                        //       ),

                        _showGoogleMaps
                            ? Container(
                                child: Animarker(
                                useRotation: false,
                                duration: Duration(seconds: 45),
                                curve: Curves.ease,
                                mapId: controller.future
                                    .then<int>((value) => value.mapId),
                                //Grab Google Map Id
                                markers: markers1.values.toSet(),
                                child: GoogleMap(
                                  markers: Set<Marker>.from(markers),
                                  initialCameraPosition: _initialLocation,
                                  myLocationEnabled: true,
                                  myLocationButtonEnabled: false,
                                  mapType: MapType.normal,
                                  zoomGesturesEnabled: true,
                                  zoomControlsEnabled: false,
                                  polylines:
                                      Set<Polyline>.of(polyLines!.values),
                                  onMapCreated: (
                                    GoogleMapController gController,
                                  ) {
                                    mapController = gController;
                                    controller.complete(gController);
                                    //_getCurrentLocation();
                                  },
                                ),
                              ))
                            : Center(
                                child: CupertinoActivityIndicator(
                                  radius: 20,
                                ),
                              ),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: 20, left: 20, right: 20),
                child: FloatingActionButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Icon(Icons.arrow_back),
                  mini: true,
                  backgroundColor: Theme.of(context).colorScheme.secondary,
                ),
              ),
              Positioned(
                  bottom: 230,
                  right: 10,
                  child: widget.estimatedKm == ""
                      ? Container()
                      : Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(7),
                              color: Theme.of(context).colorScheme.secondary,
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.grey.withOpacity(0.4),
                                    blurRadius: 1,
                                    spreadRadius: 1,
                                    offset: Offset(1, 1))
                              ]),
                          padding: EdgeInsets.symmetric(
                              vertical: 10, horizontal: 15),
                          child: Text(
                            "${widget.estimatedKm} away",
                            style: TextStyle(fontWeight: FontWeight.w500),
                          ),
                        )),
              Positioned(
                bottom: 0,
                child: Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(30),
                            topLeft: Radius.circular(30)),
                        color: Colors.white),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                margin: EdgeInsets.all(10),
                                alignment: Alignment.center,
                                width: 55,
                                height: 1.8,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  color: Colors.grey[300],
                                ),
                              ),
                            ]),
                        Container(
                            alignment: Alignment.center,
                            padding: EdgeInsets.symmetric(
                                vertical: 5, horizontal: 20),
                            child: getTitle("Driver on the way")),
                        SizedBox(
                          height: 5,
                        ),
                        ListTile(
                          leading: Container(
                            height: 55,
                            width: 55,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                boxShadow: [
                                  BoxShadow(
                                      color: Theme.of(context)
                                          .primaryColorLight
                                          .withOpacity(0.2),
                                      blurRadius: 1,
                                      spreadRadius: 1),
                                ],
                                color: Theme.of(context).cardColor),
                            child: Image.asset(
                              App24UserAppImages.rideCompleteDriverImage,
                              width: 50,
                              height: 50,
                            ),
                          ),
                          title: Row(
                            children: [
                              Flexible(
                                child: Text(
                                  widget.providerName.toString(),
                                  style: TextStyle(fontWeight: FontWeight.w600),
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Text(
                                    "4.8",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 14),
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Icon(
                                    Icons.star,
                                    color: App24Colors
                                        .greenOrderEssentialThemeColor,
                                    size: 17,
                                  )
                                ],
                              )
                            ],
                          ),
                          subtitle: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Text(
                                widget.providerVehicleModel.toString(),
                                style: TextStyle(fontWeight: FontWeight.w500),
                              ),
                              Text(
                                widget.providerVehicleNumber.toString(),
                                style: TextStyle(fontWeight: FontWeight.w500),
                              ),
                            ],
                          ),
                          trailing: FloatingActionButton(
                            mini: true,
                            backgroundColor: App24Colors.darkTextColor,
                            heroTag: null,
                            child: Icon(
                              Icons.call,
                              color: Colors.white,
                            ),
                            onPressed: () {
                              // Navigator.push(
                              //   context,
                              //   MaterialPageRoute(
                              //       builder: (context) => RideDetailsPage(
                              //             pickupAddress: widget.pickUpAddress,
                              //             dropAddress: widget.dropAddress,
                              //             selectedVehicleType:
                              //                 widget.selectedVehicle,
                              //             estimatedKm: widget.estimatedKm,
                              //             type: "payment",
                              //           )),
                              // );
                            },
                          ),
                          isThreeLine: true,
                        ),
                        Divider(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            TextButton(
                              child: Column(
                                children: [
                                  Icon(
                                    App24User.alarm_light,
                                    size: 24,
                                    color: App24Colors.darkTextColor,
                                  ),
                                  Text(
                                    "SoS",
                                    style:
                                        TextStyle(fontWeight: FontWeight.w500),
                                  ),
                                ],
                              ),
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          RideCompletedPage()),
                                );
                              },
                            ),
                            TextButton(
                              child: Column(
                                children: [
                                  Icon(
                                      // App24User.share,
                                      // size: 24,
                                      Icons.share),
                                  Text(
                                    "Share",
                                    style:
                                        TextStyle(fontWeight: FontWeight.w500),
                                  ),
                                ],
                              ),
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          RideCompletedPage()),
                                );
                              },
                            ),
                            TextButton(
                              child: Column(
                                children: [
                                  Icon(
                                    Icons.close,
                                    size: 24,
                                    color: Colors.red,
                                  ),
                                  Text(
                                    "Cancel",
                                    style:
                                        TextStyle(fontWeight: FontWeight.w500),
                                  ),
                                ],
                              ),
                              onPressed: () {
                                Helpers().getCommonBottomSheet(
                                    context: context,
                                    content: CancellationBottomSheet(),
                                    title: "Reason for Cancellation");
                                // Navigator.push(
                                //   context,
                                //   MaterialPageRoute(
                                //       builder: (context) => RideCompletedPage()),
                                // );
                              },
                            ),
                          ],
                        )
                      ],
                    )),
              )
            ]),
          );
        }) // This trailing comma makes auto-formatting nicer for build methods.
        );
    // Scaffold(
    //   resizeToAvoidBottomInset: false,
    //   body: SafeArea(
    //     child: SingleChildScrollView(
    //       child: Column(
    //         children: [
    //           Row(
    //             mainAxisAlignment: MainAxisAlignment.end,
    //             children: <Widget>[
    //               // Padding(
    //               //     padding: EdgeInsets.only(bottom: 5, right: 10),
    //               //     child: Column(
    //               //       crossAxisAlignment: CrossAxisAlignment.end,
    //               //       children: [
    //               //         Container(
    //               //             decoration: BoxDecoration(
    //               //                 borderRadius: BorderRadius.circular(7),
    //               //                 color: Theme.of(context).cardColor,
    //               //                 boxShadow: [
    //               //                   BoxShadow(
    //               //                       color: Colors.grey.withOpacity(0.4),
    //               //                       blurRadius: 1,
    //               //                       spreadRadius: 1,
    //               //                       offset: Offset(1, 1))
    //               //                 ]),
    //               //             padding:
    //               //             EdgeInsets.symmetric(vertical: 10, horizontal: 15),
    //               //             child: Column(
    //               //               crossAxisAlignment: CrossAxisAlignment.end,
    //               //               children: [
    //               //                 Text(
    //               //                   "1.5 km away",
    //               //                   style: TextStyle(fontWeight: FontWeight.w500),
    //               //                 ),
    //               //                 Text(
    //               //                   "10 mins remaining",
    //               //                   style: TextStyle(fontWeight: FontWeight.w500),
    //               //                 ),
    //               //               ],
    //               //             )),
    //               //         SizedBox(
    //               //           height: 3,
    //               //         ),
    //               //       ],
    //               //     )
    //               // )
    //             ],
    //           ),
    //           // Container(
    //           //     width: MediaQuery.of(context).size.width,
    //           //     decoration: BoxDecoration(
    //           //         borderRadius: BorderRadius.only(
    //           //             topRight: Radius.circular(30),
    //           //             topLeft: Radius.circular(30)),
    //           //         color: Colors.white),
    //           //     child: Column(
    //           //       crossAxisAlignment: CrossAxisAlignment.stretch,
    //           //       mainAxisSize: MainAxisSize.min,
    //           //       children: <Widget>[
    //           //         Row(mainAxisAlignment: MainAxisAlignment.center, children: [
    //           //           Container(
    //           //             margin: EdgeInsets.all(10),
    //           //             alignment: Alignment.center,
    //           //             width: 55,
    //           //             height: 1.8,
    //           //             decoration: BoxDecoration(
    //           //               borderRadius: BorderRadius.circular(5),
    //           //               color: Colors.grey[300],
    //           //             ),
    //           //           ),
    //           //         ]),
    //           //         Container(
    //           //             alignment: Alignment.center,
    //           //             padding: EdgeInsets.symmetric(vertical: 5, horizontal: 20),
    //           //             child: getTitle("Driver on the way")),
    //           //         SizedBox(
    //           //           height: 5,
    //           //         ),
    //           //         ListTile(
    //           //           leading: Container(
    //           //             height: 55,
    //           //             width: 55,
    //           //             decoration: BoxDecoration(
    //           //                 borderRadius: BorderRadius.circular(10),
    //           //                 boxShadow: [
    //           //                   BoxShadow(
    //           //                       color: Theme.of(context)
    //           //                           .primaryColorLight
    //           //                           .withOpacity(0.2),
    //           //                       blurRadius: 1,
    //           //                       spreadRadius: 1),
    //           //                 ],
    //           //                 color: Theme.of(context).cardColor),
    //           //             child: Image.asset(
    //           //               App24UserAppImages.rideCompleteDriverImage,
    //           //               width: 50,
    //           //               height: 50,
    //           //             ),
    //           //           ),
    //           //           title: Row(
    //           //             children: [
    //           //               Flexible(
    //           //                 child: Text(
    //           //                   "Driver Name",
    //           //                   style: TextStyle(fontWeight: FontWeight.w600),
    //           //                 ),
    //           //               ),
    //           //               SizedBox(
    //           //                 width: 10,
    //           //               ),
    //           //               Row(
    //           //                 crossAxisAlignment: CrossAxisAlignment.end,
    //           //                 children: [
    //           //                   Text(
    //           //                     "4.8",
    //           //                     style: TextStyle(
    //           //                         fontWeight: FontWeight.w600, fontSize: 14),
    //           //                   ),
    //           //                   SizedBox(
    //           //                     width: 5,
    //           //                   ),
    //           //                   Icon(
    //           //                     Icons.star,
    //           //                     color: App24Colors.greenOrderEssentialThemeColor,
    //           //                     size: 17,
    //           //                   )
    //           //                 ],
    //           //               )
    //           //             ],
    //           //           ),
    //           //           subtitle: Column(
    //           //             crossAxisAlignment: CrossAxisAlignment.stretch,
    //           //             children: [
    //           //               Text(
    //           //                 "Honda Civic",
    //           //                 style: TextStyle(fontWeight: FontWeight.w500),
    //           //               ),
    //           //               Text(
    //           //                 "KL 45 L 2836",
    //           //                 style: TextStyle(fontWeight: FontWeight.w500),
    //           //               ),
    //           //             ],
    //           //           ),
    //           //           trailing: FloatingActionButton(
    //           //             mini: true,
    //           //             backgroundColor: App24Colors.darkTextColor,
    //           //             heroTag: null,
    //           //             child: Icon(
    //           //               Icons.call,
    //           //               color: Colors.white,
    //           //             ),
    //           //             onPressed: () {
    //           //               Navigator.push(
    //           //                 context,
    //           //                 MaterialPageRoute(
    //           //                     builder: (context) => RideDetailsPage(
    //           //                       pickupAddress: widget.pickUpAddress,
    //           //                       dropAddress: widget.dropAddress,
    //           //                       selectedVehicleType: widget.selectedVehicle,
    //           //                       estimatedKm: widget.estimatedKm,
    //           //                       type: "payment",
    //           //                     )),
    //           //               );
    //           //             },
    //           //           ),
    //           //           isThreeLine: true,
    //           //         ),
    //           //         Divider(),
    //           //         Row(
    //           //           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    //           //           children: [
    //           //             TextButton(
    //           //               child: Column(
    //           //                 children: [
    //           //                   Icon(
    //           //                     App24User.alarm_light,
    //           //                     size: 24,
    //           //                     color: App24Colors.darkTextColor,
    //           //                   ),
    //           //                   Text(
    //           //                     "SoS",
    //           //                     style: TextStyle(fontWeight: FontWeight.w500),
    //           //                   ),
    //           //                 ],
    //           //               ),
    //           //               onPressed: () {
    //           //                 Navigator.push(
    //           //                   context,
    //           //                   MaterialPageRoute(
    //           //                       builder: (context) => RideCompletedPage()),
    //           //                 );
    //           //               },
    //           //             ),
    //           //             TextButton(
    //           //               child: Column(
    //           //                 children: [
    //           //                   Icon(
    //           //                     // App24User.share,
    //           //                     // size: 24,
    //           //                       Icons.share),
    //           //                   Text(
    //           //                     "Share",
    //           //                     style: TextStyle(fontWeight: FontWeight.w500),
    //           //                   ),
    //           //                 ],
    //           //               ),
    //           //               onPressed: () {
    //           //                 Navigator.push(
    //           //                   context,
    //           //                   MaterialPageRoute(
    //           //                       builder: (context) => RideCompletedPage()),
    //           //                 );
    //           //               },
    //           //             ),
    //           //             TextButton(
    //           //               child: Column(
    //           //                 children: [
    //           //                   Icon(
    //           //                     Icons.close,
    //           //                     size: 24,
    //           //                     color: Colors.red,
    //           //                   ),
    //           //                   Text(
    //           //                     "Cancel",
    //           //                     style: TextStyle(fontWeight: FontWeight.w500),
    //           //                   ),
    //           //                 ],
    //           //               ),
    //           //               onPressed: () {
    //           //                 Helpers().getCommonBottomSheet(
    //           //                     context: context,
    //           //                     content: CancellationBottomSheet(),
    //           //                     title: "Reason for Cancellation");
    //           //                 // Navigator.push(
    //           //                 //   context,
    //           //                 //   MaterialPageRoute(
    //           //                 //       builder: (context) => RideCompletedPage()),
    //           //                 // );
    //           //               },
    //           //             ),
    //           //           ],
    //           //         )
    //           //       ],
    //           //     )),
    //           Container(
    //             height: MediaQuery.of(context).size.height,
    //
    //             child: Stack(
    //
    //               children: <Widget>[
    //                 _showGoogleMaps
    //                     ? Container(
    //                     margin: EdgeInsets.only(bottom: 0),
    //                     child: GoogleMap(
    //                       // markers: markers != null
    //                       //     ? Set<Marker>.from(markers)
    //                       //     : null,
    //                       initialCameraPosition: _initialLocation,
    //                       myLocationEnabled: false,
    //                       myLocationButtonEnabled: false,
    //                       mapType: MapType.normal,
    //                       zoomGesturesEnabled: true,
    //                       zoomControlsEnabled: false,
    //                       polylines: Set<Polyline>.of(polylines.values),
    //                       onMapCreated: (GoogleMapController controller) {
    //                         mapController = controller;
    //                         setInitialMapValues();
    //                       },
    //                     ))
    //                     : Center(
    //                   child: CupertinoActivityIndicator(
    //                     radius: 20,
    //                   ),
    //                 ),
    //                 Container(
    //                   margin: EdgeInsets.only(top: 20, left: 20),
    //                   child: FloatingActionButton(
    //                     onPressed: () {
    //                       Navigator.pop(context);
    //                     },
    //                     child: Icon(Icons.arrow_back),
    //                     mini: true,
    //                     backgroundColor: Theme.of(context).cardColor,
    //                   ),
    //                 ),
    //               ],
    //             ),
    //           )            ],
    //       ),
    //     ),
    //
    //   ));
  }
}
