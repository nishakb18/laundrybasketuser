// To parse this JSON data, do
//
//     final otpModel = otpModelFromJson(jsonString);

import 'dart:convert';

OtpModel otpModelFromJson(String str) => OtpModel.fromJson(json.decode(str));

String otpModelToJson(OtpModel data) => json.encode(data.toJson());

class OtpModel {
  String? statusCode;
  String? title;
  String? message;
  ResponseData? responseData;
  List<dynamic>? responseNewData;
  List<dynamic>? error;

  OtpModel({
    this.statusCode,
    this.title,
    this.message,
    this.responseData,
    this.responseNewData,
    this.error,
  });

  factory OtpModel.fromJson(Map<String, dynamic> json) => OtpModel(
    statusCode: json["statusCode"],
    title: json["title"],
    message: json["message"],
    responseData: json["responseData"] == null ? null : ResponseData.fromJson(json["responseData"]),
    responseNewData: json["responseNewData"] == null ? [] : List<dynamic>.from(json["responseNewData"]!.map((x) => x)),
    error: json["error"] == null ? [] : List<dynamic>.from(json["error"]!.map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "statusCode": statusCode,
    "title": title,
    "message": message,
    "responseData": responseData?.toJson(),
    "responseNewData": responseNewData == null ? [] : List<dynamic>.from(responseNewData!.map((x) => x)),
    "error": error == null ? [] : List<dynamic>.from(error!.map((x) => x)),
  };
}

class ResponseData {
  The0? the0;
  dynamic yourOtp;

  ResponseData({
    this.the0,
    this.yourOtp,
  });

  factory ResponseData.fromJson(Map<String, dynamic> json) => ResponseData(
    the0: json["0"] == null ? null : The0.fromJson(json["0"]),
    yourOtp: json["YourOtp"],
  );

  Map<String, dynamic> toJson() => {
    "0": the0?.toJson(),
    "YourOtp": yourOtp,
  };
}

class The0 {
  int? userStatus;

  The0({
    this.userStatus,
  });

  factory The0.fromJson(Map<String, dynamic> json) => The0(
    userStatus: json["user_status"],
  );

  Map<String, dynamic> toJson() => {
    "user_status": userStatus,
  };
}
