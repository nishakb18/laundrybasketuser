LoginModel loginModelFromJson(str) {
  return LoginModel.fromJson(str);
}

class LoginModel {
  String? statusCode;
  String? title;
  String? message;
  ResponseData? responseData;
  List<dynamic>? error;

  LoginModel(
      {this.statusCode,
      this.title,
      this.message,
      this.responseData,
      this.error});

  LoginModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    title = json['title'];
    message = json['message'];
    responseData = json['responseData'] != null
        ? new ResponseData.fromJson(json['responseData'])
        : null;
    if (json['error'] != null) {
      error = [];
      json['error'].forEach((v) {
        error!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['title'] = this.title;
    data['message'] = this.message;
    if (this.responseData != null) {
      data['responseData'] = this.responseData!.toJson();
    }
    if (this.error != null) {
      data['error'] = this.error!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ResponseData {
  String? tokenType;
  int? expiresIn;
  String? accessToken;
  User? user;
  String? mobile;

  ResponseData(
      {this.tokenType,
      this.expiresIn,
      this.accessToken,
      this.user,
      this.mobile});

  ResponseData.fromJson(Map<String, dynamic> json) {
    tokenType = json['token_type'];
    expiresIn = json['expires_in'];
    accessToken = json['access_token'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
    mobile = json['mobile'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['token_type'] = this.tokenType;
    data['expires_in'] = this.expiresIn;
    data['access_token'] = this.accessToken;
    if (this.user != null) {
      data['user'] = this.user!.toJson();
    }
    data['mobile'] = this.mobile;
    return data;
  }
}

class User {
  int? id;
  String? firstName;
  String? lastName;
  String? paymentMode;
  String? userType;
  dynamic email;
  String? mobile;
  String? gender;
  String? countryCode;
  String? currencySymbol;
  String? picture;
  String? address;
  dynamic pincode;
  String? loginBy;
  double? latitude;
  double? longitude;
  dynamic walletBalance;
  String? language;
  String? referralUniqueId;
  int? countryId;
  int? stateId;
  int? cityId;
  int? companyId;
  int? status;
  String? appVersion;
  String? createdAt;

  User(
      {this.id,
      this.firstName,
      this.lastName,
      this.paymentMode,
      this.userType,
      this.email,
      this.mobile,
      this.gender,
      this.countryCode,
      this.currencySymbol,
      this.picture,
      this.address,
      this.pincode,
      this.loginBy,
      this.latitude,
      this.longitude,
      this.walletBalance,
      this.language,
      this.referralUniqueId,
      this.countryId,
      this.stateId,
      this.cityId,
      this.companyId,
      this.status,
      this.appVersion,
      this.createdAt});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    paymentMode = json['payment_mode'];
    userType = json['user_type'];
    email = json['email'];
    mobile = json['mobile'];
    gender = json['gender'];
    countryCode = json['country_code'];
    currencySymbol = json['currency_symbol'];
    picture = json['picture'];
    address = json['address'];
    pincode = json['pincode'];
    loginBy = json['login_by'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    walletBalance = json['wallet_balance'];
    language = json['language'];
    referralUniqueId = json['referral_unique_id'];
    countryId = json['country_id'];
    stateId = json['state_id'];
    cityId = json['city_id'];
    companyId = json['company_id'];
    status = json['status'];
    appVersion = json['app_version'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['payment_mode'] = this.paymentMode;
    data['user_type'] = this.userType;
    data['email'] = this.email;
    data['mobile'] = this.mobile;
    data['gender'] = this.gender;
    data['country_code'] = this.countryCode;
    data['currency_symbol'] = this.currencySymbol;
    data['picture'] = this.picture;
    data['address'] = this.address;
    data['pincode'] = this.pincode;
    data['login_by'] = this.loginBy;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['wallet_balance'] = this.walletBalance;
    data['language'] = this.language;
    data['referral_unique_id'] = this.referralUniqueId;
    data['country_id'] = this.countryId;
    data['state_id'] = this.stateId;
    data['city_id'] = this.cityId;
    data['company_id'] = this.companyId;
    data['status'] = this.status;
    data['app_version'] = this.appVersion;
    data['created_at'] = this.createdAt;
    return data;
  }
}
