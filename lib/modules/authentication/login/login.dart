import 'package:app24_user_app/constants/asset_path.dart';
import 'package:app24_user_app/constants/color_coman.dart';
import 'package:app24_user_app/constants/color_path.dart';
import 'package:app24_user_app/modules/sign_in/otp_verification.dart';
import 'package:app24_user_app/utils/helpers/common_helpers.dart';
import 'package:app24_user_app/utils/services/api/api_providers.dart';
import 'package:app24_user_app/utils/services/firebase/firebase_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class LogInPage extends StatefulWidget {
  const LogInPage({super.key, this.mobile});

  final String? mobile;

  @override
  State<LogInPage> createState() => _LogInPageState();
}

class _LogInPageState extends State<LogInPage> {
//  late final SocketService _socketService;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  // late SmsUserConsent smsUserConsent;
  //final ApiRepository _apiRepository = ApiRepository();

  final List _countryCode = ["+971", "+92", "+93", "+94", "+101", "+91"];
  String? _countryCodeValue;
  var mobileNumberController = TextEditingController();
  bool isPhoneAutoFetched = false;

  @override
  void dispose() {
    showLog("testing dispose");
    mobileNumberController.dispose();
    // smsUserConsent.dispose();
    super.dispose();
  }

  updatePhoneNumber(String number) {
    if (number.contains("91")) {
      return number.substring(3, number.length);
    } else if (number.startsWith("0")) {
      return number.substring(1, number.length);
    } else {
      return number;
    }
  }
  String? removePlus(String phoneNumber) {
    return phoneNumber.replaceAll('+', '');
  }
  sendOTP(String? countryCode) {
    // FirebaseNotifications(context).firebaseCloudMessagingListeners();
    var body = {
      'country_code': removePlus(countryCode ?? '971') ?? '971',
      'mobile': mobileNumberController.text,
      'salt_key': "MQ==",

      // 'type': "user"
    };

    context.read(sendOtpProvider.notifier).sendOtp(body: body);
  }

/*
  Future<Null> checkMobileNumbersacsac1() async {
    if (_formKey.currentState!.validate()) {
      var prefs = await SharedPreferences.getInstance();

      Map<String, dynamic> body = {
        'country_code': '91',
        'salt_key': 'MQ==',
        'version': prefs.getString(SharedPreferencesPath.playStoreVersionKey),
        'device_token': prefs.getString(SharedPreferencesPath.firebaseTokenKey),
        'mobile': mobileNumberController.text.trim(),
        'checkexist': 1
      };

      try {
        var result = await _apiRepository.fetchLogin(context, body);
        Navigator.pop(context);
        if (result.responseData != null) {
          _socketService = SocketService.instance;
          SharedPreferences prefs = await SharedPreferences.getInstance();
          // prefs.setString(SharedPreferencesPath.glblSrchLclTimeStamp,)
          // prefs.setBool(SharedPreferencesPath.isGlobalSearchDone,true);
          prefs.setBool(SharedPreferencesPath.isUserRegisteredKey, true);
          prefs.setString(SharedPreferencesPath.accessToken,
              result.responseData!.accessToken!);
          context.read(homeNotifierProvider.notifier).getAppConfigurations();
          context.read(homeNotifierProvider.notifier).getProfile();
          context.read(homeNotifierProvider.notifier).getCurrentLocation();
          context
              .read(orderDetailsNotifierProvider.notifier)
              .checkOngoingOrders();
          getSavedAddresses();
          _socketService.listenSocket();
          // Navigator.of(context).pushReplacementNamed(homeScreen);
          Navigator.push(context, MaterialPageRoute(builder: (context) => OTPVerificationPage(
            mobile: mobileNumberController.text,
          )));
        } else {
          showMessage(context, result.message!,false,true);
        }
      } catch (e) {
        Navigator.pop(context);
        showMessage(context, e.toString(),true,true);
        print(e);
      }
      // final results = List<Map<String, dynamic>>.from(result['responseData']);

      //print(result);

*/ /*      CommonHelpers().getPostData("/shop/exist", body,null).then((value) {
        Navigator.pop(context);
        if(value == "error") {
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            behavior: SnackBarBehavior.floating,
            content: Text("Please check your connection and try again!"),
          ));
          // Navigator.pop(context);
        }
        else if (json.decode(value)['statusCode'] == "200") {
          */ /* */ /* Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => PasswordVerificationPage(
                    mobile: mobileNumberController.text.trim(),
                  )));*/ /* */ /*

          if (json.decode(value)['message'] == 'Shop exists') {
            //Navigator.pop(context);
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => PasswordVerificationPage(
                      mobile: mobileNumberController.text.trim(),
                    )));
          } else {
            _scaffoldKey.currentState.showSnackBar(SnackBar(
              behavior: SnackBarBehavior.floating,
              content: Text(json.decode(value)['message']),
            ));
            //Navigator.pop(context);
          }
        }else{
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            behavior: SnackBarBehavior.floating,
            content: Text(json.decode(value)['message']),
          ));
          //Navigator.pop(context);
        }
      });*/ /*
    }
  }*/

  Future<bool> onWillPop() async {
    return (await SystemChannels.platform
            .invokeMethod('SystemNavigator.pop')) ??
        false;
  }

  DateTime preBackPress = DateTime.now();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body:
          //  WillPopScope(
          //   onWillPop: onWillPop,
          //   child: ProviderListener(
          //     provider: sendOtpProvider,
          //     onChange: (context, dynamic state) {
          //       if (state.isLoading!) {
          //         showProgress(context);
          //       } else if (state.isError!) {
          //         Navigator.pop(context);
          //         showMessage(context, state.errorMessage!, true, true);
          //       } else {
          //         Navigator.pop(context); //2955
          //         print(mobileNumberController);
          //         Navigator.push(
          //             context,
          //             MaterialPageRoute(
          //                 builder: (context) => OTPVerificationPage(
          //                       mobile: mobileNumberController.text,
          //                     )));
          //       }
          //     },
          //     child:
          SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 50),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              // mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  "Welcome",
                  style: TextStyle(fontSize: 36, fontWeight: FontWeight.bold),
                ),
                const Text(
                  "Get started and order from nearby businesses!",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
                ),
                const SizedBox(
                  height: 30,
                ),
                Center(
                    child: Image.asset(
                  App24UserAppImages.otpLoginBg,
                  width: 300,
                )),
                const SizedBox(
                  height: 20,
                ),
                // Text(
                //   "Get 4-digit OTP",
                //   style: TextStyle(
                //       fontSize: MediaQuery.of(context).size.width / 20,
                //       fontWeight: FontWeight.bold),
                // ),
                const SizedBox(
                  height: 25,
                ),
                Container(
                  padding: const EdgeInsets.only(left: 5),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25),
                    color: const Color(0xfff5f5f5),
                  ),
                  child: Form(
                    key: _formKey,
                    child: Row(
                      children: [
                        Container(
                          padding: const EdgeInsets.symmetric(
                              vertical: 0, horizontal: 10),
                          decoration: BoxDecoration(
                            // color: Colors.grey[200],
                            borderRadius: BorderRadius.circular(15),
                          ),
                          child: DropdownButton(
                              underline: const SizedBox(),
                              isExpanded: false,
                              // hint: Text(
                              //   "Gender",
                              //   style: TextStyle(color: App24Colors.darkTextColor,fontSize: MediaQuery.of(context).size.width/25),
                              // ),
                              elevation: 2,
                              dropdownColor: Colors.white,
                              iconEnabledColor: App24Colors.darkTextColor,
                              style: const TextStyle(
                                  color: App24Colors.darkTextColor,
                                  fontWeight: FontWeight.bold),
                              hint: Text(_countryCode[0].toString()),
                              value: _countryCodeValue,
                              onChanged: (dynamic value) {
                                setState(() {
                                  _countryCodeValue = value;
                                });
                              },
                              items: _countryCode.map((value) {
                                return DropdownMenuItem(
                                    value: value, child: Text(value));
                              }).toList()),
                        ),
                        Expanded(
                          child: TextFormField(
                            onTap: () {
                              // if (!isPhoneAutoFetched)
                              //   smsUserConsent.requestPhoneNumber();
                            },
                            validator: (String? arg) {
                              showLog("arg.length :: ${arg?.length}");
                              if (arg!.isEmpty) {
                                return 'Mobile Number can\'t be empty!';
                              } else {
                                if (arg.length <8 ) {
                                  return 'Mobile Number not valid!';
                                } else {
                                  return null;
                                }
                              }
                            },
                            keyboardType: TextInputType.number,
                            style: TextStyle(
                              fontSize: MediaQuery.of(context).size.width / 24,
                              color: App24Colors.darkTextColor,
                              fontWeight: FontWeight.w600,
                            ),
                            controller: mobileNumberController,
                            textInputAction: TextInputAction.done,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              contentPadding: const EdgeInsets.symmetric(
                                  horizontal: 15, vertical: 20),
                              fillColor: const Color(0xfff5f5f5),
                              filled: true,
                              hintText: "Phone Number",
                              hintStyle: TextStyle(
                                  fontSize:
                                      MediaQuery.of(context).size.width / 24,
                                  fontWeight: FontWeight.w600,
                                  color: const Color(0xff929292)),
                              focusedBorder: OutlineInputBorder(
                                borderSide: const BorderSide(color: Colors.white),
                                borderRadius: BorderRadius.circular(25),
                              ),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: const BorderSide(color: Colors.white),
                                borderRadius: BorderRadius.circular(25),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
                // const SizedBox(
                //   height: 20,
                // )
                const SizedBox(
                  height: 20,
                ),

                const Divider(),
                const SizedBox(
                  height: 20,
                ),
                Material(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25)),
                  color: AppColor.carvebarcolor,
                  child: InkWell(
                    borderRadius: BorderRadius.circular(25),
                    onTap: () {
                      {
                        if (_formKey.currentState!.validate()) {
                          FirebaseNotifications(context)
                              .firebaseCloudMessagingListeners();
                          showLog("_countryCodeValue :: $_countryCodeValue");
                          sendOTP(_countryCodeValue);
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => OTPVerificationPage(
                                      mobile: mobileNumberController.text,
                                  countryCode: _countryCodeValue ?? '+971',
                                    )),
                          );
                        }
                      }
                    },
                    child: const Padding(
                      padding: EdgeInsets.symmetric(vertical: 20),
                      child: Center(
                        child: Text(
                          "Send OTP",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w600,
                              fontSize: 20),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
